ifndef TARGET
	TARGET = target
endif

GIT_BRANCH ?= $(shell git rev-parse --abbrev-ref HEAD)
ifeq ($(GIT_BRANCH), development)
	CATO_VERSION_BRANCH = $(CATO_VERSION)
else ifeq ($(GIT_BRANCH), origin/development)
	CATO_VERSION_BRANCH = $(CATO_VERSION)
else
	GIT_COMMIT ?= $(shell git log -1 --pretty=format:"%H")
	GIT_COMMIT_SHORT ?= $(shell git rev-parse --short $(GIT_COMMIT))
	CATO_VERSION_BRANCH ?= $(CATO_VERSION)+$(subst /,_,$(GIT_BRANCH))+$(subst /,_,$(GIT_COMMIT_SHORT))
endif


DOCKER_REGISTRY?=docker-registry.cato.cyberpointllc.com:5000
DOCKER_USER = $(shell grep '^' ~/.docker-user-identity | awk -F@ '{print $$1}')


RM = rm -rf
MKDIR = mkdir -p
CP = cp -R

PACKAGE_NAME = cato-ui
BUILD_IMAGE = build_ui

DEPENDENCY_CACHE = $(TARGET)/dependency_cache
INSTALLATION = installation
THEMES = $(INSTALLATION)/themes
NODE = node_modules

NODE_VERSION = 13.2.0
NVM_DIR ?= $(HOME)/.nvm
NPM = $(NVM_DIR)/versions/node/v$(NODE_VERSION)/bin/npm
NODE = $(NVM_DIR)/versions/node/v$(NODE_VERSION)/bin/node


ARTIFACTORY_URL = http://artifactory.cato.cyberpointllc.com
CPE_PATH = ext-release-local/nist/cpe/2.3
CPE_ALL = cpe.2.3.all.min.json.gz



# tell `make` that these tasks aren't files
.PHONY: main all fast-all fast-init init npm create-dirs cpe-json build fast-build angular angular-prod config check-env docker-build watch refresh-templates develop library-contents

# use a fast build if NO_VPN is defined (still need VPN to push docker containers unless SKIP_PUSH=true)
ifndef NO_VPN
main: all
else
main: fast-all
endif

all: clean init build docker-build
fast-all: clean fast-init fast-build docker-build


clean-all: clean
	$(RM) $(NODE)
	$(RM) $(BOWER)
	$(RM) $(TARGET)

clean:
	$(RM) dist/*
	$(RM) src/less/libs
ifndef NO_VPN	# this syntax is ridiculous
	$(RM) $(TARGET)
endif

fast-init: create-dirs config
init: npm fast-init cpe-json library-contents
	# See https://cyberpointllc.slack.com/archives/G69UA672R/p1517585199000717 for the issue the line below fixes
	sudo chown -R ${USER}:$(id -gn ${USER}) /home/${USER}/.config/configstore

npm:
	# NOTE: this only uses this version of nvm for this session.
	# And yes, it's dumb to install node again here, but jenkins won't build otherwise.
	\. "$(NVM_DIR)/nvm.sh" && nvm install $(NODE_VERSION) && nvm use $(NODE_VERSION) && npm install


create-dirs:
	$(MKDIR) dist/app


cpe-json:
ifndef NO_VPN
	$(MKDIR) $(DEPENDENCY_CACHE) && cd $(DEPENDENCY_CACHE) && \
	wget $(ARTIFACTORY_URL)/$(CPE_PATH)/$(CPE_ALL) && \
	gunzip -f $(CPE_ALL) && $(RM) $(CPE_ALL)
endif


library-contents:
ifndef NO_VPN
	cd src/app/library/docs/ && \
	wget -r --no-parent -nH --cut-dirs=4 --reject="index.html*" --reject="*.md5" --reject="*.sha1" \
	$(ARTIFACTORY_URL)/artifactory/ext-release-local/cato/library/
endif


config: check-env
	cp installation/config.js dist/app/
	sed -i -e "s/VERSION/$(CATO_VERSION_BRANCH)/g" dist/app/config.js



build: angular-prod

fast-build: angular


angular-prod:
ifndef SOURCE_MAPS
	$(NPM) run build:prod
else
	$(NPM) run build:prod -- --source-map=$(SOURCE_MAPS)
endif

angular:
	$(NPM) run build
	# assuming that if you're running this, it's for dev, so set the theme
	$(NPM) run theme

# build and watch for changes
watch: config
	$(NPM) run build:watch


refresh-templates:
	cd src/load && find ../app -name *.html | node ../../createImports.js > cato-templates.ts


check-env:
ifndef CATO_VERSION
	$(error CATO_VERSION is undefined)
endif


#######################################################################################

# run docker build and then push to registry. requires package to be built first on dev's env for now
docker-build:
	../utilities/build_and_push.sh ./ $(PACKAGE_NAME) $(CATO_VERSION) LATEST

#######################################################################################



#######################################################################################

# install the tools necessary for development
develop:
	git config --global url."https://".insteadOf git://
	sudo mkdir -p /usr/local/nvm
	sudo mkdir -p ${HOME}/.nvm/.cache
	sudo curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.0/install.sh | NVM_DIR=/usr/local/nvm sudo bash
	sudo chown -R ${USER}:${USER} /usr/local/nvm
	sudo chown -R ${USER}:${USER} $(NVM_DIR)
	@echo "*** NOTE: If this is your first time installing nvm, you must"
	@echo "*** start a new terminal session to proceed with installing node/npm."
	@echo "*** Then, you may simply re-run this 'make develop' command."
	\. "$(NVM_DIR)/nvm.sh" && nvm install $(NODE_VERSION)

#######################################################################################
