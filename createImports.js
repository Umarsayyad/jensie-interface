//Eric Warrington

const stdin=process.openStdin();
let data="";

stdin.on('data', chunk => data+=chunk);
stdin.on('end', () => createImports(data));


/**
 * @param chunk {Buffer}
 */
function createImports(chunk)
{
	//split chunk by line
	let lines=chunk.toString().split('\n').sort();

	//iterate over each line
	for(let l of lines)
	{
        //skip empty lines
        if(!l) continue;

		//modify line to include `import`
		l=`import '${l}';`;
		//log modified line so it can be piped to a file
		console.log(l);
	}
};
