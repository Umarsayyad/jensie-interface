const lodash=require("lodash");
const webpackMerge=require("webpack-merge");
const TerserPlugin=require("terser-webpack-plugin");
const IndexHtmlWebpackPlugin=require("./index-html-webpack-plugin").IndexHtmlWebpackPlugin;

const angular8Cg =
{
	name: "@angular",
	chunks: "all",
	enforce: true,
	priority: 15,
	test: /[\\/]node_modules[\\/](@angular)[\\/]/,
};
const angularJsCg =
{
	name: "angularjs",
	chunks: "all",
	enforce: true,
	priority: 14,
	test: /[\\/]node_modules[\\/](.*angular.*|ng-.*)[\\/]/,
};
const largeDepsCg =
{
	name: "vendor-3",
	chunks: "all",
	enforce: true,
	priority: 15,
	test: /[\\/]node_modules[\\/](cytoscape.*|lodash|moment|nvd3.*|ol.*)[\\/]/,
};
const vendor2Cg =
{
	name: "vendor-2",
	chunks: "all",
	enforce: true,
	priority: 14,
	test: /[\\/]node_modules[\\/]([m-zM-Z].*)[\\/]/,
};


const webpackConfig =
{
	externals:
	{
		jquery: "jQuery",
		angular: "angular",
		"@cato/config": "{ config }",
		"ng-table": "''",
	},
	module:
	{
		rules:
		[
			{
				test: /\.html?$/,
				use: [`ngtemplate-loader?relativeTo=${__dirname}/src`, "html-loader"],
			},
		]
	},
};

const terserOptions =
{
	mangle: false,
	keep_fnames: true,
	keep_classnames: true,
};

const entrypoints =
[
	"polyfills-nomodule-es5",
	"runtime",
	"polyfills-es5",
	"polyfills",
	"sw-register",
	"styles",
	"scripts",

	angular8Cg.name,
	angularJsCg.name,
	largeDepsCg.name,
	vendor2Cg.name,
	
	"vendor",
	"main",
];





// This code comes directly from '@angular-bulders/custom-webpack/dist/webpack-config-merger.js'
function mergeConfigs(webpackConfig1, webpackConfig2, mergeStrategies = {}, replacePlugins = false)
{
	const mergedConfig = webpackMerge.smartStrategy(mergeStrategies)(webpackConfig1, webpackConfig2);
	if(webpackConfig1.plugins && webpackConfig2.plugins)
	{
		const conf1ExceptConf2 = lodash.differenceWith(webpackConfig1.plugins, webpackConfig2.plugins, (item1, item2) => item1.constructor.name === item2.constructor.name);
		if(!replacePlugins)
		{
			const conf1ByName = lodash.keyBy(webpackConfig1.plugins, "constructor.name");
			webpackConfig2.plugins = webpackConfig2.plugins.map(p => conf1ByName[p.constructor.name] ? lodash.merge(conf1ByName[p.constructor.name], p) : p);
		}
		mergedConfig.plugins = [...conf1ExceptConf2, ...webpackConfig2.plugins];
	}

	return mergedConfig;
}

// This code comes directly from 'angular-builder-custom-terser-options/dist/src/common.js'
function mergeTerserOptions(webpackConfig, terserOptions)
{
	if(webpackConfig.optimization && webpackConfig.optimization.minimizer && Array.isArray(webpackConfig.optimization.minimizer))
	{
		const terserPlugin = webpackConfig.optimization.minimizer.find(isTerserPlugin);

		if(terserPlugin)
		{
			const terserOptionsOriginal = terserPlugin.options.terserOptions;
			terserPlugin.options.terserOptions = Object.assign({}, terserOptionsOriginal, terserOptions);

			console.log(terserPlugin.options.terserOptions.mangle);
			if(terserPlugin.options.terserOptions.mangle) throw "your a fukin bich!";
		}
		else
		{
			console.log("WTF!", webpackConfig.optimization.minimizer);
		}
	}

	return webpackConfig;
}

function isTerserPlugin(minimizer)
{
	return minimizer instanceof TerserPlugin || (minimizer.options && minimizer.options.terserOptions);
}

function addToCacheGroups(config, cgArray)
{
	for(let cg of cgArray)
		config.optimization.splitChunks.cacheGroups[cg.name]=cg;
}





// See https://github.com/just-jeb/angular-builders/tree/master/packages/custom-webpack#custom-webpack-config-function
return module.exports = (config, options) =>
{
	// console.log(config);
	// console.log("\n\n\n\n");
	// console.log(JSON.stringify(config.optimization.splitChunks));
	// console.log(JSON.stringify(options, null, 2));
	// console.log("\n\n\n\n");
	// console.log(options);
	// console.log("\n\n\n\n");



	// Merge the webpack configs using the default custom-webpack method
	let newConfig=mergeConfigs(config, webpackConfig);

	newConfig.module.rules.forEach((rule) =>
	{
		if(rule.test.source === "\\.less$")
		{
			let ll = rule.use.find(u => u.loader && u.loader.includes("less-loader"));
			if(ll)
			{
				ll.options.modifyVars = {...ll.options.modifyVars, catotheme: process.env.CATO_THEME || "default"};
			}
		}
	});

	// Do these things only if optimization is set to true (production builds)
	if(options.optimization)
	{
		// Merge the default terser options with our custom ones
		newConfig=mergeTerserOptions(newConfig, terserOptions);
		
		addToCacheGroups(newConfig, [angular8Cg, angularJsCg, largeDepsCg, vendor2Cg]);
	
		// This code comes directly from 'node_modules/@angular-devkit/build-angular/src/dev-server/index.js'
		newConfig.plugins.push(new IndexHtmlWebpackPlugin(
		{
			input: "src/app/index.html",
			output: "index.html",
			baseHref: '/',	// TODO: is this correct?
			deployUrl: options.deployUrl,
			sri: options.subresourceIntegrity,
			noModuleEntrypoints: ["polyfills-es5"],
			crossOrigin: options.crossOrigin || "none",

			entrypoints,
		}));
	}

	return newConfig;
};

