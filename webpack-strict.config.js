//This file is necessary for building with the 'strict' configuration.
//Use the default webpack config, but add the catotheme less variable.
return module.exports = (config, options) =>
{
	config.module.rules.forEach((rule) =>
	{
		if(rule.test.source === "\\.less$")
		{
			let ll = rule.use.find(u => u.loader && u.loader.includes("less-loader"));
			if(ll)
			{
				ll.options.modifyVars = {...ll.options.modifyVars, catotheme: process.env.CATO_THEME || "default"};
			}
		}
    });
    
    return config;
}
