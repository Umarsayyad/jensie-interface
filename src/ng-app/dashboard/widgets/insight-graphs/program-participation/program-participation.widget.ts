import { InjectionToken } from '@angular/core';

import { IWidgetParams } from '../../../models/widget.interface';
import { loadForInsight } from '../../../services/load-for-utils';
//import { InsightGraphsComponent } from "./insight-graphs.component";

export const id = 'program-participation';

export const PROGRAM_PARTICIPATION_WIDGET = new InjectionToken(id);

export const programParticipationWidget: IWidgetParams = {
    id,
    version: 1,
    title: 'Program Participation',
    gridsterClasses: [],
    sizeX: 3,
    sizeY: 2,
    isAngularComponent: true,
    component: PROGRAM_PARTICIPATION_WIDGET,
    templateUrl: '',
    update: () => null,
    shouldLoad: loadForInsight,
    description:
        'Number of participating companies that are also part of the NIST Manufacturing Extension Partnership (MEP) and/or the Mentor-Protege Program.',
};
