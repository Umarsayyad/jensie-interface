import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Crossfilter } from 'crossfilter2';
import { format } from 'd3';
import { redrawAll, RowChart, rowChart } from 'dc';
import { noop, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { LoadingContentComponent } from '../../../../common';
import { rowChartColors } from '../../../../common/data';
import { IWidget } from '../../../models/widget.interface';
import { DashboardWidgets } from '../../../services/dashboard-widgets.service';
import { BarChartConfig } from '../data/bar-chart-config';
import { InsightWidgetsService, LoadProgress } from '../insight-widgets.service';
import { Insight } from '../models/insight/insight.namespace';
import { ParticipationDimensionService } from '../services/participation-dimension.service';
import { id } from './program-participation.widget';

@Component({
    selector: id,
    templateUrl: '../common/insight-widget.html',
    styleUrls: ['../common/insight-widget.less'],
})
export class ProgramParticipationComponent implements OnInit, AfterViewInit, OnDestroy {
    private widget: IWidget | undefined;
    private ndx: Crossfilter<Insight.ICustomerCrossfilterRecord>;
    private participationChart: RowChart;
    private onDestroy$ = new Subject();

    //must start at -1 because otherwise the graph ElementRef is undefined
    size = -1;
    showReset: boolean;

    @ViewChild('spinner') spinner: LoadingContentComponent;
    @ViewChild('widgetGraph') participation: ElementRef;

    constructor(
        private dbWidgets: DashboardWidgets,
        private widgetManager: InsightWidgetsService,
        private dimService: ParticipationDimensionService
    ) {}

    ngOnInit() {
        this.widget = this.dbWidgets.getWidget({ id });
        if (!this.widget) throw new Error('Error: Could not find Program Participation widget.');
    }

    ngAfterViewInit() {
        Promise.resolve().then(() => this.spinner.startLoading());

        this.participationChart = rowChart(this.participation.nativeElement);

        this.widgetManager.getNdx().then((ndx) => {
            this.ndx = ndx;

            // Don't subscribe after the component is unloaded (possible if the user leaves the tab before any requests complete)
            if (!this.onDestroy$.isStopped) {
                this.widgetManager
                    .loadProgress()
                    .pipe(takeUntil(this.onDestroy$))
                    .subscribe((status) => {
                        if (status === LoadProgress.Started) {
                            this.spinner.startLoading();
                        } else if (status === LoadProgress.LevelLoaded) {
                            this.spinner.stopLoading();
                            this.size = this.dimService.getGroup().size();
                            setTimeout(() => this.participationChart.render());
                        }
                    });

                this.configureCharts();
                this.participationChart.render();

                this.widget.update = () => this.participationChart.render();

                this.spinner.stopLoading();
            }
        });

        this.dimService
            .filtersChange()
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((filters) => {
                if (filters.length > 0) (this.participationChart as any).replaceFilter([filters]);
                else this.participationChart.filterAll();

                if (this.ndx) redrawAll();
            });
    }

    ngOnDestroy(): void {
        this.onDestroy$.next();
        this.onDestroy$.complete();
        this.widget.update = noop;
    }

    hideWidget() {
        this.dbWidgets.hideWidget(this.widget);
    }

    private configureCharts() {
        const participationDimension = this.dimService.getDimension();
        const participationGroup = this.dimService.getGroup();

        this.size = participationGroup.size();

        this.participationChart
            .on('filtered.showReset', (chart) => {
                this.showReset = chart.hasFilter();
            })
            .ordinalColors(rowChartColors)
            .elasticX(true)
            .filterHandler(this.dimService.getFilterHandler())
            .dimension(participationDimension)
            .gap(BarChartConfig.GAP)
            .labelOffsetY(BarChartConfig.LABEL_OFFSET_Y)
            .labelOffsetX(BarChartConfig.LABEL_OFFSET_X)
            .group(participationGroup);

        this.participationChart.onClick = (d) => {
            const key = this.participationChart.keyAccessor()(d);
            if (key === 'Others' && d.others) this.dimService.toggleFilters(d.others);
            else this.dimService.toggleFilters(key);
        };

        this.participationChart.xAxis().tickFormat(format(',d'));
    }

    resetFilters() {
        this.dimService.setFilters([]);
        this.showReset = false;
    }
}
