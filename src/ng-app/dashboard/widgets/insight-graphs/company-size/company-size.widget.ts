import { InjectionToken } from '@angular/core';

import { IWidgetParams } from '../../../models/widget.interface';
import { loadForInsight } from '../../../services/load-for-utils';

export const id = 'company-size';

export const COMPANY_SIZE_WIDGET = new InjectionToken(id);

export const companySizeWidget: IWidgetParams = {
    id,
    version: 1,
    title: 'Employees Per DIB Participant',
    gridsterClasses: [],
    sizeX: 2,
    sizeY: 3,
    isAngularComponent: true,
    component: COMPANY_SIZE_WIDGET,
    templateUrl: '',
    update: () => null,
    shouldLoad: loadForInsight,
    description: 'How many employees each participating company has on staff.',
};
