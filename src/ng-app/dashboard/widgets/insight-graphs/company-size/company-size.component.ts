import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Crossfilter } from 'crossfilter2';
import { legend, PieChart, pieChart, redrawAll } from 'dc';
import { noop, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { LoadingContentComponent } from '../../../../common/components';
import { defaultColors } from '../../../../common/data';
import { IWidget, WidgetConst } from '../../../models/widget.interface';
import { DashboardWidgets } from '../../../services/dashboard-widgets.service';
import { InsightWidgetsService, LoadProgress } from '../insight-widgets.service';
import { Insight } from '../models/insight/insight.namespace';
import { SizeDimensionService } from '../services/size-dimension.service';
import { id } from './company-size.widget';

@Component({
    selector: id,
    templateUrl: '../common/insight-widget.html',
    styleUrls: ['../common/insight-widget.less'],
})
export class CompanySizeComponent implements OnInit, AfterViewInit, OnDestroy {
    readonly MAX_LEGEND_ITEMS: number = 10;

    private widget: IWidget | undefined;
    private ndx: Crossfilter<Insight.ICustomerCrossfilterRecord>;
    private deferRender: Promise<void>;
    private sizeChart: PieChart;
    private onDestroy$ = new Subject();

    //must start at -1 because otherwise the graph ElementRef is undefined
    size = -1;
    showReset: boolean;

    @ViewChild('spinner') spinner: LoadingContentComponent;
    @ViewChild('widgetContainer') widgetContainer: ElementRef;
    @ViewChild('widgetGraph') sizeGraph: ElementRef;

    constructor(private dbWidgets: DashboardWidgets, private widgetManager: InsightWidgetsService, private dimService: SizeDimensionService) {}

    ngOnInit() {
        let renderResolve: Function;
        this.deferRender = new Promise((resolve) => (renderResolve = resolve));

        this.widget = this.dbWidgets.getWidget({ id });
        if (!this.widget) throw new Error('Error: Could not find Company Size widget.');

        this.widgetManager.getNdx().then((ndx) => {
            this.ndx = ndx;

            // Don't subscribe after the component is unloaded (possible if the user leaves the tab before any requests complete)
            if (!this.onDestroy$.isStopped) {
                this.widgetManager
                    .loadProgress()
                    .pipe(takeUntil(this.onDestroy$))
                    .subscribe((status) => {
                        if (status === LoadProgress.Started) {
                            this.spinner.startLoading();
                        } else if (status === LoadProgress.LevelLoaded) {
                            this.spinner.stopLoading();
                            this.size = this.dimService.getGroup().size();
                            this.sizeChart.redraw();
                        }
                    });

                this.configureCharts();
                renderResolve();

                this.widget.update = () => {
                    this.computeChartSize();
                    this.sizeChart.redraw();
                };
            }
        });
    }

    ngAfterViewInit() {
        this.spinner.startLoading();

        this.sizeChart = pieChart(this.sizeGraph.nativeElement);

        this.deferRender.then(() => {
            this.sizeChart.render();
            this.spinner.stopLoading();
        });

        this.dimService
            .filtersChange()
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((filters) => {
                if (filters.length > 0) (this.sizeChart as any).replaceFilter([filters]);
                else this.sizeChart.filterAll();

                if (this.ndx) redrawAll();
            });
    }

    ngOnDestroy(): void {
        this.onDestroy$.next();
        this.onDestroy$.complete();
        this.widget.update = noop;
    }

    hideWidget() {
        this.dbWidgets.hideWidget(this.widget);
    }

    private configureCharts() {
        const sizeDimension = this.dimService.getDimension();
        const sizeGroup = this.dimService.getGroup();

        this.size = sizeGroup.size();

        this.computeChartSize();

        this.sizeChart
            .on('filtered.showReset', (chart) => {
                this.showReset = chart.hasFilter();
            })
            .ordinalColors(defaultColors)
            .emptyTitle('No companies match filters')
            .renderLabel(false)
            .filterHandler(this.dimService.getFilterHandler())
            .dimension(sizeDimension)
            .group(sizeGroup);

        this.sizeChart.legend(
            legend()
                .gap(WidgetConst.LEGEND_ITEM_GAP)
                .maxItems(this.MAX_LEGEND_ITEMS)
                .legendText((d) => `${d.name} (${d.data})`)
        );
        this.sizeChart.legend().itemWidth(100).legendWidth(200).horizontal(true);

        this.sizeChart.onClick = (d) => {
            const key = this.sizeChart.keyAccessor()(d);
            if (key === 'Others' && d.others) this.dimService.toggleFilters(d.others);
            else this.dimService.toggleFilters(key);
        };
    }

    private computeChartSize() {
        const e = this.widgetContainer.nativeElement as HTMLElement;
        const height = e.getBoundingClientRect().height;
        const width = e.getBoundingClientRect().width;

        const rad = Math.min(height - 30 - WidgetConst.RADIUS_OFFSET, width) / 2;

        this.sizeChart
            .height(height - 15)
            .cy(height / 2 + WidgetConst.POSITION_OFFSET)
            .width(width - 15)
            .radius(rad)
            .innerRadius(rad / 2);
    }

    resetFilters() {
        this.dimService.setFilters([]);
        this.showReset = false;
    }
}
