import { InjectionToken } from '@angular/core';

import { IWidgetParams } from '../../../models/widget.interface';
import { loadForInsight } from '../../../services/load-for-utils';

export const id = 'industry-breakdown';

export const INDUSTRY_BREAKDOWN_WIDGET = new InjectionToken(id);

export const industryBreakdownWidget: IWidgetParams = {
    id,
    version: 1,
    title: 'DIB Participants by Industry',
    gridsterClasses: [],
    sizeX: 3,
    sizeY: 4,
    isAngularComponent: true,
    component: INDUSTRY_BREAKDOWN_WIDGET,
    templateUrl: '',
    update: () => null,
    shouldLoad: loadForInsight,
    description:
        'Based on each company’s provided primary NAICS code, this reflects the top level NAICS categories under which participating companies fall.',
};
