import { InjectionToken } from '@angular/core';

import { IWidgetParams } from '../../../models/widget.interface';
import { loadForInsight } from '../../../services/load-for-utils';

export const id = 'state-breakdown';

export const STATE_BREAKDOWN_WIDGET = new InjectionToken(id);

export const stateBreakdownWidget: IWidgetParams = {
    id,
    version: 1,
    title: 'Program Participants by State',
    gridsterClasses: [],
    sizeX: 2,
    sizeY: 4,
    isAngularComponent: true,
    component: STATE_BREAKDOWN_WIDGET,
    templateUrl: '',
    update: () => null,
    shouldLoad: loadForInsight,
    description: 'State in the United States of each participating company’s primary physical location.',
};
