import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Crossfilter } from 'crossfilter2';
import { format } from 'd3';
import { redrawAll, RowChart, rowChart } from 'dc';
import { noop, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { LoadingContentComponent } from '../../../../common/components';
import { rowChartColors } from '../../../../common/data';
import { IWidget } from '../../../models/widget.interface';
import { DashboardWidgets } from '../../../services/dashboard-widgets.service';
import { BarChartConfig } from '../data/bar-chart-config';
import { InsightWidgetsService, LoadProgress } from '../insight-widgets.service';
import { Insight } from '../models/insight/insight.namespace';
import { StateDimensionService } from '../services/state-dimension.service';
import { id } from './state-breakdown.widget';

@Component({
    selector: id,
    templateUrl: '../common/insight-widget.html',
    styleUrls: ['../common/insight-widget.less'],
})
export class StateBreakdownComponent implements OnInit, AfterViewInit, OnDestroy {
    private widget: IWidget | undefined;
    private ndx: Crossfilter<Insight.ICustomerCrossfilterRecord>;
    private stateChart: RowChart;
    private onDestroy$ = new Subject();

    //must start at -1 because otherwise the graph ElementRef is undefined
    size = -1;
    showReset = false;

    @ViewChild('spinner') spinner: LoadingContentComponent;
    @ViewChild('widgetGraph') state: ElementRef;

    constructor(private dbWidgets: DashboardWidgets, private widgetManager: InsightWidgetsService, private dimService: StateDimensionService) {}

    ngOnInit() {
        this.widget = this.dbWidgets.getWidget({ id });
        if (!this.widget) throw new Error('Error: Could not find State Breakdown widget.');
    }

    ngAfterViewInit() {
        this.spinner.startLoading();

        this.stateChart = rowChart(this.state.nativeElement);

        this.widgetManager.getNdx().then((ndx) => {
            this.ndx = ndx;

            // Don't subscribe after the component is unloaded (possible if the user leaves the tab before any requests complete)
            if (!this.onDestroy$.isStopped) {
                this.widgetManager
                    .loadProgress()
                    .pipe(takeUntil(this.onDestroy$))
                    .subscribe((status) => {
                        if (status === LoadProgress.Started) {
                            this.spinner.startLoading();
                        } else if (status === LoadProgress.LevelLoaded) {
                            this.spinner.stopLoading();
                            this.size = this.dimService.getGroup().size();
                            setTimeout(() => this.stateChart.render());
                        }
                    });

                this.configureCharts();
                this.stateChart.render();

                this.widget.update = () => this.stateChart.render();

                this.spinner.stopLoading();
            }
        });

        this.dimService
            .filtersChange()
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((filters) => {
                if (filters.length > 0) (this.stateChart as any).replaceFilter([filters]);
                else this.stateChart.filterAll();

                if (this.ndx) redrawAll();
            });
    }

    ngOnDestroy(): void {
        this.onDestroy$.next();
        this.onDestroy$.complete();
        this.widget.update = noop;
    }

    hideWidget() {
        this.dbWidgets.hideWidget(this.widget);
    }

    private configureCharts() {
        const stateDimension = this.dimService.getDimension();
        const stateGroup = this.dimService.getGroup();

        this.size = stateGroup.size();

        this.stateChart
            .on('filtered.showReset', (chart) => {
                this.showReset = chart.hasFilter();
            })
            .ordinalColors(rowChartColors)
            .cap(10)
            .elasticX(true)
            .filterHandler(this.dimService.getFilterHandler())
            .dimension(stateDimension)
            .gap(BarChartConfig.GAP)
            .labelOffsetY(BarChartConfig.LABEL_OFFSET_Y)
            .labelOffsetX(BarChartConfig.LABEL_OFFSET_X)
            .group(stateGroup);

        this.stateChart.onClick = (d) => {
            const key = this.stateChart.keyAccessor()(d);
            if (key === 'Others' && d.others) {
                if (this.stateChart.filters().includes('Others')) this.dimService.removeFilters([...d.others, 'Others']);
                else this.dimService.addFilters([...d.others, 'Others']);
            } else {
                this.dimService.toggleFilters(key);
            }
        };

        this.stateChart.xAxis().tickFormat(format(',d'));
    }

    resetFilters() {
        this.dimService.setFilters([]);
    }
}
