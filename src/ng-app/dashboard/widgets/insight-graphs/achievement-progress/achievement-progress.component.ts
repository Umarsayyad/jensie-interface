import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Crossfilter, Dimension, Group } from 'crossfilter2';
import * as d3 from 'd3';
import { BarChart, barChart, CompositeChart, compositeChart, legend, redrawAll, units } from 'dc';
import * as dc from 'dc';
import { noop, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { getEnumValues } from '../../../../common';
import { LoadingContentComponent } from '../../../../common/components';
import { levelColorsObject } from '../../../../global-data/cmmc-level-colors';
import { IWidget } from '../../../models/widget.interface';
import { DashboardWidgets } from '../../../services/dashboard-widgets.service';
import { InsightWidgetsService, LoadProgress } from '../insight-widgets.service';
import { Insight } from '../models/insight/insight.namespace';
import { Level } from '../models/level/level.namespace';
import { id } from './achievement-progress.widget';

@Component({
    selector: id,
    templateUrl: '../common/insight-widget.html',
    styleUrls: ['../common/insight-widget.less'],
})
export class AchievementProgressComponent implements OnInit, AfterViewInit, OnDestroy {
    private progressL1Chart: BarChart;
    private progressL2Chart: BarChart;
    private progressL3Chart: BarChart;
    private progressL4Chart: BarChart;
    private progressL5Chart: BarChart;

    private widget: IWidget | undefined;
    private ndx: Crossfilter<Insight.ICustomerCrossfilterRecord>;
    private pCharts: Record<Level.Levels, BarChart>; //are these keyed by Level? (if so, we should use the enum instead)
    private progressDims: Record<Level.Levels, Dimension<Insight.ICustomerCrossfilterRecord, number> | null> = {} as any;
    private progressGroups: Record<Level.Levels, Group<Insight.ICustomerCrossfilterRecord, number, number>> = {} as any;
    private progressCompositeChart: CompositeChart;
    private onDestroy$ = new Subject();

    //must start at -1 because otherwise the graph ElementRef is undefined
    size = -1;

    @ViewChild('spinner') spinner: LoadingContentComponent;
    @ViewChild('widgetGraph') progress: ElementRef;
    public showReset: boolean;

    constructor(private dbWidgets: DashboardWidgets, private widgetManager: InsightWidgetsService) {}

    ngOnInit(): void {
        this.widget = this.dbWidgets.getWidget({ id });
        if (!this.widget) throw new Error('Error: Could not find Achievement widget.');
    }

    ngAfterViewInit() {
        this.spinner.startLoading();

        this.progressCompositeChart = compositeChart(this.progress.nativeElement);
        this.progressL1Chart = barChart(this.progressCompositeChart);
        this.progressL2Chart = barChart(this.progressCompositeChart);
        this.progressL3Chart = barChart(this.progressCompositeChart);
        this.progressL4Chart = barChart(this.progressCompositeChart);
        this.progressL5Chart = barChart(this.progressCompositeChart);

        this.pCharts = {
            //all: this.progressCompositeChart,
            1: this.progressL1Chart,
            2: this.progressL2Chart,
            3: this.progressL3Chart,
            4: this.progressL4Chart,
            5: this.progressL5Chart,
        };

        this.widgetManager.getNdx().then((ndx) => {
            this.ndx = ndx;

            // Don't subscribe after the component is unloaded (possible if the user leaves the tab before any requests complete)
            if (!this.onDestroy$.isStopped) {
                this.widgetManager
                    .loadProgress()
                    .pipe(takeUntil(this.onDestroy$))
                    .subscribe((status) => {
                        if (status === LoadProgress.Started) {
                            this.spinner.startLoading();
                        } else if (status === LoadProgress.LevelLoaded) {
                            this.spinner.stopLoading();
                            this.size = this.progressGroups[Level.Levels.Level1].size();
                            setTimeout(() => this.widget.update());
                        }
                    });

                this.configureCharts();

                this.progressCompositeChart.render();

                this.widget.update = () => {
                    // @ts-ignore: `rescale` isn't in the .d.ts for dc
                    this.progressCompositeChart.rescale();
                    this.progressCompositeChart.render();
                    const width = this.progressCompositeChart.width();
                    const height = this.progressCompositeChart.height();
                    this.progressCompositeChart.legend().legendWidth(width);
                    for (let level of getEnumValues<Level.Levels>(Level.Levels)) {
                        this.pCharts[level].width(width);
                        this.pCharts[level].height(height);
                        // @ts-ignore: `rescale` isn't in the .d.ts for dc
                        this.pCharts[level].rescale();
                    }

                    this.progressCompositeChart.redraw();
                };

                this.spinner.stopLoading();
            }
        });
    }

    ngOnDestroy(): void {
        this.onDestroy$.next();
        this.onDestroy$.complete();
        this.widget.update = noop;
        for (let level of getEnumValues<Level.Levels>(Level.Levels)) {
            if (this.progressDims[level]) {
                this.progressDims[level].dispose();
                this.progressGroups[level].dispose();
            }
        }
    }

    hideWidget() {
        this.dbWidgets.hideWidget(this.widget);
    }

    private configureCharts() {
        const bins = [
            [0, 19],
            [20, 39],
            [40, 59],
            [60, 79],
            [80, 99],
            [100, 100],
        ];

        this.progressCompositeChart.margins().top = 50;
        this.progressCompositeChart
            .x(d3.scale.linear().domain([-10, 110]))
            //.xUnits(dc.units.ordinal)
            .xUnits(units.fp.precision(3))
            // .y(d3.scale.linear().domain([0, this.widgetManager.getExperiments().length]))    //TODO
            //.xUnits(dc.units.fp.precision(20))
            .brushOn(false)
            .xAxisLabel('Progress %')
            .yAxisLabel('Company Count')
            .renderHorizontalGridLines(true)
            .compose([this.progressL1Chart, this.progressL2Chart, this.progressL3Chart, this.progressL4Chart, this.progressL5Chart])
            .on('renderlet.barclick', (chart) => {
                // console.log('renderlet', chart)
                chart.selectAll('rect.bar').on('click.barclick', (d) => {
                    const level = (d.x - d.data.key) / 3 + 3;
                    //console.log("click!", (d.x-d.data.key)/3 + 3, d, JSON.stringify(d));
                    //this.pCharts[level].onClick(d);
                    this.pCharts[level].filter(d.data.key);
                    this.pCharts[level].redrawGroup();
                    this.checkShowReset();
                });
            })
            .legend(
                legend()
                    .legendText((d, idx) => `Level ${idx + 1}`)
                    //.autoItemWidth(true)
                    .horizontal(true)
            );
        this.progressCompositeChart.elasticY(true);
        this.progressCompositeChart.xAxis().ticks(bins.length).innerTickSize(0).outerTickSize(0);
        this.progressCompositeChart.yAxis().tickFormat(d3.format(',d'));

        for (let level of getEnumValues<Level.Levels>(Level.Levels)) {
            this.progressDims[level] = this.ndx.dimension((d) => {
                for (let bin of bins) {
                    if (d[`level${level}progress`] <= bin[1]) {
                        return bin[0];
                    }
                }
            });

            this.progressGroups[level] = this.progressDims[level].group();
            this.pCharts[level]
                // .height(350)
                //.x(d3.scaleBand().domain([0,20,40,60,80,100]))
                // .x(d3.scale.linear().domain([0, 100]))
                // //.xUnits(dc.units.ordinal)
                // .xUnits(units.fp.precision(20))
                // // .y(d3.scale.linear().domain([0, this.widgetManager.getExperiments().length]))    //TODO
                // //.xUnits(dc.units.fp.precision(20))
                // .minWidth(120)
                // .brushOn(false)
                .centerBar(true)
                .gap(0)
                .keyAccessor((a) => {
                    return a.key + 3 * (level - 3);
                })
                .colorCalculator(() => levelColorsObject[level])
                // .xAxisLabel("Progress %")
                // .renderHorizontalGridLines(true)
                .dimension(this.progressDims[level])
                .group(this.progressGroups[level]);
            // this.pCharts[i].xAxis().ticks(bins.length);

            // dc.js sees the charts as having a linear scale, but we are treating it as ordinal so we need to override
            // how it determines which bars to shade as deselected
            this.pCharts[level]['fadeDeselectedArea'] = () => {
                const bars = this.pCharts[level].chartBodyG().selectAll('rect.bar');

                if (this.pCharts[level].hasFilter()) {
                    bars.classed(dc['constants'].SELECTED_CLASS, (d) => {
                        return this.pCharts[level].hasFilter(d.data.key);
                    });
                    bars.classed(dc['constants'].DESELECTED_CLASS, (d) => {
                        return !this.pCharts[level].hasFilter(d.data.key);
                    });
                } else {
                    bars.classed(dc['constants'].SELECTED_CLASS, false);
                    bars.classed(dc['constants'].DESELECTED_CLASS, false);
                }
            };
        }
        this.size = this.progressGroups[1].size();
    }

    resetFilters() {
        //this.progressCompositeChart.filterAll();
        Object.values(this.pCharts).forEach((c) => c.filterAll());
        redrawAll();
        this.showReset = false;
    }

    checkShowReset() {
        this.showReset = Object.values(this.pCharts).filter((c) => c.hasFilter()).length > 0;
    }
}
