import { InjectionToken } from '@angular/core';

import { IWidgetParams } from '../../../models/widget.interface';
import { loadForInsight } from '../../../services/load-for-utils';

export const id = 'achievement-progress';

export const ACHIEVEMENT_GRAPH_WIDGET = new InjectionToken(id);

export const achievementProgressWidget: IWidgetParams = {
    id,
    version: 1,
    title: 'CMMC Readiness Progress',
    gridsterClasses: [],
    sizeX: 4,
    sizeY: 3,
    isAngularComponent: true,
    component: ACHIEVEMENT_GRAPH_WIDGET,
    templateUrl: '',
    update: () => null,
    shouldLoad: loadForInsight,
    description: 'This graph is showing how many companies reached levels 1-5, as well as show partial credit.',
};
