export const BarChartConfig: Readonly<Record<string, number>> = {
    LABEL_OFFSET_Y: -8,
    LABEL_OFFSET_X: 0,
    GAP: 22,
};
