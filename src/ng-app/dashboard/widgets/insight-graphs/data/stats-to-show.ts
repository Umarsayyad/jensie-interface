export enum StatsToShow {
    PROGRESS = 'Average progress towards compliance',
    COMPLIANT = 'Percent of companies compliant',
}
