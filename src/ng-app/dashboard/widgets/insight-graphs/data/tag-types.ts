export enum TagTypes {
    Contract = 'contract',
    Participation = 'participation',
    Category = 'category',
}
