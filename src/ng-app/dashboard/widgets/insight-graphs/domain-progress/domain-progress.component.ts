import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ColorHelper, HeatMapComponent } from '@swimlane/ngx-charts';
import { Crossfilter, EventType, GroupAll } from 'crossfilter2';
import { AsyncSubject, interval, noop, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { LoadingContentComponent } from '../../../../common/components';
import { heatmapColors } from '../../../../common/data';
import { PROGRESS_COMPLETE_VALUE } from '../../../../common/data/cmmc';
import { ID } from '../../../../common/models';
import { getEnumValues } from '../../../../common/utilities';
import { IWidget } from '../../../models/widget.interface';
import { DashboardWidgets } from '../../../services/dashboard-widgets.service';
import { StatsToShow } from '../data/stats-to-show';
import { InsightWidgetsService, LoadProgress } from '../insight-widgets.service';
import { DomainProgressChart } from '../models/insight/domain-progress-chart.namespace';
import { Insight } from '../models/insight/insight.namespace';
import { Level } from '../models/level/level.namespace';
import { id } from './domain-progress.widget';

const MIN_WIDTH_FOR_FULL_NAMES = 450;

@Component({
    selector: id,
    templateUrl: 'domain-progress.component.html',
    styleUrls: ['../common/insight-widget.less', 'domain-progress.component.less'],
})
export class DomainProgressComponent implements OnInit, AfterViewInit, OnDestroy {
    widget: IWidget | undefined;
    private ndx: Crossfilter<Insight.ICustomerCrossfilterRecord>;
    private crossfilterOnChangeUnsub?: () => void;
    private onDestroy$ = new Subject();
    private group: GroupAll<Insight.ICustomerCrossfilterRecord, DomainProgressChart.IGroupedValue>;
    private populateLoadTimeout: number;
    showReset: boolean;
    chartData: DomainProgressChart.ILevelData[];
    statToShow: StatsToShow = StatsToShow.COMPLIANT;
    availableStats = Object.values(StatsToShow);

    size = -1;

    @ViewChild('spinner') spinner: LoadingContentComponent;
    @ViewChild('heatMapComponent') heatMapComponent: HeatMapComponent;

    scheme = { domain: heatmapColors };

    constructor(private dbWidgets: DashboardWidgets, private widgetManager: InsightWidgetsService) {}

    ngOnInit(): void {
        this.widget = this.dbWidgets.getWidget({ id });
        if (!this.widget) throw new Error('Error: Could not find Domain Progress widget.');
    }

    ngAfterViewInit(): void {
        this.spinner.startLoading();

        this.heatMapComponent.getValueDomain = () => [0, 100];
        this.heatMapComponent.setColors = () => {
            this.heatMapComponent.colors = new ColorHelper(this.scheme, 'quantile', [-10, 110]);
        };

        this.widgetManager.getNdx().then((ndx) => {
            this.ndx = ndx;

            // Don't subscribe after the component is unloaded (possible if the user leaves the tab before any requests complete)
            if (!this.onDestroy$.isStopped) {
                const items = this.ndx.all();
                if (items.length > 0 && Object.keys(items[0].domainProgress).length > 0) {
                    // configuring chart for already-loaded data
                    this.configureCharts();
                    this.spinner.stopLoading();
                } else if (items.length === 0) {
                    // data already loaded, but empty
                    this.size = 0;
                    this.spinner.stopLoading();
                }
                this.widget.update = () => {
                    if (this.group) {
                        // data has been loaded and the chart configured
                        this.size = this.group.value().count;
                        this.checkPopulateGraphData();
                        this.heatMapComponent.update();
                    }
                };
                this.widgetManager
                    .loadProgress()
                    .pipe(takeUntil(this.onDestroy$))
                    .subscribe((status) => {
                        if (status === LoadProgress.Started) {
                            this.spinner.startLoading();
                        } else if (status === LoadProgress.DomainLoaded) {
                            this.configureCharts();
                            this.spinner.stopLoading();
                        }
                    });
            }
        });
    }

    ngOnDestroy(): void {
        if (this.crossfilterOnChangeUnsub) {
            this.crossfilterOnChangeUnsub();
        }
        this.onDestroy$.next();
        this.onDestroy$.complete();
        this.widget.update = noop;
        clearTimeout(this.populateLoadTimeout);
    }

    hideWidget() {
        this.dbWidgets.hideWidget(this.widget);
    }

    private configureCharts() {
        if (this.group) {
            this.group.dispose();
        }
        this.group = this.ndx.groupAll();
        this.group.reduce(reduceAdd, reduceRemove, reduceInit);

        this.size = this.group.value().count;

        if (!this.crossfilterOnChangeUnsub) {
            this.crossfilterOnChangeUnsub = this.ndx.onChange((_: EventType) => {
                this.size = this.group.value().count;
                if (this.size > 0) {
                    this.checkPopulateGraphData();
                }
            });
        }

        this.checkPopulateGraphData();
    }

    getTooltipHtml(c): string {
        // TODO: this function is called way too many times, but for now it will do
        return `
            <span class="tooltip-label">${c.series} - ${c.cell.fullName} (${c.cell.identifier})</span>
            <span class="tooltip-val">
                ${
                    c.cell.exists
                        ? c.cell.attemptCount > 0
                            ? `
                        Average percent complete: ${c.cell.meanProgress}%
                        <br>
                        Percent of companies compliant: ${c.cell.percentCompliant}%
                        <br>
                        Number of companies attempting level: ${c.cell.attemptCount}
                    `
                            : `No companies have attempted to achieve level ${c.cell.level}`
                        : `There are no level ${c.cell.level} practices for ${c.cell.fullName}`
                }
            </span>
        `;
    }

    private checkPopulateGraphData() {
        if (this.size !== 0) {
            let deferPopulate = new AsyncSubject();

            deferPopulate.subscribe(() => {
                this.doPopulateGraphData();
            });

            const checkReady = (): boolean => {
                if (this.heatMapComponent.getContainerDims() !== null) {
                    deferPopulate.next(0); // 0 doesn't mean anything, .next() just requires an argument
                    deferPopulate.complete();
                    return true;
                } else {
                    return false;
                }
            };
            // Call checkReady before starting the interval in case we can load the data immediately (such as when switching stat types)
            if (!checkReady()) {
                interval(100).pipe(takeUntil(this.onDestroy$), takeUntil(deferPopulate)).subscribe(checkReady);
            }
        }
    }

    private doPopulateGraphData() {
        this.chartData = [];
        const wideEnough = this.heatMapComponent.getContainerDims().width > MIN_WIDTH_FOR_FULL_NAMES;
        const currentData = this.group.value();
        const seriesRef = new Map<Level.Levels, DomainProgressChart.ISectionData[]>();

        for (let level of getEnumValues<Level.Levels>(Level.Levels)) {
            seriesRef.set(level, []);
            this.chartData.push({ name: `Level ${level}`, series: seriesRef.get(level) });
        }

        // ngx-charts heatmap puts the first item at the bottom so we need to iterate in reverse order to get AC at the top
        for (const [sectionId, sectionSums] of Array.from(currentData.sums).reverse()) {
            const sectionInfo = this.widgetManager.getSection(sectionId);
            for (const [level, levelStats] of sectionSums) {
                let meanProgress = -1;
                let percentCompliant = -1;
                // Leaving this commented out logic here in case we want to allow toggling it on in the future
                // if (levelStats.count) {
                //     // Calculate progress and compliance based only on companies that have attempted at least the given level
                //     meanProgress = Math.floor(levelStats.progress / levelStats.count);
                //     percentCompliant = Math.floor(100 * levelStats.compliant / levelStats.count);
                // }
                if (levelStats.exists && levelStats.count) {
                    meanProgress = Math.floor(levelStats.progress / currentData.count);
                    percentCompliant = Math.floor((100 * levelStats.compliant) / currentData.count);
                }
                seriesRef.get(level).push({
                    name: wideEnough ? sectionInfo.name : sectionInfo.identifier,
                    value: this.statToShow === StatsToShow.PROGRESS ? meanProgress : percentCompliant,
                    meanProgress,
                    percentCompliant,
                    exists: levelStats.exists,
                    attemptCount: levelStats.count,
                    section: sectionId,
                    level: level,
                    fullName: sectionInfo.name,
                    identifier: sectionInfo.identifier,
                });
            }
        }
    }

    setStatType(stat: StatsToShow) {
        this.statToShow = stat;
        this.checkPopulateGraphData();
    }
}

function reduceAdd(p: DomainProgressChart.IGroupedValue, v: Insight.ICustomerCrossfilterRecord): DomainProgressChart.IGroupedValue {
    p.count++;
    for (const sectionData of Object.values(v.domainProgress)) {
        if (!p.sums.has(sectionData.section)) {
            p.sums.set(sectionData.section, new Map<number, DomainProgressChart.IStats>());
        }

        const sectionSums = p.sums.get(sectionData.section);

        for (let level of getEnumValues<Level.Levels>(Level.Levels)) {
            if (!sectionSums.has(level)) {
                sectionSums.set(level, { progress: 0, compliant: 0, count: 0, exists: false });
            }
            const current = sectionSums.get(level);
            const levelData: Level.ILevel = sectionData[level];
            current.progress += levelData.practices.progress;
            if (levelData.practices.progress === 100) {
                current.compliant++;
            }
            if (levelData.attempted) {
                current.count++;
            }
            // If there are no questions (and thus practices) in the section at this or any lower level, total will be 0
            if (levelData.practices.availablePoints > 0) current.exists = true;
        }
    }
    return p;
}

function reduceRemove(p: DomainProgressChart.IGroupedValue, v: Insight.ICustomerCrossfilterRecord, nf: boolean): DomainProgressChart.IGroupedValue {
    if (p.count === 0) {
        // This should never actually happen, but if it does, detect it, log some helpful debug info and return to stop
        // further processing to hopefully prevent additional errors that would make the dashboard look completely broken
        console.error(
            'removing when no data was loaded; there is probably a bug somewhere',
            p,
            v,
            nf,
            this,
            JSON.stringify(p, (_, v) => (v instanceof Map ? Array.from(v) : v), 1)
        );
        return p;
    }
    p.count--;
    for (const sectionData of Object.values(v.domainProgress)) {
        const sectionSums = p.sums.get(sectionData.section);

        for (let level of getEnumValues<Level.Levels>(Level.Levels)) {
            const current = sectionSums.get(level);
            const levelData: Level.ILevel = sectionData[level];
            current.progress -= levelData.practices.progress;
            if (levelData.practices.progress === PROGRESS_COMPLETE_VALUE) {
                current.compliant--;
            }
            if (levelData.attempted) {
                current.count--;
            }
        }
    }
    return p;
}

function reduceInit(): DomainProgressChart.IGroupedValue {
    return {
        count: 0,
        sums: new Map<ID /*section*/, Map<Level.Levels, DomainProgressChart.IStats>>(),
    };
}
