import { InjectionToken } from '@angular/core';

import { IWidgetParams } from '../../../models/widget.interface';
import { loadForInsight } from '../../../services/load-for-utils';

export const id = 'domain-progress';

export const DOMAIN_GRAPH_WIDGET = new InjectionToken(id);

export const domainProgressWidget: IWidgetParams = {
    id,
    version: 1,
    title: 'CMMC Domain Progress',
    gridsterClasses: [],
    sizeX: 4,
    sizeY: 3,
    isAngularComponent: true,
    component: DOMAIN_GRAPH_WIDGET,
    templateUrl: '',
    update: () => null,
    shouldLoad: loadForInsight,
    description:
        'This heat map illustrates the overall progress of the displayed participant companies towards compliance in each domain at each respective CMMC level. Black bars indicate domains that are absent at particular levels. Progress colors range from red (0% compliance) to green (100% compliance), as indicated by the scale on the right. Mouse over each bar to see additional details.',
};
