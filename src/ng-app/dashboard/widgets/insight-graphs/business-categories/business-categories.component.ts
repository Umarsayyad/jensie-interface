import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Crossfilter } from 'crossfilter2';
import { format } from 'd3';
import { redrawAll, RowChart, rowChart } from 'dc';
import { noop, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { LoadingContentComponent } from '../../../../common/components';
import { rowChartColors } from '../../../../common/data/colors';
import { IWidget } from '../../../models/widget.interface';
import { DashboardWidgets } from '../../../services/dashboard-widgets.service';
import { BarChartConfig } from '../data/bar-chart-config';
import { InsightWidgetsService, LoadProgress } from '../insight-widgets.service';
import { Insight } from '../models/insight/insight.namespace';
import { CategoryDimensionService } from '../services/category-dimension.service';
import { id } from './business-categories.widget';

@Component({
    selector: id,
    templateUrl: '../common/insight-widget.html',
    styleUrls: ['../common/insight-widget.less'], //"./business-categories.component.less"],
})
export class BusinessCategoriesComponent implements OnInit, AfterViewInit, OnDestroy {
    private widget: IWidget | undefined;
    private ndx: Crossfilter<Insight.ICustomerCrossfilterRecord>;
    private categoriesChart: RowChart;
    private onDestroy$ = new Subject();

    //must start at -1 because otherwise the graph ElementRef is undefined
    size = -1;
    showReset = false;

    @ViewChild('spinner') spinner: LoadingContentComponent;
    @ViewChild('widgetGraph') categories: ElementRef;

    constructor(private dbWidgets: DashboardWidgets, private widgetManager: InsightWidgetsService, private dimService: CategoryDimensionService) {}

    ngOnInit() {
        this.widget = this.dbWidgets.getWidget({ id });
        if (!this.widget) throw new Error('Error: Could not find Business Categories widget.');
    }

    ngAfterViewInit() {
        this.spinner.startLoading();

        this.categoriesChart = rowChart(this.categories.nativeElement);

        this.widgetManager.getNdx().then((ndx) => {
            this.ndx = ndx;

            // Don't subscribe after the component is unloaded (possible if the user leaves the tab before any requests complete)
            if (!this.onDestroy$.isStopped) {
                this.widgetManager
                    .loadProgress()
                    .pipe(takeUntil(this.onDestroy$))
                    .subscribe((status) => {
                        if (status === LoadProgress.Started) {
                            this.spinner.startLoading();
                        } else if (status === LoadProgress.LevelLoaded) {
                            this.spinner.stopLoading();
                            this.size = this.dimService.getGroup().size();
                            setTimeout(() => this.categoriesChart.render());
                        }
                    });

                this.configureCharts();
                this.categoriesChart.render();

                this.widget.update = () => this.categoriesChart.render();

                this.spinner.stopLoading();
            }
        });

        this.dimService
            .filtersChange()
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((filters) => {
                if (filters.length > 0) (this.categoriesChart as any).replaceFilter([filters]);
                else this.categoriesChart.filterAll();

                if (this.ndx) redrawAll();
            });
    }

    ngOnDestroy(): void {
        this.onDestroy$.next();
        this.onDestroy$.complete();
        this.widget.update = noop;
    }

    hideWidget() {
        this.dbWidgets.hideWidget(this.widget);
    }

    private configureCharts() {
        const categoriesDimension = this.dimService.getDimension();
        const categoriesGroup = this.dimService.getGroup();

        this.size = categoriesGroup.size();

        this.categoriesChart
            .on('filtered.showReset', (chart) => {
                this.showReset = chart.hasFilter();
            })
            .ordinalColors(rowChartColors)
            .elasticX(true)
            .filterHandler(this.dimService.getFilterHandler())
            .dimension(categoriesDimension)
            .gap(BarChartConfig.GAP)
            .labelOffsetY(BarChartConfig.LABEL_OFFSET_Y)
            .labelOffsetX(BarChartConfig.LABEL_OFFSET_X)
            .group(categoriesGroup);

        this.categoriesChart.onClick = (d) => {
            const key = this.categoriesChart.keyAccessor()(d);
            if (key === 'Others' && d.others) this.dimService.toggleFilters(d.others);
            else this.dimService.toggleFilters(key);
        };

        this.categoriesChart.xAxis().tickFormat(format(',d'));
    }

    resetFilters() {
        this.dimService.setFilters([]);
        this.showReset = false;
    }
}
