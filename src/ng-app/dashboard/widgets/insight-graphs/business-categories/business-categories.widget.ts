import { InjectionToken } from '@angular/core';

import { IWidgetParams } from '../../../models/widget.interface';
import { loadForInsight } from '../../../services/load-for-utils';

export const id = 'business-categories';

export const BUSINESS_CATEGORIES_WIDGET = new InjectionToken(id);

export const businessCategoriesWidget: IWidgetParams = {
    id,
    version: 1,
    title: 'Socio-Economic Categories',
    gridsterClasses: [],
    sizeX: 3,
    sizeY: 2,
    isAngularComponent: true,
    component: BUSINESS_CATEGORIES_WIDGET,
    templateUrl: '',
    update: () => null,
    shouldLoad: loadForInsight,
    description:
        'Categories: Number of participating companies who have been certified to fall under one or more of the Small Business Administration’s contracting assistance programs for certain socio-economic categories.',
};
