import { ID } from '../../../../../common';
import { Level } from '../level/level.namespace';

export namespace Domain {
    export interface IResponse {
        progressStats: IProgress[];
        sectionDetail: ISection[];
    }

    export interface IProgress extends Level.IProgress {
        section: ID;
    }

    export interface ISection {
        // This is like ISection used by the questionnaire, but progressDetail will always be null so it isn't included here
        id: ID;
        name: string;
        identifier: string;
        type: string; //FIXME: use enum
    }
}
