import { ID } from '../../../../../common/models';

export namespace Level {
    export enum Levels {
        Level1 = 1,
        Level2 = 2,
        Level3 = 3,
        Level4 = 4,
        Level5 = 5,
    }

    export type IResponse = IProgress[];

    export interface IProgress extends Record<Levels, ILevel> {
        customer: ID;
    }

    export interface ILevel {
        practices: IPractice;
        questions: IQuestion;
        attempted: boolean;
    }

    export interface IPractice {
        availablePoints: number;
        proofPoints: number;
        questionPoints: number;
        progress: number;
        earnedPoints: number;
    }

    export interface IQuestion {
        yes: number;
        no: number;
        notApplicable: number;
        incomplete: number;
        progress: number;
        passing: number;
        total: number;
    }
}
