import { ID } from '../../../../../common/models';
import { Domain } from '../domain/domain.namespace';
import { Level } from '../level/level.namespace';
import { Insight } from './insight.namespace';

export namespace ProgressHistory {
    export interface IEntry {
        id: ID;
        timestamp: string;
        questionnaire: ID;
    }

    export interface IRawDataEntry extends IAbstractDataEntry {
        customerData: Insight.IResponseCustomer[];
    }
    export interface IDataEntry extends IAbstractDataEntry {
        customerData: Insight.ICustomer[];
    }
    interface IAbstractDataEntry extends IEntry {
        domainProgressData: Domain.IResponse;
        levelProgressData: Level.IResponse;
    }
}
