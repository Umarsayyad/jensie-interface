import { ID } from '../../../../../common';
import { Domain } from '../domain/domain.namespace';

export namespace Insight {
    export interface ICustomerCrossfilterRecord {
        id: ID;
        name: string;
        size: string;
        nickname: string;
        naics: string;
        businessCategoryTags: string[];
        participationTags: string[];
        contractTags: string[];
        currentLevel: string; //format is `Level ${value}`
        level1progress: number;
        level2progress: number;
        level3progress: number;
        level4progress: number;
        level5progress: number;
        domainProgress: Record<ID, Domain.IProgress>;
        industry: string;
        manufacturerType: string;
        state: string;
    }

    // This is a subset of the fields of ICustomer in global-data/models/customer.interface.ts
    export interface ICustomer {
        id: ID;
        // name: string; // "name" exists on the data, but prefer to use "nickname"
        nickname: string;
        naics: string;
        size: string;

        primary_location_state: string;
        primary_location_zip: number;
        primary_location_zip_latitude: number;
        primary_location_zip_longitude: number;

        has_civilian_prime_contract: boolean;
        has_civilian_sub_contract: boolean;
        has_dod_prime_contract: boolean;
        has_dod_sub_contract: boolean;
        has_participated_mentor_program: boolean;
        has_participated_nist_mep: boolean;
        has_current_participation_mentor_program: boolean;

        has_8a_small_business: boolean;
        has_hubzone_business: boolean;
        has_indian_economic_enterprise: boolean;
        has_service_disabled_veteran_business: boolean;
        has_veteran_owned_business: boolean;
        has_women_owned_business: boolean;
    }

    export interface IResponseCustomer {
        id: ID;
        // name: string; // "name" exists on the data, but prefer to use "nickname"
        nickname: string;
        naics: string;
        size: string;

        primaryLocationState: string;
        primaryLocationZip: number;
        primaryLocationZipLatitude: number;
        primaryLocationZipLongitude: number;

        hasCivilianPrimeContract: boolean;
        hasCivilianSubContract: boolean;
        hasDodPrimeContract: boolean;
        hasDodSubContract: boolean;
        hasParticipatedMentorProgram: boolean;
        hasParticipatedNistMep: boolean;
        hasCurrentParticipationMentorProgram: boolean;

        has8aSmallBusiness: boolean;
        hasHubzoneBusiness: boolean;
        hasIndianEconomicEnterprise: boolean;
        hasServiceDisabledVeteranBusiness: boolean;
        hasVeteranOwnedBusiness: boolean;
        hasWomenOwnedBusiness: boolean;
    }
}
