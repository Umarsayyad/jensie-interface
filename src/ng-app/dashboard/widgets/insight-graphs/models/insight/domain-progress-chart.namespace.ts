import { ID } from '../../../../../common/models';
import { Level } from '../level/level.namespace';

export namespace DomainProgressChart {
    export interface ILevelData {
        name: string; // "Level #"
        series: ISectionData[];
    }

    export interface ISectionData {
        name: string; // identifier: "AC"
        value: number;
        meanProgress: number;
        percentCompliant: number;
        attemptCount: number;
        exists: boolean;
        section: ID;
        level: ID;
        fullName: string;
        identifier: string;
    }

    export interface IGroupedValue {
        count: number;
        sums: Map<ID /*sectionId*/, Map<Level.Levels, IStats>>;
    }

    export interface IStats {
        progress: number; // Sum of section.progress
        compliant: number; // Count of compliant companies
        count: number; // Count of companies with self-assessments covering this domain and level
        exists: boolean;
    }
}
