import { Injectable } from '@angular/core';
import { Dimension } from 'crossfilter2';

import { InsightWidgetsService } from '../insight-widgets.service';
import { Insight } from '../models/insight/insight.namespace';
import { DimensionService } from './dimension-service';

//do not provide in root -- we want this scoped to Insight widgets
@Injectable()
export class IndustryDimensionService extends DimensionService<string> {
    private industryDimension: Dimension<Insight.ICustomerCrossfilterRecord, string>;
    protected get dimension(): Dimension<Insight.ICustomerCrossfilterRecord, string> {
        return this.industryDimension;
    }

    constructor(private widgetManager: InsightWidgetsService) {
        super();
        this.widgetManager.getNdx().then((ndx) => {
            this.ndx = ndx;
            this.industryDimension = this.ndx.dimension((d) => d.industry);
        });
    }
}
