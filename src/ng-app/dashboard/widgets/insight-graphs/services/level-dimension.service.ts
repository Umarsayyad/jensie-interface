import { Injectable } from '@angular/core';
import { Dimension } from 'crossfilter2';

import { KeyValueHtml } from '../../../models/key-value';
import { InsightWidgetsService } from '../insight-widgets.service';
import { Insight } from '../models/insight/insight.namespace';
import { DimensionService } from './dimension-service';

//do not provide in root -- we want this scoped to Insight widgets
@Injectable()
export class LevelDimensionService extends DimensionService<string> {
    private levelDimension: Dimension<Insight.ICustomerCrossfilterRecord, string>;
    protected get dimension(): Dimension<Insight.ICustomerCrossfilterRecord, string> {
        return this.levelDimension;
    }

    constructor(private widgetManager: InsightWidgetsService) {
        super();
        this.widgetManager.getNdx().then((ndx) => {
            this.ndx = ndx;
            this.levelDimension = this.ndx.dimension((d) => d.currentLevel);
        });
    }

    //@override
    public currentData(): KeyValueHtml[] {
        return this.getGroup()
            .reduceCount()
            .all() //DON'T sort these by largest value
            .map<KeyValueHtml>((e) => Object.assign(e, { html: `${e.key} &nbsp; (${e.value})` }) as KeyValueHtml);
    }
}
