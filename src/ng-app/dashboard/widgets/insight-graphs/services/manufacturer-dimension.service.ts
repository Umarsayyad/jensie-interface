import { Injectable } from '@angular/core';
import { Dimension } from 'crossfilter2';

import { KeyValueHtml } from '../../../models/key-value';
import { InsightWidgetsService } from '../insight-widgets.service';
import { Insight } from '../models/insight/insight.namespace';
import { DimensionService } from './dimension-service';

//do not provide in root -- we want this scoped to Insight widgets
@Injectable()
export class ManufacturerDimensionService extends DimensionService<string> {
    private manufacturerTypeDimension: Dimension<Insight.ICustomerCrossfilterRecord, string>;
    protected get dimension(): Dimension<Insight.ICustomerCrossfilterRecord, string> {
        return this.manufacturerTypeDimension;
    }

    constructor(private widgetManager: InsightWidgetsService) {
        super();
        this.widgetManager.getNdx().then((ndx) => {
            this.ndx = ndx;
            this.manufacturerTypeDimension = this.ndx.dimension((d) => d.manufacturerType);
        });
    }

    //@override
    public currentData(): KeyValueHtml[] {
        return super.currentData().filter((d) => d.key !== '');
    }
}
