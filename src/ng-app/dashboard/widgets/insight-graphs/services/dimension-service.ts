import { OnDestroy } from '@angular/core';
import { Crossfilter, Dimension, EventType, Group, NaturallyOrderedValue } from 'crossfilter2';
import { Filter as DcFilter } from 'dc';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

import { KeyValueHtml } from '../../../models/key-value';
import { Insight } from '../models/insight/insight.namespace';

// @types/dc's Filter def is incorrect (missing filterType) -- see https://dc-js.github.io/dc.js/docs/html/filters.html
type Filter = DcFilter & { filterType: string };

export abstract class DimensionService<T extends string> implements OnDestroy {
    protected ndx: Crossfilter<Insight.ICustomerCrossfilterRecord>;
    protected filters = new Set<T>();
    protected filters$ = new BehaviorSubject<Array<T>>([]);
    private crossfiltersChange$ = new Subject<EventType>();
    private crossfilterOnChangeUnsub: Function;
    private group: Group<Insight.ICustomerCrossfilterRecord, NaturallyOrderedValue, unknown>;

    protected abstract get dimension(): Dimension<Insight.ICustomerCrossfilterRecord, T>;

    ngOnDestroy() {
        this.filters$.complete();
        this.crossfiltersChange$.complete();
        this.crossfilterOnChangeUnsub();
    }

    public getDimension() {
        return this.dimension;
    }

    public getGroup() {
        if (!this.group) {
            this.group = this.dimension.group().reduceCount();
        }
        return this.group;
    }

    public getFilterHandler(): (dimension, filters) => any {
        return this.filterHandler.bind(this);
    }

    public setFilters(filters: T[]) {
        this.filters = new Set(filters);
        this.filter();
    }

    public addFilters(filters: T[]) {
        for (let f of filters) this.addFilter(f);
        this.filter();
    }
    public removeFilters(filters: T[]) {
        for (let f of filters) this.removeFilter(f);
        this.filter();
    }

    public toggleFilters(filters: T | T[]) {
        if (filters instanceof Array) {
            //avoid emitting filterChanged event for an empty array
            if (filters.length === 0) return;

            for (let filter of filters) this.toggleFilter(filter);
        } else if (filters != null) {
            this.toggleFilter(filters);
        } else {
            //error?
            return;
        }

        this.filter();
    }

    private addFilter(filter: T) {
        this.filters.add(filter);
    }
    private removeFilter(filter: T) {
        this.filters.delete(filter);
    }

    private toggleFilter(filter: T) {
        if (this.filters.has(filter)) this.removeFilter(filter);
        else this.addFilter(filter);
    }

    private filter() {
        this.filters$.next(Array.from(this.filters));
        this.filterHandler(this.dimension, Array.from(this.filters));
    }

    public filtersChange(): Observable<Array<T>> {
        return this.filters$.asObservable();
    }

    public crossfiltersChange(): Observable<EventType> {
        // const crossfiltersChange$=new Subject<EventType>();

        if (!this.crossfilterOnChangeUnsub) {
            this.crossfilterOnChangeUnsub = this.ndx.onChange((event: EventType) => {
                this.crossfiltersChange$.next(event);
            });
        }

        return this.crossfiltersChange$.asObservable();
    }

    public currentData(): KeyValueHtml[] {
        if (!this.dimension) throw new Error("ERROR (in Dimension Service): Trying to access data before it's been loaded!");

        return this.getGroup()
            .top(Infinity)
            .map<KeyValueHtml>((e) => Object.assign(e, { html: `${e.key} &nbsp; (${e.value})` }) as KeyValueHtml);
    }

    //the default filter handler handles all possible cases for the charts in dc.js
    //you can replace it with something more specialized for your own chart
    protected filterHandler(dimension: Dimension<Insight.ICustomerCrossfilterRecord, T>, filters: T[] | Filter[]): T[] | Filter[] {
        if (filters.length === 0) {
            //the empty case (no filtering)
            dimension.filter(null);
        } else if (filters.length === 1) {
            const filter = filters[0];
            if (!this.isFilter(filter) || !filter.isFiltered) {
                //single value and not a function-based filter
                dimension.filterExact(filter as T);
            } else if (filter.filterType === 'RangedFilter') {
                //single range-based filter
                dimension.filterRange(filter as any);
            }
        } else {
            //an array of values, or an array of filter objects
            dimension.filterFunction((d) => {
                for (let filter of filters) {
                    if (this.isFilter(filter) && filter.isFiltered(d)) {
                        return true;
                    } else if (filter <= d && filter >= d) {
                        return true;
                    }
                }

                return false;
            });
        }

        return filters;
    }

    private isFilter(f: any): f is Filter {
        try {
            return f.hasOwnProperty('filterType') && f.hasOwnProperty('isFiltered');
        } catch (e) {
            return false;
        }
    }
}
