import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Crossfilter } from 'crossfilter2';
import { format } from 'd3';
import { redrawAll, RowChart, rowChart } from 'dc';
import { noop, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { LoadingContentComponent } from '../../../../common';
import { rowChartColors } from '../../../../common/data';
import { IWidget } from '../../../models/widget.interface';
import { DashboardWidgets } from '../../../services/dashboard-widgets.service';
import { BarChartConfig } from '../data/bar-chart-config';
import { InsightWidgetsService, LoadProgress } from '../insight-widgets.service';
import { Insight } from '../models/insight/insight.namespace';
import { ContractDimensionService } from '../services/contract-dimension.service';
import { id } from './contract-types.widget';

@Component({
    selector: id,
    templateUrl: '../common/insight-widget.html',
    styleUrls: ['../common/insight-widget.less'],
})
export class ContractTypesComponent implements OnInit, AfterViewInit, OnDestroy {
    private widget: IWidget | undefined;
    private ndx: Crossfilter<Insight.ICustomerCrossfilterRecord>;
    private contractTypesChart: RowChart;
    private onDestroy$ = new Subject();

    //must start at -1 because otherwise the graph ElementRef is undefined
    size = -1;

    @ViewChild('spinner') spinner: LoadingContentComponent;
    @ViewChild('widgetGraph') contractTypes: ElementRef;
    private showReset: boolean;

    constructor(private dbWidgets: DashboardWidgets, private widgetManager: InsightWidgetsService, private dimService: ContractDimensionService) {}

    ngOnInit() {
        this.widget = this.dbWidgets.getWidget({ id });
        if (!this.widget) throw new Error('Error: Could not find Program Participation widget.');
    }

    ngAfterViewInit() {
        this.spinner.startLoading();

        this.contractTypesChart = rowChart(this.contractTypes.nativeElement);

        this.widgetManager.getNdx().then((ndx) => {
            this.ndx = ndx;

            // Don't subscribe after the component is unloaded (possible if the user leaves the tab before any requests complete)
            if (!this.onDestroy$.isStopped) {
                this.widgetManager
                    .loadProgress()
                    .pipe(takeUntil(this.onDestroy$))
                    .subscribe((status) => {
                        if (status === LoadProgress.Started) {
                            this.spinner.startLoading();
                        } else if (status === LoadProgress.LevelLoaded) {
                            this.spinner.stopLoading();
                            this.size = this.dimService.getGroup().size();
                            setTimeout(() => this.contractTypesChart.render());
                        }
                    });

                this.configureCharts();
                this.contractTypesChart.render();

                this.widget.update = () => this.contractTypesChart.render();

                this.spinner.stopLoading();
            }
        });

        this.dimService
            .filtersChange()
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((filters) => {
                if (filters.length > 0) (this.contractTypesChart as any).replaceFilter([filters]);
                else this.contractTypesChart.filterAll();

                if (this.ndx) redrawAll();
            });
    }

    ngOnDestroy(): void {
        this.onDestroy$.next();
        this.onDestroy$.complete();
        this.widget.update = noop;
    }

    hideWidget() {
        this.dbWidgets.hideWidget(this.widget);
    }

    private configureCharts() {
        const contractsDimension = this.dimService.getDimension();
        const contractsGroup = this.dimService.getGroup();

        this.size = contractsGroup.size();

        this.contractTypesChart
            .on('filtered.showReset', (chart) => {
                this.showReset = chart.hasFilter();
            })
            .ordinalColors(rowChartColors)
            .elasticX(true)
            .filterHandler(this.dimService.getFilterHandler())
            .dimension(contractsDimension)
            .gap(BarChartConfig.GAP)
            .labelOffsetY(BarChartConfig.LABEL_OFFSET_Y)
            .labelOffsetX(BarChartConfig.LABEL_OFFSET_X)
            .group(contractsGroup);

        this.contractTypesChart.onClick = (d) => {
            const key = this.contractTypesChart.keyAccessor()(d);
            if (key === 'Others' && d.others) this.dimService.toggleFilters(d.others);
            else this.dimService.toggleFilters(key);
        };

        this.contractTypesChart.xAxis().tickFormat(format(',d'));
    }

    resetFilters() {
        this.dimService.setFilters([]);
        this.showReset = false;
    }
}
