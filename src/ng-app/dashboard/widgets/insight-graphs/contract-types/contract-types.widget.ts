import { InjectionToken } from '@angular/core';

import { IWidgetParams } from '../../../models/widget.interface';
import { loadForInsight } from '../../../services/load-for-utils';

export const id = 'contract-types';

export const CONTRACT_TYPES_WIDGET = new InjectionToken(id);

export const contractTypesWidget: IWidgetParams = {
    id,
    version: 1,
    title: 'Contract Types',
    gridsterClasses: [],
    sizeX: 2,
    sizeY: 2,
    isAngularComponent: true,
    component: CONTRACT_TYPES_WIDGET,
    templateUrl: '',
    update: () => null,
    shouldLoad: loadForInsight,
    description:
        'How many participating companies hold at least one Department of Defense (DOD) prime contract, DOD subcontract, civilian agency prime contract, or civilian agency subcontract. Each participant may possess multiple contract types.',
};
