import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Crossfilter, Group, Grouping } from 'crossfilter2';
import { format } from 'd3';
import { redrawAll, RowChart, rowChart } from 'dc';
import { noop, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { LoadingContentComponent } from '../../../../common/components';
import { rowChartColors } from '../../../../common/data';
import { IWidget } from '../../../models/widget.interface';
import { DashboardWidgets } from '../../../services/dashboard-widgets.service';
import { BarChartConfig } from '../data/bar-chart-config';
import { InsightWidgetsService, LoadProgress } from '../insight-widgets.service';
import { Insight } from '../models/insight/insight.namespace';
import { ManufacturerDimensionService } from '../services/manufacturer-dimension.service';
import { id } from './manufacturer-breakdown.widget';

@Component({
    selector: id,
    templateUrl: '../common/insight-widget.html',
    styleUrls: ['../common/insight-widget.less'],
})
export class ManufacturerBreakdownComponent implements OnInit, AfterViewInit, OnDestroy {
    private widget: IWidget | undefined;
    private ndx: Crossfilter<Insight.ICustomerCrossfilterRecord>;
    private manufacturerTypeChart: RowChart;
    private onDestroy$ = new Subject();

    //must start at -1 because otherwise the graph ElementRef is undefined
    size = -1;
    showReset: boolean;

    @ViewChild('spinner') spinner: LoadingContentComponent;
    @ViewChild('widgetGraph') manufacturerType: ElementRef;

    constructor(
        private dbWidgets: DashboardWidgets,
        private widgetManager: InsightWidgetsService,
        private dimService: ManufacturerDimensionService
    ) {}

    ngOnInit() {
        this.widget = this.dbWidgets.getWidget({ id });
        if (!this.widget) throw new Error('Error: Could not find Manufacturer Breakdown widget.');
    }

    ngAfterViewInit() {
        this.spinner.startLoading();

        this.manufacturerTypeChart = rowChart(this.manufacturerType.nativeElement);

        this.widgetManager.getNdx().then((ndx) => {
            this.ndx = ndx;

            // Don't subscribe after the component is unloaded (possible if the user leaves the tab before any requests complete)
            if (!this.onDestroy$.isStopped) {
                this.widgetManager
                    .loadProgress()
                    .pipe(takeUntil(this.onDestroy$))
                    .subscribe((status) => {
                        if (status === LoadProgress.Started) {
                            this.spinner.startLoading();
                        } else if (status === LoadProgress.LevelLoaded) {
                            this.spinner.stopLoading();
                            this.size = this.dimService.getGroup().size();
                            setTimeout(() => this.manufacturerTypeChart.render());
                        }
                    });

                this.configureCharts();
                this.manufacturerTypeChart.render();

                this.widget.update = () => this.manufacturerTypeChart.render();

                this.spinner.stopLoading();
            }
        });

        this.dimService
            .filtersChange()
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((filters) => {
                if (filters.length > 0) (this.manufacturerTypeChart as any).replaceFilter([filters]);
                else this.manufacturerTypeChart.filterAll();

                if (this.ndx) redrawAll();
            });
    }

    ngOnDestroy(): void {
        this.onDestroy$.next();
        this.onDestroy$.complete();
        this.widget.update = noop;
    }

    hideWidget() {
        this.dbWidgets.hideWidget(this.widget);
    }

    private configureCharts() {
        const manufacturerTypeDimension = this.dimService.getDimension();
        const manufacturerTypeGroup = this.dimService.getGroup();

        this.size = this.removeNoManufacturerTypeBin(manufacturerTypeGroup).all().length;

        this.manufacturerTypeChart
            .on('filtered.showReset', (chart) => {
                this.showReset = chart.hasFilter();
            })
            .ordinalColors(rowChartColors)
            .elasticX(true)
            .filterHandler(this.dimService.getFilterHandler())
            .dimension(manufacturerTypeDimension)
            .gap(BarChartConfig.GAP)
            .labelOffsetY(BarChartConfig.LABEL_OFFSET_Y)
            .labelOffsetX(BarChartConfig.LABEL_OFFSET_X)
            .group(this.removeNoManufacturerTypeBin(manufacturerTypeGroup));

        this.manufacturerTypeChart.onClick = (d) => {
            const key = this.manufacturerTypeChart.keyAccessor()(d);
            if (key === 'Others' && d.others) this.dimService.toggleFilters(d.others);
            else this.dimService.toggleFilters(key);
        };

        this.manufacturerTypeChart.xAxis().tickFormat(format(',d'));
    }

    private removeNoManufacturerTypeBin(sourceGroup: Group<Insight.ICustomerCrossfilterRecord, any, any>): { all: () => Array<Grouping<any, any>> } {
        return {
            all: () => {
                return sourceGroup.all().filter((d) => {
                    // console.log(d);
                    return d.key !== '';
                });
            },
        };
    }

    resetFilters() {
        this.dimService.setFilters([]);
        this.showReset = false;
    }
}
