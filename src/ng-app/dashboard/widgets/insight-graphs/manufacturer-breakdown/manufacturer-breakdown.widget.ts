import { InjectionToken } from '@angular/core';

import { IWidgetParams } from '../../../models/widget.interface';
import { loadForInsight } from '../../../services/load-for-utils';

export const id = 'manufacturer-breakdown';

export const MANUFACTURER_BREAKDOWN_WIDGET = new InjectionToken(id);

export const manufacturerBreakdownWidget: IWidgetParams = {
    id,
    version: 1,
    title: 'DIB Participants by Manufacturing NAICS Subsector',
    gridsterClasses: [],
    sizeX: 3,
    sizeY: 4,
    isAngularComponent: true,
    component: MANUFACTURER_BREAKDOWN_WIDGET,
    templateUrl: '',
    update: () => null,
    shouldLoad: loadForInsight,
    description:
        'Of the participating companies that are classified under a manufacturing NAICS code, this lists their specific primary NAICS codes.',
};
