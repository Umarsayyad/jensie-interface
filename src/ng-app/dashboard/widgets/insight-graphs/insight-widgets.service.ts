import { Injectable, OnDestroy } from '@angular/core';
import { Crossfilter } from 'crossfilter2';
import * as crossfilter from 'crossfilter2';
import { Observable, Subject } from 'rxjs';

import { ID } from '../../../common/models';
import { getEnumValues } from '../../../common/utilities';
import { NaicsIndustryService } from '../../../company-profile';
import { ICustomer } from '../../../global-data/models/customer.interface';
import { GlobalDataService } from '../../../ngjs-upgrade/globalDataService';
import { TagTypes } from './data/tag-types';
import { InsightDashboardResourceishService } from './insight-dashboard-resourceish.service';
import { Domain } from './models/domain/domain.namespace';
import { Insight } from './models/insight/insight.namespace';
import { ProgressHistory } from './models/insight/progress-history.namespace';
import { Level } from './models/level/level.namespace';

type Levels = Level.Levels;
const Levels = Level.Levels;

export const enum LoadProgress {
    Started = 'start',
    LevelLoaded = 'level',
    DomainLoaded = 'domain',
}

//do not provide in root -- we want this scoped to Insight widgets
@Injectable()
export class InsightWidgetsService implements OnDestroy {
    private ndx: Crossfilter<Insight.ICustomerCrossfilterRecord>;
    private ndx$: Promise<Crossfilter<Insight.ICustomerCrossfilterRecord>> | undefined;
    private historyEntries$: Promise<ProgressHistory.IEntry[]>;
    private sectionDetailMap: Map<ID, Domain.ISection>;
    private loadProgress$ = new Subject<LoadProgress>();
    private currentEntry: ProgressHistory.IEntry | null;

    constructor(
        private insightResource: InsightDashboardResourceishService,
        private naicsIndustryService: NaicsIndustryService,
        private gdata: GlobalDataService
    ) {
        this.sectionDetailMap = new Map<ID, Domain.ISection>();
    }

    ngOnDestroy(): void {
        this.loadProgress$.complete();
    }

    private init(): Promise<void> {
        this.ndx = crossfilter([]);

        return this.loadHistoryEntry(null);
    }

    private loadData(byLevel: Promise<Level.IResponse>, byDomain: Promise<Domain.IResponse>, customerMap: Record<ID, ICustomer>): Promise<void> {
        const byLevelProcessed = byLevel.then((customerProgresses) => {
            const chartData = customerProgresses.map((customerProgress) => {
                const customer: ICustomer = customerMap[customerProgress.customer];
                this.gdata.setCustomerBusinessValues(customer);
                const levelInfo = {};
                let currentLevel = 0;

                for (let level of getEnumValues<Levels>(Levels)) {
                    const progressLevel: Level.IPractice = customerProgress[level].practices;
                    levelInfo[`level${level}progress`] = progressLevel.progress;
                    if (progressLevel.earnedPoints === progressLevel.availablePoints) {
                        currentLevel = level;
                    }
                }

                const industry = this.naicsIndustryService.getNaicsSectorName(customer.naics, false);
                const retVal: Insight.ICustomerCrossfilterRecord = {
                    id: customer.id,
                    name: customer.name,
                    size: customer.size || 'Not provided',
                    nickname: customer.nickname,
                    naics: customer.naics,
                    level1progress: customerProgress[Levels.Level1].practices.progress,
                    level2progress: customerProgress[Levels.Level2].practices.progress,
                    level3progress: customerProgress[Levels.Level3].practices.progress,
                    level4progress: customerProgress[Levels.Level4].practices.progress,
                    level5progress: customerProgress[Levels.Level5].practices.progress,
                    currentLevel: this.levelNumToString(currentLevel),
                    domainProgress: {},
                    businessCategoryTags: (customer as any).business_category,
                    contractTags: this.buildTags(TagTypes.Contract, customer, this.gdata.customerOptions),
                    participationTags: this.buildTags(TagTypes.Participation, customer, this.gdata.customerOptions),
                    state: customer.primary_location_state || 'Not provided',
                    industry,
                    manufacturerType: industry === 'Manufacturing' ? `${this.naicsIndustryService.getNaicsSubSectorName(customer.naics, true)}` : '',
                };

                return retVal;
            });

            this.ndx.add(chartData);
            this.loadProgress$.next(LoadProgress.LevelLoaded);
        });

        Promise.all([byLevelProcessed, byDomain]).then(([_, domainProgresses]) => {
            const domainProgressByCustomer = new Map<ID, { [key: number]: Domain.IProgress }>();
            for (const section of domainProgresses.sectionDetail) {
                this.sectionDetailMap.set(section.id, section);
            }

            for (let progressStat of domainProgresses.progressStats) {
                if (!domainProgressByCustomer.has(progressStat.customer)) {
                    domainProgressByCustomer.set(progressStat.customer, {});
                }
                domainProgressByCustomer.get(progressStat.customer)[progressStat.section] = progressStat;
            }

            for (let customerEntry of this.ndx.all()) {
                customerEntry.domainProgress = domainProgressByCustomer.get(customerEntry.id);
            }
            this.loadProgress$.next(LoadProgress.DomainLoaded);
        });
        return byLevelProcessed;
    }

    public getNdx(): Promise<Crossfilter<Insight.ICustomerCrossfilterRecord>> {
        if (!this.ndx$) {
            this.ndx$ = new Promise((resolve) => {
                this.init().then(() => resolve(this.ndx));
            });
        }

        return this.ndx$;
    }

    public getCurrentEntry(): ProgressHistory.IEntry | null {
        return this.currentEntry;
    }

    public getHistoryEntries(): Promise<ProgressHistory.IEntry[]> {
        if (!this.historyEntries$) {
            this.historyEntries$ = this.insightResource.listProgressHistory().toPromise();
        }

        return this.historyEntries$;
    }

    /**
     * Load the given history entry or current results if `null` was passed
     *
     * @param entry - history entry to load or `null` to display current results
     */
    public loadHistoryEntry(entry: ProgressHistory.IEntry | null): Promise<void> {
        this.loadProgress$.next(LoadProgress.Started);
        this.currentEntry = entry;
        if (entry !== null) {
            return this.insightResource
                .getProgressHistoryData(entry.id)
                .toPromise()
                .then((dataEntry: ProgressHistory.IDataEntry) => {
                    const customerMap = {};
                    for (const customer of dataEntry.customerData) {
                        customerMap[customer.id] = customer;
                    }
                    this.ndx.remove(() => true);
                    return this.loadData(Promise.resolve(dataEntry.levelProgressData), Promise.resolve(dataEntry.domainProgressData), customerMap);
                });
        } else {
            this.ndx.remove(() => true);

            const byLevel = this.insightResource.progressByLevel().toPromise();
            const byDomain = this.insightResource.progressByDomain().toPromise();
            const customerMap = this.gdata.customerMap;
            return this.loadData(byLevel, byDomain, customerMap);
        }
    }

    public loadProgress(): Observable<LoadProgress> {
        return this.loadProgress$.asObservable();
    }

    public getSection(id: ID): Domain.ISection {
        return this.sectionDetailMap.get(id);
    }

    private buildTags(type: TagTypes, c: ICustomer, customerOptions: { [key: string]: any }): string[] {
        const props: Record<TagTypes, Array<string>> = {
            contract: ['has_civilian_prime_contract', 'has_civilian_sub_contract', 'has_dod_prime_contract', 'has_dod_sub_contract'],
            participation: ['has_participated_mentor_program', 'has_participated_nist_mep'],
            category: [], //unused here
        };

        const tags = [];
        for (let prop of props[type]) {
            if (c[prop] && customerOptions[prop]) {
                tags.push(customerOptions[prop].help_text);
            }
        }

        return tags;
    }

    private levelNumToString(key: number): string {
        if (isNaN(key)) return key as any;
        else if (key !== 0) return `Level ${key}`;
        else return 'No level achieved';
    }
}
