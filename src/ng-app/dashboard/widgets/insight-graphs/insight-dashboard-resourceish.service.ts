import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { config } from '@cato/config';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ID } from '../../../common/models';
import { Domain } from './models/domain/domain.namespace';
import { Insight } from './models/insight/insight.namespace';
import { ProgressHistory } from './models/insight/progress-history.namespace';
import { Level } from './models/level/level.namespace';

@Injectable({ providedIn: 'root' })
export class InsightDashboardResourceishService {
    private readonly prefix = config.apiPath + 'compliance/insight';

    constructor(private http: HttpClient) {}

    public progressByLevel(questionnaire: ID = 1): Observable<Level.IResponse> {
        return this.http.get<Level.IResponse>(`${this.prefix}/questionnaire/${questionnaire}/progressbylevel`);
    }

    public progressByDomain(questionnaire: ID = 1): Observable<Domain.IResponse> {
        return this.http.get<Domain.IResponse>(`${this.prefix}/questionnaire/${questionnaire}/progressbydomain`);
    }

    public listProgressHistory(questionnaire: ID = 1): Observable<Array<ProgressHistory.IEntry>> {
        return this.http.get<Array<ProgressHistory.IEntry>>(`${this.prefix}/progresshistory`, {
            params: new HttpParams().set('questionnaire', questionnaire.toString()),
        });
    }

    public getProgressHistoryData(historyEntryId: ID): Observable<ProgressHistory.IDataEntry> {
        return this.http.get<ProgressHistory.IRawDataEntry>(`${this.prefix}/progresshistory/${historyEntryId}/data`).pipe(
            map(
                (entry: ProgressHistory.IRawDataEntry): ProgressHistory.IDataEntry => {
                    return {
                        ...entry,
                        customerData: entry.customerData.map((customer) => {
                            /* eslint-disable @typescript-eslint/camelcase */
                            return {
                                id: customer.id,
                                name: (customer as any).name, // "name" exists on the data, but prefer to use "nickname"
                                nickname: customer.nickname,
                                naics: customer.naics,
                                size: customer.size,

                                primary_location_state: customer.primaryLocationState,
                                primary_location_zip: customer.primaryLocationZip,
                                primary_location_zip_latitude: customer.primaryLocationZipLatitude,
                                primary_location_zip_longitude: customer.primaryLocationZipLongitude,

                                has_civilian_prime_contract: customer.hasCivilianPrimeContract,
                                has_civilian_sub_contract: customer.hasCivilianSubContract,
                                has_dod_prime_contract: customer.hasDodPrimeContract,
                                has_dod_sub_contract: customer.hasDodSubContract,
                                has_participated_mentor_program: customer.hasParticipatedMentorProgram,
                                has_participated_nist_mep: customer.hasParticipatedNistMep,
                                has_current_participation_mentor_program: customer.hasCurrentParticipationMentorProgram,

                                has_8a_small_business: customer.has8aSmallBusiness,
                                has_hubzone_business: customer.hasHubzoneBusiness,
                                has_indian_economic_enterprise: customer.hasIndianEconomicEnterprise,
                                has_service_disabled_veteran_business: customer.hasServiceDisabledVeteranBusiness,
                                has_veteran_owned_business: customer.hasVeteranOwnedBusiness,
                                has_women_owned_business: customer.hasWomenOwnedBusiness,
                            };
                            /* eslint-enable @typescript-eslint/camelcase */
                        }),
                    };
                }
            )
        );
    }
}
