import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Crossfilter } from 'crossfilter2';
import { legend, PieChart, pieChart, redrawAll } from 'dc';
import { noop, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { LoadingContentComponent } from '../../../../common/components';
import { levelColorsObject } from '../../../../global-data/cmmc-level-colors';
import { IWidget, WidgetConst } from '../../../models/widget.interface';
import { DashboardWidgets } from '../../../services/dashboard-widgets.service';
import { InsightWidgetsService, LoadProgress } from '../insight-widgets.service';
import { Insight } from '../models/insight/insight.namespace';
import { LevelDimensionService } from '../services/level-dimension.service';
import { id } from './readiness-level.widget';

@Component({
    selector: id,
    templateUrl: '../common/insight-widget.html',
    styleUrls: ['../common/insight-widget.less'],
})
export class ReadinessLevelComponent implements OnInit, AfterViewInit, OnDestroy {
    readonly MAX_LEGEND_ITEMS: number = 6;

    private widget: IWidget | undefined;
    private ndx: Crossfilter<Insight.ICustomerCrossfilterRecord>;
    private deferRender: Promise<void>;
    private levelChart: PieChart;
    private onDestroy$ = new Subject();

    //must start at -1 because otherwise the graph ElementRef is undefined
    size = -1;
    showReset: boolean;

    @ViewChild('spinner') spinner: LoadingContentComponent;
    @ViewChild('widgetContainer') widgetContainer: ElementRef;
    @ViewChild('widgetGraph') level: ElementRef;

    constructor(private dbWidgets: DashboardWidgets, private widgetManager: InsightWidgetsService, private dimService: LevelDimensionService) {}

    ngOnInit() {
        let renderResolve;
        this.deferRender = new Promise((resolve) => (renderResolve = resolve));

        this.widget = this.dbWidgets.getWidget({ id });
        if (!this.widget) throw new Error('Error: Could not find Readiness Level widget.');

        this.widgetManager.getNdx().then((ndx) => {
            this.ndx = ndx;

            // Don't subscribe after the component is unloaded (possible if the user leaves the tab before any requests complete)
            if (!this.onDestroy$.isStopped) {
                this.widgetManager
                    .loadProgress()
                    .pipe(takeUntil(this.onDestroy$))
                    .subscribe((status) => {
                        if (status === LoadProgress.Started) {
                            this.spinner.startLoading();
                        } else if (status === LoadProgress.LevelLoaded) {
                            this.spinner.stopLoading();
                            this.size = this.dimService.getGroup().size();
                            this.levelChart.redraw();
                        }
                    });

                this.configureCharts();
                renderResolve();

                this.widget.update = () => {
                    this.computeWidgetSize();
                    this.levelChart.redraw();
                };
            }
        });
    }

    ngAfterViewInit() {
        this.spinner.startLoading();

        this.levelChart = pieChart(this.level.nativeElement);

        this.deferRender.then(() => {
            this.levelChart.render();
            this.spinner.stopLoading();
        });

        this.dimService
            .filtersChange()
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((filters) => {
                if (filters.length > 0) (this.levelChart as any).replaceFilter([filters]);
                else this.levelChart.filterAll();

                if (this.ndx) redrawAll();
            });
    }

    ngOnDestroy(): void {
        this.onDestroy$.next();
        this.onDestroy$.complete();
        this.widget.update = noop;
    }

    hideWidget() {
        this.dbWidgets.hideWidget(this.widget);
    }

    private configureCharts() {
        const levelDimension = this.dimService.getDimension();
        const levelGroup = this.dimService.getGroup();

        this.size = levelGroup.size();

        this.computeWidgetSize();

        this.levelChart
            .on('filtered.showReset', (chart) => {
                this.showReset = chart.hasFilter();
            })
            .colorCalculator((d) => {
                return levelColorsObject[d.key];
            })
            .emptyTitle('No companies match filters')
            .renderLabel(false)
            .filterHandler(this.dimService.getFilterHandler())
            .dimension(levelDimension)
            .group(levelGroup)
            .legend(
                legend()
                    .gap(WidgetConst.LEGEND_ITEM_GAP)
                    .maxItems(this.MAX_LEGEND_ITEMS)
                    .legendText((d) => `${d.name} (${d.data})`)
            );

        this.levelChart.onClick = (d) => {
            const key = this.levelChart.keyAccessor()(d);
            if (key === 'Others' && d.others) this.dimService.toggleFilters(d.others);
            else this.dimService.toggleFilters(key);
        };
    }

    private computeWidgetSize() {
        const e = this.widgetContainer.nativeElement as HTMLElement;
        const height = e.getBoundingClientRect().height;
        const width = e.getBoundingClientRect().width;

        const rad = Math.min(height - 30 - WidgetConst.RADIUS_OFFSET, width) / 2;

        this.levelChart
            .height(height - 15)
            .cy(height / 2 + WidgetConst.POSITION_OFFSET)
            .width(width - 15)
            .radius(rad);
    }

    resetFilters() {
        this.dimService.setFilters([]);
        this.showReset = false;
    }
}
