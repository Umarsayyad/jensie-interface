import { InjectionToken } from '@angular/core';

import { IWidgetParams } from '../../../models/widget.interface';
import { loadForInsight } from '../../../services/load-for-utils';

export const id = 'readiness-level';

export const READINESS_LEVEL_WIDGET = new InjectionToken(id);

export const readinessLevelWidget: IWidgetParams = {
    id,
    version: 1,
    title: 'DIB Participants by CMMC Level',
    gridsterClasses: [],
    sizeX: 2,
    sizeY: 3,
    isAngularComponent: true,
    component: READINESS_LEVEL_WIDGET,
    templateUrl: '',
    update: () => null,
    shouldLoad: loadForInsight,
    description: 'The highest CMMC compliance level achieved for each participating company, based on their self-assessment results.',
};
