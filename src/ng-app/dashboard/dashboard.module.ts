import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { UIRouterModule } from '@uirouter/angular';
import { GridsterModule } from 'angular2gridster';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';

import { CatoCommonModule } from '../common/common.module';
import { DashboardComponent } from './cato-dashboard/dashboard.component';
import { WidgetDirective } from './directives/widget.directive';
import { WidgetWrapperDirective } from './directives/widget-wrapper.directive';

import { InsightFiltersComponent } from './insight-filters/insight-filters.component';
import { InsightFiltersService } from './insight-filters/insight-filters.service';
import { InsightFilterTitleComponent } from './insight-filter-title/insight-filter-title.component';
import { InsightHistorySelectorComponent } from './insight-history-selector/insight-history-selector.component';
import { InsightWidgetsService } from './widgets/insight-graphs/insight-widgets.service';

import { CategoryDimensionService } from './widgets/insight-graphs/services/category-dimension.service';
import { ContractDimensionService } from './widgets/insight-graphs/services/contract-dimension.service';
import { IndustryDimensionService } from './widgets/insight-graphs/services/industry-dimension.service';
import { LevelDimensionService } from './widgets/insight-graphs/services/level-dimension.service';
import { ManufacturerDimensionService } from './widgets/insight-graphs/services/manufacturer-dimension.service';
import { ParticipationDimensionService } from './widgets/insight-graphs/services/participation-dimension.service';
import { SizeDimensionService } from './widgets/insight-graphs/services/size-dimension.service';
import { StateDimensionService } from './widgets/insight-graphs/services/state-dimension.service';

import { AchievementProgressComponent, ACHIEVEMENT_GRAPH_WIDGET } from './widgets/insight-graphs';
import { BusinessCategoriesComponent, BUSINESS_CATEGORIES_WIDGET } from './widgets/insight-graphs';
import { CompanySizeComponent, COMPANY_SIZE_WIDGET } from './widgets/insight-graphs';
import { ContractTypesComponent, CONTRACT_TYPES_WIDGET } from './widgets/insight-graphs';
import { DomainProgressComponent, DOMAIN_GRAPH_WIDGET } from './widgets/insight-graphs';
import { IndustryBreakdownComponent, INDUSTRY_BREAKDOWN_WIDGET } from './widgets/insight-graphs';
import { ManufacturerBreakdownComponent, MANUFACTURER_BREAKDOWN_WIDGET } from './widgets/insight-graphs';
import { ProgramParticipationComponent, PROGRAM_PARTICIPATION_WIDGET } from './widgets/insight-graphs';
import { ReadinessLevelComponent, READINESS_LEVEL_WIDGET } from './widgets/insight-graphs';
import { StateBreakdownComponent, STATE_BREAKDOWN_WIDGET } from './widgets/insight-graphs';

const dashboardViewState = {
    name: 'cato.loggedIn.dashboard.matrix',
    url: '/home',
    sticky: true,
    views: {
        dashboard: {
            component: DashboardComponent,
        },
    },
    resolve: {
        $title: ($title) => $title,
    },
};

@NgModule({
    declarations: [
        DashboardComponent,
        InsightFilterTitleComponent,
        InsightFiltersComponent,
        WidgetDirective,
        WidgetWrapperDirective,

        AchievementProgressComponent,
        BusinessCategoriesComponent,
        CompanySizeComponent,
        ContractTypesComponent,
        DomainProgressComponent,
        IndustryBreakdownComponent,
        InsightHistorySelectorComponent,
        ManufacturerBreakdownComponent,
        ProgramParticipationComponent,
        ReadinessLevelComponent,
        StateBreakdownComponent,
    ],
    entryComponents: [DashboardComponent],
    imports: [
        CommonModule,
        FormsModule,
        GridsterModule,
        MatButtonModule,
        MatDatepickerModule,
        MatDialogModule,
        MatInputModule,
        MatInputModule,
        MatListModule,
        MatMenuModule,
        MatSelectModule,
        MatTooltipModule,
        NgxMatSelectSearchModule,
        ReactiveFormsModule,
        UIRouterModule.forChild({ states: [dashboardViewState] }),

        CatoCommonModule,
        NgxChartsModule,
        MatCardModule,
    ],
    exports: [DashboardComponent, InsightFilterTitleComponent],
    providers: [
        InsightFiltersService,
        InsightWidgetsService,

        CategoryDimensionService,
        ContractDimensionService,
        IndustryDimensionService,
        LevelDimensionService,
        ManufacturerDimensionService,
        ParticipationDimensionService,
        SizeDimensionService,
        StateDimensionService,

        { provide: ACHIEVEMENT_GRAPH_WIDGET, useValue: AchievementProgressComponent },
        { provide: BUSINESS_CATEGORIES_WIDGET, useValue: BusinessCategoriesComponent },
        { provide: COMPANY_SIZE_WIDGET, useValue: CompanySizeComponent },
        { provide: CONTRACT_TYPES_WIDGET, useValue: ContractTypesComponent },
        { provide: DOMAIN_GRAPH_WIDGET, useValue: DomainProgressComponent },
        { provide: INDUSTRY_BREAKDOWN_WIDGET, useValue: IndustryBreakdownComponent },
        { provide: MANUFACTURER_BREAKDOWN_WIDGET, useValue: ManufacturerBreakdownComponent },
        { provide: STATE_BREAKDOWN_WIDGET, useValue: StateBreakdownComponent },
        { provide: PROGRAM_PARTICIPATION_WIDGET, useValue: ProgramParticipationComponent },
        { provide: READINESS_LEVEL_WIDGET, useValue: ReadinessLevelComponent },
    ],
})
export class DashboardModule {}
