import { Component, Injector, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { IInsightSearchService } from '../../insight/models/insight-search-service.interface';
import { GlobalDataService } from '../../ngjs-upgrade/globalDataService';
import { InsightFiltersService } from '../insight-filters/insight-filters.service';
import { VulnerabilityFiltersService } from '../../insight/vulnerability-trends/services/vulnerability-filters.service';

@Component({
    selector: 'insight-filter-title',
    templateUrl: './insight-filter-title.component.html',
    styleUrls: ['./insight-filter-title.component.less'],
})
export class InsightFilterTitleComponent implements OnInit {
    filterTitle = '';
    filterChanged = false;
    searchService: IInsightSearchService;

    @Input() isInsightDash: boolean;

    constructor(private injector: Injector) {}
    private onDestroy$ = new Subject();

    ngOnInit(): void {
        if (this.isInsightDash) {
            this.searchService = this.injector.get(InsightFiltersService) as InsightFiltersService;
        } else {
            this.searchService = this.injector.get(VulnerabilityFiltersService) as VulnerabilityFiltersService;
        }
        this.searchService
            .currentSavedSearchChanges()
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((filterSearch) => {
                this.filterTitle = filterSearch ? filterSearch.name : '';
            });
        this.searchService
            .currentFilterChanges()
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((filterChanged) => {
                this.filterChanged = filterChanged;
            });
    }
}
