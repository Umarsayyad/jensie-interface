export type KeyValue = { key: string; value: string };
export type KeyValueHtml = KeyValue & { html: string };
