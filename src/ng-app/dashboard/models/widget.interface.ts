import { InjectionToken, Type } from '@angular/core';
import { IUser } from '../../global-data/models/user.interface';

export type ShouldLoadWidgetFunction = (userData: IUser) => boolean;

// eslint-disable-next-line
export interface IWidgetParams {
    id: string;
    isAngularComponent: boolean; //false: expect `templateUrl` to be set; true: expect `component` to be set
    templateUrl: string; //use "" if `isAngularComponent` is true
    component: Type<any> | InjectionToken<any> | null; //use null if `isAngularComponent` is false
    title: string;
    version: number;

    col?: number;
    row?: number;
    sizeX?: number;
    sizeY?: number;
    resize?: boolean;
    show?: boolean;

    shouldLoad: ShouldLoadWidgetFunction;
    update: Function; //replaces `updateWidgetGraph` in db ctrl and `chart` here

    gridsterClasses?: string[];

    //used for Service widgets
    service?: any;
    expired?: boolean;

    description: string;
}

export interface IWidget extends IWidgetParams {
    colDefault: number;
    rowDefault: number;
    sizeXDefault: number;
    sizeYDefault: number;

    //don't allow these to be optional
    col: number;
    row: number;
    sizeX: number;
    sizeY: number;
    resize: boolean;
    show: boolean;
    gridsterClasses: string[];
}

export class WidgetConst {
    static readonly POSITION_OFFSET: number = 40;
    static readonly RADIUS_OFFSET: number = WidgetConst.POSITION_OFFSET * 2;
    static readonly LEGEND_ITEM_GAP: number = 5;
}
