import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class SidebarService {
    private collapsed = false;
    private collapsed$ = new BehaviorSubject<boolean>(false);

    public getCollapsedState(): boolean {
        return this.collapsed;
    }

    public getCollapsed(): Observable<boolean> {
        return this.collapsed$.asObservable();
    }

    public toggleCollapsed() {
        this.collapsed = !this.collapsed;
        this.collapsed$.next(this.collapsed);
    }
}
