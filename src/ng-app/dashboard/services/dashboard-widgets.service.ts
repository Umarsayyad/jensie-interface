import { EventEmitter, Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';

import { CustomerSubscriptions } from '../../customer/services/customer-subscriptions.resource';
import { UserResource } from '../../global-data/user.resource';
import { ServicesResource } from '../../global-data/services/services.resource'; //TODO: move up 1 folder
import { IUser, UserPrefData } from '../../global-data/models/user.interface';
import { IWidget, IWidgetParams } from '../models/widget.interface';
import { loadForCustomer, loadForOperator } from './load-for-utils';
import { achievementProgressWidget } from '../widgets/insight-graphs/achievement-progress/achievement-progress.widget';
import { businessCategoriesWidget } from '../widgets/insight-graphs/business-categories/business-categories.widget';
import { companySizeWidget } from '../widgets/insight-graphs/company-size/company-size.widget';
import { contractTypesWidget } from '../widgets/insight-graphs/contract-types/contract-types.widget';
import { industryBreakdownWidget } from '../widgets/insight-graphs/industry-breakdown/industry-breakdown.widget';
import { manufacturerBreakdownWidget } from '../widgets/insight-graphs/manufacturer-breakdown/manufacturer-breakdown.widget';
import { programParticipationWidget } from '../widgets/insight-graphs/program-participation/program-participation.widget';
import { readinessLevelWidget } from '../widgets/insight-graphs/readiness-level/readiness-level.widget';
import { stateBreakdownWidget } from '../widgets/insight-graphs/state-breakdown/state-breakdown.widget';
import { domainProgressWidget } from '../widgets/insight-graphs/domain-progress/domain-progress.widget';
import { DEFAULT_WIDGET_LAYOUT } from './widget-defaults';

@Injectable({ providedIn: 'root' }) //FIXME
export class DashboardWidgets implements OnDestroy {
    private onDestroy$ = new Subject();
    private registeredWidgets = new Map<string, IWidgetParams>(); //used to hard reset widgets array
    private user: IUser | null = null;
    private widgets: IWidget[] = [];
    private widgets$ = new BehaviorSubject(this.widgets);
    private widgetUpdateBouncer = new EventEmitter<void>();
    private svcMap: any;

    constructor(private Users: UserResource, private Subscriptions: CustomerSubscriptions, private Services: ServicesResource) {
        this.widgetUpdateBouncer.pipe(debounceTime(500), takeUntil(this.onDestroy$)).subscribe(() => this.forceUpdateWidgetPrefs());

        //detect when other preferences change and update internal data
        this.Users.uiDataChanges()
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((userData: UserPrefData) => {
                if (this.user && this.user.id === userData.userId) this.user.ui_data = userData.uiData;
            });
    }

    ngOnDestroy() {
        this.onDestroy$.next();
        this.onDestroy$.complete();
    }

    public init(user: IUser) {
        this.widgets = user.ui_data.widgets || [];
        this.user = user;
        this.registeredWidgets.clear();
        this.widgets$.next(this.widgets);
        this.svcMap = {};
    }

    public load() {
        this.loadServiceTileWidgets(this.user).then(() => {
            for (let widget of allWidgets) {
                if (widget.shouldLoad(this.user)) {
                    this.addWidget(widget);
                }
            }

            //support old versions of widgets
            //loop backward so we can remove elements more easily
            for (let i = this.widgets.length - 1; i >= 0; i--) {
                let widget = this.widgets[i];
                if ('additionalParams' in widget) {
                    let w = widget as any;
                    let p = w.additionalParams;
                    if ('service' in p) w.service = p.service;
                    if ('expired' in p) w.expired = p.expired;
                }

                // Load defaults for widgets we have defaults for.
                if (DEFAULT_WIDGET_LAYOUT.hasOwnProperty(widget.id)) {
                    let defaultWidget = DEFAULT_WIDGET_LAYOUT[widget.id];
                    widget.row = defaultWidget.row;
                    widget.sizeYDefault = defaultWidget.sizeYDefault;
                    widget.sizeXDefault = defaultWidget.sizeXDefault;
                    widget.sizeX = defaultWidget.sizeX;
                    widget.sizeY = defaultWidget.sizeY;
                    widget.colDefault = defaultWidget.colDefault;
                    widget.rowDefault = defaultWidget.rowDefault;
                    widget.col = defaultWidget.col;
                    widget.show = defaultWidget.show;
                }

                if (widget.isAngularComponent) {
                    let w = this.registeredWidgets.get(widget.id);
                    if (w) {
                        widget.component = w.component;
                    } else {
                        console.warn(`Could not find component class for widget: ${widget.title}.  Skipping.`);
                        this.widgets.splice(i, 1);
                    }
                }
            }

            this.widgets$.next(this.widgets);
        });
    }

    private addWidget(widget: IWidgetParams) {
        //add to the Map; overwrite what was already there
        this.registeredWidgets.set(widget.id, widget);

        const existingWidget = this.getWidget({ id: widget.id });

        //add to the widgets list if not already there
        if (!existingWidget || existingWidget.version < widget.version) {
            if (existingWidget) {
                this.widgets.splice(this.widgets.indexOf(existingWidget), 1);
            }
            let w = this.createWidget(widget);
            this.widgets.push(w);

            this.updateWidgetPrefs();
        } else {
            // Copy over things that can change every time widgets are loaded or are not JSON serializable
            // TODO: Fix loading order; currently, the nvd3 update function gets put in place when the widgets are displayed based on the contents of `ui_data`, before this code gets to run
            for (let prop of ['service', 'expired', 'component', 'description' /*"update"*/]) {
                if (prop in widget) {
                    // @ts-ignore: NoImplicitAny
                    existingWidget[prop] = widget[prop];
                }
            }
        }
    }

    public getWidgets(): Observable<Array<IWidget>> {
        return this.widgets$.asObservable();
    }

    public getWidget(params: Partial<IWidgetParams>): IWidget | undefined {
        const keys = Object.keys(params);
        return this.widgets.find((e: IWidget) => {
            for (let k of keys) {
                // @ts-ignore: NoImplicitAny
                if (e[k] !== params[k]) return false;
            }

            return true;
        });
    }

    public hideWidget(widget: IWidget) {
        //FIXME: redesign
        // let ui_data = $rootScope.user.ui_data;
        widget.show = false;
        // ui_data['widgets'] = $scope.widgets;

        this.updateWidgetPrefs();
    }
    public showWidget(widget: IWidget) {
        //FIXME: redesign
        // let ui_data = $rootScope.user.ui_data;
        widget.show = true;
        // ui_data['widgets'] = $scope.widgets;

        this.updateWidgetPrefs();
    }

    public updateWidget(widget: IWidget) {
        let w = this.getWidget({ id: widget.id });
        if (!w) throw new Error('Updating non-existent widget: ' + widget.title);

        if (widget.update) setTimeout(() => widget.update(), 100);
        //replace old widget data with the new values
        // Object.assign(w, widget);
        this.updateWidgetPrefs();
    }

    public updateWidgets() {
        this.widgets.forEach((widget) => (widget.update ? widget.update() : 0));
    }

    public resetWidgets(hardReset = false) {
        if (hardReset) {
            console.log('hard resetting all widgets');
            this.widgets = [];
            // this.loadWidgets();

            for (let [_, widget] of this.registeredWidgets) {
                this.widgets.push(this.createWidget(widget));
            }

            this.widgets$.next(this.widgets);
        } else {
            for (let widget of this.widgets) {
                widget.show = true;
                widget.col = widget.colDefault;
                widget.row = widget.rowDefault;
                widget.sizeX = widget.sizeXDefault;
                widget.sizeY = widget.sizeYDefault;

                if (widget.update) setTimeout(widget.update, 500);
            }
        }

        this.updateWidgetPrefs();
    }

    //do NOT call this; call the below method instead
    private forceUpdateWidgetPrefs() {
        this.Users.updatePrefs(this.user, { widgets: this.widgets }).subscribe();
    }
    private updateWidgetPrefs() {
        this.widgetUpdateBouncer.next();
    }

    private createWidget(params: IWidgetParams): IWidget {
        let widget = {
            // Defaults
            sizeX: 1,
            sizeY: 1,

            row: 1,
            col: 1,

            show: true,
            resize: true,
            gridsterClasses: params.resize === false ? ['no-resize'] : [],
        } as IWidget;

        let other =
            // Props that override the `params` variable
            {
                sizeXDefault: params.sizeX || 1,
                sizeYDefault: params.sizeY || 1,
                rowDefault: params.row || 1,
                colDefault: params.col || 1,
            };

        Object.assign(widget, params, other);

        //TODO
        this.getNextColAndRowForWidget(widget);
        return widget;
    }

    // function lastRowForWidgetInCol0() {
    //     var lastWidgetRow = 0;
    //     angular.forEach($scope.widgets, function(widget) {
    //         if (widget.col === 0) {
    //             if (widget.row >= lastWidgetRow) {
    //                 lastWidgetRow = widget.row + widget.sizeY;
    //             }
    //         }
    //     });
    //     return lastWidgetRow;
    // }

    //modifies nextWidget in place and returns it
    private getNextColAndRowForWidget(nextWidget: IWidget): IWidget {
        /* TODO: This code makes assumptions about the order of widgets in this.widgets and ignores some
         *       collisions that can occur when widgets of different sizes are mixed.  See the following links for
         *       more robust implementations:
         * https://github.com/ManifestWebDesign/angular-gridster/blob/fe28af0bf31f7a25d875ec6868c407fde2fa52b7/src/angular-gridster.js#L157-L175
         * https://github.com/dsmorse/gridster.js/blob/adfd1ffce1f9077e1bb9f651ab8c4cc692e1b22b/src/jquery.gridster.js#L986-L1026
         */

        let lastWidget = this.widgets[this.widgets.length - 1];
        let col = 0;
        let row = 0;
        let columns = 8; //gridsterColumns;   //FIXME

        if (!lastWidget) {
            // No widgets added yet; happy with the defaults;
        } else if (lastWidget.col >= columns - 1 || lastWidget.col + lastWidget.sizeX + nextWidget.sizeX > columns) {
            const reducer = (lastWidgetRow: number, widget: IWidget) => {
                if (widget.col === 0 && widget.row >= lastWidgetRow) return (lastWidgetRow = widget.row + widget.sizeY);
                else return lastWidgetRow;
            };

            col = 0;
            row = this.widgets.reduce<number>(reducer, 0);
        } else {
            col = lastWidget.col + lastWidget.sizeX;
            row = lastWidget.row;
        }

        nextWidget.col = col;
        nextWidget.row = row;
        nextWidget.colDefault = col;
        nextWidget.rowDefault = row;
        return nextWidget;
    }

    private loadServiceTileWidgets(user: IUser): Promise<void> {
        if (!loadForCustomer(user)) return Promise.resolve();

        // Add widgets for services with subscriptions.
        return Promise.all([
            this.Subscriptions.query(user.customer).toPromise(),
            this.Services.getAll().toPromise(),
            this.Services.getFindingSeverityInfo().toPromise(),
        ]).then(([subscriptions, services, severities]: [any[], any[], any[]]) => {
            this.svcMap = services.reduce((map, e) => (map[e.id] = e) && map, {});
            const severityMap = severities.reduce((map, e) => (map[e.service] = e) && map, {});
            for (let sub of subscriptions) {
                const expired = new Date(sub.expiration_date) < new Date();

                const svc = this.svcMap[sub.service];
                const svcSeverities = severityMap[sub.service];
                svc.totalFindings = svcSeverities.findings; // Not even used anymore?
                svc.max_severity = svcSeverities.max_severity;
                svc.last_max_added = svcSeverities.last_max_added;
                svc.latest_finding = svcSeverities.latest_finding;
                svc.severity_counts = svcSeverities.severity_counts;

                // $scope.data.services[svc.id] = svc;
                this.addWidget({
                    id: 'service-tile-' + svc.short_name,
                    version: 1,
                    isAngularComponent: false,
                    templateUrl: '/app/dashboard/widgets/singleService/svcWidgetTmpl.html',
                    component: null,
                    title: svc.name,
                    resize: false,
                    gridsterClasses: expired ? ['bg-gray-striped'] : [svc.short_name.toLowerCase()],
                    service: svc,
                    expired,
                    update: () => null,
                    shouldLoad: loadForCustomer,
                    description: null,
                });
            }
        });
    }

    public getServiceInfoMap() {
        return this.svcMap;
    }
}

let allWidgets: IWidgetParams[] = [
    {
        id: 'findings-by',
        version: 1,
        templateUrl: '/app/dashboard/widgets/findingsBy/findingsByTmpl.html',
        title: 'Your Findings',
        gridsterClasses: ['findingsByData', 'graph-link'],
        sizeX: 4,
        sizeY: 2,
        isAngularComponent: false,
        component: null,
        update: () => null,
        shouldLoad: loadForCustomer,
        description: null,
    },
    {
        id: 'todo',
        version: 1,
        templateUrl: '/app/dashboard/widgets/todo/todoTmpl.html',
        title: 'To Do',
        sizeX: 4,
        sizeY: 2,
        isAngularComponent: false,
        component: null,
        update: () => null,
        shouldLoad: loadForOperator,
        description: null,
    },
    {
        id: 'opsActivity',
        version: 1,
        templateUrl: '/app/dashboard/widgets/opsActivity/opsActivityLoader.html',
        title: 'Customer Ops Activity',
        sizeX: 8,
        sizeY: 4,
        isAngularComponent: false,
        component: null,
        update: () => null,
        shouldLoad: loadForOperator,
        description: null,
    },
    {
        id: 'target-count',
        version: 1,
        //ctrl: '/app/dashboard/widgets/targetCount/TargetCountCtrl',
        templateUrl: '/app/dashboard/widgets/targetCount/targetCountTmpl.html',
        title: 'Target Count',
        gridsterClasses: ['bg-orange'],
        resize: false,
        isAngularComponent: false,
        component: null,
        update: () => null,
        shouldLoad: loadForCustomer,
        description: null,
    },
    {
        id: 'completed-operations-count',
        version: 1,
        //ctrl: '/app/dashboard/widgets/completedOps/CompletedOpsCtrl',
        templateUrl: '/app/dashboard/widgets/completedOps/completedOpsTmpl.html',
        title: 'Completed Operations',
        gridsterClasses: ['bg-teal'],
        resize: false,
        isAngularComponent: false,
        component: null,
        update: () => null,
        shouldLoad: loadForCustomer,
        description: null,
    },
    // {
    //     id: 'tools',
    //     version: 1,
    //     //ctrl: '/app/dashboard/widgets/completedOps/CompletedOpsCtrl',
    //     templateUrl: "",
    //     title: 'Tools',
    //     gridsterClasses: [],
    //     sizeX: 4,
    //     sizeY: 5,
    //     resize: false,
    //     isAngularComponent: true,
    //     component: ToolsWindow,
    //     update: () => null,
    //     shouldLoad: loadForCustomer,
    // },
    {
        id: 'cyvar',
        version: 1,
        //ctrl: '/app/dashboard/widgets/cyvar/CyvarCtrl',
        templateUrl: '/app/dashboard/widgets/cyvar/cyvarTmpl.html',
        title: 'Your CyVaR calculated expected loss',
        sizeX: 4,
        sizeY: 2,
        isAngularComponent: false,
        component: null,
        update: () => null,
        shouldLoad: (userData) => loadForCustomer(userData) && userData.username === 'kgumtow',
        description: null,
    },
    {
        id: 'target-count',
        version: 1,
        templateUrl: '/app/dashboard/widgets/targetCount/targetCountTmpl.html',
        title: 'Target Count',
        gridsterClasses: ['bg-orange'],
        resize: false,
        isAngularComponent: false,
        component: null,
        update: () => null,
        shouldLoad: loadForCustomer,
        description: null,
    },
    {
        id: 'target-types',
        version: 1,
        templateUrl: '/app/dashboard/widgets/targetTypes/targetTypesTmpl.html',
        title: 'Target Types Widget',
        sizeX: 3,
        sizeY: 2,
        gridsterClasses: ['graph-link'],
        isAngularComponent: false,
        component: null,
        update: () => null,
        shouldLoad: loadForCustomer,
        description: null,
    },
    {
        id: 'most-vulnerable-target',
        version: 1,
        templateUrl: '/app/dashboard/widgets/mvt/mvtTmpl.html',
        title: 'Most Vulnerable',
        sizeX: 4,
        sizeY: 2,
        isAngularComponent: false,
        component: null,
        update: () => null,
        shouldLoad: loadForCustomer,
        description: null,
    },
    {
        id: 'score-history',
        version: 1,
        templateUrl: '/app/dashboard/widgets/scoreHistory/score-history-loader.html',
        title: 'Finding Discovery and Resolution',
        sizeX: 4,
        sizeY: 2,
        isAngularComponent: false,
        component: null,
        update: () => null,
        shouldLoad: loadForCustomer,
        description: null,
    },
    {
        id: 'phishing-pie-list',
        version: 1,
        templateUrl: '/app/dashboard/widgets/phishing/phishingTmpl.html',
        title: 'Phishing Widget',
        sizeX: 4,
        sizeY: 2,
        isAngularComponent: false,
        component: null,
        update: () => null,
        shouldLoad: loadForCustomer,
        description: null,
    },

    achievementProgressWidget,
    domainProgressWidget,
    readinessLevelWidget,
    companySizeWidget,
    industryBreakdownWidget,
    manufacturerBreakdownWidget,
    stateBreakdownWidget,
    programParticipationWidget,
    contractTypesWidget,
    businessCategoriesWidget,
];
