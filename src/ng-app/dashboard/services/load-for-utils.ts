import { IUser } from '../../global-data/models/user.interface';
import { ShouldLoadWidgetFunction } from '../models/widget.interface';

//TODO: (CATO-2253) make enum for user role strings
export const loadForCustomer: ShouldLoadWidgetFunction = (userData: IUser) => {
    return userData.roles.includes('customer');
};
export const loadForOperator: ShouldLoadWidgetFunction = (userData: IUser) => {
    return userData.roles.includes('operator'); // This includes MDs because all MDs are operators
};
export const loadForMissionDirector: ShouldLoadWidgetFunction = (userData: IUser) => {
    return userData.roles.includes('mission_director');
};
export const loadForInsight: ShouldLoadWidgetFunction = (userData: IUser) => {
    return userData.roles.includes('insight');
};
