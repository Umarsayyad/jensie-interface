import { AfterViewInit, Component, OnDestroy, ViewChild, ViewContainerRef } from '@angular/core';
import catotheme from '@cato/config/themes/catotheme';
import { ValidTheme } from '@cato/config/themes/valid-themes';
import { ɵb as InjectionService } from '@swimlane/ngx-charts';
import { GridsterComponent } from 'angular2gridster';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { GlobalDataService } from '../../ngjs-upgrade/globalDataService';
import { gridsterOpts } from '../data/gridster-options';
import { IWidget } from '../models/widget.interface';
import { DashboardWidgets } from '../services/dashboard-widgets.service';
import { SidebarService } from '../services/sidebar.service';

@Component({
    selector: 'cato-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.less'],
})
export class DashboardComponent implements AfterViewInit, OnDestroy {
    @ViewChild(GridsterComponent)
    gridster: GridsterComponent;

    readonly gridsterOptions = gridsterOpts;
    readonly isInsight = catotheme.name === ValidTheme.jensie && this.gdata.isInsight;

    widgets: IWidget[] = [];

    private onDestroy$ = new Subject();

    currentPage = 1;
    status = {};
    service = {};
    results = {};
    hiddenServices = [];
    showArchived = false;

    data = {
        findings: [],
        rows: [],
        services: {},
        showSelectCustomer: false,
    };

    // Have to set this here due to timing issues
    gridsterColumns = 8;

    constructor(
        private dbWidgets: DashboardWidgets,
        private sidebarService: SidebarService,
        private gdata: GlobalDataService,
        vcr: ViewContainerRef,
        tooltipInjectorService: InjectionService
    ) {
        if (this.isInsight) {
            this.gridsterOptions = Object.assign({}, gridsterOpts);
            this.gridsterOptions.dragAndDrop = false;
            this.gridsterOptions.resizable = false;
        } else {
            this.gridsterOptions = gridsterOpts;
        }

        tooltipInjectorService.setRootViewContainer(vcr);

        dbWidgets.load();
    }

    ngAfterViewInit() {
        this.sidebarService
            .getCollapsed()
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((_) => {
                setTimeout(() => this.gridster.reload(), 100); // TODO: figure out why delay is needed to keep gridster from staying too big on sidebar expand sometimes
            });

        this.dbWidgets
            .getWidgets()
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((widgets) => {
                this.widgets = widgets;
                setTimeout(() => this.gridster.reload(), 100);
            });
    }

    ngOnDestroy() {
        this.onDestroy$.next();
        this.onDestroy$.complete();
    }

    resetWidgets($event: { altKey: boolean; shiftKey: boolean }) {
        //do a hard reset if alt+shift+clicking
        this.dbWidgets.resetWidgets($event.altKey && $event.shiftKey);
        this.gridster.reload();
    }

    showWidget(widget: IWidget) {
        this.dbWidgets.showWidget(widget);
        this.gridster.reload();
    }

    onReflow() {
        this.dbWidgets.updateWidgets(); // TODO: do we need to debounce calls to this?
    }

    onChange(widget: IWidget, data: { x?: number; y?: number; w?: number; h?: number }) {
        if (data.x || data.x === 0) widget.col = data.x;
        if (data.y || data.y === 0) widget.row = data.y;
        if (data.h) widget.sizeY = data.h;
        if (data.w) widget.sizeX = data.w;

        // Object.assign(widget, size);
        this.dbWidgets.updateWidget(widget);
    }
}

/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

// function oldDash($scope,
//     $rootScope,$q,$timeout,$uibModal,
//     FindingService,PendingItemsService,stateBuilder,
//     socketService,modalService,CustomerSubscriptions,
//     Operations,Services,Users)
// {
//     function updateSeverity(serviceId, severity) {
//         var max_severity = $scope.data.services[serviceId].max_severity;
//         var last_max_severity = $scope.data.services[serviceId].last_max_severity;
//         $scope.data.services[serviceId].max_severity = max_severity > severity ? max_severity : severity;
//         $scope.data.services[serviceId].last_max_severity = $scope.data.services[serviceId].max_severity;
//     }

//     function getFindingSeverities() {
//         $scope.data.findingSeveritiesLoaded = $q.defer();

//         $scope.data.loadFindingSeverities = Services.findings().$promise
//             .then(function(serviceFindingsWithMaxSeverity) {
//                 angular.forEach(serviceFindingsWithMaxSeverity, function(svc) {
//                     var id = svc.service;
//                     if (!$scope.data.services[id]) {
//                         $scope.data.services[id] = $rootScope.gdata.serviceMap[id];
//                     }
//                     $scope.data.services[id].totalFindings = svc.findings; // Not even used anymore?
//                     $scope.data.services[id].max_severity = svc.max_severity;
//                     $scope.data.services[id].last_max_added = svc.last_max_added;
//                     $scope.data.services[id].latest_finding = svc.latest_finding;
//                     $scope.data.services[id].severity_counts = svc.severity_counts;
//                 });
//                 $scope.data.findingSeveritiesLoaded.resolve($scope.data.services);
//             });
//     }

//     function loadToDoWidget() {
//         loadWidget({
//             id: 'todo',
//             version: 1,
//             tmpl: '/app/dashboard/widgets/todo/todoTmpl.html',
//             title: 'To Do',
//             sizeX: 4,
//             sizeY: 2,
//         });
//     }

//     function loadOpsActivityWidget() {
//         loadWidget({
//             id: 'opsActivity',
//             version: 1,
//             tmpl: '/app/dashboard/widgets/opsActivity/opsActivityLoader.html',
//             title: 'Customer Ops Activity',
//             sizeX: 8,
//             sizeY: 4,
//         });
//     }

//     function loadTargetCountWidget() {
//         loadWidget({
//             id: 'target-count',
//             version: 1,
//             //ctrl: '/app/dashboard/widgets/targetCount/TargetCountCtrl',
//             tmpl: '/app/dashboard/widgets/targetCount/targetCountTmpl.html',
//             title: 'Target Count',
//             gridsterClasses: ['bg-orange'],
//             resize: false,
//         });
//     }

//     function loadFindingsByWidget() {
//         loadWidget({
//             id: 'findings-by',
//             version: 1,
//             //ctrl: '/app/dashboard/widgets/findingsBy/FindingsByCtrl',
//             tmpl: '/app/dashboard/widgets/findingsBy/findingsByTmpl.html',
//             title: 'Your Findings',
//             gridsterClasses: ['findingsByData', 'graph-link'],
//             sizeX: 4,
//             sizeY: 2,
//             chart: {
//                 api: {}
//             }
//         });
//     }

//     function loadCompletedOpsWidget() {
//         loadWidget({
//             id: 'completed-operations-count',
//             version: 1,
//             //ctrl: '/app/dashboard/widgets/completedOps/CompletedOpsCtrl',
//             tmpl: '/app/dashboard/widgets/completedOps/completedOpsTmpl.html',
//             title: 'Completed Operations',
//             gridsterClasses: ['bg-teal'],
//             resize: false,
//         });
//     }

//     function loadPhishingWidget() {
//         // Verify that the user has access to the phishing service, and if so, then go ahead and load the data
//         if (!_.isNil(_.find($scope.data.services, {short_name: 'socialeng'}))) {
//             // We use the title in the widget, and want it to match, so load it into scope
//             $scope.data.phishingWidgetTitle = "Phishing Widget";
//             loadWidget({
//                 id: 'phishing-pie-list',
//                 version: 1,
//                 //ctrl: '/app/dashboard/widgets/phishing/PhishingCtrl',
//                 tmpl: '/app/dashboard/widgets/phishing/phishingTmpl.html',
//                 title: $scope.data.phishingWidgetTitle,
//                 sizeX: 4,
//                 sizeY: 2,
//                 chart: {api: {}},
//             });
//         }
//     }

//     function loadTargetTypesWidget() {
//         $scope.data.targetTypesWidgetTitle = "Target Types Widget";
//         loadWidget({
//             id: 'target-types',
//             version: 1,
//             tmpl: '/app/dashboard/widgets/targetTypes/targetTypesTmpl.html',
//             title: $scope.data.targetTypesWidgetTitle,
//             sizeX: 3,
//             sizeY: 2,
//             gridsterClasses: ['graph-link'],
//             chart: {api: {}},
//         });
//     }

//     function loadMostVulnerableTargetWidget() {
//         loadWidget({
//             id: 'most-vulnerable-target',
//             version: 1,
//             //ctrl: '/app/dashboard/widgets/mvt/MvtCtrl',
//             tmpl: '/app/dashboard/widgets/mvt/mvtTmpl.html',
//             title: 'Most Vulnerable',
//             sizeX: 4,
//             sizeY: 2,
//             chart: {api: {}},
//         });
//     }

//     function loadCyVaRWidget() {
//         if ($rootScope.user.username === 'kgumtow') {
//             loadWidget({
//                 id: 'cyvar',
//                 version: 1,
//                 //ctrl: '/app/dashboard/widgets/cyvar/CyvarCtrl',
//                 tmpl: '/app/dashboard/widgets/cyvar/cyvarTmpl.html',
//                 title: 'Your CyVaR calculated expected loss',
//                 sizeX: 4,
//                 sizeY: 2,
//             });
//         }
//     }

//     function loadWidgets() {
//         // CATO-1174 changed some of the widget data, but widgets created earlier will still have the old format
//         // detect that and hard-reset before continuing to load the widgets so they aren't broken
//         _.each($scope.widgets, function(widget) {
//             if (_.includes(widget.templateUrl, '/app/common/widgets/')) {
//                 $scope.widgets = [];
//                 $rootScope.user.ui_data.widgets = $scope.widgets;
//                 Users.patch({userId: $rootScope.user.id}, {ui_data: $rootScope.user.ui_data}, function(user) {
//                     $rootScope.updateObjOld($rootScope.user, user);
//                 }, console.error);
//                 return false;
//             }
//         });

//         if ($scope.gdata.isOperator) {
//             loadToDoWidget();
//             loadOpsActivityWidget();
//         }
//         else {
//             loadServiceTileWidgets();
//             loadTargetCountWidget();
//             loadCompletedOpsWidget();
//             loadTargetTypesWidget();
//             loadMostVulnerableTargetWidget();
//             loadPhishingWidget();
//             loadFindingsByWidget();
//             loadCyVaRWidget();
//         }
//     }

//     function updateSeverity(serviceId, severity) {  // TODO: support this in new dashboard
//         var max_severity = $scope.data.services[serviceId].max_severity;
//         var last_max_severity = $scope.data.services[serviceId].last_max_severity;
//         $scope.data.services[serviceId].max_severity = max_severity > severity ? max_severity : severity;
//         $scope.data.services[serviceId].last_max_severity = $scope.data.services[serviceId].max_severity;
//     }
//
//     if ($scope.gdata.isCustomer) {
//         var findingUnsubscribe = socketService.subscribeByModel('finding', function(finding) {
//             //console.log('got FINDING data back for ' + $rootScope.catotheme.customer + ' (DashboardCtrl): ', finding);
//             if (finding.deleted) {
//                 _.remove($scope.data.findings[customerId], {id: finding.id});
//                 return;
//             }
//             if (finding.archived !== true) {
//                 $rootScope.newItemCounts.findings = $rootScope.newItemCounts.findings + 1;
//             }
//
//             //updateSeverity(finding.service, finding.severity);  // TODO: support this in new dashboard
//         });
//
//         $scope.$on("$destroy", function() {
//             findingUnsubscribe();
//         });
//
//     }

//     // Create/load widgets for customers
//     if ($scope.user.type === 'customer') {
//         $q.all([$scope.gdata.customer.$promise,
//         $scope.gdata.services.$promise
//         ]).then(function() {
//             $scope.customer = $scope.gdata.customer;
//             getFindingSeverities();
//             loadWidgets();
//             // No longer pull all findings
//             //FindingService.getFindingDetails();
//         });
//     }
//     else {
//         loadWidgets();

//         $scope.custServices = $rootScope.services;

//         $scope.showFollowOnOperationLink = true;
//         $scope.addOperation = function(operation, finding) {
//             if (!operationsConfig) operationsConfig = Operations.options();

//             operation.customer = finding.customer;
//             var data = {
//                 operation: operation,
//                 finding: finding,
//                 config: operationsConfig.$promise,
//             };

//             var modal = stateBuilder.componentModalGenerator('modal-add-operation', stateBuilder.operationModalBindings, data, 'operation/modalAddOperation', 'followUpOpCustomizer');
//             $uibModal.open(modal);
//         };

//         $scope.archiveFinding = function(finding) {
//             // Sockets handle updating
//             FindingService.archiveFinding(finding).$promise;
//         };

//         $scope.unarchiveFinding = function(finding) {
//             // Sockets handle updating
//             FindingService.unarchiveFinding(finding).$promise;
//         };
//     }
// });
