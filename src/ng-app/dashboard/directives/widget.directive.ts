import { ComponentFactoryResolver, Directive, Input, Type, ViewContainerRef, Injector, InjectionToken } from '@angular/core';

@Directive({
    selector: '[widgetcomp]',
})
export class WidgetDirective {
    constructor(private componentFactoryResolver: ComponentFactoryResolver, private viewContainerRef: ViewContainerRef, private injector: Injector) {}

    @Input() set widgetcomp(component: Type<any> | InjectionToken<any>) {
        if (!component) throw new Error('Error: component is falsey');

        if (component instanceof InjectionToken) {
            // TODO: Is there a better way to do this?
            component = this.injector.get<Type<any>>(component);
            if (!component) throw new Error(`Error: could not find InjectionToken: ${component}`);
            this.createComponent(component);
        } else if (component instanceof Type) {
            this.createComponent(component);
        } else {
            //log the error but don't throw
            console.error('component parameter is neither a Type nor an InjectionToken', component);
        }
    }

    private createComponent(component: Type<any>) {
        const componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);
        const componentRef = this.viewContainerRef.createComponent(componentFactory);
    }
}
