import { Directive, ElementRef, Injector, Input } from '@angular/core';
import { UpgradeComponent } from '@angular/upgrade/static';

import { IWidget } from '../models/widget.interface';

//Tell Angular what "widget-wrapper" is by creating a directive
@Directive({
    selector: 'widget-wrapper',
})
export class WidgetWrapperDirective extends UpgradeComponent {
    constructor(elementRef: ElementRef, injector: Injector) {
        super('widgetWrapper', elementRef, injector);
    }

    @Input() set templateUrl(url: string) {}
    @Input() set widget(w: IWidget) {}
}
