import { IGridsterOptions } from 'angular2gridster';

let gridsterColumns = 8;

export const gridsterOpts: IGridsterOptions = {
    // columns: gridsterColumns,    //TODO: set lanes to correct value
    lanes: gridsterColumns,
    direction: 'vertical',

    // pushing: true,
    floating: true,
    // swapping: true,

    // width: "auto",
    // colWidth: "auto",
    // rowHeight: "match",
    widthHeightRatio: 1,

    // margins: [25, 25],
    // outerMargin: false,
    // defaultSizeX: 1,
    // defaultSizeY: 1,
    // minColumns: 1,
    // minRows: 2,

    //mobileBreakPoint: 800,
    // mobileModeEnabled: false,
    responsiveView: true,
    responsiveSizes: false,

    dragAndDrop: true,
    // draggableOptions: {handlerClass: ".gridster-item-drag"},
    resizable: true,
    resizeHandles: { e: true, s: true, w: true, nw: true, se: true, sw: true },

    shrink: true,
    useCSSTransforms: true,
    lines: {
        visible: true,
        color: '#afafaf',
        width: 2,
    },
};
