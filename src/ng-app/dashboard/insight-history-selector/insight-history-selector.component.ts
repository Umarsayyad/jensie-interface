import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import * as moment from 'moment';

import { LoadingContentComponent } from '../../common/components';
import { InsightWidgetsService } from '../widgets/insight-graphs/insight-widgets.service';
import { ProgressHistory } from '../widgets/insight-graphs/models/insight/progress-history.namespace';

@Component({
    selector: 'insight-history-selector',
    templateUrl: './insight-history-selector.component.html',
    styleUrls: ['./insight-history-selector.component.less'],
})
export class InsightHistorySelectorComponent implements AfterViewInit {
    private historyEntryMap = new Map<string, ProgressHistory.IEntry>();
    maxHistoryDate = moment.utc().subtract(1, 'day');
    minHistoryDate = this.maxHistoryDate;
    historyDate = new FormControl(null);
    validDateFormatMessage = `Valid formats are ${moment.localeData().longDateFormat('l')} and YYYY-MM-DD.`;

    @ViewChild('spinner') spinner: LoadingContentComponent;

    availableHistoryDateFilter = (m: moment.Moment | null): boolean => {
        return this.historyEntryMap.has(getYearString(m));
    };

    constructor(private insightWidgetsService: InsightWidgetsService) {}

    ngAfterViewInit() {
        this.spinner.startLoading();
        const cur = this.insightWidgetsService.getCurrentEntry();
        if (cur !== null) {
            this.historyDate.setValue(moment.utc(cur.timestamp));
        }
        this.insightWidgetsService.getHistoryEntries().then((entries) => {
            for (const entry of entries) {
                const m = moment.utc(entry.timestamp);
                if (m.isBefore(this.minHistoryDate)) {
                    this.minHistoryDate = m;
                }
                this.historyEntryMap.set(getYearString(m), entry);
            }
            this.spinner.stopLoading();
        });
    }

    dateSelected() {
        if (this.historyDate.valid) {
            this.spinner.startLoading();
            let loading: Promise<void>;
            if (this.historyDate.value === null) {
                loading = this.insightWidgetsService.loadHistoryEntry(null);
            } else {
                loading = this.insightWidgetsService.loadHistoryEntry(this.historyEntryMap.get(getYearString(this.historyDate.value)));
            }
            loading.finally(() => this.spinner.stopLoading());
        }
    }
}

function getYearString(m: moment.Moment) {
    return m.format('YYYY-MM-DD');
}
