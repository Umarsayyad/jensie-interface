import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { EventType } from 'crossfilter2';
import { ReplaySubject, Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';

import { LoadingContentComponent } from '../../common';
import { LoadInsightSearchComponent } from '../../insight/load-insight-search/load-insight-search.component';
import { IInsightServiceData } from '../../insight/models/insight-service-data.interface';
import { SaveInsightSearchComponent } from '../../insight/save-insight-search/save-insight-search.component';
import { KeyValue, KeyValueHtml } from '../models/key-value';
import { IWidget } from '../models/widget.interface';
import { DashboardWidgets } from '../services/dashboard-widgets.service';
import { InsightWidgetsService, LoadProgress } from '../widgets/insight-graphs/insight-widgets.service';
import { CategoryDimensionService } from '../widgets/insight-graphs/services/category-dimension.service';
import { ContractDimensionService } from '../widgets/insight-graphs/services/contract-dimension.service';
import { DimensionService } from '../widgets/insight-graphs/services/dimension-service';
import { IndustryDimensionService } from '../widgets/insight-graphs/services/industry-dimension.service';
import { LevelDimensionService } from '../widgets/insight-graphs/services/level-dimension.service';
import { ManufacturerDimensionService } from '../widgets/insight-graphs/services/manufacturer-dimension.service';
import { ParticipationDimensionService } from '../widgets/insight-graphs/services/participation-dimension.service';
import { SizeDimensionService } from '../widgets/insight-graphs/services/size-dimension.service';
import { StateDimensionService } from '../widgets/insight-graphs/services/state-dimension.service';
import { InsightFiltersService } from './insight-filters.service';

type DimensionServiceStringOrNum = DimensionService<string>;
type FilterData = { name: string; dimService: DimensionServiceStringOrNum; subject: Subject<Array<KeyValueHtml>>; placeholder: string };

@Component({
    selector: 'insight-filters',
    templateUrl: './insight-filters.component.html',
    styleUrls: ['./insight-filters.component.less'],
})
export class InsightFiltersComponent implements OnInit, AfterViewInit, OnDestroy {
    multiselects = new FormGroup({});
    readonly pickerSuffix = 'Picker';
    readonly filterSuffix = 'Filter';
    activeFilters = new Set<string>();
    private widgets: IWidget[];
    private onDestroy$ = new Subject();

    @ViewChild('spinner') spinner: LoadingContentComponent;

    constructor(
        private categoriesDimService: CategoryDimensionService,
        private contractDimService: ContractDimensionService,
        private industryDimService: IndustryDimensionService,
        private levelDimService: LevelDimensionService,
        private manufacturerDimService: ManufacturerDimensionService,
        private participationDimService: ParticipationDimensionService,
        private sizeDimService: SizeDimensionService,
        private stateDimService: StateDimensionService,

        private dbWidgets: DashboardWidgets,
        private filtersService: InsightFiltersService,
        private widgetManager: InsightWidgetsService,
        private dialog: MatDialog
    ) {}

    ngOnInit() {
        for (let obj of this.data) {
            //set up subjects
            obj.subject = new ReplaySubject<Array<KeyValueHtml>>(1);

            //set up form fields
            this.multiselects.addControl(obj.name + this.pickerSuffix, new FormControl());
            this.multiselects.addControl(obj.name + this.filterSuffix, new FormControl());
        }
    }

    ngAfterViewInit() {
        this.spinner.startLoading();

        this.dbWidgets
            .getWidgets()
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((widgets) => (this.widgets = widgets));

        this.widgetManager.getNdx().then((ndx) => {
            for (let { name, dimService, subject } of this.data) {
                this.initializeFilters(name, dimService as DimensionService<string>, subject);
            }

            this.spinner.stopLoading();

            // Don't subscribe after the component is unloaded (possible if the user leaves the tab before any requests complete)
            if (!this.onDestroy$.isStopped) {
                this.widgetManager
                    .loadProgress()
                    .pipe(takeUntil(this.onDestroy$))
                    .subscribe((status) => {
                        if (status === LoadProgress.Started) {
                            this.spinner.startLoading();
                        } else if (status === LoadProgress.LevelLoaded) {
                            this.spinner.stopLoading();
                        }
                    });
            }
        });
    }

    ngOnDestroy() {
        //clean up subscriptions/subjects
        this.onDestroy$.next();
        this.onDestroy$.complete();

        for (let { subject } of this.data) subject.complete();
    }

    clearFilters(dimService: DimensionService<any>) {
        dimService.setFilters([]);
    }

    saveFilters() {
        const data: IInsightServiceData = { service: this.filtersService };
        this.dialog.open(SaveInsightSearchComponent, { width: '50vw', panelClass: 'gray-bg', disableClose: false, data });
    }

    loadFilters() {
        const data: IInsightServiceData = { service: this.filtersService };
        this.dialog.open(LoadInsightSearchComponent, { width: '50vw', panelClass: 'gray-bg', disableClose: false, data });
    }

    private initializeFilters(name: string, dimService: DimensionService<string>, subject: Subject<Array<KeyValueHtml>>) {
        subject.next(dimService.currentData());

        //listen for multiselect field value changes
        this.multiselects
            .get(name + 'Picker')
            .valueChanges.pipe(takeUntil(this.onDestroy$))
            .subscribe((filters: any[]) => {
                dimService.setFilters(filters);

                for (let w of this.widgets) w.update && w.update();
            });

        //listen for search field value changes
        this.multiselects
            .get(name + this.filterSuffix)
            .valueChanges.pipe(takeUntil(this.onDestroy$), debounceTime(150))
            .subscribe((val: string) => {
                subject.next(this.filterMultiselectData(dimService.currentData(), val));
            });

        dimService
            .filtersChange()
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((filters) => {
                this.multiselects.get(name + this.pickerSuffix).setValue(filters, { emitEvent: false });

                if (filters.length) this.activeFilters.add(name);
                else this.activeFilters.delete(name);
            });

        dimService
            .crossfiltersChange()
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((_: EventType) => {
                const searchTerm = this.multiselects.get(name + this.filterSuffix).value || '';
                subject.next(this.filterMultiselectData(dimService.currentData(), searchTerm));
            });
    }

    private filterMultiselectData(dataToFilter: KeyValue[], searchTerm: string): KeyValueHtml[] {
        const searchPatternStr = searchTerm
            .toLowerCase()
            .replace(/[.*+\-?^${}()|[\]\\]/g, '(\\$&).*') //escape any regex special characters
            .replace(/[^.*+\-?^${}()|[\]\\]/g, '($&).*'); //allow any characters between letters in the search term (ignore special chars that were matched above)

        const searchPattern = new RegExp(searchPatternStr, 'i');

        //filter the stuff and return
        return dataToFilter.filter((e) => searchPattern.test(e.key)).map((e) => mapper(e));

        //identify matches in the element's key via bold and underline
        function mapper(e: KeyValue): KeyValueHtml {
            let indices = [];

            //find indices
            for (let char of searchTerm.toLowerCase()) {
                let start = indices[indices.length - 1] + 1 || 0;
                indices.push(e.key.toLowerCase().indexOf(char, start));
            }

            let keyArray = e.key.split('');
            for (let i of indices.reverse()) {
                //insert bold and underline tags around the matched letter
                keyArray.splice(i + 1, 0, '</u></b>');
                keyArray.splice(i, 0, '<b><u>');
            }

            return { key: e.key, value: e.value, html: `${keyArray.join('')} &nbsp; (${e.value})` };
        }
    }

    //TODO (CATO-2265): fix labels to match chart titles
    //TODO (CATO-2265): put these in some sort of order?
    data: FilterData[] = [
        {
            name: 'state',
            dimService: this.stateDimService,
            subject: null,
            placeholder: 'States',
        },
        {
            name: 'categories',
            dimService: this.categoriesDimService,
            subject: null,
            placeholder: 'Categories',
        },
        {
            name: 'size',
            dimService: this.sizeDimService,
            subject: null,
            placeholder: 'Company Sizes',
        },
        {
            name: 'contracts',
            dimService: this.contractDimService,
            subject: null,
            placeholder: 'Contract Types',
        },
        {
            name: 'industry',
            dimService: this.industryDimService,
            subject: null,
            placeholder: 'Industries',
        },
        {
            name: 'manufacturer',
            dimService: this.manufacturerDimService,
            subject: null,
            placeholder: 'Manufacturer Types',
        },
        {
            name: 'participation',
            dimService: this.participationDimService,
            subject: null,
            placeholder: 'Programs',
        },
        {
            name: 'level',
            dimService: this.levelDimService,
            subject: null,
            placeholder: 'CMMC Levels',
        },
    ];
}
