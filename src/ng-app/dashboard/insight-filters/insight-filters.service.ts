import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { pairwise, startWith, takeUntil } from 'rxjs/operators';

import { IUser, UserPrefData } from '../../global-data/models/user.interface';
import { UserResource } from '../../global-data/user.resource';
import { IInsightSearchService } from '../../insight/models/insight-search-service.interface';
import { InsightSearch, ISearchMetadata, SavedSearches } from '../../insight/models/saved-search.interface';

import { CategoryDimensionService } from '../widgets/insight-graphs/services/category-dimension.service';
import { ContractDimensionService } from '../widgets/insight-graphs/services/contract-dimension.service';
import { DimensionService } from '../widgets/insight-graphs/services/dimension-service';
import { IndustryDimensionService } from '../widgets/insight-graphs/services/industry-dimension.service';
import { LevelDimensionService } from '../widgets/insight-graphs/services/level-dimension.service';
import { ManufacturerDimensionService } from '../widgets/insight-graphs/services/manufacturer-dimension.service';
import { ParticipationDimensionService } from '../widgets/insight-graphs/services/participation-dimension.service';
import { SizeDimensionService } from '../widgets/insight-graphs/services/size-dimension.service';
import { StateDimensionService } from '../widgets/insight-graphs/services/state-dimension.service';

/* eslint-disable @typescript-eslint/camelcase */
//do not provide in root -- we want this scoped to Insight widgets
@Injectable()
export class InsightFiltersService implements IInsightSearchService, OnDestroy {
    private onDestroy$ = new Subject();
    private allServices: Record<string, DimensionService<string>> = {};
    private allFilters: InsightSearch = {};

    private getUserData: Promise<UserPrefData>;
    private currentlyLoadedSearchIndex$ = new BehaviorSubject<number | null>(null);
    private currentlyLoadedSearch$ = new BehaviorSubject<ISearchMetadata | null>(null);
    private filterChanged$ = new BehaviorSubject<boolean | null>(null);

    constructor(
        private user: UserResource,

        categories: CategoryDimensionService,
        contracts: ContractDimensionService,
        industries: IndustryDimensionService,
        levels: LevelDimensionService,
        manufacturers: ManufacturerDimensionService,
        participation: ParticipationDimensionService,
        sizes: SizeDimensionService,
        states: StateDimensionService
    ) {
        this.allServices = {
            categories,
            contracts,
            industries,
            levels,
            manufacturers,
            participation,
            sizes,
            states,
        };

        this.init();
    }

    private init() {
        //retrieve stored searches from `ui_data`
        this.getUserData = this.user.get('me').toPromise().then(this.mapUserToUiData);

        //detect when other preferences change and update internal data
        this.user
            .uiDataChanges()
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((userData: UserPrefData) => {
                if (!userData.uiData.savedSearches) userData.uiData.savedSearches = [];
                this.getUserData = Promise.resolve(userData);
            });

        //set up filter listeners
        for (let [dimension, service] of Object.entries(this.allServices)) {
            service
                .filtersChange()
                .pipe(takeUntil(this.onDestroy$))
                .pipe(startWith(null)) //tslint:disable-line:deprecation
                .pipe(pairwise())
                .subscribe(([filtersPrevious, filtersCurrent]) => {
                    //store the current filters in allFilters
                    this.allFilters[dimension] = filtersCurrent;
                    if (filtersPrevious && filtersCurrent && JSON.stringify(filtersPrevious) !== JSON.stringify(filtersCurrent)) {
                        this.filterChanged$.next(true);
                    }
                    if (Object.values(this.allFilters).every((filters) => filters.length == 0)) {
                        this.currentlyLoadedSearchIndex$.next(null);
                        this.currentlyLoadedSearch$.next(null);
                    }
                });
        }
    }

    ngOnDestroy() {
        this.onDestroy$.next();
        this.onDestroy$.complete();
    }

    public saveSearch(index: number, name: string): Promise<IUser> {
        return this.getUserData.then(({ userId, uiData }) => {
            const search = { ...this.allFilters };
            const lastModified = Date.now().valueOf();

            uiData.savedSearches[index] = { name, search, lastModified };
            this.filterChanged$.next(false);
            return this.user
                .patch({ id: userId, ui_data: uiData })
                .toPromise()
                .then((obj) => {
                    this.currentlyLoadedSearchIndex$.next(index);
                    this.currentlyLoadedSearch$.next(uiData.savedSearches[index]);
                    return obj;
                });
        });
    }

    public getSavedSearches(): Promise<SavedSearches> {
        return this.getUserData.then(({ uiData }) => {
            const searches = uiData.savedSearches || [];
            return searches.sort((A, B) => A.lastModified - B.lastModified);
        });
    }

    public async loadSearch(index: number) {
        const loadedSearchMetadata: Readonly<ISearchMetadata> = (await this.getUserData).uiData.savedSearches[index];

        //if, in the future, filters are added or removed, the current supported filters
        //will be keys of this.allFilters
        for (let dimension of Object.keys(this.allFilters)) {
            this.allFilters[dimension] = { ...loadedSearchMetadata.search[dimension] };

            const filters = loadedSearchMetadata.search[dimension];
            this.allServices[dimension].setFilters(filters);
        }

        this.filterChanged$.next(false);
        this.currentlyLoadedSearchIndex$.next(index);
        this.currentlyLoadedSearch$.next(loadedSearchMetadata);
    }

    public deleteSearch(index: number) {
        return this.getUserData.then(({ userId, uiData }) => {
            uiData.savedSearches.splice(index, 1);

            const promise = this.user.patch({ id: userId, ui_data: uiData }).toPromise();
            this.getUserData = promise.then(this.mapUserToUiData);

            const current = this.currentlyLoadedSearchIndex$.value;

            //if user deleted the currently loaded filter
            if (current === index) {
                this.currentlyLoadedSearchIndex$.next(null);
                this.currentlyLoadedSearch$.next(null);
            } else if (current > index) {
                this.currentlyLoadedSearchIndex$.next(current - 1);
                this.currentlyLoadedSearch$.next(uiData.savedSearches[current - 1]);
            }

            return promise;
        });
    }

    public currentSearchIndexChanges(): Observable<number> {
        return this.currentlyLoadedSearchIndex$.asObservable();
    }

    public currentSavedSearchChanges(): Observable<ISearchMetadata> {
        return this.currentlyLoadedSearch$.asObservable();
    }

    public currentFilterChanges(): Observable<boolean> {
        return this.filterChanged$.asObservable();
    }

    private mapUserToUiData(user: IUser): UserPrefData {
        if (!user.ui_data.savedSearches) user.ui_data.savedSearches = [];
        return { userId: user.id, uiData: user.ui_data };
    }
}
/* eslint-enable @typescript-eslint/camelcase */
