import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ID } from '../../common/models';
import { Observable } from 'rxjs';
import { config } from '@cato/config';
import { INonDisclosure } from '../models/non-disclosure.interface';

@Injectable({ providedIn: 'root' })
export class NonDisclosureResource {
    protected get url() {
        return config.apiPath + 'customer';
    }

    constructor(private http: HttpClient) {}

    public downloadNda(customerId: ID, ndaId: ID): Promise<ArrayBuffer> {
        return this.http.get(`${this.url}/${customerId}/nda/${ndaId}/download_nda`, { responseType: 'arraybuffer' }).toPromise();
    }

    public list(customerId: ID): Observable<Array<INonDisclosure>> {
        return this.http.get<Array<INonDisclosure>>(`${this.url}/${customerId}/nda`);
    }

    public delete(customerId: ID, ndaId: ID): Observable<INonDisclosure> {
        return this.http.delete<INonDisclosure>(`${this.url}/${customerId}/nda/${ndaId}`);
    }
}
