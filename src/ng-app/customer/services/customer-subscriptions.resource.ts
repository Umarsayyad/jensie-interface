import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { config } from '@cato/config';
import { ID } from '../../common/models';

@Injectable({ providedIn: 'root' }) //FIXME //extends UserResource<any>
export class CustomerSubscriptions {
    protected get url() {
        return config.apiPath + 'customer';
    }

    constructor(private http: HttpClient) {}

    public get(customerId: ID, subId: ID): Observable<any> {
        return this.http.get(`${this.url}/${customerId}/subscription/${subId}`);
    }

    //prettier-ignore
    public query(customerId: ID): Observable<Array<any>> { //FIXME
        return this.http.get<Array<any>>(`${this.url}/${customerId}/subscription`);
    }
}

// .factory('CustomerSubscriptions', ['$resource', '$rootScope',
// function($resource, $rootScope) {
//     return $resource($rootScope.restUrl + 'customer/:customerId/subscription/:subscriptionId',
//         {customerId: '@customerId', subscriptionId: '@subscriptionId'}, {
//             add: {method: 'POST', params: {format: 'json'}},
//             get: {method: 'GET', params: {format: 'json'}},
//             update: {method: 'PUT', params: {format: 'json'}},
//             delete: {method: 'DELETE', params: {format: 'json'}},
//             query: {method: 'GET', params: {format: 'json'}, isArray: true},
//         });
// }])
