import { ID } from '../../common/models';

export interface INonDisclosure {
    id: ID;
    customer: ID;
    name: string;
    timestamp: string;
    type: string;
    size: number;
}

export interface INonDisclosureResult {
    deleted: Array<INonDisclosure>;
    uploaded: Array<INonDisclosure>;
}
