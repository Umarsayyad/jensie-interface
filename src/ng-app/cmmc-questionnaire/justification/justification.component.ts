import { Component, OnInit, Inject, Input, Optional } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'justification',
    templateUrl: 'justification.component.html',
    styleUrls: ['justification.component.less'],
})
export class JustificationComponent implements OnInit {
    data: any = {};

    @Input('data') inputData: string;

    constructor(@Optional() @Inject(MAT_DIALOG_DATA) private dialogData: string) {}

    ngOnInit() {
        this.data.justification = this.dialogData || this.inputData || '';
    }

    log(...args) {
        console.log(args);
    }
}
