import { AfterViewInit, Component, Inject, Input, OnInit, Optional, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';

import { QuestionnaireService } from '../services/questionnaire.service';
import { IEvidence } from '../models';
import { LoadingContentComponent } from '../../common/components/loading-content/loading-content.component';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ID } from '../../common';
import { DocType } from '../models/doc-type';

@Component({
    selector: 'evidence',
    templateUrl: 'evidence.component.html',
    styleUrls: ['evidence.component.less'],
})
export class EvidenceComponent implements OnInit, AfterViewInit {
    evidence: IEvidence[] = [];
    uploads: IEvidence[] = [];
    evidenceForm: FormArray = new FormArray([]);
    uploadForm: FormArray = new FormArray([]);
    forms: FormGroup = new FormGroup({ uploadForm: this.uploadForm, evidenceForm: this.evidenceForm });
    DocType = DocType;

    @Input('data') inputData: ID[];

    @ViewChild(LoadingContentComponent)
    spinner: LoadingContentComponent;

    constructor(private qService: QuestionnaireService, @Optional() @Inject(MAT_DIALOG_DATA) private dialogData: ID[]) {}

    ngOnInit() {
        let data = new Set(this.dialogData || this.inputData || []);

        this.qService.listEvidence().subscribe((evidence: IEvidence[]) => {
            //sort the evidence files by name
            this.evidence = evidence.sort((e1, e2) => e1.name.localeCompare(e2.name));

            for (let e of this.evidence) this.evidenceForm.push(new FormControl(!!data.has(e.id)));

            this.spinner.stopLoading();
        });
    }

    ngAfterViewInit() {
        //do this here because it's undefined in ngOnInit
        this.spinner.startLoading();
    }

    uploadFile(files: FileList) {
        for (let i = 0; i < files.length; i++) {
            let file = files[i];
            this.qService.uploadEvidence(file).subscribe((evidence) => {
                this.uploads.push(evidence);
                this.uploadForm.push(new FormControl(true));
            });
        }
    }

    download(evidence: IEvidence) {
        this.qService.downloadEvidenceAttachment(evidence);
    }

    done(): Set<ID> {
        //only keep evidence files whose form values are checked
        let ev = this.evidence.filter((e, i) => this.evidenceForm.value[i]).map<ID>((e) => e.id);
        let up = this.uploads.filter((e, i) => this.uploadForm.value[i]).map<ID>((e) => e.id);

        return new Set([...ev, ...up]);
    }

    log(...args) {
        console.log(args);
    }
}
