import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { computeProgress, IProgressDetail } from '../models/progress-detail.interface';

import { IAnswer, IQuestion, ISection, ISelfAssessment } from '../models/data-models';
import { IEvidence } from '../models';
import { QuestionnaireService } from '../services/questionnaire.service';

const range = (start, end) => [...Array(end - start + 1)].map((_, i) => start + i);

@Component({
    selector: 'review',
    templateUrl: './review.component.html',
    styleUrls: ['./review.component.less'],
})
export class ReviewComponent implements OnInit {
    progress: { [key: string]: IProgressDetail } = {};
    levels = [];
    questionsBySectionLevel: {};
    questionViews = { All: ['YES', 'NO', 'N/A', null], 'Needs work': ['NO', null] };
    questionFilterForm = new FormGroup({
        currentLevel: new FormControl(),
        currentQuestionView: new FormControl(),
    });

    @Input() assessment: ISelfAssessment;
    @Input() sections: ISection[];
    @Input() questions: IQuestion[];
    @Input() answers: IAnswer[];
    @Input() evidence: IEvidence[];

    private answersMap: Map<number, IAnswer>;
    private evidenceMap: Map<number, IEvidence>;

    constructor(private qService: QuestionnaireService) {}

    ngOnInit() {
        const initialQuestionView = 'Needs work';
        const initialLevelView = this.assessment.goalLevel;
        this.questionFilterForm.setValue({ currentLevel: initialLevelView, currentQuestionView: initialQuestionView });
        this.levels = range(1, initialLevelView);
        this.answersMap = this.groupBy(this.answers, 'question');
        this.evidenceMap = this.groupBy(this.evidence, 'id');

        this.questionsBySectionLevel = this.groupByNested(this.questions, initialQuestionView, initialLevelView);
        this.questionFilterForm.valueChanges.subscribe((change) => {
            this.questionsBySectionLevel = this.groupByNested(this.questions, change.currentQuestionView, change.currentLevel);
        });

        for (let section of this.sections) {
            this.progress[section.id] = computeProgress(section.progressDetail);
        }
    }

    getBackgroundColor(questionId: number) {
        if (this.answersMap[questionId]) {
            switch (this.answersMap[questionId].response) {
                case 'YES':
                    return '#78bf587d';
                case 'NO':
                    return '#bf58587d';
                case 'N/A':
                    return '#afacac';
            }
        }
        return '#fffab4';
    }

    hasChildren(section: { [key: number]: IQuestion[] }) {
        if (!section) return false;

        for (const [level, questions] of Object.entries(section)) {
            if (questions.length !== 0) {
                return true;
            }
        }
        return false;
    }

    download(evidenceId: number) {
        let evidence = this.evidenceMap[evidenceId];
        this.qService.downloadEvidenceAttachment(evidence);
    }

    private groupBy<T>(array: T[], key1: string): Map<number, T> {
        return array.reduce((result, currentValue) => {
            result[currentValue[key1]] = currentValue || {};
            return result;
        }, new Map<number, T>());
    }

    private groupByNested(array: IQuestion[], questionView, level): {} {
        const filteredQuestionResponses = this.questionViews[questionView];
        return array.reduce((result, currentQuestion) => {
            let questionSection = currentQuestion['section'];
            let questionLevel = String(currentQuestion['level']);
            let section = (result[questionSection] = result[questionSection] || {});
            let questionArray = (section[questionLevel] = section[questionLevel] || []);
            if (level !== undefined && currentQuestion['level'] > level) {
                return result;
            }
            if (
                questionView &&
                this.answersMap[currentQuestion.id] &&
                !filteredQuestionResponses.includes(this.answersMap[currentQuestion.id].response)
            ) {
                return result;
            }
            questionArray.push(currentQuestion);
            return result;
        }, {});
    }
}
