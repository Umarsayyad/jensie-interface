import { Component, Input, OnInit } from '@angular/core';

import { ID } from '../../common';
import { computeProgress, IProgressDetail } from '../models/progress-detail.interface';
import { QuestionnaireService } from '../services/questionnaire.service';
import { ISection, ISelfAssessment } from '../models/data-models';
import { StateService } from '@uirouter/angular';

@Component({
    selector: 'questionnaire',
    templateUrl: 'questionnaire.component.html',
    styleUrls: ['questionnaire.component.less'],
})
export class QuestionnaireComponent implements OnInit {
    private readonly helpKey = 'cmmcHideHelp';
    survey = false;
    currentSection: ID = null;
    progress: { [key: string]: IProgressDetail } = {};
    dismissed: boolean | 'true' | 'false' = null;

    @Input() assessment: ISelfAssessment;
    @Input() sections: ISection[];

    constructor(private qService: QuestionnaireService, private stateService: StateService) {}

    ngOnInit() {
        this.dismissed = sessionStorage.getItem(this.helpKey) as 'true' | 'false';
        if (this.dismissed == undefined) {
            sessionStorage.setItem(this.helpKey, 'false');
            this.dismissed = false;
        } else {
            this.dismissed = this.dismissed === 'true' ? true : false;
        }

        for (let section of this.sections) {
            this.progress[section.id] = computeProgress(section.progressDetail);
        }

        if (this.stateService.params.sectionIdentifier !== null) {
            const sectionIdentifier = this.stateService.params.sectionIdentifier;
            const targetSection = this.sections.find((item) => item.identifier === sectionIdentifier);
            if (targetSection) {
                this.openSection(targetSection.id);
            }
        }
    }

    dismiss() {
        sessionStorage.setItem(this.helpKey, 'true');
        this.dismissed = true;
    }

    openSection(id: ID) {
        this.survey = true;
        this.currentSection = id;
    }

    exitSection(data: any) {
        this.survey = false;
        this.qService.continueAssessment(this.assessment.id).subscribe((sections) => {
            this.sections = sections;
            for (let section of sections) {
                this.progress[section.id] = computeProgress(section.progressDetail);
            }
        });
    }
}
