import { Transition } from '@uirouter/core';

import { CmmcComponent } from '../cmmc.component';
import { CmmcDashboardComponent } from '../dashboard-component/cmmc-dashboard.component';
import { QuestionnaireComponent } from '../questionnaire-component/questionnaire.component';
import { QuestionnaireService } from '../services/questionnaire.service';
import { ReviewComponent } from '../review-component/review.component';
import { ReviewService } from '../services/review.service';
import { ISelfAssessment } from '../models/data-models';

export const cmmcViewStates = [
    {
        name: 'cato.loggedIn.dashboard.compliance',
        url: '/compliance',
        redirectTo: 'cato.loggedIn.dashboard.compliance.overview',
        views: {
            cmmc: {
                component: CmmcComponent,
            },
        },
        resolve: {
            $title: resolveDashboardTitle,
        },
    },
    {
        name: 'cato.loggedIn.dashboard.compliance.overview',
        url: '/overview',
        views: {
            compliance: {
                component: CmmcDashboardComponent,
            },
        },
    },
    {
        name: 'cato.loggedIn.dashboard.compliance.questionnaire',
        url: '/questionnaire/:assessmentId/{sectionIdentifier:[A-Z]{2}}{practiceIdentifierPart:\\.\\d\\.\\d{3}|[\\/;]\\d+}', // TODO: figure out how to get `/` to work correctly in the parameter value; for now using `;`
        params: {
            sectionIdentifier: {
                dynamic: true,
                value: null,
            },
            practiceIdentifierPart: {
                dynamic: true,
                value: null,
                raw: true,
            },
        },
        views: {
            compliance: {
                component: QuestionnaireComponent,
            },
        },
        resolve: [
            {
                token: 'assessment',
                deps: [Transition, QuestionnaireService],
                resolveFn: resolveAssessment,
            },
            {
                token: 'sections',
                deps: [Transition, QuestionnaireService],
                resolveFn: resolveSections,
            },
            {
                token: '$title',
                deps: ['assessment', '$title'],
                resolveFn: resolveQuestionnaireTitle,
            },
        ],
    },
    {
        name: 'cato.loggedIn.dashboard.compliance.review',
        url: '/review/:assessmentId',
        views: {
            compliance: {
                component: ReviewComponent,
            },
        },
        resolve: [
            {
                token: '$title',
                deps: ['assessment', '$title'],
                resolveFn: resolveReviewTitle,
            },
            {
                token: 'answers',
                deps: [Transition, ReviewService],
                resolveFn: resolveAssessmentAnswers,
            },
            {
                token: 'assessment',
                deps: [Transition, ReviewService],
                resolveFn: resolveReviewAssessment,
            },
            {
                token: 'evidence',
                deps: [Transition, ReviewService],
                resolveFn: resolveReviewEvidence,
            },
            {
                token: 'sections',
                deps: [Transition, ReviewService],
                resolveFn: resolveReviewSections,
            },
            {
                token: 'questions',
                deps: [Transition, ReviewService],
                resolveFn: resolveReviewQuestions,
            },
        ],
    },
];

export function resolveDashboardTitle($title) {
    return $title + ' - Compliance';
}
export function resolveAssessment(trans: Transition, qService: QuestionnaireService) {
    return qService.getAssessment(trans.params().assessmentId).toPromise();
}
export function resolveSections(trans: Transition, qService: QuestionnaireService) {
    return qService.continueAssessment(trans.params().assessmentId).toPromise();
}
export function resolveQuestionnaireTitle(assessment: ISelfAssessment, $title) {
    return $title + ' - ' + assessment.name;
    // TODO: title doesn't get updated when dynamic parameters change so the logic below needs to be somewhere else, perhaps a transition hook
    /*const params = trans.params();
    if (params.sectionIdentifier) {
        const section = sections.find(item => item.identifier === params.sectionIdentifier);
        if (section) {
            title += ' - ' + section.name;
        }
        // TODO: include practice in title?
    }
    return title;*/
}

export function resolveReviewTitle(assessment: ISelfAssessment, $title) {
    return $title + ' - Review - ' + assessment.name;
}
export function resolveAssessmentAnswers(trans: Transition, reviewService: ReviewService) {
    return reviewService.getAssessmentAnswers(trans.params().assessmentId).toPromise();
}
export function resolveReviewAssessment(trans: Transition, reviewService: ReviewService) {
    return reviewService.getAssessment(trans.params().assessmentId).toPromise();
}
export function resolveReviewEvidence(trans: Transition, reviewService: ReviewService) {
    return reviewService.getAssessmentEvidence(trans.params().assessmentId).toPromise();
}
export function resolveReviewSections(trans: Transition, reviewService: ReviewService) {
    return reviewService.getAssessmentSections(trans.params().assessmentId).toPromise();
}
export function resolveReviewQuestions(trans: Transition, reviewService: ReviewService) {
    return reviewService.getAssessmentQuestions(trans.params().assessmentId).toPromise();
}
