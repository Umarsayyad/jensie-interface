import { Component, OnInit, Input, AfterViewInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ɵb as InjectionService } from '@swimlane/ngx-charts';

import { LoadingContentComponent } from '../../common/components';
import { QuestionnaireService } from '../services/questionnaire.service';
import { IPractice } from '../models/data-models';
import { levelColorsArray } from '../../global-data/cmmc-level-colors';

@Component({
    selector: 'cmmc-explorer',
    templateUrl: './explorer.component.html',
    styleUrls: ['./explorer.component.less', '../dashboard-component/dashboard-chart.scss'],
})
export class CmmcExplorerComponent implements OnInit, AfterViewInit {
    private readonly lvlmax = 5;
    chartData: any[] = [];
    practiceMap: Map<number, Array<IPractice>> = new Map(
        Array(this.lvlmax)
            .fill(null)
            .map((v, i) => [i + 1, []])
    );

    title = 'CMMC Practices';
    yAxisLabel = 'Number of Practices';

    colorScheme = {
        domain: levelColorsArray,
    };

    @Input('data') inputData: string;

    @ViewChild(LoadingContentComponent)
    spinner: LoadingContentComponent;

    constructor(private qService: QuestionnaireService, vcr: ViewContainerRef, tooltipInjectorService: InjectionService) {
        tooltipInjectorService.setRootViewContainer(vcr);
    }

    ngOnInit() {
        this.qService.listPractices().subscribe((practices) => {
            for (let p of practices) {
                //add practices to the map
                this.practiceMap.get(p.level).push(p);
            }

            let array = [];
            for (let i = 0; i < this.lvlmax; i++) {
                array[i] = {
                    name: `Level ${i + 1} Practices`,
                    value: this.practiceMap.get(i + 1).length,
                };
            }

            this.chartData = Array(this.lvlmax)
                .fill(null)
                .map((v, i) => {
                    return {
                        name: 'Level ' + (i + 1),
                        series: array.slice(0, i + 1),
                    };
                });

            this.spinner.stopLoading();
        });
    }

    ngAfterViewInit() {
        this.spinner.startLoading();
    }

    log(...args) {
        console.log(args);
    }
}
