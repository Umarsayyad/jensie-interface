import { Component } from '@angular/core';
import { StateService } from '@uirouter/core';

@Component({
    selector: 'cmmc-wrapper',
    template: `<ui-view name="compliance"></ui-view>`,
    styles: [':host {height: calc(100vh - 210px); width: 100%; display: block;}'],
})
export class CmmcComponent {}
