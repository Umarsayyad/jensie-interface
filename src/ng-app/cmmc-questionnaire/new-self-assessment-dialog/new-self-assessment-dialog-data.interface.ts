import { ISelfAssessment } from '../models/data-models';

export interface INewSelfAssessmentDialogData {
    levels: number[];
    assessments: ISelfAssessment[];
}
