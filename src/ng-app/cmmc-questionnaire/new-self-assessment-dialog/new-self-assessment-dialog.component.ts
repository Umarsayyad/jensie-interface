import { Component, Inject, Optional } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

import { ID } from '../../common/models';
import { INewSelfAssessmentDialogData } from './new-self-assessment-dialog-data.interface';
import { ISelfAssessment } from '../models/data-models';
import { renameDuplicate } from '../../../app/common/utilities/rename-duplicate';

const DEFAULT_NAME = 'CMMC Self Assessment';

@Component({
    selector: 'new-self-assessment-dialog',
    templateUrl: './new-self-assessment-dialog.component.html',
    styleUrls: ['./new-self-assessment-dialog.component.scss'],
})
export class NewSelfAssessmentDialogComponent {
    questionnaire: ID = 1;
    sourceAssessment: ISelfAssessment | '' = '';
    name: string = DEFAULT_NAME;
    level: number = 1;
    levelChoices: number[] = [];
    assessments: ISelfAssessment[];
    private oldName: string;
    private oldLevel: number;

    constructor(@Inject(MAT_DIALOG_DATA) data: INewSelfAssessmentDialogData) {
        this.levelChoices = data.levels;
        this.assessments = data.assessments;
        this.oldName = this.name;
        this.oldLevel = this.level;
    }

    sourceAssessmentSelected() {
        if (this.name === this.oldName || this.name === '') {
            if (this.sourceAssessment === '') {
                this.name = this.oldName = DEFAULT_NAME;
            } else {
                this.name = this.oldName = renameDuplicate(this.sourceAssessment.name);
            }
        }
        if (this.level === this.oldLevel && this.sourceAssessment !== '') {
            this.level = this.oldLevel = this.sourceAssessment.goalLevel;
        }
    }
}
