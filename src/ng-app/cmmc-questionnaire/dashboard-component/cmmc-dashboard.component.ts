import { AfterViewInit, Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ɵb as InjectionService } from '@swimlane/ngx-charts';
import { StateService, TransitionPromise } from '@uirouter/core';
import { saveAs } from 'file-saver';

import { LoadingContentComponent } from '../../common/components';
import { getId, ID } from '../../common/models';
import { levelColorsArray } from '../../global-data/cmmc-level-colors';
import { CmmcExplorerComponent } from '../cmmc-explorer/explorer.component';
import { ConfirmDeleteAssessmentDialogComponent } from '../confirm-delete-assessment-dialog/confirm-delete-assessment-dialog.component';
import { ISelfAssessmentOptions } from '../models';
import { ISelfAssessment } from '../models/data-models';
import { computeProgress, IProgressDetail } from '../models/progress-detail.interface';
import { INewSelfAssessmentDialogData } from '../new-self-assessment-dialog/new-self-assessment-dialog-data.interface';
import { NewSelfAssessmentDialogComponent } from '../new-self-assessment-dialog/new-self-assessment-dialog.component';
import { QuestionnaireService } from '../services/questionnaire.service';

@Component({
    selector: 'cmmc-dashboard',
    templateUrl: './cmmc-dashboard.component.html',
    styleUrls: ['./cmmc-dashboard.component.less', './dashboard-chart.scss'],
})
export class CmmcDashboardComponent implements OnInit, AfterViewInit {
    level = 0;
    absoluteMaxLevel = 5;
    date = undefined;
    history: ISelfAssessment[];
    highlightedAssessment = {} as ISelfAssessment;
    progress: { [key: string]: IProgressDetail } = {};
    sections = [];
    data = null;
    firstVisit = false;

    colorScheme = {
        domain: levelColorsArray,
    };

    @ViewChild(LoadingContentComponent)
    spinner: LoadingContentComponent;

    constructor(
        private snackBar: MatSnackBar,
        private dialog: MatDialog,
        private stateService: StateService,
        private qService: QuestionnaireService,
        vcr: ViewContainerRef,
        tooltipInjectorService: InjectionService
    ) {
        tooltipInjectorService.setRootViewContainer(vcr);
    }

    ngOnInit() {
        const assessmentList = this.updateAssessmentList();
        assessmentList.then(() => {
            if (this.spinner.isLoading()) this.spinner.stopLoading();
        });
    }

    private updateAssessmentList() {
        const assessments = this.qService.getAssessments().toPromise();
        assessments.then(({ results }) => {
            this.history = results;
            if (this.history.length > 0) {
                this.updateDashStatistics(this.history[0]);
            } else {
                this.firstVisit = true;
            }
            for (let assess of this.history) {
                this.progress[assess.id] = computeProgress(assess.progressDetail);
            }
        });
        return assessments;
    }

    private updateDashStatistics(assessment: ISelfAssessment) {
        this.highlightedAssessment = assessment;

        this.date = assessment.updated ? this.formatDate(assessment.updated) : 'Not Started';

        this.data = [];
        for (let i = 1; i <= assessment.goalLevel; i++) {
            if (assessment.progressDetailByLevel[i].practices.progress === 100) {
                this.level = i;
            }

            this.data.push({
                name: 'Level ' + i,
                value: assessment.progressDetailByLevel[i].practices.progress,
                detail: assessment.progressDetailByLevel[i],
            });
        }
    }

    ngAfterViewInit() {
        this.spinner.startLoading();
    }

    openExplorer() {
        this.dialog.open(CmmcExplorerComponent, { width: '80vw', height: '80vh' });
    }

    newAssessment() {
        //prettier-ignore
        const levels = Array(this.absoluteMaxLevel).fill(null).map((v, i) => i + 1);
        const data: INewSelfAssessmentDialogData = { levels, assessments: this.history };
        const dialogRef = this.dialog.open(NewSelfAssessmentDialogComponent, { data });
        dialogRef.afterClosed().subscribe((result: ISelfAssessmentOptions) => {
            if (result) {
                this.spinner.startLoading();
                this.qService
                    .startAssessment(result.questionnaire, result.name, result.level, result.sourceAssessment)
                    .toPromise()
                    .then((assess) => this.go(assess.id))
                    .then(() => this.spinner.stopLoading())
                    .catch((error) => {
                        console.error(error);
                        alert(JSON.stringify(error.error)); // TODO: handle this in a prettier way, possibly using modalService.openErrorResponse('Error creating self assessment', error.error, false)
                        this.spinner.cancelLoading();
                    });
            }
        });
    }

    downloadEvidenceZip(assessmentId: ID, assessmentName: string) {
        this.spinner.startLoading();
        this.qService
            .downloadEvidenceZip(assessmentId)
            .subscribe((res) => {
                if (!res) {
                    this.snackBar.open('No evidence to download', 'Dismiss', { duration: 5000 });
                } else {
                    saveAs(res, `${assessmentName}.zip`);
                }
            })
            .add(() => {
                this.spinner.stopLoading();
            });
    }

    deleteAssessment(assessmentId: ID, assessmentName: string) {
        const data = { assessmentId, assessmentName };
        const dialogRef = this.dialog.open(ConfirmDeleteAssessmentDialogComponent, { data });
        dialogRef.afterClosed().subscribe((result: boolean) => {
            if (result) {
                this.spinner.startLoading();
                this.qService
                    .deleteAssessment(assessmentId)
                    .toPromise()
                    .then(() => {
                        let i = this.history.findIndex((e) => getId(e) === assessmentId);
                        this.history.splice(i, 1);
                        if (this.history.length > 0) {
                            if (i == 0) {
                                this.updateDashStatistics(this.history[0]);
                            }
                        } else {
                            this.firstVisit = true;
                        }
                        this.spinner.stopLoading();
                    })
                    .catch((error) => {
                        console.error(error);
                        alert(JSON.stringify(error.error)); // TODO: handle this in a prettier way, possibly using modalService.openErrorResponse('Error creating self assessment', error.error, false)
                        this.spinner.cancelLoading();
                    });
            }
        });
    }

    transitionSpinner() {
        this.spinner.startLoading();
        this.stateService.transition.promise
            .then(() => this.spinner.stopLoading())
            .catch((error) => {
                console.error(error);
                this.spinner.cancelLoading();
            });
    }

    pin(assessmentId: ID) {
        this.qService.pinAssessment(assessmentId).subscribe(() => {
            for (let assessment of this.history) {
                if (assessment.id === assessmentId) {
                    assessment.isPinned = true;
                    this.updateDashStatistics(assessment);
                } else {
                    assessment.isPinned = false;
                }
            }
        });
    }

    unpin(assessmentId: ID) {
        this.qService.unpinAssessment(assessmentId).subscribe(() => {
            for (let assessment of this.history) {
                if (assessment.id === assessmentId) {
                    assessment.isPinned = false;
                }
            }
            this.updateDashStatistics(this.history[0]);
        });
    }

    onSelect(event) {
        console.log(event);
    }

    private go(assessmentId: ID): TransitionPromise {
        const stateName = 'cato.loggedIn.dashboard.compliance.questionnaire';
        return this.stateService.go(stateName, { assessmentId }, { inherit: false });
    }

    private formatDate(date: string): string {
        if (!date) return 'demo timestamp'; //FIXME

        let d = new Date(date);
        return `${d.toLocaleString('default', { month: 'short' })} ${d.getDate()}, ${d.getFullYear()}`;
    }

    range = (lower, upper) => Array.from(new Array(upper), (x, i) => i + lower);
}
