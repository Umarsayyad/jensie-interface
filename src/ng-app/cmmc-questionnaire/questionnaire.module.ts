import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { UIRouterModule } from '@uirouter/angular';

import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTreeModule } from '@angular/material/tree';

import { CatoCommonModule } from '../common/common.module';
import { CmmcComponent } from './cmmc.component';
import { CmmcDashboardComponent } from './dashboard-component/cmmc-dashboard.component';
import { CmmcExplorerComponent } from './cmmc-explorer/explorer.component';
import { ConfirmDeleteAssessmentDialogComponent } from './confirm-delete-assessment-dialog/confirm-delete-assessment-dialog.component';
import { EvidenceComponent } from './evidence/evidence.component';
import { JustificationComponent } from './justification/justification.component';
import { NewSelfAssessmentDialogComponent } from './new-self-assessment-dialog/new-self-assessment-dialog.component';
import { QuestionnaireComponent } from './questionnaire-component/questionnaire.component';
import { SectionComponent } from './section-component/section.component';
import { ReviewComponent } from './review-component/review.component';
import { cmmcViewStates } from './data/cmmc-view-states';
import { QuestionProgressComponent } from './question-progress-component/question-progress.component';
import { MatSidenavModule } from '@angular/material/sidenav';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,

        MatButtonModule,
        MatCardModule,
        MatCheckboxModule,
        MatDialogModule,
        MatDividerModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatGridListModule,
        MatInputModule,
        MatListModule,
        MatMenuModule,
        MatProgressBarModule,
        MatRadioModule,
        MatSelectModule,
        MatSnackBarModule,
        MatStepperModule,
        MatTooltipModule,
        MatTreeModule,

        ReactiveFormsModule,
        NgxChartsModule,
        UIRouterModule.forChild({ states: cmmcViewStates }),

        CatoCommonModule,
        MatSidenavModule,
    ],
    exports: [CmmcDashboardComponent, CmmcExplorerComponent, QuestionnaireComponent],
    entryComponents: [
        CmmcComponent,
        CmmcDashboardComponent,
        CmmcExplorerComponent,
        EvidenceComponent,
        JustificationComponent,
        NewSelfAssessmentDialogComponent,
        QuestionnaireComponent,
        SectionComponent,
    ],
    declarations: [
        CmmcComponent,
        CmmcDashboardComponent,
        CmmcExplorerComponent,
        ConfirmDeleteAssessmentDialogComponent,
        EvidenceComponent,
        JustificationComponent,
        NewSelfAssessmentDialogComponent,
        QuestionnaireComponent,
        ReviewComponent,
        SectionComponent,
        QuestionProgressComponent,
    ],
    providers: [],
})
export class QuestionnaireModule {}
