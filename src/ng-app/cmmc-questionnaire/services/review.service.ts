import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { config } from '@cato/config';
import { ID } from '../../common/models';
import { IAnswer, IQuestion, ISection, ISelfAssessment } from '../models/data-models';
import { IEvidence } from '../models';

@Injectable({ providedIn: 'root' })
export class ReviewService {
    private readonly prefix = config.apiPath + 'compliance';

    constructor(private http: HttpClient) {}

    public getAssessment(assessment: ID): Observable<ISelfAssessment> {
        return this.http.get<ISelfAssessment>(`${this.prefix}/selfassessment/${assessment}`);
    }

    public getAssessmentSections(assessment: ID): Observable<Array<ISection>> {
        return this.http.get<Array<ISection>>(`${this.prefix}/selfassessment/${assessment}/section`);
    }

    public getAssessmentQuestions(assessment: ID): Observable<Array<IQuestion>> {
        const params = { questionnaire__self_assessments__id: assessment + '' };
        return this.http.get<Array<IQuestion>>(`${this.prefix}/question`, { params });
    }

    public getAssessmentAnswers(assessment: ID): Observable<Array<IAnswer>> {
        const params = { assessment_id: assessment + '' };
        return this.http.get<Array<IAnswer>>(`${this.prefix}/answer`, { params });
    }

    public getAssessmentEvidence(assessment: ID): Observable<Array<IEvidence>> {
        const params = { practice_responses__assessment_id: assessment + '' };
        return this.http.get<Array<IEvidence>>(`${this.prefix}/evidence`, { params });
    }
}
