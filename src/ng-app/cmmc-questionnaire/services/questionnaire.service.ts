import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { config } from '@cato/config';
import { saveAs } from 'file-saver';
import { Observable } from 'rxjs';

import { ID, IPaginatedResult } from '../../common/models';
import { IEvidence } from '../models';
import { IAnswer, IPractice, IPracticeResponse, ISection, ISectionContent, ISelfAssessment } from '../models/data-models';

@Injectable({ providedIn: 'root' })
export class QuestionnaireService {
    private readonly prefix = config.apiPath + 'compliance';

    constructor(private http: HttpClient) {}

    public getAssessment(assessId: ID): Observable<ISelfAssessment> {
        return this.http.get<ISelfAssessment>(`${this.prefix}/selfassessment/${assessId}`);
    }

    public getAssessments(): Observable<IPaginatedResult<ISelfAssessment>> {
        return this.http.get<IPaginatedResult<ISelfAssessment>>(`${this.prefix}/selfassessment?ordering=customer_is_pinned,-updated`);
    }

    public startAssessment(questionnaire: ID, name: string, goalLevel: number, sourceAssessment?: ID): Observable<ISelfAssessment> {
        return this.http.post<ISelfAssessment>(`${this.prefix}/selfassessment`, { questionnaire, name, goalLevel, sourceAssessment });
    }

    public continueAssessment(assessment: ID): Observable<Array<ISection>> {
        // return of(startedAss);
        return this.http.get<Array<ISection>>(`${this.prefix}/selfassessment/${assessment}/section`);
    }

    public pinAssessment(assessmentId: ID) {
        return this.http.put<null>(`${this.prefix}/selfassessment/${assessmentId}/pin`, {});
    }

    public unpinAssessment(assessmentId: ID) {
        return this.http.put<null>(`${this.prefix}/selfassessment/${assessmentId}/unpin`, {});
    }

    public copyAssessment(assessment: ID): Observable<ISelfAssessment> {
        let data = { name: 'something?' };
        return this.http.post<ISelfAssessment>(`${this.prefix}/selfassessment/${assessment}/copy`, data);
    }

    public deleteAssessment(assessment: ID): Observable<null> {
        return this.http.delete<null>(`${this.prefix}/selfassessment/${assessment}`);
    }

    public getSectionContents(assessment: ID, section: ID): Observable<ISectionContent> {
        // return of(sectionData);
        return this.http.get<ISectionContent>(`${this.prefix}/selfassessment/${assessment}/section/${section}`);
    }

    public savePracticeResponse(practiceResponse: IPracticeResponse): Observable<IPracticeResponse> {
        return this.http.put<IPracticeResponse>(`${this.prefix}/practiceresponse/${practiceResponse.id}`, practiceResponse);
    }

    public save(answer: IAnswer): Observable<IAnswer> {
        return this.http.put<IAnswer>(`${this.prefix}/answer/${answer.id}`, answer);
    }

    public uploadEvidence(evidence: File): Observable<IEvidence> {
        let fd = new FormData();
        fd.append('attachment', evidence);
        fd.append('name', evidence.name);
        fd.append('type', evidence.type);

        return this.http.post<IEvidence>(`${this.prefix}/evidence`, fd); //, {reportProgress: true,  observe: "events"})
    }

    public downloadEvidenceAttachment(evidence: IEvidence) {
        this.http.get(`${this.prefix}/evidence/${evidence.id}/download`, { responseType: 'blob' }).subscribe((res) => {
            saveAs(res, evidence.name);
        });
    }

    public listEvidence(): Observable<Array<IEvidence>> {
        return this.http.get<Array<IEvidence>>(`${this.prefix}/evidence`);
    }

    public listPractices(): Observable<Array<IPractice>> {
        return this.http.get<Array<IPractice>>(`${this.prefix}/practice`);
    }

    public downloadEvidenceZip(assessmentId: ID): Observable<Blob> {
        return this.http.get(`${this.prefix}/selfassessment/${assessmentId}/evidence_zip`, { responseType: 'blob' });
    }
}
