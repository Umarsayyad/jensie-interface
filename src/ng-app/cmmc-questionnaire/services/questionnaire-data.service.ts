import { Injectable } from '@angular/core';
// import { Observable } from "rxjs";
import * as deepFreeze from 'deep-freeze';

import { ID } from '../../common';
import { DataService } from '../../global-data/models/data-service.class';
import { SocketService } from '../../global-data/socket.service';
import { QuestionnaireService } from './questionnaire.service';
import { IQuestionnaireData, SectionData } from '../models';

@Injectable({ providedIn: 'root' })
export class QuestionnaireData extends DataService<IQuestionnaireData> {
    private quizMap = new Map<ID, IQuestionnaireData>();
    private quizzes: IQuestionnaireData[] = null;

    protected get objArray(): IQuestionnaireData[] {
        return this.quizzes;
    }
    protected get objMap(): Map<ID, IQuestionnaireData> {
        return this.quizMap;
    }

    constructor(private qService: QuestionnaireService, private socket: SocketService) {
        super();

        // this.qService.load().subscribe((quiz) =>
        // {
        //     deepFreeze(quiz);
        //     this.quizzes=[quiz];
        //     this.quizMap.set(quiz.id, quiz);

        //     this.items$.next([...this.quizzes]);
        // },
        // this.handleError);

        this.socket.updatesByModel('questionnaires').subscribe((val: IQuestionnaireData) => this.handler(val));
        // this.qService.fakeSocket().subscribe((val: IQuestionnaireData)=>this.handler(val));
    }

    public save(id: string, data: SectionData) {
        // this.qService.save(id, data);
        console.log(data);
    }
}
