import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ID } from '../../common/models';

@Component({
    selector: 'confirm-delete-assessment-dialog',
    templateUrl: './confirm-delete-assessment-dialog.component.html',
    styleUrls: ['./confirm-delete-assessment-dialog.component.less'],
})
export class ConfirmDeleteAssessmentDialogComponent {
    assessmentName: string;

    constructor(@Inject(MAT_DIALOG_DATA) data: { assessmentName: string }) {
        this.assessmentName = data.assessmentName;
    }
}
