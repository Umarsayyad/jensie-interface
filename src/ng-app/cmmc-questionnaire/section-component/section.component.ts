import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatStepper } from '@angular/material/stepper';
import { StateService } from '@uirouter/angular';

import { ID } from '../../common/models';
import { LoadingContentComponent } from '../../common/components';
import { AnswerOrNull, SectionData } from '../models';
import { IAnswer, IPractice, IPracticeResponse, IQuestion, ISectionContent } from '../models/data-models';
import { QuestionnaireService } from '../services/questionnaire.service';
import { EvidenceComponent } from '../evidence/evidence.component';
import { JustificationComponent } from '../justification/justification.component';

@Component({
    selector: 'section',
    templateUrl: 'section.component.html',
    styleUrls: ['section.component.less'],
})
export class SectionComponent implements OnInit, AfterViewInit {
    opened = true;
    forms: { [key: string]: FormGroup } = {};
    pages: IPractice[] = [];
    practiceResponseMap: Record<ID, IPracticeResponse> = {};
    sectionIdentifier: string = '';
    private data: SectionData;
    private pageCompletion: boolean[] = [];
    private allQs: IQuestion[];
    private qMap: Record<ID, IAnswer> = {};

    @Input() assessmentId: ID = null;
    @Input() sectionId: ID = null;
    @Output('exit') exit$ = new EventEmitter<SectionData>();

    @ViewChild(LoadingContentComponent)
    spinner: LoadingContentComponent;

    @ViewChild('stepper')
    private stepper: MatStepper;
    answerCounts: Record<ID, { total: number; passing: number; answered: number }> = {};

    constructor(
        private fb: FormBuilder,
        private qService: QuestionnaireService,
        private dialog: MatDialog,
        private snackBar: MatSnackBar,
        private stateService: StateService
    ) {}

    ngOnInit() {
        this.qService.getSectionContents(this.assessmentId, this.sectionId).subscribe((contents: ISectionContent) => {
            this.pages = contents.practices;
            this.allQs = contents.questions;
            this.sectionIdentifier = contents.name;

            //map the questions to their answers
            for (let a of contents.answers) {
                this.qMap[a.question] = a;
            }

            //map the questions to their answers
            for (let practiceResponse of contents.practiceResponses) {
                this.practiceResponseMap[practiceResponse.practice] = practiceResponse;
            }

            let index = -1;
            for (let page of this.pages) {
                index++;
                const i = index; //needs to be scoped to the block for some unknown reason

                let group = {} as any;
                for (let q of this.getQuestions(page.id)) {
                    group[q.id] = [''];
                }
                group[page.id + 'comments'] = [this.practiceResponseMap[page.id].justification];
                this.forms[page.id] = this.fb.group(group);

                this.answerCounts[page.id] = this.computeAnswerCounts(page.id);
                //needs to be done in a separate loop
                for (let q of this.getQuestions(page.id)) {
                    let control = this.forms[page.id].get([q.id]);
                    if (control) {
                        //set form value
                        let answer: IAnswer = this.qMap[q.id];
                        if (answer.response !== null) {
                            control.setValue(answer.response);
                        }

                        //listen for value-changes
                        control.valueChanges.subscribe((val: AnswerOrNull) => {
                            let ans: IAnswer = this.qMap[q.id];
                            ans.response = val;
                            this.answerCounts[page.id] = this.computeAnswerCounts(page.id);

                            this.qService.save(ans).subscribe(console.log, console.error);
                        });
                    }
                }
            }

            if (this.stateService.params.practiceIdentifierPart !== null) {
                const sectionIdentifier = this.stateService.params.sectionIdentifier;
                const practiceIdentifierPart = this.stateService.params.practiceIdentifierPart;
                setTimeout(() => {
                    let targetStep = -1;
                    if (practiceIdentifierPart[0] === '.') {
                        targetStep = this.pages.findIndex((item) => item.identifier === sectionIdentifier + practiceIdentifierPart);
                    } else if (practiceIdentifierPart[0] === '/' || practiceIdentifierPart[0] === ';') {
                        const practiceId = Number(practiceIdentifierPart.slice(1));
                        targetStep = this.pages.findIndex((item) => item.id === practiceId);
                    } else {
                        console.warn('Unexpected practice identifier part format: %s', practiceIdentifierPart);
                    }

                    if (targetStep >= 0) {
                        if (this.stepper.selectedIndex === targetStep && practiceIdentifierPart[0] !== '.') {
                            this.stateService.go('.', { practiceIdentifierPart: this.pages[targetStep].identifier.slice(2) });
                        }

                        this.stepper.selectedIndex = targetStep;
                    }
                });
            }

            if (this.spinner.isLoading()) this.spinner.stopLoading();
        });
    }

    ngAfterViewInit() {
        this.spinner.startLoading();
    }

    getQuestions(pageId): IQuestion[] {
        return this.allQs.filter((e) => e.practice === pageId);
    }

    openJustification(page: IPractice) {
        let practiceResponse: IPracticeResponse = this.practiceResponseMap[page.id];

        this.dialog
            .open(JustificationComponent, { width: '50vw', panelClass: 'gray-bg', data: practiceResponse.justification })
            .afterClosed()
            .subscribe((res: string) => {
                if (res === undefined) return;
                practiceResponse.justification = res;
                this.qService.savePracticeResponse(practiceResponse).subscribe((val: IPracticeResponse) => {
                    this.practiceResponseMap[page.id] = val;
                    this.forms[page.id].get(`${page.id}comments`).setValue(val.justification);
                }, console.error);
            });
    }

    chooseEvidence(practiceResponse: IPracticeResponse) {
        this.dialog
            .open(EvidenceComponent, { width: '50vw', panelClass: 'gray-bg', data: practiceResponse.evidence })
            .afterClosed()
            .subscribe((res?: Set<ID>) => {
                if (!res) return;

                let same = practiceResponse.evidence.length === res.size;
                for (let id of practiceResponse.evidence) {
                    if (!res.has(id)) {
                        same = false;
                        break;
                    }
                }

                if (same) return;

                practiceResponse.evidence = Array.from(res);
                this.qService.savePracticeResponse(practiceResponse).subscribe((val: IPracticeResponse) => {
                    // this.qMap[question.id]=val;
                    this.snackBar
                        .open('Evidence Added', 'View', { duration: 3000 })
                        .onAction()
                        .subscribe(() => this.chooseEvidence(practiceResponse));
                    this.practiceResponseMap[practiceResponse.practice] = val;
                    this.forms[practiceResponse.practice].get(`${practiceResponse.practice}comments`).setValue(val.justification);
                }, console.error);
            });
    }

    clear(practiceResponse: IPracticeResponse) {
        const cleared: IPracticeResponse = { ...practiceResponse, evidence: [], justification: '' };

        this.qService.savePracticeResponse(cleared).subscribe((res: IPracticeResponse) => {
            this.practiceResponseMap[practiceResponse.practice] = res;
            this.forms[practiceResponse.practice].get(`${practiceResponse.practice}comments`).setValue(res.justification);
        });
    }

    clearQuestion(question: IQuestion) {
        const answer: IAnswer = this.qMap[question.id];
        const cleared: IAnswer = { ...answer, response: null };

        this.qService.save(cleared).subscribe((res: IAnswer) => {
            this.qMap[question.id] = res;
            this.forms[question.practice].get(question.id.toString()).setValue(null, { emitEvent: false });
            this.answerCounts[question.practice] = this.computeAnswerCounts(question.practice);
        });
    }

    toString(thing: any) {
        return JSON.stringify(thing);
    }

    pageComplete(i: number): boolean {
        return this.pageCompletion[i];
    }

    selectionChanged($event: StepperSelectionEvent) {
        const practice = this.pages[$event.selectedIndex];
        if (practice) {
            this.stateService.go('.', { practiceIdentifierPart: practice.identifier.slice(2) });
        } else {
            this.stateService.go('.', { practiceIdentifierPart: null }); //can't set to "done" because of pattern, so just remove
        }
    }

    exit() {
        this.stateService.go('.', { sectionIdentifier: null, practiceIdentifierPart: null });
        this.exit$.emit(this.data);
    }

    private computeAnswerCounts(pageId: ID) {
        const counts = { total: 0, passing: 0, answered: 0 };
        for (let q of this.getQuestions(pageId)) {
            counts.total++;
            const answer: IAnswer = this.qMap[q.id];
            if (answer.response !== null) {
                counts.answered++;
                if (answer.response !== 'NO') {
                    counts.passing++;
                }
            }
        }
        return counts;
    }
}
