export enum Answer {
    yes = 'YES',
    no = 'NO',
    notApp = 'N/A',
}

export type AnswerOrNull = Answer | null;
