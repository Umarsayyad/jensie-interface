import { ID, IPaginatedResult } from '../../common/models';
import { Level } from '../../dashboard/widgets/insight-graphs/models/level/level.namespace';
import { AnswerOrNull } from './answer';

// Note: all URLs should be prefixed with "/api/v1/selfassess" to get the full path

let req;

// Dashboard/overview
// 	GET /questionnaire
// 		// Get just the questionnaire models so we have the name, etc.
// Self Assessment History
// 	GET /selfassessment
// 			?customer=<id>&level[__gte,__lte,…]=<>&date[…]=<>
type res = IPaginatedResult<ISelfAssessment>;

export interface ISelfAssessment {
    id: ID;
    questionnaire: number;
    name: string;
    customer: number;
    started: string; //Date/time
    updated: string; //Date/time
    completed: string; //Date/time
    percentAnswered: number;
    progressDetail: Level.ILevel;
    progressDetailByLevel: { [key: number]: Level.ILevel };
    needsProof: boolean;
    isComplete: boolean;
    isPinned: boolean;
    goalLevel: number;
    sections?: ISection[];
}

// Start self assessment
// 	POST /selfassessment
req = {
    questionnaire: 1,
    name: 'something?',
    goalLevel: 3,
};
type resp = ISelfAssessment;

export interface ISection {
    id: ID;
    name: string;
    identifier: string;
    type: string; //FIXME: use enum
    progressDetail: Level.ILevel;
}

// Continue existing self assessment
// 	GET /selfassessment/<self_assessment_id>/continue
// 			(without /continue doesn't include sections)
// 		Response is the same as POST, but sections may have progress
// Copy/start from existing self assessment (to implement later)
// 	POST /selfassessment/<self_assessment_id>/copy
req = {
    name: 'something else?',
    goalLevel: 4, // Optional -- leave off to keep the same level
};
// Response is the same format as GET /…/continue

// Opening a section
// 	GET /selfassessment/<self_assessment_id>/section/<section_id>
export interface ISectionContent {
    id: 1;
    name: string;
    capabilities: ICapability[];
    practices: IPractice[];
    practiceResponses: IPracticeResponse[];
    questions: IQuestion[];
    answers: IAnswer[];
}

export interface ICapability {
    id: ID;
    identifier: string;
    value: string;
    section: ID;
}

export interface IPractice {
    id: ID;
    identifier: string;
    value: string;
    section: ID;
    capability: number;
    level: number;
}

export interface IPracticeResponse {
    id: ID;
    assessment: string;
    evidence: ID[];
    justification: string;
    needsProof: string;
    practice: ID;
    hasProof: boolean;
    updated: string;
}

export interface IQuestion {
    id: ID;
    practice: ID;
    level: Number;
    value: String;
    section: ID;
    language: 'EN'; //FIXME: use enum?
    explanation: string;
    questionnaire: ID;
    capability: ID;
    timestamp: string; //Date/time
}

export interface IAnswer {
    id: ID;
    response: AnswerOrNull;
    evidence: any[];
    justification: string;
    question: ID;
    assessment: ID;
    user: ID;
    timestamp: string; //Date/time
}

// Saving an answer:
// 	PUT /answer/<answer_id>
req = {
    response: 'YES',
    evidence: [], // optional
};
// res =
// {
//     id: 422,
//     response: "YES",
//     question: 1,
//     assessment: 1,
//     evidence: [],
//     timestamp: "...",
//     user: 3,
// }
