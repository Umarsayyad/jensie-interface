import { IQuestion } from './question.interface';

export interface IPage {
    id: string;
    title: string;
    visible: string; //expression
    questions: IQuestion[];
}
