import { IPage } from './page.interface';

export interface ISection {
    id: string;
    title: string;
    pages: IPage[];
}
