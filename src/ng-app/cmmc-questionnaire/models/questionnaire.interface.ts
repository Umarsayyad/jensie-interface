import { ISection } from './section.interface';

export interface IQuestionnaire {
    sections: ISection[];
}
