import { ID } from '../../common';
import { DocType } from './doc-type';

export interface IEvidence {
    id: ID;
    attachment: string;
    name: string;
    type: DocType;
    timestamp: string;
    answers: ID[];
    customer: ID;
}
