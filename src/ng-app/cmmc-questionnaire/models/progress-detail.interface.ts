import { Level } from '../../dashboard/widgets/insight-graphs/models/level/level.namespace';

export interface IProgressDetail {
    pass: number;
    fail: number;
    incomplete: number;
    percentPassed: number;
    percentFailed: number;
    detail: Level.ILevel;
}

export function computeProgress(progress: Level.ILevel): IProgressDetail {
    let total = progress.questions.total;
    let percentFailed =
        total !== 0 ? Math.floor(((progress.questions.no + progress.questions.yes + progress.questions.notApplicable) / total) * 100) : 100;
    // If at least one question has a yes or no answer, the percentage is at least 1.
    if (percentFailed === 0 && progress.questions.no + progress.questions.yes + progress.questions.notApplicable > 0) {
        percentFailed = 1;

        if (progress.questions.yes + progress.questions.notApplicable > 0 && progress.questions.progress < 1) {
            progress.questions.progress = 1;
        }
    }

    return {
        pass: progress.questions.passing,
        fail: progress.questions.no,
        incomplete: progress.questions.incomplete,
        percentPassed: progress.questions.progress,
        percentFailed,
        detail: progress,
    };
}
