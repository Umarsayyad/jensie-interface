export enum DocType {
    image = 'image/png',
    excel = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    word = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    pdf = 'application/pdf',
    text = 'text/plain',
}
