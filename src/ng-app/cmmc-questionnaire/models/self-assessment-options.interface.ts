import { ID } from '../../common';

export interface ISelfAssessmentOptions {
    name: any;
    level: number;
    questionnaire: ID;
    sourceAssessment: ID;
}
