export type IQuestionnaireData = { id: string } & { [key: string]: SectionData };

export interface SectionData {
    [key: string]: PageData;
}

export interface PageData {
    [key: string]: AnswerValue;
}

export type AnswerValue = boolean | null; //FIXME
