import { Component, Input } from '@angular/core';
import { IProgressDetail } from '../models/progress-detail.interface';

@Component({
    selector: 'question-progress',
    templateUrl: './question-progress.component.html',
    styleUrls: ['./question-progress.component.less'],
})
export class QuestionProgressComponent {
    @Input() progress: IProgressDetail;

    @Input() needsProof: boolean;

    constructor() {}
}
