import { Component, Input } from '@angular/core';

@Component({
    selector: 'tools-window',
    templateUrl: './tools-window.component.html',
    styleUrls: ['./tools-window.component.less'],
})
export class ToolsWindow {
    @Input() close: Function;
}
