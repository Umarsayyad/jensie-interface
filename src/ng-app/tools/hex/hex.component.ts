import { Component } from '@angular/core';

@Component({
    selector: 'hex-component',
    templateUrl: './hex.component.html',
    styleUrls: ['./hex.component.less'],
})
export class HexComponent {
    private readonly displayBufferMax = 10000;

    int8Array: Uint8Array | null;
    lines: string;
    loadingFile: Promise<void>; //FIXME: Use loading-content component

    private displayBuffer: number;
    private slider = {
        value: 0,
        options: {
            floor: 0,
            ceil: 0,
            step: 1,
        },
    };

    constructor() {
        this.displayBuffer = 100;
    }

    // Largely borrowed from: https://codepen.io/l0ner/pen/xVxrQe
    convertItoa(i: number): string {
        // Converts an integer (unicode value) to a char
        return String.fromCharCode(i);
    }

    convertItph(int: number, padding: number): string {
        // returns hex value of passed integer, padded with zeroes.
        if (isNaN(int) || isNaN(padding)) throw 'int2paddedHex(): Arg is NaN';
        let returnString = '';
        let val = int.toString(16);
        let zeroes = padding - val.length;
        for (let i = 0; i < zeroes; i++) returnString += '0';
        return returnString + val.toUpperCase();
    }

    hexLine(linen: number): string {
        let addr = this.convertItph(linen, 7);
        let vals = '';
        let latinString = '';
        for (let k = 0; k < 16; k++) {
            let ival = this.getValueAtLineOffset(linen, k);
            if (ival == null) {
                // We've run off the end.
                break;
            }
            vals += this.convertItph(ival, 2);
            vals += ' ';
            // No unprintables. Only convert basic latin set.
            if (ival > 31 && ival < 127) {
                latinString += this.convertItoa(ival);
            } else {
                latinString += '.';
            }
        }

        vals = vals.padEnd(48, ' ');

        return addr + '  ' + vals + '  ' + latinString;
    }

    loadFile(file: File) {
        this.loadingFile = new Promise((resolve, reject) => {
            this.int8Array = null;
            let reader = new FileReader();
            reader.onload = (e) => {
                // It will be an ArrayBuffer, as we're calling readAsArrayBuffer
                this.int8Array = new Uint8Array(reader.result as ArrayBuffer);
                this.slider.options.ceil = Math.floor(this.int8Array.length / 16);
                this.renderText();

                resolve();
            };

            reader.readAsArrayBuffer(file);
        });
    }

    renderText() {
        if (this.displayBuffer > this.displayBufferMax) {
            this.displayBuffer = this.displayBufferMax;
        }

        this.lines = '';
        // Each line, as displayed, is 16 bytes.
        let count = 0;

        if (this.int8Array) {
            for (let i = this.slider.value; i < this.int8Array.length / 16 && count < this.displayBuffer; i++) {
                this.lines += this.hexLine(i) + '\n';
                count++;
            }
        }
    }

    getValueAtLineOffset(linen: number, offset: number): number | null {
        // Return the integer value at a given line for a given offset.
        // Each line is 16 bytes wide..
        if (this.int8Array) return this.int8Array[linen * 16 + offset];
        else return null;
    }
}
