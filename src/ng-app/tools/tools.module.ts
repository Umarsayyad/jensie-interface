import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatSliderModule } from '@angular/material/slider';
import { MatTabsModule } from '@angular/material/tabs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UpgradeModule } from '@angular/upgrade/static';

import { CatoCommonModule } from '../common/common.module';
import { Base64Component } from './base-64/base-64.component';
import { CidrComponent } from './cidr/cidr.component';
import { HexComponent } from './hex/hex.component';
import { ToolsWindow } from './window/tools-window.component';

@NgModule({
    declarations: [Base64Component, CidrComponent, HexComponent, ToolsWindow],
    entryComponents: [ToolsWindow],
    imports: [BrowserAnimationsModule, FormsModule, MatSliderModule, MatTabsModule, UpgradeModule, CatoCommonModule],
    exports: [
        // Base64Component,
        // CidrComponent,
        // HexComponent,
        // ToolsWindow,
    ],
})
export class ToolsModule {}
