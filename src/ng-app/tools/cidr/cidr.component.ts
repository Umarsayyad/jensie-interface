import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { CidrCalculationResult, calculateCidrBlock, calculateIpRange, calculateTotalHosts } from '../../common/utilities/cidr-calculation';
import { Patterns } from '../../common/utilities/validation/patterns';

@Component({
    selector: 'cidr-component',
    templateUrl: './cidr.component.html',
    styleUrls: ['./cidr.component.less'],
})
export class CidrComponent {
    readonly Patterns = Patterns;
    readonly requiredErrorMsg = 'This field is required';

    @ViewChild('cidrform') cidrform: NgForm;
    cidrBlock: string;
    rangeResults: CidrCalculationResult;
    rangeError: string | null = null;

    @ViewChild('iprange') iprange: NgForm;
    rangeStart: string;
    rangeEnd: string;
    cidrResults: string[];
    cidrError: string | null = null;

    onCalculateRangeClick(force = false) {
        if (this.cidrform.invalid && !force) return;

        try {
            let [ip, prefixLen] = this.cidrBlock.split('/');
            let ipRange = calculateIpRange(ip, +prefixLen);
            let totalHosts = calculateTotalHosts(+prefixLen);

            this.rangeResults = {
                ...ipRange,
                totalHosts,
                range: this.cidrBlock,
            };

            this.rangeError = null;
        } catch (e) {
            this.rangeError = 'There was an error calculating the IP address range.';
            console.error('Error calculating IP range:', e);
        }
    }

    onCalculateCidrClick() {
        if (this.iprange.invalid) return;

        try {
            this.cidrResults = calculateCidrBlock(this.rangeStart, this.rangeEnd);

            this.cidrError = null;
        } catch (e) {
            this.cidrError = 'There was an error calculating the CIDR block.';
            console.error('Error calculating CIDR block:', e);
        }
    }

    quickView(cidrBlock: string) {
        this.cidrBlock = cidrBlock;
        this.onCalculateRangeClick(true);
    }
}

// Allow angularjs to use this component
// app.component("cidrComponent", downgradeComponent({component: CidrComponent}));
