import { Component } from '@angular/core';

interface Text {
    text: string;
}

@Component({
    selector: 'base-64-component',
    templateUrl: './base-64.component.html',
    styleUrls: ['./base-64.component.less'],
})
export class Base64Component {
    plain: Text = { text: '' };
    base64: Text = { text: '' };

    plainToB64() {
        this.base64 = { text: btoa(this.plain.text) };
    }

    b64ToPlain() {
        this.plain = { text: atob(this.base64.text) };
    }
}
