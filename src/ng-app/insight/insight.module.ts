import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';

import { CatoCommonModule } from '../common/common.module';
import { LoadInsightSearchComponent } from './load-insight-search/load-insight-search.component';
import { SaveInsightSearchComponent } from './save-insight-search/save-insight-search.component';

@NgModule({
    //prettier-ignore
    declarations: [
        LoadInsightSearchComponent,
        SaveInsightSearchComponent,
    ],
    //prettier-ignore
    entryComponents: [
        LoadInsightSearchComponent,
        SaveInsightSearchComponent,
    ],
    //prettier-ignore
    imports: [
        CommonModule,
        FormsModule,
        MatButtonModule,
        MatDialogModule,
        MatDividerModule,
        MatListModule,
        ReactiveFormsModule,

        CatoCommonModule,
    ],
    exports: [],
    providers: [],
})
export class InsightModule {}
