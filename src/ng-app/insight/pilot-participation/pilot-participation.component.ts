import { Component, OnInit } from '@angular/core';
import { DetailGridInfo } from 'ag-grid-community';

import { ITableOptions, RowModel } from '../../common/table/models/table-options.interface';
import { NaicsIndustryService } from '../../company-profile';
import { ICustomer } from '../../global-data/models/customer.interface';
import { GlobalDataService } from '../../ngjs-upgrade/globalDataService';
import { pilotColumns } from './data/pilot-participation.columns';
import { IParticipant } from './models/participant.interface';

@Component({
    selector: 'pilot-participation',
    templateUrl: './pilot-participation.component.html',
    styleUrls: ['./pilot-participation.component.less'],
})
export class PilotParticipationComponent implements OnInit {
    dataset: IParticipant[] = [];
    columns = pilotColumns;
    targetTagActions = [];
    tableOptions: ITableOptions<IParticipant> = {
        custom: {
            rowModelType: RowModel.ClientSide,
            checkboxes: false,
            sizeColumnsToFit: true,
        },
    };

    constructor(private gdata: GlobalDataService, private naics: NaicsIndustryService) {}

    ngOnInit() {
        for (let c of this.gdata.customers as ICustomer[]) {
            let participant: IParticipant = {
                id: c.id,
                name: c.nickname,
                state: c.primary_location_state,
                programs: '',
                businessType: '',
                industry: this.naics.getNaicsName(c.naics),
            };

            let src = [];
            if (c.has_participated_mentor_program) src.push('Mentor-Protégé');
            if (c.has_participated_nist_mep) src.push('NIST MEP');
            participant.programs = src.length > 0 ? src.join(', ') : 'N/A';

            let type = [];
            if (c.has_8a_small_business) type.push('8(a) Small Business');
            if (c.has_hubzone_business) type.push('HUBZone');
            if (c.has_indian_economic_enterprise) type.push('IEE');
            if (c.has_service_disabled_veteran_business) type.push('Disabled Veteran');
            if (c.has_veteran_owned_business) type.push('Veteran');
            if (c.has_women_owned_business) type.push('Woman');
            participant.businessType = type.length > 0 ? type.join(', ') : 'N/A';

            this.dataset.push(participant);
        }
    }
}
