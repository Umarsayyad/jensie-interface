import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UIRouterModule } from '@uirouter/angular';

import { CatoCommonModule } from '../../common/common.module';
import { PilotParticipationComponent } from './pilot-participation.component';

const pilotViewState = {
    name: 'cato.loggedIn.dashboard.pilotParticipation',
    url: '/pilot',
    // sticky: true,
    views: {
        pilotParticipation: {
            component: PilotParticipationComponent,
        },
    },
    resolve: {
        $title: ($title) => $title,
    },
};

@NgModule({
    declarations: [PilotParticipationComponent],
    entryComponents: [PilotParticipationComponent],
    imports: [CommonModule, UIRouterModule.forChild({ states: [pilotViewState] }), CatoCommonModule],
    exports: [PilotParticipationComponent],
})
export class PilotParticipationModule {}
