import { ID } from '../../../common/models';

export interface IParticipant {
    id: ID;
    name: string;
    state: string;
    programs: string;
    businessType: string; //TODO: enum?
    industry: string;
}
