import { IColumn } from '../../../common/table';
import { IParticipant } from '../models/participant.interface';

export const pilotColumns: Readonly<Array<IColumn<IParticipant>>> = [
    {
        field: 'name',
        header: 'Company Name',
        width: 18,
    },
    {
        field: 'state',
        header: 'Location',
        width: 12,
    },
    {
        field: 'programs',
        header: 'Programs',
        width: 23,
    },
    {
        field: 'businessType',
        header: 'Socio-Economic Categories',
        width: 26,
    },
    {
        field: 'industry',
        header: 'Industry',
        width: 21,
    },
];
