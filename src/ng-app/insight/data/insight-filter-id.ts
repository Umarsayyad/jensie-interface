export enum InsightFilterId {
    Categories = 'categories',
    Contracts = 'contracts',
    Customer = 'customerIds',
    Size = 'companySize',
    State = 'states',
    Industry = 'industries',
    Manufacturer = 'manufacturerTypes',
    Participation = 'participation',
    Levels = 'levels',
}
