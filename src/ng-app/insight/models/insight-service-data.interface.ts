import { IInsightSearchService } from './insight-search-service.interface';

export interface IInsightServiceData {
    service: IInsightSearchService;
}
