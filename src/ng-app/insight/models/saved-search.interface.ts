import { InsightFilterId } from '../data/insight-filter-id';

export type SavedSearches = ISearchMetadata[];
export type InsightSearch = Partial<Record<InsightFilterId, Array<string>>>;

export interface ISearchMetadata {
    search: InsightSearch;
    name: string;
    lastModified: number; //ms since epoch; can be converted to Date via ctor
}
