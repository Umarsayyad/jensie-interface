import { Observable } from 'rxjs';

import { IUser } from '../../global-data/models/user.interface';
import { ISearchMetadata, SavedSearches } from './saved-search.interface';

export interface IInsightSearchService {
    saveSearch(index: number, name: string): Promise<IUser>;

    getSavedSearches(): Promise<SavedSearches>;

    loadSearch(index: number): Promise<void>;

    deleteSearch(index: number);

    currentSearchIndexChanges(): Observable<number>;

    currentSavedSearchChanges(): Observable<ISearchMetadata>;

    currentFilterChanges(): Observable<boolean>;
}
