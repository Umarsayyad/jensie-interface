import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { UIRouterModule } from '@uirouter/angular';

import { CatoCommonModule } from '../../common/common.module';
import { InsightStatsComponent } from './insight-stats.component';

const insightViewState = {
    name: 'cato.loggedIn.dashboard.insightStats',
    url: '/stats',
    views: {
        stats: {
            component: InsightStatsComponent,
        },
    },
    resolve: {
        $title: ($title) => $title,
    },
};

@NgModule({
    declarations: [InsightStatsComponent],
    entryComponents: [InsightStatsComponent],
    imports: [
        CommonModule,
        MatCardModule,
        MatFormFieldModule,
        MatSelectModule,
        ReactiveFormsModule,
        UIRouterModule.forChild({ states: [insightViewState] }),

        CatoCommonModule,
    ],
    exports: [InsightStatsComponent],
})
export class InsightStatsModule {}
