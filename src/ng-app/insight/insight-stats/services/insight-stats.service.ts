import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { config } from '@cato/config';
import { IAssessmentCounts } from '../models/assessment-counts.interface';

@Injectable({ providedIn: 'root' })
export class InsightStatsService {
    private readonly prefix = config.apiPath + 'compliance';

    constructor(private http: HttpClient) {}

    public getCounts(fromDate?: string, toDate?: string): Observable<IAssessmentCounts> {
        const params = {};
        if (fromDate) {
            params['completed__date__gte'] = fromDate;
        }
        if (toDate) {
            params['completed__date__lte'] = toDate;
        }

        return this.http.get<IAssessmentCounts>(`${this.prefix}/selfassessment/counts`, { params });
    }
}
