import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import * as Moment from 'moment';

import { DateRangeType } from '../../common/data';
import { CustomerResource } from '../../global-data/customer.resource';
import { InsightStatsService } from './services/insight-stats.service';

interface StatsRange {
    value: string;
    viewValue: string;
}

interface DateRange {
    fromDate: string;
    toDate: string;
}

@Component({
    selector: 'insight-stats',
    templateUrl: './insight-stats.component.html',
    styleUrls: ['./insight-stats.component.less'],
})
export class InsightStatsComponent implements OnInit {
    statsRanges: StatsRange[];
    topNaics: Array<{ count: number; naics: string }> = [];
    counts = {
        newEnrollments: 0,
        completed: 0,
        mppEnrollments: 0,
        vetOwned: 0,
        womanOwned: 0,
        eightA: 0,
        hubZone: 0,
        manufacturers: 0,
    };
    statsRangeControl = new FormControl();

    constructor(private statService: InsightStatsService, private customers: CustomerResource) {}

    ngOnInit() {
        this.statsRanges = [
            { value: DateRangeType.ThisWeek, viewValue: 'This week' },
            { value: DateRangeType.LastWeek, viewValue: 'Last week' },
            { value: DateRangeType.LastSevenDays, viewValue: 'Last 7 days' },
            { value: DateRangeType.ThisMonth, viewValue: 'This month' },
            { value: DateRangeType.LastMonth, viewValue: 'Last month' },
            { value: DateRangeType.LastThirtyDays, viewValue: 'Last 30 days' },
            { value: DateRangeType.LastNinetyDays, viewValue: 'Last 90 days' },
            { value: DateRangeType.ThisQuarter, viewValue: 'This quarter' },
            { value: DateRangeType.LastQuarter, viewValue: 'Last quarter' },
            { value: DateRangeType.LastYear, viewValue: 'Last year' },
            { value: DateRangeType.YearToDate, viewValue: 'Year to date' },
        ];

        // TODO: Nice way to combine the following three code paragraphs?
        // TODO: May also want to change the appearance of the standard Material selector
        this.statsRangeControl.valueChanges.subscribe((newRangeValue) => {
            const dateRange = InsightStatsComponent.getDatesForRange(newRangeValue);
            this.statService.getCounts(dateRange.fromDate, dateRange.toDate).subscribe((counts) => {
                this.counts.completed = counts.customers;
            });

            this.customers.getCounts(dateRange.fromDate, dateRange.toDate).subscribe((stats) => {
                this.counts.newEnrollments = stats.enrollments;
                this.counts.mppEnrollments = stats.has_participated_mentor_program;
                this.counts.vetOwned = stats.has_veteran_owned_business;
                this.counts.womanOwned = stats.has_women_owned_business;
                this.counts.eightA = stats.has_8a_small_business;
                this.counts.hubZone = stats.has_hubzone_business;
                this.counts.manufacturers = stats.manufacturer;

                this.topNaics = stats.top_naics_codes.slice(0, 3);
            });
        });

        this.statService.getCounts().subscribe((counts) => {
            this.counts.completed = counts.customers;
        });

        this.customers.getCounts().subscribe((stats) => {
            this.counts.newEnrollments = stats.enrollments;
            this.counts.mppEnrollments = stats.has_participated_mentor_program;
            this.counts.vetOwned = stats.has_veteran_owned_business;
            this.counts.womanOwned = stats.has_women_owned_business;
            this.counts.eightA = stats.has_8a_small_business;
            this.counts.hubZone = stats.has_hubzone_business;
            this.counts.manufacturers = stats.manufacturer;

            this.topNaics = stats.top_naics_codes.slice(0, 3);
        });
    }

    private static getDatesForRange(newValue: string): DateRange {
        console.debug('Calculating dates for range:', newValue);
        let fromDate: Moment.Moment, toDate: Moment.Moment;
        switch (newValue) {
            case DateRangeType.ThisWeek:
                fromDate = Moment().startOf('week');
                toDate = Moment();
                break;
            case DateRangeType.LastWeek:
                // Calculate the start of last week
                fromDate = Moment().startOf('week').subtract(1, 'day').startOf('week');
                toDate = fromDate.clone().endOf('week');
                break;
            case DateRangeType.LastSevenDays:
                fromDate = Moment().subtract(7, 'days');
                toDate = Moment();
                break;
            case DateRangeType.ThisMonth:
                fromDate = Moment().startOf('month');
                toDate = Moment();
                break;
            case DateRangeType.LastMonth:
                // Calculate the start of last week
                fromDate = Moment().startOf('month').subtract(1, 'day').startOf('month');
                toDate = fromDate.clone().endOf('month');
                break;
            case DateRangeType.LastThirtyDays:
                fromDate = Moment().subtract(30, 'days');
                toDate = Moment();
                break;
            case DateRangeType.LastNinetyDays:
                fromDate = Moment().subtract(90, 'days');
                toDate = Moment();
                break;
            case DateRangeType.ThisQuarter:
                fromDate = Moment().startOf('quarter');
                toDate = Moment();
                break;
            case DateRangeType.LastQuarter:
                // Calculate the start of last quarter
                fromDate = Moment().startOf('quarter').subtract(1, 'day').startOf('quarter');
                toDate = fromDate.clone().endOf('quarter');
                break;
            case DateRangeType.YearToDate:
                fromDate = Moment().startOf('year');
                toDate = Moment();
                break;
            case DateRangeType.LastYear:
                fromDate = Moment().startOf('year').subtract(1, 'day').startOf('year');
                toDate = fromDate.clone().endOf('year');
                break;
            default:
                break;
        }
        return { fromDate: fromDate.format('YYYY-MM-DD'), toDate: toDate.format('YYYY-MM-DD') };
    }
}
