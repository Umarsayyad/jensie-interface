export interface IAssessmentCounts {
    completed: Array<{ count: number; level: number }>;
    customers: number;
}
