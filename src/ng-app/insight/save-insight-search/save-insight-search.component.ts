import { AfterViewInit, Component, Inject, OnDestroy, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { LoadingContentComponent } from '../../common';
import { IInsightSearchService } from '../models/insight-search-service.interface';
import { IInsightServiceData } from '../models/insight-service-data.interface';
import { ISearchMetadata, SavedSearches } from '../models/saved-search.interface';

@Component({
    selector: 'save-filters',
    templateUrl: './save-insight-search.component.html',
    styleUrls: ['./save-insight-search.component.less'],
})
export class SaveInsightSearchComponent implements AfterViewInit, OnDestroy {
    private readonly onDestroy$ = new Subject();
    private insightSearches: IInsightSearchService;

    searches: ISearchMetadata[];
    searchName = 'Insight Search';
    currentIndex: number | null = null;
    private nextIndex: number;

    @ViewChild('spinner') spinner: LoadingContentComponent;

    constructor(
        private dialogRef: MatDialogRef<SaveInsightSearchComponent>,
        private snackBar: MatSnackBar,
        @Inject(MAT_DIALOG_DATA) data: IInsightServiceData
    ) {
        this.insightSearches = data.service;
    }

    ngAfterViewInit() {
        this.spinner.startLoading();

        const searchProcessingCompleted = this.insightSearches.getSavedSearches().then((searches: SavedSearches) => {
            this.searches = searches;
            this.nextIndex = this.searches.length;

            this.spinner.stopLoading();
        });

        this.insightSearches
            .currentSearchIndexChanges()
            .pipe(takeUntil(this.onDestroy$))
            .subscribe(async (newIndex: number) => {
                await searchProcessingCompleted;
                this.currentIndex = newIndex;
                this.searchName = newIndex != null && this.searches[newIndex] ? this.searches[newIndex].name : 'Insight Search';
            });
    }

    ngOnDestroy() {
        this.onDestroy$.next();
        this.onDestroy$.complete();
    }

    async save(name: string) {
        await this.insightSearches.saveSearch(this.currentIndex, name);
        this.dialogRef.close();
    }

    async saveAs(name: string) {
        name = this.stripName(name);

        let i = 1;
        let name2 = name;

        while (this.isDuplicateSearchName(name2)) {
            name2 = `${name} (${i++})`;
        }

        await this.insightSearches.saveSearch(this.nextIndex, name2);
        this.snackBar.open(`Saved new filter: ${name2}`, 'Dismiss', { duration: 3000 });
        this.dialogRef.close();
    }

    private stripName(name: string): string {
        const regex = /^(.+) \([0-9]+\)$/;
        if (regex.test(name)) {
            let nameArray = name.split(' ');
            nameArray.pop();
            name = nameArray.join(' ');
        }

        return name;
    }

    private isDuplicateSearchName(sName: string): boolean {
        for (let { name } of this.searches) {
            if (sName === name) return true;
        }

        return false;
    }
}
