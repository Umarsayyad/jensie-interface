import { IColumn } from '../../../common/table';
import { IMentorProtegeParticipant } from '../models/mentor-protege.interface';
import { DateUtil } from '../../../common/utilities/date-util';

export const mentorProtegeColumns: Readonly<Array<IColumn<IMentorProtegeParticipant>>> = [
    {
        field: 'companyName',
        header: 'Company Name',
        width: 40,
        resizable: true,
    },
    {
        field: 'mentorCompany',
        header: 'Mentor',
        width: 40,
        resizable: true,
    },
    {
        field: 'startDate',
        header: 'Start Date',
        comparator: DateUtil.compareDate,
        filter: 'agDateColumnFilter',
        filterParams: {
            comparator: DateUtil.compareDate,
        },
        width: 20,
        resizable: true,
    },
    {
        field: 'isCurrentParticipant',
        header: 'Current Participant',
        width: 20,
    },
];
