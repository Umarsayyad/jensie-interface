import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UIRouterModule } from '@uirouter/angular';

import { CatoCommonModule } from '../../common/common.module';
import { MentorProtegeComponent } from './mentor-protege.component';

const mentorProtegeViewState = {
    name: 'cato.loggedIn.dashboard.mpp',
    url: '/mpp',
    views: {
        mpp: {
            component: MentorProtegeComponent,
        },
    },
};

@NgModule({
    declarations: [MentorProtegeComponent],
    entryComponents: [MentorProtegeComponent],
    imports: [CommonModule, UIRouterModule.forChild({ states: [mentorProtegeViewState] }), CatoCommonModule],
    exports: [MentorProtegeComponent],
})
export class MentorProtegeModule {}
