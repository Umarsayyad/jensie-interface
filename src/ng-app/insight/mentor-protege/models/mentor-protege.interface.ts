import { ID } from '../../../common/models';

export interface IMentorProtegeParticipant {
    id: ID;
    companyName: string;
    mentorCompany: string;
    isCurrentParticipant: string;
    startDate: string;
}
