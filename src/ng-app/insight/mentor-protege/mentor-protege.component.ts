import { Component, OnInit } from '@angular/core';
import { RowModel } from '../../common/table/models/table-options.interface';
import { mentorProtegeColumns } from './data/mentor-protege.columns';
import { IMentorProtegeParticipant } from './models/mentor-protege.interface';
import { CustomerResource } from '../../global-data/customer.resource';
import { IMppDetails } from '../../global-data/models/mpp-details.interface';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { YesNoFormat, YesNoPipe } from '../../common/pipes/yes-no.pipe';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'mentor-protege',
    templateUrl: './mentor-protege.component.html',
})
export class MentorProtegeComponent implements OnInit {
    dataset: Observable<IMentorProtegeParticipant[]>;
    columns = mentorProtegeColumns;
    targetTagActions = [];
    tableOptions = {
        rowModelType: RowModel.ClientSide,
        custom: {
            checkboxes: false,
            sizeColumnsToFit: true,
        },
    };

    constructor(private customers: CustomerResource, private yesNo: YesNoPipe, private datePipe: DatePipe) {}

    ngOnInit(): void {
        this.dataset = this.customers.getMPPDetails().pipe(
            map((mppDetails: IMppDetails[]) => {
                let rows: IMentorProtegeParticipant[] = [];

                for (let i = 0; i < mppDetails.length; i++) {
                    const mppDetail = mppDetails[i];

                    const mentorProtegeParticipant: IMentorProtegeParticipant = {
                        id: i,
                        companyName: mppDetail.name,
                        mentorCompany: mppDetail.mentorCompany,
                        isCurrentParticipant: this.yesNo.transform(mppDetail.isCurrentParticipant, YesNoFormat.Long),
                        startDate: this.datePipe.transform(mppDetail.startDate, 'MM/dd/yyyy'),
                    };
                    rows.push(mentorProtegeParticipant);
                }

                return rows;
            })
        );
    }
}
