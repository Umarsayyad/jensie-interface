import { Component, AfterViewInit, ViewChild, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { LoadingContentComponent } from '../../common';
import { IInsightSearchService } from '../models/insight-search-service.interface';
import { IInsightServiceData } from '../models/insight-service-data.interface';
import { SavedSearches, ISearchMetadata } from '../models/saved-search.interface';

@Component({
    selector: 'load-insight-search',
    templateUrl: './load-insight-search.component.html',
    styleUrls: ['./load-insight-search.component.less'],
})
export class LoadInsightSearchComponent implements AfterViewInit {
    insightSearches: IInsightSearchService;
    searches: ISearchMetadata[];

    @ViewChild('spinner') spinner: LoadingContentComponent;

    constructor(private dialogRef: MatDialogRef<LoadInsightSearchComponent>, @Inject(MAT_DIALOG_DATA) data: IInsightServiceData) {
        this.insightSearches = data.service;
    }

    ngAfterViewInit() {
        this.init();
    }

    private init() {
        this.spinner.startLoading();

        this.insightSearches.getSavedSearches().then((searches: SavedSearches) => {
            this.searches = searches;

            this.spinner.stopLoading();
        });
    }

    loadSearch(index: number) {
        this.spinner.startLoading();

        this.insightSearches.loadSearch(index).then(() => {
            this.spinner.stopLoading();
            this.dialogRef.close();
        });
    }

    deleteSearch(index: number) {
        this.spinner.startLoading();

        this.insightSearches.deleteSearch(index).then(() => {
            this.init();
        });
    }
}
