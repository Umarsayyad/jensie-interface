import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class AuthService {
    public getAuthorizationToken(): string | null {
        return sessionStorage.getItem('token') || null;
        // return {"Authorization": `token ${token}`};
    }
}
