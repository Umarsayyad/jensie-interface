import * as Highcharts from 'highcharts';
import { defaultColors } from './colors';

export function setGradientColors() {
    Highcharts.setOptions({
        colors: defaultColors,
    });

    Highcharts.setOptions({
        colors: Highcharts.map(Highcharts.getOptions().colors, (color) => {
            return {
                linearGradient: {
                    x1: 0,
                    x2: 0,
                    y1: 0,
                    y2: 1,
                },
                stops: [
                    [
                        0,
                        Highcharts.color(color)
                            .brighten(+0.1)
                            .get('rgb'),
                    ],
                    [1, color],
                ],
            };
        }),
    });
}
