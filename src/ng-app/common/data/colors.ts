export const customSet3 = [
    '#8dd3c7',
    '#ebeb8c',
    '#bebada',
    '#fb8072',
    '#80b1d3',
    '#fdb462',
    '#b3de69',
    '#fccde5',
    '#d9d9d9',
    '#bc80bd',
    '#ccebc5',
    '#ffed6f',
];

export const defaultColors = [
    '#082A73',
    '#EF7D7D',
    '#B9DEA9',
    '#FFC167',
    '#67C4E7',
    '#F5EDAE',
    '#DD6259',
    '#91BED4',
    '#FF7011',
    '#86C974',
    '#AC5F70',
    '#6FB5BB',
    '#F4E361',
    '#DAE8F5',
    '#FC9E6F',
    '#8F5298',
    '#AA2C22',
];

export const heatmapColors = [
    '#705250',
    '#B7654E',
    '#AF6E2D',
    '#C28449',
    '#D1A557',
    '#E0C366',
    '#9EA43A',
    '#80AF39',
    '#62AF39',
    '#68BD39',
    '#8EE160',
    '#A8F57E',
];

export const dark2 = ['#1b9e77', '#d95f02', '#7570b3', '#e7298a', '#66a61e', '#e6ab02', '#a6761d', '#666666'];

export const set2 = ['#66c2a5', '#fc8d62', '#8da0cb', '#e78ac3', '#a6d854', '#ffd92f', '#e5c494', '#b3b3b3'];

export const rowChartColors = defaultColors;
