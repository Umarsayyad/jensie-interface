export const PROGRESS_COMPLETE_VALUE = 100;

export const LOWEST_LEVEL = 1;
export const HIGHEST_LEVEL = 5;
