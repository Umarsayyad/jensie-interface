export enum DateRangeType {
    ThisWeek = 'ThisWeek',
    LastWeek = 'LastWeek',
    LastSevenDays = 'LastSevenDays',
    ThisMonth = 'ThisMonth',
    LastMonth = 'LastMonth',
    LastThirtyDays = 'LastThirtyDays',
    LastNinetyDays = 'LastNinetyDays',
    ThisQuarter = 'ThisQuarter',
    LastQuarter = 'LastQuarter',
    LastYear = 'LastYear',
    YearToDate = 'YearToDate',
}
