export interface IGenericDialogData {
    title?: string;
    message: string;
    buttonText: { ok: string; cancel?: string };
}
