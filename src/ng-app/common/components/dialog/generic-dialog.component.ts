import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { IGenericDialogData } from './generic-dialog-data.interface';

@Component({
    selector: 'generic-dialog',
    templateUrl: 'generic-dialog.component.html',
})
export class GenericDialogComponent {
    title: string;
    message: string;
    cancelBtnText: string;
    okButtonText: string;

    constructor(@Inject(MAT_DIALOG_DATA) private data: IGenericDialogData) {
        this.message = data.message;
        this.title = data.title;
        this.okButtonText = data.buttonText.ok;
        this.cancelBtnText = data.buttonText.cancel;
    }
}
