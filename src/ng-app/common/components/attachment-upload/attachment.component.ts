import { Component, ViewChild, Inject, Input, Optional } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ID } from '../../models';
import { LoadingContentComponent } from '..';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DocType } from '../../../cmmc-questionnaire/models/doc-type';

@Component({
    selector: 'attachment',
    templateUrl: 'attachment.component.html',
    styleUrls: ['attachment.component.less'],
})
export class AttachmentComponent {
    queuedAttachments: File[] = [];
    DocType = DocType;

    constructor(private fb: FormBuilder, @Optional() @Inject(MAT_DIALOG_DATA) private dialogData: ID[]) {}

    queueAttachment(files: FileList) {
        for (let i = 0; i < files.length; i++) {
            this.queuedAttachments.push(files[i]);
        }
    }

    done(): File[] {
        return this.queuedAttachments;
    }
}
