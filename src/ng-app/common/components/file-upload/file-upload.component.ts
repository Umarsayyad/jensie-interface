import { Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';

@Component({
    selector: 'file-upload',
    templateUrl: './file-upload.component.html',
    styleUrls: ['./file-upload.component.less'],
})
export class FileUploadComponent implements OnDestroy {
    @Output()
    upload = new EventEmitter<FileList | File>();
    @Input('multiple-files')
    multiple: boolean;
    isDragging = false;

    ngOnDestroy() {
        this.upload.complete();
    }

    fileChanged($event: DragEvent | any) {
        if (!$event) return;
        $event.preventDefault();
        let files: FileList | null = null;

        if ($event instanceof DragEvent) {
            files = $event.dataTransfer.files;
        } else if ($event.target.files) {
            files = $event.target.files;
        }

        if (files) {
            this.upload.emit(this.multiple ? files : files[0]);
        }

        this.isDragging = false;
    }

    dragover($event: DragEvent) {
        //Disables the browser's default functionality and enables dropping files
        $event.preventDefault();
        $event.stopPropagation();

        this.isDragging = true;
    }

    dragleave() {
        this.isDragging = false;
    }
}
