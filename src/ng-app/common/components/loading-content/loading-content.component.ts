import { Component, Input, OnInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';

@Component({
    selector: 'loading-content',
    templateUrl: './loading-content.component.html',
    styleUrls: ['./loading-content.component.less'],
})
export class LoadingContentComponent implements OnInit, AfterViewInit {
    private static readonly DEFAULT_MSG = 'Please Wait...';
    private readonly mintime = 500;
    private customMsg = false;
    private isloading = false;
    private loading$: Promise<any> = null;
    private startTime: number;
    initialized = false;

    @Input() msg = '';
    //option to turn off random loading msgs
    @Input() random = true;
    //option to turn off fun loading msgs
    @Input() fun = true;
    //option to turn on a delay (to see the loader a min amount of time)
    @Input() delay = false;

    constructor(private changeDetectorRef: ChangeDetectorRef) {}

    ngOnInit() {
        if (this.msg) this.customMsg = true;
        this.reset();
    }

    ngAfterViewInit() {
        this.initialized = true;
    }

    public stopLoading: () => void;
    public cancelLoading: () => void;
    public startLoading(): () => void {
        if (this.isloading) {
            this.error('Start');
            return;
        }

        this.isloading = true;
        this.startTime = Date.now();
        this.loading$ = new Promise((resolve, reject) => {
            this.stopLoading = () => {
                if (this.delay) {
                    let delay = this.mintime - (Date.now() - this.startTime);
                    setTimeout(resolve, delay);
                } else {
                    resolve();
                }
            };

            this.cancelLoading = (err?) => {
                reject(err);
            };
        })
            .catch(console.error)
            .finally(() => {
                // console.log((Date.now() - this.startTime) / 1000);
                this.reset();
            });

        //kick off change detection (fixes annoying error msg)
        this.changeDetectorRef.detectChanges();
        return this.stopLoading;
    }

    public isLoading(): boolean {
        return this.isloading;
    }

    public getPromise(): Promise<void> | null {
        return this.loading$;
    }

    private reset() {
        this.isloading = false;
        this.msg = this.generateLoadingMsg();
        this.loading$ = null;
        this.stopLoading = this.error('Stop');
        this.cancelLoading = this.error('Cancel');
    }

    private generateLoadingMsg(): string {
        if (!this.random) return LoadingContentComponent.DEFAULT_MSG;
        if (this.customMsg) return this.msg;

        const choose = (strs: string[]) => {
            let index = Math.floor(Math.random() * strs.length);
            return strs[index];
        };

        const loadingMSgs = ['Hold on...', 'Initializing...', 'Just a sec...', 'Loading...', 'Please Stand By...', 'Please Wait...', 'Processing...'];

        const funMsgs = [
            'Brace yourself -- winter is coming.',
            'Crashing a wedding...',
            'Dividing by zero...',
            'Downloading more RAM...',
            'Generating witty dialog...',
            'Making some coffee...',
            'Mining some bitcoins...',
            'Patience is a virtue',
            'Proving P=NP...',
            'Popping some popcorn...',
            'Repositioning satellites...',
            'Running with scissors...',
            'Saving the princess...',
            'Searching for plot device...',
            'Shovelling coal into the server...',
            'Storming the castle...',
            'Slaying a dragon...',
            'Walking the cat...',
        ];

        // ~10% chance
        if (this.fun && Math.random() * 100 < 10) {
            console.log('rare');
            return choose(funMsgs);
        } else {
            return choose(loadingMSgs);
        }
    }

    private error(cmd: string) {
        return () => console.error(`Error: Unable to '${cmd}' because nothing is loading.`);
    }
}
