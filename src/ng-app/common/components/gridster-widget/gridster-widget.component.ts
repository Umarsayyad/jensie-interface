import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import catotheme from '@cato/config/themes/catotheme';
import { ValidTheme } from '@cato/config/themes/valid-themes';

import { GlobalDataService } from '../../../ngjs-upgrade/globalDataService';
import { LoadingContentComponent } from '../loading-content/loading-content.component';
// import { Crossfilter } from "crossfilter2";
// import { RowChart, rowChart } from "dc";
// import { IWidget } from "../../../models/widget.interface";
// import { DashboardWidgets } from "../../../services/dashboard-widgets.service";
// import { InsightWidgetsService } from "../insight-widgets.service";
// import { id } from "./industry-breakdown.widget";

@Component({
    selector: 'cato-widget',
    templateUrl: './gridster-widget.component.html',
    // styleUrls: ["./gridster-widget.less"],
}) //implements OnInit, AfterViewInit
export class GridsterWidgetComponent {
    @Output() close = new EventEmitter<MouseEvent>();
    @ViewChild('spinner') spinner: LoadingContentComponent;

    isInsight = catotheme.name === ValidTheme.jensie && this.gdata.isInsight;

    constructor(private gdata: GlobalDataService) {}

    ngOnDestroy() {
        this.close.complete();
    }

    hideWidget($event: MouseEvent) {
        this.close.emit($event);
    }

    //pass through all the spinner's public functions
    public startLoading(): () => void {
        return this.spinner.startLoading();
    }
    public stopLoading() {
        return this.spinner.stopLoading();
    }
    public cancelLoading() {
        return this.spinner.cancelLoading();
    }
    public isLoading(): boolean {
        return this.spinner.isLoading();
    }
    public getPromise(): Promise<void> | null {
        return this.spinner.getPromise();
    }
}
