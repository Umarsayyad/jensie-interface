import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { PreferencesService } from '../../../preferences/services/preferences.service';

export interface ITargetDisplay {
    type: string;
    alias: string;
    value: string;
}

@Component({
    selector: 'target-display',
    templateUrl: './target-display.component.html',
    styleUrls: ['./target-display.component.less'],
})
export class TargetDisplayComponent implements OnInit, OnDestroy {
    @Input() target: ITargetDisplay;
    isAliasShown = false;
    private onDestroy$ = new Subject();

    constructor(private preferencesService: PreferencesService) {}

    ngOnInit() {
        this.preferencesService
            .getTargetDisplayPrefSubscription()
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((preference) => {
                this.isAliasShown = preference;
            });
    }

    ngOnDestroy(): void {
        this.onDestroy$.next();
        this.onDestroy$.complete();
    }
}
