import { CommonModule, DatePipe } from '@angular/common';
import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';

import { AttachmentComponent } from './components/attachment-upload/attachment.component';
import { GenericDialogComponent } from './components/dialog/generic-dialog.component';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import { GridsterWidgetComponent } from './components/gridster-widget/gridster-widget.component';
import { LoadingContentComponent } from './components/loading-content/loading-content.component';
import { SpinnerComponent } from './components/loading-content/spinner/spinner.component';
import { TargetDisplayComponent } from './components/target-display/target-display.component';
import { TargetIconDirective } from './directives/target-icon.directive';
import { YesNoPipe } from './pipes/yes-no.pipe';
import { BackupService } from './services';
import { AttachmentService } from './services/attachment.service';
import { CatoTableModule } from './table';

@NgModule({
    declarations: [
        FileUploadComponent,
        AttachmentComponent,
        GenericDialogComponent,
        GridsterWidgetComponent,
        LoadingContentComponent,
        SpinnerComponent,
        TargetDisplayComponent,
        TargetIconDirective,
        YesNoPipe,
    ],
    entryComponents: [
        FileUploadComponent,
        AttachmentComponent,
        GridsterWidgetComponent,
        LoadingContentComponent,
        GenericDialogComponent,
        SpinnerComponent,
    ],
    imports: [CommonModule, FormsModule, CatoTableModule, MatCardModule, ReactiveFormsModule, MatTooltipModule, MatCheckboxModule, MatDialogModule],
    exports: [
        AttachmentComponent,
        CatoTableModule,
        DatePipe,
        FileUploadComponent,
        GenericDialogComponent,
        GridsterWidgetComponent,
        LoadingContentComponent,
        SpinnerComponent,
        TargetDisplayComponent,
        YesNoPipe,
    ],
    providers: [DatePipe, YesNoPipe],
})
export class CatoCommonModule {
    constructor(@Optional() @SkipSelf() parentModule: CatoCommonModule) {
        if (parentModule) {
            throw new Error('CatoCommonModule is already loaded.  Import it in the CatoAppModule only');
        }
    }

    static forRoot(): ModuleWithProviders<CatoCommonModule> {
        return {
            ngModule: CatoCommonModule,
            providers: [BackupService, AttachmentService],
        };
    }
}
