import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { saveAs } from 'file-saver';

import { config } from '@cato/config';
import { Observable } from 'rxjs';
import { IAttachment } from '../models/attachment.interface';

@Injectable()
export class AttachmentService {
    constructor(private $http: HttpClient) {}

    public uploadAttachment(attachment: File, messageId): Observable<IAttachment> {
        let fd = new FormData();
        fd.append('file', attachment);
        fd.append('name', attachment.name);
        fd.append('type', 'message');
        fd.append('message', messageId);

        return this.$http.post<IAttachment>(`${config.apiPath}attachment`, fd);
    }

    public downloadAttachment(attachment: IAttachment) {
        this.$http.get(`${config.apiPath}attachment/${attachment.id}/download`, { responseType: 'blob' }).subscribe((res) => {
            saveAs(res, attachment.name);
        });
    }

    private headers() {
        const token = sessionStorage.getItem('token');
        return { Authorization: `token ${token}` };
    }
}
