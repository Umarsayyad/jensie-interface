import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { config } from '@cato/config';

@Injectable()
export class BackupService {
    constructor(private $http: HttpClient) {}

    public downloadBackup(timestamp: string): Promise<ArrayBuffer> {
        return this.$http.get(config.apiPath + 'backups/?timestamp=' + timestamp, { responseType: 'arraybuffer' }).toPromise();
    }

    public backUpNow(): Promise<any> {
        return this.$http.post(config.apiPath + 'backups/?backup_now=true', {}, { headers: this.headers() }).toPromise();
    }

    public list(): Promise<any> {
        return this.$http.get(config.apiPath + 'backups/').toPromise();
    }

    private headers() {
        const token = sessionStorage.getItem('token');
        return { Authorization: `token ${token}` };
    }
}
