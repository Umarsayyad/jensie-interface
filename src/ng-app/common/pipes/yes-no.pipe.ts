import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'yesNo' })
export class YesNoPipe implements PipeTransform {
    transform(value: boolean, format?: YesNoFormat): string {
        switch (format) {
            case YesNoFormat.Short:
                return value ? 'Y' : 'N';
            case YesNoFormat.Long:
                return value ? 'Yes' : 'No';
        }
    }
}

export enum YesNoFormat {
    Long,
    Short,
}
