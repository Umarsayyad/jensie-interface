import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { GridApi, ColumnApi, IServerSideDatasource, GridOptions, GridReadyEvent, GridOptionsWrapper } from 'ag-grid-community';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { DataObject, ID, getId } from '../../models';
import { defaultColumn } from '../data/default-column';
import { IAction } from '../models/action.interface';
import { ICatoTable } from '../models/cato-table.interface';
import { IColumn } from '../models/column.interface';
import { ICustomTableOptions, ITableOptions, RowModel } from '../models/table-options.interface';

@Component({
    selector: 'cato-table',
    templateUrl: './cato-table.component.html',
    styleUrls: ['cato-table.component.less'],
})
export class CatoTableComponent<T extends DataObject> implements OnInit, OnDestroy, ICatoTable<T> {
    private onDestroy$ = new Subject<void>();

    options: GridOptions = {};
    private customOptions: Partial<ICustomTableOptions<T>> = {};

    //This represents the default table options
    private defaultOptions: ICustomTableOptions<T> = {
        pageOptions: [25, 10, 50, 100, 250],
        multiselect: true,
        rowModelType: RowModel.ServerSide,
    };
    private infiniteModelDefaults: GridOptions = {
        cacheBlockSize: 250, //Same as or greater than the max in pageOptions
        cacheOverflowSize: 2,
        maxConcurrentDatasourceRequests: 2,
        infiniteInitialRowCount: 1,
        maxBlocksInCache: 2,
    };

    private api: GridApi | null = null;
    private colApi: ColumnApi | null = null;
    private isReady = false;
    private defaultColumn = { ...defaultColumn };

    @Input('table-options')
    tableOptions: ITableOptions<T> = {};

    @Input('page-options')
    pageOptions: number[];

    @Input()
    columns: Array<IColumn<T>>;

    @Input()
    actions: Array<IAction<T>>;

    @Input('bulk-actions')
    bulkActions: Array<IAction<T>>;

    @Input('multi-select')
    multiselect: boolean;

    @Input()
    datasource: Observable<IServerSideDatasource>;
    @Input('row-data')
    rowData: DataObject[]; //assume for now that it won't change...

    //This is also a way to expose the gridApi and gridColumnApi
    @Output()
    ready = new EventEmitter<GridReadyEvent>();

    @Output()
    selectionChanged = new EventEmitter<Array<T>>();

    ngOnInit() {
        this.customOptions = { ...this.defaultOptions, ...this.tableOptions.custom };

        //Combine all the options into 1 object
        //Priority (high to low) is inputs --> options --> cato-defaults --> grid-defaults
        if (this.pageOptions) this.customOptions.pageOptions = this.pageOptions;
        if (this.columns) this.customOptions.columns = this.columns;
        if (this.actions) this.customOptions.actions = this.actions;
        if (this.bulkActions) {
            this.customOptions.bulkActions = this.bulkActions;
            this.customOptions.multiselect = true;
        }

        //Map our `multiselect` into ag-grids's `rowSelection="multiple"`
        if (this.multiselect || this.multiselect === false) {
            this.options.rowSelection = this.multiselect ? 'multiple' : 'single';
        } else if (this.customOptions.multiselect) {
            this.options.rowSelection = 'multiple';
        } else if (this.customOptions.multiselect === false) {
            this.options.rowSelection = 'single';
        }

        switch (this.customOptions.rowModelType) {
            case RowModel.ServerSide:
                //grab infinite RowModel options from what's passed in
                let infiniteModelOptions: GridOptions = {};
                for (let [key, defaultVal] of Object.entries(this.infiniteModelDefaults)) {
                    infiniteModelOptions[key] = this.tableOptions[key] || defaultVal;
                }

                this.options = { ...infiniteModelOptions, ...this.options };

                //remove headerCheckboxSelection since it's not supported for Inifinite row model
                //TODO: (CATO-2199) support checkboxes for infinite model
                this.customOptions.checkboxes = false;
                break;

            case RowModel.ClientSide:
                break; //no-op
            default:
                break; //no-op
        }

        if (this.customOptions.pageOptions) this.options.paginationPageSize = this.customOptions.pageOptions[0];
        else this.options.paginationPageSize = this.defaultOptions.pageOptions[0];

        //remove checkboxes when checkboxes=false
        if (this.customOptions.checkboxes === false) {
            this.defaultColumn.headerCheckboxSelection = () => false;
            this.defaultColumn.checkboxSelection = () => false;

            let cols = [];
            for (let c of this.customOptions.columns) {
                let col = { ...c };
                if (col.headerCheckboxSelection) delete col.headerCheckboxSelection;
                cols.push(col);
            }

            (this.options as any).columns = cols;
        }

        this.options = {
            ...this.tableOptions,
            ...this.options,
            getRowNodeId: (e) => getId(e).toString(),
            defaultColDef: this.defaultColumn,
        };

        //map columns passed in as IColumn (cato) into Column (ag-grid)
        if ((this.options as any).columns) {
            this.options.columnDefs = [];
            for (let c of (this.options as any).columns) {
                let cDef = { ...c };
                cDef.headerName = c.header;
                if ('header' in cDef) delete cDef.header;
                this.options.columnDefs.push(cDef);
            }

            delete (this.options as any).columns;
        }

        //final clean-up -- prevents ag-grid from logging warnings about unknown/invalid properties
        if ('custom' in this.options) delete (this.options as any).custom;
    }

    ngOnDestroy() {
        this.onDestroy$.next();
        this.onDestroy$.complete();
        this.ready.complete();
    }

    onReady(p: GridReadyEvent) {
        this.api = p.api;
        this.colApi = p.columnApi;
        this.isReady = true;

        if (this.options.rowModelType === RowModel.ServerSide && this.datasource) {
            this.datasource.pipe(takeUntil(this.onDestroy$)).subscribe((datasource) => {
                this.api.setServerSideDatasource(datasource);
            });
        }

        if (this.customOptions.sizeColumnsToFit) {
            this.api.sizeColumnsToFit();
        }

        this.ready.emit(p);
        this.ready.complete();
    }

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.ready.toPromise().then(() => this.api.sizeColumnsToFit());
    }

    //@override
    public getSelectedRows(): T[] | never {
        if (!this.isReady) throw new Error('Trying to get rows before cato-table is ready!');
        if (!this.api) throw new Error('Table ready but api not set!');

        return this.api.getSelectedRows();
    }

    //@override
    public selectRowById(id: ID): void | never {
        if (!this.ready) throw new Error('Trying to select row before cato-table is ready!');
        if (!this.api) throw new Error('Table ready but api not set!');

        this.api.getRowNode(id.toString()).setSelected(true);
    }
}
