import { ICellRendererParams } from 'ag-grid-community';

export interface IActionCellRendererParams extends ICellRendererParams {
    actions: Array<IGridAction>;
}

export interface IGridAction {
    label: string;
    callback: (params: ICellRendererParams) => void;
    disabled?: (params: ICellRendererParams) => boolean;
    style?: { [cssProp: string]: any };
}
