import { Component } from '@angular/core';
import { IBarWithNumberCellRendererParams } from './bar-with-number-cell-renderer-params.interface';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
    selector: 'bar-cell-renderer',
    templateUrl: './bar-with-number-cell-renderer.component.html',
})
export class BarWithNumberCellRendererComponent implements ICellRendererAngularComp {
    params: IBarWithNumberCellRendererParams;

    agInit(params: IBarWithNumberCellRendererParams): void {
        this.params = params;
    }

    refresh(params: IBarWithNumberCellRendererParams): boolean {
        return false;
    }
}
