import { ICellRendererParams } from 'ag-grid-community';

export interface IBarWithNumberCellRendererParams extends ICellRendererParams {
    barHeight?: string;
    calculateBarColor(value: number): string;
    calculateOpacity(value: number): number;
    calculateWidth(value: number): string;
}
