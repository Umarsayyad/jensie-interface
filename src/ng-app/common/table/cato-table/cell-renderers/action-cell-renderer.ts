import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { ICellRendererParams } from 'ag-grid-community';
import { IActionCellRendererParams } from './action-cell-renderer-params.interface';

@Component({
    selector: 'action-cell-renderer',
    templateUrl: './action-cell-renderer.component.html',
})
export class ActionCellRendererComponent implements ICellRendererAngularComp {
    params: IActionCellRendererParams;

    agInit(params: IActionCellRendererParams): void {
        this.params = params;
    }

    refresh(params: ICellRendererParams): boolean {
        return false;
    }
}
