import { IColumn } from '../models/column.interface';
import { DataObject } from '../../models';

//TODO: (CATO-2200) figure out if this is needed and/or if we can use it in some way
export enum SupportedColTypes {
    number = 'numericColumn',
    date = 'dateColumn',
    nonEditableColumn = 'nonEditableColumn',
    rightAligned = 'rightAligned',
}

export const columnTypes: Partial<{ [T in SupportedColTypes]: Partial<IColumn<DataObject>> }> = {
    nonEditableColumn: {
        editable: false,
    },
    dateColumn: {
        filter: 'agDateColumnFilter',
    },
};
