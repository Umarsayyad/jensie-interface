import { IColumn } from '../models/column.interface';
import { DataObject } from '../../models';

//A default column definition with properties that get applied to every column
export const defaultColumn: Partial<IColumn<DataObject>> = {
    // set every column width
    width: 100,
    // make every column not editable
    editable: false,
    // make every column use default ('text'?) filter by default
    filter: true, //'agTextColumnFilter',
    // make every column sortable by default
    sortable: true,
    // make every column resizable by default
    resizable: true,

    //Use checkbox for selection (only if first column)
    headerCheckboxSelection: isFirstColumn,
    checkboxSelection: isFirstColumn,
};

function isFirstColumn(params: any) {
    //This will only be supported by ag-grid; not sure how we'd do this with another lib.
    let displayedColumns = params.columnApi.getAllDisplayedColumns();
    let thisIsFirstColumn = displayedColumns[0] === params.column;
    return thisIsFirstColumn;
}
