import { HttpParams } from '@angular/common/http';
import { IServerSideGetRowsRequest } from 'ag-grid-community';

export function makePaginationParams(req: IServerSideGetRowsRequest, existingParams?: HttpParams): HttpParams {
    if (!existingParams) {
        existingParams = new HttpParams();
    }

    return existingParams
        .set('pager', 'limit')
        .set('offset', req.startRow.toString())
        .set('limit', (req.endRow - req.startRow).toString());
}
