import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { AgGridModule } from 'ag-grid-angular';
import 'ag-grid-enterprise';

import { CatoTableComponent } from './cato-table/cato-table.component';
import { ActionCellRendererComponent } from './cato-table/cell-renderers/action-cell-renderer';

@NgModule({
    providers: [],
    declarations: [CatoTableComponent, ActionCellRendererComponent],
    //prettier-ignore
    imports: [
        MatMenuModule,
        MatButtonModule,
        HttpClientModule,
        CommonModule,
        AgGridModule.withComponents([ActionCellRendererComponent])
    ],
    exports: [CatoTableComponent],
})
export class CatoTableModule {}
