export { CatoTableModule } from './cato-table.module';

export { CatoTableComponent } from './cato-table/cato-table.component';

export { ICatoTable } from './models/cato-table.interface';
export { ITableOptions } from './models/table-options.interface';
export { IColumn } from './models/column.interface';
export { IAction } from './models/action.interface';

export { columnTypes, SupportedColTypes } from './data/column-types';

export * from './utilities';
