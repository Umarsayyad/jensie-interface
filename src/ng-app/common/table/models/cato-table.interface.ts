import { EventEmitter } from '@angular/core';
import { GridReadyEvent, IServerSideDatasource } from 'ag-grid-community';
import { Observable } from 'rxjs';

import { DataObject } from '../../models';
import { IColumn } from './column.interface';
import { IAction } from './action.interface';
import { ITableOptions } from './table-options.interface';

//TODO: (CATO-2101) service for saving/restoring column state

export interface ICatoTable<T extends DataObject> {
    //@input
    tableOptions: ITableOptions<T>;

    //@input
    pageOptions: number[];

    //@input
    columns: Array<IColumn<T>>;

    //@input
    actions: Array<IAction<T>>;

    //@input
    bulkActions: Array<IAction<T>>;

    //@input
    multiselect: boolean;

    //@input
    datasource: Observable<IServerSideDatasource>;
    //@input
    rowData: DataObject[];

    //@output
    ready: EventEmitter<GridReadyEvent>;

    //@output
    selectionChanged: EventEmitter<Array<T>>;

    /*public*/ getSelectedRows(): T[];
}
