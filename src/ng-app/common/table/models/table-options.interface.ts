import { DataObject } from '../../models';
import { IColumn } from './column.interface';
import { IAction } from './action.interface';
import { GridOptions } from 'ag-grid-community';

export enum RowModel {
    ClientSide = 'clientSide',
    ServerSide = 'serverSide',
}

export interface ITableOptions<T extends DataObject> extends GridOptions {
    custom?: ICustomTableOptions<T>;
}
export interface ICustomTableOptions<T extends DataObject> {
    pageOptions?: number[]; // A list of integers representing available page sizes for the table. Default size is the first entry. Sizes will be sorted for display. Default is [25, 10, 50, 100, 250]

    // I don't think we need/want this because the IDataSource should handle filtering/sorting
    // processParams: any;     // A callback that gets passed and returns a JSON representation of the parameters to be added to the URL. This will support modifying filtering and ordering determined outside the table itself, such as the advanced filtering above the operations and findings tables.

    columns?: Array<IColumn<T>>; // A list of objects defining how to display each field.

    actions?: Array<IAction<T>>; // A list of objects describing actions

    bulkActions?: Array<IAction<T>>; // Same as actions. If any bulk actions are defined, checkboxes are added and the table becomes selectable.

    multiselect?: boolean; // Turn multiselect on or off

    checkboxes?: false | undefined; //TODO: (CATO-2199) accept true as a possible value and implement adding checkboxes for infinite model

    sizeColumnsToFit?: boolean; // If set to true the columns will be auto sized when table is loaded

    rowModelType?: RowModel;
}
