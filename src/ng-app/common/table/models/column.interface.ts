import { DataObject } from '../../models';
import { SupportedColTypes } from '../data/column-types';

export interface IColumn<T extends DataObject> {
    field: keyof T; //Name of the field in the model.
    header: string; //Display name for the field. Defaults to the `label` value from OPTIONS. If not identified in OPTIONS (in the case of non-model fields), it will fall back to the value provided for `field`.
    editable?: boolean;
    filter?: any; //Added to support ag-grid's implementation
    filterable?: boolean; //Identifies the column as being filterable, adding an input to the top of the table. Defaults to true.
    sortable?: boolean; //Identifies the column as being sortable. Defaults to true.
    comparator?: (...args: any[]) => number;
    resizable?: boolean; //Identifies the column as being resizable. Defaults to true.
    type?: SupportedColTypes;
    width?: number;
    flex?: any;
    minWidth?: number;
    maxWidth?: number;
    sort?: string;
    filterParams?: any;
    cellRenderer?: any;
    cellRendererFramework?: any;
    cellRendererParams?: any;
    headerCheckboxSelection?: (p: any) => boolean | boolean;
    checkboxSelection?: (p: any) => boolean | boolean;
    cellStyle?: any;
    tooltipValueGetter?: any;
    getDisplay?: (row: T) => string; //Callback given the object and returns the string to display. Useful, for instance, to convert a customer ID to a name. Defaults to a string for the type returned by OPTIONS.
    getRenderer?: (row: T) => any; //Returns a renderable element to display the data. Cannot be used in conjunction with getDisplay.
}
