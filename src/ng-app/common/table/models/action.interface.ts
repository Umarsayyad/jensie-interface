import { DataObject } from '../../models';

export interface IAction<T extends DataObject> {
    label: string; // Display name for the action.
    icon: string; // Icon for the action.
    class: string; // Class to apply to the action button.
    action: Function; // Callback to call when the action is called.
    isActionable: (row: T) => boolean; // A callback that gets passed a row object and returns a list of which actions are applicable to it.  E.g., an approved finding doesn’t need an 'approve' action.
}
