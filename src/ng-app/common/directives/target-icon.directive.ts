import { Directive, ElementRef, Injector, Input } from '@angular/core';
import { UpgradeComponent } from '@angular/upgrade/static';

@Directive({
    selector: 'target-icon',
})
export class TargetIconDirective extends UpgradeComponent {
    constructor(elementRef: ElementRef, injector: Injector) {
        super('targetIcon', elementRef, injector);
    }

    @Input() set type(url: string) {}
}
