/**
 * For enums with numerical values, TS produces an object that contains a two-way mapping
 * between the enum key and value (key-->value and value-->key)
 *
 * For example, this enum:
 * enum MotifIntervention
 * {
 *     Intrusion,
 *     Identification=2,
 * }
 *
 * Produces JS like so (some lines removed for brevity):
 * MotifIntervention[MotifIntervention["Intrusion"] = 0] = "Intrusion";
 * MotifIntervention[MotifIntervention["Identification"] = 2] = "Identification";
 *
 * resulting in an object that looks like:
 * {
 *     0: "Intrusion",
 *     1: "Identification",
 *     Intrusion: 0,
 *     Identification: 1,
 * }
 *
 * getEnumValues takes this into account and discards any values that match the keys of the enum
 */

export function getEnumValues<T>(anEnum: object): T[] {
    const keys = new Set(Object.keys(anEnum));
    return Object.values(anEnum).filter((value) => !keys.has(value));
}
