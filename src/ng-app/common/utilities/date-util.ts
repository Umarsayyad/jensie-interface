export class DateUtil {
    public static compareDate(left: string, right: string): number {
        let rightDate: Date = right ? new Date(right) : null;
        let leftDate: Date = left ? new Date(left) : null;

        if (leftDate < rightDate) {
            return -1;
        } else if (leftDate > rightDate) {
            return 1;
        } else {
            return 0;
        }
    }
}
