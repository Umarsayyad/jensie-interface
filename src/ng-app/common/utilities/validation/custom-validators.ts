import { AbstractControl, ValidationErrors } from '@angular/forms';
import { Patterns } from './patterns';

export class CustomValidators {
    public static cidrBlock(control: AbstractControl): ValidationErrors | null {
        if (control.value) {
            return Patterns.cidrBlock.test(control.value) ? null : { cidrBlock: true };
        }

        return null;
    }

    public static ipAddress(control: AbstractControl): ValidationErrors | null {
        if (control.value) {
            return Patterns.ipAddress.test(control.value) ? null : { ipAddress: true };
        }

        return null;
    }
}
