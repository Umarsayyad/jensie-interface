const ipSegment = '(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])';
const cidrPrefix = '(3[0-2]|[0-2]?[0-9])'; // 0-32

export class Patterns {
    public static readonly cidrBlock = new RegExp(`^${ipSegment}\\.${ipSegment}\\.${ipSegment}\\.${ipSegment}\\/${cidrPrefix}$`);
    // patternError='This field must be formatted as a CIDR block';

    public static readonly ipAddress = new RegExp(`^${ipSegment}\\.${ipSegment}\\.${ipSegment}\\.${ipSegment}$`);
    // patternError2='This field must be formatted as an IP address';
}
