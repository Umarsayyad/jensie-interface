import { Cidr, IpAddress, IpRange as Range } from 'cidr-calc';

export interface IpRange {
    firstIp: string;
    lastIp: string;
}

export interface CidrCalculationResult extends IpRange {
    range: string;
    netmask?: string;
    wildcard?: string;
    totalHosts: number;
}

export function calculateIpRange(ip: string, prefixLen: number): IpRange {
    let range = new Cidr(IpAddress.of(ip), prefixLen).toIpRange();
    const retval = {
        firstIp: range.startIpAddr.toString(),
        lastIp: range.endIpAddr.toString(),
    };

    return retval;
}

export function calculateTotalHosts(prefixLen: number): number {
    const IPv4_MAX_BITS = 32;
    return Math.pow(2, IPv4_MAX_BITS - prefixLen);
}

export function calculateCidrBlock(startIp: string, endIp: string): string[] {
    let ipRange = new Range(IpAddress.of(startIp), IpAddress.of(endIp));
    return ipRange.toCidrs().map((c) => c.toString());
}
