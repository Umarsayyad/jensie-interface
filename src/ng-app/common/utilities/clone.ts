export function cloneJson<T extends object>(obj: T): T | never {
    if (!(obj instanceof Object)) throw new Error(`Cloning a non-object: ${obj}`);
    return JSON.parse(JSON.stringify(obj));
}
