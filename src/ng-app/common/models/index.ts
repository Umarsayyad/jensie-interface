export * from './data-object.interface';
export { Deletable } from './deletable.type';
export { IPaginatedResult } from './paginated-result.interface';
export { Resource } from './resource.class';
