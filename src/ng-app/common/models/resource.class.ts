import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable, Component } from '@angular/core';
import { Observable } from 'rxjs';

import { config } from '@cato/config';
import { DataObject } from '.';
import { getId, ID } from './data-object.interface';
import { type } from 'os';

type CatoHttpParams = HttpParams | { [param: string]: string | string[] };

export abstract class Resource<T extends DataObject> {
    protected get url(): string {
        console.warn('Using the base URL for CRUD calls.  Be careful!');

        return '';
    }

    constructor(protected http: HttpClient) {}

    //TODO: what do create, update, and delete actually return??
    public create(thing: T): Observable<T> {
        return this.http.post<T>(`${this.url}`, thing);
    }

    public get(id: ID): Observable<T> {
        return this.http.get<T>(`${this.url}/${id}`);
    }

    public getAll(params?: CatoHttpParams): Observable<Array<T>> {
        return this.http.get<Array<T>>(`${this.url}`, { params });
    }

    public update(thing: T): Observable<T> {
        let id = getId(thing);
        return this.http.put<T>(`${this.url}/${id}`, thing);
    }

    public delete(thing: T): Observable<T> {
        let id = getId(thing);
        return this.http.delete<T>(`${this.url}/${id}`);
    }

    public patch(thing: Partial<T> & DataObject): Observable<T> {
        let id = getId(thing);
        return this.http.patch<T>(`${this.url}/${id}`, thing);
    }

    public options() {
        return this.http.options(this.url);
    }

    // protected helper(paths: Array<{id: number, path: string}>): string
    // {
    //     let strBuilder=[];
    //     for(let {id, path} of paths)
    //     {
    //         strBuilder.concat([path, id]);
    //     }

    //     return strBuilder.join('/');
    // }
}

/////////////////////////////////////////////////////////////
/////////// This is for demo purposes/reference /////////////
/////////////////////////////////////////////////////////////

// class Finding {id: number /* other stuff here... */}

// @Injectable({providedIn: "root"})
// class FindingResource extends Resource<Finding>
// {
//     protected get url(): string
//     {
//         return `${config.apiPath}/findings`;
//     }

//     public approve(finding: Finding): Observable<boolean>
//     {
//         return this.http.post<boolean>(`${this.url}/${finding.id}/approve`, finding);
//     }
// }

// @Component(
// {
//     selector: "findings-component",
//     template: "<h2>FINDINGS!!!!</h2>",
//     styleUrls: [],
// })
// class FindingComponent
// {
//     constructor(private findings: FindingResource)
//     {
//         //use `findings` to do something
//         findings.getAll();
//     }
// }
