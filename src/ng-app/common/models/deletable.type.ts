export type Deletable = {
    deleted?: boolean;
};
