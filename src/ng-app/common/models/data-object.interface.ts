export type ID = string | number; //makes this easy to change in the future

type IdProp = { id: ID };
type IdFunc = { getId: () => ID };

//Type Guard utilities
export function hasIdProp(obj: DataObject): obj is IdProp {
    return 'id' in obj && (obj as IdProp).id != undefined;
}
export function hasIdFunc(obj: DataObject): obj is IdFunc {
    return 'getId' in obj && (obj as IdFunc).getId() != undefined;
}

//ID getter utility
export function getId(d: DataObject): ID {
    return hasIdProp(d) ? d.id : d.getId();
}

export type DataObject = IdProp | IdFunc;
