import { ID } from '../../common';

export interface IAttachment {
    id: ID;
    attachment: string;
    name: string;
    timestamp: string;
    answers: ID[];
    customer: ID;
}
