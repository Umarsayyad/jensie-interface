import { Component, OnDestroy, OnInit } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { PreferencesService } from '../services/preferences.service';

@Component({
    selector: 'preferences-modal',
    templateUrl: './preferences-modal.component.html',
    styleUrls: ['./preferences-modal.component.less'],
})
export class PreferencesModalComponent implements OnInit, OnDestroy {
    isAliasShown: boolean;
    private onDestroy$ = new Subject();

    constructor(private preferencesService: PreferencesService) {}

    ngOnInit() {
        this.preferencesService
            .getTargetDisplayPrefSubscription()
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((preference) => {
                this.isAliasShown = preference;
            });
    }

    ngOnDestroy(): void {
        this.onDestroy$.next();
        this.onDestroy$.complete();
    }

    updatePreference() {
        this.preferencesService.setTargetNamingPreference(this.isAliasShown, true);
    }
}
