export interface IPreference<T> {
    name: string;
    value: T;
}
