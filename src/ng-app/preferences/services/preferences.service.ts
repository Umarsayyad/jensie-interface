import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { IUser, UserPrefData } from '../../global-data/models/user.interface';
import { UserResource } from '../../global-data/user.resource';

const ShowAlias = 'showAlias';

@Injectable({ providedIn: 'root' })
export class PreferencesService implements OnDestroy {
    private user: IUser;
    private onDestroy$ = new Subject();

    private targetDisplayPref = false;
    private targetDisplayPref$ = new BehaviorSubject(this.targetDisplayPref);

    constructor(private Users: UserResource) {
        this.Users.get('me').subscribe((user) => {
            this.user = user;
            if (this.user && this.user.ui_data && this.user.ui_data.viewPreferences) {
                let viewPref = this.user.ui_data.viewPreferences.find((pref) => pref.name === ShowAlias);
                if (viewPref) {
                    this.setTargetNamingPreference(viewPref.value);
                }
            }
        });

        this.Users.uiDataChanges()
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((data: UserPrefData) => {
                if (this.user && this.user.id === data.userId) this.user.ui_data = data.uiData;
            });
    }

    ngOnDestroy() {
        this.targetDisplayPref$.complete();
        this.onDestroy$.next();
        this.onDestroy$.complete();
    }

    public getTargetDisplayPref() {
        return this.targetDisplayPref;
    }

    public getTargetDisplayPrefSubscription() {
        return this.targetDisplayPref$.asObservable();
    }

    public setTargetNamingPreference(toggleVal: boolean, persist?: boolean) {
        this.targetDisplayPref = toggleVal;
        this.targetDisplayPref$.next(toggleVal);
        if (persist) {
            const viewPreferences = [{ name: ShowAlias, value: toggleVal }];
            this.Users.updatePrefs(this.user, { viewPreferences }).subscribe();
        }
    }
}
