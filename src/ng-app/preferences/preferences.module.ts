import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

import { PreferencesModalComponent } from './preferences-modal/preferences-modal.component';

@NgModule({
    declarations: [PreferencesModalComponent],
    exports: [PreferencesModalComponent],
    imports: [CommonModule, FormsModule, MatButtonModule, MatDialogModule, MatSlideToggleModule, ReactiveFormsModule],
})
export class PreferencesModule {}
