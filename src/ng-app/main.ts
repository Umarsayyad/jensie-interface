import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { UIRouter, UrlService } from '@uirouter/core';

import { CatoAppModule } from './app.module';
import { mainModule } from '../app/cato.module';

export function bootstrapCato() {
    // This tells UI-Router to delay the initial URL sync (until all bootstrapping is complete)
    mainModule().config(['$urlServiceProvider', ($urlService: UrlService) => $urlService.deferIntercept()]);

    platformBrowserDynamic()
        .bootstrapModule(CatoAppModule)
        .then((platformRef) => {
            // Intialize the Angular Module
            // get() the UIRouter instance from DI to initialize the router
            const urlService: UrlService = platformRef.injector.get(UIRouter).urlService;

            // Instruct UIRouter to listen to URL changes
            urlService.listen();
            urlService.sync();
        });
}
