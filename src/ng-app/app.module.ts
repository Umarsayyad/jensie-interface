import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MAT_MOMENT_DATE_FORMATS, MomentDateModule } from '@angular/material-moment-adapter';
import { MAT_DATE_FORMATS } from '@angular/material/core';
import { MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';
import { BrowserModule } from '@angular/platform-browser';
import { UpgradeModule } from '@angular/upgrade/static';
import { UIRouterModule } from '@uirouter/angular';
import { UIRouterUpgradeModule } from '@uirouter/angular-hybrid';
import { GridsterModule } from 'angular2gridster';
import * as moment from 'moment';

import { AppComponent } from './app.component';
import { AuthModule } from './authorization/auth.module';
import { CmmcMapsModule } from './cmmc-maps/cmmc-maps.module';
import { QuestionnaireModule } from './cmmc-questionnaire/questionnaire.module';
import { CatoCommonModule } from './common/common.module';
import { CompanyProfileModule } from './company-profile/company-profile.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { environment } from './environments/environment';
import { InsightModule } from './insight/insight.module';
import { InsightStatsModule } from './insight/insight-stats/insight-stats.module';
import { MentorProtegeModule } from './insight/mentor-protege/mentor-protege.module';
import { PilotParticipationModule } from './insight/pilot-participation/pilot-participation.module';
import { VulnerabilityTrendsModule } from './insight/vulnerability-trends/vulnerability-trends.module';
import { CatoUpgradeModule } from './ngjs-upgrade/cato-upgrade.module';
import { ToolsModule } from './tools/tools.module';
import { PreferencesModule } from './preferences/preferences.module';

@NgModule({
    providers: [
        // TODO: find a way to globally re-enable pressing "esc" key to close
        { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: true, disableClose: true } },
        { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true, strict: true } },
        {
            provide: MAT_DATE_FORMATS,
            useValue: {
                parse: {
                    dateInput: ['l', moment.ISO_8601],
                },
                display: MAT_MOMENT_DATE_FORMATS.display,
            },
        },
    ],
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        GridsterModule.forRoot(),
        HttpClientModule,
        MomentDateModule,
        UIRouterUpgradeModule,
        UIRouterModule,
        UpgradeModule,

        AuthModule.forRoot(),
        CatoCommonModule.forRoot(),
        CatoUpgradeModule.forRoot(),
        CmmcMapsModule,
        CompanyProfileModule.forRoot(),
        DashboardModule,
        InsightModule,
        InsightStatsModule,
        PilotParticipationModule,
        MentorProtegeModule,
        PreferencesModule,
        QuestionnaireModule,
        ToolsModule,
        VulnerabilityTrendsModule,
    ],
})
export class CatoAppModule {
    constructor(private upgrade: UpgradeModule) {}

    public ngDoBootstrap() {
        this.setUpLoggingConfig();
        console.log('bootstrapping...');
        this.upgrade.bootstrap(document as any, ['cato.main'], { strictDi: false });
    }

    private setUpLoggingConfig() {
        const w = window as any;
        w.originalLog = w.console.log;

        if (!environment.enableLogging) {
            w.console.log = function () {};
        }

        w.enableLogging = (enable: boolean) => {
            if (enable != undefined) {
                w.console.log = enable ? w.originalLog : () => {};
            }
        };
    }
}
