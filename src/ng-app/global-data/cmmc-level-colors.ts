import { defaultColors } from '../common/data';

export const COLOR_NO_LEVEL = defaultColors[13];
export const COLOR_LEVEL_1 = defaultColors[1];
export const COLOR_LEVEL_2 = defaultColors[2];
export const COLOR_LEVEL_3 = defaultColors[3];
export const COLOR_LEVEL_4 = defaultColors[4];
export const COLOR_LEVEL_5 = defaultColors[5];

export const levelColorsArray = [COLOR_LEVEL_1, COLOR_LEVEL_2, COLOR_LEVEL_3, COLOR_LEVEL_4, COLOR_LEVEL_5];

export const levelColorsObject = {
    0: COLOR_NO_LEVEL,
    1: COLOR_LEVEL_1,
    2: COLOR_LEVEL_2,
    3: COLOR_LEVEL_3,
    4: COLOR_LEVEL_4,
    5: COLOR_LEVEL_5,

    'No level achieved': COLOR_NO_LEVEL,
    'Level 1': COLOR_LEVEL_1,
    'Level 2': COLOR_LEVEL_2,
    'Level 3': COLOR_LEVEL_3,
    'Level 4': COLOR_LEVEL_4,
    'Level 5': COLOR_LEVEL_5,
};
