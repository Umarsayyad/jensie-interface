import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';

import { config } from '@cato/config';
import { Resource, ID } from '../../common/models';

@Injectable({ providedIn: 'root' }) //TODO
export class ServicesResource extends Resource<any> {
    protected get url() {
        return config.apiPath + 'service';
    }

    constructor(http: HttpClient) {
        super(http);
    }

    //TODO: fill in other methods later
    public getFindingSeverityInfo() {
        return this.http.get(`${this.url}/findings`);
    }
}

// .factory('Services', ['$resource', '$rootScope',
// function($resource, $rootScope) {
//     return $resource($rootScope.restUrl + 'service/:serviceId', {}, {
//         options: {method: 'OPTIONS', params: {format: 'json'}},
//         getByShortName: {method: 'GET', params: {format: 'json'}},
//         campaigns: {
//             method: 'GET',
//             url: $rootScope.restUrl + 'service/:serviceId/campaigns',
//             isArray: true
//         },
//         findings: {
//             method: 'GET',
//             url: $rootScope.restUrl + 'service/:serviceId/findings',
//             params: {format: 'json'},
//             isArray: true
//         },
//         campaign_findings: {
//             method: 'GET',
//             url: $rootScope.restUrl + 'service/campaign_findings',
//             params: {format: 'json'},
//             isArray: true
//         },
//         severities: {
//             method: 'GET',
//             url: $rootScope.restUrl + 'service/:serviceId/severities',
//             params: {format: 'json'},
//             isArray: true
//         },
//         create: {
//             method: 'POST',
//             headers: {'Content-Type': 'application/json', 'Accept': 'application/json'}
//         }
//     });
// }])
