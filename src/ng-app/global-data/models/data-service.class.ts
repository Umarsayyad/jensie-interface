import { BehaviorSubject, Subject, Observable } from 'rxjs';
import * as deepFreeze from 'deep-freeze';
import catotheme from '@cato/config/themes/catotheme';

import { DataObject, ID, getId, Deletable } from '../../common/models';

export abstract class DataService<T extends DataObject> {
    protected readonly items$ = new BehaviorSubject<Array<T>>([]);
    protected readonly added$ = new Subject<T>();
    protected readonly updated$ = new Subject<T>();
    protected readonly deleted$ = new Subject<T>();

    protected abstract get objArray(): T[];
    protected abstract get objMap(): Map<ID, T>;

    public create(thing: T) {}
    public getAll(): Observable<Readonly<Array<Readonly<T>>>> {
        return this.items$.asObservable();
    }
    public get(id: ID): Readonly<T> | undefined {
        return this.objMap.get(id);
    }
    public update(id: ID) {}
    public delete(id: ID) {}

    public added(): Observable<Readonly<T>> {
        return this.added$.asObservable();
    }
    public updated(): Observable<Readonly<T>> {
        return this.updated$.asObservable();
    }
    public deleted(): Observable<Readonly<T>> {
        return this.deleted$.asObservable();
    }

    // Cannot be overridden
    protected readonly handler = (newObj: Readonly<Deletable & T>) => {
        deepFreeze(newObj);

        if (newObj.deleted) {
            this._delete(newObj);
        } else {
            const id = getId(newObj as T);
            const oldObj = this.objMap.get(id);
            if (oldObj) {
                this._update(newObj, oldObj);
            } else {
                this._add(newObj);
            }
        }

        // Shallow copy the array so that observers can't modify (even though it's typed `Readonly<Array<T>>`)
        this.items$.next([...this.objArray]);
    };

    // Allow subclasses easy access to overwriting add/update/delete methods
    protected _add(newObj: T) {
        const id = getId(newObj);

        this.added$.next(newObj);

        this.objArray.push(newObj);
        this.objMap.set(id, newObj);
    }

    protected _update(newObj: T, oldObj: T) {
        const id = getId(newObj);

        newObj = { ...oldObj, ...newObj };
        this.updated$.next(newObj);

        let i = this.objArray.findIndex((e) => getId(e) === id);
        this.objArray[i] = newObj;
        this.objMap.set(id, newObj);
    }

    protected _delete(newObj: T) {
        const id = getId(newObj);

        const oldObj = this.objMap.get(id) || newObj;
        this.deleted$.next(oldObj);

        let i = this.objArray.findIndex((e) => getId(e) === id);
        this.objArray.splice(i, 1);
        this.objMap.delete(id);
    }

    //TODO: this needs to move to a utility in common at some point
    protected handleError(err: any) {
        let modalService = { openErrorResponse: (...args) => null };
        modalService.openErrorResponse(`An error occurred while loading ${catotheme.appName}, so please refresh the page.`, err, false);
    }
}
