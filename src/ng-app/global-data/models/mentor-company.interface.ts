import { ID } from '../../common';

export interface IMentorCompany {
    id: ID;
    name: string;
}
