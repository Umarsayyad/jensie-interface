export interface ISignupStats {
    has_participated_mentor_program: number;
    has_veteran_owned_business: number;
    top_naics_codes: Array<{ count: number; naics: string }>;
    has_hubzone_business: number;
    enrollments: number;
    has_women_owned_business: number;
    has_8a_small_business: number;
    manufacturer: number;
}
