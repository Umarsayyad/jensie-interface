import { ID } from '../../common';

export interface ILocation {
    id: ID;
    customer: number;
    country: string;
    name: string;
    address: string;
    operatingHours: string;
    primaryLocation: boolean;
    personalLocation: boolean;
    zipCode: number;
    latitude: number;
    longitude: number;
}
