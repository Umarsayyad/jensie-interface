import { ID } from '../../common';

export interface ICustomer {
    id: ID;
    name: string;
    nickname: string;
    duns: string;
    naics: string;
    size: string;
    cagecode: string;
    dnb: string;
    enforce_2fa: boolean;
    require_access_groups: boolean;
    date_joined: string;
    period_of_performance_start_date: string;
    period_of_performance_end_date: string;

    primary_location: boolean;
    primary_location_state: string;
    primary_location_zip: number;
    primary_location_zip_latitude: number;
    primary_location_zip_longitude: number;

    has_civilian_prime_contract: boolean;
    has_civilian_sub_contract: boolean;
    has_dod_prime_contract: boolean;
    has_dod_sub_contract: boolean;
    has_participated_mentor_program: boolean;
    has_participated_nist_mep: boolean;
    has_current_participation_mentor_program: boolean;
    mentor_company: ID;

    has_8a_small_business: boolean;
    has_hubzone_business: boolean;
    has_indian_economic_enterprise: boolean;
    has_service_disabled_veteran_business: boolean;
    has_veteran_owned_business: boolean;
    has_women_owned_business: boolean;
}
