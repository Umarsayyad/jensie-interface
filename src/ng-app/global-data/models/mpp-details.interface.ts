export interface IMppDetails {
    name: string;
    mentorCompany: string;
    startDate: string;
    isCurrentParticipant: boolean;
}
