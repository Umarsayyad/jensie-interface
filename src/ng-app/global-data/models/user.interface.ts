import { ID } from '../../common';
import { IWidget } from '../../dashboard/models/widget.interface';
import { SavedSearches } from '../../insight/models/saved-search.interface';
import { IPreference } from '../../preferences/models/preference.interface';

export interface IUser {
    id: ID;
    username: string;
    firstName: string;
    lastName: string;
    type: string;
    level: number;
    customer: ID;
    email: string;

    assignedCustomers: any[];
    schedules: number[];
    requestPending: boolean;
    notificationPreferences: number[];
    accessGroups: any[];
    // password: any;
    comments: string;
    uiData: any;
    ui_data: UiData;
    roles: string[];
    pocs: string[];
    title: string;

    updated: string; //DateTime
    lastLogin: string; //DateTime
    dateJoined: string; //DateTime
    isActive: boolean;
}

export type UiData = {
    widgets?: IWidget[];
    savedSearches?: SavedSearches;
    savedVulnSearches?: SavedSearches;
    viewPreferences?: Array<IPreference<boolean>>;
};

export type UserPrefData = { userId: ID; uiData: UiData };
