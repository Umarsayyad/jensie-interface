import { ID } from '../../common';

export interface IZipCode {
    id: ID;
    country_code: string;
    postal_code: string;
    place_name: string;
    admin_name1: string;
    admin_code1: string;
    admin_name2: string;
    admin_code2: string;
    admin_name3: string;
    admin_code3: string;
    latitude: number;
    longitude: number;
    accuracy: number;
}
