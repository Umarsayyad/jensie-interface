import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { config } from '@cato/config';
import { ID, Resource } from '../common/models';
import { ICustomer } from './models/customer.interface';
import { ILocation } from './models/location.interface';
import { ISignupStats } from './models/signup-stats.interface';
import { IMppDetails } from './models/mpp-details.interface';
import { IMentorCompany } from './models/mentor-company.interface';

@Injectable({ providedIn: 'root' })
export class CustomerResource extends Resource<ICustomer> {
    protected get url() {
        return config.apiPath + 'customer';
    }

    constructor(http: HttpClient) {
        super(http);
    }

    public getAll(): Observable<ICustomer[]> {
        return this.http.get<ICustomer[]>(`${this.url}`);
    }

    public getMPPDetails(): Observable<IMppDetails[]> {
        return this.http.get<IMppDetails[]>(`${this.url}/mppdetails`);
    }

    public getLocations(customer: ID): Observable<ILocation[]> {
        return this.http.get<ILocation[]>(`${this.url}/${customer}/location`);
    }

    public getAvailableMentors(): Observable<IMentorCompany[]> {
        return this.http.get<IMentorCompany[]>(`${this.url}/available_mentors`);
    }

    public getCounts(fromDate?: string, toDate?: string): Observable<ISignupStats> {
        const params = {};
        if (fromDate) {
            params['date_joined__date__gte'] = fromDate;
        }
        if (toDate) {
            params['date_joined__date__lte'] = toDate;
        }

        return this.http.get<ISignupStats>(`${this.url}/signupstats`, { params });
    }
}
