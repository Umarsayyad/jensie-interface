import { Injectable } from '@angular/core';
import { WebSocketBridge } from 'django-channels';
import { Observable, Subject } from 'rxjs';

import { config } from '@cato/config';
import { UserResource } from './user.resource';
import { DataObject, Deletable } from '../common';

@Injectable({ providedIn: 'root' })
export class SocketService {
    private socketBridge: WebSocketBridge = null;
    private isConnected = false;
    private sendQueue: Array<{ stream: string; body: any }> = [];

    private readonly subscriptions = {
        groups: new Map<string, Subject<any>>(),
        models: new Map<string, Subject<any>>(),
    };

    constructor(private Users: UserResource) {}

    public connect() {
        if (this.socketBridge) {
            console.error('socketService.connect() was called without calling disconnect() first!  Ignoring connect request.');
            return;
        }

        this.socketBridge = new WebSocketBridge();
        console.debug('socketBridge: ', this.socketBridge);

        const options = {
            maxReconnectionDelay: 2 * 60 * 1000, // 2 minutes
            debug: config.prod !== 'true',
        };

        this.socketBridge.connect('/api/ws/', [], options);

        this.socketBridge.socket.addEventListener('error', (e) => {
            console.log('connection error: ', e);
        });

        this.socketBridge.socket.addEventListener('open', () => this.doWsAuth());
        this.socketBridge.socket.addEventListener('close', () => this.onDisconnect());
        this.socketBridge.demultiplex('auth', () => this.onConnect());
        this.socketBridge.demultiplex('unsubscribe', (response) => {
            console.log('Unsubscribe result: ', response);
        });

        this.socketBridge.demultiplex('updates', (response: { group?: string; model?: string; data: any }) => {
            const { group, model, data } = response;

            if (model) {
                let subject = this.getModelSub(model);
                subject.next({ ...data }); //assume we only need shallow copy for now
            }

            if (group) {
                let subject = this.getGroupSub(group);
                subject.next({ ...data }); //assume we only need shallow copy for now
            }
        });

        this.socketBridge.listen();
    }

    public disconnect() {
        // Clear queues and subscriptions
        this.sendQueue = [];
        this.resetSubscriptions();

        if (this.socketBridge && this.socketBridge.socket) {
            // Calling the default close causes the ReconnectingWebsocket to retry!
            const closeOptions = { keepClosed: true };
            this.socketBridge.socket.close(1000, 'Closing normally!', closeOptions);
        }

        // We check to see if the socketBridge is defined whenever we initiate the connect call
        this.socketBridge = null;
    }

    public updatesByGroup(group: string): Observable<any> {
        let sub = this.getGroupSub(group);

        // Send the "subscribe" msg to the server even if the subscription exists
        if (this.isConnected) {
            // Tell the API we want data for this group
            this.socketBridge.stream('subscribe').send({ group });
        }

        return sub.asObservable();
    }

    public updatesByModel<T extends DataObject>(model: string): Observable<Readonly<Deletable & T>> {
        return this.getModelSub(model).asObservable();
    }

    public send(stream: string, body: any) {
        if (this.isConnected) {
            this.socketBridge.stream(stream).send(body);
        } else {
            this.sendQueue.push({ stream, body });
        }
    }

    // public isConnected(): boolean
    // {
    //     return this.isConnected;
    // }

    private doWsAuth() {
        // if($rootScope.loggedIn)
        // {
        // Do auth if logged in, otherwise wait for for the login controller to request auth
        this.Users.getWsAuth().subscribe(
            (res: { token: string }) => {
                this.socketBridge.stream('auth').send({ token: res.token });
            },
            (err) => {
                console.log('Auth request error: ', err);
                // If requesting an auth token failed, the websocket connection was probably lost too
                // so this will be called again once it reconnects
            }
        );
        // }
    }

    private onConnect() {
        if (!this.isConnected) {
            this.isConnected = true;

            // Defer invoking the func until the current call stack has cleared
            Promise.resolve().then(() => {
                // Re-request any existing subscriptions on reconnect
                for (let [groupName, subject] of this.subscriptions.groups) {
                    if (subject.observers.length > 0) {
                        this.updatesByGroup(groupName);
                    }
                }

                // Send queued messages on reconnect
                for (let { stream, body } of this.sendQueue) {
                    this.socketBridge.stream(stream).send(body);
                }
            });

            console.log('Connected!');
        }
    }

    private onDisconnect() {
        this.isConnected = false;
        console.log('Disconnected!');
    }

    // Retrieve the Subject corresponding to the group/model, or a new one -- which will be added to the `subscriptions` object
    private getGroupSub(group: string): Subject<any> {
        let subject = this.subscriptions.groups.get(group);
        if (!subject) {
            let sub = (subject = new Subject());
            this.subscriptions.groups.set(group, subject);

            let unsub = subject.unsubscribe;
            subject.unsubscribe = () => {
                unsub();
                if (sub.observers.length === 0) {
                    if (this.isConnected) {
                        // Tell the API we no longer want data for this group
                        this.socketBridge.stream('unsubscribe').send({ group });
                    }
                }
            };
        }

        return subject;
    }

    private getModelSub(model: string): Subject<any> {
        let subject = this.subscriptions.models.get(model);
        if (!subject) {
            subject = new Subject();
            this.subscriptions.models.set(model, subject);
        }

        return subject;
    }

    private resetSubscriptions() {
        //Clean up the Observables
        for (let subject of this.subscriptions.groups.values()) subject.complete();
        for (let subject of this.subscriptions.models.values()) subject.complete();

        //Clear the Maps
        this.subscriptions.groups.clear();
        this.subscriptions.models.clear();
    }
}
