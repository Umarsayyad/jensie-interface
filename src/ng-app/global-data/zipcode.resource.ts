import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { config } from '@cato/config';
import { ID, IPaginatedResult, Resource } from '../common/models';
import { IZipCode } from './models/zipcode.interface';

@Injectable({ providedIn: 'root' })
export class ZipCodeResource extends Resource<IZipCode> {
    protected get url() {
        return config.apiPath + 'zipcode';
    }

    constructor(http: HttpClient) {
        super(http);
    }

    /*public getAll(): Observable<IPaginatedResult<ICustomer>>
    {
        return this.http.get<IPaginatedResult<ICustomer>>(`${this.url}`);
    }*/

    public getZipCode(zipcode: number): Observable<IPaginatedResult<IZipCode>> {
        // For now, we're just assuming the US.
        // Only one result should return.
        return this.http.get<IPaginatedResult<IZipCode>>(`${this.url}?postal_code=${zipcode}&country_code=US`);
    }
}
