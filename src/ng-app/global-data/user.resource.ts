import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { tap } from 'rxjs/operators';

import { config } from '@cato/config';
import { Resource, DataObject } from '../common/models';
import { IUser, UiData, UserPrefData } from './models/user.interface';

@Injectable({ providedIn: 'root' })
export class UserResource extends Resource<IUser> {
    private userData$ = new ReplaySubject<UserPrefData>(1);

    protected get url() {
        return config.apiPath + 'user';
    }

    constructor(http: HttpClient) {
        super(http);
    }

    //@override
    public update(thing: IUser): Observable<IUser> {
        return super.update(thing).pipe(
            tap((user) => {
                this.userData$.next({ userId: user.id, uiData: user.ui_data });
            })
        );
    }

    //@override
    public patch(thing: Partial<IUser> & DataObject): Observable<IUser> {
        return super.patch(thing).pipe(
            tap((user: IUser) => {
                this.userData$.next({ userId: user.id, uiData: user.ui_data });
            })
        );
    }

    public uiDataChanges(): Observable<UserPrefData> {
        return this.userData$.asObservable();
    }

    public getWsAuth() {
        return this.http.get(`${this.url}/0/ws_auth`);
    }

    public me(): Observable<IUser> {
        return this.get('me');
    }

    public updatePrefs(user: IUser, prefs: UiData): Observable<IUser> {
        // eslint-disable-next-line @typescript-eslint/camelcase
        return this.patch({ id: user.id, ui_data: { ...user.ui_data, ...prefs } });
    }

    // detail: {
    //     method: 'GET',
    //     params: {format: 'json', detail: 'true'},
    //     isArray: true
    // },
    // query: {method: 'GET', params: {format: 'json'}, isArray: true},
    // add: {method: 'POST', params: {format: 'json'}},
    // options: {method: 'OPTIONS', params: {format: 'json'}},
    // addGroup: {
    //     method: 'PUT',
    //     url: $rootScope.restUrl + 'user/:userId/group/:type',
    //     params: {format: 'json'},
    //     isArray: true
    // },
    // update: {
    //     method: 'PUT',
    //     headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
    // },
    // patch: {
    //     method: 'PATCH',
    //     headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
    // },
    // getToken: {
    //     method: 'GET',
    //     params: {format: 'json'},
    //     url: $rootScope.restUrl + 'user/0/password/0/get_token',
    // },
    // requestReset: {
    //     method: 'GET',
    //     params: {format: 'json'},
    //     url: $rootScope.restUrl + 'user/0/password/0/request_reset',
    // },
    // resetPassword: {
    //     method: 'POST',
    //     params: {format: 'json'},
    //     url: $rootScope.restUrl + 'user/0/password/0/reset',
    // },
    // validateToken: {
    //     method: 'GET',
    //     params: {format: 'json'},
    //     url: $rootScope.restUrl + 'user/0/password/0/validate_token',
    // },
    // bulkAdd: {
    //     method: 'POST',
    //     url: $rootScope.restUrl + 'user/0/bulk_create',
    //     headers: {'Content-Type': 'application/json', 'Accept': 'application/json'}
    // },
    // listDevices: {
    //     method: 'GET',
    //     url: $rootScope.restUrl + 'user/:userId/devices'
    // },
    // verifyOTP: {
    //     method: 'POST',
    //     url: $rootScope.restUrl + 'user/0/devices/0/verify_token'
    // },
    // confirmDevice: {
    //     method: 'POST',
    //     url: $rootScope.restUrl + 'user/0/devices/:deviceId/confirm_device'
    // },
    // addDevice: {
    //     method: 'POST',
    //     url: $rootScope.restUrl + 'user/0/devices'
    // },
    // deleteDevice: {
    //     method: 'DELETE',
    //     url: $rootScope.restUrl + 'user/0/devices/:deviceId'
    // },
    // notifications: {
    //     method: 'GET',
    //     params: {format: 'json'},
    //     url: $rootScope.restUrl + 'user/:userId/notification',
    //     isArray: true
    // },
    // notificationOptions: {
    //     method: 'OPTIONS',
    //     url: $rootScope.restUrl + 'user/:userId/notification',
    //     headers: {'Content-Type': 'application/json', 'Accept': 'application/json'}
    // },
    // addNotification: {
    //     method: 'POST',
    //     url: $rootScope.restUrl + 'user/:userId/notification',
    //     headers: {'Content-Type': 'application/json', 'Accept': 'application/json'}
    // },
    // updateNotification: {
    //     method: 'PUT',
    //     url: $rootScope.restUrl + 'user/:userId/notification',
    //     headers: {'Content-Type': 'application/json', 'Accept': 'application/json'}
    // },
    // deleteNotification: {
    //     method: 'DELETE',
    //     url: $rootScope.restUrl + 'user/:userId/notification/:notificationId',
    //     headers: {'Content-Type': 'application/json', 'Accept': 'application/json'}
    // },
    // approve: {
    //     url: $rootScope.restUrl + 'user/:userId/approve',
    //     method: 'PUT',
    //     headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
    // },
    // reject: {
    //     url: $rootScope.restUrl + 'user/:userId/reject',
    //     method: 'PUT',
    //     headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
    // },
    // bulk_approve: {
    //     url: $rootScope.restUrl + 'user/0/bulk_approve',
    //     method: 'PUT',
    //     headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
    // },
    // bulk_reject: {
    //     url: $rootScope.restUrl + 'user/0/bulk_reject',
    //     method: 'PUT',
    //     headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
    // }
}
