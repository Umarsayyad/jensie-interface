import { Component } from '@angular/core';

@Component({
    selector: 'app-root',
    template: `<div>Welcome to {{ title }}, {{ person }}!</div>`, //Url: './app.component.html',
    styleUrls: ['./app.component.less'],
})
export class AppComponent {
    title = 'test-app';
    person = 'blahhhhh';
}
