import { Level } from '../../dashboard/widgets/insight-graphs/models/level/level.namespace';
import states_data from './data/statesdata';
import stateCenters from './data/stateCenters';
import * as L from 'leaflet';

import { LoadingContentComponent, getEnumValues } from '../../common';
import { PROGRESS_COMPLETE_VALUE } from '../../common/data/cmmc';
import { QuestionnaireService } from '../../cmmc-questionnaire/services/questionnaire.service';
import { GlobalDataService } from '../../ngjs-upgrade/globalDataService';
import { CustomerLevels } from './maps.types';
import { LatLngExpression, CircleMarker, CircleMarkerOptions } from 'leaflet';
import { ICustomer } from '../../global-data/models/customer.interface';
import { latLng, getLevelColor, getOpacity } from './maps.utils';
import { InsightDashboardResourceishService } from '../../dashboard/widgets/insight-graphs/insight-dashboard-resourceish.service';

const levelColors: any = [
    'rgb(128, 128, 128)',
    'rgb(150, 255, 180)',
    'rgb(168, 56, 93)',
    'rgb(101, 235, 253)',
    'rgb(252, 238, 75)',
    'rgb(253, 214, 227)',
];
const populated_state_outline_color = '#555';
const populated_state_fill_color = '#999';
const populated_state_hover_outline_color = '#666';
const unpopulated_state_outline_color = 'white';
const unpopulated_state_fill_color = '#EEE';

let leaflet_map: L.Map;
let leaflet_info: L.Control;
let populated_states: string[] = [];
let markers: CustomCircleMarker[] = [];
let info_panel: HTMLElement;
let resize_interval: any;
let previous_state: any; // The state shape click callback e.target, doesn't seem to have a set type.
let previous_zips: L.LayerGroup<CustomCircleMarker>;
let gdata: GlobalDataService;
let isService: InsightDashboardResourceishService;
let levelsByState: StateStatistics = {};
let customerLevels: CustomerLevels = {};

const DrawLeafletMap = (
    globalDataService: GlobalDataService,
    qService: QuestionnaireService,
    insightService: InsightDashboardResourceishService,
    spinner: LoadingContentComponent
) => {
    clearInitialData();
    gdata = globalDataService;
    isService = insightService;
    loadQuestionnaireData().then(() => {
        loadCustomerData(qService);
        createLeafletMap();
        addTiles(leaflet_map);
        addComplianceHoverOverInfo(leaflet_map);
        addLegend(leaflet_map);
        triggerResizeToGetDataToShow();
        loadStateShapeData(leaflet_map);
        loadStateLabels(leaflet_map);
        resize_interval = setInterval(triggerResizeToGetDataToShow, 200);
        spinner.stopLoading();
        return leaflet_map;
    });
};

export default DrawLeafletMap;

const clearInitialData = () => {
    populated_states = [];
    markers = [];
    info_panel = undefined;
    leaflet_map = undefined;
    resize_interval = undefined;
    previous_state = undefined;
    previous_zips = undefined;
    gdata = undefined;
    levelsByState = {};
    customerLevels = {};
};

const loadQuestionnaireData = (): Promise<void> => {
    return isService
        .progressByLevel()
        .toPromise()
        .then((customerProgresses) => {
            for (let progress of customerProgresses) {
                let level = 0;
                const customerId = progress.customer;

                for (let l of getEnumValues<Level.Levels>(Level.Levels)) {
                    const levelProgress: Level.ILevel = progress[l];
                    if (levelProgress.practices.progress === PROGRESS_COMPLETE_VALUE) {
                        level = l;
                    }
                }

                customerLevels[customerId] = level;
                gdata.customerMap[customerId].level = level;
            }
        });
};

const triggerResizeToGetDataToShow = (): any => {
    // The map doesn't render properly till the window resizes.
    window.dispatchEvent(new Event('resize'));
    return leaflet_map;
};

const createLeafletMap = (): any => {
    leaflet_map = L.map('cmmcmap').setView([37.8, -96], 4);
};

const loadCustomerData = (qService: QuestionnaireService): void => {
    const customerIds = Object.keys(gdata.customerMap);
    for (let i in customerIds) {
        const customerId = customerIds[i];
        const customer = gdata.customerMap[customerId];

        if (customer.primary_location_state) {
            const state = customer.primary_location_state;
            const latitude: number = customer.primary_location_zip_latitude;
            const longitude: number = customer.primary_location_zip_longitude;
            const zip: string = customer.primary_location_zip.toString();

            if (populated_states.indexOf(state) < 0) {
                populated_states.push(state);
            }

            const marker = createOrGetMarker(latitude, longitude, state, zip);

            if (!(state in levelsByState)) {
                levelsByState[state] = {};
            }
            if (customer.level in levelsByState[state]) {
                levelsByState[state][customer.level]++;
            } else {
                levelsByState[state][customer.level] = 1;
            }

            registerCustomerToMarker(marker, customer, customer.level);

            if (marker.options.highestLevel < customer.level) {
                marker.options.highestLevel = customer.level;
                marker.setStyle({
                    color: getLevelColor[customer.level],
                });
            }
        }
    }
};

const getHighestLevelForState = (state: string): number => {
    const levels = levelsByState[state];
    for (let i = 5; i > 0; i--) {
        if (levels[i]) {
            return i;
        }
    }
};

const createOrGetMarker = (lat: number, lng: number, state: string, zip: string): CustomCircleMarker => {
    const key = latLng(lat, lng);
    if (!markers[key]) {
        createMarker(lat, lng, state, zip);
    }
    return markers[key];
};

const registerCustomerToMarker = (marker: CustomCircleMarker, customer: ICustomer, level: number) => {
    marker.options.customers.push(customer);
    if (level > marker.options.highestLevel) {
        marker.options.highestLevel = level;
        marker.setStyle({
            fillColor: getLevelColor(level),
        });
    }
    marker.bindPopup(getPopupContentForZipCode(marker));
};

interface CustomCircleMarkerOptions extends CircleMarkerOptions {
    // Can put whatever we want in here
    customers?: ICustomer[];
    state?: string;
    zip?: string;
    highestLevel?: number;
}

export class CustomCircleMarker<P = any> extends CircleMarker {
    constructor(latlng: LatLngExpression, options?: CustomCircleMarkerOptions) {
        super(latlng, options);
    }
    options: CustomCircleMarkerOptions;
}

export type StateStatistics = { [key: string]: { [key: number]: number } };

const createMarker = (lat: number, lng: number, state: string, zip: string): CustomCircleMarker => {
    const marker = new CustomCircleMarker([lat, lng], {
        radius: Math.log(2) * 5 + 5,
        fillOpacity: 0.8,
        fillColor: getLevelColor(0),
        color: 'white',
    });
    marker.options.highestLevel = 0;
    marker.options.customers = [];
    marker.options.state = state;
    marker.options.zip = zip;

    markers[latLng(lat, lng)] = marker;
    return marker;
};

const addComplianceHoverOverInfo = (leaflet_map: L.Map) => {
    // Control that shows state compliance level on hover.
    leaflet_info = new L.Control();
    leaflet_info.onAdd = function () {
        return updateInfoPanel(null);
    };

    leaflet_info.addTo(leaflet_map);
};

const updateInfoPanel = (props: any): HTMLElement => {
    if (typeof info_panel === 'undefined') {
        info_panel = L.DomUtil.create('div', 'leaflet-info');
    }

    // Show state compliance level in the upper right.
    let html = '<h4>CMMC Compliance Level</h4>';
    if (props && populated_states.indexOf(props.name) > -1) {
        const state = props.name;
        html += '<b>' + state + '</b>';
        for (let [level, count] of Object.entries(levelsByState[state])) {
            const legendColor = levelColors[level];
            html +=
                '<br/><div style="height: 15px; width: 15px; background-color: ' + legendColor + ';display: inline-block; margin-right: 5px;"></div>';
            html += '</i><span>' + count + ' ' + (count == 1 ? 'Company' : 'Companies') + '</span>';
        }
    } else {
        html += 'Hover over a state';
    }

    info_panel.innerHTML = html;
    return info_panel;
};

const getPopupContentForZipCode = (marker: CustomCircleMarker): string => {
    const first_customer = marker.options.customers[0];
    let content = '<div class="leaflet-legend"><b>' + marker.options.zip + '</b><br/>' + getLevelBlock(first_customer) + first_customer.nickname;

    for (let i = 1; i < marker.options.customers.length; i++) {
        const customer = marker.options.customers[i];
        if (i > 12) {
            content += '<br/><i>Et al.</i>';
            break;
        }

        content += '<br/>' + getLevelBlock(customer) + customer.nickname;
    }
    return content + '</div>';
};

const getLevelBlock = (customer: ICustomer): string => {
    const level = gdata.customerMap[customer.id].level;
    return '<i style="background:' + getLevelColor(level) + '"></i>';
};

const addLegend = (leaflet_map: L.Map): void => {
    const legend = new L.Control({ position: 'bottomright' });
    // Populate legend on lower right.
    legend.onAdd = () => {
        const div = L.DomUtil.create('div', 'leaflet-info leaflet-legend'),
            levels = [0, 1, 2, 3, 4, 5],
            labels = [];

        for (let i = 0; i < levels.length; i++) {
            const level = levels[i];
            labels.push('<i style="background:' + getLevelColor(level) + '"></i> Level ' + level);
        }

        div.innerHTML = labels.join('<br>');
        return div;
    };
    legend.addTo(leaflet_map);
};

const getMarkersForState = (state: string): CustomCircleMarker[] => {
    const returnMarkers: CustomCircleMarker[] = [];
    for (let i in markers) {
        if (markers[i].options.state === state) {
            returnMarkers.push(markers[i]);
        }
    }
    return returnMarkers;
};

const addStateMarkers = (state: string): L.LayerGroup<CustomCircleMarker> | void => {
    const stateMarkers = getMarkersForState(state);
    if (stateMarkers.length) {
        const layerGroup: L.LayerGroup<CustomCircleMarker> = L.layerGroup(stateMarkers);
        leaflet_map.addLayer(layerGroup);
        return layerGroup;
    }
};

const bringMarkersToFront = (layerGroup: L.LayerGroup<CustomCircleMarker>): void => {
    const layers: CustomCircleMarker[] = <CustomCircleMarker[]>layerGroup.getLayers();

    for (let i in layers) {
        layers[i].bringToFront();
    }
};

const loadStateLabels = (leafletMap: L.Map) => {
    for (let stateName in stateCenters) {
        const coordinates = stateCenters[stateName];
        const stateNameTooltipOffsetX = 0;
        const stateNameTooltipOffsetY = -22;
        L.marker(coordinates, { opacity: 0 })
            .bindTooltip(stateName, {
                permanent: true,
                className: 'state-label',
                direction: 'center',
                offset: [stateNameTooltipOffsetX, stateNameTooltipOffsetY],
            })
            .addTo(leafletMap);
    }
};

const loadStateShapeData = (leaflet_map: L.Map) => {
    const geojson = L.geoJSON(states_data, {
        style: (feature: any) => {
            if (populated_states.indexOf(feature.properties['name']) > -1) {
                const level = getHighestLevelForState(feature.properties['name']);

                // Maybe do this, but scaled to zoom
                // addStateMarkers(feature.properties['name']);
                return {
                    weight: 2,
                    opacity: 1,
                    color: populated_state_outline_color,
                    dashArray: '3',
                    fillOpacity: getOpacity(feature.properties.level),
                    fillColor: getLevelColor(level),
                };
            }

            return {
                weight: 1,
                opacity: 1,
                color: unpopulated_state_outline_color,
                dashArray: '3',
                fillOpacity: getOpacity(feature.properties.level),
                fillColor: unpopulated_state_fill_color,
            };
        },
        onEachFeature: (feature: any, layer: any) => {
            layer.on({
                mouseover: (e: any) => {
                    updateInfoPanel(layer.feature.properties);

                    if (previous_state && e.target.feature.properties['name'] === previous_state.feature.properties['name']) {
                        return;
                    }

                    if (resize_interval) {
                        clearInterval(resize_interval);
                        resize_interval = null;
                    }

                    const hoveredState = e.target.feature.properties['name'];
                    if (getMarkersForState(hoveredState).length > 0) {
                        // Highlight the state on mouseover.
                        const layer = e.target;
                        layer.setStyle({
                            weight: 5,
                            color: populated_state_hover_outline_color,
                            dashArray: '',
                            fillOpacity: 0.7,
                        });
                    }
                },

                mouseout: (e: any) => {
                    updateInfoPanel(null);

                    if (previous_state && e.target.feature.properties['name'] === previous_state.feature.properties['name']) {
                        return;
                    }

                    // Unhighlight the state when the mouse exits.
                    geojson.resetStyle(e.target);
                },

                click: (e: any) => {
                    const clickedState = e.target.feature.properties['name'];

                    // Do nothing if we're clicking the already active state.
                    if (
                        (previous_state && clickedState === previous_state.feature.properties['name']) ||
                        populated_states.indexOf(clickedState) < 0
                    ) {
                        return;
                    }

                    e.target.setStyle({
                        fillOpacity: 0.3,
                    });

                    const layerGroup = addStateMarkers(clickedState);

                    if (layerGroup) {
                        bringMarkersToFront(layerGroup);
                    }

                    if (previous_state && previous_zips) {
                        leaflet_map.removeLayer(previous_zips);
                        const level = getHighestLevelForState(previous_state.feature.properties['name']);
                        previous_state.setStyle({
                            fillOpacity: 0.7,
                        });
                    }

                    previous_state = e.target;

                    if (layerGroup) {
                        previous_zips = layerGroup;
                    }

                    // Zoom into a state on click.
                    leaflet_map.fitBounds(e.target.getBounds());
                },
            });
        },
    }).addTo(leaflet_map);
    return leaflet_map;
};

const addTiles = (leaflet_map: L.Map): any => {
    /*L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 9,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox/light-v9',
        tileSize: 512,
        zoomOffset: -1
    }).addTo(this.map);*/
    return leaflet_map;
};
