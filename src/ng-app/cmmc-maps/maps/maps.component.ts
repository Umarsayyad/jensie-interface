import { Component, ViewChild, AfterViewInit } from '@angular/core';

import { LoadingContentComponent } from '../../common';
import { QuestionnaireService } from '../../cmmc-questionnaire/services/questionnaire.service';
import { InsightDashboardResourceishService } from '../../dashboard/widgets/insight-graphs/insight-dashboard-resourceish.service';
import { GlobalDataService } from '../../ngjs-upgrade/globalDataService';
import DrawLeafletMap from './drawLeafletMap';

declare var $: any;

@Component({
    selector: 'maps',
    templateUrl: './maps.component.html',
    styleUrls: ['./maps.component.less'],
})
export class MapsComponent implements AfterViewInit {
    @ViewChild(LoadingContentComponent) spinner: LoadingContentComponent;

    constructor(private qService: QuestionnaireService, private gdata: GlobalDataService, private isService: InsightDashboardResourceishService) {}

    title = 'CMMC Compliance by State';

    ngAfterViewInit(): void {
        this.spinner.startLoading();
        DrawLeafletMap(this.gdata, this.qService, this.isService, this.spinner);
    }

    ngOnDestroy(): void {}
}
