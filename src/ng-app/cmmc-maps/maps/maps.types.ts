export type CustomerLevels = {
    [customerId: number]: number;
};

export type latlng = {
    latitude: number;
    longitude: number;
};
