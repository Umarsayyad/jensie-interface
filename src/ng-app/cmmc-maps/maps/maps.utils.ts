export const latLng = (latitude: number, longitude: number): string => {
    return `${latitude},${longitude}`;
};

export const getLevelColor = (level: number): string => {
    if (level === -1) {
        // No data to display.
        return '#CCCCCC';
    }

    if (level === null) {
        level = 0;
    }

    let levelColors = [
        'rgb(128, 128, 128)',
        'rgb(150, 255, 180)',
        'rgb(168, 56, 93)',
        'rgb(101, 235, 253)',
        'rgb(252, 238, 75)',
        'rgb(253, 214, 227)',
    ];

    return levelColors[level];
};

export const getOpacity = (level: number): number => {
    if (level === -1) {
        return 0.9;
    } else {
        return 0.7;
    }
};
