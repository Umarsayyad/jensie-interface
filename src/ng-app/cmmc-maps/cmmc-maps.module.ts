import { NgModule } from '@angular/core';
import { CatoCommonModule } from '../common/common.module';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { UIRouterModule } from '@uirouter/angular';
import { mapsViewState } from './maps-view-state';
import { MapsComponent } from './maps/maps.component';

@NgModule({
    declarations: [MapsComponent],
    imports: [CatoCommonModule, CommonModule, BrowserModule, UIRouterModule.forChild({ states: [mapsViewState] })],
})
export class CmmcMapsModule {}
