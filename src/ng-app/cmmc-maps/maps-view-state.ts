import { MapsComponent } from './maps/maps.component';

export const mapsViewState = {
    name: 'cato.loggedIn.dashboard.insightmaps',
    url: '/insightmaps',
    views: {
        insightmapsview: {
            component: MapsComponent,
        },
    },
    resolve: {
        $title: resolveDashboardTitle,
    },
};

export function resolveDashboardTitle($title) {
    return $title + ' - CMMC Maps';
}
