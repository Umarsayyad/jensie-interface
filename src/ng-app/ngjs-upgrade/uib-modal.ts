export class UibModal<T> {
    opened: Promise<void>; //Is resolved when a modal gets opened after downloading content's template and resolving all variables.
    rendered: Promise<void>; //Is resolved when a modal is rendered.
    closed: Promise<T>; //Is resolved when a modal is closed and the animation completes.
    result: Promise<T>; //Is resolved when a modal is closed and rejected when a modal is dismissed.

    close: (result?: T) => void; //Can be used to close a modal, passing a result.
    dismiss: (reason?: string) => void; //(Type: function) - Can be used to dismiss a modal, passing a reason.
}
