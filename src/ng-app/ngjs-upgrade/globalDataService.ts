// TODO: upgrade globalDataService for real so this file isn't needed

export class GlobalDataService {
    [key: string]: any;
}

export function globalDataServiceFactory(i: any) {
    return i.get('globalDataService');
}

export const globalDataServiceProvider = {
    provide: GlobalDataService,
    useFactory: globalDataServiceFactory,
    deps: ['$injector'],
};
