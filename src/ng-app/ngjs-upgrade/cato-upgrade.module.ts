import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { globalDataServiceProvider } from './globalDataService';

// This module will contain providers that are upgraded from AngularJS.
// Add them to the `providers` in the `forRoot` function.
@NgModule({
    declarations: [],
    entryComponents: [],
    imports: [],
    exports: [],
    providers: [globalDataServiceProvider],
})
export class CatoUpgradeModule {
    constructor(@Optional() @SkipSelf() parentModule: CatoUpgradeModule) {
        if (parentModule) {
            throw new Error('CatoUpgradeModule is already loaded.  Import it in the CatoAppModule only');
        }
    }

    static forRoot(): ModuleWithProviders<CatoUpgradeModule> {
        return {
            ngModule: CatoUpgradeModule,
            providers: [
                // {provide: UibModal, useFactory: (i: any) => i.get("$uibModalInstance"), deps: ["$injector"]},
            ],
        };
    }
}
