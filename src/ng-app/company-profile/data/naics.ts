import * as deepfreeze from 'deep-freeze';
import { ISector } from '../models/sector.interface';

//hint: scroll down

const naics = {
    '11': {
        name: 'Agriculture, Forestry, Fishing, and Hunting',
        subsectors: {
            '111': {
                name: 'Crop Production',
                industries: {
                    '111110': {
                        name: 'Soybean Farming',
                    },
                    '111120': {
                        name: 'Oilseed (except Soybean) Farming',
                    },
                    '111130': {
                        name: 'Dry Pea and Bean Farming',
                    },
                    '111140': {
                        name: 'Wheat Farming',
                    },
                    '111150': {
                        name: 'Corn Farming',
                    },
                    '111160': {
                        name: 'Rice Farming',
                    },
                    '111191': {
                        name: 'Oilseed and Grain Combination Farming',
                    },
                    '111199': {
                        name: 'All Other Grain Farming',
                    },
                    '111211': {
                        name: 'Potato Farming',
                    },
                    '111219': {
                        name: 'Other Vegetable (except Potato) and Melon Farming',
                    },
                    '111310': {
                        name: 'Orange Groves',
                    },
                    '111320': {
                        name: 'Citrus (except Orange) Groves',
                    },
                    '111331': {
                        name: 'Apple Orchards',
                    },
                    '111332': {
                        name: 'Grape Vineyards',
                    },
                    '111333': {
                        name: 'Strawberry Farming',
                    },
                    '111334': {
                        name: 'Berry (except Strawberry) Farming',
                    },
                    '111335': {
                        name: 'Tree Nut Farming',
                    },
                    '111336': {
                        name: 'Fruit and Tree Nut Combination Farming',
                    },
                    '111339': {
                        name: 'Other Noncitrus Fruit Farming',
                    },
                    '111411': {
                        name: 'Mushroom Production',
                    },
                    '111419': {
                        name: 'Other Food Crops Grown Under Cover',
                    },
                    '111421': {
                        name: 'Nursery and Tree Production',
                    },
                    '111422': {
                        name: 'Floriculture Production',
                    },
                    '111910': {
                        name: 'Tobacco Farming',
                    },
                    '111920': {
                        name: 'Cotton Farming',
                    },
                    '111930': {
                        name: 'Sugarcane Farming',
                    },
                    '111940': {
                        name: 'Hay Farming',
                    },
                    '111991': {
                        name: 'Sugar Beet Farming',
                    },
                    '111992': {
                        name: 'Peanut Farming',
                    },
                    '111998': {
                        name: 'All Other Miscellaneous Crop Farming',
                    },
                },
            },
            '112': {
                name: 'Animal Production and Aquaculture',
                industries: {
                    '112111': {
                        name: 'Beef Cattle Ranching and Farming',
                    },
                    '112112': {
                        name: 'Cattle Feedlots',
                    },
                    '112120': {
                        name: 'Dairy Cattle and Milk Production',
                    },
                    '112210': {
                        name: 'Hog and Pig Farming',
                    },
                    '112310': {
                        name: 'Chicken Egg Production',
                    },
                    '112320': {
                        name: 'Broilers and Other Meat Type Chicken Production',
                    },
                    '112330': {
                        name: 'Turkey Production',
                    },
                    '112340': {
                        name: 'Poultry Hatcheries',
                    },
                    '112390': {
                        name: 'Other Poultry Production',
                    },
                    '112410': {
                        name: 'Sheep Farming',
                    },
                    '112420': {
                        name: 'Goat Farming',
                    },
                    '112511': {
                        name: 'Finfish Farming and Fish Hatcheries',
                    },
                    '112512': {
                        name: 'Shellfish Farming',
                    },
                    '112519': {
                        name: 'Other Aquaculture',
                    },
                    '112910': {
                        name: 'Apiculture',
                    },
                    '112920': {
                        name: 'Horses and Other Equine Production',
                    },
                    '112930': {
                        name: 'Fur-Bearing Animal and Rabbit Production',
                    },
                    '112990': {
                        name: 'All Other Animal Production',
                    },
                },
            },
            '113': {
                name: 'Forestry and Logging',
                industries: {
                    '113110': {
                        name: 'Timber Tract Operations',
                    },
                    '113210': {
                        name: 'Forest Nurseries and Gathering of Forest Products',
                    },
                    '113310': {
                        name: 'Logging',
                    },
                },
            },
            '114': {
                name: 'Fishing, Hunting, and Trapping',
                industries: {
                    '114111': {
                        name: 'Finfish Fishing',
                    },
                    '114112': {
                        name: 'Shellfish Fishing',
                    },
                    '114119': {
                        name: 'Other Marine Fishing',
                    },
                    '114210': {
                        name: 'Hunting and Trapping',
                    },
                },
            },
            '115': {
                name: 'Support Activities for Agriculture and Forestry',
                industries: {
                    '115111': {
                        name: 'Cotton Ginning',
                    },
                    '115112': {
                        name: 'Soil Preparation, Planting, and Cultivating',
                    },
                    '115113': {
                        name: 'Crop Harvesting, Primarily by Machine',
                    },
                    '115114': {
                        name: 'Postharvest Crop Activities (except Cotton Ginning)',
                    },
                    '115115': {
                        name: 'Farm Labor Contractors and Crew Leaders',
                    },
                    '115116': {
                        name: 'Farm Management Services',
                    },
                    '115210': {
                        name: 'Support Activities for Animal Production',
                    },
                    '115310': {
                        name: 'Support Activities for Forestry',
                    },
                },
            },
        },
    },
    '21': {
        name: 'Mining, Quarrying, and Oil and Gas Extraction',
        subsectors: {
            '211': {
                name: 'Oil and Gas Extraction',
                industries: {
                    '211120': {
                        name: 'Crude Petroleum Extraction',
                    },
                    '211130': {
                        name: 'Natural Gas Extraction',
                    },
                },
            },
            '212': {
                name: 'Mining (except Oil and Gas)',
                industries: {
                    '212111': {
                        name: 'Bituminous Coal and Lignite Surface Mining',
                    },
                    '212112': {
                        name: 'Bituminous Coal Underground Mining',
                    },
                    '212113': {
                        name: 'Anthracite Mining',
                    },
                    '212210': {
                        name: 'Iron Ore Mining',
                    },
                    '212221': {
                        name: 'Gold Ore Mining',
                    },
                    '212222': {
                        name: 'Silver Ore Mining',
                    },
                    '212230': {
                        name: 'Copper, Nickel, Lead, and Zinc Mining',
                    },
                    '212291': {
                        name: 'Uranium-Radium-Vanadium Ore Mining',
                    },
                    '212299': {
                        name: 'All Other Metal Ore Mining',
                    },
                    '212311': {
                        name: 'Dimension Stone Mining and Quarrying',
                    },
                    '212312': {
                        name: 'Crushed and Broken Limestone Mining and Quarrying',
                    },
                    '212313': {
                        name: 'Crushed and Broken Granite Mining and Quarrying',
                    },
                    '212319': {
                        name: 'Other Crushed and Broken Stone Mining and Quarrying',
                    },
                    '212321': {
                        name: 'Construction Sand and Gravel Mining',
                    },
                    '212322': {
                        name: 'Industrial Sand Mining',
                    },
                    '212324': {
                        name: 'Kaolin and Ball Clay Mining',
                    },
                    '212325': {
                        name: 'Clay and Ceramic and Refractory Minerals Mining',
                    },
                    '212391': {
                        name: 'Potash, Soda, and Borate Mineral Mining',
                    },
                    '212392': {
                        name: 'Phosphate Rock Mining',
                    },
                    '212393': {
                        name: 'Other Chemical and Fertilizer Mineral Mining',
                    },
                    '212399': {
                        name: 'All Other Nonmetallic Mineral Mining',
                    },
                },
            },
            '213': {
                name: 'Support Activities for Mining',
                industries: {
                    '213111': {
                        name: 'Drilling Oil and Gas Wells',
                    },
                    '213112': {
                        name: 'Support Activities for Oil and Gas Operations',
                    },
                    '213113': {
                        name: 'Support Activities for Coal Mining',
                    },
                    '213114': {
                        name: 'Support Activities for Metal Mining',
                    },
                    '213115': {
                        name: 'Support Activities for Nonmetallic Minerals (except Fuels)',
                    },
                },
            },
        },
    },
    '22': {
        name: 'Utilities',
        subsectors: {
            '221': {
                name: 'Utilities',
                industries: {
                    '221111': {
                        name: 'Hydroelectric Power Generation',
                    },
                    '221112': {
                        name: 'Fossil Fuel Electric Power Generation',
                    },
                    '221113': {
                        name: 'Nuclear Electric Power Generation',
                    },
                    '221114': {
                        name: 'Solar Electric Power Generation',
                    },
                    '221115': {
                        name: 'Wind Electric Power Generation',
                    },
                    '221116': {
                        name: 'Geothermal Electric Power Generation',
                    },
                    '221117': {
                        name: 'Biomass Electric Power Generation',
                    },
                    '221118': {
                        name: 'Other Electric Power Generation',
                    },
                    '221121': {
                        name: 'Electric Bulk Power Transmission and Control',
                    },
                    '221122': {
                        name: 'Electric Power Distribution',
                    },
                    '221210': {
                        name: 'Natural Gas Distribution',
                    },
                    '221310': {
                        name: 'Water Supply and Irrigation Systems',
                    },
                    '221320': {
                        name: 'Sewage Treatment Facilities',
                    },
                    '221330': {
                        name: 'Steam and Air-Conditioning Supply',
                    },
                },
            },
        },
    },
    '23': {
        name: 'Construction',
        subsectors: {
            '236': {
                name: 'Construction of Buildings',
                industries: {
                    '236115': {
                        name: 'New Single-family Housing Construction (Except For-Sale Builders)',
                    },
                    '236116': {
                        name: 'New Multifamily Housing Construction (except For-Sale Builders)',
                    },
                    '236117': {
                        name: 'New Housing For-Sale Builders',
                    },
                    '236118': {
                        name: 'Residential Remodelers',
                    },
                    '236210': {
                        name: 'Industrial Building Construction',
                    },
                    '236220': {
                        name: 'Commercial and Institutional Building Construction',
                    },
                },
            },
            '237': {
                name: 'Heavy and Civil Engineering Construction',
                industries: {
                    '237110': {
                        name: 'Water and Sewer Line and Related Structures Construction',
                    },
                    '237120': {
                        name: 'Oil and Gas Pipeline and Related Structures Construction',
                    },
                    '237130': {
                        name: 'Power and Communication Line and Related Structures Construction',
                    },
                    '237210': {
                        name: 'Land Subdivision',
                    },
                    '237310': {
                        name: 'Highway, Street, and Bridge Construction',
                    },
                    '237990': {
                        name: 'Other Heavy and Civil Engineering Construction',
                    },
                },
            },
            '238': {
                name: 'Specialty Trade Contractors',
                industries: {
                    '238110': {
                        name: 'Poured Concrete Foundation and Structure Contractors',
                    },
                    '238120': {
                        name: 'Structural Steel and Precast Concrete Contractors',
                    },
                    '238130': {
                        name: 'Framing Contractors',
                    },
                    '238140': {
                        name: 'Masonry Contractors',
                    },
                    '238150': {
                        name: 'Glass and Glazing Contractors',
                    },
                    '238160': {
                        name: 'Roofing Contractors',
                    },
                    '238170': {
                        name: 'Siding Contractors',
                    },
                    '238190': {
                        name: 'Other Foundation, Structure, and Building Exterior Contractors',
                    },
                    '238210': {
                        name: 'Electrical Contractors and Other Wiring Installation Contractors',
                    },
                    '238220': {
                        name: 'Plumbing, Heating, and Air-Conditioning Contractors',
                    },
                    '238290': {
                        name: 'Other Building Equipment Contractors',
                    },
                    '238310': {
                        name: 'Drywall and Insulation Contractors',
                    },
                    '238320': {
                        name: 'Painting and Wall Covering Contractors',
                    },
                    '238330': {
                        name: 'Flooring Contractors',
                    },
                    '238340': {
                        name: 'Tile and Terrazzo Contractors',
                    },
                    '238350': {
                        name: 'Finish Carpentry Contractors',
                    },
                    '238390': {
                        name: 'Other Building Finishing Contractors',
                    },
                    '238910': {
                        name: 'Site Preparation Contractors',
                    },
                    '238990': {
                        name: 'All Other Specialty Trade Contractors',
                    },
                },
            },
        },
    },
    '31 - 33': {
        name: 'Manufacturing',
        subsectors: {
            '311': {
                name: 'Food Manufacturing',
                industries: {
                    '311111': {
                        name: 'Dog and Cat Food Manufacturing',
                    },
                    '311119': {
                        name: 'Other Animal Food Manufacturing',
                    },
                    '311211': {
                        name: 'Flour Milling',
                    },
                    '311212': {
                        name: 'Rice Milling',
                    },
                    '311213': {
                        name: 'Malt Manufacturing',
                    },
                    '311221': {
                        name: 'Wet Corn Milling',
                    },
                    '311224': {
                        name: 'Soybean and Other Oilseed Processing',
                    },
                    '311225': {
                        name: 'Fats and Oils Refining and Blending',
                    },
                    '311230': {
                        name: 'Breakfast Cereal Manufacturing',
                    },
                    '311313': {
                        name: 'Beet Sugar Manufacturing',
                    },
                    '311314': {
                        name: 'Cane Sugar Manufacturing',
                    },
                    '311340': {
                        name: 'Nonchocolate Confectionery Manufacturing',
                    },
                    '311351': {
                        name: 'Chocolate and Confectionery Manufacturing from Cacao Beans',
                    },
                    '311352': {
                        name: 'Confectionery Manufacturing from Purchased Chocolate',
                    },
                    '311411': {
                        name: 'Frozen Fruit, Juice, and Vegetable Manufacturing',
                    },
                    '311412': {
                        name: 'Frozen Specialty Food Manufacturing',
                    },
                    '311421': {
                        name: 'Fruit and Vegetable Canning',
                    },
                    '311422': {
                        name: 'Specialty Canning',
                    },
                    '311423': {
                        name: 'Dried and Dehydrated Food Manufacturing',
                    },
                    '311511': {
                        name: 'Fluid Milk Manufacturing',
                    },
                    '311512': {
                        name: 'Creamery Butter Manufacturing',
                    },
                    '311513': {
                        name: 'Cheese Manufacturing',
                    },
                    '311514': {
                        name: 'Dry, Condensed, and Evaporated Dairy Product Manufacturing',
                    },
                    '311520': {
                        name: 'Ice Cream and Frozen Dessert Manufacturing',
                    },
                    '311611': {
                        name: 'Animal (except Poultry) Slaughtering',
                    },
                    '311612': {
                        name: 'Meat Processed from Carcasses',
                    },
                    '311613': {
                        name: 'Rendering and Meat Byproduct Processing',
                    },
                    '311615': {
                        name: 'Poultry Processing',
                    },
                    '311710': {
                        name: 'Seafood Product Preparation and Packaging',
                    },
                    '311811': {
                        name: 'Retail Bakeries',
                    },
                    '311812': {
                        name: 'Commercial Bakeries',
                    },
                    '311813': {
                        name: 'Frozen Cakes, Pies, and Other Pastries Manufacturing',
                    },
                    '311821': {
                        name: 'Cookie and Cracker Manufacturing',
                    },
                    '311824': {
                        name: 'Dry Pasta, Dough, and Flour Mixes Manufacturing from Purchased Flour',
                    },
                    '311830': {
                        name: 'Tortilla Manufacturing',
                    },
                    '311911': {
                        name: 'Roasted Nuts and Peanut Butter Manufacturing',
                    },
                    '311919': {
                        name: 'Other Snack Food Manufacturing',
                    },
                    '311920': {
                        name: 'Coffee and Tea Manufacturing',
                    },
                    '311930': {
                        name: 'Flavoring Syrup and Concentrate Manufacturing',
                    },
                    '311941': {
                        name: 'Mayonnaise, Dressing, and Other Prepared Sauce Manufacturing',
                    },
                    '311942': {
                        name: 'Spice and Extract Manufacturing',
                    },
                    '311991': {
                        name: 'Perishable Prepared Food Manufacturing',
                    },
                    '311999': {
                        name: 'All Other Miscellaneous Food Manufacturing',
                    },
                },
            },
            '312': {
                name: 'Beverage and Tobacco Product Manufacturing',
                industries: {
                    '312111': {
                        name: 'Soft Drink Manufacturing',
                    },
                    '312112': {
                        name: 'Bottled Water Manufacturing',
                    },
                    '312113': {
                        name: 'Ice Manufacturing',
                    },
                    '312120': {
                        name: 'Breweries',
                    },
                    '312130': {
                        name: 'Wineries',
                    },
                    '312140': {
                        name: 'Distilleries',
                    },
                    '312230': {
                        name: 'Tobacco Manufacturing',
                    },
                },
            },
            '313': {
                name: 'Textile Mills',
                industries: {
                    '313110': {
                        name: 'Fiber, Yarn, and Thread Mills',
                    },
                    '313210': {
                        name: 'Broadwoven Fabric Mills',
                    },
                    '313220': {
                        name: 'Narrow Fabric Mills and Schiffli Machine Embroidery',
                    },
                    '313230': {
                        name: 'Nonwoven Fabric Mills',
                    },
                    '313240': {
                        name: 'Knit Fabric Mills',
                    },
                    '313310': {
                        name: 'Textile and Fabric Finishing Mills',
                    },
                    '313320': {
                        name: 'Fabric Coating Mills',
                    },
                },
            },
            '314': {
                name: 'Textile Product Mills',
                industries: {
                    '314110': {
                        name: 'Carpet and Rug Mills',
                    },
                    '314120': {
                        name: 'Curtain and Linen Mills',
                    },
                    '314910': {
                        name: 'Textile Bag and Canvas Mills',
                    },
                    '314994': {
                        name: 'Rope, Cordage, Twine, Tire Cord, and Tire Fabric Mills',
                    },
                    '314999': {
                        name: 'All Other Miscellaneous Textile Product Mills',
                    },
                },
            },
            '315': {
                name: 'Apparel Manufacturing',
                industries: {
                    '315110': {
                        name: 'Hosiery and Sock Mills',
                    },
                    '315190': {
                        name: 'Other Apparel Knitting Mills',
                    },
                    '315210': {
                        name: 'Cut and Sew Apparel Contractors',
                    },
                    '315220': {
                        name: "Men's and Boys' Cut and Sew Apparel Manufacturing",
                    },
                    '315240': {
                        name: "Women's, Girls', and Infants' Cut and Sew Apparel Manufacturing",
                    },
                    '315280': {
                        name: 'Other Cut and Sew Apparel Manufacturing',
                    },
                    '315990': {
                        name: 'Apparel Accessories and Other Apparel Manufacturing',
                    },
                },
            },
            '316': {
                name: 'Leather and Allied Product Manufacturing',
                industries: {
                    '316110': {
                        name: 'Leather and Hide Tanning and Finishing',
                    },
                    '316210': {
                        name: 'Footwear Manufacturing',
                    },
                    '316992': {
                        name: 'Women\u2019s Handbag and Purse Manufacturing',
                    },
                    '316998': {
                        name: 'All Other Leather Good and Allied Product Manufacturing',
                    },
                },
            },
            '321': {
                name: 'Wood Product Manufacturing',
                industries: {
                    '321113': {
                        name: 'Sawmills',
                    },
                    '321114': {
                        name: 'Wood Preservation',
                    },
                    '321211': {
                        name: 'Hardwood Veneer and Plywood Manufacturing',
                    },
                    '321212': {
                        name: 'Softwood Veneer and Plywood Manufacturing',
                    },
                    '321213': {
                        name: 'Engineered Wood Member (except Truss) Manufacturing',
                    },
                    '321214': {
                        name: 'Truss Manufacturing',
                    },
                    '321219': {
                        name: 'Reconstituted Wood Product Manufacturing',
                    },
                    '321911': {
                        name: 'Wood Window and Door Manufacturing',
                    },
                    '321912': {
                        name: 'Cut Stock, Resawing Lumber, and Planing',
                    },
                    '321918': {
                        name: 'Other Millwork (including Flooring)',
                    },
                    '321920': {
                        name: 'Wood Container and Pallet Manufacturing',
                    },
                    '321991': {
                        name: 'Manufactured Home (Mobile Home) Manufacturing',
                    },
                    '321992': {
                        name: 'Prefabricated Wood Building Manufacturing',
                    },
                    '321999': {
                        name: 'All Other Miscellaneous Wood Product Manufacturing',
                    },
                },
            },
            '322': {
                name: 'Paper Manufacturing',
                industries: {
                    '322110': {
                        name: 'Pulp Mills',
                    },
                    '322121': {
                        name: 'Paper (except Newsprint) Mills',
                    },
                    '322122': {
                        name: 'Newsprint Mills',
                    },
                    '322130': {
                        name: 'Paperboard Mills',
                    },
                    '322211': {
                        name: 'Corrugated and Solid Fiber Box Manufacturing',
                    },
                    '322212': {
                        name: 'Folding Paperboard Box Manufacturing',
                    },
                    '322219': {
                        name: 'Other Paperboard Container Manufacturing',
                    },
                    '322220': {
                        name: 'Paper Bag and Coated and Treated Paper Manufacturing',
                    },
                    '322230': {
                        name: 'Stationery Product Manufacturing',
                    },
                    '322291': {
                        name: 'Sanitary Paper Product Manufacturing',
                    },
                    '322299': {
                        name: 'All Other Converted Paper Product Manufacturing',
                    },
                },
            },
            '323': {
                name: 'Printing and Related Support Activities',
                industries: {
                    '323111': {
                        name: 'Commercial Printing (except Screen and Books)',
                    },
                    '323113': {
                        name: 'Commercial Screen Printing',
                    },
                    '323117': {
                        name: 'Books Printing',
                    },
                    '323120': {
                        name: 'Support Activities for Printing',
                    },
                },
            },
            '324': {
                name: 'Petroleum and Coal Products Manufacturing',
                industries: {
                    '324110': {
                        name: 'Petroleum Refineries',
                    },
                    '324121': {
                        name: 'Asphalt Paving Mixture and Block Manufacturing',
                    },
                    '324122': {
                        name: 'Asphalt Shingle and Coating Materials Manufacturing',
                    },
                    '324191': {
                        name: 'Petroleum Lubricating Oil and Grease Manufacturing',
                    },
                    '324199': {
                        name: 'All Other Petroleum and Coal Products Manufacturing',
                    },
                },
            },
            '325': {
                name: 'Chemical Manufacturing',
                industries: {
                    '325110': {
                        name: 'Petrochemical Manufacturing',
                    },
                    '325120': {
                        name: 'Industrial Gas Manufacturing',
                    },
                    '325130': {
                        name: 'Synthetic Dye and Pigment Manufacturing',
                    },
                    '325180': {
                        name: 'Other Basic Inorganic Chemical Manufacturing',
                    },
                    '325193': {
                        name: 'Ethyl Alcohol Manufacturing',
                    },
                    '325194': {
                        name: 'Cyclic Crude, Intermediate, and Gum and Wood Chemical Manufacturing',
                    },
                    '325199': {
                        name: 'All Other Basic Organic Chemical Manufacturing',
                    },
                    '325211': {
                        name: 'Plastics Material and Resin Manufacturing',
                    },
                    '325212': {
                        name: 'Synthetic Rubber Manufacturing',
                    },
                    '325220': {
                        name: 'Artificial and Synthetic Fibers and Filaments Manufacturing',
                    },
                    '325311': {
                        name: 'Nitrogenous Fertilizer Manufacturing',
                    },
                    '325312': {
                        name: 'Phosphatic Fertilizer Manufacturing',
                    },
                    '325314': {
                        name: 'Fertilizer (Mixing Only) Manufacturing',
                    },
                    '325320': {
                        name: 'Pesticide and Other Agricultural Chemical Manufacturing',
                    },
                    '325411': {
                        name: 'Medicinal and Botanical Manufacturing',
                    },
                    '325412': {
                        name: 'Pharmaceutical Preparation Manufacturing',
                    },
                    '325413': {
                        name: 'In-Vitro Diagnostic Substance Manufacturing',
                    },
                    '325414': {
                        name: 'Biological Product (except Diagnostic) Manufacturing',
                    },
                    '325510': {
                        name: 'Paint and Coating Manufacturing',
                    },
                    '325520': {
                        name: 'Adhesive Manufacturing',
                    },
                    '325611': {
                        name: 'Soap and Other Detergent Manufacturing',
                    },
                    '325612': {
                        name: 'Polish and Other Sanitation Good Manufacturing',
                    },
                    '325613': {
                        name: 'Surface Active Agent Manufacturing',
                    },
                    '325620': {
                        name: 'Toilet Preparation Manufacturing',
                    },
                    '325910': {
                        name: 'Printing Ink Manufacturing',
                    },
                    '325920': {
                        name: 'Explosives Manufacturing',
                    },
                    '325991': {
                        name: 'Custom Compounding of Purchased Resins',
                    },
                    '325992': {
                        name: 'Photographic Film, Paper, Plate, and Chemical Manufacturing',
                    },
                    '325998': {
                        name: 'All Other Miscellaneous Chemical Product and Preparation Manufacturing',
                    },
                },
            },
            '326': {
                name: 'Plastics and Rubber Products Manufacturing',
                industries: {
                    '326111': {
                        name: 'Plastic Bag and Pouch Manufacturing',
                    },
                    '326112': {
                        name: 'Plastics Packaging Film and Sheet (including Laminated) Manufacturing',
                    },
                    '326113': {
                        name: 'Unlaminated Plastics Film and Sheet (except Packaging) Manufacturing',
                    },
                    '326121': {
                        name: 'Unlaminated Plastics Profile Shape Manufacturing',
                    },
                    '326122': {
                        name: 'Plastics Pipe and Pipe Fitting Manufacturing',
                    },
                    '326130': {
                        name: 'Laminated Plastics Plate, Sheet (except Packaging), and Shape Manufacturing',
                    },
                    '326140': {
                        name: 'Polystyrene Foam Product Manufacturing',
                    },
                    '326150': {
                        name: 'Urethane and Other Foam Product (except Polystyrene) Manufacturing',
                    },
                    '326160': {
                        name: 'Plastics Bottle Manufacturing',
                    },
                    '326191': {
                        name: 'Plastics Plumbing Fixture Manufacturing',
                    },
                    '326199': {
                        name: 'All Other Plastics Product Manufacturing',
                    },
                    '326211': {
                        name: 'Tire Manufacturing (except Retreading)',
                    },
                    '326212': {
                        name: 'Tire Retreading',
                    },
                    '326220': {
                        name: 'Rubber and Plastics Hoses and Belting Manufacturing',
                    },
                    '326291': {
                        name: 'Rubber Product Manufacturing for Mechanical Use',
                    },
                    '326299': {
                        name: 'All Other Rubber Product Manufacturing',
                    },
                },
            },
            '327': {
                name: 'Nonmetallic Mineral Product Manufacturing',
                industries: {
                    '327110': {
                        name: 'Pottery, Ceramics, and Plumbing Fixture Manufacturing',
                    },
                    '327120': {
                        name: 'Clay Building Material and Refractories Manufacturing',
                    },
                    '327211': {
                        name: 'Flat Glass Manufacturing',
                    },
                    '327212': {
                        name: 'Other Pressed and Blown Glass and Glassware Manufacturing',
                    },
                    '327213': {
                        name: 'Glass Container Manufacturing',
                    },
                    '327215': {
                        name: 'Glass Product Manufacturing Made of Purchased Glass',
                    },
                    '327310': {
                        name: 'Cement Manufacturing',
                    },
                    '327320': {
                        name: 'Ready-Mix Concrete Manufacturing',
                    },
                    '327331': {
                        name: 'Concrete Block and Brick Manufacturing',
                    },
                    '327332': {
                        name: 'Concrete Pipe Manufacturing',
                    },
                    '327390': {
                        name: 'Other Concrete Product Manufacturing',
                    },
                    '327410': {
                        name: 'Lime Manufacturing',
                    },
                    '327420': {
                        name: 'Gypsum Product Manufacturing',
                    },
                    '327910': {
                        name: 'Abrasive Product Manufacturing',
                    },
                    '327991': {
                        name: 'Cut Stone and Stone Product Manufacturing',
                    },
                    '327992': {
                        name: 'Ground or Treated Mineral and Earth Manufacturing',
                    },
                    '327993': {
                        name: 'Mineral Wool Manufacturing',
                    },
                    '327999': {
                        name: 'All Other Miscellaneous Nonmetallic Mineral Product Manufacturing',
                    },
                },
            },
            '331': {
                name: 'Primary Metal Manufacturing',
                industries: {
                    '331110': {
                        name: 'Iron and Steel Mills and Ferroalloy Manufacturing',
                    },
                    '331210': {
                        name: 'Iron and Steel Pipe and Tube Manufacturing from Purchased Steel',
                    },
                    '331221': {
                        name: 'Rolled Steel Shape Manufacturing',
                    },
                    '331222': {
                        name: 'Steel Wire Drawing',
                    },
                    '331313': {
                        name: 'Alumina Refining and Primary Aluminum Production',
                    },
                    '331314': {
                        name: 'Secondary Smelting and Alloying of Aluminum',
                    },
                    '331315': {
                        name: 'Aluminum Sheet, Plate, and Foil Manufacturing',
                    },
                    '331318': {
                        name: 'Other Aluminum Rolling, Drawing, and Extruding',
                    },
                    '331410': {
                        name: 'Nonferrous Metal (except Aluminum) Smelting and Refining',
                    },
                    '331420': {
                        name: 'Copper Rolling, Drawing, Extruding, and Alloying',
                    },
                    '331491': {
                        name: 'Nonferrous Metal (except Copper and Aluminum) Rolling, Drawing, and Extruding',
                    },
                    '331492': {
                        name: 'Secondary Smelting, Refining, and Alloying of Nonferrous Metal (except Copper and Aluminum)',
                    },
                    '331511': {
                        name: 'Iron Foundries',
                    },
                    '331512': {
                        name: 'Steel Investment Foundries',
                    },
                    '331513': {
                        name: 'Steel Foundries (except Investment)',
                    },
                    '331523': {
                        name: 'Nonferrous Metal Die-Casting Foundries',
                    },
                    '331524': {
                        name: 'Aluminum Foundries (except Die-Casting)',
                    },
                    '331529': {
                        name: 'Other Nonferrous Metal Foundries (except Die-Casting)',
                    },
                },
            },
            '332': {
                name: 'Fabricated Metal Product Manufacturing',
                industries: {
                    '332111': {
                        name: 'Iron and Steel Forging',
                    },
                    '332112': {
                        name: 'Nonferrous Forging',
                    },
                    '332114': {
                        name: 'Custom Roll Forming',
                    },
                    '332117': {
                        name: 'Powder Metallurgy Part Manufacturing',
                    },
                    '332119': {
                        name: 'Metal Crown, Closure, and Other Metal Stamping (except Automotive)',
                    },
                    '332215': {
                        name: 'Metal Kitchen Cookware, Utensil, Cutlery, and Flatware (except Precious) Manufacturing',
                    },
                    '332216': {
                        name: 'Saw Blade and Handtool Manufacturing',
                    },
                    '332311': {
                        name: 'Prefabricated Metal Building and Component Manufacturing',
                    },
                    '332312': {
                        name: 'Fabricated Structural Metal Manufacturing',
                    },
                    '332313': {
                        name: 'Plate Work Manufacturing',
                    },
                    '332321': {
                        name: 'Metal Window and Door Manufacturing',
                    },
                    '332322': {
                        name: 'Sheet Metal Work Manufacturing',
                    },
                    '332323': {
                        name: 'Ornamental and Architectural Metal Work Manufacturing',
                    },
                    '332410': {
                        name: 'Power Boiler and Heat Exchanger Manufacturing',
                    },
                    '332420': {
                        name: 'Metal Tank (Heavy Gauge) Manufacturing',
                    },
                    '332431': {
                        name: 'Metal Can Manufacturing',
                    },
                    '332439': {
                        name: 'Other Metal Container Manufacturing',
                    },
                    '332510': {
                        name: 'Hardware Manufacturing',
                    },
                    '332613': {
                        name: 'Spring Manufacturing',
                    },
                    '332618': {
                        name: 'Other Fabricated Wire Product Manufacturing',
                    },
                    '332710': {
                        name: 'Machine Shops',
                    },
                    '332721': {
                        name: 'Precision Turned Product Manufacturing',
                    },
                    '332722': {
                        name: 'Bolt, Nut, Screw, Rivet, and Washer Manufacturing',
                    },
                    '332811': {
                        name: 'Metal Heat Treating',
                    },
                    '332812': {
                        name: 'Metal Coating, Engraving (except Jewelry and Silverware), and Allied Services to Manufacturers',
                    },
                    '332813': {
                        name: 'Electroplating, Plating, Polishing, Anodizing, and Coloring',
                    },
                    '332911': {
                        name: 'Industrial Valve Manufacturing',
                    },
                    '332912': {
                        name: 'Fluid Power Valve and Hose Fitting Manufacturing',
                    },
                    '332913': {
                        name: 'Plumbing Fixture Fitting and Trim Manufacturing',
                    },
                    '332919': {
                        name: 'Other Metal Valve and Pipe Fitting Manufacturing',
                    },
                    '332991': {
                        name: 'Ball and Roller Bearing Manufacturing',
                    },
                    '332992': {
                        name: 'Small Arms Ammunition Manufacturing',
                    },
                    '332993': {
                        name: 'Ammunition (except Small Arms) Manufacturing',
                    },
                    '332994': {
                        name: 'Small Arms, Ordnance, and Ordnance Accessories Manufacturing',
                    },
                    '332996': {
                        name: 'Fabricated Pipe and Pipe Fitting Manufacturing',
                    },
                    '332999': {
                        name: 'All Other Miscellaneous Fabricated Metal Product Manufacturing',
                    },
                },
            },
            '333': {
                name: 'Machinery Manufacturing',
                industries: {
                    '333111': {
                        name: 'Farm Machinery and Equipment Manufacturing',
                    },
                    '333112': {
                        name: 'Lawn and Garden Tractor and Home Lawn and Garden Equipment Manufacturing',
                    },
                    '333120': {
                        name: 'Construction Machinery Manufacturing',
                    },
                    '333131': {
                        name: 'Mining Machinery and Equipment Manufacturing',
                    },
                    '333132': {
                        name: 'Oil and Gas Field Machinery and Equipment Manufacturing',
                    },
                    '333241': {
                        name: 'Food Product Machinery Manufacturing',
                    },
                    '333242': {
                        name: 'Semiconductor Machinery Manufacturing',
                    },
                    '333243': {
                        name: 'Sawmill, Woodworking, and Paper Machinery Manufacturing',
                    },
                    '333244': {
                        name: 'Printing Machinery and Equipment Manufacturing',
                    },
                    '333249': {
                        name: 'Other Industrial Machinery Manufacturing',
                    },
                    '333314': {
                        name: 'Optical Instrument and Lens Manufacturing',
                    },
                    '333316': {
                        name: 'Photographic and Photocopying Equipment Manufacturing',
                    },
                    '333318': {
                        name: 'Other Commercial and Service Industry Machinery Manufacturing',
                    },
                    '333413': {
                        name: 'Industrial and Commercial Fan and Blower and Air Purification Equipment Manufacturing',
                    },
                    '333414': {
                        name: 'Heating Equipment (except Warm Air Furnaces) Manufacturing',
                    },
                    '333415': {
                        name: 'Air-Conditioning and Warm Air Heating Equipment and Commercial and Industrial Refrigeration Equipment Manufacturing',
                    },
                    '333511': {
                        name: 'Industrial Mold Manufacturing',
                    },
                    '333514': {
                        name: 'Special Die and Tool, Die Set, Jig, and Fixture Manufacturing',
                    },
                    '333515': {
                        name: 'Cutting Tool and Machine Tool Accessory Manufacturing',
                    },
                    '333517': {
                        name: 'Machine Tool Manufacturing',
                    },
                    '333519': {
                        name: 'Rolling Mill and Other Metalworking Machinery Manufacturing',
                    },
                    '333611': {
                        name: 'Turbine and Turbine Generator Set Unit Manufacturing',
                    },
                    '333612': {
                        name: 'Speed Changer, Industrial High-Speed Drive and Gear Manufacturing',
                    },
                    '333613': {
                        name: 'Mechanical Power Transmission Equipment Manufacturing',
                    },
                    '333618': {
                        name: 'Other Engine Equipment Manufacturing',
                    },
                    '333912': {
                        name: 'Air and Gas Compressor Manufacturing',
                    },
                    '333914': {
                        name: 'Measuring, Dispensing, and Other Pumping Equipment Manufacturing',
                    },
                    '333921': {
                        name: 'Elevator and Moving Stairway Manufacturing',
                    },
                    '333922': {
                        name: 'Conveyor and Conveying Equipment Manufacturing',
                    },
                    '333923': {
                        name: 'Overhead Traveling Crane, Hoist, and Monorail System Manufacturing',
                    },
                    '333924': {
                        name: 'Industrial Truck, Tractor, Trailer, and Stacker Machinery Manufacturing',
                    },
                    '333991': {
                        name: 'Power-Driven Hand Tool Manufacturing',
                    },
                    '333992': {
                        name: 'Welding and Soldering Equipment Manufacturing',
                    },
                    '333993': {
                        name: 'Packaging Machinery Manufacturing',
                    },
                    '333994': {
                        name: 'Industrial Process Furnace and Oven Manufacturing',
                    },
                    '333995': {
                        name: 'Fluid Power Cylinder and Actuator Manufacturing',
                    },
                    '333996': {
                        name: 'Fluid Power Pump and Motor Manufacturing',
                    },
                    '333997': {
                        name: 'Scale and Balance Manufacturing',
                    },
                    '333999': {
                        name: 'All Other Miscellaneous General Purpose Machinery Manufacturing',
                    },
                },
            },
            '334': {
                name: 'Computer and Electronic Product Manufacturing',
                industries: {
                    '334111': {
                        name: 'Electronic Computer Manufacturing',
                    },
                    '334112': {
                        name: 'Computer Storage Device Manufacturing',
                    },
                    '334118': {
                        name: 'Computer Terminal and Other Computer Peripheral Equipment Manufacturing',
                    },
                    '334210': {
                        name: 'Telephone Apparatus Manufacturing',
                    },
                    '334220': {
                        name: 'Radio and Television Broadcasting and Wireless Communications Equipment Manufacturing',
                    },
                    '334290': {
                        name: 'Other Communications Equipment Manufacturing',
                    },
                    '334310': {
                        name: 'Audio and Video Equipment Manufacturing',
                    },
                    '334412': {
                        name: 'Bare Printed Circuit Board Manufacturing',
                    },
                    '334413': {
                        name: 'Semiconductor and Related Device Manufacturing',
                    },
                    '334416': {
                        name: 'Capacitor, Resistor, Coil, Transformer, and Other Inductor Manufacturing',
                    },
                    '334417': {
                        name: 'Electronic Connector Manufacturing',
                    },
                    '334418': {
                        name: 'Printed Circuit Assembly (Electronic Assembly) Manufacturing',
                    },
                    '334419': {
                        name: 'Other Electronic Component Manufacturing',
                    },
                    '334510': {
                        name: 'Electromedical and Electrotherapeutic Apparatus Manufacturing',
                    },
                    '334511': {
                        name: 'Search, Detection, Navigation, Guidance, Aeronautical, and Nautical System and Instrument Manufacturing',
                    },
                    '334512': {
                        name: 'Automatic Environmental Control Manufacturing for Residential, Commercial, and Appliance Use',
                    },
                    '334513': {
                        name:
                            'Instruments and Related Products Manufacturing for Measuring, Displaying, and Controlling Industrial Process Variables',
                    },
                    '334514': {
                        name: 'Totalizing Fluid Meter and Counting Device Manufacturing',
                    },
                    '334515': {
                        name: 'Instrument Manufacturing for Measuring and Testing Electricity and Electrical Signals',
                    },
                    '334516': {
                        name: 'Analytical Laboratory Instrument Manufacturing',
                    },
                    '334517': {
                        name: 'Irradiation Apparatus Manufacturing',
                    },
                    '334519': {
                        name: 'Other Measuring and Controlling Device Manufacturing',
                    },
                    '334613': {
                        name: 'Blank Magnetic and Optical Recording Media Manufacturing',
                    },
                    '334614': {
                        name: 'Software and Other Prerecorded Compact Disc, Tape, and Record Reproducing',
                    },
                },
            },
            '335': {
                name: 'Electrical Equipment, Appliance, and Component Manufacturing',
                industries: {
                    '335110': {
                        name: 'Electric Lamp Bulb and Part Manufacturing',
                    },
                    '335121': {
                        name: 'Residential Electric Lighting Fixture Manufacturing',
                    },
                    '335122': {
                        name: 'Commercial, Industrial, and Institutional Electric Lighting Fixture Manufacturing',
                    },
                    '335129': {
                        name: 'Other Lighting Equipment Manufacturing',
                    },
                    '335210': {
                        name: 'Small Electrical Appliance Manufacturing',
                    },
                    '335220': {
                        name: 'Major Household Appliance Manufacturing',
                    },
                    '335311': {
                        name: 'Power, Distribution, and Specialty Transformer Manufacturing',
                    },
                    '335312': {
                        name: 'Motor and Generator Manufacturing',
                    },
                    '335313': {
                        name: 'Switchgear and Switchboard Apparatus Manufacturing',
                    },
                    '335314': {
                        name: 'Relay and Industrial Control Manufacturing',
                    },
                    '335911': {
                        name: 'Storage Battery Manufacturing',
                    },
                    '335912': {
                        name: 'Primary Battery Manufacturing',
                    },
                    '335921': {
                        name: 'Fiber Optic Cable Manufacturing',
                    },
                    '335929': {
                        name: 'Other Communication and Energy Wire Manufacturing',
                    },
                    '335931': {
                        name: 'Current-Carrying Wiring Device Manufacturing',
                    },
                    '335932': {
                        name: 'Noncurrent-Carrying Wiring Device Manufacturing',
                    },
                    '335991': {
                        name: 'Carbon and Graphite Product Manufacturing',
                    },
                    '335999': {
                        name: 'All Other Miscellaneous Electrical Equipment and Component Manufacturing',
                    },
                },
            },
            '336': {
                name: 'Transportation Equipment Manufacturing',
                industries: {
                    '336111': {
                        name: 'Automobile Manufacturing',
                    },
                    '336112': {
                        name: 'Light Truck and Utility Vehicle Manufacturing',
                    },
                    '336120': {
                        name: 'Heavy Duty Truck Manufacturing',
                    },
                    '336211': {
                        name: 'Motor Vehicle Body Manufacturing',
                    },
                    '336212': {
                        name: 'Truck Trailer Manufacturing',
                    },
                    '336213': {
                        name: 'Motor Home Manufacturing',
                    },
                    '336214': {
                        name: 'Travel Trailer and Camper Manufacturing',
                    },
                    '336310': {
                        name: 'Motor Vehicle Gasoline Engine and Engine Parts Manufacturing',
                    },
                    '336320': {
                        name: 'Motor Vehicle Electrical and Electronic Equipment Manufacturing',
                    },
                    '336330': {
                        name: 'Motor Vehicle Steering and Suspension Components (except Spring) Manufacturing',
                    },
                    '336340': {
                        name: 'Motor Vehicle Brake System Manufacturing',
                    },
                    '336350': {
                        name: 'Motor Vehicle Transmission and Power Train Parts Manufacturing',
                    },
                    '336360': {
                        name: 'Motor Vehicle Seating and Interior Trim Manufacturing',
                    },
                    '336370': {
                        name: 'Motor Vehicle Metal Stamping',
                    },
                    '336390': {
                        name: 'Other Motor Vehicle Parts Manufacturing',
                    },
                    '336411': {
                        name: 'Aircraft Manufacturing',
                    },
                    '336412': {
                        name: 'Aircraft Engine and Engine Parts Manufacturing',
                    },
                    '336413': {
                        name: 'Other Aircraft Part and Auxiliary Equipment Manufacturing',
                    },
                    '336414': {
                        name: 'Guided Missile and Space Vehicle Manufacturing',
                    },
                    '336415': {
                        name: 'Guided Missile and Space Vehicle Propulsion Unit and Propulsion Unit Parts Manufacturing',
                    },
                    '336419': {
                        name: 'Other Guided Missile and Space Vehicle Parts and Auxiliary Equipment Manufacturing',
                    },
                    '336510': {
                        name: 'Railroad Rolling Stock Manufacturing',
                    },
                    '336611': {
                        name: 'Ship Building and Repairing',
                    },
                    '336612': {
                        name: 'Boat Building',
                    },
                    '336991': {
                        name: 'Motorcycle, Bicycle, and Parts Manufacturing',
                    },
                    '336992': {
                        name: 'Military Armored Vehicle, Tank, and Tank Component Manufacturing',
                    },
                    '336999': {
                        name: 'All Other Transportation Equipment Manufacturing',
                    },
                },
            },
            '337': {
                name: 'Furniture and Related Product Manufacturing',
                industries: {
                    '337110': {
                        name: 'Wood Kitchen Cabinet and Counter Top Manufacturing',
                    },
                    '337121': {
                        name: 'Upholstered Household Furniture Manufacturing',
                    },
                    '337122': {
                        name: 'Nonupholstered Wood Household Furniture Manufacturing',
                    },
                    '337124': {
                        name: 'Metal Household Furniture Manufacturing',
                    },
                    '337125': {
                        name: 'Household Furniture (except Wood and Metal) Manufacturing',
                    },
                    '337127': {
                        name: 'Institutional Furniture Manufacturing',
                    },
                    '337211': {
                        name: 'Wood Office Furniture Manufacturing',
                    },
                    '337212': {
                        name: 'Custom Architectural Woodwork and Millwork Manufacturing',
                    },
                    '337214': {
                        name: 'Office Furniture (Except Wood) Manufacturing',
                    },
                    '337215': {
                        name: 'Showcase, Partition, Shelving, and Locker Manufacturing',
                    },
                    '337910': {
                        name: 'Mattress Manufacturing',
                    },
                    '337920': {
                        name: 'Blind and Shade Manufacturing',
                    },
                },
            },
            '339': {
                name: 'Miscellaneous Manufacturing',
                industries: {
                    '339112': {
                        name: 'Surgical and Medical Instrument Manufacturing',
                    },
                    '339113': {
                        name: 'Surgical Appliance and Supplies Manufacturing',
                    },
                    '339114': {
                        name: 'Dental Equipment and Supplies Manufacturing',
                    },
                    '339115': {
                        name: 'Ophthalmic Goods Manufacturing',
                    },
                    '339116': {
                        name: 'Dental Laboratories',
                    },
                    '339910': {
                        name: 'Jewelry and Silverware Manufacturing',
                    },
                    '339920': {
                        name: 'Sporting and Athletic Goods Manufacturing',
                    },
                    '339930': {
                        name: 'Doll, Toy, and Game Manufacturing',
                    },
                    '339940': {
                        name: 'Office Supplies (except Paper) Manufacturing',
                    },
                    '339950': {
                        name: 'Sign Manufacturing',
                    },
                    '339991': {
                        name: 'Gasket, Packing, and Sealing Device Manufacturing',
                    },
                    '339992': {
                        name: 'Musical Instrument Manufacturing',
                    },
                    '339993': {
                        name: 'Fastener, Button, Needle, and Pin Manufacturing',
                    },
                    '339994': {
                        name: 'Broom, Brush, and Mop Manufacturing',
                    },
                    '339995': {
                        name: 'Burial Casket Manufacturing',
                    },
                    '339999': {
                        name: 'All Other Miscellaneous Manufacturing',
                    },
                },
            },
        },
    },
    '42': {
        name: 'Wholesale Trade',
        subsectors: {
            '423': {
                name: 'Merchant Wholesalers, Durable Goods',
                industries: {
                    '423110': {
                        name: 'Automobile and Other Motor Vehicle Merchant Wholesalers',
                    },
                    '423120': {
                        name: 'Motor Vehicle Supplies and New Parts Merchant Wholesalers',
                    },
                    '423130': {
                        name: 'Tire and Tube Merchant Wholesalers',
                    },
                    '423140': {
                        name: 'Motor Vehicle Parts (Used) Merchant Wholesalers',
                    },
                    '423210': {
                        name: 'Furniture Merchant Wholesalers',
                    },
                    '423220': {
                        name: 'Home Furnishing Merchant Wholesalers',
                    },
                    '423310': {
                        name: 'Lumber, Plywood, Millwork, and Wood Panel Merchant Wholesalers',
                    },
                    '423320': {
                        name: 'Brick, Stone, and Related Construction Material Merchant Wholesalers',
                    },
                    '423330': {
                        name: 'Roofing, Siding, and Insulation Material Merchant Wholesalers',
                    },
                    '423390': {
                        name: 'Other Construction Material Merchant Wholesalers',
                    },
                    '423410': {
                        name: 'Photographic Equipment and Supplies Merchant Wholesalers',
                    },
                    '423420': {
                        name: 'Office Equipment Merchant Wholesalers',
                    },
                    '423430': {
                        name: 'Computer and Computer Peripheral Equipment and Software Merchant Wholesalers',
                    },
                    '423440': {
                        name: 'Other Commercial Equipment Merchant Wholesalers',
                    },
                    '423450': {
                        name: 'Medical, Dental, and Hospital Equipment and Supplies Merchant Wholesalers',
                    },
                    '423460': {
                        name: 'Ophthalmic Goods Merchant Wholesalers',
                    },
                    '423490': {
                        name: 'Other Professional Equipment and Supplies Merchant Wholesalers',
                    },
                    '423510': {
                        name: 'Metal Service Centers and Other Metal Merchant Wholesalers',
                    },
                    '423520': {
                        name: 'Coal and Other Mineral and Ore Merchant Wholesalers',
                    },
                    '423610': {
                        name: 'Electrical Apparatus and Equipment, Wiring Supplies, and Related Equipment Merchant Wholesalers',
                    },
                    '423620': {
                        name: 'Household Appliances, Electric Housewares, and Consumer Electronics Merchant Wholesalers',
                    },
                    '423690': {
                        name: 'Other Electronic Parts and Equipment Merchant Wholesalers',
                    },
                    '423710': {
                        name: 'Hardware Merchant Wholesalers',
                    },
                    '423720': {
                        name: 'Plumbing and Heating Equipment and Supplies (Hydronics) Merchant Wholesalers',
                    },
                    '423730': {
                        name: 'Warm Air Heating and Air-Conditioning Equipment and Supplies Merchant Wholesalers',
                    },
                    '423740': {
                        name: 'Refrigeration Equipment and Supplies Merchant Wholesalers',
                    },
                    '423810': {
                        name: 'Construction and Mining (except Oil Well) Machinery and Equipment Merchant Wholesalers',
                    },
                    '423820': {
                        name: 'Farm and Garden Machinery and Equipment Merchant Wholesalers',
                    },
                    '423830': {
                        name: 'Industrial Machinery and Equipment Merchant Wholesalers',
                    },
                    '423840': {
                        name: 'Industrial Supplies Merchant Wholesalers',
                    },
                    '423850': {
                        name: 'Service Establishment Equipment and Supplies Merchant Wholesalers',
                    },
                    '423860': {
                        name: 'Transportation Equipment and Supplies (except Motor Vehicle) Merchant Wholesalers',
                    },
                    '423910': {
                        name: 'Sporting and Recreational Goods and Supplies Merchant Wholesalers',
                    },
                    '423920': {
                        name: 'Toy and Hobby Goods and Supplies Merchant Wholesalers',
                    },
                    '423930': {
                        name: 'Recyclable Material Merchant Wholesalers',
                    },
                    '423940': {
                        name: 'Jewelry, Watch, Precious Stone, and Precious Metal Merchant Wholesalers',
                    },
                    '423990': {
                        name: 'Other Miscellaneous Durable Goods Merchant Wholesalers',
                    },
                },
            },
            '424': {
                name: 'Merchant Wholesalers, Nondurable Goods',
                industries: {
                    '424110': {
                        name: 'Printing and Writing Paper Merchant Wholesalers',
                    },
                    '424120': {
                        name: 'Stationary and Office Supplies Merchant Wholesalers',
                    },
                    '424130': {
                        name: 'Industrial and Personal Service Paper Merchant Wholesalers',
                    },
                    '424210': {
                        name: 'Drugs and Druggists\u2019 Sundries Merchant Wholesalers',
                    },
                    '424310': {
                        name: 'Piece Goods, Notions, and Other Dry Goods Merchant Wholesalers',
                    },
                    '424320': {
                        name: 'Men\u2019s and Boys\u2019 Clothing and Furnishings Merchant Wholesalers',
                    },
                    '424330': {
                        name: 'Women\u2019s, Children\u2019s, and Infants\u2019 Clothing and Accessories Merchant Wholesalers',
                    },
                    '424340': {
                        name: 'Footwear Merchant Wholesalers',
                    },
                    '424410': {
                        name: 'General Line Grocery Merchant Wholesalers',
                    },
                    '424420': {
                        name: 'Packaged Frozen Food Merchant Wholesalers',
                    },
                    '424430': {
                        name: 'Dairy Product (except Dried or Canned) Merchant Wholesalers',
                    },
                    '424440': {
                        name: 'Poultry and Poultry Product Merchant Wholesalers',
                    },
                    '424450': {
                        name: 'Confectionery Merchant Wholesalers',
                    },
                    '424460': {
                        name: 'Fish and Seafood Merchant Wholesalers',
                    },
                    '424470': {
                        name: 'Meat and Meat Product Merchant Wholesalers',
                    },
                    '424480': {
                        name: 'Fresh Fruit and Vegetable Merchant Wholesalers',
                    },
                    '424490': {
                        name: 'Other Grocery and Related Products Merchant Wholesalers',
                    },
                    '424510': {
                        name: 'Grain and Field Bean Merchant Wholesalers',
                    },
                    '424520': {
                        name: 'Livestock Merchant Wholesalers',
                    },
                    '424590': {
                        name: 'Other Farm Product Raw Material Merchant Wholesalers',
                    },
                    '424610': {
                        name: 'Plastics Materials and Basic Forms and Shapes Merchant Wholesalers',
                    },
                    '424690': {
                        name: 'Other Chemical and Allied Products Merchant Wholesalers',
                    },
                    '424710': {
                        name: 'Petroleum Bulk Stations and Terminals',
                    },
                    '424720': {
                        name: 'Petroleum and Petroleum Products Merchant Wholesalers (except Bulk Stations and Terminals)',
                    },
                    '424810': {
                        name: 'Beer and Ale Merchant Wholesalers',
                    },
                    '424820': {
                        name: 'Wine and Distilled Alcoholic Beverage Merchant Wholesalers',
                    },
                    '424910': {
                        name: 'Farm Supplies Merchant Wholesalers',
                    },
                    '424920': {
                        name: 'Book, Periodical, and Newspaper Merchant Wholesalers',
                    },
                    '424930': {
                        name: 'Flower, Nursery Stock, and Florists\u2019 Supplies Merchant Wholesalers',
                    },
                    '424940': {
                        name: 'Tobacco and Tobacco Product Merchant Wholesalers',
                    },
                    '424950': {
                        name: 'Paint, Varnish, and Supplies Merchant Wholesalers',
                    },
                    '424990': {
                        name: 'Other Miscellaneous Nondurable Goods Merchant Wholesalers',
                    },
                },
            },
            '425': {
                name: 'Wholesale Electronic Markets and Agents and Brokers',
                industries: {
                    '425110': {
                        name: 'Business to Business Electronic Markets',
                    },
                    '425120': {
                        name: 'Wholesale Trade Agents and Brokers',
                    },
                },
            },
        },
    },
    '44 - 45': {
        name: 'Retail Trade',
        subsectors: {
            '441': {
                name: 'Motor Vehicle and Parts Dealers',
                industries: {
                    '441110': {
                        name: 'New Car Dealers',
                    },
                    '441120': {
                        name: 'Used Car Dealers',
                    },
                    '441210': {
                        name: 'Recreational Vehicle Dealers',
                    },
                    '441222': {
                        name: 'Boat Dealers',
                    },
                    '441228': {
                        name: 'Motorcycle, ATV, and All Other Motor Vehicle Dealers',
                    },
                    '441310': {
                        name: 'Automotive Parts and Accessories Stores',
                    },
                    '441320': {
                        name: 'Tire Dealers',
                    },
                },
            },
            '442': {
                name: 'Furniture and Home Furnishings Stores',
                industries: {
                    '442110': {
                        name: 'Furniture Stores',
                    },
                    '442210': {
                        name: 'Floor Covering Stores',
                    },
                    '442291': {
                        name: 'Window Treatment Stores',
                    },
                    '442299': {
                        name: 'All Other Home Furnishings Stores',
                    },
                },
            },
            '443': {
                name: 'Electronics and Appliance Stores',
                industries: {
                    '443141': {
                        name: 'Household Appliance Stores',
                    },
                    '443142': {
                        name: 'Electronics Stores',
                    },
                },
            },
            '444': {
                name: 'Building Material and Garden Equipment and Supplies Dealers',
                industries: {
                    '444110': {
                        name: 'Home Centers',
                    },
                    '444120': {
                        name: 'Paint and Wallpaper Stores',
                    },
                    '444130': {
                        name: 'Hardware Stores',
                    },
                    '444190': {
                        name: 'Other Building Material Dealers',
                    },
                    '444210': {
                        name: 'Outdoor Power Equipment Stores',
                    },
                    '444220': {
                        name: 'Nursery and Garden Centers',
                    },
                },
            },
            '445': {
                name: 'Food and Beverage Stores',
                industries: {
                    '445110': {
                        name: 'Supermarkets and Other Grocery (except Convenience) Stores',
                    },
                    '445120': {
                        name: 'Convenience Stores',
                    },
                    '445210': {
                        name: 'Meat Markets',
                    },
                    '445220': {
                        name: 'Fish and Seafood Markets',
                    },
                    '445230': {
                        name: 'Fruit and Vegetable Markets',
                    },
                    '445291': {
                        name: 'Baked Goods Stores',
                    },
                    '445292': {
                        name: 'Confectionery and Nut Stores',
                    },
                    '445299': {
                        name: 'All Other Specialty Food Stores',
                    },
                    '445310': {
                        name: 'Beer, Wine, and Liquor Stores',
                    },
                },
            },
            '446': {
                name: 'Health and Personal Care Stores',
                industries: {
                    '446110': {
                        name: 'Pharmacies and Drug Stores',
                    },
                    '446120': {
                        name: 'Cosmetics, Beauty Supplies and Perfume Stores',
                    },
                    '446130': {
                        name: 'Optical Goods Stores',
                    },
                    '446191': {
                        name: 'Food (Health) Supplement Stores',
                    },
                    '446199': {
                        name: 'All Other Health and Personal Care Stores',
                    },
                },
            },
            '447': {
                name: 'Gasoline Stations',
                industries: {
                    '447110': {
                        name: 'Gasoline Stations with Convenience Stores',
                    },
                    '447190': {
                        name: 'Other Gasoline Stations',
                    },
                },
            },
            '448': {
                name: 'Clothing and Clothing Accessories Stores',
                industries: {
                    '448110': {
                        name: 'Men\u2019s Clothing Stores',
                    },
                    '448120': {
                        name: 'Women\u2019s Clothing Stores',
                    },
                    '448130': {
                        name: 'Children\u2019s and Infants\u2019 Clothing Stores',
                    },
                    '448140': {
                        name: 'Family Clothing Stores',
                    },
                    '448150': {
                        name: 'Clothing Accessories Stores',
                    },
                    '448190': {
                        name: 'Other Clothing Stores',
                    },
                    '448210': {
                        name: 'Shoe Stores',
                    },
                    '448310': {
                        name: 'Jewelry Stores',
                    },
                    '448320': {
                        name: 'Luggage and Leather Goods Stores',
                    },
                },
            },
            '451': {
                name: 'Sporting Good, Hobby, Book, and Music Stores',
                industries: {
                    '451110': {
                        name: 'Sporting Goods Stores',
                    },
                    '451120': {
                        name: 'Hobby, Toy, and Game Stores',
                    },
                    '451130': {
                        name: 'Sewing, Needlework, and Piece Goods Stores',
                    },
                    '451140': {
                        name: 'Musical Instrument and Supplies Stores',
                    },
                    '451211': {
                        name: 'Book Stores',
                    },
                    '451212': {
                        name: 'News Dealers and Newsstands',
                    },
                },
            },
            '452': {
                name: 'General Merchandise Stores',
                industries: {
                    '452210': {
                        name: 'Department Stores',
                    },
                    '452311': {
                        name: 'Warehouse Clubs and Supercenters',
                    },
                    '452319': {
                        name: 'All Other General Merchandise Stores',
                    },
                },
            },
            '453': {
                name: 'Miscellaneous Store Retailers',
                industries: {
                    '453110': {
                        name: 'Florists',
                    },
                    '453210': {
                        name: 'Office Supplies and Stationery Stores',
                    },
                    '453220': {
                        name: 'Gift, Novelty, and Souvenir Stores',
                    },
                    '453310': {
                        name: 'Used Merchandise Stores',
                    },
                    '453910': {
                        name: 'Pet and Pet Supplies Stores',
                    },
                    '453920': {
                        name: 'Art Dealers',
                    },
                    '453930': {
                        name: 'Manufactured (Mobile) Home Dealers',
                    },
                    '453991': {
                        name: 'Tobacco Stores',
                    },
                    '453998': {
                        name: 'All Other Miscellaneous Store Retailers (except Tobacco Stores)',
                    },
                },
            },
            '454': {
                name: 'Nonstore Retailers',
                industries: {
                    '454110': {
                        name: 'Electronic Shopping and Mail-Order Houses',
                    },
                    '454210': {
                        name: 'Vending Machine Operators',
                    },
                    '454310': {
                        name: 'Fuel Dealers',
                    },
                    '454390': {
                        name: 'Other Direct Selling Establishments',
                    },
                },
            },
        },
    },
    '48 - 49': {
        name: 'Transportation and Warehousing',
        subsectors: {
            '481': {
                name: 'Air Transportation',
                industries: {
                    '481111': {
                        name: 'Scheduled Passenger Air Transportation',
                    },
                    '481112': {
                        name: 'Scheduled Freight Air Transportation',
                    },
                    '481211': {
                        name: 'Nonscheduled Chartered Passenger Air Transportation',
                    },
                    '481212': {
                        name: 'Nonscheduled Chartered Freight Air Transportation',
                    },
                    '481219': {
                        name: 'Other Nonscheduled Air Transportation',
                    },
                },
            },
            '482': {
                name: 'Rail Transportation',
                industries: {
                    '482111': {
                        name: 'Line-Haul Railroads',
                    },
                    '482112': {
                        name: 'Short Line Railroads',
                    },
                },
            },
            '483': {
                name: 'Water Transportation',
                industries: {
                    '483111': {
                        name: 'Deep Sea Freight Transportation',
                    },
                    '483112': {
                        name: 'Deep Sea Passenger Transportation',
                    },
                    '483113': {
                        name: 'Coastal and Great Lakes Freight Transportation',
                    },
                    '483114': {
                        name: 'Coastal and Great Lakes Passenger Transportation',
                    },
                    '483211': {
                        name: 'Inland Water Freight Transportation',
                    },
                    '483212': {
                        name: 'Inland Water Passenger Transportation',
                    },
                },
            },
            '484': {
                name: 'Truck Transportation',
                industries: {
                    '484110': {
                        name: 'General Freight Trucking, Local',
                    },
                    '484121': {
                        name: 'General Freight Trucking, Long-Distance, Truckload',
                    },
                    '484122': {
                        name: 'General Freight Trucking, Long-Distance, Less Than Truckload',
                    },
                    '484210': {
                        name: 'Used Household and Office Goods Moving',
                    },
                    '484220': {
                        name: 'Specialized Freight (except Used Goods) Trucking, Local',
                    },
                    '484230': {
                        name: 'Specialized Freight (except Used Goods) Trucking, Long-Distance',
                    },
                },
            },
            '485': {
                name: 'Transit and Ground Passenger Transportation',
                industries: {
                    '485111': {
                        name: 'Mixed Mode Transit Systems',
                    },
                    '485112': {
                        name: 'Commuter Rail Systems',
                    },
                    '485113': {
                        name: 'Bus and Other Motor Vehicle Transit Systems',
                    },
                    '485119': {
                        name: 'Other Urban Transit Systems',
                    },
                    '485210': {
                        name: 'Interurban and Rural Bus Transportation',
                    },
                    '485310': {
                        name: 'Taxi Service',
                    },
                    '485320': {
                        name: 'Limousine Service',
                    },
                    '485410': {
                        name: 'School and Employee Bus Transportation',
                    },
                    '485510': {
                        name: 'Charter Bus Industry',
                    },
                    '485991': {
                        name: 'Special Needs Transportation',
                    },
                    '485999': {
                        name: 'All Other Transit and Ground Passenger Transportation',
                    },
                },
            },
            '486': {
                name: 'Pipeline Transportation',
                industries: {
                    '486110': {
                        name: 'Pipeline Transportation of Crude Oil',
                    },
                    '486210': {
                        name: 'Pipeline Transportation of Natural Gas',
                    },
                    '486910': {
                        name: 'Pipeline Transportation of Refined Petroleum Products',
                    },
                    '486990': {
                        name: 'All Other Pipeline Transportation',
                    },
                },
            },
            '487': {
                name: 'Scenic and Sightseeing Transportation',
                industries: {
                    '487110': {
                        name: 'Scenic and Sightseeing Transportation, Land',
                    },
                    '487210': {
                        name: 'Scenic and Sightseeing Transportation, Water',
                    },
                    '487990': {
                        name: 'Scenic and Sightseeing Transportation, Other',
                    },
                },
            },
            '488': {
                name: 'Support Activities for Transportation',
                industries: {
                    '488111': {
                        name: 'Air Traffic Control',
                    },
                    '488119': {
                        name: 'Other Airport Operations',
                    },
                    '488190': {
                        name: 'Other Support Activities for Air Transportation',
                    },
                    '488210': {
                        name: 'Support Activities for Rail Transportation',
                    },
                    '488310': {
                        name: 'Port and Harbor Operations',
                    },
                    '488320': {
                        name: 'Marine Cargo Handling',
                    },
                    '488330': {
                        name: 'Navigational Services to Shipping',
                    },
                    '488390': {
                        name: 'Other Support Activities for Water Transportation',
                    },
                    '488410': {
                        name: 'Motor Vehicle Towing',
                    },
                    '488490': {
                        name: 'Other Support Activities for Road Transportation',
                    },
                    '488510': {
                        name: 'Freight Transportation Arrangement',
                    },
                    '488991': {
                        name: 'Packing and Crating',
                    },
                    '488999': {
                        name: 'All Other Support Activities for Transportation',
                    },
                },
            },
            '491': {
                name: 'Postal Service',
                industries: {
                    '491110': {
                        name: 'Postal Service',
                    },
                },
            },
            '492': {
                name: 'Couriers and Messengers',
                industries: {
                    '492110': {
                        name: 'Couriers and Express Delivery Services',
                    },
                    '492210': {
                        name: 'Local Messengers and Local Delivery',
                    },
                },
            },
            '493': {
                name: 'Warehousing and Storage',
                industries: {
                    '493110': {
                        name: 'General Warehousing and Storage',
                    },
                    '493120': {
                        name: 'Refrigerated Warehousing and Storage',
                    },
                    '493130': {
                        name: 'Farm Product Warehousing and Storage',
                    },
                    '493190': {
                        name: 'Other Warehousing and Storage',
                    },
                },
            },
        },
    },
    '51': {
        name: 'Information',
        subsectors: {
            '511': {
                name: 'Publishing Industries (except Internet)',
                industries: {
                    '511110': {
                        name: 'Newspaper Publishers',
                    },
                    '511120': {
                        name: 'Periodical Publishers',
                    },
                    '511130': {
                        name: 'Book Publishers',
                    },
                    '511140': {
                        name: 'Directory and Mailing List Publishers',
                    },
                    '511191': {
                        name: 'Greeting Card Publishers',
                    },
                    '511199': {
                        name: 'All Other Publishers',
                    },
                    '511210': {
                        name: 'Software Publishers',
                    },
                },
            },
            '512': {
                name: 'Motion Picture and Sound Recording Industries',
                industries: {
                    '512110': {
                        name: 'Motion Picture and Video Production',
                    },
                    '512120': {
                        name: 'Motion Picture and Video Distribution',
                    },
                    '512131': {
                        name: 'Motion Picture Theaters (except Drive-Ins)',
                    },
                    '512132': {
                        name: 'Drive-In Motion Picture Theaters',
                    },
                    '512191': {
                        name: 'Teleproduction and Other Postproduction Services',
                    },
                    '512199': {
                        name: 'Other Motion Picture and Video Industries',
                    },
                    '512230': {
                        name: 'Music Publishers',
                    },
                    '512240': {
                        name: 'Sound Recording Studios',
                    },
                    '512250': {
                        name: 'Record Production and Distribution',
                    },
                    '512290': {
                        name: 'Other Sound Recording Industries',
                    },
                },
            },
            '515': {
                name: 'Broadcasting (except Internet)',
                industries: {
                    '515111': {
                        name: 'Radio Networks',
                    },
                    '515112': {
                        name: 'Radio Stations',
                    },
                    '515120': {
                        name: 'Television Broadcasting',
                    },
                    '515210': {
                        name: 'Cable and Other Subscription Programming',
                    },
                },
            },
            '517': {
                name: 'Telecommunications',
                industries: {
                    '517311': {
                        name: 'Wired Telecommunications Carriers',
                    },
                    '517312': {
                        name: 'Wireless Telecommunications Carriers (except Satellite)',
                    },
                    '517410': {
                        name: 'Satellite Telecommunications',
                    },
                    '517911': {
                        name: 'Telecommunications Resellers',
                    },
                    '517919': {
                        name: 'All Other Telecommunications',
                    },
                },
            },
            '518': {
                name: 'Data Processing - Hosting, and Related Services',
                industries: {
                    '518210': {
                        name: 'Data Processing, Hosting, and Related Services',
                    },
                },
            },
            '519': {
                name: 'Other Information Services',
                industries: {
                    '519110': {
                        name: 'News Syndicates',
                    },
                    '519120': {
                        name: 'Libraries and Archives',
                    },
                    '519130': {
                        name: 'Internet Publishing and Broadcasting and Web Search Portals',
                    },
                    '519190': {
                        name: 'All Other Information Services',
                    },
                },
            },
        },
    },
    '52': {
        name: 'Finance and Insurance',
        subsectors: {
            '522': {
                name: 'Credit Intermediation and Related Activities',
                industries: {
                    '522110': {
                        name: 'Commercial Banking',
                    },
                    '522120': {
                        name: 'Savings Institutions',
                    },
                    '522130': {
                        name: 'Credit Unions',
                    },
                    '522190': {
                        name: 'Other Depository Credit Intermediation8',
                    },
                    '522210': {
                        name: 'Credit Card Issuing',
                    },
                    '522220': {
                        name: 'Sales Financing',
                    },
                    '522291': {
                        name: 'Consumer Lending',
                    },
                    '522292': {
                        name: 'Real Estate Credit',
                    },
                    '522293': {
                        name: 'International Trade Financing',
                    },
                    '522294': {
                        name: 'Secondary Market Financing',
                    },
                    '522298': {
                        name: 'All Other Nondepository Credit Intermediation',
                    },
                    '522310': {
                        name: 'Mortgage and Nonmortgage Loan Brokers',
                    },
                    '522320': {
                        name: 'Financial Transactions Processing, Reserve, and Clearinghouse Activities',
                    },
                    '522390': {
                        name: 'Other Activities Related to Credit Intermediation',
                    },
                },
            },
            '523': {
                name: 'Financial Investments and Related Activities',
                industries: {
                    '523110': {
                        name: 'Investment Banking and Securities Dealing',
                    },
                    '523120': {
                        name: 'Securities Brokerage',
                    },
                    '523130': {
                        name: 'Commodity Contracts Dealing',
                    },
                    '523140': {
                        name: 'Commodity Contracts Brokerage',
                    },
                    '523210': {
                        name: 'Securities and Commodity Exchanges',
                    },
                    '523910': {
                        name: 'Miscellaneous Intermediation',
                    },
                    '523920': {
                        name: 'Portfolio Management',
                    },
                    '523930': {
                        name: 'Investment Advice',
                    },
                    '523991': {
                        name: 'Trust, Fiduciary, and Custody Activities',
                    },
                    '523999': {
                        name: 'Miscellaneous Financial Investment Activities',
                    },
                },
            },
            '524': {
                name: 'Insurance Carriers and Related Activities',
                industries: {
                    '524113': {
                        name: 'Direct Life Insurance Carriers',
                    },
                    '524114': {
                        name: 'Direct Health and Medical Insurance Carriers',
                    },
                    '524126': {
                        name: 'Direct Property and Casualty Insurance Carriers',
                    },
                    '524127': {
                        name: 'Direct Title Insurance Carriers',
                    },
                    '524128': {
                        name: 'Other Direct Insurance (except Life, Health, and Medical) Carriers',
                    },
                    '524130': {
                        name: 'Reinsurance Carriers',
                    },
                    '524210': {
                        name: 'Insurance Agencies and Brokerages',
                    },
                    '524291': {
                        name: 'Claims Adjusting',
                    },
                    '524292': {
                        name: 'Third Party Administration of Insurance and Pension Funds',
                    },
                    '524298': {
                        name: 'All Other Insurance Related Activities',
                    },
                },
            },
            '525': {
                name: 'Funds, Trusts, and Other Financial Vehicles',
                industries: {
                    '525110': {
                        name: 'Pension Funds',
                    },
                    '525120': {
                        name: 'Health and Welfare Funds',
                    },
                    '525190': {
                        name: 'Other Insurance Funds',
                    },
                    '525910': {
                        name: 'Open-End Investment Funds',
                    },
                    '525920': {
                        name: 'Trusts, Estates, and Agency Accounts',
                    },
                    '525990': {
                        name: 'Other Financial Vehicles',
                    },
                },
            },
        },
    },
    '53': {
        name: 'Real Estate and Rental and Leasing',
        subsectors: {
            '531': {
                name: 'Real Estate',
                industries: {
                    '531110': {
                        name: 'Lessors of Residential Buildings and Dwellings',
                    },
                    '531120': {
                        name: 'Lessors of Nonresidential Buildings (except Miniwarehouses)',
                    },
                    '531130': {
                        name: 'Lessors of Miniwarehouses and Self Storage Units',
                    },
                    '531190': {
                        name: 'Lessors of Other Real Estate Property',
                    },
                    '531210': {
                        name: 'Offices of Real Estate Agents and Brokers',
                    },
                    '531311': {
                        name: 'Residential Property Managers',
                    },
                    '531312': {
                        name: 'Nonresidential Property Managers',
                    },
                    '531320': {
                        name: 'Offices of Real Estate Appraisers',
                    },
                    '531390': {
                        name: 'Other Activities Related to Real Estate',
                    },
                },
            },
            '532': {
                name: 'Rental and Leasing Services',
                industries: {
                    '532111': {
                        name: 'Passenger Car Rental',
                    },
                    '532112': {
                        name: 'Passenger Car Leasing',
                    },
                    '532120': {
                        name: 'Truck, Utility Trailer, and RV (Recreational Vehicle) Rental and Leasing',
                    },
                    '532210': {
                        name: 'Consumer Electronics and Appliances Rental',
                    },
                    '532281': {
                        name: 'Formal Wear and Costume Rental',
                    },
                    '532282': {
                        name: 'Video Tape and Disc Rental',
                    },
                    '532283': {
                        name: 'Home Health Equipment Rental',
                    },
                    '532284': {
                        name: 'Recreational Goods Rental',
                    },
                    '532289': {
                        name: 'All Other Consumer Goods Rental',
                    },
                    '532310': {
                        name: 'General Rental Centers',
                    },
                    '532411': {
                        name: 'Commercial Air, Rail, and Water Transportation Equipment Rental and Leasing',
                    },
                    '532412': {
                        name: 'Construction, Mining, and Forestry Machinery and Equipment Rental and Leasing',
                    },
                    '532420': {
                        name: 'Office Machinery and Equipment Rental and Leasing',
                    },
                    '532490': {
                        name: 'Other Commercial and Industrial Machinery and Equipment Rental and Leasing',
                    },
                },
            },
            '533': {
                name: 'Lessors of Nonfinancial Intangible Assets (except Copyrighted Works)',
                industries: {
                    '533110': {
                        name: 'Lessors of Nonfinancial Intangible Assets (except Copyrighted Works)',
                    },
                },
            },
        },
    },
    '54': {
        name: 'Professional, Scientific, and Technical Services',
        subsectors: {
            '541': {
                name: 'Professional, Scientific, and Technical Services',
                industries: {
                    '541110': {
                        name: 'Offices of Lawyers',
                    },
                    '541191': {
                        name: 'Title Abstract and Settlement Offices',
                    },
                    '541199': {
                        name: 'All Other Legal Services',
                    },
                    '541211': {
                        name: 'Offices of Certified Public Accountants',
                    },
                    '541213': {
                        name: 'Tax Preparation Services',
                    },
                    '541214': {
                        name: 'Payroll Services',
                    },
                    '541219': {
                        name: 'Other Accounting Services',
                    },
                    '541310': {
                        name: 'Architectural Services',
                    },
                    '541320': {
                        name: 'Landscape Architectural Services',
                    },
                    '541330': {
                        name: 'Engineering Services',
                    },
                    '541340': {
                        name: 'Drafting Services',
                    },
                    '541350': {
                        name: 'Building Inspection Services',
                    },
                    '541360': {
                        name: 'Geophysical Surveying and Mapping Services',
                    },
                    '541370': {
                        name: 'Surveying and Mapping (except Geophysical) Services',
                    },
                    '541380': {
                        name: 'Testing Laboratories',
                    },
                    '541410': {
                        name: 'Interior Design Services',
                    },
                    '541420': {
                        name: 'Industrial Design Services',
                    },
                    '541430': {
                        name: 'Graphic Design Services',
                    },
                    '541490': {
                        name: 'Other Specialized Design Services',
                    },
                    '541511': {
                        name: 'Custom Computer Programming Services',
                    },
                    '541512': {
                        name: 'Computer Systems Design Services',
                    },
                    '541513': {
                        name: 'Computer Facilities Management Services',
                    },
                    '541519': {
                        name: 'Other Computer Related Services',
                    },
                    '541611': {
                        name: 'Administrative Management and General Management Consulting Services',
                    },
                    '541612': {
                        name: 'Human Resources Consulting Services',
                    },
                    '541613': {
                        name: 'Marketing Consulting Services',
                    },
                    '541614': {
                        name: 'Process, Physical Distribution, and Logistics Consulting Services',
                    },
                    '541618': {
                        name: 'Other Management Consulting Services',
                    },
                    '541620': {
                        name: 'Environmental Consulting Services',
                    },
                    '541690': {
                        name: 'Other Scientific and Technical Consulting Services',
                    },
                    '541713': {
                        name: 'Research and Technology in Nanotechnology',
                    },
                    '541714': {
                        name: 'Research and Technology in Biotechnology (except Nanobiotechnology)',
                    },
                    '541715': {
                        name: 'Research and Development in the Physical, Engineering, and Life Sciences (except Nanotechnology and Biotechnology)',
                    },
                    '541720': {
                        name: 'Research and Development in the Social Sciences and Humanities',
                    },
                    '541810': {
                        name: 'Advertising Agencies',
                    },
                    '541820': {
                        name: 'Public Relations Agencies',
                    },
                    '541830': {
                        name: 'Media Buying Agencies',
                    },
                    '541840': {
                        name: 'Media Representatives',
                    },
                    '541850': {
                        name: 'Outdoor Advertising',
                    },
                    '541860': {
                        name: 'Direct Mail Advertising',
                    },
                    '541870': {
                        name: 'Advertising Material Distribution Services',
                    },
                    '541890': {
                        name: 'Other Services Related to Advertising',
                    },
                    '541910': {
                        name: 'Marketing Research and Public Opinion Polling',
                    },
                    '541921': {
                        name: 'Photography Studios, Portrait',
                    },
                    '541922': {
                        name: 'Commercial Photography',
                    },
                    '541930': {
                        name: 'Translation and Interpretation Services',
                    },
                    '541940': {
                        name: 'Veterinary Services',
                    },
                    '541990': {
                        name: 'All Other Professional, Scientific, and Technical Services',
                    },
                },
            },
        },
    },
    '55': {
        name: 'Management of Companies and Enterprises',
        subsectors: {
            '551': {
                name: 'Management of Companies and Enterprises',
                industries: {
                    '551111': {
                        name: 'Offices of Bank Holding Companies',
                    },
                    '551112': {
                        name: 'Offices of Other Holding Companies',
                    },
                },
            },
        },
    },
    '56': {
        name: 'Administrative and Support, Waste Management, and Remediation Services',
        subsectors: {
            '561': {
                name: 'Administrative and Support Services',
                industries: {
                    '561110': {
                        name: 'Office Administrative Services',
                    },
                    '561210': {
                        name: 'Facilities Support Services',
                    },
                    '561311': {
                        name: 'Employment Placement Agencies',
                    },
                    '561312': {
                        name: 'Executive Search Services',
                    },
                    '561320': {
                        name: 'Temporary Help Services',
                    },
                    '561330': {
                        name: 'Professional Employer Organizations',
                    },
                    '561410': {
                        name: 'Document Preparation Services',
                    },
                    '561421': {
                        name: 'Telephone Answering Services',
                    },
                    '561422': {
                        name: 'Telemarketing Bureaus and Other contact Centers',
                    },
                    '561431': {
                        name: 'Private Mail Centers',
                    },
                    '561439': {
                        name: 'Other Business Service Centers (including Copy Shops)',
                    },
                    '561440': {
                        name: 'Collection Agencies',
                    },
                    '561450': {
                        name: 'Credit Bureaus',
                    },
                    '561491': {
                        name: 'Repossession Services',
                    },
                    '561492': {
                        name: 'Court Reporting and Stenotype Services',
                    },
                    '561499': {
                        name: 'All Other Business Support Services',
                    },
                    '561510': {
                        name: 'Travel Agencies',
                    },
                    '561520': {
                        name: 'Tour Operators',
                    },
                    '561591': {
                        name: 'Convention and Visitors Bureaus',
                    },
                    '561599': {
                        name: 'All Other Travel Arrangement and Reservation Services',
                    },
                    '561611': {
                        name: 'Investigation Services',
                    },
                    '561612': {
                        name: 'Security Guards and Patrol Services',
                    },
                    '561613': {
                        name: 'Armored Car Services',
                    },
                    '561621': {
                        name: 'Security Systems Services (except Locksmiths)',
                    },
                    '561622': {
                        name: 'Locksmiths',
                    },
                    '561710': {
                        name: 'Exterminating and Pest Control Services',
                    },
                    '561720': {
                        name: 'Janitorial Services',
                    },
                    '561730': {
                        name: 'Landscaping Services',
                    },
                    '561740': {
                        name: 'Carpet and Upholstery Cleaning Services',
                    },
                    '561790': {
                        name: 'Other Services to Buildings and Dwellings',
                    },
                    '561910': {
                        name: 'Packaging and Labeling Services',
                    },
                    '561920': {
                        name: 'Convention and Trade Show Organizers',
                    },
                    '561990': {
                        name: 'All Other Support Services',
                    },
                },
            },
            '562': {
                name: 'Waste Management and Remediation Services',
                industries: {
                    '562111': {
                        name: 'Solid Waste Collection',
                    },
                    '562112': {
                        name: 'Hazardous Waste Collection',
                    },
                    '562119': {
                        name: 'Other Waste Collection',
                    },
                    '562211': {
                        name: 'Hazardous Waste Treatment and Disposal',
                    },
                    '562212': {
                        name: 'Solid Waste Landfill',
                    },
                    '562213': {
                        name: 'Solid Waste Combustors and Incinerators',
                    },
                    '562219': {
                        name: 'Other Nonhazardous Waste Treatment and Disposal',
                    },
                    '562910': {
                        name: 'Remediation Services',
                    },
                    '562920': {
                        name: 'Materials Recovery Facilities',
                    },
                    '562991': {
                        name: 'Septic Tank and Related Services',
                    },
                    '562998': {
                        name: 'All Other Miscellaneous Waste Management Services',
                    },
                },
            },
        },
    },
    '61': {
        name: 'Educational Services',
        subsectors: {
            '611': {
                name: 'Educational Services',
                industries: {
                    '611110': {
                        name: 'Elementary and Secondary Schools',
                    },
                    '611210': {
                        name: 'Junior Colleges',
                    },
                    '611310': {
                        name: 'Colleges, Universities, and Professional Schools',
                    },
                    '611410': {
                        name: 'Business and Secretarial Schools',
                    },
                    '611420': {
                        name: 'Computer Training',
                    },
                    '611430': {
                        name: 'Professional and Management Development Training',
                    },
                    '611511': {
                        name: 'Cosmetology and Barber Schools',
                    },
                    '611512': {
                        name: 'Flight Training',
                    },
                    '611513': {
                        name: 'Apprenticeship Training',
                    },
                    '611519': {
                        name: 'Other Technical and Trade Schools',
                    },
                    '611610': {
                        name: 'Fine Arts Schools',
                    },
                    '611620': {
                        name: 'Sports and Recreation Instruction',
                    },
                    '611630': {
                        name: 'Language Schools',
                    },
                    '611691': {
                        name: 'Exam Preparation and Tutoring',
                    },
                    '611692': {
                        name: 'Automobile Driving Schools',
                    },
                    '611699': {
                        name: 'All Other Miscellaneous Schools and Instruction',
                    },
                    '611710': {
                        name: 'Educational Support Services',
                    },
                },
            },
        },
    },
    '62': {
        name: 'Health Care and Social Assistance',
        subsectors: {
            '621': {
                name: 'Ambulatory Health Care Services',
                industries: {
                    '621111': {
                        name: 'Offices of Physicians (except Mental Health Specialists)',
                    },
                    '621112': {
                        name: 'Offices of Physicians, Mental Health Specialists',
                    },
                    '621210': {
                        name: 'Offices of Dentists',
                    },
                    '621310': {
                        name: 'Offices of Chiropractors',
                    },
                    '621320': {
                        name: 'Offices of Optometrists',
                    },
                    '621330': {
                        name: 'Offices of Mental Health Practitioners (except Physicians)',
                    },
                    '621340': {
                        name: 'Offices of Physical, Occupational, and Speech Therapists and Audiologists',
                    },
                    '621391': {
                        name: 'Offices of Podiatrists',
                    },
                    '621399': {
                        name: 'Offices of All Other Miscellaneous Health Practitioners',
                    },
                    '621410': {
                        name: 'Family Planning Centers',
                    },
                    '621420': {
                        name: 'Outpatient Mental Health and Substance Abuse Centers',
                    },
                    '621491': {
                        name: 'HMO Medical Centers',
                    },
                    '621492': {
                        name: 'Kidney Dialysis Centers',
                    },
                    '621493': {
                        name: 'Freestanding Ambulatory Surgical and Emergency Centers',
                    },
                    '621498': {
                        name: 'All Other Outpatient Care Centers',
                    },
                    '621511': {
                        name: 'Medical Laboratories',
                    },
                    '621512': {
                        name: 'Diagnostic Imaging Centers',
                    },
                    '621610': {
                        name: 'Home Health Care Services',
                    },
                    '621910': {
                        name: 'Ambulance Services',
                    },
                    '621991': {
                        name: 'Blood and Organ Banks',
                    },
                    '621999': {
                        name: 'All Other Miscellaneous Ambulatory Health Care Services',
                    },
                },
            },
            '622': {
                name: 'Hospitals',
                industries: {
                    '622110': {
                        name: 'General Medical and Surgical Hospitals',
                    },
                    '622210': {
                        name: 'Psychiatric and Substance Abuse Hospitals',
                    },
                    '622310': {
                        name: 'Specialty (except Psychiatric and Substance Abuse) Hospitals',
                    },
                },
            },
            '623': {
                name: 'Nursing and Residential Care Facilities',
                industries: {
                    '623110': {
                        name: 'Nursing Care Facilities (Skilled Nursing Facilities)',
                    },
                    '623210': {
                        name: 'Residential Intellectual and Developmental Disability Facilities',
                    },
                    '623220': {
                        name: 'Residential Mental Health and Substance Abuse Facilities',
                    },
                    '623311': {
                        name: 'Continuing Care Retirement Communities',
                    },
                    '623312': {
                        name: 'Assisted Living Facilities for the Elderly',
                    },
                    '623990': {
                        name: 'Other Residential Care Facilities',
                    },
                },
            },
            '624': {
                name: 'Social Assistance',
                industries: {
                    '624110': {
                        name: 'Child and Youth Services',
                    },
                    '624120': {
                        name: 'Services for the Elderly and Persons with Disabilities',
                    },
                    '624190': {
                        name: 'Other Individual and Family Services',
                    },
                    '624210': {
                        name: 'Community Food Services',
                    },
                    '624221': {
                        name: 'Temporary Shelters',
                    },
                    '624229': {
                        name: 'Other Community Housing Services',
                    },
                    '624230': {
                        name: 'Emergency and Other Relief Services',
                    },
                    '624310': {
                        name: 'Vocational Rehabilitation Services',
                    },
                    '624410': {
                        name: 'Child Day Care Services',
                    },
                },
            },
        },
    },
    '71': {
        name: 'Arts, Entertainment, and Recreation',
        subsectors: {
            '711': {
                name: 'Performing Arts, Spectator Sports and Related Industries',
                industries: {
                    '711110': {
                        name: 'Theater Companies and Dinner Theaters',
                    },
                    '711120': {
                        name: 'Dance Companies',
                    },
                    '711130': {
                        name: 'Musical Groups and Artists',
                    },
                    '711190': {
                        name: 'Other Performing Arts Companies',
                    },
                    '711211': {
                        name: 'Sports Teams and Clubs',
                    },
                    '711212': {
                        name: 'Race Tracks',
                    },
                    '711219': {
                        name: 'Other Spectator Sports',
                    },
                    '711310': {
                        name: 'Promoters of Performing Arts, Sports, and Similar Events with Facilities',
                    },
                    '711320': {
                        name: 'Promoters of Performing Arts, Sports, and Similar Events without Facilities',
                    },
                    '711410': {
                        name: 'Agents and Managers for Artists, Athletes, Entertainers, and Other Public Figures',
                    },
                    '711510': {
                        name: 'Independent Artists, Writers, and Performers',
                    },
                },
            },
            '712': {
                name: 'Museums, Historical Sites and Similar Institutions',
                industries: {
                    '712110': {
                        name: 'Museums',
                    },
                    '712120': {
                        name: 'Historical Sites',
                    },
                    '712130': {
                        name: 'Zoos and Botanical Gardens',
                    },
                    '712190': {
                        name: 'Nature Parks and Other Similar Institutions',
                    },
                },
            },
            '713': {
                name: 'Amusement, Gambling, and Recreation Industries',
                industries: {
                    '713110': {
                        name: 'Amusement and Theme Parks',
                    },
                    '713120': {
                        name: 'Amusement Arcades',
                    },
                    '713210': {
                        name: 'Casinos (except Casino Hotels)',
                    },
                    '713290': {
                        name: 'Other Gambling Industries',
                    },
                    '713910': {
                        name: 'Golf Courses and Country Clubs',
                    },
                    '713920': {
                        name: 'Skiing Facilities',
                    },
                    '713930': {
                        name: 'Marinas',
                    },
                    '713940': {
                        name: 'Fitness and Recreational Sports Centers',
                    },
                    '713950': {
                        name: 'Bowling Centers',
                    },
                    '713990': {
                        name: 'All Other Amusement and Recreation Industries',
                    },
                },
            },
        },
    },
    '72': {
        name: 'Accommodation and Food Services',
        subsectors: {
            '721': {
                name: 'Accommodation',
                industries: {
                    '721110': {
                        name: 'Hotels (except Casino Hotels) and Motels',
                    },
                    '721120': {
                        name: 'Casino Hotels',
                    },
                    '721191': {
                        name: 'Bed-and-Breakfast Inns',
                    },
                    '721199': {
                        name: 'All Other Traveler Accommodation',
                    },
                    '721211': {
                        name: 'RV (Recreational Vehicle) Parks and Campgrounds',
                    },
                    '721214': {
                        name: 'Recreational and Vacation Camps (except Campgrounds)',
                    },
                    '721310': {
                        name: 'Rooming and Boarding Houses, Dormitories, and Workers Camps',
                    },
                },
            },
            '722': {
                name: 'Food Services and Drinking Places',
                industries: {
                    '722310': {
                        name: 'Food Service Contractors',
                    },
                    '722320': {
                        name: 'Caterers',
                    },
                    '722330': {
                        name: 'Mobile Food Services',
                    },
                    '722410': {
                        name: 'Drinking Places (Alcoholic Beverages)',
                    },
                    '722511': {
                        name: 'Full-Service Restaurants',
                    },
                    '722513': {
                        name: 'Limited-Service Restaurants',
                    },
                    '722514': {
                        name: 'Cafeterias, Grill Buffets, and Buffets',
                    },
                    '722515': {
                        name: 'Snack and Nonalcoholic Beverage Bars',
                    },
                },
            },
        },
    },
    '81': {
        name: 'Other Services',
        subsectors: {
            '811': {
                name: 'Repair and Maintenance',
                industries: {
                    '811111': {
                        name: 'General Automotive Repair',
                    },
                    '811112': {
                        name: 'Automotive Exhaust System Repair',
                    },
                    '811113': {
                        name: 'Automotive Transmission Repair',
                    },
                    '811118': {
                        name: 'Other Automotive Mechanical and Electrical Repair and Maintenance',
                    },
                    '811121': {
                        name: 'Automotive Body, Paint, and Interior Repair and Maintenance',
                    },
                    '811122': {
                        name: 'Automotive Glass Replacement Shops',
                    },
                    '811191': {
                        name: 'Automotive Oil Change and Lubrication Shops',
                    },
                    '811192': {
                        name: 'Car Washes',
                    },
                    '811198': {
                        name: 'All Other Automotive Repair and Maintenance',
                    },
                    '811211': {
                        name: 'Consumer Electronics Repair and Maintenance',
                    },
                    '811212': {
                        name: 'Computer and Office Machine Repair and Maintenance',
                    },
                    '811213': {
                        name: 'Communication Equipment Repair and Maintenance',
                    },
                    '811219': {
                        name: 'Other Electronic and Precision Equipment Repair and Maintenance',
                    },
                    '811310': {
                        name: 'Commercial and Industrial Machinery and Equipment (except Automotive and Electronic) Repair and Maintenance',
                    },
                    '811411': {
                        name: 'Home and Garden Equipment Repair and Maintenance',
                    },
                    '811412': {
                        name: 'Appliance Repair and Maintenance',
                    },
                    '811420': {
                        name: 'Reupholstery and Furniture Repair',
                    },
                    '811430': {
                        name: 'Footwear and Leather Goods Repair',
                    },
                    '811490': {
                        name: 'Other Personal and Household Goods Repair and Maintenance',
                    },
                },
            },
            '812': {
                name: 'Personal and Laundry Services',
                industries: {
                    '812111': {
                        name: 'Barber Shops',
                    },
                    '812112': {
                        name: 'Beauty Salons',
                    },
                    '812113': {
                        name: 'Nail Salons',
                    },
                    '812191': {
                        name: 'Diet and Weight Reducing Centers',
                    },
                    '812199': {
                        name: 'Other Personal Care Services',
                    },
                    '812210': {
                        name: 'Funeral Homes and Funeral Services',
                    },
                    '812220': {
                        name: 'Cemeteries and Crematories',
                    },
                    '812310': {
                        name: 'Coin-Operated Laundries and Drycleaners',
                    },
                    '812320': {
                        name: 'Drycleaning and Laundry Services (except Coin-Operated)',
                    },
                    '812331': {
                        name: 'Linen Supply',
                    },
                    '812332': {
                        name: 'Industrial Launderers',
                    },
                    '812910': {
                        name: 'Pet Care (except Veterinary) Services',
                    },
                    '812921': {
                        name: 'Photofinishing Laboratories (except One-Hour)',
                    },
                    '812922': {
                        name: 'One-Hour Photofinishing',
                    },
                    '812930': {
                        name: 'Parking Lots and Garages',
                    },
                    '812990': {
                        name: 'All Other Personal Services',
                    },
                },
            },
            '813': {
                name: 'Religious, Grantmaking, Civic, Professional, and Similar Organizations',
                industries: {
                    '813110': {
                        name: 'Religious Organizations',
                    },
                    '813211': {
                        name: 'Grantmaking Foundations',
                    },
                    '813212': {
                        name: 'Voluntary Health Organizations',
                    },
                    '813219': {
                        name: 'Other Grantmaking and Giving Services',
                    },
                    '813311': {
                        name: 'Human Rights Organizations',
                    },
                    '813312': {
                        name: 'Environment, Conservation, and Wildlife Organizations',
                    },
                    '813319': {
                        name: 'Other Social Advocacy Organizations',
                    },
                    '813410': {
                        name: 'Civic and Social Organizations',
                    },
                    '813910': {
                        name: 'Business Associations',
                    },
                    '813920': {
                        name: 'Professional Organizations',
                    },
                    '813930': {
                        name: 'Labor Unions and Similar Labor Organizations',
                    },
                    '813940': {
                        name: 'Political Organizations',
                    },
                    '813990': {
                        name: 'Other Similar Organizations (except Business, Professional, Labor, and Political Organizations)',
                    },
                },
            },
        },
    },
};

export const naicsData: Readonly<Record<string, ISector>> = deepfreeze(naics);
