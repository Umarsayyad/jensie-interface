import { Component, Input, ViewChild } from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';
import { FormControl } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { debounceTime, map, startWith } from 'rxjs/operators';

import { IIndustry } from '../models/industry.interface';
import { ISector } from '../models/sector.interface';
import { NaicsIndustryService } from '../services/naics-industry.service';

@Component({
    selector: 'industry',
    templateUrl: './industry-selector.component.html',
    styleUrls: ['./industry-selector.component.less'],
})
export class IndustrySelectorComponent {
    naics: Record<string, ISector>;
    industries: Record<string, IIndustry>;
    drawerContent: Observable<Record<string, IIndustry>>;
    naicsSelectorControl = new FormControl();

    @Input() close: Function;
    @ViewChild(MatDrawer) drawer: MatDrawer;

    constructor(private naicsIndustryService: NaicsIndustryService) {
        this.naics = this.naicsIndustryService.naics();
        this.industries = this.naicsIndustryService.naicsIndustries();
    }

    isEmpty(objectItem: Object) {
        return Object.keys(objectItem).length === 0;
    }

    handleAutocomplete() {
        this.drawerContent = this.naicsSelectorControl.valueChanges.pipe(
            startWith(''),
            debounceTime(100),
            map((industry) => (industry ? this.filterStates(industry) : {}))
        );

        return this.drawer.open();
    }

    submit(naicsVal: string) {
        this.close(naicsVal);
    }

    operateDrawer(industries: Record<string, IIndustry>) {
        this.drawerContent = of(industries);
        return this.drawer.toggle();
    }

    private filterStates(value: string): Record<string, IIndustry> {
        const filterValue = value.toLowerCase();
        return this.filterObject(this.industries, (id, industry) => industry.name.toLowerCase().contains(filterValue) || id === filterValue);
    }

    private filterObject(obj: { [key: string]: any }, predicate: (key: string, obj: any) => boolean) {
        return Object.keys(obj)
            .filter((key) => predicate(key, obj[key]))
            .reduce((res, key) => Object.assign(res, { [key]: obj[key] }), {});
    }
}
