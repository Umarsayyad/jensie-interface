import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTooltipModule } from '@angular/material/tooltip';

import { IndustrySelectorComponent } from './industry/industry-selector.component';
import { NaicsIndustryService } from './services/naics-industry.service';

@NgModule({
    declarations: [IndustrySelectorComponent],
    imports: [
        CommonModule,
        FormsModule,
        MatAutocompleteModule,
        MatButtonModule,
        MatDividerModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatInputModule,
        MatSidenavModule,
        MatTooltipModule,
        ReactiveFormsModule,
    ],
})
export class CompanyProfileModule {
    static forRoot(): ModuleWithProviders<CompanyProfileModule> {
        return {
            ngModule: CompanyProfileModule,
            providers: [NaicsIndustryService],
        };
    }
}
