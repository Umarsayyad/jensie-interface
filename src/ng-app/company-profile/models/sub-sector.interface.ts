import { IIndustry } from './industry.interface';

export interface ISubSector {
    name: string;
    industries: Record<string, IIndustry>;
}
