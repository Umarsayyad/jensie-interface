import { ISubSector } from './sub-sector.interface';

export interface ISector {
    name: string;
    subsectors: Record<string, ISubSector>;
}
