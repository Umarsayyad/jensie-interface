import { Injectable } from '@angular/core';

import { naicsData } from '../data/naics';
import { IIndustry } from '../models/industry.interface';
import { ISector } from '../models/sector.interface';

@Injectable()
export class NaicsIndustryService {
    private readonly naicsHierarchy: Record<string, ISector> = naicsData;

    constructor() {}

    public naics(): Readonly<Record<string, ISector>> {
        return this.naicsHierarchy;
    }

    public naicsIndustries(): Record<string, IIndustry> {
        let industries = {};
        for (let [_, sector] of Object.entries(this.naicsHierarchy)) {
            for (let [_, subsector] of Object.entries(sector.subsectors)) {
                Object.assign(industries, subsector.industries);
            }
        }
        return industries;
    }

    public getNaicsSectorName(naicsChoice?: string, includeCode = true): string {
        if (!naicsChoice) {
            return 'None specified';
        }

        const codeParts = this.splitNaicsCode(naicsChoice);
        if (!codeParts) return `NAICS (${naicsChoice})`;

        const [sector, subsector, industry] = codeParts;

        // const sectorId = NaicsIndustryService.lookUpSector(sector);
        if (includeCode) {
            return `${this.naicsHierarchy[sector].name} (${sector})`;
        } else {
            return this.naicsHierarchy[sector].name;
        }
    }

    public getNaicsSubSectorName(naicsChoice?: string, includeCode = true): string {
        if (!naicsChoice) {
            return 'None specified';
        }

        const codeParts = this.splitNaicsCode(naicsChoice);
        if (!codeParts) return `NAICS (${naicsChoice})`;

        const [sector, subsector, industry] = codeParts;

        // const sectorId = NaicsIndustryService.lookUpSector(sector);
        const naicsSector = this.naicsHierarchy[sector];

        const naicsSubSector = naicsSector.subsectors[subsector];
        if (!naicsSubSector) {
            console.debug('NAICS subsector not found');
            return 'NAICS (' + naicsChoice + ')';
        }

        if (includeCode) {
            return `${naicsSubSector.name} (${subsector})`;
        } else {
            return naicsSubSector.name;
        }
    }

    public getNaicsName(naicsChoice?: string, includeCode = true): string {
        if (!naicsChoice) {
            return 'None specified';
        }

        const codeParts = this.splitNaicsCode(naicsChoice);
        if (!codeParts) return `NAICS (${naicsChoice})`;

        const [sector, subsector, industry] = codeParts;

        // const sectorId = NaicsIndustryService.lookUpSector(sector);
        const naicsSector = this.naicsHierarchy[sector];

        const naicsSubSector = naicsSector.subsectors[subsector];
        if (!naicsSubSector) {
            console.debug('NAICS subsector not found');
            return 'NAICS (' + naicsChoice + ')';
        }

        const naicsIndustry = naicsSubSector.industries[industry];
        if (!naicsIndustry) {
            console.debug('NAICS industry not found');
            return 'NAICS (' + naicsChoice + ')';
        }

        if (includeCode) {
            return `${naicsIndustry.name} (${naicsChoice})`;
        } else {
            return naicsIndustry.name;
        }
    }

    private splitNaicsCode(code: string): [string, string, string] | null {
        //doesn't need to match end of input (541715_a_Except is valid)
        const codeRegex = /^[1-9][0-9]{5}/;
        if (!codeRegex.test(code)) return null;

        const sector = this.lookUpSector(code.substring(0, 2));
        const subsector = code.substring(0, 3);
        const industry = code.substring(0);
        return [sector, subsector, industry];
    }

    private lookUpSector(sectorId: string): string {
        if (['31', '32', '33'].includes(sectorId)) return '31 - 33';
        else if (['44', '45'].includes(sectorId)) return '44 - 45';
        else if (['48', '49'].includes(sectorId)) return '48 - 49';
        else return sectorId;
    }
}
