import app from '../app';
import * as angular from 'angular';
import * as _ from 'lodash';

app.controller('NoteDisplayCtrl', function (
    $scope,
    $rootScope,
    $uibModal,
    $uibModalInstance,
    modalService,
    params,
    Operations,
    Appliances,
    Targets,
    MetaTargets
) {
    $scope.newnote = {};
    $scope.type = params.type;
    var resource = null;

    var ids: any = {};
    if ($scope.type == 'operation') {
        resource = Operations;
        ids.operationId = params.operationId;
    } else if ($scope.type === 'appliance') {
        resource = Appliances;
        ids.applianceId = params.applianceId;
    } else if ($scope.type === 'target') {
        resource = Targets;
        ids.targetId = params.targetId;
    } else if ($scope.type === 'metatarget') {
        resource = MetaTargets;
        ids.metaTargetId = params.metaTargetId;
    }

    $scope.notes = resource.getNotes(ids);

    $scope.deleteNote = function (note) {
        var deleteParams = angular.copy(ids);
        deleteParams.noteId = note.id;
        modalService.openConfirm('Really delete?', 'delete this note', null, null, function () {
            resource.deleteNote(
                deleteParams,
                note,
                function (note) {
                    _.remove($scope.notes, { id: note.id });
                },
                function (res) {
                    modalService.openErrorResponse('Note could not be deleted.', res, false);
                }
            );
        });
    };

    $scope.submitNote = function () {
        resource.addNote(
            ids,
            $scope.newnote,
            function (newnote) {
                $scope.notes.push(newnote);
                $scope.newnote = {};
            },
            function (res) {
                modalService.openErrorResponse('Note could not be added.', res, false);
            }
        );
    };

    $scope.editNote = function (note) {
        var modalParams = angular.copy(params);
        modalParams.note = note;

        var modalInstance = $uibModal.open({
            templateUrl: '/app/note/modalNote.html',
            controller: 'NoteCtrl',
            size: 'lg',
            scope: $scope,
            resolve: {
                params: modalParams,
            },
        });

        modalInstance.result.then(function (note) {
            _.remove($scope.notes, { id: note.id });
            $scope.notes.push(note);
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});
