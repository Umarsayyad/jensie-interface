import app from '../app';
import * as angular from 'angular';

app.controller('NoteLoaderCtrl', function ($scope, $uibModal) {
    $uibModal.open({
        templateUrl: '/app/note/modalNote.html',
        controller: 'NoteCtrl',
        size: 'lg',
        scope: $scope,
    });
});

app.controller('NoteCtrl', function ($scope, $rootScope, $uibModalInstance, modalService, params, Operations, Appliances, Targets, MetaTargets) {
    $scope.note = angular.copy(params.note);
    var type = params.type;
    var isNew = $scope.note == null;
    var resource = null;

    var ids: any = {};
    if (type == 'operation') {
        resource = Operations;
        ids.operationId = params.operationId;
    } else if (type === 'appliance') {
        resource = Appliances;
        ids.applianceId = params.applianceId;
    } else if (type === 'target') {
        resource = Targets;
        ids.targetId = params.targetId;
    } else if (type === 'metatarget') {
        resource = MetaTargets;
        ids.metaTargetId = params.metaTargetId;
    }

    $scope.submitNote = function () {
        if (isNew) {
            resource.addNote(
                ids,
                $scope.note,
                function (note) {
                    $uibModalInstance.close(note);
                },
                function (res) {
                    modalService.openErrorResponse('Note could not be added.', res, false);
                }
            );
        } else {
            ids.noteId = $scope.note.id;

            resource.editNote(
                ids,
                $scope.note,
                function (note) {
                    $uibModalInstance.close(note);
                },
                function (res) {
                    modalService.openErrorResponse('Note could not be added.', res, false);
                }
            );
        }
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});
