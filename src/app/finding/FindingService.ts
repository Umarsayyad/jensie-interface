import app from '../app';
import * as _ from 'lodash';
import * as angular from 'angular';

app.service('FindingService', function ($uibModal, modalService, Findings, Operations) {
    var SEVERITY_LEVEL = ['NONE', 'INFO', 'LOW', 'MEDIUM', 'HIGH', 'CRITICAL'];

    var SEVERITY_LEVELS_VALUE_TO_LABEL = {
        0: 'NONE',
        1: 'INFO',
        2: 'LOW',
        3: 'MEDIUM',
        4: 'HIGH',
        5: 'CRITICAL',
    };

    var SEVERITY_LEVELS_LABEL_TO_VALUE = _.invert(SEVERITY_LEVELS_VALUE_TO_LABEL);

    var STATES = ['new', 'dismissed', 'pending', 'fix', 'approved'];

    var colors = {
        INFO: '#3131f7',
        LOW: '#b2c8a2',
        MEDIUM: '#e5e38d',
        HIGH: '#e5c161',
        CRITICAL: '#e46f74',
    };

    var SEVERITY_LEVEL_MAP = [
        {
            label: 'NONE',
            value: 0,
            color: '#f5f4f4',
            class: 'label-none',
            style: 'background: repeating-linear-gradient(45deg, #fff, #fff 10px, #f5f4f4 10px, #f5f4f4 20px)',
        },
        {
            label: 'INFO',
            value: 1,
            color: '#2893f7',
            class: 'label-info',
            style: 'background: repeating-linear-gradient(45deg, #81b4e3, #81b4e3 10px, #2893f7 10px, #2893f7 20px)',
        },
        {
            label: 'LOW',
            value: 2,
            color: '#b2c8a2',
            class: 'label-low',
            style: 'background: repeating-linear-gradient(45deg, #c6dfb4, #c6dfb4 10px, #b2c8a2 10px, #b2c8a2 20px)',
        },
        {
            label: 'MEDIUM',
            value: 3,
            color: '#e5e38d',
            class: 'label-medium',
            style: 'background: repeating-linear-gradient(45deg, #fffd9d, #fffd9d 10px, #e5e38d 10px, #e5e38d 20px)',
        },
        {
            label: 'HIGH',
            value: 4,
            color: '#e5c161',
            class: 'label-high',
            style: 'background: repeating-linear-gradient(45deg, #ffd76c, #ffd76c 10px, #e5c161 10px, #e5c161 20px)',
        },
        {
            label: 'CRITICAL',
            value: 5,
            color: '#e46f74',
            class: 'label-critical',
            style: 'background: repeating-linear-gradient(45deg, #fe7c81, #fe7c81 10px, #e46f74 10px, #e46f74 20px)',
        },
    ];

    function getSeverityLevel(severity_number) {
        return SEVERITY_LEVEL[severity_number];
    }

    function getSeverityLevelFromFindings(findings) {
        var maxSeverity = 0;
        _.each(findings, function (v, k) {
            maxSeverity = v.severity > maxSeverity ? v.severity : maxSeverity;
        });
        return SEVERITY_LEVEL[maxSeverity];
    }

    function loadFindings(operationId) {
        return Findings.get({ operation: operationId });
    }

    function loadFindingsWithDetails(operationId) {
        return Findings.get({ operation: operationId, detail: true });
    }

    function getFindingById(findingId) {
        return Findings.get({ findingId: findingId });
    }

    function getFindingByIdFromMessage(message) {
        return getFindingById(message.finding);
    }

    function loadFindingMessages(findingId) {
        return Findings.messages({ findingId: findingId });
    }

    function getFindingDetails(findingParams, initialLoad) {
        var findings = {};
        var rows = [];
        return Findings.detail(findingParams).$promise.then(function (returnedFindings) {
            var f;
            if ('results' in returnedFindings) {
                f = returnedFindings.results;
            } else {
                f = [returnedFindings];
            }
            _.each(f, function (finding) {
                // Replace if already exists
                var idx = _.findIndex(findings[finding.customer], { id: finding.id });
                if (idx > -1) {
                    findings[finding.customer][idx] = finding;
                    // Figure out how to update the rows
                } else {
                    if (_.isNil(findings[finding.customer])) {
                        findings[finding.customer] = [];
                    }
                    findings[finding.customer].unshift(finding);
                    rows.push({
                        c: [
                            { v: new Date(Date.parse(finding.added)) },
                            { v: finding.severity },
                            //{v: Math.random()*1000 },
                            { v: finding.title },
                            { v: finding.description },
                            { v: finding.id },
                        ],
                    });
                }
            });
            angular.forEach(findings, function (customerFindings: object, customerId) {
                findings[customerId] = _.reverse(
                    _.sortBy(customerFindings, [
                        function (finding: any) {
                            return finding.updated;
                        },
                    ])
                );
            });
            if (initialLoad) {
                _.reverse(
                    (rows = _.sortBy(rows, [
                        function (o) {
                            return o.c[0];
                        },
                    ]))
                );
            }
            //console.log('findings: ', findings);
            return { findings: findings, rows: rows, count: returnedFindings.count };
        });
    }

    function archiveFinding(finding) {
        console.log('Update finding to set flag to archive/viewed');
        return Findings.setArchived({ findingId: finding.id }, {});
    }

    function unarchiveFinding(finding) {
        console.log('Update finding to set flag to unarchive/unread');
        return Findings.setUnArchived({ findingId: finding.id }, {});
    }

    function retestFinding(finding) {
        var modalInstance = $uibModal.open({
            templateUrl: '/app/finding/modalRequestRetest.html',
            controller: function ($scope) {
                $scope.requestRetest = function () {
                    Findings.retest(
                        { findingId: finding.id },
                        { reason: $scope.reason },
                        function (res) {
                            console.log('retest request result: ', res);
                            modalService.openSuccess('Retest Request Submitted', res.message, false);
                            modalInstance.close(res);
                        },
                        modalService.openErrorResponseCB('Could not request retest of finding')
                    );
                };
            },
            size: 'lg',
        });
        return modalInstance;
    }

    function findingsContainNew(findings) {
        return _.filter(findings, { state: 'new' }).length > 0;
    }

    function findingStateFiltersContain(findingStateFilters, state) {
        return findingStateFilters.indexOf(state) > -1;
    }

    function findingSeverityFiltersContain(findingSeverityFilters, severity) {
        return findingSeverityFilters.indexOf(severity) > -1;
    }

    function addRemoveFindingFilter(findingFilter, filter) {
        var idx = findingFilter.indexOf(filter);
        if (idx > -1) {
            _.pullAt(findingFilter, idx);
        } else {
            findingFilter.push(filter);
        }
        return findingFilter;
    }

    function doDeleteFinding(finding, selectedFindings, data, callback?: Function) {
        data.message = 'Deleting Finidngs';
        data.findingsLoading = Findings.delete(
            { findingId: finding.id },
            function () {}, // No success callback needed, websocket update handles everything
            modalService.openErrorResponseCB('Unable to delete finding ' + finding.title)
        );

        data.findingsLoading.$promise.then(function () {
            if (selectedFindings && selectedFindings.length > 0) {
                doDeleteFinding(selectedFindings.pop(), selectedFindings, data, callback);
            } else if (callback) {
                callback();
            }
        });
    }

    function deleteFinding(finding) {
        return modalService.openDelete('Delete Finding', 'the finding: ' + finding.title, finding, function () {
            doDeleteFinding(finding, null, {});
        });
    }

    function approveFinding(id) {
        return Findings.approve({ findingId: id }, {});
    }

    function bulkRequestFollowup($event, selectedFindings, data) {
        if ($event.currentTarget.hasAttribute('disabled')) {
            return;
        }

        var finding_ids = _.map(selectedFindings, 'id');

        data.message = 'Requesting followups.';
        data.findingsLoading = Findings.bulk_requestfollowup(
            { findingId: 0 },
            { ids: finding_ids },
            function () {
                _.each(selectedFindings, function (finding) {
                    finding.followup_requested = true;
                });
            },
            modalService.openErrorResponseCB('Request followup of some findings failed')
        );

        return;
    }

    function bulkUnrequestFollowup($event, selectedFindings, data) {
        if ($event.currentTarget.hasAttribute('disabled')) {
            return;
        }

        var finding_ids = _.map(selectedFindings, 'id');

        data.message = 'Unrequesting followups.';
        data.findingsLoading = Findings.bulk_requestfollowup(
            { findingId: 0 },
            {
                ids: finding_ids,
                unrequest: true,
            },
            function () {
                _.each(selectedFindings, function (finding) {
                    finding.followup_requested = false;
                });
            },
            modalService.openErrorResponseCB('Unrequest followup of some findings failed')
        );

        return;
    }

    function bulkApproveFindings($event, selectedFindings, data) {
        if ($event.currentTarget.hasAttribute('disabled')) {
            return;
        }

        var finding_ids = _.map(selectedFindings, 'id');

        data.message = 'Approving Findings';
        data.findingsLoading = Findings.bulk_approve(
            { findingId: 0 },
            { ids: finding_ids },
            angular.noop,
            modalService.openErrorResponseCB('Bulk approval of some findings failed')
        );

        return;
    }

    function bulkEditFindings($event, selectedFindings, $scope) {
        if ($event.currentTarget.hasAttribute('disabled')) {
            return;
        }
        if (_.isUndefined($scope)) {
            $scope = this;
        }

        return $uibModal.open({
            templateUrl: '/app/finding/modalBulkEditFinding.html',
            controller: 'EditCtrl',
            size: 'lg',
            scope: $scope,
            resolve: {
                params: {
                    selectedFindings: selectedFindings,
                },
            },
        });
    }

    function bulkDeleteFindings($event, selectedFindings, data, callback) {
        if ($event.currentTarget.hasAttribute('disabled')) {
            return;
        }

        return modalService.openDelete('Bulk Delete Findings', selectedFindings.length + ' findings', null, function () {
            doDeleteFinding(selectedFindings.pop(), selectedFindings, data, callback);
        });
    }

    function bulkRejectFindings($event, selectedFindings, deselect, $scope) {
        if (selectedFindings.length === 0 || $event.currentTarget.hasAttribute('disabled')) {
            return;
        }
        if (_.isUndefined($scope)) {
            $scope = this;
        }
        var modal = $uibModal.open({
            templateUrl: '/app/finding/modalRejectFinding.html',
            controller: 'RejectionCtrl',
            size: 'lg',
            scope: $scope,
            resolve: {
                params: {
                    selObjsData: selectedFindings,
                    type: 'finding',
                    action: 'reject',
                },
            },
        });
        if (deselect) {
            modal.closed.then(function () {
                _.each(selectedFindings, function (finding) {
                    finding.selected = false;
                });
            });
        }
        return modal;
    }

    return {
        SEVERITY_LEVEL: SEVERITY_LEVEL,
        SEVERITY_LEVEL_MAP: SEVERITY_LEVEL_MAP,
        SEVERITY_LEVELS_VALUE_TO_LABEL: SEVERITY_LEVELS_VALUE_TO_LABEL,
        SEVERITY_LEVELS_LABEL_TO_VALUE: SEVERITY_LEVELS_LABEL_TO_VALUE,
        getSeverityLevel: getSeverityLevel,
        getSeverityLevelFromFindings: getSeverityLevelFromFindings,
        loadFindings: loadFindings,
        getFindingById: getFindingById,
        loadFindingMessages: loadFindingMessages,
        archiveFinding: archiveFinding,
        unarchiveFinding: unarchiveFinding,
        retestFinding: retestFinding,
        findingsContainNew: findingsContainNew,
        findingStateFiltersContain: findingStateFiltersContain,
        findingSeverityFiltersContain: findingSeverityFiltersContain,
        addRemoveFindingFilter: addRemoveFindingFilter,
        doDeleteFinding: doDeleteFinding,
        deleteFinding: deleteFinding,
        approveFinding: approveFinding,
        bulkApproveFindings: bulkApproveFindings,
        bulkEditFindings: bulkEditFindings,
        bulkDeleteFindings: bulkDeleteFindings,
        bulkRejectFindings: bulkRejectFindings,
        bulkRequestFollowup: bulkRequestFollowup,
        bulkUnrequestFollowup: bulkUnrequestFollowup,
    };
});
