import app from '../app';
import { config } from '@cato/config';
import * as _ from 'lodash';
import * as angular from 'angular';

import './FindingService';
import './reference/referencesEditor';

app.controller('addFindingLoaderCtrl', function ($scope, $uibModal) {
    $uibModal.open({
        templateUrl: '/app/finding/addFindingModal.html',
        controller: 'AddFindingCtrl',
        size: 'lg',
        scope: $scope,
    });
});

app.controller('AddFindingCtrl', function (
    $scope,
    $state,
    $stateParams,
    $uibModalInstance,
    $q,
    modalService,
    Attachments,
    Findings,
    Operations,
    Recommendationtemplates,
    Recommendations,
    Targets,
    Upload,
    FindingService
) {
    $scope.modalTitle = 'ADD A FINDING';

    Findings.options().$promise.then(function (options) {
        $scope.config = options.actions.POST;
        $scope.categories = options.actions.POST.category.choices;
        if ($scope.finding && !$scope.finding.categoryLabel) {
            $scope.finding.categoryLabel = $scope.categories[0].display_name;
        }
    });

    $scope.priorities = FindingService.SEVERITY_LEVEL_MAP;
    $scope.recommendations = Recommendationtemplates.list();

    var parentFinding: any = {};
    var findingId = parseInt($state.params.finding_id);
    var submissionDefer;

    function initializeRecommendations(finding) {
        if (!_.isNil(finding.recommendation)) {
            console.log(finding.recommendation);
            $scope.finding.recommendation.body_values = finding.recommendation.body_values.length > 0 ? finding.recommendation.body_values : '';
        } else {
            $scope.finding.recommendation = {};
            $scope.finding.recommendation.body_values = '';
        }
    }

    function setupFinding(finding?: any) {
        if (finding) {
            $scope.finding = finding;
        }
        parentFinding = $scope.finding;
        $scope.finding = angular.copy($scope.finding);
        if ($scope.finding.target && $scope.finding.target.id) {
            $scope.finding.target = $scope.finding.target.id;
        }
        setupOperationPrep();
        $scope.finding.severityLabel = $scope.priorities[$scope.finding.severity];
        $scope.finding.categoryLabel = $scope.finding.category;
        $scope.finding.approve = $scope.finding.state === 'approved';

        if ($scope.finding.attachment_details) {
            $scope.finding.attachments = $scope.finding.attachment_details; // TODO: remove this once TODO page requests are cleaned up and attachment_details is no longer used
        }
        initializeRecommendations($scope.finding);
    }

    function setupOperationPrep() {
        if ($scope.operationArr) {
            $scope.operationArr.then(setupOperation);
        } else if ($scope.finding.operation) {
            if (_.isNumber($scope.finding.operation)) {
                Operations.get({ operationId: $scope.finding.operation }, setupOperation);
            } else {
                // $scope.finding.operation is truthy and not a number, assume it is an operation object
                setupOperation($scope.finding.operation);
            }
        } else if ($state.params.operation_id) {
            Operations.get({ operationId: $state.params.operation_id }, setupOperation);
        } else {
            console.warn(
                'Creating a new finding but not on operation detail page to get parent operation, even from the URL; cannot check if a operation ran against a metatarget to display the target selector'
            );
        }
    }

    function setupOperation(operation) {
        if (operation.metatarget) {
            var mtId;
            if (_.isNumber(operation.metatarget)) {
                mtId = operation.metatarget;
            } else if (!_.isArray(operation.metatarget.targets) || _.isNumber(operation.metatarget.targets[0])) {
                mtId = operation.metatarget.id;
            } else {
                $scope.metatargetTargets = operation.metatarget.targets;
            }
            if (mtId) {
                Targets.basicList({ metatargets: mtId }, function (targets) {
                    $scope.metatargetTargets = targets;
                });
            }
        }
    }

    if (!_.isNaN(findingId)) {
        if ($scope.finding && $scope.finding.id === findingId) {
            // Prototypical scope inheritance on findings tab
            setupFinding();
        } else {
            // To-Do tab (at least when already loaded)
            $scope.finding = _.find($scope.data.findingsToProcess, { id: findingId });
            if ($scope.finding) {
                setupFinding();
            } else {
                Findings.detail({ findingId: findingId }, setupFinding, function (err) {
                    modalService.openErrorResponse('Error loading finding', err, false);
                    $uibModalInstance.dismiss(err);
                });
            }
        }
    } else {
        $scope.finding = {
            severityLabel: $scope.priorities[0],
            approve: false,
        };
        setupOperationPrep();
    }

    if ($state.params.finding_id) {
        $scope.modalTitle = 'EDIT FINDING';
        $scope.action = 'edit';
    }

    $scope.deleteFinding = function (finding) {
        modalService.openDelete('Delete Finding', 'the finding: ' + finding.title, finding, function (finding) {
            Findings.delete({ findingId: finding.id }, {}, function () {
                $uibModalInstance.dismiss();
            });
        });
    };

    function updateExistingFinding(finding, recommendation?: any) {
        if (_.isNil(recommendation)) {
            /* TODO: Handle removing a recommendation.
            delete finding.recommendation;
            delete $scope.finding.recommendation;
            delete parentFinding.recommendation;
            */
        } else {
            finding.recommendation = recommendation.id;
        }

        if (finding.task === null) {
            delete finding.task;
        }
        if (finding.approve && $scope.gdata.isMissionDirectorOrAdmin) {
            finding.state = 'approved';
        }
        var findingUpdate = $scope.copyForRequest(finding, $scope.config, undefined, ['data']);

        Findings.update(
            { findingId: finding.id },
            findingUpdate,
            function (updatedFinding) {
                // We can't pass detail to an update, so we need to refetch to get an accurate detailed object.
                uploadAttachments(updatedFinding.id).then(function () {
                    Findings.query({ findingId: updatedFinding.id }, function (updatedFinding: any) {
                        updatedFinding.recommendation = recommendation;

                        $scope.updateObj($scope.finding, updatedFinding);
                        $scope.updateObj(parentFinding, updatedFinding);

                        parentFinding.target = updatedFinding.target;
                        parentFinding.attachments = $scope.finding.attachments;
                        if (parentFinding.attachment_details) {
                            parentFinding.attachment_details = $scope.finding.attachment_details;
                        }
                        if (finding.state === 'approved') {
                            Findings.approve({ findingId: finding.id }, {});
                        } else if (finding.state === 'new' || updatedFinding.state === 'new') {
                            $scope.data.findingsContainNew = true;
                        }

                        submissionDefer.resolve();
                        $uibModalInstance.close(updatedFinding);
                    });
                });
            },
            function (res) {
                submissionDefer.reject();
                modalService.openErrorResponse('Finding could not be updated.', res, false);
            }
        );
    }

    function addRecommendationToFinding(finding, recommendation_template?: any, additional_recommendation?: any) {
        if (recommendation_template || _.get(finding, 'recommendation.recommendation_template')) {
            if (!recommendation_template) {
                recommendation_template = finding.recommendation.recommendation_template;
                additional_recommendation = finding.recommendation.body_values;
            }
            var recommendation = {
                recommendation_template: recommendation_template.id,
                finding: finding.id,
                created_by: $scope.user.id,
                body_values: additional_recommendation,
            };

            if (finding.recommendation && finding.recommendation.id !== undefined) {
                Recommendations.update(
                    { recommendationId: finding.recommendation.id },
                    recommendation,
                    function (rec) {
                        rec.recommendation_template = recommendation_template;
                        updateExistingFinding(finding, rec);
                    },
                    modalService.openErrorResponseCB('Error updating recommendation', false, submissionDefer.reject)
                );
            } else {
                Recommendations.create(
                    recommendation,
                    function (rec) {
                        rec.recommendation_template = recommendation_template;
                        updateExistingFinding(finding, rec);
                    },
                    modalService.openErrorResponseCB('Error creating recommendation', false, submissionDefer.reject)
                );
            }
        } else {
            delete finding.recommendation;
            updateExistingFinding(finding);
        }
    }

    function updateFindingPrep() {
        var finding = _.clone($scope.finding);
        delete finding.attachments;

        for (var key in finding) {
            if (finding.hasOwnProperty(key)) {
                //Now, finding[key] is the current value
                if (finding[key] === null || finding[key] === '') delete finding[key];
            }
        }

        finding.severity = finding.severityLabel.value;
        finding.category = finding.categoryLabel;
        finding.operation = $stateParams.operation_id || $scope.finding.operation.id;

        /* updating a task shouldn't change its state */
        /* Also, if we're an MD/Admin - don't reset the state to 'pending' */
        if (!$scope.gdata.isMissionDirectorOrAdmin && finding.state === 'approved') {
            finding.state = 'pending';
        }
        if (_.isObject(finding.task)) {
            if (!_.isUndefined(finding.task.id)) {
                finding.task = finding.task.id;
            } else {
                console.error('finding.task is an object but has no id, this should never happen');
            }
        }

        submissionDefer = $q.defer();
        $scope.submission = submissionDefer.promise;
        addRecommendationToFinding(finding);
    }

    function addFinding() {
        // TODO: simplify all of this intermixed logic for adding and updating findings
        var finding = _.clone($scope.finding);
        delete finding.attachments;

        finding.severity = finding.severityLabel.value;
        finding.category = finding.categoryLabel;
        finding.operation = $stateParams.operation_id;
        finding.campaign = $scope.data.operation.campaign.id;
        // Don't want to send this along with the finding when created
        var recommendation_template = undefined;
        var additional_recommendation;
        if (finding.recommendation !== undefined) {
            recommendation_template = finding.recommendation.recommendation_template;
            additional_recommendation = finding.recommendation.body_values;
            delete finding.recommendation;
        }
        finding.references = _.map(finding.references, 'id');

        submissionDefer = $q.defer();
        $scope.submission = submissionDefer.promise;

        Findings.save(
            finding,
            function (newFinding) {
                $scope.data.findingsContainNew = true;
                uploadAttachments(newFinding.id).then(function () {
                    finding.id = newFinding.id;
                    addRecommendationToFinding(finding, recommendation_template, additional_recommendation);

                    if (finding.approve) {
                        Findings.approve({ findingId: newFinding.id }, {});
                    }
                });
            },
            function (res) {
                //console.log(res);
                submissionDefer.reject();
                modalService.openErrorResponse('Finding could not be saved.', res, false);
            }
        );
    }

    function uploadAttachments(findingId) {
        if (_.isUndefined($scope.finding.attachment_details)) {
            $scope.finding.attachment_details = [];
        }
        //console.log('$scope.finding.attachments: ', $scope.finding.attachments);
        var uploading = $q.defer();
        var uploaded = 0;
        var needToUpload = _.size($scope.finding.attachments);

        _.each($scope.finding.attachments, function (att, idx) {
            if (typeof att.id === 'undefined') {
                var campaign = $scope.finding.campaign || _.get($scope.finding.operation, 'campaign') || _.get($scope.data, 'operation.campaign');
                if (!campaign) {
                    console.error(
                        "Could not determine finding's campaign from $scope.finding.campaign, $scope.finding.operation.campaign, or $scope.data.operation.campaign; update this logic to retrieve the campaign from somewhere so the finding's attachments can be uploaded"
                    );
                    console.error('$scope.finding:', JSON.stringify($scope.finding));
                    console.error('$scope.data:', JSON.stringify($scope.data));
                    modalService.openError(
                        'An error occurred while attempting to upload attachments',
                        'Please report the errors in the browser console so we can address the issue.',
                        false
                    );
                    uploading.resolve('ERROR');
                    return false;
                }
                if (campaign.id) {
                    campaign = campaign.id;
                }
                Upload.upload({
                    url: config.apiPath + 'attachment',
                    data: {
                        file: att,
                        name: att.name,
                        type: 'finding',
                        finding: findingId,
                        campaign: campaign,
                    },
                }).then(
                    function (resp) {
                        console.log('Success ' + resp.config.data.file.name + ' uploaded. Response: ', resp);
                        $scope.finding.attachments[idx] = resp.data;
                        $scope.finding.attachment_details.push(resp.data);
                        uploaded++;
                        if (uploaded === needToUpload) {
                            uploading.resolve($scope.finding.attachments);
                        }
                    },
                    function (err) {
                        uploaded++;
                        if (uploaded === needToUpload) {
                            uploading.resolve($scope.finding.attachments);
                        }
                        modalService.openErrorResponse('Error uploading attachment', err, false);
                    },
                    function (evt) {
                        if (evt !== undefined) {
                            var progressPercentage = Math.floor((100.0 * evt.loaded) / evt.total);
                            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                        }
                    }
                );
            } else {
                needToUpload--;
            }
        });
        if (needToUpload === 0) {
            uploading.resolve($scope.finding.attachments);
        }
        return uploading.promise;
    }

    $scope.addOrUpdateFinding = function () {
        if ($state.params.finding_id) {
            if (!$scope.gdata.isMissionDirectorOrAdmin && $scope.finding.state === 'approved') {
                modalService.openConfirm(
                    'Really edit?',
                    'edit this finding?',
                    'Editing an approved finding will reset it to pending approval. Continue',
                    null,
                    function () {
                        updateFindingPrep();
                    }
                );
            } else {
                updateFindingPrep();
            }
        } else {
            addFinding();
        }
    };

    $scope.removeFile = function (file) {
        if (typeof file.id !== 'undefined') {
            Attachments.delete(
                { id: file.id },
                function () {
                    console.log('file: ', file);
                    _.remove($scope.finding.attachments, { id: file.id });
                },
                function (res) {
                    console.log('could not delete file: ' + res);
                    //console.log(res);
                }
            );
        } else {
            _.pull($scope.finding.attachments, file);
        }
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };

    $uibModalInstance.result.then(
        function () {
            $state.go('^');
        },
        function () {
            $state.go('^');
        }
    );
});
