import app from '../app';
import * as _ from 'lodash';
import * as angular from 'angular';

app.controller('EditCtrl', function (
    $scope,
    $state,
    $stateParams,
    Findings,
    Recommendations,
    Recommendationtemplates,
    FindingService,
    modalService,
    params,
    $q,
    $uibModal,
    $uibModalInstance
) {
    var findings = params['selectedFindings'];
    var requestsRemaining = 0;

    $scope.finding = {};
    $scope.enableSeverity = false;
    $scope.enableCategory = false;
    $scope.enableCvssbase = false;
    $scope.enableCvsstemporal = false;
    $scope.enableCvssenv = false;
    $scope.enableRecommendedRemediation = false;

    $scope.errorDisplayed = false;

    $scope.config = Findings.options().$promise.then(function (options) {
        $scope.config = options.actions.POST;
        $scope.categories = options.actions.POST.category.choices;
    });
    $scope.priorities = FindingService.SEVERITY_LEVEL_MAP;

    $scope.recommendations = Recommendationtemplates.list();

    $scope.save = function () {
        $scope.errorDisplayed = false;
        $scope.recommendationErrorDisplayed = false;
        requestsRemaining = 0;

        var newValues = {};

        // TODO: update this to use Angular's ng-model instead of jQuery
        if ($scope.enableSeverity) {
            newValues['severity'] = $('#finding_severity').val();
        }
        if ($scope.enableCategory) {
            newValues['category'] = $('#finding_category option:Selected').text();
        }
        if ($scope.enableCvssbase) {
            newValues['cvss_base'] = $('#cvss_base').val();
        }
        if ($scope.enableCvsstemporal) {
            newValues['cvss_temporal'] = $('#cvss_temporal').val();
        }
        if ($scope.enableCvssenv) {
            newValues['cvss_env'] = $('#cvss_env').val();
        }

        var containsApproved = false;

        for (var i = 0; i < findings.length; i++) {
            if (findings[i].state === 'approved') {
                containsApproved = true;
                break;
            }
        }

        // The findings list in the operations page prevents multi-select of approved findings for operators, but let's still make sure we check.
        if (!$scope.gdata.isMissionDirectorOrAdmin && containsApproved) {
            modalService.openConfirm(
                'Really edit?',
                'edit these findings',
                'Editing a approved findings will reset them to pending approval. Continue',
                null,
                function () {
                    iterateAndUpdate(newValues, $scope.finding.approve);
                }
            );
        } else {
            iterateAndUpdate(newValues, $scope.finding.approve);
        }
    };

    function iterateAndUpdate(newValues, approve) {
        angular.forEach(findings, function (finding) {
            requestsRemaining++;
            if ($scope.enableRecommendedRemediation) {
                var recommendation = {
                    body_values: $('#finding_recommendation').val(),
                    recommendation_template: $('#finding_recommendation_choice').val(),
                    finding: finding.id,
                };

                Recommendations.create(recommendation).$promise.then(
                    function (rec) {
                        finding.recommendation = rec;
                        updateFinding(finding, newValues, approve);
                    },
                    function (res) {
                        // As all changes will be the same, so too will all the errors. No need for {n} errors to show up.
                        if (!$scope.recommendationErrorDisplayed) {
                            modalService.openErrorResponse('Finding recommendation could not be updated.', res, false);
                        }
                        $scope.recommendationErrorDisplayed = true;
                    }
                );
            } else {
                if (finding.recommendation == null) {
                    delete finding['recommendation'];
                }
                updateFinding(finding, newValues, approve);
            }
        });
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };

    function updateFinding(finding, newValues, approve) {
        var updatedFinding = $scope.copyForRequest(finding, $scope.config);

        if (updatedFinding.state == 'approved' && !$scope.gdata.isMissionDirectorOrAdmin) {
            updatedFinding.state = 'pending';
        }

        if (approve) {
            updatedFinding.state = 'approved';
        }

        angular.extend(updatedFinding, newValues);

        // Fetching a finding will include a {'task': null} entry. While a task can be not set, you can't explicitly set to null.
        if (updatedFinding.task === null) {
            delete updatedFinding.task;
        }

        $scope.submission = $q.defer();
        $scope.submission.message = 'Updating Findings';

        Findings.update(
            { findingId: updatedFinding.id },
            updatedFinding,
            function (returnedFinding) {
                // We can't pass detail to an update, so we need to refetch to get an accurate detailed object.
                Findings.query({ findingId: returnedFinding.id }, function (detailedFinding) {
                    $scope.updateObjOld(finding, detailedFinding);
                    if (approve) {
                        Findings.approve({ findingId: finding.id }, {});
                    } else if (updatedFinding.state === 'new') {
                        $scope.data.findingsContainNew = true;
                    }
                    requestsRemaining--;
                    if (requestsRemaining === 0) {
                        $uibModalInstance.close();
                        $scope.submission.resolve(true);
                    }
                });
            },
            function (res) {
                // As all changes will be the same, so too will all the errors. No need for {n} errors to show up.
                if (!$scope.errorDisplayed) {
                    modalService.openErrorResponse('Finding could not be updated.', res, false);
                }
                $scope.errorDisplayed = true;
                $scope.submission.resolve(true);
            }
        );
    }
});
