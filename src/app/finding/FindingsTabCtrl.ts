import app from '../app';
import * as _ from 'lodash';
import catotheme from '@cato/config/themes/catotheme';

import { FindingSearchService } from './search/finding-search.service';
import { FindingSearchNode } from './search/nodes';

import '../common/actionsDropdownComp';
import '../customer/manage/CondensedCustomerFilterComp';
import './reference/referencesEditor';
import './reference/referencesViewer';
import './search/finding-search.component';
import { MatDialog } from '@angular/material/dialog';
import { AttachmentComponent } from '../../ng-app/common/components/attachment-upload/attachment.component';
import { AttachmentService } from '../../ng-app/common/services/attachment.service';

app.controller('FindingsTabCtrl', function (
    $anchorScroll,
    $rootScope,
    $scope,
    $state,
    $transitions,
    $timeout,
    MatDialog: MatDialog,
    AttachmentService: AttachmentService,
    FileSaver,
    Findings,
    FindingCsv,
    FindingService,
    FindingSearch: FindingSearchService,
    OperationTemplates,
    Messages,
    MessageService,
    modalService,
    ngTableService,
    PreferencesService,
    socketService,
    Targets
) {
    // TODO: refactor this controller into a component so we don't have to have detection logic for each state that uses it

    $scope.operationDetailPage = $state.$current.includes['cato.loggedIn.operator'];
    $scope.findingsToDoTab = $state.$current.includes['cato.loggedIn.dashboard.todo'];
    $scope.graphFilter = undefined;

    if ($scope.operationDetailPage) {
        $scope.findingDetailState = 'cato.loggedIn.operator.findingDetail';
        $scope.editFindingState = 'cato.loggedIn.operator.findingDetail.editFinding';
        $scope.followUpOperationState = 'cato.loggedIn.operator.findingDetail.followUpOperation';
    } else {
        // Findings tab
        $scope.findingDetailState = 'cato.loggedIn.dashboard.findings';
        $scope.editFindingState = 'cato.loggedIn.dashboard.findings.editFinding';
        $scope.followUpOperationState = 'cato.loggedIn.dashboard.findings.followUpOperation';
    }

    if ($scope.findingsToDoTab) {
        $scope.$parent.findingsReviewScope = this;
        $scope.findingsTab = _.find($scope.$parent.tabs, { stateName: 'findingsToReview' });
    }

    if ($scope.gdata.isOperator) {
        $scope.findingLinkState = 'cato.loggedIn.operator.findingDetail';
    } else {
        $scope.findingLinkState = 'cato.loggedIn.service.campaign.operation.findings';
    }

    $scope.messageFormData = {};
    $scope.msgIsMissionDirector = MessageService.msgIsMissionDirector;
    $scope.msgIsOperator = MessageService.msgIsOperator;
    $scope.isMe = MessageService.isMe;
    $scope.getInitials = MessageService.getInitials;
    $scope.getNameOrRole = MessageService.getNameOrRole;
    $scope.data.selectedFindings = [];
    $scope.data.findingMessages = {};
    $scope.data.allFindingsSelected = false;
    $scope.data.showArchived = false;
    $scope.numPages = 0;

    $scope.findingsLoaded = false;
    $scope.form = {
        findingsQuery: $state.params.search || '',
        appliance: parseInt($state.params.appliance) || undefined,
        target: parseInt($state.params.target) || undefined,
        opTemplate: parseInt($state.params.operation_template) || undefined,
        followup_requested: $state.params.followup_requested || 'either',
        references: [],
    };
    $scope.data.targets = {};
    $scope.customer = $scope.gdata.customerMap[$state.params.customer] || _.get($rootScope.user, 'ui_data.selectedCustomer') || $scope.gdata.customer;
    if (!$scope.customer) {
        delete $scope.customer;
    }

    $scope.itemsPerPage = $state.params.page_size || 30;
    $scope.currentPage = $state.params.page || 1;

    $scope.FINDING_STATE_FILTERS = $rootScope.gdata.findingStateFilters;
    $scope.findingStateFilters = _.isEmpty($state.params.state) ? _.clone($scope.FINDING_STATE_FILTERS) : _.clone($state.params.state);

    $scope.SEVERITY_LEVELS = _.clone(FindingService.SEVERITY_LEVEL);
    if ($scope.gdata.isCustomer) {
        // Don't show customer findings with severity of 'NONE'
        _.pull($scope.SEVERITY_LEVELS, 'NONE');
    }
    $scope.SEVERITY_LEVELS_REVERSE = _.clone($scope.SEVERITY_LEVELS).reverse();
    $scope.findingSeverityFilters = _.isEmpty($state.params.severity) ? _.clone($scope.SEVERITY_LEVELS) : _.clone($state.params.severity);

    $scope.retestFinding = FindingService.retestFinding;
    $scope.archiveFinding = FindingService.archiveFinding;
    $scope.unarchiveFinding = FindingService.unarchiveFinding;
    $scope.approveFinding = FindingService.approveFinding;
    $scope.deleteFinding = FindingService.deleteFinding;

    $scope.isAliasShown = false;
    $scope.targetViewPreference = PreferencesService.getTargetDisplayPrefSubscription().subscribe((preference) => {
        $scope.isAliasShown = preference;
    });

    $scope.viewApproveReject = function () {
        if ($scope.gdata.isMissionDirector) {
            return true;
        }
        return false;
    };

    $scope.downloadCsv = function () {
        var timestamp = new Date().toISOString();
        var nickname = $rootScope.gdata.customerMap[$scope.customer.id].nickname;
        var params = getParameters();
        FindingCsv.download(
            params,
            function (data) {
                FileSaver.saveAs(data.blob, `${catotheme.appName.toLowerCase()}_findings_${nickname}_${timestamp}.csv`);
            },
            modalService.openErrorResponseCB('Unable to download CSV')
        );
    };

    $scope.bulkActions = {
        bulkEditFindings: {
            action: function (target, event) {
                FindingService.bulkEditFindings(event, $scope.data.selectedFindings, $scope);
            },
            class: 'btn-white',
            icon: 'fa-pencil',
            label: 'Edit',
            tooltip: 'Perform bulk edits',
        },
        bulkApproveFindings: {
            action: function (target, event) {
                FindingService.bulkApproveFindings(event, $scope.data.selectedFindings, $scope.data);
            },
            class: 'btn-success',
            icon: 'fa-thumbs-up',
            label: 'Approve',
            show: $scope.viewApproveReject,
            tooltip: 'Perform bulk approvals',
        },
        bulkRejectFindings: {
            action: function (target, event) {
                FindingService.bulkRejectFindings(event, $scope.data.selectedFindings, true, $scope);
            },
            class: 'btn-danger',
            icon: 'fa-thumbs-down',
            label: 'Reject',
            show: $scope.viewApproveReject,
            tooltip: 'Perform bulk rejections',
        },
        bulkDeleteFindings: {
            action: function (target, event) {
                FindingService.bulkDeleteFindings(event, $scope.data.selectedFindings, $scope.data);
            },
            class: 'btn-black',
            icon: 'fa-times',
            label: 'Delete',
            tooltip: 'Perform bulk deletion',
        },
        bulkRequestFollowup: {
            action: function (target, event) {
                FindingService.bulkRequestFollowup(event, $scope.data.selectedFindings, $scope.data);
            },
            class: 'btn-info',
            icon: 'fa-calendar-check-o',
            label: 'Request Follow-up',
            tooltip: 'Request a follow-up',
        },
        bulkUnrequestFollowup: {
            action: function (target, event) {
                FindingService.bulkUnrequestFollowup(event, $scope.data.selectedFindings, $scope.data);
            },
            class: 'btn-info',
            icon: 'fa-calendar-times-o',
            label: 'Unrequest Follow-up',
            tooltip: 'Unrequest a follow-up',
        },
    };

    $scope.starting_popup = {
        opened: false,
    };

    $scope.ending_popup = {
        opened: false,
    };

    $scope.beforeDateOptions = {};
    $scope.afterDateOptions = {};
    $scope.form.added_after = '';
    $scope.pickStartingDate = function () {
        $scope.starting_popup.opened = true;
    };
    $scope.form.added_before = '';
    $scope.pickEndingDate = function () {
        $scope.ending_popup.opened = true;
    };

    $scope.dateChanged = function () {
        // Set min/max dates.
        if ($scope.form.added_after) {
            $scope.beforeDateOptions.minDate = $scope.form.added_before;
        }
        if ($scope.form.added_before) {
            $scope.afterDateOptions.maxDate = $scope.form.added_after;
        }

        $scope.loadFindings();
    };

    $scope.getSeverityLevel = FindingService.getSeverityLevel;
    $scope.getSeverityLevelFromFindings = FindingService.getSeverityLevelFromFindings;
    $scope.findingStateFiltersContain = FindingService.findingStateFiltersContain;
    $scope.findingSeverityFiltersContain = FindingService.findingSeverityFiltersContain;

    if (!$scope.operationDetailPage) {
        $scope.$watch('user.ui_data.selectedCustomer', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                $scope.customer = newVal;
                $scope.form.appliance = null;
                $scope.form.target = null;
                $scope.currentPage = 1;
                $scope.loadFindings();
            }
        });
    }

    if ($scope.gdata.isOperator) {
        $scope.operationTemplates = OperationTemplates.list({ basic: true }); // TODO: if/when operations field is removed from base OperationTemplateSerializer, {basic:true} can be removed

        $scope.$watch('data.selectedFindings.length', function (newValue, oldValue) {
            if ($scope.data.findings === undefined) {
                $scope.data.findings = [];
            }
            if ($scope.gdata.isMissionDirector) {
                $scope.data.allFindingsSelected = newValue !== 0 && newValue === $scope.data.findings.length;
            } else {
                // Operators
                var findingsSelected = _.reject($scope.data.selectedFindings, { state: 'approved' });
                var unapprovedFindings = _.reject($scope.data.findings, { state: 'approved' });
                $scope.data.allFindingsSelected = newValue !== 0 && unapprovedFindings.length === findingsSelected.length;
            }
        });
    }

    function removeFinding(finding) {
        var removed = _.remove($scope.data.findings, { id: finding.id });

        if (($state.params.operation_id && $state.params.operation_id.toString() === finding.operation.toString()) || removed.length > 0) {
            $scope.data.count--;
            if ($scope.findingsToDoTab) {
                $scope.$parent.data.findingsCount--;
                $scope.findingsTab.notifications.pop();
            }
        }

        if ($scope.data.findings.length === 0 && removed.length > 0) {
            if ($scope.currentPage === $scope.numPages) {
                if ($scope.currentPage > 1) {
                    // We are on the last page and need to go back one because the current page no longer exists
                    $scope.currentPage--;
                } else {
                    $scope.numPages = 0;
                    return; // We just removed the last finding from the only page so there is nothing left to display
                }
            }
            $scope.loadFindings($scope.currentPage); // TODO: This could still 404 if enough findings from other pages were deleted or edited to not match the current filters
        }
    }

    var transitionUnsubscribe = $transitions.onSuccess(
        {
            to: stateIsFindingDetail,
            from: stateIsFindingDetailOrOperationDetail,
        },
        loadOpenedFindingDetails
    );

    var findingUnsubscribe = socketService.subscribeByModel('finding', function (finding) {
        if ($scope.findingsToDoTab) {
            if (finding.deleted || finding.state === 'approved' || finding.state === 'fix') {
                removeFinding(finding);
                return;
            }
        }

        if (finding.deleted || ($scope.gdata.isCustomer && finding.archived !== $scope.data.showArchived)) {
            removeFinding(finding);
            return;
        }

        if ($state.params.operation_id && $state.params.operation_id.toString() !== finding.operation.toString()) {
            return; // Findings for a different operation on the operation detail page should not be displayed
        }
        if (!$scope.operationDetailPage && !$scope.findingsToDoTab && (!$scope.customer || $scope.customer.id !== finding.customer)) {
            return; // On the findings tab, do not display findings for other customers or if no customer is selected
        }

        // TODO: only show findings that match the current filters
        var curFinding = _.find($scope.data.findings, { id: finding.id });
        if (curFinding) {
            $scope.updateObj(curFinding, finding);
        } else {
            $scope.data.findings.push(finding);
            if ($scope.numPages === 0) {
                $scope.numPages = 1;
            }
            $scope.data.count++;

            if ($scope.findingsToDoTab) {
                $scope.$parent.data.findingsCount++;
                $scope.findingsTab.notifications.push('');
            }
        }
    });

    var messageUnsubscribe = socketService.subscribeByModel('message', function (response) {
        // Make sure it's a finding message and also that we even had those messages for that finding open, otherwise, we'll fetch it on click later
        if (response.finding) {
            const msgFindingId = response.finding.id;
            if (_.isArray($scope.data.findingMessages[msgFindingId])) {
                const existingMessageIndex = $scope.data.findingMessages[msgFindingId].findIndex((item) => item.id === response.id);
                if (existingMessageIndex >= 0) {
                    $scope.data.findingMessages[msgFindingId][existingMessageIndex] = response;
                } else {
                    $rootScope.newItemCounts.messages = $rootScope.newItemCounts.messages + 1;
                    $scope.data.findingMessages[msgFindingId].push(response);
                }
            } else {
                $scope.data.findingMessages[msgFindingId] = [response];
            }
            var finding: any = _.find($scope.data.findings, { id: msgFindingId });
            if (!_.isUndefined(finding) && response.relevant_customers.length) {
                const existingMessageFindingIndex = finding.messages.findIndex((item) => item.id === response.id);
                if (existingMessageFindingIndex > 0) {
                    finding.messages[existingMessageFindingIndex] = response;
                } else {
                    finding.messages.push(response);
                }
            }
        }
    });

    $scope.$on('$destroy', function () {
        findingUnsubscribe();
        messageUnsubscribe();
        transitionUnsubscribe();
        $scope.targetViewPreference.unsubscribe();
    });

    $scope.followupToggle = function () {
        console.log($scope.form.followup_requested);
        if ($scope.form.followup_requested === 'either') {
            $scope.form.followup_requested = 'requested';
        } else if ($scope.form.followup_requested === 'requested') {
            $scope.form.followup_requested = 'unrequested';
        } else if ($scope.form.followup_requested === 'unrequested') {
            $scope.form.followup_requested = 'either';
        }
        $scope.loadFindings();
    };

    $scope.clearTarget = () => {
        $scope.form.target = null;
        $scope.loadFindings();
    };

    $scope.filterAliasOrValue = function (search) {
        return function (target, index, array) {
            const matchesAliasOrValue =
                target.value.toLowerCase().includes(search.toLowerCase()) ||
                (target.alias && target.alias.toLowerCase().includes(search.toLowerCase()));
            return matchesAliasOrValue;
        };
    };

    $scope.loadFindingsByCustomer = function (customer) {
        delete $scope.targets; // Clear saved targets so we load the newly selected customer's targets
        $scope.form.appliance = null;
        $scope.form.target = null;
        $scope.customer = customer;
        if (!customer && !$scope.findingsToDoTab) {
            $scope.data.findings = [];
            $scope.numPages = 0;
            return;
        }
        $scope.loadFindings();
    };

    $scope.clearCustomer = function (fromButton?: boolean) {
        if (fromButton) {
            $scope.customer = undefined;
            $scope.loadFindings();
        }
    };

    function handleFindingResults(results: any[], total: number) {
        $scope.data.findings = results;
        $scope.data.count = total;
        $scope.numPages = Math.ceil($scope.data.count / $scope.itemsPerPage);
        $scope.findingsLoaded = true;

        if ($state.params.finding_id) loadOpenedFindingDetails();

        if ($scope.selectedFinding) {
            $scope.findingsTable.data.filter((e) => e.id === $scope.selectedFinding.id)[0].selected = true;
            $scope.selectedFinding = null;
        }

        if ($scope.findingsToDoTab) {
            $scope.$parent.data.findingsCount = $scope.data.count;
            $scope.findingsTab.notifications = new Array($scope.data.count);
        }
    }

    $scope.loadGraphFindings = function (filter: FindingSearchNode) {
        $scope.loadFindings(1, false, filter);
    };

    $scope.loadFindings = function (page = 1, initial = false, filter: FindingSearchNode = undefined) {
        if (!$scope.runFindingsPrecheck()) return;

        // TODO: figure out some way to undo the filter change if the user cancels?
        $scope.openReloadDialog(confirmed);

        function confirmed() {
            let params: any = getParameters(initial);
            if (!params) return;

            params.page = page;
            params.page_size = $scope.itemsPerPage;
            $scope.currentPage = page;

            $scope.initial = initial;

            if ($scope.findingsToDoTab) {
                delete params.state;
                if ($scope.gdata.isMissionDirector) {
                    $scope.findingsTable = ngTableService.serverFilteredTable({
                        resource: Findings.unapproved,
                        defaultParams: params,
                        extraInitialParams: {
                            count: 30,
                        },
                        extraInitialSettings: {
                            counts: [10, 30, 50, 100, 200, 500],
                        },
                        loadedCallback: handleFindingResults,
                        ignoreSecondLoadIfDuplicate: true,
                    });
                } else if ($scope.gdata.isOperator) {
                    params.operator = $rootScope.user.id;
                    $scope.findingsTable = ngTableService.serverFilteredTable({
                        resource: Findings.rejected,
                        defaultParams: params,
                        extraInitialParams: {
                            count: 30,
                        },
                        extraInitialSettings: {
                            counts: [10, 30, 50, 100, 200, 500],
                        },
                        loadedCallback: handleFindingResults,
                        ignoreSecondLoadIfDuplicate: true,
                    });
                }
            } else if (filter || (filter === undefined && $scope.graphFilter)) {
                // `undefined` is a valid, distinct, value for filter
                $scope.selectedFinding = $scope.data.selectedFindings[0];

                $scope.findingsTable = ngTableService.serverFilteredTable({
                    resource: FindingSearch.getGraphFindingsAsResource.bind(FindingSearch), //  <---- this line right here; the rest I've shown for context
                    defaultParams: params,
                    extraFilter: filter || $scope.graphFilter,
                    extraInitialParams: {
                        count: 30,
                    },
                    extraInitialSettings: {
                        counts: [10, 30, 50, 100, 200, 500],
                    },
                    loadedCallback: handleFindingResults,
                    ignoreSecondLoadIfDuplicate: true,
                });
            } else {
                $scope.findingsTable = ngTableService.serverFilteredTable({
                    resource: Findings.get,
                    defaultParams: params,
                    extraInitialParams: {
                        count: 30,
                    },
                    extraInitialSettings: {
                        counts: [10, 30, 50, 100, 200, 500],
                    },
                    loadedCallback: handleFindingResults,
                    ignoreSecondLoadIfDuplicate: true,
                });
            }
            $scope.data.selectedFindings = [];
            $scope.findingsTable.reload();

            // Handles the case where user clears the filters
            if (filter !== undefined) $scope.graphFilter = filter;
        }
    };

    $scope.runFindingsPrecheck = function (): boolean {
        if ($scope.customer) {
            $scope.appliances = $rootScope.loadFilterableAppliances($scope.gdata.appliancesByCustomer[$scope.customer.id]);
        }

        if (!$scope.customer && !$scope.operationDetailPage && !$scope.findingsToDoTab) {
            $scope.data.findings = [];
            $scope.numPages = 0;
            return false;
        }

        return true;
    };

    $scope.openReloadDialog = function (successCB: Function, cancelCB: Function) {
        // Only need to confirm/reload when a Finding is selected or graph search is enabled
        if ($scope.data.selectedFindings.length || $scope.graphFilter != null) {
            modalService.openConfirm(
                'Findings selected',
                'reload',
                'the displayed findings and clear your selection',
                undefined,
                successCB,
                cancelCB
            );
        } else {
            successCB();
        }
    };

    function getParameters(initial?: boolean) {
        if (!$scope.targets && !$scope.operationDetailPage && $scope.customer) {
            $scope.targets = Targets.basicList({ customer: $scope.customer.id });
        }

        $scope.findingsLoaded = false;
        $scope.data.message = 'Loading Findings';

        var severityIndices = [];
        // Filtering by all severities is a no-op so only pass them if we aren't passing them all
        if ($scope.findingSeverityFilters.length !== $scope.SEVERITY_LEVELS.length) {
            severityIndices = _.at(FindingService.SEVERITY_LEVELS_LABEL_TO_VALUE, $scope.findingSeverityFilters);
        }

        var followupRequested = undefined;
        if ($scope.form.followup_requested === 'requested') {
            followupRequested = true;
        } else if ($scope.form.followup_requested === 'unrequested') {
            followupRequested = false;
        }

        var joinedReferences = $scope.form.references.length > 0 ? '' : undefined;
        for (var cnt = 0; cnt < $scope.form.references.length; cnt++) {
            joinedReferences += $scope.form.references[cnt].id;

            if (cnt < $scope.form.references.length - 1) {
                joinedReferences += ',';
            }
        }

        var params = {
            ordering: 'updated',
            severity: severityIndices,
            page_size: $scope.itemsPerPage,
            search: $scope.form.findingsQuery,
            appliance: $scope.form.appliance,
            followup_requested: followupRequested,
            archived: undefined,
            customer: undefined,
            added__gte: undefined,
            added__lte: undefined,

            //TODO: Determine if it is a problem to have these not declared for UI-Router
            operation: $state.params.operation,
            target_multi: $scope.form.target,
            operation_template: $scope.form.opTemplate,
            state: [],
            reference_ids_any: joinedReferences,
        };

        if ($scope.form.added_after) {
            $scope.form.added_after.setHours(0, 0, 0, 0);
            params.added__gte = $scope.form.added_after;
        }

        if ($scope.form.added_before) {
            $scope.form.added_before.setHours(23, 59, 59, 999);
            params.added__lte = $scope.form.added_before;
        }

        if ($scope.gdata.isCustomer) {
            params.archived = $scope.data.showArchived;
        } else {
            // Operators
            // Filtering by all states is a no-op so only pass them if we aren't passing them all
            if ($scope.findingStateFilters.length !== $scope.FINDING_STATE_FILTERS.length) {
                params.state = $scope.findingStateFilters.concat();
            }
        }

        if ($scope.operationDetailPage) {
            params.operation = $state.params.operation_id;
        } else if ($scope.customer || !$scope.findingsToDoTab) {
            params.customer = $scope.customer.id;
        }

        if ($state.params.svc) {
            params['service'] = _.get($scope.gdata.serviceMap[$state.params.svc], 'id');
        }

        return params;
    }

    $scope.loadFindings($state.params.page, true);

    $scope.getDisplayTarget = (finding) => {
        if ($scope.isAliasShown) {
            if (finding.discovered_target_alias) {
                return finding.discovered_target_alias;
            } else if (finding.target_alias) {
                return finding.target_alias;
            }
        } else {
            if (finding.discovered_target_value) {
                return finding.discovered_target_value;
            } else if (finding.target_value) {
                return finding.target_value;
            }
        }
        if (finding.metatarget_name && $scope.gdata.isOperator) {
            return finding.metatarget_name;
        }
        return 'N/A';
    };

    $scope.getDisplayTargetType = (finding) => {
        if (finding.discovered_target_type) {
            return finding.discovered_target_type;
        } else if (finding.target_type) {
            return finding.target_type;
        } else if (finding.metatarget_type && $scope.gdata.isOperator) {
            return finding.metatarget_type;
        }
        return undefined;
    };

    $scope.findingDetailLinkClicked = function ($event, finding) {
        if (finding.id.toString() === _.toString($state.params.finding_id)) {
            // Clicked the same finding so close it by leaving its state
            var state = '.'; // Findings tab
            if ($scope.operationDetailPage) {
                state = '^';
            }
            $state.go(state, { finding_id: '' });
            $event.preventDefault();
        }
    };

    $scope.checkCanBeSelected = function (finding) {
        if (!$scope.gdata.isMissionDirector && finding.state === 'approved' && finding.selected) {
            finding.selected = false;
        }
    };

    $scope.selectAllFindings = function () {
        $scope.findingsTable.data = $rootScope.selectAll($scope.data.allFindingsSelected, $scope.findingsTable.data);
    };

    $scope.addRemoveFindingStateFilter = function ($event, state) {
        if ($event.shiftKey) {
            // If the shift key is pressed, toggle all other states
            _.each($scope.FINDING_STATE_FILTERS, function (s) {
                if (s !== state) {
                    $scope.findingStateFilters = FindingService.addRemoveFindingFilter($scope.findingStateFilters, s);
                }
            });
        } else {
            $scope.findingStateFilters = FindingService.addRemoveFindingFilter($scope.findingStateFilters, state);
        }
        $scope.loadFindings(1);
    };

    $scope.addRemoveFindingSeverityFilter = function ($event, severity) {
        if ($event.shiftKey) {
            // If the shift key is pressed, toggle all other severities
            _.each($scope.SEVERITY_LEVELS, function (s) {
                if (s !== severity) {
                    $scope.findingSeverityFilters = FindingService.addRemoveFindingFilter($scope.findingSeverityFilters, s);
                }
            });
        } else {
            $scope.findingSeverityFilters = FindingService.addRemoveFindingFilter($scope.findingSeverityFilters, severity);
        }
        $scope.loadFindings(1);
    };

    $scope.setPageSize = function (size) {
        $scope.loadFindings(1);
    };

    $scope.uploadAttachment = function () {
        MatDialog.open(AttachmentComponent)
            .afterClosed()
            .subscribe((queuedAttachments: File[]) => {
                ($scope.messageFormData.queuedAttachments = $scope.messageFormData.queuedAttachments || []).push(...queuedAttachments);
            });
    };

    $scope.submitNewMessage = function () {
        var finding = this.finding;
        var msg = {
            value: $scope.messageFormData.value,
            finding: finding.id,
        };
        Messages.addMessage(msg)
            .$promise.then((msgResp) => {
                if ($scope.messageFormData.queuedAttachments) {
                    for (const attachment of $scope.messageFormData.queuedAttachments) {
                        AttachmentService.uploadAttachment(attachment, msgResp.id)
                            .toPromise()
                            .then((attachmentRes) => {
                                console.log(attachmentRes);
                            });
                    }
                }
                $scope.messageFormData = {};
            })
            .catch(modalService.openErrorResponseCB('Sending message failed'));
    };

    function stateIsFindingDetail(state) {
        return state.name === 'cato.loggedIn.operator.findingDetail' || state.name === 'cato.loggedIn.dashboard.findings';
    }

    function stateIsFindingDetailOrOperationDetail(state) {
        return state.includes['cato.loggedIn.operator'] || stateIsFindingDetail(state);
    }

    function loadOpenedFindingDetails() {
        var findingId = parseInt($state.params.finding_id);
        if (findingId) {
            var finding: any = _.find($scope.data.findings, { id: findingId });
            if (finding) {
                if (!finding.loaded) {
                    finding.$promise = Findings.detail({ findingId: findingId }, function (findingDetail) {
                        findingDetail.operation.customer = findingDetail.customer;
                        $scope.data.findingMessages[finding.id] = _.clone(findingDetail.messages);
                        finding.loaded = true;

                        $rootScope.updateObj(finding, findingDetail);
                    }).$promise;

                    scrollToFinding(findingId);
                }
            } else {
                Findings.detail({ findingId: findingId }, function (finding) {
                    finding.operation.customer = finding.customer;
                    $scope.data.findingMessages[finding.id] = _.clone(finding.messages);
                    finding.loaded = true;

                    $scope.data.findings.unshift(finding);

                    scrollToFinding(findingId);
                });
            }
        }
    }

    function scrollToFinding(findingId) {
        $timeout(function () {
            // Make sure transitions have settled and the DOM has rendered
            $anchorScroll('finding-' + findingId);
        });
    }

    function pick<T, K extends keyof T>(obj: T, ...keys: K[]): Pick<T, K> {
        const ret: any = {};
        keys.forEach((key) => {
            if (obj[key] !== undefined) {
                ret[key] = obj[key];
            }
        });
        return ret;
    }
});
