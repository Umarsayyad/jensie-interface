import app from '../../app';
import * as _ from 'lodash';

import './providerUrlTemplateEditor/providerUrlTemplateEditor';

app.component('modalEditProvider', {
    bindings: {
        provider: '<?',
        providerSaved: '<?',
        modalTitle: '<?',
        config: '<',
        close: '<',
        dismiss: '<',
    },
    templateUrl: '/app/finding/reference/modalEditProvider.html',
    controller: function ($rootScope, modalService, Providers) {
        var ctrl: any = {
            editCount: 0,

            $onInit: function () {
                // Bindings are now available
                if (_.isUndefined(ctrl.provider)) {
                    ctrl.modalTitle = ctrl.modalTitle || 'Create New Reference Provider';
                    ctrl.provider = { url_templates: [] };
                } else {
                    ctrl.modalTitle = ctrl.modalTitle || 'Edit Reference Provider';
                    ctrl.provider = $rootScope.makePlainCopy(ctrl.provider);
                }
            },

            // Event/callback handlers
            addUrlTemplate: function () {
                if (!ctrl.provider.url_templates) {
                    ctrl.provider.url_templates = [];
                }
                ctrl.provider.url_templates.unshift({
                    label: '',
                    template: '',
                });
            },
            updateUrlTemplate: function (oldTemplate, newTemplate) {
                if (newTemplate === undefined) {
                    _.pull(ctrl.provider.url_templates, oldTemplate);
                } else {
                    $rootScope.updateObj(oldTemplate, newTemplate);
                }
            },

            // Save/submit handlers
            saveProvider: function () {
                var sendProvider = $rootScope.copyForRequest(ctrl.provider, ctrl.config);

                if (ctrl.provider.id) {
                    ctrl.submission = Providers.update(sendProvider, saveSuccess, saveError);
                } else {
                    ctrl.submission = Providers.create(sendProvider, saveSuccess, saveError);
                }
            },
        };

        function saveSuccess(provider) {
            (ctrl.providerSaved || _.noop)(provider);
            if (!ctrl.addAnother) {
                ctrl.close(provider);
            } else {
                ctrl.message = 'Reference Provider saved successfully';
            }
        }

        function saveError(result) {
            modalService.openErrorResponse('Reference Provider could not be saved.', result, false);
        }

        for (var prop in ctrl) {
            if (!ctrl.hasOwnProperty(prop)) {
                //The current property is not a direct property of ctrl
                continue;
            }
            this[prop] = ctrl[prop];
        }

        //Replace the initial ctrl object with the updated this object in order to prevent the two from getting out of sync
        ctrl = this;
    },
});
