import app from '../../app';
import * as _ from 'lodash';

import '../../common/actionsDropdownComp';
import './modalEditReference';

app.component('modalReferences', {
    bindings: {
        provider: '<?',
        close: '<',
        dismiss: '<',
    },
    templateUrl: '/app/finding/reference/modalReferences.html',
    controller: function ($rootScope, $uibModal, ngTableService, NgTableParams, globalDataService, modalService, stateBuilder, References, Targets) {
        var lastOpened = {};
        var config;
        var filters = {};

        var filterComparator = function (value, searchTerm) {
            var returnVal = undefined;
            ctrl.searchTerm = undefined;
            if (typeof value === 'string' && value.length > 0) {
                returnVal = (value.toLowerCase() || '').indexOf(searchTerm.toLowerCase()) !== -1;
                ctrl.searchTerm = searchTerm;
            } else if (typeof value === 'number') {
                returnVal = value === Number(searchTerm);
            }

            return returnVal;
        };

        var defaultParams = {};

        var extraInitialParams = {};

        function updateParamsCallback(params) {}

        var ctrl: any = {
            filters: filters,
            reloading: false,
            gdata: globalDataService,
            catotheme: $rootScope.catotheme,
            dataset: [],
            lockedProvider: false,
            targetMap: {},
            _extraData: {},
            numberOfLinesPerTag: 5,
            searchTerm: undefined,

            table: ngTableService.serverFilteredTable({
                resource: References.get,
                defaultParams: defaultParams,
                extraInitialParams: extraInitialParams,
                updateParamsCallback: updateParamsCallback,
            }),

            referenceActions: {
                editReference: {
                    action: function (reference) {
                        if (!config) config = References.options();
                        var referenceSaved = false;

                        var data = {
                            reference: reference,
                            config: config.$promise,
                            referenceSaved: function (newRef) {
                                referenceSaved = true; // Track if references have been added so if the user uses the "add multiple" checkbox then dismisses the modal when finished, we still reload
                                /*  // Use the code below to update the table in-place when references are added or updated instead of reloading
                                if (reference) {
                                    $rootScope.updateObj(reference, newRef);
                                }
                                else {
                                    ctrl.table.data.push(newRef);
                                }*/
                            },
                        };

                        var modal = stateBuilder.componentModalGenerator(
                            'modal-edit-reference',
                            stateBuilder.editReferenceModalBindings,
                            data,
                            'finding/reference/modalEditReference'
                        );

                        var modalInstance = $uibModal.open(modal);

                        function checkReload() {
                            if (referenceSaved) {
                                // Only reload the table if references have been added
                                ctrl.table.reload();
                            }
                        }

                        modalInstance.result.then(checkReload, checkReload);
                    },
                    class: 'btn-white',
                    icon: 'fa-pencil',
                    label: 'Edit',
                    tooltip: 'Edit Reference',
                },
                deleteReference: {
                    action: function (reference) {
                        modalService.openDelete(
                            'Confirmation',
                            "Reference '" + reference.source_key + (reference.title ? ' ' : '') + reference.title + "'",
                            {},
                            function () {
                                References.delete(
                                    { id: reference.id },
                                    function () {
                                        ctrl.table.reload();
                                    },
                                    modalService.openErrorResponseCB('Error deleting Reference')
                                );
                            }
                        );
                    },
                    class: 'btn-white',
                    icon: 'fa-trash',
                    label: 'Delete',
                    tooltip: 'Delete Reference',
                },
            },
            $onInit: function () {
                if (typeof ctrl.provider !== 'undefined') {
                    defaultParams['provider__name__icontains'] = ctrl.provider.name;
                    ctrl.lockedProvider = true;
                }
            },
            $onChanges: function () {},
            $onDestroy: function () {},
            extraData: function (tag) {
                tag = tag || {};
                var extraData = ctrl._extraData[tag.id];
                if (!extraData) {
                    extraData = ctrl._extraData[tag.id] = {};
                }
                return extraData;
            },
            toggleTargetQuickView: function (tag) {
                ctrl.extraData(tag).targetPopoverIsOpen = !ctrl.extraData(tag).targetPopoverIsOpen;
                if (ctrl.extraData(tag).targetPopoverIsOpen) {
                    closeLastOpen(tag, 'target');
                    var makeTable = true;
                    if (!ctrl.extraData(tag).targetDetails) {
                        if (_.some(_.at(ctrl.targetMap, tag.targets), _.isUndefined)) {
                            makeTable = false;
                            ctrl.extraData(tag).targetDetails = Targets.basicList({ tags: tag.id }, function (targets) {
                                _.each(targets, storeTarget);
                                buildTargetsTable(tag);
                            });
                        } else {
                            ctrl.extraData(tag).targetDetails = true;
                        }
                    }
                    if (makeTable) {
                        buildTargetsTable(tag);
                    }
                }
            },
        };

        for (var prop in ctrl) {
            if (!ctrl.hasOwnProperty(prop)) {
                //The current property is not a direct property of ctrl
                continue;
            }
            this[prop] = ctrl[prop];
        }

        //Replace the initial ctrl object with the updated this object in order to prevent the two from getting out of sync
        ctrl = this;

        function storeTarget(target) {
            target.customerName = globalDataService.customerMap[target.customer].name;
            if (ctrl.targetMap[target.id]) {
                $rootScope.updateObj(ctrl.targetMap[target.id], target);
            } else {
                ctrl.targetMap[target.id] = target;
            }
        }

        function closeLastOpen(tag, current) {
            if (current !== 'customer' || tag !== lastOpened) {
                ctrl.extraData(lastOpened).customerPopoverIsOpen = false;
            }
            if (current !== 'target' || tag !== lastOpened) {
                ctrl.extraData(lastOpened).targetPopoverIsOpen = false;
            }
            lastOpened = tag;
        }

        var _targetsTableCache = {};

        function buildTargetsTable(tag) {
            if (_targetsTableCache[tag.id] && _.isEqual(tag.targets, _targetsTableCache[tag.id].targets)) {
                return;
            }

            var table = new NgTableParams(
                {
                    count: 20,
                },
                {
                    counts: [5, 10, 20, 40],
                    dataset: _.at(ctrl.targetMap, tag.targets),
                }
            );

            _targetsTableCache[tag.id] = {
                targets: tag.targets.concat(),
                table: table,
            };
            ctrl.extraData(tag).targetsTable = table;
        }
    },
});
