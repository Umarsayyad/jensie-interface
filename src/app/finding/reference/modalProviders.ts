import app from '../../app';
import * as angular from 'angular';
import * as _ from 'lodash';

import { renameDuplicate } from '../../common/utilities/rename-duplicate';

import '../../common/actionsDropdownComp';
import './modalEditProvider';
import './modalReferences';

app.component('modalProviders', {
    bindings: {
        close: '<',
        dismiss: '<',
    },
    templateUrl: '/app/finding/reference/modalProviders.html',
    controller: function (
        $filter,
        $rootScope,
        $uibModal,
        Blob,
        FileSaver,
        NgTableParams,
        globalDataService,
        modalService,
        socketService,
        stateBuilder,
        Providers
    ) {
        var config;
        var providerUnsubscribe;

        var ctrl: any = {
            gdata: globalDataService,
            catotheme: $rootScope.catotheme,
            table: new NgTableParams(
                {
                    count: 25, // initial page size
                },
                {
                    // page size buttons
                    counts: [10, 25, 50, 100, 500, 1000],
                    // determines the pager buttons (blocks in center around current):  round((max-min)/2)
                    paginationMaxBlocks: 7,
                    paginationMinBlocks: 0,
                    dataset: globalDataService.providers,
                    filterOptions: {
                        filterFn: function filterFn(data, filter, comparator) {
                            // TODO: abstract this into a function any table can use to generate a function for filtering
                            filter = _.clone(filter); // Only need a shallow copy
                            if (filter.url_templates !== undefined) {
                                data = _.filter(data, function (item) {
                                    return item.url_templates.length === filter.url_templates.length;
                                });
                                delete filter.url_templates;
                            }
                            if (filter.referenceCount !== undefined) {
                                data = _.filter(data, function (item) {
                                    return item.referenceCount === filter.referenceCount;
                                });
                                delete filter.referenceCount;
                            }

                            return $filter('filter')(data, filter, comparator); // Use AngularJS's built in filter for the rest
                        },
                    },
                }
            ),
            providerActions: {
                editProvider: {
                    action: function (provider) {
                        if (!config) config = Providers.options();

                        var data = {
                            provider: provider,
                            config: config.$promise,
                        };

                        var modal = stateBuilder.componentModalGenerator(
                            'modal-edit-provider',
                            stateBuilder.editProviderModalBindings,
                            data,
                            'finding/reference/modalEditProvider'
                        );
                        $uibModal.open(modal);
                    },
                    class: 'btn-white',
                    icon: 'fa-pencil',
                    label: 'Edit',
                    tooltip: 'Edit Provider',
                },
                duplicateProvider: {
                    action: function (provider) {
                        provider = angular.copy(provider);
                        provider.name = renameDuplicate(provider.name);
                        delete provider.id;
                        ctrl.providerActions.editProvider.action(provider);
                    },
                    class: 'btn-white',
                    icon: 'fa-copy',
                    label: 'Duplicate',
                    tooltip: 'Duplicate Provider',
                },
                viewReferences: {
                    action: function (provider) {
                        var data = {
                            provider: provider,
                        };

                        var modal = stateBuilder.componentModalGenerator(
                            'modal-references',
                            stateBuilder.referencesModalBindings,
                            data,
                            'finding/reference/modalReferences'
                        );
                        $uibModal.open(modal);
                    },
                    class: 'btn-white',
                    icon: 'fa-eye',
                    label: 'View References',
                    tooltip: 'View References for this provider',
                },
                deleteProvider: {
                    action: function (provider) {
                        modalService.openDelete('Confirmation', "Reference Provider '" + provider.name + "'", {}, function () {
                            Providers.delete(
                                { id: provider.id },
                                function () {},
                                modalService.openErrorResponseCB('Error deleting Reference Provider')
                            );
                        });
                    },
                    class: 'btn-white',
                    icon: 'fa-trash',
                    label: 'Delete',
                    tooltip: 'Delete Provider',
                },
            },
            $onInit: function () {
                ctrl.referenceCounts = Providers.referenceCount(function (referenceCounts) {
                    _.each(referenceCounts, function (providerData) {
                        // Yes, this modifies objects in gdata, but it won't break references and is safe enough for now
                        // It has the added benefit of updating all copies of the provider modal if multiple are opened simultaneously
                        globalDataService.providerMap[providerData.id].referenceCount = providerData.reference_count;
                    });
                });

                providerUnsubscribe = socketService.subscribeByModel('provider', function (provider) {
                    if (!provider.deleted && globalDataService.providerMap[provider.id].referenceCount === undefined) {
                        globalDataService.providerMap[provider.id].referenceCount = 0;
                    }
                    ctrl.table.reload();
                });
            },
            $onDestroy: function () {
                if (providerUnsubscribe) {
                    providerUnsubscribe();
                }
            },
            import: function ($file) {
                if ($file !== null) {
                    var reader = new FileReader();
                    reader.addEventListener('loadend', function () {
                        function displayImportResults(res) {
                            $uibModal.open({
                                templateUrl: '/app/md/planning/modalImportResult.html',
                                controller: function ($scope, $uibModalInstance) {
                                    $scope.title = 'Import Result';
                                    $scope.logmsgs = res.log;

                                    $scope.errors = _.map(res.errors, function (err) {
                                        if (!_.isString(err)) {
                                            return angular.toJson(err);
                                        }
                                        return err;
                                    });

                                    $scope.ok = function () {
                                        $uibModalInstance.close('ok');
                                    };
                                },
                                size: 'lg',
                            });
                        }

                        Providers.import(
                            reader.result,
                            function (res) {
                                displayImportResults(res);
                            },
                            function (err) {
                                displayImportResults(err);
                            }
                        );
                    });
                    reader.readAsText($file);
                }
            },
            export: function () {
                Providers.export(function (res) {
                    var data = new Blob([angular.toJson(res, true)], { type: 'application/json;charset=utf-8' });
                    FileSaver.saveAs(data, 'providers-export.json');
                }, modalService.openErrorResponseCB('Reference Providers Export Error'));
            },
        };

        for (var prop in ctrl) {
            if (!ctrl.hasOwnProperty(prop)) {
                //The current property is not a direct property of ctrl
                continue;
            }
            this[prop] = ctrl[prop];
        }

        //Replace the initial ctrl object with the updated this object in order to prevent the two from getting out of sync
        ctrl = this;
    },
});
