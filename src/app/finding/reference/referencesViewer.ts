import app from '../../app';
import * as _ from 'lodash';

import './modalEditReference';

app.component('referencesViewer', {
    templateUrl: '/app/finding/reference/referencesViewer.html',
    bindings: {
        references: '<',
    },
    controller: function ($rootScope, $uibModal, stateBuilder, References) {
        var config;

        var ctrl: any = {
            viewReference: function (reference) {
                if (!config) config = References.options();

                var data = {
                    reference: reference,
                    config: config.$promise,
                    referenceSaved: function (newRef) {
                        $rootScope.updateObj(reference, newRef);
                    },
                };

                var modal = stateBuilder.componentModalGenerator(
                    'modal-edit-reference',
                    stateBuilder.editReferenceModalBindings,
                    data,
                    'finding/reference/modalEditReference'
                );
                $uibModal.open(modal);
            },
        };

        for (var prop in ctrl) {
            if (!ctrl.hasOwnProperty(prop)) {
                //The current property is not a direct property of ctrl
                continue;
            }
            this[prop] = ctrl[prop];
        }

        //Replace the initial ctrl object with the updated this object in order to prevent the two from getting out of sync
        ctrl = this;
    },
});
