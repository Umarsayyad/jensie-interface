import app from '../../../app';
import * as _ from 'lodash';
import * as angular from 'angular';

app.component('providerUrlTemplateEditor', {
    templateUrl: '/app/finding/reference/providerUrlTemplateEditor/providerUrlTemplateEditor.html',
    bindings: {
        template: '<',
        templateChanged: '&',
        editStarted: '&',
        editStopped: '&',
    },
    controller: function providerUrlTemplateEditor() {
        var ctrl: any = {
            editing: false,
            new: false,

            $onInit: function () {
                ctrl.updatedTemplate = angular.copy(ctrl.template);
                if (ctrl.updatedTemplate.template === '') {
                    ctrl.new = true;
                    ctrl.edit();
                }
            },

            appendReplacementVariable: function ($event) {
                if (!$event.currentTarget.hasAttribute('disabled') && ctrl.updatedTemplate.template.indexOf('$source_key') < 0) {
                    ctrl.updatedTemplate.template += '$source_key';
                }
            },
            checkSave: function ($event) {
                if ($event.which === 13) {
                    $event.preventDefault();
                    ctrl.save();
                }
            },

            edit: function () {
                ctrl.editStarted();
                ctrl.editing = true;
            },
            save: function () {
                ctrl.editStopped();
                ctrl.templateChanged({ updatedTemplate: ctrl.updatedTemplate });
                ctrl.editing = false;
                ctrl.new = false;
            },
            remove: function () {
                if (ctrl.new) {
                    ctrl.editStopped();
                }
                ctrl.templateChanged({ updatedTemplate: undefined });
            },
            cancel: function () {
                ctrl.editStopped();
                ctrl.updatedTemplate = angular.copy(ctrl.template);
                ctrl.editing = false;
            },
        };

        for (var prop in ctrl) {
            if (!ctrl.hasOwnProperty(prop)) {
                //The current property is not a direct property of ctrl
                continue;
            }
            this[prop] = ctrl[prop];
        }

        //Replace the initial ctrl object with the updated this object in order to prevent the two from getting out of sync
        ctrl = this;
    },
});
