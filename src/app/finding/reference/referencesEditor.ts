import app from '../../app';
import * as _ from 'lodash';

app.component('referencesEditor', {
    templateUrl: '/app/finding/reference/referencesEditor.html',
    require: {
        ngModelCtrl: '?ngModel',
    },
    bindings: {
        references: '<?',
        ngModel: '<?',
        filterMode: '<?',
        confirmRemoval: '<?',
        disabled: '@?',
        referenceAdded: '&',
        referenceRemoved: '&',
    },
    controller: function (globalDataService, modalService, References) {
        var ctrl: any = {
            gdata: globalDataService,
            availableReferences: [],
            params: {
                page: 1, //This might not be needed if the API always defaults to the first page.
                page_size: 10, //TODO: Do we want to make this a preference?
            },

            $onInit: function () {
                // Bindings are now available
                if (ctrl.references && ctrl.ngModel) {
                    throw 'You cannot use both references="" and ng-model="" on the same <references-editor>';
                }

                loadByType();
                if (ctrl.ngModel && ctrl.ngModel.length > 0 && typeof ctrl.ngModel[0] === 'number') {
                    References.listIds({ ids: ctrl.ngModel }).$promise.then(function (response) {
                        console.log(response);
                        ctrl.ngModel = response;
                    });
                }
            },

            $onChanges: function (changes) {
                if (changes.type) {
                    loadByType();
                }
            },

            $onDestroy: function () {
                console.log(ctrl.ngModel);
            },
            onRemove: function ($item) {
                ctrl.ngModelCtrl.$setViewValue(ctrl.ngModel);
            },
            onSelect: function ($item) {
                ctrl.ngModelCtrl.$setViewValue(ctrl.ngModel);
            },
            processIncomingReferences: function (references) {},
            searchReferences: function (searchValue) {
                ctrl.params.source_key__icontains = searchValue;

                References.get(ctrl.params).$promise.then(function (response) {
                    ctrl.availableReferences = response.results;
                });
            },
        };

        for (var prop in ctrl) {
            if (!ctrl.hasOwnProperty(prop)) {
                //The current property is not a direct property of ctrl
                continue;
            }
            this[prop] = ctrl[prop];
        }

        //Replace the initial ctrl object with the updated this object in order to prevent the two from getting out of sync
        ctrl = this;

        /*
        TODO: update ngreferencesInput to support calling a function to create or modify the reference object when it is added
        This will allow significantly simplifying the logic involved in setting the ngModel value by not requiring
        multiple loops through AngularJS's change detection to update all of the references to the appropriate objects.
        */

        // Utility functions
        function loadByType() {
            ctrl.referenceMap = globalDataService[ctrl.type + 'Map'];
            ctrl.referenceArr = globalDataService[ctrl.type + 's'];
            ctrl.loaded = ctrl.referenceMap && ctrl.referenceArr;

            if (ctrl.loaded && ctrl.filterMode && ctrl.customer) {
                var customer = parseInt(ctrl.customer.id || ctrl.customer);
                ctrl.referenceArr = _.filter(ctrl.referenceArr, function (reference) {
                    return reference.public || _.includes(reference.customers, customer);
                });
            }
        }
    },
});
