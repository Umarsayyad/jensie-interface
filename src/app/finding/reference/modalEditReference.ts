import app from '../../app';
import * as _ from 'lodash';
import * as XRegExp from 'xregexp';

app.component('modalEditReference', {
    bindings: {
        reference: '<?',
        referenceSaved: '<?',
        modalTitle: '<?',
        config: '<',
        close: '<',
        dismiss: '<',
    },
    templateUrl: '/app/finding/reference/modalEditReference.html',
    controller: function ($rootScope, globalDataService, modalService, References) {
        var ctrl: any = {
            providers: globalDataService.providers,
            gdata: globalDataService,
            source_key_regex: undefined,
            urls: [],

            $onInit: function () {
                // Bindings are now available

                if (_.isUndefined(ctrl.reference)) {
                    ctrl.modalTitle = ctrl.modalTitle || 'Create New Reference';
                    ctrl.reference = {
                        source_key: '',
                        title: '',
                        provider: '',
                    };
                } else {
                    ctrl.modalTitle = ctrl.modalTitle || 'Edit Reference';
                    ctrl.reference = $rootScope.makePlainCopy(ctrl.reference);
                    ctrl.selectProvider(globalDataService.providerNameMap[ctrl.reference.provider]);
                    console.log(ctrl.reference);
                }
            },

            makeURL: function (url) {
                if (!/^https?:\/\//.test(url)) {
                    return 'https://' + url;
                } else {
                    return url;
                }
            },

            // Save/submit handlers
            saveReference: function () {
                var sendReference = ctrl.reference; //$rootScope.copyForRequest(ctrl.reference, ctrl.config);

                if (ctrl.reference.id) {
                    ctrl.submission = References.update(sendReference, saveSuccess, saveError);
                } else {
                    ctrl.submission = References.create(sendReference, saveSuccess, saveError);
                }
            },
            selectProvider: function ($item) {
                if ($item.source_key_regex) {
                    ctrl.source_key_regex = XRegExp($item.source_key_regex); // type: xregexp.XRegExp
                }
                ctrl.reference.provider_obj = $item;
                ctrl.refreshUrls();
            },
            refreshUrls: function () {
                ctrl.urls = [];

                if (ctrl.reference.provider_obj && ctrl.reference.source_key) {
                    var sourceKey = ctrl.reference.source_key;
                    if (ctrl.source_key_regex) {
                        var match = XRegExp.exec(sourceKey, ctrl.source_key_regex);
                        if (match && match.url_source_key) {
                            sourceKey = match.url_source_key;
                        } else {
                            console.log(
                                'source_key_regex "%s" did not match source key "%s" or it had no url_source_key group; using source_key as-is in URL',
                                ctrl.source_key_regex,
                                sourceKey
                            );
                        }
                    }
                    _.each(ctrl.reference.provider_obj.url_templates, function (template) {
                        ctrl.urls.push({
                            url: template.template.replace('$source_key', sourceKey),
                            template: template,
                        });
                    });
                }
            },
        };

        function saveSuccess(reference) {
            (ctrl.referenceSaved || _.noop)(reference);
            if (!ctrl.addAnother) {
                ctrl.close(reference);
            } else {
                ctrl.message = 'Reference saved successfully';
            }
        }

        function saveError(result) {
            modalService.openErrorResponse('Reference could not be saved.', result, false);
        }

        for (var prop in ctrl) {
            if (!ctrl.hasOwnProperty(prop)) {
                //The current property is not a direct property of ctrl
                continue;
            }
            this[prop] = ctrl[prop];
        }

        //Replace the initial ctrl object with the updated this object in order to prevent the two from getting out of sync
        ctrl = this;
    },
});
