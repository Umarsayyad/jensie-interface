import app from '../../app';
import { config } from '@cato/config';
import { IHttpResponse, IHttpService, IModule, Injectable, IPromise } from 'angular';

import { PaginatedResult } from '../../common/interfaces/paginated-result';
import { FindingNode, FindingSearchNode } from './nodes';

// Convert `app` to the correct type
const appModule = (app as unknown) as IModule;

export class FindingSearchService implements Injectable<any> {
    public static $inject = ['$http', '$q'];
    constructor(private $http: IHttpService, private $q: any) {}

    public getGraphFindings(filter: FindingSearchNode, params: any): Promise<PaginatedResult<any>> {
        return this.$q((resolve, reject) => {
            // Map the result of the HTTP Request to PaginatedResult
            this.$http
                .post(config.apiPath + 'graph/list_findings', filter, { params })
                .then((res: IHttpResponse<PaginatedResult<any>>) => resolve(res.data))
                .catch(reject);
        });
    }

    public getGraphFindingChildren(finding: number, node: string): Promise<FindingNode> {
        return this.$q((resolve, reject) => {
            // Map the result of the HTTP Request to FindingNode
            this.$http
                .post(config.apiPath + 'graph/list_node_properties', { finding, node })
                .then((res: IHttpResponse<FindingNode>) => resolve({ ...res.data, finding }))
                .catch(reject);
        });
    }

    public getGraphFindingProperties(finding: number, children: string[] = []): IPromise<IHttpResponse<any>> {
        return this.$http.post(config.apiPath + 'graph/list_properties', { finding, children });
    }

    public getGraphFindingsAsResource(filter: FindingSearchNode, params: any): { $promise: Promise<PaginatedResult<any>> } {
        return { $promise: this.getGraphFindings(filter, params) };
    }
}

appModule.service('FindingSearch', FindingSearchService);
