export interface NodeMap<T> {
    nodes: { [key: string]: T };
}
export interface Finding {
    finding: number;
}

export interface FindingNode extends NodeMap<Node> {
    finding: number;
    root: string;
}

export type FindingSearchNode = Finding & NodeMap<Array<string>>;

export interface Node {
    label: string;
    labels: string[];
    node_id: string;
    properties: object;
    children: string[];
}
