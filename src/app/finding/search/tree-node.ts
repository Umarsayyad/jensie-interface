export interface TreeNode {
    id: string;
    title: string;
    description?: string;
    children: TreeNode[];
    expandable: boolean;
    loading?: Promise<void>;
}
