import app from '../../app';

import './finding-search.service';

import { FindingSearchService } from './finding-search.service';
import { TreeNode } from './tree-node';
import { Node, FindingNode, FindingSearchNode } from './nodes';

class FindingSearchCtrl implements ng.IComponentController {
    private list: TreeNode[];
    private allTreeNodes: { [id: string]: TreeNode } = {};
    private filterProperties: Set<string>;

    private isopen = false;
    private selected: { id: number; title: string };
    private prevSelected: number = null;
    private selectedTitle = '';
    private loadGraphFindings: Function;
    private loadingError = false;
    private actionTooltip =
        'Select a Finding to filter for others with similar properties (selecting more than 1 will list the properties for just the first)';

    public static $inject = ['FindingSearch'];
    constructor(private findingService: FindingSearchService) {}

    $onChanges() {
        // No Finding selected, do nothing
        if (!this.selected || this.selected.id === this.prevSelected) return;
        this.prevSelected = this.selected.id;

        this.selectedTitle = this.selected.title.substring(0, 25) + (this.selected.title.length > 25 ? '...' : '');

        this.findingService
            .getGraphFindingProperties(this.selected.id)
            .then((res) => {
                let data: FindingNode = res.data;

                const root = this.findRoot(data, (e: Node) => e.labels.indexOf('Identifying') > -1);
                this.list = this.buildTree(root, data.nodes);

                this.filterProperties = new Set();

                this.loadingError = false;
            })
            .catch((e) => {
                this.list = [];
                this.loadingError = true;
                console.log(e);
            });
    }

    search() {
        let filter: FindingSearchNode = {
            finding: this.selected.id,
            nodes: {},
        };

        //use selected properties to for the query
        this.filterProperties.forEach((p) => {
            //split propertyIds
            let [id, property] = p.split('.');
            if (!id || !property) {
                console.error(`Error: Property ID "${p}" is not in the format 'nodeId.propertyName'.  Skipping.`);
                return;
            }

            filter.nodes[id] = filter.nodes[id] || [];
            filter.nodes[id].push(property);
        });

        if (this.filterProperties.size === 0) filter = null;
        this.loadGraphFindings({ filter });
    }

    clearFilter() {
        this.loadGraphFindings({ filter: null });
    }

    openPanel() {
        this.isopen = true;
    }
    closePanel() {
        this.isopen = false;
    }

    loadFindingChildren(nodeId: string): void | never {
        // If the nodeId doesn't match a node in the map, raise an error
        if (!this.allTreeNodes[nodeId]) throw new Error(`Node with ID ${nodeId} does not exist!`);
        // If this node's children have already been loaded, do nothing
        if (this.allTreeNodes[nodeId].children.length > 0) return;

        this.allTreeNodes[nodeId].loading = this.findingService.getGraphFindingChildren(this.selected.id, nodeId).then((data) => {
            if (!data.root) throw new Error('Expected a `root` property but found none.');

            let root = this.mapToTreeNode(data.nodes[data.root]);

            //destructure: children=list[0].children
            let [{ children }] = this.buildTree([root], data.nodes);

            this.allTreeNodes[root.id].children = children;
        });
    }

    // Property IDs should be of the form `nodeId.propertyName`
    toggleProperty(propertyId: string, selected: boolean) {
        if (selected) this.filterProperties.add(propertyId);
        else this.filterProperties.delete(propertyId);
    }

    isSelected(propertyId: string): boolean {
        return this.filterProperties.has(propertyId);
    }

    // `comparator` fn will return true if the Node meets the root conditions
    private findRoot(value: FindingNode, comparator: (e: Node) => boolean): TreeNode[] | never {
        const nodes = value.nodes;

        // Find the root node and add it to `list`
        let list = this.getValues(nodes).filter(comparator).map(this.mapToTreeNode);

        if (list.length === 0) throw new Error(`No nodes were found for the given Finding`);

        return list;
    }

    // `root`gets modified in place and contains the entire tree when complete
    private buildTree(root: TreeNode[], allNodes: { [key: string]: Node }): TreeNode[] {
        for (let node of root) {
            if (!this.allTreeNodes[node.id]) this.allTreeNodes[node.id] = node;
        }

        let queue = [...root];
        while (queue.length > 0) {
            let node = queue.shift();

            //add properties first
            let properties = [];
            if (allNodes[node.id].properties) properties = this.mapPropertiesAsTreeNodes(allNodes[node.id].properties, node.id);
            //find the children of the root node
            let children = this.mapChildrenAsTreeNodes(node.id, allNodes);
            //add to the children array
            node.children = [...properties, ...children];

            //put each node into the map
            for (let c of children) this.allTreeNodes[c.id] = c;

            //add the next level to the queue
            queue.push(...children);
        }

        return root;
    }

    private getValues(obj: object): any[] {
        let retval = [];
        for (let k of Object.keys(obj)) retval.push(obj[k]);
        return retval;
    }

    private mapToTreeNode(e: Node): TreeNode {
        const mainLabels: string[] = [];
        const extraLabels: string[] = [];
        e.labels.forEach((l) => {
            if (l.includes('_')) {
                extraLabels.push(l);
            } else {
                mainLabels.push(l);
            }
        });
        let title = mainLabels.sort().concat(extraLabels.sort()).join(', ');
        if (title.length > 150) title = title.substring(0, 100) + '...';
        return { id: e.node_id, title, children: [], expandable: true };
    }

    //modifies the `childNodes` array in-place
    private mapPropertiesAsTreeNodes(properties: object, id: string): TreeNode[] {
        let childNodes: TreeNode[] = [];

        for (let key of Object.keys(properties)) {
            let title = key;
            let description = properties[key];
            if (description.length > 150) description = description.substring(0, 100) + '...';
            childNodes.push({ id: `${id}.${key}`, title, description, children: [], expandable: false });
        }

        return childNodes;
    }

    //modifies the `childNodes` array in-place
    private mapChildrenAsTreeNodes(parentId: string, nodes: { [key: string]: Node }): TreeNode[] {
        let childNodes: TreeNode[] = [];

        let children = nodes[parentId].children;
        for (let child of children || []) {
            if (this.allTreeNodes[child]) {
                childNodes.push(this.allTreeNodes[child]);
            } else {
                let n = nodes[child];
                childNodes.push(this.mapToTreeNode(n));
            }
        }

        return childNodes;
    }
}

const bindings = {
    selected: '<',
    isGraphSearch: '<',
    loadGraphFindings: '&',
};

const FindingSearch: ng.IComponentOptions = {
    controller: FindingSearchCtrl,
    controllerAs: '$ctrl',
    templateUrl: '/app/finding/search/finding-search.component.html',
    bindings,
};

app.component('findingSearch', FindingSearch);
