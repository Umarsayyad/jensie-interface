import app from '../app';
import * as angular from 'angular';
import * as _ from 'lodash';
// TODO: Fix being able to select the same PoC on multiple Company POCs steps
// TODO: Only use one variable for the currently displayed POC; keeping $scope.currentPOC (index, used as $scope.ir.pocs[$scope.currentPOC]) and $scope.selectedPOC (actual POC object) in sync makes things unnecessarily complex
app.controller('IncidentReportingCtrl', function (
    $scope,
    $rootScope,
    $state,
    $stateParams,
    $timeout,
    requiredAddressesService,
    Customers,
    Locations,
    PointsOfContact,
    PointOfContactAddresses,
    Users,
    WizardHandler
) {
    $scope.activeType = 'me';
    $scope.meVisible = true;
    $scope.showLocations = false;
    $scope.addOrEditPOC = false;
    $scope.showAddLocations = false;
    $scope.currentPOC = 0;
    $scope.currentUSGPOC = 0;
    $scope.pocCount = 1;
    if (_.isNil($scope.data)) {
        $scope.data = {};
    }
    var pocFields;
    var locationFields;

    $scope.icons = {
        phonenumber: 'fa-phone',
        email: 'fa-envelope-o',
        physical: 'fa-map-marker',
        legal: 'fa-balance-scale',
        technical: 'fa-terminal',
        billing: 'fa-usd',
        executive: 'fa-empire',
        'USG Program Manager': 'fa-gavel',
    };

    $scope.tabs = [
        {
            heading: 'DCISE ICF',
            icon: 'fa-exclamation-triangle',
            show: function (tab) {
                return $scope.hasRole('customer');
            },
            viewName: 'dcise-icf',
            stateName: 'dcise-icf',
        },
        {
            heading: 'SUBMITTED REPORTS',
            icon: 'fa-file-text',
            show: function (tab) {
                return $scope.hasRole('customer');
            },
            viewName: 'reports',
            stateName: 'reports',
        },
    ];

    var stateChangeStartListener = $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, options) {
        if (toState.name === 'cato.loggedIn.incident_reporting') {
            var targetState = 'reports';
            _.each($scope.tabs, function (tab) {
                if (tab.show(tab)) {
                    targetState = tab.stateName;
                    return false;
                }
            });
            event.preventDefault();
            $state.go('cato.loggedIn.incident_reporting.' + targetState, {}, { reload: true });
        } else if (_.startsWith(toState.name, 'cato.loggedIn.incident_reporting.')) {
            $scope.activeTab = toState.name.split('.')[3];
        }
    });

    $scope.$on('$destroy', function () {
        stateChangeStartListener();
    });

    $scope.go = function (tab) {
        var targetState = 'cato.loggedIn.incident_reporting.' + tab.stateName;
        if ($state.includes(targetState)) {
            if ($state.is(targetState)) {
                // TODO: handle reloading a tab when clicking on the currently loaded tab, as long as the page isn't loading
            } else {
                // We are already in a child state of the target, don't do anything because the tab was likely activated while the page was still loading
                return;
            }
        }
        $state.go(targetState);
    };

    $scope.activeTab = $state.current.name.split('.')[3];
    if (typeof $scope.activeTab === 'undefined') {
        _.each($scope.tabs, function (tab) {
            if (tab.show(tab)) {
                $scope.activeTab = tab.stateName;
                return false;
            }
        });
    }

    $scope.reset = function () {
        // Incident Reporting Form
        $scope.ir = {
            follow_on_report: false,
            shared_with_other_fed_agency: false,
            pocs: [],
            USGpocs: [],
            trackingNumbers: [],
            customer: {},
            impact_defense_info: 'No',
            impact_op_support: 'No',
            incidentLocations: [],
        };
        $scope.defaulLocations = [];

        $scope.dateOptions = {
            formatYear: 'yy',
            maxDate: new Date(),
            startingDay: 1,
        };

        $scope.calendarOpened = false;
        $scope.openCalendar = function () {
            $scope.calendarOpened = true;
        };

        // ADD POCs
        $scope.config = {
            poc: {},
        };
        $scope.poc = {};
    };

    $scope.reset();

    $scope.validate = true;

    var base = '/app/incidentReporting/';

    var pocStep = {
        title: 'Company POCs (' + $scope.pocCount + ')',
        include: base + 'companyPOC.html',
        additionalButtons: base + 'addAdditionalPOC.html',
        previous: base + 'previous.html',
        next: base + 'next.html',
        order: 3,
        minHeight: '420px',
        nextDisabled: companyPOCInvalid,
        //previousDisabled: companyPOCInvalid,
    };

    $scope.wizardSteps = [
        {
            title: 'File an Incident Report',
            include: base + 'general.html',
            previous: base + 'start-over.html',
            next: base + 'next.html',
            order: 1,
            minHeight: '465px',
        },
        {
            title: 'Company Information',
            include: base + 'company.html',
            previous: base + 'previous.html',
            next: base + 'next.html',
            order: 2,
            minHeight: '465px',
        },
        pocStep,
        {
            title: 'Contract',
            include: base + 'contract.html',
            previous: base + 'previous.html',
            next: base + 'next.html',
            nextDisabled: contractPOCInvalid,
            //previousDisabled: contractPOCInvalid,
            order: 40, // TODO: this limits the maximum number of company POCs to ~37 before a company POC step and this step have the same order, which will make the company POC step be displayed at the end
        },
        {
            title: 'Incident Information',
            include: base + 'incidentInfo.html',
            previous: base + 'previous.html',
            next: base + 'finish.html',
            order: 50,
        },
    ];

    Locations.options().$promise.then(function (result) {
        locationFields = result.actions.POST;
    });

    PointsOfContact.options().$promise.then(function (result) {
        pocFields = result.actions.POST;
    });

    function checkPOC(poc, selectedPOC) {
        if (_.isNil(poc)) return true;
        var invalid = false;
        angular.forEach(pocFields, function (field, key) {
            if (field.required && (_.isNil(poc[key]) || poc[key].length === 0)) {
                invalid = true;
            }
        });
        if (requiredAddressesService.getRequired(selectedPOC.pointofcontactaddresses).length > 0) {
            invalid = true;
        }
        return invalid;
    }

    function companyPOCInvalid() {
        var selectedPOC = $scope.selectedPOC;
        var poc = $scope.ir.pocs[$scope.currentPOC];
        return checkPOC(poc, selectedPOC);
    }

    function contractPOCInvalid() {
        var selectedPOC = $scope.selectedUSGPOC;
        var poc = $scope.ir.USGpocs[$scope.currentUSGPOC];
        return checkPOC(poc, selectedPOC);
    }

    Customers.get({ customerId: $scope.user.customer }).$promise.then(function (customer) {
        $scope.customer = customer;
        $scope.ir.customer.duns = customer.duns;
        $scope.ir.customer.cagecode = customer.cagecode;
        $scope.ir.incident_cage_code = customer.cagecode;
        $scope.ir.customer.facility_clearance_level = customer.facility_clearance_level || 'Not applicable';
        $scope.ir.customer.cleared_contractor = customer.cleared_contractor || false;
    });

    $scope.addPOC = function () {
        $scope.pocCount++;

        var stepToAdd = angular.copy(pocStep);
        stepToAdd.title = 'Company POCs (' + $scope.pocCount + ')';

        stepToAdd.order = $scope.pocCount + 2;
        $scope.addOrEditPOC = false;

        //$scope.ir.pocs.push({locations: []});

        $scope.wizardSteps.splice($scope.pocCount + 1, 0, stepToAdd);
        $timeout(WizardHandler.wizard().next); // TODO: go to the newly added step instead of just the next one
    };

    $scope.removePOC = function (index) {
        // Don't allow user to delete the first POC.
        if ($scope.pocCount === 1) return;
        $scope.ir.pocs.splice($scope.currentPOC, 1);
        $scope.pocCount--;

        $scope.wizardSteps.splice(index, 1);
        WizardHandler.wizard().goTo(index - 1);
    };

    $scope.exitValidation = function (formName) {
        // TODO: Delete this function? It doesn't appear to be used.

        console.log('formName: ', formName);
        console.log('valid? ', formName.$valid);
        return (formName.$valid = true);
    };

    Locations.query({ customerId: $scope.user.customer }).$promise.then(function (locations) {
        $scope.locations = _.filter(locations, { personalLocation: false });
        $scope.data.locationMap = _.keyBy(locations, 'id');
    });

    // One for non-USG managers
    $scope.data.pocs = [{ first_name: 'Add a new POC', id: 'new' }];
    // One for USG Program Managers
    $scope.data.pocs.push({ first_name: 'Add a new POC', id: 'new', type: 'USG Program Manager' });
    Users.list().$promise.then(function (users) {
        $scope.users = users;
        $scope.data.usersSel = [];
        angular.forEach($scope.users, function (user) {
            $scope.data.usersSel.push({ label: user.first_name + ' ' + user.last_name, id: user.id, pocs: user.pocs });
        });
        PointsOfContact.list({ customerId: $scope.user.customer }).$promise.then(function (pocs) {
            angular.forEach(pocs, function (poc) {
                poc.locations = [];
                var user: any = _.find(users, { id: poc.user });
                poc.first_name = user.first_name;
                poc.last_name = user.last_name;
                $scope.data.pocs.push(poc);
            });
        });
    });

    $scope.deleteAddress = function (location) {
        var poc = $scope.ir.pocs[$scope.currentPOC];
        var pocAddr: any = _.find(poc.pointofcontactaddresses, { location: location.id });
        var pocAddrId = pocAddr.id;
        PointOfContactAddresses.delete({ customerId: location.customer, pocId: poc.id, pocAddrId: pocAddrId }).$promise.then(function (results) {
            Locations.delete({ customerId: location.customer, locationId: location.id });
            _.remove($scope.ir.pocs[$scope.currentPOC].locations, { id: location.id });
        });
    };

    $scope.setPoc = function (poc) {
        $scope.selectedPOC = poc;
        if (poc.id !== 'new') {
            $scope.ir.pocs[$scope.currentPOC] = poc;
        } else {
            $scope.addOrEditPOC = true;
            $scope.ir.pocs[$scope.currentPOC] = { locations: [] };
        }
    };

    $scope.setUSGPoc = function (poc) {
        $scope.selectedUSGPOC = poc;
        if (poc.id !== 'new') {
            $scope.ir.USGpocs[$scope.currentUSGPOC] = poc;
        } else {
            $scope.addOrEditPOC = true;
            $scope.ir.USGpocs[$scope.currentUSGPOC] = { locations: [] };
        }
    };

    $scope.filterPocsByUser = function (poc) {
        if (_.isNil($scope.poc.user)) return true;
        return $scope.poc.user.pocs.indexOf(poc.value) === -1;
    };

    $scope.filterUsersByType = function (user) {
        if (user.pocs.length === 1) return false;
        if (_.isNil($scope.poc.type)) return true;
        return user.pocs.indexOf($scope.poc.type.value) === -1;
    };

    function loadAllOptions() {
        PointsOfContact.options().$promise.then(function (results) {
            $scope.orig_config = {};
            $scope.orig_config.poc = results.actions.POST;
        });
    }

    loadAllOptions();

    $scope.$on('wizard:stepChanged', function (event, args) {
        var wizard = WizardHandler.wizard();
        var curTitle = wizard.currentStepTitle();
        if (_.includes(curTitle, 'Company POCs')) {
            $scope.config.poc = _.cloneDeep($scope.orig_config.poc);
            $scope.config.poc.type.choices = _.reject($scope.config.poc.type.choices, { value: 'USG Program Manager' });
            $scope.currentPOC = wizard.currentStepNumber() - 3;
            $scope.selectedPOC = $scope.ir.pocs[$scope.currentPOC];
        }
        if (curTitle === 'Contract') {
            $scope.config.poc = _.cloneDeep($scope.orig_config.poc);
            $scope.config.poc.type.choices = _.filter($scope.config.poc.type.choices, { value: 'USG Program Manager' });
        }
    });

    $scope.addNewPOC = function () {
        var newpoc = {
            customer: $scope.user.customer,
            user: $scope.poc.user.id,
            type: $scope.poc.type.value,
        };

        PointsOfContact.create({ customerId: $scope.user.customer }, newpoc).$promise.then(function (poc) {
            poc.locations = [];
            var user: any = _.find($scope.users, { id: poc.user });
            user.pocs.push(poc.type);
            poc.first_name = user.first_name;
            poc.last_name = user.last_name;
            poc.email = user.email;
            if (poc.type === 'USG Program Manager') {
                $scope.ir.USGpocs[$scope.currentUSGPOC] = poc;
                $scope.selectedUSGPOC = poc;
            } else {
                $scope.ir.pocs[$scope.currentPOC] = poc;
                $scope.selectedPOC = poc;
            }
            $scope.data.pocs.push(poc);
            $scope.poc = {};
        });
    };
    // END ADD POCs

    $scope.updateTitle = function () {
        // We can't actually update other user's titles...
        /*var poc = $scope.ir.pocs[$scope.currentPOC];
        if(poc.user && !_.isNil(poc.title)) {
            Users.patch({userId: poc.user}, {title: poc.title}).$promise;
        }
        */
    };

    $scope.updateDUNS = function () {
        if (!this.wizard['ir-company-duns'].$error.pattern) {
            $scope.customer.duns = $scope.ir.customer.duns;
            Customers.update({ customerId: $scope.user.customer }, $scope.customer);
        }
    };

    $scope.updateCAGE = function () {
        if (!this.wizard['ir-facility-cage'].$error.pattern) {
            $scope.customer.cagecode = $scope.ir.customer.cagecode;
            Customers.update({ customerId: $scope.user.customer }, $scope.customer);
        }
    };

    function activeUser(id) {
        if ($scope.activeUser) return $scope.activeId === $scope.activeUser.id;
    }

    function isActiveRole(activeUserRole, type) {
        if (!activeUserRole) return;
        if (activeUserRole.type === type) return 'active';
    }

    $scope.isActiveCompanyRole = function (type) {
        return isActiveRole($scope.activeCompanyPOC, type);
    };

    $scope.isActiveUSGRole = function (type) {
        return isActiveRole($scope.activeUSGPOC, type);
    };

    function isActiveId(activeUserRole, id) {
        if (activeUserRole.id === id) return 'active';
    }

    $scope.isActiveCompanyUser = function (id) {
        return isActiveId($scope.activeCompanyPOC, id);
    };

    $scope.isActiveUSGId = function (id) {
        return isActiveId($scope.activeUSGPOC, id);
    };

    $scope.notEmpty = function (obj) {
        if (!obj) return;
        return Object.keys(obj).length > 0;
    };

    $scope.setUser = function (type, user) {
        user.type = type;

        angular.forEach(user.customer.pointsofcontact, function (poc) {
            if (poc.type === type) {
                var phonenumber: any = _.find(poc.pointofcontactaddresses, { type: 'phonenumber' });
                user.telephonenumber = phonenumber !== undefined ? phonenumber.value : null;

                var email: any = _.find(poc.pointofcontactaddresses, { type: 'email' });
                user.email = email !== undefined ? email.value : null;

                var address: any = _.find(poc.pointofcontactaddresses, { type: 'physical' });
                user.address = address !== undefined ? address.value : null;
            }
        });

        if (type === 'USG Program Manager') {
            $scope.activeUSGPOC = user;
        } else {
            $scope.activeCompanyPOC = user;
        }
    };

    $scope.locAlreadyUsed = function (loc) {
        return _.find($scope.selectedPOC.pointofcontactaddresses, { id: loc.id });
    };

    $scope.locNotAlreadyUsed = function (loc) {
        return !$scope.locAlreadyUsed(loc);
    };

    $scope.incidentLocAlreadyUsed = function (loc) {
        return _.find($scope.ir.incidentLocations, { id: loc.id });
    };

    $scope.incidentLocNotAlreadyUsed = function (loc) {
        return !$scope.incidentLocAlreadyUsed(loc);
    };

    Locations.query({ customerId: $scope.user.customer }).$promise.then(function (locations) {
        $scope.locations = _.filter(locations, { personalLocation: true });
    });

    $scope.addIncidentLocation = function (location) {
        $scope.ir.incidentLocations.push(location);
    };

    $scope.removeIncidentLocation = function (location) {
        _.remove($scope.ir.incidentLocations, { id: location.id });
    };

    $scope.finishedWizard = function () {
        var ui_data = $scope.user.ui_data;
        $scope.ir.date_submitted = new Date();
        if (_.isNil(ui_data.reports)) {
            ui_data.reports = [];
        }
        ui_data.reports.push($scope.ir);
        console.log('ir: ', $scope.ir);
        Users.patch({ userId: $scope.user.id }, { ui_data: ui_data }).$promise.then(function () {
            $state.go('cato.loggedIn.incident_reporting.reports', { submitted: $scope.ir.report_name });
        });
    };

    $scope.availableContracts = [
        'EL DIABLO',
        'SHAKE N BAKE',
        'SUPERFLY',
        'MAGIC MAN',
        'LAUGHING CLOWN',
        'WONDER BREAD',
        'GIRARD',
        'WALKER',
        'TEXAS RANGER',
    ];

    $scope.contracts = ['WALKER', 'TEXAS RANGER'];
});
