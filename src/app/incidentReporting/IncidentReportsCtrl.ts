import app from '../app';
import * as angular from 'angular';
import * as _ from 'lodash';

app.controller('IncidentReportsCtrl', function ($scope, $rootScope, $state, $stateParams, Users) {
    $scope.reportsExist = false;

    Users.list().$promise.then(function (users) {
        $scope.users = users;
        angular.forEach(users, function (user) {
            if (user.ui_data.reports && user.ui_data.reports.length > 0) {
                $scope.reportsExist = true;
            }
        });
        console.log('reportsExist? ', $scope.reportsExist);
    });

    $scope.formattedJSON = function (json) {
        console.log('json: ', json);
        return JSON.stringify(json, null, '  ');
    };
});
