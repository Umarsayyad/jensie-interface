//This file allows our ng 8+ app to "attach" downgraded components/services without
//depending on the entire JS side of the application

import * as angular from 'angular';

export const appName = 'cato';
export const appModule = () => angular.module(appName);

export const mainName = 'cato.main';
export const mainModule = () => angular.module(mainName);
