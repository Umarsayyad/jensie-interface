import app from '../app';
import * as _ from 'lodash';
import * as angular from 'angular';

import '../common/PendingItemsService';
import '../customer/manage/CondensedCustomerFilterComp';
import { MatDialog } from '@angular/material/dialog';
import { AttachmentComponent } from '../../ng-app/common/components/attachment-upload/attachment.component';
import { AttachmentService } from '../../ng-app/common/services/attachment.service';

app.controller('MailboxCtrl', function (
    $scope,
    $rootScope,
    $state,
    MatDialog: MatDialog,
    AttachmentService: AttachmentService,
    MessageService,
    modalService,
    socketService,
    PendingItemsService,
    RandomColorService,
    stateBuilder,
    moment,
    Messages
) {
    $scope.config = {};
    //$scope.data = {};
    $scope.messageFormData = {};
    $scope.messageFormData.customthread = {};
    $scope.findings = {};
    $scope.showArchived = false;
    $rootScope.newItemCounts.messages = 0;

    $scope.userId = $scope.user.id;

    $scope.getInitials = MessageService.getInitials;
    $scope.getNameOrRole = MessageService.getNameOrRole;
    $scope.userIconColor = RandomColorService.userIconColor;

    function getMsgKey(msg) {
        if (_.isNil(msg)) return;
        return !_.isNil(msg.finding) ? 'f' + (msg.finding.id || msg.finding) : 't' + (msg.customthread.id || msg.customthread);
    }

    $scope.getMsgKeyFromMessageObjs = function (msgObjs) {
        var msgKey = getMsgKey(msgObjs.messages[0]);
        return msgKey;
    };

    var getMessageGroup = function (msgKey, key) {
        return $scope.data.messageObjs[msgKey][key];
    };

    var setMessageGroup = function (msgKey, key, value) {
        $scope.data.messageObjs[msgKey][key] = value;
    };

    var setMessageGroupStatus = function (status, msgKey) {
        if (status === 'unread') {
            setMessageGroup(msgKey, 'color', 'red-bg');
            setMessageGroup(msgKey, 'icon', 'fa-flag');
            setMessageGroup(msgKey, 'read', false);
            setMessageGroup(msgKey, 'tooltip', 'Unread');
            //setMessageGroup(msgKey, 'archived', false); This isn't necessarily true!
        } else if (status === 'read') {
            setMessageGroup(msgKey, 'color', 'green-bg');
            setMessageGroup(msgKey, 'icon', 'fa-check');
            setMessageGroup(msgKey, 'read', true);
            setMessageGroup(msgKey, 'tooltip', 'Read');
        } else if (status == 'noReplyWithin24hrs') {
            setMessageGroup(msgKey, 'icon', 'fa-exclamation-triangle');
            setMessageGroup(msgKey, 'color', 'red-bg');
            setMessageGroup(msgKey, 'read', false);
            setMessageGroup(msgKey, 'tooltip', 'Needs reply: Not replied to within 24 hours');
        } else if (status === 'needsReply') {
            setMessageGroup(msgKey, 'icon', 'fa-clock-o');
            setMessageGroup(msgKey, 'color', 'orange-bg');
            setMessageGroup(msgKey, 'read', true);
            setMessageGroup(msgKey, 'tooltip', 'Needs reply');
        }
    };

    $scope.isFindingThread = function (msg) {
        if (_.isNil(msg)) return false;
        if (_.isNil(msg.finding)) return false;
        //return (!_.isNil(msg.finding) && msg.finding.id != null);
        return !_.isNil(msg.finding);
    };

    function addWebsocketMessageToThread(msg, msgThreadId) {
        var relevant_customer = paramsCustomer();

        // If this message already exists, just replace the old copy
        if ($scope.data.messageObjs[msgThreadId]) {
            const existingMessageIndex = $scope.data.messageObjs[msgThreadId].messages.findIndex((item) => item.id === msg.id);
            if (existingMessageIndex >= 0) {
                $scope.data.messageObjs[msgThreadId].messages[existingMessageIndex] = msg;
                return;
            }
        }

        if (msg.relevant_customers.length || ($scope.gdata.isOperator && msg.customthread && msg.customthread.for_operators)) {
            var index = msg.relevant_customers.indexOf(relevant_customer);
            if (index > -1 || _.isUndefined(relevant_customer)) {
                $rootScope.newItemCounts.messages += 1;
                addMessageToThread(msg, msgThreadId);
                setMessageGroupStatus('unread', msgThreadId);
            }
        }
    }

    function addMessageToThread(msg, msgThreadId) {
        if ($scope.data.messageObjs[msgThreadId] === undefined) {
            $scope.data.messageObjs[msgThreadId] = {};
            $scope.data.messageObjs[msgThreadId].messages = [];
        }
        //console.log('msgThreadId: ', msgThreadId);
        //console.log('$scope.data.messageObjs[msgThreadId]: ', $scope.data.messageObjs[msgThreadId]);
        var msgKey = getMsgKey(msg);

        if ($scope.isFindingThread(msg) && !msg.finding.id) {
            // Add on the finding obj, since that is only returned when we manually fetch it for the first message in the finding thread
            var msgs = $scope.data.messageObjs[msgKey].messages;
            msg.finding = msgs[msgs.length - 1].finding;
        }
        $scope.data.messageObjs[msgThreadId].messages.push(msg);

        var selectedCustomer = paramsCustomer();
        if (selectedCustomer) {
        }

        if (getMessageGroup(msgKey, 'archived') === true) {
            setMessageGroup(msgKey, 'archived', false);
            console.log('setMessageGroup archived = false');
            getMessageGroup(msgKey, 'archived');
            $scope.showArchived = false;
        }
        setMessageGroup(msgThreadId, 'lastMessageInThread', msg.created);
    }

    var messageUnsubscribe = socketService.subscribeByModel('message', function (response) {
        //console.log('got MESSAGE data back for (MailboxCtrl): ', response);
        // Add message to thread if we know which thread it belongs to
        var msgThreadId = getMsgKey(response);
        //console.log("Thread id: ", msgThreadId);

        if (response.author.id !== $scope.user.id) {
            addWebsocketMessageToThread(response, msgThreadId);
        }
    });

    $scope.$on('$destroy', function () {
        messageUnsubscribe();
    });

    function loadOptions() {
        Messages.options().$promise.then(function (result) {
            $scope.config = result.actions.POST;
        });
    }

    loadOptions();

    function getAuthorFullNameOrInitials(message) {
        var author = $rootScope.gdata.userMap[message.author.id];
        if (author) {
            return author.first_name + ' ' + author.last_name;
        } else {
            return message.author.initials;
        }
    }

    $scope.messageAuthor = function (message) {
        return $scope.isMe(message) ? 'Me' : getAuthorFullNameOrInitials(message);
    };

    function getStatus(msgKey) {
        var d = new Date();
        var now = d.getTime();

        var msgs = $scope.data.messageObjs[msgKey].messages;
        var lastMsg = msgs[msgs.length - 1];
        if (lastMsg.state.read === false && lastMsg.author.id !== $scope.user.id) {
            // Last message was not read and replied to by a customer, and we're a operator, so mark it as no reply within last 24 hours
            if (moment().diff(moment(lastMsg.created), 'hours') >= 24 && lastMsg.author.type === 'customer' && $scope.gdata.isOperator) {
                setMessageGroupStatus('noReplyWithin24hrs', msgKey);
            } else {
                setMessageGroupStatus('unread', msgKey);
            }
        } else if (lastMsg.read !== false || lastMsg.read === undefined) {
            // Last message was not replied to by user, so mark as needs reply
            if (lastMsg.state.read && lastMsg.author.id !== $scope.user.id) {
                setMessageGroupStatus('needsReply', msgKey);
            } else {
                setMessageGroupStatus('read', msgKey);
            }
        }
        setMessageGroup(msgKey, 'lastMessageInThread', lastMsg.created);
    }

    $scope.loadMessagesByCustomer = function (customer) {
        $scope.customer = customer;
        if (!customer) {
            $scope.data.messageObjs = {};
        }
        $scope.getMessages();
    };

    $scope.$watch('user.ui_data.selectedCustomer', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.loadMessagesByCustomer(newVal);
        }
    });

    function paramsCustomer() {
        if ($scope.gdata.isOperator) {
            if ($scope.customer) {
                return $scope.customer.id;
            }
        } else {
            return $scope.user.customer;
        }
    }

    $scope.getMessages = function () {
        var params = {
            all: true,
            customer: paramsCustomer(),
        };

        if (!$scope.gdata.isOperator) {
            // Redo message count.
            $rootScope.newItemCounts.messages = 0;
        }

        $scope.messagesLoading = Messages.details(params).$promise.then(function (messages) {
            // Ensure the requested customer matches the current customer selected.
            if (params['customer'] !== paramsCustomer()) {
                return;
            }

            $scope.data.messageObjs = {};

            var prevMsgKey = null;
            var currentCreated = null;
            angular.forEach(messages, function (message) {
                var msgKey = getMsgKey(message);
                if (_.isNil($scope.data.messageObjs[msgKey])) {
                    $scope.data.messageObjs[msgKey] = {};
                    $scope.data.messageObjs[msgKey].messages = [];
                }

                $scope.data.messageObjs[msgKey].messages.push(message);
                if (message.state.hidden) {
                    setMessageGroup(msgKey, 'archived', true);
                }

                // First load of the page always gets all, so we only count when not filtering by customer.
                if (!$scope.customer && message.state.read === false && message.state.hidden === false && message.author.id !== $scope.user.id) {
                    $rootScope.newItemCounts.messages++;
                }
            });

            angular.forEach($scope.data.messageObjs, function (msgObj, msgKey) {
                getStatus(msgKey);
            });
        });
    };

    $scope.loadMessagesByCustomer(_.get($scope.user, 'ui_data.selectedCustomer'));

    $scope.msgIsMissionDirector = MessageService.msgIsMissionDirector;
    $scope.msgIsOperator = MessageService.msgIsOperator;
    $scope.isMe = MessageService.isMe;

    function collapseAllMessages() {
        angular.forEach($scope.data.messageObjs, function (msgObj) {
            angular.forEach(msgObj.messages, function (msg) {
                $scope.clearAndCollapse(msg);
            });
        });
    }

    $scope.getRecipients = function (message) {
        var recipients = '';

        if (message.customthread) {
            if (message.customthread.customer) {
                var customer = $scope.gdata.isOperator ? $rootScope.gdata.customerMap[message.customthread.customer] : $rootScope.gdata.customer;
                recipients = customer.name;
            }
            var addComma = true;
            if (message.customthread.deliver_to) {
                if (message.customthread.deliver_to === $scope.user.id) {
                    addComma = false;
                } else {
                    var user = $rootScope.gdata.userMap[message.customthread.deliver_to];
                    if (_.isUndefined(user)) {
                        console.log('Unknown recipient specified: ', message.customthread.deliver_to);
                    } else {
                        recipients = user.first_name + ' ' + user.last_name;
                    }
                }
            }
            if (message.customthread.for_operators) {
                recipients += addComma ? ', Operators' : 'Operators';
            }
        } else {
            if ($scope.gdata.isOperator) {
                customer = $rootScope.gdata.customerMap[message.finding.customer];
            } else {
                customer = $rootScope.gdata.customer;
            }
            if (typeof customer !== 'undefined') {
                recipients = customer.name + ', Operators';
            }
        }
        return recipients;
    };

    $scope.compose = false;
    $scope.addNewMessage = function () {
        $scope.compose = true;
        $scope.messageFormData = {};
        collapseAllMessages();
        $('html, body').animate({ scrollTop: 0 }, 'fast');
    };

    function addMessage(message, fromCompose) {
        Messages.addMessage(message).$promise.then((msgResp) => {
            let msg = msgResp;
            msg['author'] = $scope.user;
            let msgThreadId = getMsgKey(msgResp);
            if ($scope.messageFormData.queuedAttachments) {
                for (const attachment of $scope.messageFormData.queuedAttachments) {
                    AttachmentService.uploadAttachment(attachment, msg.id)
                        .toPromise()
                        .then((attachmentRes) => {
                            msg.attachments.push(attachmentRes);
                            console.log(attachmentRes);
                        });
                }
            }
            addMessageToThread(msg, msgThreadId);
            setMessageGroupStatus('read', msgThreadId);
            $scope.messageFormData = {};
            if (fromCompose) {
                $scope.compose = false;
            }
            $('html, body').animate({ scrollTop: 0 }, 'fast');
        });
    }

    $scope.uploadAttachment = function () {
        MatDialog.open(AttachmentComponent)
            .afterClosed()
            .subscribe((queuedAttachments: File[]) => {
                ($scope.messageFormData.queuedAttachments = $scope.messageFormData.queuedAttachments || []).push(...queuedAttachments);
            });
    };

    $scope.submitNewMessage = function (msgObj, fromCompose) {
        console.log('submit new message: ', msgObj, fromCompose);
        let msg = _.clone($scope.messageFormData);

        if (!_.isNil(msgObj)) {
            let msgThreadId = getMsgKey(msgObj);

            if ($scope.isFindingThread(msgObj)) {
                msg['finding'] = msgObj.finding.id;
                delete msg.customthread;
            } else {
                msg['customthread'] = msgObj.customthread.id;
            }
        }

        if (fromCompose) {
            // Create new customthread
            var customthread = {
                title: msg.title,
            };
            delete msg.finding;
            if (msg.customer && _.isNil(msg.user)) {
                // Deliver to the selected customer (all users)
                customthread['customer'] = msg.customer.id;
            } else {
                if (msg.user.for_operators) {
                    // Send to all operators (but also include the user on the thread)
                    if (msg.user.id) {
                        customthread['deliver_to'] = msg.user.id;
                    } else {
                        if (!msg.user.customer) {
                            customthread['deliver_to'] = $scope.user.id;
                        } else {
                            customthread['customer'] = msg.user.customer.id;
                        }
                    }
                    customthread['for_operators'] = msg.user.for_operators;
                } else if (msg.user.isCustomer) {
                    // Leaving this in for now -- but no longer used in present case
                    // Deliver to everyone in the company
                    customthread['customer'] = msg.user.customer.id;
                    customthread['for_operators'] = msg.user.for_operators;
                } else {
                    // Send to just the specified user
                    customthread['deliver_to'] = msg.user.id;
                    // If a customer specifies that it's for a specific user, don't show it to operators
                    if ($scope.gdata.isCustomer) customthread['for_operators'] = false;
                }
            }
            msg['customthread'] = customthread;
        }

        addMessage(msg, fromCompose);
    };

    $scope.markAllAsReadWithMsgObj = function (msgObj) {
        if (_.isNil(msgObj)) return; // Compose for new msg case

        // Message Thread
        var msgKey = getMsgKey(msgObj.messages[0]);
        angular.forEach(msgObj.messages, function (message) {
            if (message.state.read !== true) {
                $scope.markAsRead(message.id);
            }
            var msgKey = getMsgKey(message);
            if (message.author.id !== $scope.user.id) {
                setMessageGroupStatus('needsReply', msgKey);
            } else {
                setMessageGroupStatus('read', msgKey);
            }
            message.state.read = true;
        });

        // Single message Object
        if (_.isNil(msgObj.messages)) {
            msgKey = $scope.getMsgKeyFromObj(msgObj);
            if (msgObj.state.read !== true) {
                $scope.markAsRead(msgObj.id);
            }
            if (msgObj.author.id !== $scope.user.id) {
                setMessageGroupStatus('needsReply', msgKey);
            } else {
                setMessageGroupStatus('read', msgKey);
            }
            msgObj.state.read = true;
        }
    };

    function subMessages() {
        if ($rootScope.newItemCounts.messages > 0) $rootScope.newItemCounts.messages--;
    }

    $scope.markAllAsRead = function (msgKey) {
        if (_.isNil(msgKey)) return; // Compose for new message case
        $scope.markAllAsReadWithMsgObj($scope.data.messageObjs[msgKey]);
    };

    $scope.markAsRead = function (msg_id) {
        Messages.setRead(
            { messageId: msg_id },
            { reader: $scope.user.id },
            function (state) {
                PendingItemsService.markMessageRead(state.message);
                subMessages();
            },
            modalService.openErrorResponseCB('Error marking message as read')
        );
    };

    $scope.markAsUnRead = function (msg_id, read) {
        Messages.setUnRead(
            { messageId: msg_id },
            { reader: $scope.user.id },
            function (state) {
                PendingItemsService.markMessageUnread(state.message);
                $rootScope.newItemCounts.messages++;
            },
            modalService.openErrorResponseCB('Error marking message as unread')
        );
    };

    $scope.markAllAsUnRead = function (msgKey) {
        if (_.isNil(msgKey)) return; // Compose for new message case
        $scope.markAllAsUnReadWithMsgObj($scope.data.messageObjs[msgKey]);
    };

    $scope.markAllAsUnReadWithMsgObj = function (msgObj) {
        var msgKey = getMsgKey(msgObj.messages[0]);
        angular.forEach(msgObj.messages, function (message) {
            if (message.author.id !== $scope.user.id) {
                $scope.markAsUnRead(message.id);
                message.state.read = false;
                setMessageGroupStatus('unread', msgKey);
            }
        });
    };

    $scope.markAllArchived = function (msgObj, archive_status) {
        var msgKey = getMsgKey(msgObj.messages[0]);
        angular.forEach(msgObj.messages, function (message) {
            $scope.markArchived(msgKey, message, archive_status);
        });
    };

    $scope.markArchived = function (msgKey, message, archive_status) {
        // TODO: Fix changing counts for own messages and before knowing if the update succeeded
        if (archive_status) {
            Messages.setArchived({ messageId: message.id }, { read: message.state.read }).$promise;
            subMessages();
        } else {
            Messages.setUnArchived({ messageId: message.id }, { read: message.state.read }).$promise;
            $rootScope.newItemCounts.messages++;
        }
        setMessageGroup(msgKey, 'archived', archive_status);
    };

    $scope.toggleChildMessages = function (msgKey) {
        if (msgKey !== undefined) {
            $scope.messageFormData = {};
            $scope.messages = $scope.data.messageObjs[msgKey].messages;
            $scope.message = $scope.messages[0];
            if ($scope.compose) {
                $scope.compose = false;
            }

            if (getMessageGroup(msgKey, 'visible') === true) {
                setMessageGroup(msgKey, 'visible', false);
            } else {
                setMessageGroup(msgKey, 'visible', true);
                $scope.markAllAsRead(msgKey);
            }

            // Scroll to the location of the expanded item
            //$anchorScroll();
        } else {
            var msgKey = null;
        }

        if ($scope.previousId && $scope.previousId != msgKey) {
            setMessageGroup($scope.previousId, 'visible', false);
        }

        $scope.previousId = msgKey;

        // Todo: fix this?
        if ($state.is('cato.loggedIn.dashboard.messages.finding')) {
            $state.go('^');
        }
    };

    $scope.hideArchived = function () {
        $scope.showArchived = false;
    };

    $scope.archived = function () {
        $scope.showArchived = true;
    };

    $scope.clearAndCollapse = function (msgObj) {
        $scope.messageFormData = {};
        // Compose new threaded message case
        if (_.isNil(msgObj)) {
            $scope.compose = false;
        } else {
            var msgKey = getMsgKey(msgObj);
            setMessageGroup(msgKey, 'visible', false);
        }
    };

    $scope.composeInvalid = function (form) {
        if (_.isNil(form)) return true;
        if ($scope.gdata.isOperator) {
            if (!form.customer && !form.user) return true;
        }
        if (!form.title || !form.value) return true;
    };
});
