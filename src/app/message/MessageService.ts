import app from '../app';
import * as _ from 'lodash';

app.service('MessageService', function ($rootScope) {
    function getInitials(userId) {
        var user = $rootScope.gdata.userMap[userId];
        if (user && user.first_name) return user.first_name[0] + user.last_name[0];
        if (user && $rootScope.gdata.isOperator && user.roles.indexOf('administrator') > -1) return 'A';
        return 'OP';
    }

    function getNameOrInitials(message) {
        if (message.author.first_name) {
            return message.author.first_name + ' ' + message.author.last_name;
        } else {
            return message.author.initials + ' (' + message.author.username + ')';
        }
    }

    function getNameOrRole(message) {
        var user = $rootScope.gdata.userMap[message.author.id];
        if (user) return user.first_name + ' ' + user.last_name;
        else {
            if (message.author.roles.indexOf('mission_director') > -1) return 'Mission Director';
            else return 'Operator';
        }
    }

    // Should only be called by Operators or higher
    function getNameOrRoleFromId(id) {
        var user = $rootScope.gdata.userMap[id];
        if (user) {
            if (user.first_name && user.last_name) return user.first_name + ' ' + user.last_name;
            if (user.first_name) return user.first_name;
            if (user.last_name) return user.last_name;
            return user.username;
        }
        return 'Operator';
    }

    function isMe(message) {
        if (_.isNil(message.author)) return true;
        return message.author.id === $rootScope.user.id;
    }

    function msgIsOperator(message) {
        return message.author.roles.includes('operator') && !message.author.roles.includes('mission_director');
    }

    function msgIsMissionDirector(message) {
        return message.author.roles.includes('mission_director');
    }

    return {
        getInitials: getInitials,
        getNameOrInitials: getNameOrInitials,
        getNameOrRole: getNameOrRole,
        getNameOrRoleFromId: getNameOrRoleFromId,
        isMe: isMe,
        msgIsOperator: msgIsOperator,
        msgIsMissionDirector: msgIsMissionDirector,
    };
});
