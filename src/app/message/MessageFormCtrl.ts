import app from '../app';
import * as _ from 'lodash';
import { AttachmentService } from '../../ng-app/common/services/attachment.service';
import { MatDialog } from '@angular/material/dialog';
import { AttachmentComponent } from '../../ng-app/common/components/attachment-upload/attachment.component';

app.controller('MessageFormCtrl', function (
    $scope,
    $rootScope,
    $state,
    $stateParams,
    MatDialog: MatDialog,
    AttachmentService: AttachmentService,
    MessageService,
    modalService,
    Findings,
    Messages
) {
    $scope.config = $scope.gdata.messageOptions;
    $scope.messageFormData = {};
    if (_.isNil($scope.data.messages)) {
        $scope.data.messages = [];
    }

    var messageParent; // = $state.$current.data.messageParent;

    $scope.uploadAttachment = function () {
        MatDialog.open(AttachmentComponent)
            .afterClosed()
            .subscribe((queuedAttachments: File[]) => {
                ($scope.messageFormData.queuedAttachments = $scope.messageFormData.queuedAttachments || []).push(...queuedAttachments);
            });
    };

    $scope.submitNewMessage = function () {
        var msg = $scope.messageFormData;
        //console.log($state.params);
        //console.log('msg', msg);
        messageParent = $state.current.data.messageParent;
        //console.log('messageParent: ', messageParent);

        if (messageParent == 'message') {
            console.log('messages service');
        } else if (messageParent == 'campaign') {
            msg['campaign'] = $state.params.campaign_id;
            /*} else if (messageParent == 'operation') {
                msg['operation_id'] = $state.params.operation_id;*/
        } else if (messageParent == 'finding') {
            if ($state.params.finding_id && $state.params.finding_id.length > 0) {
                msg['finding'] = $state.params.finding_id;
            } else {
                msg['finding'] = $state.current.data.findingId;
            }
        }

        Messages.addMessage(msg)
            .$promise.then((message) => {
                if ($scope.messageFormData.queuedAttachments) {
                    for (const attachment of $scope.messageFormData.queuedAttachments) {
                        AttachmentService.uploadAttachment(attachment, message.id)
                            .toPromise()
                            .then((attachmentRes) => {
                                message.attachments.push(attachmentRes);
                                console.log(attachmentRes);
                            });
                    }
                }
                /* so we set up message.author as a clone of ourselves. */
                message.author = _.clone($rootScope.user);
                message.author.initials = message.author.first_name.substr(0, 1).toUpperCase() + message.author.last_name.substr(0, 1).toUpperCase();
                $scope.data.messages.push(message);
                //console.log('$scope.data.messages: ', $scope.data.messages);
                $scope.messageFormData.value = '';
                var container = $('.chat-discussion');
                container.animate({
                    scrollTop: container[0].scrollHeight,
                });
            })
            .catch((res) => {
                modalService.openError('Message could not be sent. <br/>' + res.error);
            });
    };
});
