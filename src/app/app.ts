import { config } from '@cato/config';
import { appName } from './cato.module';

import * as angular from 'angular';
import * as _ from 'lodash';

import 'angular-ui-bootstrap';
import 'angular-bootstrap-contextmenu';

import 'angular-animate';
import 'angular-sanitize';

import 'graphlib';

import 'angular-busy';
import 'angular-nvd3';
import 'ace-builds';
import 'angular-ui-ace';

import uiRouter from '@uirouter/angularjs';
import { upgradeModule } from '@uirouter/angular-hybrid';
import '@uirouter/angularjs/release/stateEvents';
import 'angular-ui-router-title';

import 'angular-resource';
import 'bootstrap';
import 'angular-xeditable';
import 'ng-file-upload';

import 'chart.js';
import 'tc-angular-chartjs';

import 'moment';
import 'angular-moment';
import 'angular-filter';
import 'angular-google-chart';
import 'angular-gridster';
import 'ui-select';
import 'angular-wizard';
import 'angular-bootstrap-datetimepicker';
// import 'angular-bootstrap-datetimepicker-templates';     //what?
import 'ng-tags-input';
import 'zingchart';
import 'zingchart-angularjs';
import 'angularjs-slider';
import 'ng-password-strength';
import 'selection-model';
import 'angular-file-saver';

import 'angular-multi-step-form';

import 'ng-sortable';
import 'angular-qr';
import 'angular-jsoneditor';
import 'angular-pretty-xml';

import 'highlightjs';
import 'angular-highlightjs';

import 'angularjs-gauge';
import 'angular-signature';
import 'angular-ui-tree';

import './common/resources';

const app: angular.IModule = angular.module(appName, [
    'ngAnimate',
    'ngSanitize',
    uiRouter,
    upgradeModule.name,
    'ui.router.state.events',
    'ui.bootstrap',
    'cgBusy',
    'nvd3',
    'ui.ace',
    'ngTable',
    'ui.router.title',
    'xeditable',
    'ngFileUpload',
    'tc.chartjs',
    'angularMoment',
    'angular.filter',
    'googlechart',
    'gridster',
    'ui.select',
    'mgo-angular-wizard',
    'ui.bootstrap.datetimepicker',
    'ngTagsInput',
    'zingchart-angularjs',
    'rzModule',
    'as.sortable',
    'ngPasswordStrength',
    'selectionModel',
    'ngFileSaver',
    'multiStepForm', //'angular-multi-step-form'
    'ja.qr',
    'angular-jsoneditor',
    'prettyXml',
    'hljs',
    'angularjs-gauge',
    'signature',
    'ui.bootstrap.contextMenu',
    'ui.tree',
    //////////////////////////
    'cato.resources',
]);

app.constant('ROE_STATE', 'cato.loggedIn.interactiveRoe');

app.config(($httpProvider, $provide) => {
    $provide.factory('notAuthenticatedInterceptor', function ($q, $injector, $rootScope, $location) {
        function safeResponse(response) {
            var res;
            if (_.isUndefined(response.data)) {
                res = response;
            } else if (_.isString(response.data)) {
                res = { error: response.data };
            } else if (_.isObject(response.data)) {
                res = response.data;
            } else {
                res = response;
            }
            if (response.status >= 400 && !('_status' in res)) {
                Object.defineProperty(res, '_status', {
                    value: response.status,
                    enumerable: false,
                    configurable: false,
                    writable: false,
                });
            }
            return res;
        }

        /* warning:
         * This makes use of some convoluted logic with resolving and replacing promises in different function
         * scopes to avoid opening multiple 2FA verification modals when multiple ultimately-failing requests are
         * made simultaneously, yet opening a new modal when the verification times out again or if the current
         * modal is closed without completing the verification
         * This may be hard to follow if you don't have a good understanding Angular's deferred/promise APIs
         */
        var doing2FA = {
            p: $q.defer(),
            initial: true,
        };

        // From https://stackoverflow.com/a/29271945
        function retryHttpRequest(config, deferred) {
            function successCallback(response) {
                deferred.resolve(response);
                doing2FA.p = $q.defer();
                doing2FA.initial = true;
            }

            function errorCallback(response) {
                deferred.reject(response.data ? response.data : response);
                doing2FA.p = $q.defer();
                doing2FA.initial = true;
            }

            var $http = $injector.get('$http');
            $http(config).then(successCallback, errorCallback);
        }

        return {
            responseError: function (response) {
                var $state = $injector.get('$state');
                var modalService;

                if (response.config && response.config.ignoreErrors) {
                    return $q.reject(safeResponse(response));
                }
                // Uh oh...can't reach the API
                /*if(_.contains([404, 502], response.status) && response.config && response.config.url == '/'+config.apiPath+'/auth/user') {
                 $state.go('dp.maintenance');
                 return;
                 } else */
                if (response.status === 403 && _.get(response, 'data.detail') === '2FA verification required.') {
                    if (doing2FA.initial) {
                        doing2FA.initial = false;
                        doing2FA.p.reject('initial');
                    }
                    var deferred = $q.defer();

                    var newDoing2FA = $q.defer();
                    doing2FA.p.promise.then(
                        function () {
                            retryHttpRequest(response.config, deferred);
                        },
                        function (message) {
                            if (message === 'cancel') return;
                            var $uibModal = $injector.get('$uibModal');
                            var modalInstance = $uibModal.open({
                                templateUrl: '/app/common/twofactor/modalTwoFactorVerification.html',
                                controller: 'TwoFactorVerificationCtrl',
                                keyboard: true,
                            });
                            modalInstance.result.then(
                                function () {
                                    newDoing2FA.resolve();
                                    retryHttpRequest(response.config, deferred);
                                },
                                function () {
                                    console.debug('2FA modal was closed, any pending requests will be lost');

                                    newDoing2FA.reject('cancel'); // Abort any other pending requests
                                    // Reset doing2FA since we won't be retrying the request
                                    doing2FA.p = $q.defer();
                                    doing2FA.initial = true;

                                    deferred.reject(safeResponse(response));
                                }
                            );
                        }
                    );

                    doing2FA.p = newDoing2FA;

                    return deferred.promise;
                }
                // Ignore errors to /user/me from login state
                else if ($state.is('cato.login') && response.config && response.config.url === config.apiPath + 'user/me') {
                    // ignore and reject below
                }
                // Bad authentication credentials
                else if (response.config && response.config.url === $rootScope.restUrl + '/auth/login') {
                    modalService = $injector.get('modalService');
                    modalService.openError('Error!', response.data.message);
                    alert('Error: ' + response.data.message);
                }
                // User is no longer logged in
                else if (response.status === 401 && response.config.url !== config.apiPath + 'user/0/ws_auth') {
                    if ($rootScope.user) {
                        $rootScope.user = undefined;
                    }
                    $rootScope.loginEvaluated = false;
                    $rootScope.sessionEvaluated = false;
                    $injector.get('$uibModalStack').dismissAll();

                    $state.go('cato.login', {
                        redirectTo: $location.url(),
                    });
                }
                // Forbidden access
                else if (response.status === 403) {
                    modalService = $injector.get('modalService');
                    modalService.openError('Error!', 'Permission Denied!');
                    //alert("Error: Permission Denied!");
                    return $q.reject(safeResponse(response));
                } else if (response.status === 404) {
                    modalService = $injector.get('modalService');
                    modalService.openError('Error!', 'Cannot resolve request!');
                    //alert("Error: Cannot resolve request!");
                }

                return $q.reject(safeResponse(response));
            },
        };
    });

    $httpProvider.interceptors.push('notAuthenticatedInterceptor');
    $provide.factory('defaultHttpResponseTransform', function () {
        return $httpProvider.defaults.transformResponse[0];
    });
});

export default app;
