import app from '../app';
import * as angular from 'angular';
import * as _ from 'lodash';

import { renameDuplicate } from '../common/utilities/rename-duplicate';

import '../campaign/AddCampaignTemplateCtrl';
import '../common/actionsDropdownComp';

app.controller('CampaignTemplateCtrl', function (
    $scope,
    $state,
    $rootScope,
    $uibModal,
    FileSaver,
    Blob,
    modalService,
    TargetService,
    CampaignTemplates,
    OperationTemplates
) {
    $scope.data.selectedCTemplates = [];
    $scope.getTargetTypeIcon = TargetService.getTargetTypeIcon;

    $scope.campaignTemplateConfig = CampaignTemplates.options();

    $scope.data.campaignTemplates = CampaignTemplates.list();

    function reloadCampaignTemplates() {
        $scope.data.campaignTemplates.$promise = CampaignTemplates.list(function (campaignTemplates) {
            $scope.data.campaignTemplates = campaignTemplates;
        }).$promise;
    }

    $scope.actions = {
        editCampaignTemplate: {
            action: function (template) {
                $scope.editCampaignTemplate(template, false);
            },
            class: 'btn-white',
            icon: 'fa-pencil',
            label: 'Edit',
            tooltip: 'Edit Campaign Template',
        },
        copyCampaignTemplate: {
            action: function (template) {
                $scope.editCampaignTemplate(template, true);
            },
            class: 'btn-white',
            icon: 'fa-copy',
            label: 'Duplicate',
            tooltip: 'Duplicate Campaign Template',
        },
        spawnCampaign: {
            action: function (template) {
                $state.go('.addCampaign', { template: template, templateId: template.id });
            },
            class: 'btn-white',
            icon: 'fa-play-circle',
            label: 'Add',
            tooltip: 'Spawn Campaign',
        },
        deleteCampaignTemplate: {
            action: function (template) {
                $scope.deleteCampaignTemplate(template);
            },
            class: 'btn-white',
            icon: 'fa-trash',
            label: 'Delete',
            tooltip: 'Delete this campaign',
        },
    };

    $scope.importTemplates = function ($file) {
        if ($file !== null) {
            var reader = new FileReader();
            reader.addEventListener('loadend', function () {
                function displayImportResults(res) {
                    $uibModal.open({
                        templateUrl: '/app/md/planning/modalImportResult.html',
                        controller: function ($scope, $uibModalInstance) {
                            $scope.title = 'Import Result';
                            $scope.logmsgs = res.log;

                            $scope.errors = _.map(res.errors, function (err) {
                                if (!_.isString(err)) {
                                    return angular.toJson(err);
                                }
                                return err;
                            });

                            $scope.ok = function () {
                                $uibModalInstance.dismiss('ok');
                            };
                        },
                        size: 'lg',
                        scope: $scope,
                    });
                }

                CampaignTemplates.importTemplates(
                    reader.result,
                    function (res) {
                        displayImportResults(res);
                        reloadCampaignTemplates();
                    },
                    function (err) {
                        displayImportResults(err);
                    }
                );
            });
            reader.readAsText($file);
        }
    };

    $scope.exportTemplates = function () {
        if ($scope.data.selectedCTemplates.length > 0) {
            var ids = _.map($scope.data.selectedCTemplates, 'id');
            CampaignTemplates.exportTemplates(
                {},
                ids,
                function (res) {
                    var data = new Blob([angular.toJson(res, true)], { type: 'application/json;charset=utf-8' });
                    FileSaver.saveAs(data, 'export.json');
                },
                function (err) {
                    console.log('export error: ', err);
                    modalService.openErrorResponse('Export Error', err, false);
                }
            );
        }
    };

    $scope.$watch('data.selectedCTemplates.length', function (newValue, oldValue) {
        $scope.allSelected = newValue !== 0 && newValue === $scope.data.campaignTemplates.length;
    });

    $scope.deleteCampaignTemplate = function (template) {
        modalService.openDelete('Confirmation', "campaign template '" + template.title + "'", {}, function () {
            CampaignTemplates.delete({ templateId: template.id }, {}, function () {
                _.remove($scope.data.campaignTemplates, { id: template.id });
            });
        });
    };

    $scope.editCampaignTemplate = function (template, copy) {
        console.log('Editing template', template);

        if (copy && template) {
            template = angular.copy(template);
            template.title = renameDuplicate(template.title);
            delete template.id;
        }

        var scope = $rootScope.$new(true);
        scope['template'] = template;
        scope['campaignTemplateSaved'] = campaignTemplateSaved;

        $uibModal.open({
            templateUrl: '/app/campaign/modalAddCampaignTemplate.html',
            controller: 'AddCampaignTemplateCtrl',
            controllerAs: 'vm',
            bindToController: true,
            size: 'lg',
            scope: scope,
            resolve: {
                operationTemplates: OperationTemplates.details().$promise,
                campaignTemplateConfig: $scope.campaignTemplateConfig.$promise,
            },
        });
    };

    function campaignTemplateSaved(ctemplate) {
        var curTemplate = _.find($scope.data.campaignTemplates, { id: ctemplate.id });
        if (_.isUndefined(curTemplate)) {
            // the campaign template is new
            $scope.data.campaignTemplates.push(ctemplate);
        } else {
            $scope.updateObj(curTemplate, ctemplate);
        }
    }
});
