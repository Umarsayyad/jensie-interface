import app from '../app';
import * as _ from 'lodash';
import * as angular from 'angular';

app.controller('AddCampaignTemplateCtrl', function (
    $scope,
    $state,
    $uibModal,
    $sce,
    $uibModalInstance,
    modalService,
    globalDataService,
    Services,
    OperationTemplates,
    CampaignTemplates,
    operationTemplates,
    campaignTemplateConfig
) {
    var vm = this;
    vm.gdata = globalDataService;
    vm.operationTemplates = operationTemplates;
    vm.campaignTemplateConfig = campaignTemplateConfig;
    vm.pickedTemplates = [];
    vm.templateKeyTooltip = $sce.trustAsHtml(
        'Name prefixes:<br>A: contains automated tasks<br>M: contains manual tasks<br>B: contains both automated and manual tasks<br>E: empty, contains no tasks'
    );

    _.each(vm.operationTemplates, function (opTemplate) {
        var processes = {
            automated: false,
            manual: false,
        };
        _.each(opTemplate.tasktemplates, function (task) {
            processes[task.process] = true;
        });
        if (processes.automated && processes.manual) {
            opTemplate.displayName = 'B: ' + opTemplate.name;
        } else if (processes.automated) {
            opTemplate.displayName = 'A: ' + opTemplate.name;
        } else if (processes.manual) {
            opTemplate.displayName = 'M: ' + opTemplate.name;
        } else {
            opTemplate.displayName = 'E: ' + opTemplate.name;
        }
    });

    if (_.isUndefined(vm.template)) {
        vm.modalTitle = 'ADD CAMPAIGN TEMPLATE';
        vm.template = {
            title: '',
            operationTemplates: [],
        };
    } else {
        vm.modalTitle = 'EDIT CAMPAIGN TEMPLATE';
        vm.template = angular.copy(vm.template);
    }

    vm.operationTemplateIdMap = _(vm.operationTemplates).keyBy('id').value();

    $scope.$watch('vm.template.types', function (pickedTargets) {
        vm.pickedTemplates = _.filter(vm.operationTemplates, function (x) {
            return _.difference(pickedTargets, x.targettypes).length === 0;
        });
    });

    vm.addOrUpdateCamapignTemplate = function () {
        var sendTemplate = _.pick(vm.template, _.keys(vm.campaignTemplateConfig));

        if (_.isUndefined(vm.template.id)) {
            CampaignTemplates.addTemplate(
                sendTemplate,
                function (template) {
                    vm.campaignTemplateSaved(template);
                    $uibModalInstance.close(template);
                },
                function (result) {
                    modalService.openErrorResponse('Campaign Template could not be saved.', result, false);
                }
            );
        } else {
            CampaignTemplates.updateTemplate(
                vm.template,
                function (template) {
                    vm.campaignTemplateSaved(template);
                    $uibModalInstance.close(template);
                },
                function (result) {
                    modalService.openErrorResponse('Campaign Template could not be saved.', result, false);
                }
            );
        }
    };

    vm.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});
