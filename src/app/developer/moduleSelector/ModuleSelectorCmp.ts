import app from '../../app';
import * as angular from 'angular';

import { IGeneralUtils } from '../../common/generalUtils';
import { renameDuplicateFile } from '../../common/utilities/rename-duplicate';

import '../../common/generalUtils';

app.component('moduleSelector', {
    bindings: {
        saveToDb: '&',
        updateCurrentModule: '&',
        newModuleContent: '<',
        currentModule: '<',
        currentModuleType: '<',
        unsavedChanges: '<',
        newModuleName: '<',
        creatingNewModuleComplete: '<',
        parentContext: '<',
        updateCurrentModuleName: '&',
        downloadToDisk: '&',
        updateModulesFlag: '<',
        selectedModules: '<',
        detectChanges: '<',
    },
    templateUrl: '/app/developer/moduleSelector/moduleSelectorTmpl.html',
    controller: class moduleSelectorCtrl {
        gdata: any;
        modules: any[]; //TODO: Come back and make a module type to use here
        submission: any;
        saveToDb: Function;
        updateCurrentModule: Function;
        newModuleContent: string; //Used to provide the content of uploaded modules
        previousName: string;
        creatingNewModule: boolean;
        creatingNewModuleComplete: Function;
        selected = 'None';
        currentModule: any;
        currentModuleType: { index: number; label: string };
        stopSizingModule: Function;
        unsavedChanges: boolean;
        newModuleName: string;
        parentContext: any;
        $timeout: Function;
        updateCurrentModuleName: Function;
        downloadToDisk: Function;
        $parent: any; //Added to shut the IDE up.
        module: any; //Added to shut the IDE up.
        updateModulesFlag: number;
        detectChanges: Function;

        // Inject dependencies.
        static $inject = ['GeneralUtils', 'globalDataService', 'AnalysisModule', 'modalService', '$timeout', 'moment'];

        constructor(
            public GeneralUtils: IGeneralUtils,
            public globalDataService: any,
            public AnalysisModule: any,
            public modalService: any,
            $timeout: Function,
            public moment: Function
        ) {
            this.gdata = globalDataService;
            this.modalService = modalService;
            this.previousName = '';
            this.modules = [];
            this.$timeout = $timeout;
        }

        $onInit() {
            this.stopSizingModule = this.GeneralUtils.setDivHeight('#moduleSelector', 253);

            this.updateModulesList(() => {
                this.createNewModule();
            });
        }

        $onChanges(changes) {
            let hasNewModuleName =
                changes.newModuleName !== undefined &&
                changes.newModuleName.currentValue !== undefined &&
                changes.newModuleName.currentValue !== changes.newModuleName.previousValue;

            let needToUpdateModulesList =
                changes.updateModulesFlag && changes.updateModulesFlag.currentValue != changes.updateModulesFlag.previousValue;

            //The second test is needed because this thing can run before the context is done initializing
            if (hasNewModuleName && this.modules !== undefined) {
                this.findAndRemoveBlankModule();
                this.createNewModule(changes.newModuleName.currentValue, this.newModuleContent);
            } else if (needToUpdateModulesList) {
                this.updateModulesList();
            }
        }

        $onDestroy() {}

        menuOptions = [
            {
                text: 'Approve/Disapprove',
                click: (event) => {
                    this.approveModule(event.module);
                },
                displayed: this.isMissionDirector,
            },
            {
                text: 'Copy',
                click: (event) => {
                    this.copyModule(event.module);
                },
            },
            {
                text: 'Delete',
                click: (event) => {
                    let module = event.module;
                    // let index = event.$index;
                    this.modalService.openDelete('Confirmation', "Module '" + module.name + "'", module, () => {
                        this.AnalysisModule.delete(
                            { analysisModuleId: module.id },
                            module,
                            () => {
                                this.updateModulesList();

                                //TODO: The code will be helpful when the modules list is presorted.
                                // if (index < this.modules.length - 1) {
                                //     this.selectModuleToDisplay(this.modules[index + 1], index);
                                // } else {
                                //     this.selectModuleToDisplay(this.modules[index - 1], index);
                                // }
                            },
                            this.modalService.openErrorResponseCB('Error Deleting Module')
                        );
                    });
                    return true;
                },
                displayed: (context) => {
                    return this.gdata.isMissionDirector || !context.module.approved;
                },
            },
            {
                text: 'Details',
                click: (event) => {
                    this.displayModuleDetails(event.module);
                },
            },
            {
                text: 'Download',
                click: (event) => {
                    this.downloadToDisk({ module: event.module });
                },
            },
            {
                text: 'Rename',
                click: (event) => {
                    this.enableRenaming(event.module);
                },
                displayed: (context) => {
                    return this.gdata.isMissionDirector || !context.module.approved;
                },
            },
        ];

        isMissionDirector() {
            return this.$parent.$ctrl.gdata.isMissionDirector;
        }

        approveModule(moduleToApprove: any) {
            this.AnalysisModule.update({
                approved: !moduleToApprove.approved,
                id: moduleToApprove.id,
            }).$promise.then(
                () => {
                    this.updateModulesList();
                },
                (error) => {
                    this.modalService.openErrorResponse(this.currentModule.name + ' cannot be approved', error, false);
                }
            );
        }

        disableRenaming(module: any) {
            module.renaming = false;
            this.creatingNewModule = false;

            this.AnalysisModule.update({
                name: module.name,
                id: module.id,
            }).$promise.then(
                () => {
                    this.updateModulesList();
                },
                (error) => {
                    this.modalService.openErrorResponse(module.name + ' cannot be renamed', error, false);
                }
            );
        }

        displayModuleDetails(module: any) {
            let details = 'Author: ' + this.gdata.userMap[module.user].username;
            details += '<br><br>Created: ' + this.moment(module.added).format('MMMM Do YYYY, h:mm:ss a');
            details += '<br><br>Last edited: ' + this.moment(module.last_updated).format('MMMM Do YYYY, h:mm:ss a');
            this.modalService.openSuccess('Module Details', details, false);
        }

        enableRenaming(module: any) {
            this.previousName = module.name;
            module.renaming = true;
        }

        findAndRemoveBlankModule(index?: number) {
            for (let cnt = 0; this.previousName.length === 0 && cnt < this.modules.length; cnt++) {
                if (this.modules[cnt].id === -1) {
                    this.modules.splice(cnt, 1);
                    break;
                }
            }
        }

        selectModuleToDisplay(module: any, index?: number) {
            let helper = () => {
                this.findAndRemoveBlankModule(index);

                this.currentModule = module;
                for (let cnt = 0; cnt < this.modules.length; cnt++) {
                    if (this.modules[cnt].id !== -1 && this.modules[cnt].id === module.id) {
                        this.currentModule = this.modules[cnt];
                        this.creatingNewModule = false;
                        break;
                    }
                }
                this.updateCurrentModule({ module: this.currentModule });

                //If you are updating the display, you are not creating a new module
                this.creatingNewModuleComplete.call(this.parentContext);
            };

            if ((this.currentModule && module.id === this.currentModule.id) || module.id === -1) {
                return;
            } else if (this.currentModule && this.unsavedChanges) {
                this.modalService.openConfirm('Leave with unsaved changes?', 'leave', 'this editor with unsaved changes', null, helper);
            } else {
                helper();
            }
        }

        copyModule(moduleToCopy: any) {
            let copyHelper = () => {
                let newModule = { ...moduleToCopy, approved: false, name: renameDuplicateFile(moduleToCopy.name) };

                let saveToDBhelper = (module) => {
                    this.enableRenaming(module);
                    this.updateModulesList();
                };

                this.previousName = renameDuplicateFile(moduleToCopy.name);

                this.currentModule = newModule;
                this.saveToDb({ moduleToSave: newModule, callback: saveToDBhelper });
            };

            if (this.currentModule && this.detectChanges.call(this.parentContext)) {
                this.modalService.openConfirm('Leave with unsaved changes?', 'leave', 'this editor with unsaved changes', null, copyHelper);
            } else {
                copyHelper();
            }
        }

        createNewModule(newModuleName?: string, newModuleContent?: string) {
            this.creatingNewModule = true;
            this.previousName = '';

            let helper = (moduleSaved) => {
                if (newModuleName && newModuleContent) {
                    this.currentModule = moduleSaved;
                }
                if (this.newModuleName) {
                    this.updateCurrentModule({ module: moduleSaved });
                }

                this.modules.unshift(moduleSaved);

                this.enableRenaming(moduleSaved);

                this.creatingNewModuleComplete.call(this.parentContext, moduleSaved);
            };

            let newModule = {
                source: newModuleContent ? newModuleContent : '',
                approved: false,
                name: newModuleName ? newModuleName : '',
                is_global: true,
                type: this.currentModuleType,
                template: null,
                id: -1,
            };

            if (newModuleName && newModuleContent) {
                this.saveToDb({ moduleToSave: newModule, callback: helper });
            } else {
                helper(newModule);
            }
        }

        updateModulesList(callback?: Function) {
            let scrollPosition = angular.element('#moduleSelector ul').scrollTop();
            this.currentModule.renaming = false;
            this.creatingNewModule = false;
            this.AnalysisModule.list().$promise.then((modules: any[]) => {
                this.modules = modules;
                // this.modules.length = 0;
                // for (let cnt = 0; cnt < modules.length; cnt++) {
                //     this.modules.push(modules[cnt]);
                // }
                for (let cnt = 0; cnt < this.modules.length; cnt++) {
                    modules[cnt].renaming = false;
                    if (modules[cnt].name.length === 0) {
                        modules[cnt].name = 'FILENAME-EMPTY';
                    }
                }

                //JAC TODO:  Figure out how to stop the modules list from scrolling on update
                //this timeout is here as a stop gap solution
                this.$timeout(() => {
                    angular.element('#moduleSelector ul').scrollTop(scrollPosition);
                    if (callback) {
                        callback();
                    }
                }, 0);
            });
        }
    },
});

app.directive('contenteditable', function () {
    return {
        require: 'ngModel',
        link: function (scope: any, element, attrs, ngModel) {
            function read() {
                let text = element.text();
                ngModel.$setViewValue(text.length > 0 ? text : '');
            }

            ngModel.$render = function () {
                element.html(ngModel.$viewValue || '');
            };

            element.bind('blur keyup', function (change) {
                if (change && change.type === 'keyup' && change.key !== 'Enter') {
                    return;
                }

                read();
                if (scope.module.id !== -1) {
                    scope.$parent.$ctrl.disableRenaming(scope.module);
                } else {
                    scope.$parent.$ctrl.saveToDb({ moduleToSave: scope.module });
                }
            });
        },
    };
});
