import app from '../app';
import '../common/generalUtils';
import { IGeneralUtils } from '../common/generalUtils';
import './moduleSelector/ModuleSelectorCmp';
import * as _ from 'lodash';

import 'ace-builds/src-noconflict/mode-python';
declare var ace: any;

app.component('developerCmp', {
    templateUrl: '/app/developer/developerTmpl.html',
    controller: class developerCtrl {
        gdata: any;
        editor: any;
        currentModule: any;
        currentModuleType: string;
        renderer: any;
        session: any;
        stopSizingTab: Function;
        stopSizingEditor: Function;
        unsavedChanges: boolean;
        lastSavedString: string;
        types: any[];
        selectedOption: any;
        operationTemplates: any[];
        currentOperationTemplate: any;
        newModuleContent: string;
        newModuleName: string;
        updateModulesFlag: number;

        selectedModules: [];
        importModulesInner: Function;
        exportModulesInner: Function;

        param: {
            value: string;
        };

        // Define `GeneralUtils` as a dependency.
        static $inject = [
            'GeneralUtils',
            'globalDataService',
            'FileSaver',
            'AnalysisModule',
            'OperationTemplates',
            'modalService',
            'importExportService',
        ];

        constructor(
            private GeneralUtils: IGeneralUtils,
            public globalDataService: any,
            public FileSaver: any,
            public AnalysisModule: any,
            public OperationTemplates: any,
            public modalService: any,
            public importExportService: any
        ) {
            this.gdata = globalDataService;
            this.param = {
                value: '',
            };
            this.unsavedChanges = false;
            this.lastSavedString = '';
            this.types = ['Drools', 'Durable Rules'];
            this.currentModuleType = 'Durable Rules';
            this.currentOperationTemplate = {};
            this.currentModule = {
                source: '',
                approved: false,
                name: '',
                is_global: true,
                type: this.currentModuleType,
                template: null,
                id: -1,
            };
            this.updateModulesFlag = 0;
            this.selectedModules = [];
        }

        $onInit() {
            this.stopSizingTab = this.GeneralUtils.setDivHeight('.dashboard-tab-developer', 170);
            this.stopSizingEditor = this.GeneralUtils.setDivHeightBasedOnSiblings(
                '#developerTabHeightContainer',
                '#developerTabContent',
                '#developerToolbar'
            );

            this.updateOperationTemplatesList();

            this.importModulesInner = this.importExportService.importer(this.AnalysisModule.import, this.updateModulesList.bind(this));

            this.exportModulesInner = this.importExportService.exporter(this.AnalysisModule.export);
        }

        $onDestroy() {
            this.stopSizingTab();
            this.stopSizingEditor();
        }

        aceLoaded(_editor) {
            this.editor = _editor;

            // Editor part
            this.session = _editor.getSession();
            this.renderer = _editor.renderer;

            // Options
            this.editor.setReadOnly(false);
            this.session.setUndoManager(new ace.UndoManager());
            this.renderer.setShowGutter(true);

            // Events
            this.editor.on('changeSession', function () {});
            this.session.on('change', this.detectChanges.bind(this));
        }

        //Call this to fire an onChanges event in the moduleSelector so that it
        //is instructed to create a new file entry.
        createNewModule(name?: string, content?: string) {
            let helper = () => {
                this.param.value = '';
                this.lastSavedString = '';
                this.newModuleContent = content ? content : '';
                this.newModuleName = name ? name : '';
                this.currentModuleType = 'Durable Rules';
                this.currentOperationTemplate = -1;
            };

            if (this.unsavedChanges) {
                this.modalService.openConfirm('Leave with unsaved changes?', 'leave', 'this editor with unsaved changes', null, helper);
            } else {
                helper();
            }
        }

        //DO NOT DO THIS, IT takes too long to run
        // createNewModule = _.debounce(this.createNewModuleHelper, 1000);

        //Add anything that needs to wait for the module creation code to complete
        creatingNewModuleComplete(newModule?: any) {
            this.newModuleName = undefined;
            this.newModuleContent = undefined;
            if (newModule) {
                this.currentModule = newModule;
            }
        }

        downloadToDisk(module) {
            let data = module.source;
            let blob = new Blob([data], { type: 'text/plain;charset=utf-8' });
            this.FileSaver.saveAs(blob, module.name + '.py');
        }

        detectChanges() {
            let currentValue;
            if (this.editor) {
                currentValue = this.editor.getValue();
                if (this.lastSavedString && this.lastSavedString.length !== 0) {
                    let currentValue = this.editor.getValue();
                    this.unsavedChanges = this.lastSavedString != currentValue;
                } else if (this.lastSavedString.length !== 0 || currentValue.length !== 0) {
                    this.unsavedChanges = true;
                } else {
                    this.unsavedChanges = false;
                }
            }

            //This is strictly for convience
            return this.unsavedChanges;
        }

        //Added this wrapper function to make is easier to disable the export function
        exportModules() {
            if (this.selectedModules.length > 0) {
                this.exportModulesInner(this.selectedModules);
            }
        }

        //Added this wrapper function to keep the import code as similar to the export
        //code as possible.  It also makes it easier in the future to add any other
        //locking functionality if it's needed.
        importModules($file) {
            this.importModulesInner($file);
        }

        moduleIslocked(moduleIsInitialized: boolean, module?: any) {
            let moduleToTest = module ? module : this.currentModule;
            return (!this.gdata.isMissionDirector && moduleToTest.approved) || (moduleIsInitialized && moduleToTest.id === -1);
        }

        saveToDb(moduleToSave?: any, callback?: Function) {
            let source = this.editor.getValue();

            if (this.moduleIslocked(false, moduleToSave)) {
                return;
            }

            if (moduleToSave) {
                this.AnalysisModule.save({
                    source: moduleToSave.source.length > 0 ? moduleToSave.source : this.editor.getValue(),
                    approved: false,
                    name: moduleToSave.name,
                    is_global: moduleToSave.is_global,
                    type: moduleToSave.type,
                    template: moduleToSave.template,
                }).$promise.then(
                    (moduleSaved) => {
                        this.lastSavedString = moduleSaved.source;
                        this.unsavedChanges = false;
                        this.updateModulesList();
                        this.updateCurrentModule(moduleSaved);
                        if (callback) {
                            callback(moduleSaved);
                        }
                    },
                    (error) => {
                        this.modalService.openErrorResponse(moduleToSave.name + ' cannot be saved', error, false);
                    }
                );
            } else if (this.currentModule && this.currentModule.id !== -1) {
                this.AnalysisModule.update({
                    source: source,
                    id: this.currentModule.id,
                }).$promise.then(
                    (moduleSaved) => {
                        this.lastSavedString = moduleSaved.source;
                        this.unsavedChanges = false;
                        this.updateModulesList();
                    },
                    (error) => {
                        this.modalService.openErrorResponse(this.currentModule.name + ' cannot be saved', error, false);
                    }
                );
            } else if (this.currentModule.name) {
                this.AnalysisModule.save({
                    source: source,
                    approved: false,
                    name: this.currentModule.name,
                    is_global: this.currentOperationTemplate === -1,
                    type: this.currentModuleType,
                    template: this.currentOperationTemplate !== -1 ? this.currentOperationTemplate : null,
                }).$promise.then(
                    (moduleSaved) => {
                        this.currentModule = moduleSaved;
                        this.unsavedChanges = false;
                        this.lastSavedString = moduleSaved.source;
                        if (this.currentModule && this.currentModule.id !== -1) {
                            this.lastSavedString = this.currentModule.source;
                            this.unsavedChanges = false;
                            this.updateModulesList();
                        }
                    },
                    (error) => {
                        this.modalService.openErrorResponse(this.currentModule.name + ' cannot be saved', error, false);
                    }
                );
            }
        }

        saveOperationTemplate() {
            if (this.moduleIslocked(true)) {
                return;
            }
            if (this.currentOperationTemplate !== -1) {
                this.AnalysisModule.update({
                    template: this.currentOperationTemplate,
                    is_global: false,
                    id: this.currentModule.id,
                }).$promise.then(() => {
                    this.updateModulesList();
                });
            } else {
                this.AnalysisModule.update({
                    is_global: true,
                    template: null,
                    id: this.currentModule.id,
                }).$promise.then(
                    () => {
                        this.updateModulesList();
                    },
                    (error) => {
                        this.modalService.openErrorResponse('Cannot update operation template of ' + this.currentModule.name, error, false);
                    }
                );
            }
        }

        saveType() {
            if (this.moduleIslocked(true)) {
                return;
            }

            if (this.currentModule && this.currentModule.id !== -1) {
                this.AnalysisModule.update({
                    type: this.currentModuleType,
                    id: this.currentModule.id,
                }).$promise.then(
                    () => {
                        this.updateModulesList();
                    },
                    (error) => {
                        this.modalService.openErrorResponse('Cannot update module type of ' + this.currentModule.name, error, false);
                    }
                );
            }
        }

        testModule() {
            if (this.moduleIslocked(true)) {
                return;
            }

            if (this.currentModule && this.currentModule.id) {
                this.AnalysisModule.test({
                    id: this.currentModule.id,
                }).$promise.then(
                    () => {
                        this.modalService.openSuccess(
                            this.currentModule.name + ' passed',
                            this.currentModule.name + ' was able to be compiled.',
                            false
                        );
                    },
                    (error) => {
                        this.modalService.openErrorResponse(this.currentModule.name + ' did not pass', error, false);
                    }
                );
            }
        }

        updateCurrentModuleName(newModuleName) {
            this.currentModule.name = newModuleName;
        }

        updateCurrentModule(module) {
            if (typeof module === 'object') {
                let readonly = !this.gdata.isMissionDirector && module.approved;
                this.editor.setReadOnly(readonly);
                this.currentModule = module;
                this.currentModuleType = this.currentModule.type;
                this.lastSavedString = this.currentModule.source;
                if (this.currentModule.is_global) {
                    this.currentOperationTemplate = -1;
                } else {
                    this.currentOperationTemplate = this.currentModule.template;
                }
                module = module.source;
            }
            this.param.value = module;
        }

        //ONLY USE THIS IF YOU ONLY NEED TO CAUSE AN UPDATE OF THE MODULES LIST
        //Added to make it easier in the future to flag ModuleSelectorCmp to update it list
        //In reality, this could be done in each place were the update is needed
        //at the moment, but I thought the code might read cleane this way.
        updateModulesList() {
            this.updateModulesFlag++;
        }

        updateOperationTemplatesList(callback?: Function) {
            this.OperationTemplates.list().$promise.then((operationTemplates: [any]) => {
                this.operationTemplates = operationTemplates;

                let global = {
                    id: -1,
                    name: 'GLOBAL',
                    process: '',
                };

                this.operationTemplates.push(global);
                this.currentOperationTemplate = -1;

                if (callback) {
                    callback();
                }
            });
        }

        uploadToEditor($file) {
            if ($file === null) {
                return;
            }
            if ($file !== null && ($file.type === 'text/x-python-script' || $file.type === 'text/x-python')) {
                let reader = new FileReader();
                reader.addEventListener('loadend', () => {
                    // Code to pass string to the Developer Component
                    //and then to theACE Editor component
                    if (typeof reader.result === 'string') {
                        //Since we are enforcing the file type we should be able to assume
                        //that we can cut off the '.py'
                        this.createNewModule($file.name.slice(0, -3), reader.result);
                    }
                });
                reader.readAsText($file);
            } else if ($file) {
                this.modalService.openErrorResponse(
                    'Invalid file.',
                    $file.name + ' is not a valid Python file. I has a type of: ' + $file.type,
                    false
                );
            } else {
                this.modalService.openErrorResponse('Invalid file.', 'unknown file recieved.', false);
                console.error($file);
            }
        }
    },
});
