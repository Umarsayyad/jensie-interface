import app from '../app';
import * as _ from 'lodash';
import * as angular from 'angular';

import '../common/actionsDropdownComp';
import '../customer/manage/CondensedCustomerFilterComp';

import { config } from '@cato/config';
import { ValidTheme } from '@cato/config/themes/valid-themes';
import { INonDisclosure, INonDisclosureResult } from '../../ng-app/customer/models/non-disclosure.interface';
import { UibModal } from '../../ng-app/ngjs-upgrade/uib-modal';

app.controller('ManageUsersCtrl', function (
    $rootScope,
    $state,
    $scope,
    $uibModal,
    modalService,
    globalDataService,
    socketService,
    Customers,
    Users,
    RulesOfEngagements,
    FileSaver,
    NonDisclosureResource,
    stateBuilder
) {
    $scope.data = {
        customersWithRoEs: {},
        customersWithNda: {},
        showInactive: false,
    };

    $scope.usersActions = {
        editUser: {
            action: function (user) {
                $state.go('.addUser', { user_id: user.id });
            },
            class: 'btn-white',
            icon: 'fa-pencil',
            label: 'Edit',
            show: $scope.gdata.isMissionDirectorOrAdmin,
            tooltip: 'Edit this user',
        },
        deleteUser: {
            action: function (user) {
                $scope.deleteUser(user);
            },
            class: 'btn-white',
            icon: 'fa-trash',
            label: 'Delete',
            show: $scope.gdata.isMissionDirectorOrAdmin,
            tooltip: 'Delete this user',
        },
    };

    $scope.operatorActions = {
        editUser: {
            action: function (operator) {
                $state.go('.addUser', { user_id: operator.id });
            },
            class: 'btn-white',
            icon: 'fa-pencil',
            label: 'Edit',
            show: $scope.gdata.isMissionDirectorOrAdmin,
            tooltip: 'Edit this operator',
        },
        deleteUser: {
            action: function (operator) {
                $scope.deleteOperator(operator);
            },
            class: 'btn-white',
            icon: 'fa-trash',
            label: 'Delete',
            show: $scope.gdata.isMissionDirectorOrAdmin,
            tooltip: 'Delete this operator',
        },
    };

    $scope.insightActions = {
        editUser: {
            action: function (insight) {
                $state.go('.addUser', { user_id: insight.id });
            },
            class: 'btn-white',
            icon: 'fa-pencil',
            label: 'Edit',
            show: $scope.gdata.isMissionDirectorOrAdmin,
            tooltip: 'Edit this insight user',
        },
        deleteUser: {
            action: function (insight) {
                $scope.deleteInsight(insight);
            },
            class: 'btn-white',
            icon: 'fa-trash',
            label: 'Delete',
            show: $scope.gdata.isMissionDirectorOrAdmin,
            tooltip: 'Delete this insight user',
        },
    };

    $scope.customersActions = {
        downloadRoE: {
            action: function (customer) {
                $scope.downloadRoE(customer.id);
            },
            class: 'btn-white',
            icon: 'fa-download',
            label: 'Download RoE',
            show: function (customer) {
                return $scope.data.customersWithRoEs[customer.id] !== undefined;
            },
            tooltip: 'Download the signed RoE',
        },
        manageNda: {
            action: function (customer) {
                $scope.manageNda(customer.id);
            },
            class: 'btn-white',
            icon: 'fa-file-pdf-o',
            label: 'Manage NDA',
            tooltip: 'Manage Non-disclosure agreements',
        },
        approveRoE: {
            action: function (customer) {
                $scope.approveRoE(customer.id);
            },
            class: 'btn-white',
            icon: 'fa-check',
            label: 'Approve RoE',
            show: function (customer) {
                return $scope.data.customersWithRoEs[customer.id] !== undefined && !$scope.data.customersWithRoEs[customer.id].is_approved;
            },
            tooltip: 'Approve the signed RoE',
        },
        rejectRoE: {
            action: function (customer) {
                $scope.rejectRoE(customer.id);
            },
            class: 'btn-white',
            icon: 'fa-ban',
            label: 'Reject RoE',
            show: function (customer) {
                return $scope.data.customersWithRoEs[customer.id] !== undefined && !$scope.data.customersWithRoEs[customer.id].is_approved;
            },
            tooltip: 'Reject the signed RoE',
        },
        uploadExternalRoE: {
            action: function (customer) {
                $state.go('.uploadExternalRoE', { customer_id: customer.id });
            },
            class: 'btn-white',
            icon: 'fa-upload',
            label: 'Upload RoE',
            show: $scope.gdata.isMissionDirectorOrAdmin,
            tooltip: 'Upload an external signed RoE',
        },
        editCustomer: {
            action: function (customer) {
                $state.go('.addCustomer', { customer_id: customer.id });
            },
            class: 'btn-white',
            icon: 'fa-pencil',
            label: 'Edit',
            show: $scope.gdata.isMissionDirectorOrAdmin,
            tooltip: 'Edit Customer',
        },
        validateSam: {
            action: function (customer) {
                $scope.validateSam(customer);
            },
            class: 'btn-white',
            icon: 'fa-check',
            label: "Validate SAM",
            show: function(customer) {
                return !$scope.gdata.customerMap[customer.id].sam_verified;
            },
            tooltip: 'Mark customer as validated in the SAM'
        },
    };

    $scope.validateSam = function(customer) {
        var response = Customers.validateSam({ customerId: customer.id }, {}).$promise;
        response.then(function () {
            $scope.gdata.customerMap[customer.id].sam_verified = true;
        });
    };

    $scope.userFilters = function (item) {
        if (!$scope.data.showInactive && !item.is_active) {
            return false;
        }

        if ($scope.gdata.isMissionDirectorOrAdmin) {
            if (item.type === 'customer') {
                if ($scope.selectedCustomer) {
                    return item.customer === $scope.selectedCustomer.id || _.includes(item.assigned_customers, $scope.selectedCustomer.id);
                } else {
                    return true;
                }
            }
        } else {
            return item.type === 'customer';
        }
        return false;
    };

    $scope.tabs = [
        {
            heading: $rootScope.catotheme.customerUppercasePlural,
            icon: 'fa-user',
            show: function (tab) {
                return $scope.gdata.isMissionDirector;
            },
            viewName: 'customers',
            stateName: 'customers',
            include: '/app/customer/customersTab.html',
        },
        {
            heading: 'USERS',
            icon: 'fa-users',
            show: function (tab) {
                return true;
            },
            viewName: 'users',
            stateName: 'users',
            include: '/app/user/usersTab.html',
        },
        {
            heading: 'OPERATORS',
            icon: 'fa-user-secret',
            show: function (tab) {
                return $scope.gdata.isMissionDirector;
            },
            viewName: 'operators',
            stateName: 'operators',
            include: '/app/operator/operatorsTab.html',
        },
        {
            heading: 'INSIGHT USERS',
            icon: 'fa-user-circle-o',
            show: () => config.catotheme === ValidTheme.jensie && $scope.gdata.isMissionDirector,
            viewName: 'insights',
            stateName: 'insights',
            include: '/app/operator/insightsTab.html',
        },
    ];

    const roeUnsubscribe = socketService.subscribeByModel('roe', function (roe) {
        if (roe.is_finalized) {
            // For now just replace the existing object.  If we need to later we can use updateObj()
            $scope.data.customersWithRoEs[roe.customer] = roe;
        }
    });

    $scope.$on('$destroy', function () {
        roeUnsubscribe();
    });

    $scope.go = function (tab) {
        $scope.activeTab = tab.stateName;

        if (tab.stateName === 'customers') {
            _.forEach($scope.gdata.customers, function (customer) {
                const roeRequest = RulesOfEngagements.list({ customerId: customer.id });
                roeRequest.$promise.then(function (roes) {
                    if (roes[0] && roes[0].is_finalized) {
                        $scope.data.customersWithRoEs[customer.id] = roes[0];
                    }
                });
            });
        }
    };

    $scope.assignedCustomerList = function (user) {
        let html = '<ul>';
        const max = 10;
        let i = 0;
        let keepGoing = true;
        angular.forEach(user.assigned_customers, function (customer) {
            if (keepGoing) {
                html += '<li>' + $rootScope.gdata.customerMap[customer].name + '</li>';
                i++;
                if (i >= max - 1) {
                    const more = user.assigned_customers.length - max;
                    html += '<li> And ' + more + ' more </li>';
                    keepGoing = false;
                }
            }
        });
        html += '</ul>';
        return html;
    };

    $scope.setSelectedCustomer = function (customer) {
        if (!_.isNil(customer)) {
            $scope.selectedCustomer = { id: customer.id, name: customer.name };
        } else {
            delete $scope.selectedCustomer;
        }
    };

    if ($scope.gdata.isOperator) {
        $scope.setSelectedCustomer(_.get($rootScope.user, 'ui_data.selectedCustomer'));
        $scope.$watch('user.ui_data.selectedCustomer', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                $scope.setSelectedCustomer(newVal);
            }
        });
    }

    function actuallyDeleteUser(user) {
        Users.delete({ userId: user.id }).$promise.then(function (result) {
            globalDataService.deleteUser(user.id);
        });
    }

    function activateOrDeactivateCustomer(customer, active) {
        Customers.patch({ customerId: customer.id }, active).$promise.then(function (result) {
            globalDataService.updateCustomer(customer);
        });
    }

    function actuallyDeactivateCustomer(customer) {
        activateOrDeactivateCustomer(customer, { active: false });
    }

    function actuallyActivateCustomer(customer) {
        activateOrDeactivateCustomer(customer, { active: true });
    }

    function addToCustomersWithNda(customerId: number, ndaFile: INonDisclosure): void {
        if (!$scope.data.customersWithNda[customerId]) $scope.data.customersWithNda[customerId] = [];
        $scope.data.customersWithNda[customerId].push(ndaFile);
    }

    function removeFromCustomersWithNda(customerId: number, removedNdaFiles: Array<INonDisclosure>): void {
        if ($scope.data.customersWithNda[customerId]) {
            removedNdaFiles.forEach((ndaFile) => {
                $scope.data.customersWithNda[customerId] = $scope.data.customersWithNda[customerId].filter((item) => {
                    return item.id != ndaFile.id;
                });
            });
        }
    }

    $scope.deleteOperator = function (operator) {
        modalService.openDelete('Delete User', 'Operator ' + operator.first_name + ' ' + operator.last_name, operator, actuallyDeleteUser);
    };

    $scope.deleteInsight = function (insight) {
        modalService.openDelete('Delete User', 'Insight ' + insight.first_name + ' ' + insight.last_name, insight, actuallyDeleteUser);
    };

    $scope.toggleActive = function (user) {
        const args = {
            is_active: !user.is_active,
            is_requested: true,
        };

        // If the user is requested, undo the flag.
        if (user.is_requested) {
            args.is_requested = false;
        }

        Users.patch(
            { userId: user.id },
            args,
            function (user) {
                globalDataService.updateUser(user);
            },
            function (res) {
                modalService.openErrorResponse("Failed to toggle user's active status", res, false);
            }
        );
    };

    $scope.deleteUser = function (user) {
        modalService.openDelete('Delete User', 'User ' + user.first_name + ' ' + user.last_name, user, actuallyDeleteUser);
    };

    $scope.deactivateCustomer = function (customer) {
        modalService.openConfirm(
            'Deactivate ' + $rootScope.catotheme.customerCapitalize,
            'deactivate',
            customer.name,
            customer,
            actuallyDeactivateCustomer
        );
    };

    $scope.activateCustomer = function (customer) {
        modalService.openConfirm(
            'Activate ' + $rootScope.catotheme.customerCapitalize,
            'activate',
            customer.name,
            customer,
            actuallyActivateCustomer
        );
    };

    $scope.downloadRoE = function (customerId) {
        if ($scope.data.customersWithRoEs[customerId]) {
            const roe = $scope.data.customersWithRoEs[customerId];
            const response = RulesOfEngagements.downloadSigned({ customerId: customerId, roeId: roe.id }).$promise;
            response.then(function (data) {
                FileSaver.saveAs(data.blob, 'Rules of Engagement');
            });
        }
    };

    $scope.manageNda = function (customerId: number): void {
        const data = { customerId: customerId, ndaFiles: $scope.data.customersWithNda[customerId] };
        const bindings = { customerId: '<', ndaFiles: '<' };

        const modal = stateBuilder.componentModalGenerator('modal-manage-nda', bindings, data);
        const modalInstance: UibModal<INonDisclosureResult> = $uibModal.open(modal);

        modalInstance.result.then(function (result) {
            if (result && result.uploaded && result.uploaded.length > 0) {
                result.uploaded.forEach(function (ndaFile) {
                    addToCustomersWithNda(customerId, ndaFile);
                });
            }

            if (result && result.deleted && result.deleted.length > 0) {
                removeFromCustomersWithNda(customerId, result.deleted);
            }
        });
    };

    $scope.approveRoE = function (customerId) {
        if ($scope.data.customersWithRoEs[customerId]) {
            const roe = $scope.data.customersWithRoEs[customerId];
            const response = RulesOfEngagements.approve({ customerId: customerId, roeId: roe.id }, {}).$promise;
            response.then(function () {
                $scope.data.customersWithRoEs[customerId].is_approved = true;
            });
        }
    };

    $scope.rejectRoE = function (customerId) {
        if ($scope.data.customersWithRoEs[customerId]) {
            const roe = $scope.data.customersWithRoEs[customerId];
            const response = RulesOfEngagements.reject({ customerId: customerId, roeId: roe.id }, {}).$promise;
            response.then(function () {
                // TODO: update the logic in the template so we don't need to set the RoE as approved when it is rejected in order to make the buttons go away
                $scope.data.customersWithRoEs[customerId].is_approved = true;
            });
        }
    };

    if ($scope.gdata.isMissionDirectorOrAdmin) {
        NonDisclosureResource.list(0).subscribe((response) => {
            response.forEach((item) => {
                addToCustomersWithNda(item.customer, item);
            });
        });
    }
});
