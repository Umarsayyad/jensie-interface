import app from '../app';
import * as _ from 'lodash';
import * as angular from 'angular';

import '../customer/manage/CondensedCustomerFilterComp';

// Based on the example code provided here: https://stackoverflow.com/questions/32574008
app.directive('emailFileDropZone', function () {
    return {
        restrict: 'EA',
        scope: false,
        link: function (scope: any, element, attrs) {
            scope.email_file_contents = '';
            var processDragOverOrEnter;

            processDragOverOrEnter = function (event) {
                if (event !== null) {
                    event.preventDefault();
                }
                event.originalEvent.dataTransfer.effectAllowed = 'copy';
                return false;
            };

            element.bind('dragover', processDragOverOrEnter);
            element.bind('dragenter', processDragOverOrEnter);
            element.bind('drop', handleDropEvent);

            function insertText(loadedFile) {
                scope.email_file_contents = loadedFile.target.result;
                scope.check_invalid();
                scope.$apply();
            }

            function handleDropEvent(event) {
                if (event !== null) {
                    event.preventDefault();
                }
                var reader = new FileReader();
                reader.onload = insertText;
                reader.readAsText(event.originalEvent.dataTransfer.files[0]);
            }
        },
    };
});

app.controller('addUserLoaderCtrl', function ($scope, $uibModal) {
    $uibModal.open({
        templateUrl: '/app/user/modalAddUser.html',
        controller: 'AddUserCtrl',
        size: 'lg',
        scope: $scope,
    });
});

app.controller('AddUserCtrl', function (
    $scope,
    $state,
    $rootScope,
    globalDataService,
    modalService,
    emailRegex,
    formatFactory,
    Users,
    Locations,
    PointsOfContact,
    PointOfContactAddresses,
    $uibModalInstance
) {
    $scope.config = {};
    $scope.mode = 'new';
    $scope.email_file_contents = '';
    $scope.welcomeEmail = false;
    $scope.types = ['billing', 'executive', 'legal', 'technical'];
    $scope.user = {
        level: 0,
        restricted_operator: false,
        showBulkCreate: false,
        bulkUsers: '',
    };
    var user_id = 0;

    if ($state.params.user_id) {
        /* edit user. */
        //console.log('edit user: ' + parseInt($state.params.user_id));

        $scope.mode = 'edit';

        user_id = parseInt($state.params.user_id);
    } else {
        /* new user. */
        console.log('new user');
        //console.log('mode: ' + $scope.mode);
        $scope.user.customer = $scope.selectedCustomer || _.get($rootScope.user, 'ui_data.selectedCustomer');
    }

    /* so that the UI starts out correctly, could use ng-init, but meh :) */
    if ($scope.gdata.isCustomer) {
        $scope.user.type = 'customer';
        $scope.user.customer = $scope.gdata.customer;
    }

    $scope.gdata.customerInfoPromise.then(function () {
        if (user_id !== 0) {
            $scope.updateObjOld($scope.user, $scope.gdata.userMap[user_id]);
            $scope.user.mission_director = _.includes($scope.user.roles, 'mission_director');
            $scope.user.restricted_operator = _.includes($scope.user.roles, 'restricted_operator');
            $scope.user.account_manager = _.includes($scope.user.roles, 'account_manager');
            if ($scope.user.account_manager || $scope.user.restricted_operator) {
                $scope.user.new_assigned_customers = [];
                for (var i = 0; i < $scope.user.assigned_customers.length; i++) {
                    let customer = $scope.gdata.customerMap[$scope.user.assigned_customers[i]];
                    $scope.user.new_assigned_customers[i] = customer;
                }
            }
        }

        if ($scope.user.customer) {
            let customer_id = $scope.user.customer;
            if ($scope.user.customer.hasOwnProperty('id')) {
                customer_id = $scope.user.customer.id;
            }
            $scope.refreshPocs(customer_id);
            $scope.refreshLocations(customer_id);
        }

        $scope.user.existing = {};
        for (var i = 0; $scope.user.pocs && i < $scope.user.pocs.length; i++) {
            $scope.user.existing[$scope.user.pocs[i]] = $scope.user.pocs;
        }
    });

    function loadUserOptions() {
        Users.options().$promise.then(function (values) {
            $scope.config = values.actions.POST;

            var choices = [];
            angular.forEach($scope.config.type.choices, function (choice) {
                if (choice.value !== 'appliance') choices.push(choice.value);
            });

            $scope.config.type.choices = choices;
            $scope.config.username.pattern = new RegExp($scope.config.username.pattern);
        });

        PointOfContactAddresses.options().$promise.then(function (results) {
            $scope.config.pocaddr = results.actions.POST;
        });
    }

    loadUserOptions();

    $scope.inputTypes = angular.copy(formatFactory);
    $scope.inputTypes.phonenumber = $scope.inputTypes.tel;
    let formats = {
        phonenumber: {
            inputType: 'tel',
            label: 'Phone number',
            placeholder: 'Please enter a telephone number.',
        },
    };
    $scope.updateObjOld($scope.inputTypes, formats);

    $scope.refreshPocs = function (customer_id) {
        PointsOfContact.list({ customerId: customer_id }).$promise.then(function (pocs) {
            const fullPoc = pocs.find((poc) => poc.user == $scope.user.id);
            $scope.user.fullPocAddresses = {};
            if (fullPoc) {
                $scope.user.fullPoc = fullPoc;
                fullPoc.pointofcontactaddresses.sort((a, b) => a.id - b.id);
                for (const addr of fullPoc.pointofcontactaddresses) {
                    switch (addr.type) {
                        case 'email':
                            if (!$scope.user.fullPocAddresses.email) {
                                $scope.user.email = addr.value;
                                $scope.user.fullPocAddresses.email = addr;
                            }
                            break;
                        case 'physical':
                            if (!$scope.user.fullPocAddresses.physical) {
                                $scope.user.physical = addr.location;
                                $scope.user.fullPocAddresses.physical = addr;
                            }
                            break;
                        case 'phonenumber':
                            if (!$scope.user.fullPocAddresses.phonenumber) {
                                $scope.user.phonenumber = addr.value;
                                $scope.user.fullPocAddresses.phonenumber = addr;
                            }
                            break;
                    }
                }
            }
        });
    };

    $scope.refreshLocations = function (customer_id) {
        Locations.list({ customerId: customer_id }).$promise.then(function (locations) {
            $scope.locations = locations;
        });
    };

    $scope.setCustomer = function (customer) {
        if (customer) {
            $scope.user.customer = $scope.gdata.customerMap[customer.id];
            $scope.refreshLocations(customer.id);
        } else {
            $scope.user.customer = undefined;
            $scope.locations = [];
        }
    };

    /* check that password strength requirements are met */
    $scope.enteredPassword = '';
    var invalid = 'fa fa-times-circle-o invalid';
    var valid = 'fa fa-check-circle-o valid';
    $scope.invalid = false;
    $scope.letter = invalid;
    $scope.capital = invalid;
    $scope.number = invalid;
    $scope.special = invalid;
    $scope.length = invalid;

    var checkInvalid = function (pass) {
        var regex = new RegExp('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^A-Za-z0-9]).{8,}$');
        return pass.match(regex) == null;
    };

    var passContains = function (regex) {
        var $password = $('#password').text();
        if ($password.length > 0) {
            return $password.match(regex) ? valid : invalid;
        } else {
            return 'invalid';
        }
    };

    var letter = function () {
        var regex = new RegExp(/[a-z]/);
        $scope.letter = passContains(regex);
    };

    var capital = function () {
        var regex = new RegExp(/[A-Z]/);
        $scope.capital = passContains(regex);
    };

    var number = function () {
        var regex = new RegExp(/[0-9]/);
        $scope.number = passContains(regex);
    };

    var special = function () {
        var regex = new RegExp(/[^A-Za-z0-9]/);
        $scope.special = passContains(regex);
    };

    var length = function () {
        var regex = new RegExp(/.{8,}/);
        $scope.length = passContains(regex);
    };

    $scope.validatePass = function () {
        var pass = $('#password').text();
        // remove the pop-over if it's empty
        $scope.invalid = pass.length > 0 ? checkInvalid(pass) : false;
        letter();
        capital();
        number();
        special();
        length();
    };

    $scope.invalid_emails = [];
    $scope.check_invalid = function () {
        if ($scope.email_file_contents) {
            var emails = $scope.email_file_contents.split('\n');
            $scope.invalid_emails = [];

            for (var i = 0; i < emails.length; i++) {
                var email = emails[i];
                if (email) {
                    // We can expect an empty line at the end, particularly when files are dragged in.
                    if (!validateEmail(email)) {
                        $scope.invalid_emails.push(email);
                    }
                }
            }
        }
    };

    function validateEmail(email) {
        return emailRegex.test(email);
    }

    $scope.showPassword = false;

    var bulkAddUsers = function (userData) {
        $scope.cleanBulkCreateState();
        var user = _.clone($scope.user);
        var users_to_add = [];

        var emails = $scope.email_file_contents.split('\n');
        for (var i = 0; i < emails.length; i++) {
            var email = emails[i];
            if (email) {
                // We can expect an empty line at the end, particularly when files are dragged in.

                var new_user = {
                    username: email,
                    email: email,
                    first_name: email.split('@')[0],
                    restricted_operator: user.restricted_operator,
                    mission_director: user.mission_director,
                    account_manager: user.account_manager,
                    type: user.type,
                    level: user.level,
                    assigned_customers: [],
                    comments: userData.comments,
                };

                if (new_user.restricted_operator || new_user.account_manager) {
                    if (typeof user.new_assigned_customers !== 'undefined') {
                        for (var j = 0; j < user.new_assigned_customers.length; j++) {
                            var customer_id = user.new_assigned_customers[j].id;
                            new_user['assigned_customers'].push(customer_id);
                        }
                        if (user.account_manager) {
                            new_user['customer'] = new_user['assigned_customers'][0];
                        }
                    }
                }

                if (user.type === 'customer') {
                    if (!new_user.account_manager) {
                        new_user['customer'] = userData.customer.id;
                        new_user['last_name'] = $scope.user.customer.nickname;
                    } else {
                        new_user['last_name'] = 'Account Manager';
                    }

                    new_user['level'] = userData.level;
                } else {
                    new_user['last_name'] = 'Operator';
                }

                users_to_add.push(new_user);
            }
        }

        var userBlob = {
            users: users_to_add,
        };

        $scope.submission = Users.bulkAdd(
            userBlob,
            function (res) {
                _.each(res.errors, function (err) {
                    if (err.message) {
                        $scope.errors.push(err.username + ': ' + $scope.parseErrorResponse(err.message));
                    } else {
                        $scope.errors.push($scope.parseErrorResponse(err));
                    }
                });
                _.each(res.infos, function (msg) {
                    if (msg.message) {
                        $scope.infos.push(msg.username + ': ' + $scope.parseErrorResponse(msg.message));
                    } else {
                        $scope.infos.push($scope.parseErrorResponse(msg));
                    }
                });
                _.each(res.successes, function (msg) {
                    if (msg.message) {
                        $scope.successes.push(msg.username + ': ' + $scope.parseErrorResponse(msg.message));
                    } else {
                        $scope.successes.push($scope.parseErrorResponse(msg));
                    }
                });
                _.each(res.new_users, function (user) {
                    globalDataService.addUser(user);
                });

                if ($scope.errors.length > 0) {
                    showErrors($scope.errors);
                } else {
                    $uibModalInstance.close(res);
                }
            },
            function (err) {
                if (_.isArray(err.errors)) {
                    showErrors(err.errors);
                }
            }
        );
    };

    var showErrors = function (errors) {
        _.each(errors, function (err) {
            if (err.message) {
                $scope.errors.push(err.username + ': ' + $scope.parseErrorResponse(err.message));
            } else {
                $scope.errors.push($scope.parseErrorResponse(err));
            }
        });
    };

    $scope.addUser = function () {
        var user = _.clone($scope.user);

        if ($scope.user.showBulkCreate) {
            return bulkAddUsers(user);
        }

        var user_create_body = {
            username: user.username,
            email: user.email,
            title: user.title,
            first_name: user.first_name,
            last_name: user.last_name,
            comments: user.comments,
            type: user.type,
            level: user.level,
            password: user.password,
            restricted_operator: user.restricted_operator,
            account_manager: user.account_manager,
            mission_director: user.mission_director,
            pocs: user.pocs,
            customer: undefined,
            assigned_customers: [],
            department: user.department,
        };
        var selected_types = [];

        // Clear out physical location if the user isn't a PoC.
        if (!user.pocs) {
            user.physical = undefined;
        }

        if (user_create_body.type === 'customer') {
            if (typeof user.customer !== 'undefined') {
                user_create_body.customer = user.customer.id;
            }

            selected_types = user.pocs;

            /* non-selected */
            if (_.isNil(selected_types)) {
                selected_types = [];
            }
        } else {
            /* operators have level and other stuff, but no POCs. */
        }

        if (user.restricted_operator || user.account_manager) {
            if (typeof user.new_assigned_customers !== 'undefined') {
                for (var i = 0; i < user.new_assigned_customers.length; i++) {
                    var customer_id = user.new_assigned_customers[i].id;
                    user_create_body.assigned_customers.push(customer_id);
                }
                if (user.account_manager) {
                    user_create_body.customer = user_create_body.assigned_customers[0];
                }
            }
        }

        const addPocAddresses = (addressObj, poc) => {
            let baseAddress = { poc: poc.id };
            let addresses: any = [];
            if (addressObj.email) {
                addresses.push({ ...baseAddress, value: addressObj.email, type: 'email' });
            }
            if (addressObj.physical) {
                addresses.push({ ...baseAddress, type: 'physical', location: addressObj.physical, value: addressObj.physical });
            }
            if (addressObj.phonenumber) {
                addresses.push({ ...baseAddress, value: addressObj.phonenumber, type: 'phonenumber' });
            }

            for (let address of addresses) {
                PointOfContactAddresses.create({ customerId: addressObj.customer.id || addressObj.customer, pocId: poc.id }, address).$promise.catch(
                    function (result) {
                        modalService.openErrorResponse('Point of contact address could not be created.', result, false);
                    }
                );
            }
        };

        const addPoc = (formUser, apiUser, selectedTypes) => {
            let customer_id = user.customer.hasOwnProperty('id') ? formUser.customer.id : formUser.customer;
            _.each(selectedTypes, function (selectedType) {
                const pocBody = {
                    user: apiUser.id,
                    customer: customer_id,
                    type: selectedType,
                };

                PointsOfContact.create({ customerId: customer_id }, pocBody)
                    .$promise.then(function (poc) {
                        addPocAddresses(formUser, poc);
                    })
                    .catch(function (error) {
                        console.log('error: ' + JSON.stringify(error));
                        modalService.openErrorResponse('Could not create Point of Contact', error, false);
                    });
            });
        };

        if ($scope.mode === 'new') {
            /* The backend prevents a user from hacking the UI to try and do things they shouldn't. */
            $scope.submission = Users.add(
                { welcome_email: $scope.welcomeEmail },
                user_create_body,
                function (user_created) {
                    if (user.type === 'customer') {
                        addPoc(user, user_created, selected_types);

                        /* assume adding the selected types worked, so let's fill it in. */
                        user_created['pocs'] = selected_types;
                    }
                    globalDataService.addUser(user_created);
                    $uibModalInstance.close(user_created);

                    if (!$scope.gdata.isMissionDirectorOrAdmin) {
                        modalService.openSuccess('User request success', 'Requested user will be active once approved.', false);
                    }
                },
                function (result) {
                    modalService.openErrorResponse('Could not create new user', result, false);
                }
            );
        } else {
            /* it's an edit. */
            if ($scope.user.fullPoc) {
                let customer_id = user.customer.hasOwnProperty('id') ? user.customer.id : user.customer;
                // A user can only have 0 or 1 pocs
                if ($scope.user.pocs.length == 1 && $scope.user.fullPoc.type !== $scope.user.pocs[0]) {
                    $scope.user.fullPoc.type = $scope.user.pocs[0];
                    PointsOfContact.update({ customerId: customer_id, pocId: $scope.user.fullPoc.id }, $scope.user.fullPoc).$promise.catch(function (
                        result
                    ) {
                        modalService.openErrorResponse('Point of contact type could not be updated.', result, false);
                    });
                }
                if ($scope.user.fullPocAddresses) {
                    for (const pocType of ['email', 'physical', 'phonenumber']) {
                        const formfilledPocAddress = $scope.user[pocType];
                        let currentPocAddress = $scope.user.fullPocAddresses[pocType];
                        const pocAddressValueProp = pocType === 'physical' ? 'location' : 'value';
                        if (
                            currentPocAddress &&
                            formfilledPocAddress !== undefined &&
                            currentPocAddress[pocAddressValueProp] !== formfilledPocAddress
                        ) {
                            if (formfilledPocAddress === null || formfilledPocAddress === '') {
                                // Null takes care of unsetting the Location
                                PointOfContactAddresses.delete(
                                    { customerId: customer_id, pocId: $scope.user.fullPoc.id, pocAddrId: currentPocAddress.id },
                                    currentPocAddress
                                ).$promise.catch(function (result) {
                                    modalService.openErrorResponse('Point of contact address could not be deleted.', result, false);
                                });
                            } else {
                                currentPocAddress[pocAddressValueProp] = formfilledPocAddress;
                                PointOfContactAddresses.update(
                                    { customerId: customer_id, pocId: $scope.user.fullPoc.id, pocAddrId: currentPocAddress.id },
                                    currentPocAddress
                                ).$promise.catch(function (result) {
                                    modalService.openErrorResponse('Point of contact address could not be updated.', result, false);
                                });
                            }
                        } else if (!currentPocAddress && formfilledPocAddress) {
                            const address = { [pocType]: formfilledPocAddress, customer: customer_id };
                            addPocAddresses(address, $scope.user.fullPoc);
                        }
                    }
                }
            } else {
                addPoc($scope.user, $scope.user, selected_types);
            }

            $scope.submission = Users.update({ userId: user_id }, user_create_body)
                .$promise.then(function (result) {
                    // TODO: Handle editing/adding/removing POC info
                    // Delete out roles, and re-add them, since removing a role would not work with updateObjOld()
                    $scope.gdata.userMap[user_id].roles.length = 0;
                    globalDataService.updateUser(result);
                    $uibModalInstance.close(result);
                })
                .catch(function (result) {
                    console.log('failed to update user: ' + JSON.stringify(result, null, 2));
                    modalService.openErrorResponse('Could not update user', result, false);
                });
        }
    };

    $scope.changeAccountManager = function () {
        if ($scope.user.account_manager && (!Array.isArray($scope.user.assigned_customers) || $scope.user.assigned_customers.length === 0)) {
            $scope.user.new_assigned_customers = [$scope.user.customer];
        }
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('canceled');
    };

    $uibModalInstance.result.then(
        function () {
            $state.go('^');
        },
        function () {
            $state.go('^');
        }
    );

    $scope.cleanBulkCreateState = function () {
        if (!$scope.user.type) {
            $scope.user.type = 'customer';
        }
        $scope.user.mission_director = false;
        $scope.infos = [];
        $scope.errors = [];
        $scope.successes = [];
    };
});
