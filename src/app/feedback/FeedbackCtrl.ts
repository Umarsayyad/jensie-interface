import app from '../app';
import * as _ from 'lodash';

app.controller('FeedbackCtrl', function ($rootScope, $scope, Feedback) {
    $scope.data = {};

    $scope.$watch('user.ui_data.selectedCustomer', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            loadFeedback(newVal);
        }
    });

    function loadFeedback(customer) {
        $scope.data.requests = {};

        var params = {};
        if (customer) {
            params['customer'] = customer.id;
        }

        $scope.requestsLoading = Feedback.list(params).$promise.then(function (requests) {
            $scope.data.requests = requests;
        });
    }

    loadFeedback(_.get($rootScope.user, 'ui_data.selectedCustomer'));
});
