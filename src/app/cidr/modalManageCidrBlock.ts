import app from '../app';
import * as _ from 'lodash';

app.component('modalManageCidrBlock', {
    bindings: {
        block: '<?',
        blockSaved: '<?',
        config: '<',
        close: '<',
        dismiss: '<',
    },
    templateUrl: '/app/cidr/modalManageCidrBlock.html',
    controller: class modalManageCidrBlock {
        owners: any;
        block: any;
        blockSaved: any;
        ownerNameMap: any;
        modalTitle: string;
        isNew: boolean;
        bulkCreate: boolean;
        addressList: string;

        close: Function;
        dismiss: Function; // It appears that this is unused, but there's an input binding for it...

        static $inject = ['CIDRBlocks', 'BlockOwners', 'globalDataService', 'modalService'];

        constructor(public CIDRBlocks: any, public BlockOwners: any, public globalDataService: any, public modalService: any) {
            this.ownerNameMap = {};
            this.isNew = false;
            this.bulkCreate = false;
        }

        $onInit() {
            this.owners = this.BlockOwners.list().$promise.then((results) => {
                for (let owner of results) {
                    if (this.block.owner === owner.id) {
                        this.block.owner = owner.name;
                    }
                    this.ownerNameMap[owner.name] = owner;
                }
            });

            if (_.isUndefined(this.block)) {
                this.modalTitle = this.modalTitle || 'Create New Block';
                this.block = {};
                this.isNew = true;
            } else {
                this.modalTitle = this.modalTitle || 'Edit Block';
                this.block = {
                    address: this.block.address,
                    network_name: this.block.network_name,
                    third_party: this.block.third_party,
                    owner: this.block.owner,
                    id: this.block.id,
                };
            }
        }

        saveBlock() {
            if (this.bulkCreate) {
                this.CIDRBlocks.bulkCreate(
                    { cidrBlockId: 0 },
                    this.addressList.split('\n'),
                    (blocks) => {
                        this.close(blocks);
                    },
                    (err) => {
                        this.modalService.openErrorResponse('Blocks could not be created.', err, false);
                    }
                );
            } else {
                let owner = this.ownerNameMap[this.block.owner];
                let sendBlock = this.block;
                if (typeof owner === 'undefined') {
                    // This is an unknown owner. Create it with the block.
                    // Set the owner to the string name of the owner.
                    sendBlock.owner = this.block.owner;
                } else {
                    sendBlock.owner = owner.id;
                }

                if (this.block.id) {
                    this.CIDRBlocks.update(
                        { cidrBlockId: this.block.id },
                        sendBlock,
                        (block) => {
                            this.close(block);
                        },
                        (result) => {
                            this.modalService.openErrorResponse('Block could not be saved.', result, false);
                        }
                    );
                } else {
                    this.CIDRBlocks.create(
                        sendBlock,
                        (block) => {
                            this.close(block);
                        },
                        (result) => {
                            this.modalService.openErrorResponse('Block could not be saved.', result, false);
                        }
                    );
                }
            }
        }
    },
});
