import app from '../app';
import * as angular from 'angular';
import * as _ from 'lodash';

import './modalManageCidrBlock';
import './modalManageOwner';

app.component('cidrTab', {
    templateUrl: '/app/cidr/cidrTab.html',
    controller: class modalManageCidrBlockCtrl {
        ownerMap: any;
        blockOwners: any[];
        cidrBlocks: any[];
        cidrBlockActions: any;
        bulkActions: any;
        isOperator: boolean;
        table: any;
        cidrUnsubscribe: any;
        param: {};
        selectedBlocks: any[];
        allBlocksSelected: boolean;
        lastOpened: any;
        _extraData: any;

        static $inject = [
            'ngTableService',
            'globalDataService',
            'modalService',
            'stateBuilder',
            'BlockOwners',
            'CIDRBlocks',
            'Targets',
            '$uibModal',
            'socketService',
            'FileSaver',
        ];

        constructor(
            public ngTableService: any,
            public globalDataService: any,
            public modalService: any,
            public stateBuilder: any,
            public BlockOwners: any,
            public CIDRBlocks: any,
            public Targets: any,
            public $uibModal: any,
            public socketService: any,
            public FileSaver: any
        ) {
            this.ownerMap = {};
            this.blockOwners = [];
            this.selectedBlocks = [];
            this.allBlocksSelected = false;
            this._extraData = {};

            this.closeLastOpen();

            this.cidrUnsubscribe = socketService.subscribeByGroup('op-cidrblocks', (signalBlock: any) => {
                if (signalBlock.deleted) {
                    this.table.data.forEach((block, index) => {
                        if (block.id === signalBlock.id) {
                            this.table.data.splice(index, 1);
                        }

                        // If it's the last one, refresh the table.
                        if (this.table.data.length === 0) {
                            this.table.reload();
                        }
                    });
                } else {
                    for (let i in this.table.data) {
                        let block = this.table.data[i];
                        if (block.id === signalBlock.id) {
                            this.table.data[i] = signalBlock;
                            this.table.reload();
                            return;
                        }
                    }

                    this.table.data.push(signalBlock);
                }
            });

            this.bulkActions = {
                deleteBlock: {
                    action: () => {
                        let selected = this.selectedBlocks.map((op) => op.id);

                        modalService.openDelete('Confirmation', 'these ' + selected.length + ' blocks', {}, () => {
                            CIDRBlocks.bulkDestroy(
                                {},
                                { ids: selected },
                                () => {},
                                modalService.openErrorResponseCB('Unable to delete selected targets')
                            );
                        });
                    },
                    class: 'btn-white',
                    icon: 'fa-trash-o text-danger',
                    label: 'Delete',
                    show: (block) => {
                        return this.globalDataService.isMissionDirector;
                    },
                    tooltip: 'Bulk Delete Blocks',
                },
            };

            this.cidrBlockActions = {
                editBlock: {
                    action: (block) => {
                        let data = {
                            block: block,
                        };

                        let modal = stateBuilder.componentModalGenerator(
                            'modal-manage-cidr-block',
                            stateBuilder.cirdBlockModalBindings,
                            data,
                            'cidr/modalManageCidrBlock'
                        );
                        let modalInstance = $uibModal.open(modal);

                        modalInstance.result.then((result: any) => {
                            let found = false;
                            for (let i in this.table.data) {
                                let block = this.table.data[i];
                                if (block.id === result.id) {
                                    this.table.data.splice(i, 1, result);
                                    found = true;
                                }
                            }

                            if (!found) {
                                this.table.data.push(result);
                            }
                        });
                    },
                    class: 'btn-white',
                    icon: 'fa-pencil',
                    label: 'Edit',
                    show: function () {
                        return true;
                    },
                    tooltip: 'Edit Block',
                },
                deleteBlock: {
                    action: (block) => {
                        modalService.openDelete('Confirmation', "CIDR Block '" + block.address + "'", block, () => {
                            this.CIDRBlocks.delete(
                                { cidrBlockId: block.id },
                                block,
                                () => {
                                    for (var j = 0; j < this.table.data.length; j++) {
                                        if (this.table.data[j].id === block.id) {
                                            this.table.data.splice(j, 1);
                                            break;
                                        }
                                    }
                                },
                                modalService.openErrorResponseCB('Error deleting CIDR Block')
                            );
                        });
                    },
                    class: 'btn-white',
                    icon: 'fa-trash',
                    label: 'Delete',
                    show: (block) => {
                        return this.globalDataService.isMissionDirector;
                    },
                    tooltip: 'Delete Block',
                },
            };
        }

        $onInit() {
            this.table = this.ngTableService.serverFilteredTable({
                resource: this.CIDRBlocks.get,
                defaultParams: {},
                updateParamsCallback: (params) => {},
            });
        }

        $onChanges() {
            this.closeLastOpen();
        }

        $onDestroy() {
            this.cidrUnsubscribe();
        }

        extraData(block) {
            block = block || {};
            var extraData = this._extraData[block.id];
            if (!extraData) {
                extraData = this._extraData[block.id] = {};
            }
            return extraData;
        }

        toggleTargetQuickView(block) {
            this.extraData(block).targetPopoverIsOpen = !this.extraData(block).targetPopoverIsOpen;
            if (this.extraData(block).targetPopoverIsOpen) {
                this.closeLastOpen(block, 'target');

                this.extraData(block).targetsTable = this.ngTableService.serverFilteredTable({
                    resource: this.Targets.list,
                    defaultParams: { cidr: block.id },
                    extraInitialParams: { count: 10 },
                    updateParamsCallback: _.noop,
                });
            }
        }

        closeLastOpen(block?: any, current?: any) {
            if (current !== 'target' || block !== this.lastOpened) {
                this.extraData(this.lastOpened).targetPopoverIsOpen = false;
            }
            this.lastOpened = block;
        }

        importBlocks($file) {
            if ($file !== null) {
                var reader = new FileReader();
                reader.addEventListener('loadend', () => {
                    this.CIDRBlocks.importBlocks(
                        {},
                        reader.result,
                        (res) => {
                            this.modalService.openSuccess('Blocks imported!', 'Import was successful.', false);
                        },
                        (err) => {
                            this.modalService.openErrorResponse('Import Error', err, false);
                        }
                    );
                });
                reader.readAsText($file);
            }
        }

        exportBlocks() {
            if (this.selectedBlocks.length > 0) {
                var ids = _.map(this.selectedBlocks, 'id');
                this.CIDRBlocks.exportBlocks(
                    {},
                    ids,
                    (res) => {
                        var data = new Blob([angular.toJson(res, true)], { type: 'application/json;charset=utf-8' });
                        this.FileSaver.saveAs(data, 'export.json');
                    },
                    (err) => {
                        this.modalService.openErrorResponse('Export Error', err, false);
                    }
                );
            }
        }

        selectAllBlocks() {
            _.each(this.table.data, (o) => {
                o.selected = this.allBlocksSelected;
            });
        }

        manageOwner(owner) {
            let data = {
                owner: owner,
            };
            let modal = this.stateBuilder.componentModalGenerator(
                'modal-manage-owner',
                this.stateBuilder.blockOwnerModalBindings,
                data,
                'cidr/modalManageOwner'
            );
            this.$uibModal.open(modal);
        }
    },
});
