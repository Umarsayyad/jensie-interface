import app from '../app';
import * as _ from 'lodash';

app.component('modalManageOwner', {
    bindings: {
        owner: '<?',
        blockSaved: '<?',
        config: '<',
        close: '<',
        dismiss: '<',
    },
    templateUrl: '/app/cidr/modalManageOwner.html',
    controller: class modalManageOwner {
        owner: any;
        owners: any;
        blocks: any;
        modalTitle: string;
        removedBlocks: any[];
        addType: string;
        current_page: number;
        page_count: number;
        per_page: number;
        block_count: number;

        static $inject = ['CIDRBlocks', 'BlockOwners', 'globalDataService', 'modalService', 'ngTableService'];

        constructor(
            public CIDRBlocks: any,
            public BlockOwners: any,
            public globalDataService: any,
            public modalService: any,
            public ngTableService: any
        ) {
            this.removedBlocks = [];
            this.addType = 'manual';
            this.current_page = 1;
            this.page_count = 0;
            this.per_page = 25;
            this.block_count = 0;
        }

        $onInit() {
            if (this.owner) {
                this.BlockOwners.get({ blockOwnerId: this.owner }, (result) => {
                    this.owner = result;
                    this.loadBlocks();
                    this.modalTitle = 'Edit ' + this.owner.name;
                });
            }
        }

        loadBlocks() {
            this.CIDRBlocks.get({ owner: this.owner.id, page_size: this.per_page, page: this.current_page, ordering: 'address' }).$promise.then(
                (results) => {
                    this.blocks = results['results'];
                    this.page_count = Math.ceil(results['count'] / this.per_page);
                    this.block_count = results['count'];
                }
            );
        }

        nextBlockPage() {
            this.current_page++;
            this.loadBlocks();
        }

        previousBlockPage() {
            this.current_page--;
            this.loadBlocks();
        }

        removeBlock(block) {
            this.modalService.openConfirm('Really remove?', 'remove block ' + block.address, '', {}, () => {
                block.owner = 'Unknown';
                block.manual_entry = true;

                this.CIDRBlocks.update(
                    { cidrBlockId: block.id },
                    block,
                    () => {
                        this.blocks.forEach((item, index) => {
                            if (item.id === block.id) this.blocks.splice(index, 1);
                            this.removedBlocks.push(block.id);
                        });
                        this.block_count--;
                        let initial_page_count = this.page_count;
                        this.page_count = Math.ceil(this.block_count / this.per_page);

                        if (this.current_page > this.page_count) {
                            // When you remove the last item on the last page, go back one.
                            this.previousBlockPage();
                        } else {
                            // Reload the current page.
                            this.loadBlocks();
                        }
                    },
                    (result) => {
                        this.modalService.openErrorResponse('Block could not be removed.', result, false);
                    }
                );
            });
        }
    },
});
