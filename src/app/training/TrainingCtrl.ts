import app from '../app';
import * as _ from 'lodash';

app.controller('TrainingCtrl', function ($scope, Users) {
    var updateFrameHeight = _.throttle(function () {
        // TODO: determine offset dynamically depending on if "RECENT FINDINGS" and "WHAT'S TRENDING?" boxes are visible
        $('#training-frame').css('min-height', $(window).height() - 375 + 'px');
    }, 100);

    $(window).on('resize', updateFrameHeight);

    $scope.$on('$viewContentLoaded', updateFrameHeight);

    $scope.$on('$destroy', function () {
        $(window).off('resize', updateFrameHeight);
    });

    Users.get({ userId: 'training_auth' }, function (res) {
        $scope.frameURL = res.loginurl;
    });
});
