import app from '../app';
import * as _ from 'lodash';
import '../customer/manage/CondensedCustomerFilterComp';

app.component('modalTagTargets', {
    bindings: {
        targets: '<',
        close: '<',
        dismiss: '<',
    },
    templateUrl: '/app/tag/modalTagTargets.html',
    controller: function (modalService, Targets) {
        var ctrl = this;
        ctrl.tags = [];
        ctrl.append = true;

        ctrl.$onInit = function () {
            // Bindings are now available
            ctrl.customer = _.get(ctrl.targets, '[0].customer');
        };

        // Save/submit handlers
        ctrl.updateTags = function () {
            var data = {
                targets: _.map(ctrl.targets, 'id'),
                tags: ctrl.tags,
                append_tags: ctrl.append,
            };
            ctrl.submission = Targets.bulkUpdate(
                data,
                function () {
                    ctrl.close(data);
                },
                modalService.openErrorResponseCB('Targets could not have tags updated')
            );
        };
    },
});
