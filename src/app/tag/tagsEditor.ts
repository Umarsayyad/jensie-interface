import app from '../app';
import * as _ from 'lodash';

app.component('tagsEditor', {
    require: {
        ngModelCtrl: '?ngModel',
    },
    bindings: {
        tags: '<?',
        ngModel: '<?', // actually works as two-way via ngModelCtrl, but only does a shallow watch so you must store a new array instance to trigger updates
        type: '@?',
        customer: '<?',
        filterMode: '<?',
        confirmRemoval: '<?',
        disabled: '@?',
        tagAdded: '&',
        tagRemoved: '&',
    },
    controller: function ($rootScope, globalDataService, modalService) {
        var ctrl = this;
        ctrl.gdata = globalDataService;
        ctrl.catotheme = $rootScope.catotheme;
        ctrl.tagObjs = [];

        ctrl.$onInit = function () {
            // Bindings are now available
            if (ctrl.tags && ctrl.ngModel) {
                throw 'You cannot use both tags="" and ng-model="" on the same <tags-editor>';
            }

            if (ctrl.ngModelCtrl) {
                //noinspection JSPrimitiveTypeWrapperUsage
                ctrl.ngModelCtrl.$render = function () {
                    ctrl.tagObjs = tagIdsToObjects(ctrl.ngModelCtrl.$viewValue);
                };
            }
            ctrl.type = ctrl.type || 'targetTag';
            loadByType();
            if (ctrl.tags || ctrl.ngModel) {
                ctrl.tagObjs = tagIdsToObjects(ctrl.tags || ctrl.ngModel);
            }
        };

        ctrl.$onChanges = function (changes) {
            if (changes.type) {
                loadByType();
            }
            if (changes.tags || changes.ngModel || changes.type) {
                var oldTagObjs = ctrl.tagObjs;
                ctrl.tagObjs = tagIdsToObjects(ctrl.tags || ctrl.ngModel);
                if (!_.isEqual(oldTagObjs, ctrl.tagObjs)) {
                    handleUpdatingModelValue();
                }
            }
        };

        // Template helper functions
        ctrl.customerHasTag = function (tag) {
            if (!ctrl.customer || tag.public || !tag.id || !tag.customers) {
                return true;
            } else {
                var customer = parseInt(ctrl.customer.id || ctrl.customer);
                return tag.public || _.includes(tag.customers, customer);
            }
        };

        // Action handler functions
        ctrl.onTagAdded = function (tag) {
            handleUpdatingModelValue();
            (ctrl.tagAdded || _.noop)({ tag: tag });
        };

        ctrl.onTagRemoved = function (tag) {
            handleUpdatingModelValue();
            (ctrl.tagRemoved || _.noop)({ tag: tag });
        };

        ctrl.onTagAdding = function (tag) {
            if (ctrl.filterMode && !tag.id) {
                return !!ctrl.getExistingOrMakeNewTag(tag.value).id;
            } else {
                return true;
            }
        };

        ctrl.onTagRemoving = function (tag) {
            if (ctrl.confirmRemoval) {
                return modalService.openConfirm('Remove tag', 'remove', 'the tag <em>' + tag.value + '</em> from this target').result;
            } else {
                return true;
            }
        };

        ctrl.getExistingOrMakeNewTag = function (value) {
            var existing = _.find(ctrl.tagArr, function (tag) {
                return tag.value.toLowerCase() === value.toLowerCase();
            });
            return existing || { value: value };
        };
        /*
        TODO: update ngTagsInput to support calling a function to create or modify the tag object when it is added
        This will allow significantly simplifying the logic involved in setting the ngModel value by not requiring
        multiple loops through AngularJS's change detection to update all of the tags to the appropriate objects.
        */

        // Utility functions
        function loadByType() {
            ctrl.tagMap = globalDataService[ctrl.type + 'Map'];
            ctrl.tagArr = globalDataService[ctrl.type + 's'];
            ctrl.loaded = ctrl.tagMap && ctrl.tagArr;

            if (ctrl.loaded && ctrl.filterMode && ctrl.customer) {
                var customer = parseInt(ctrl.customer.id || ctrl.customer);
                ctrl.tagArr = _.filter(ctrl.tagArr, function (tag) {
                    return tag.public || _.includes(tag.customers, customer);
                });
            }
        }

        function tagIdsToObjects(tagIds) {
            if (ctrl.loaded && tagIds) {
                var tags = [];
                var missing = [];
                _.each(tagIds, function (id) {
                    if (_.isNumber(id)) {
                        var tag = ctrl.tagMap[id];
                        if (tag) {
                            tags.push(tag);
                        } else {
                            missing.push(id);
                        }
                    } else {
                        tags.push(ctrl.getExistingOrMakeNewTag(id));
                    }
                });
                if (missing.length > 0) {
                    console.warn('Some requested tags do not appear to exist.  Do we need to reload them?', 'Missing tags: ', missing);
                    // TODO: reload tags to fetch ones created while we were offline?
                }
                return tags;
            } else {
                return [];
            }
        }

        function handleUpdatingModelValue() {
            if (ctrl.ngModelCtrl) {
                ctrl.ngModelCtrl.$setViewValue(tagObjectsToIds());
            }
        }

        function tagObjectsToIds() {
            return _.map(ctrl.tagObjs, function (tag) {
                return tag.id || tag.value;
            });
        }
    },
    templateUrl: '/app/tag/tagsEditor.html',
});
