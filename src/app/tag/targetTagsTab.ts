import app from '../app';
import * as _ from 'lodash';

import './modalManageTargetTag';

app.component('targetTagsTab', {
    bindings: {
        currentTab: '<',
    },
    templateUrl: '/app/tag/targetTagsTab.html',
    controller: function ($rootScope, $uibModal, NgTableParams, globalDataService, modalService, socketService, stateBuilder, Targets, TargetTags) {
        //TODO:  Tag would make a great example for how to define and use an interface
        var lastOpened: any = {};
        var config;
        var filters: any = {};

        var filterComparator = function (value, searchTerm) {
            var returnVal = undefined;
            ctrl.searchTerm = undefined;
            if (typeof value === 'string' && value.length > 0) {
                returnVal = (value.toLowerCase() || '').indexOf(searchTerm.toLowerCase()) !== -1;
                ctrl.searchTerm = searchTerm;
            } else if (typeof value === 'number') {
                returnVal = value === Number(searchTerm);
            }

            return returnVal;
        };

        var ctrl: any = {
            filters: filters,
            reloading: false,
            gdata: globalDataService,
            catotheme: $rootScope.catotheme,
            dataset: [],
            targetMap: {},
            _extraData: {},
            numberOfLinesPerTag: 5,
            searchTerm: undefined,
            table: new NgTableParams(
                {
                    count: 10, // initial page size
                    filter: filters,
                },
                {
                    // page size buttons (right set of buttons in demo)
                    counts: [5, 10, 20, 40],
                    // determines the pager buttons (left set of buttons in demo)
                    paginationMaxBlocks: 13,
                    paginationMinBlocks: 2,
                    dataset: [],
                    filterOptions: {
                        filterComparator: filterComparator,
                    },
                }
            ),
            targetTypes: [],
            targetUnsubscribe: undefined,
            targetTagActions: {
                editTag: {
                    action: function (tag) {
                        if (tag && tag.id) {
                            tag = ctrl.gdata.targetTagMap[tag.id];
                        }
                        closeLastOpen(tag);
                        if (!config) config = TargetTags.options();

                        var data = {
                            tag: tag,
                            config: config.$promise,
                        };

                        var modal = stateBuilder.componentModalGenerator(
                            'modal-manage-target-tag',
                            stateBuilder.tagModalBindings,
                            data,
                            'tag/modalManageTargetTag'
                        );
                        $uibModal.open(modal);
                    },
                    class: 'btn-white',
                    icon: 'fa-pencil',
                    label: 'Edit',
                    tooltip: 'Edit Tag',
                },
                deleteTag: {
                    action: function (tag) {
                        modalService.openDelete('Confirmation', "Target Tag '" + tag.value + "'", {}, function () {
                            TargetTags.delete({ id: tag.id }, function () {}, modalService.openErrorResponseCB('Error deleting Target Tag'));
                        });
                    },
                    class: 'btn-white',
                    icon: 'fa-trash',
                    label: 'Delete',
                    show: function (tag) {
                        return tag.targets.length === 0;
                    },
                    tooltip: 'Delete Tag',
                },
            },
            $onInit: function () {
                Targets.options(function (result) {
                    ctrl.targetTypes = _.map(_.sortBy(result.actions.POST.type.choices, 'display_name'), function (type) {
                        return { id: type.value, title: type.display_name };
                    });
                });

                ctrl.dataset = ctrl.processData(ctrl.gdata.targetTags);
                ctrl.table = ctrl.setFilter($rootScope.user.ui_data.selectedCustomer, ctrl.dataset);

                ctrl.targetTagsWatch = $rootScope.$watch(
                    function () {
                        return ctrl.gdata.targetTags;
                    },
                    function (newVal?: any, oldVal?: any) {
                        if (newVal !== undefined) {
                            ctrl.dataset = ctrl.processData(ctrl.gdata.targetTags);
                            ctrl.table.settings({ dataset: ctrl.dataset });
                        }
                    },
                    true
                );

                ctrl.selectedCustomersWatch = $rootScope.$watch('user.ui_data.selectedCustomer', function (newVal, oldVal) {
                    if (newVal !== undefined) {
                        ctrl.table = ctrl.setFilter(newVal);
                    }
                });

                ctrl.filterCustomersWatch = $rootScope.$watch(
                    function () {
                        return filters.customers;
                    },
                    function (newVal, oldVal) {
                        if (newVal === '') {
                            ctrl.dataset = ctrl.processData(ctrl.gdata.targetTags);
                            ctrl.table = ctrl.setFilter(null, ctrl.dataset);
                        }
                    }
                );

                ctrl.targetUnsubscribe = socketService.subscribeByModel('target', storeTarget); // TODO: update target's tags' tables automatically?
            },
            $onChanges: function () {
                ctrl.dataset = ctrl.processData(ctrl.gdata.targetTags);
                ctrl.table = ctrl.setFilter($rootScope.user.ui_data.selectedCustomer, ctrl.dataset);
                closeLastOpen();
            },
            $onDestroy: function () {
                if (ctrl.targetUnsubscribe) {
                    ctrl.targetUnsubscribe();
                }
                if (ctrl.targetTagsWatch) {
                    ctrl.targetTagsWatch();
                }
                if (ctrl.filterCustomersWatch) {
                    ctrl.filterCustomersWatch();
                }
                if (ctrl.selectedCustomersWatch) {
                    ctrl.selectedCustomersWatch();
                }
            },
            displayedCustomer: function (customer) {
                if (!filters.customers || (filters.customers && customer.toLowerCase().indexOf(filters.customers.toLowerCase()) !== -1)) {
                    return true;
                } else {
                    return false;
                }
            },
            extraData: function (tag) {
                tag = tag || {};
                var extraData = ctrl._extraData[tag.id];
                if (!extraData) {
                    extraData = ctrl._extraData[tag.id] = {};
                }
                return extraData;
            },
            processData: function (tags) {
                var dataset = _.cloneDeep(tags);

                for (var cnt = 0; cnt < dataset.length; cnt++) {
                    var tag = dataset[cnt];
                    var customers = [];
                    for (var cnt2 = 0; cnt2 < tag.customers.length; cnt2++) {
                        var customer = tag.customers[cnt2];
                        if (typeof customer === 'number') {
                            customers.push(ctrl.gdata.customerMap[tag.customers[cnt2]].name);
                        }
                    }
                    tag.customersOrig = tag.customers = customers;
                    tag.customersCount = customers.length;
                    ctrl.selectedCustomersWatch;
                }
                return dataset;
            },
            setFilter: function (newVal, dataset) {
                var settings = ctrl.table.settings();
                settings.filterOptions = {
                    filterComparator: filterComparator,
                };

                if (newVal && newVal.name) {
                    filters.customers = newVal.name;
                } else {
                    filters.customers = undefined;
                }
                settings.dataset = dataset ? dataset : ctrl.processData(ctrl.gdata.targetTags);

                var tableParams = new NgTableParams({ filter: filters }, settings);
                return tableParams;
            },
            toggleTargetQuickView: function (tag) {
                ctrl.extraData(tag).targetPopoverIsOpen = !ctrl.extraData(tag).targetPopoverIsOpen;
                if (ctrl.extraData(tag).targetPopoverIsOpen) {
                    closeLastOpen(tag, 'target');
                    var makeTable = true;
                    if (!ctrl.extraData(tag).targetDetails) {
                        if (_.some(_.at(ctrl.targetMap, tag.targets), _.isUndefined)) {
                            makeTable = false;
                            ctrl.extraData(tag).targetDetails = Targets.basicList({ tags: tag.id }, function (targets) {
                                _.each(targets, storeTarget);
                                buildTargetsTable(tag);
                            });
                        } else {
                            ctrl.extraData(tag).targetDetails = true;
                        }
                    }
                    if (makeTable) {
                        buildTargetsTable(tag);
                    }
                }
            },
        };

        for (var prop in ctrl) {
            if (!ctrl.hasOwnProperty(prop)) {
                //The current property is not a direct property of ctrl
                continue;
            }
            this[prop] = ctrl[prop];
        }

        //Replace the initial ctrl object with the updated this object in order to prevent the two from getting out of sync
        ctrl = this;

        function storeTarget(target) {
            target.customerName = globalDataService.customerMap[target.customer].name;
            if (ctrl.targetMap[target.id]) {
                $rootScope.updateObj(ctrl.targetMap[target.id], target);
            } else {
                ctrl.targetMap[target.id] = target;
            }
        }

        function closeLastOpen(tag?: any, current?: String) {
            if (current !== 'customer' || tag !== lastOpened) {
                ctrl.extraData(lastOpened).customerPopoverIsOpen = false;
            }
            if (current !== 'target' || tag !== lastOpened) {
                ctrl.extraData(lastOpened).targetPopoverIsOpen = false;
            }
            lastOpened = tag;
        }

        var _targetsTableCache = {};

        function buildTargetsTable(tag) {
            if (_targetsTableCache[tag.id] && _.isEqual(tag.targets, _targetsTableCache[tag.id].targets)) {
                return;
            }

            var table = new NgTableParams(
                {
                    count: 20,
                },
                {
                    counts: [5, 10, 20, 40],
                    dataset: _.at(ctrl.targetMap, tag.targets),
                }
            );

            _targetsTableCache[tag.id] = {
                targets: tag.targets.concat(),
                table: table,
            };
            ctrl.extraData(tag).targetsTable = table;
        }
    },
});
