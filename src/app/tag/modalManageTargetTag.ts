import app from '../app';
import * as _ from 'lodash';

app.component('modalManageTargetTag', {
    bindings: {
        tag: '<?',
        tagSaved: '<?',
        modalTitle: '<?',
        config: '<',
        close: '<',
        dismiss: '<',
    },
    templateUrl: '/app/tag/modalManageTargetTag.html',
    controller: function ($rootScope, globalDataService, modalService, Customers, TargetTags) {
        var ctrl = this;
        ctrl.gdata = globalDataService;
        ctrl.catotheme = $rootScope.catotheme;
        ctrl.customersUsingTagMap = {};

        ctrl.$onInit = function () {
            // Bindings are now available

            if (_.isUndefined(ctrl.tag)) {
                ctrl.modalTitle = ctrl.modalTitle || 'Create New Tag';
                ctrl.tag = {};
            } else {
                ctrl.modalTitle = ctrl.modalTitle || 'Edit Tag';
                ctrl.tag = $rootScope.makePlainCopy(ctrl.tag);

                // TODO: monitor targets directly so this can be updated dynamically, at the cost of tracking much more data?
                Customers.list({ targets__tags: ctrl.tag.id }, function (customers) {
                    _.each(customers, function (customer) {
                        ctrl.customersUsingTagMap[customer.id] = customer;
                    });
                });
            }
        };

        // Save/submit handlers
        ctrl.saveTag = function () {
            var sendTag = $rootScope.copyForRequest(ctrl.tag, ctrl.config);

            if (ctrl.tag.id) {
                ctrl.submission = TargetTags.update(sendTag, saveSuccess, saveError);
            } else {
                ctrl.submission = TargetTags.create(sendTag, saveSuccess, saveError);
            }
        };

        function saveSuccess(tag) {
            (ctrl.tagSaved || _.noop)(tag);
            if (!ctrl.addAnother) {
                ctrl.close(tag);
            } else {
                ctrl.message = 'Tag saved successfully';
            }
        }

        function saveError(result) {
            modalService.openErrorResponse('Tag could not be saved.', result, false);
        }
    },
});
