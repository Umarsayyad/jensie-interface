import app from '../app';
import * as angular from 'angular';
import * as _ from 'lodash';
import '../customer/manage/CondensedCustomerFilterComp';

app.controller('ApplianceEditLoaderCtrl', function ($scope, $state, $uibModal) {
    $uibModal.open({
        templateUrl: '/app/appliances/modalAppliance.html',
        controller: 'ApplianceEditCtrl',
        size: 'lg',
        scope: $scope,
        resolve: {
            params: {
                appliance_id: $state.params.appliance_id,
            },
        },
    });
});

app.controller('ApplianceEditCtrl', function (
    $scope,
    $state,
    $window,
    $rootScope,
    $uibModal,
    $uibModalInstance,
    globalDataService,
    modalService,
    $interval,
    $filter,
    moment,
    Appliances,
    Operations,
    params
) {
    var GENERATE_ZONE = 'Generate new zone for appliance';

    $scope.readyToRetire = false;

    $scope.thresholdOptionsDisk = {};
    $scope.thresholdOptionsRAM = {};
    $scope.thresholdOptionsCPU = {};
    $scope.diskMax = 0;
    $scope.ramMax = 0;

    $scope.tilHeartbeatAlert = 'warning';
    $scope.tilHeartbeatTime = 'Loading...';

    const stopCountdown = $interval(() => {
        let appliance = $scope.gdata.applianceMap[$scope.appliance.id];
        let secondsTilHeartbeat = 0;
        if ($scope.heartbeats[$scope.appliance.id]) {
            let lastHeartbeat = $scope.heartbeats[$scope.appliance.id].timestamp;
            secondsTilHeartbeat = moment(lastHeartbeat).add(appliance.temp_interval, 'seconds').diff(moment(), 'seconds');
        }
        if (Math.abs(secondsTilHeartbeat) < 600) {
            // Under ten minutes, we get an exact countdown in seconds
            if (secondsTilHeartbeat >= 0) {
                $scope.tilHeartbeatTime = secondsTilHeartbeat + ' seconds';
            } else {
                $scope.tilHeartbeatTime = Math.abs(secondsTilHeartbeat) + ' seconds late';
            }
        } else {
            // More than ten minutes, we want to be more human-readable friendly
            $scope.tilHeartbeatTime = $filter('amDurationFormat')(secondsTilHeartbeat, 'seconds') + ' late';
        }
        if (!appliance.new_interval_pending) {
            $scope.tilHeartbeatAlert = secondsTilHeartbeat >= 0 ? 'success' : 'danger';
        } else {
            $scope.tilHeartbeatAlert = 'warning';
            $scope.tilHeartbeatTime = 'Pending interval change, but ' + $scope.tilHeartbeatTime + ' based on current interval';
        }
        $scope.tilHeartbeatTime = _.capitalize($scope.tilHeartbeatTime);
    }, 1000);

    $scope.$on('$destroy', function () {
        $interval.cancel(stopCountdown);
    });

    Operations.get(
        {
            appliance: params.appliance_id,
            state: ['requested', 'in progress', 'paused'],
            page_size: 1,
        },
        function (results) {
            $scope.readyToRetire = results.count === 0;
            if (results.count === 0) {
                $scope.readyToRetire = true;
            } else {
                $scope.notRetireReadyReason = 'Appliance still has active operations.';
            }
        },
        function (result) {
            modalService.openErrorResponse('Could not query operations.', result, false);
        }
    );

    $scope.gdata.appliances.$promise.then(function () {
        $scope.appliance = angular.copy($scope.gdata.applianceMap[params.appliance_id]);
        if ($scope.appliance.customer === null) {
            $scope.appliance.customer = _.get($scope.selectedCustomer, 'id') || _.get($rootScope.user, 'ui_data.selectedCustomer.id');
            $scope.appliance.zone = GENERATE_ZONE;
        }
        populateZones();
    });

    $scope.heartbeats.$promise.then(function (heartbeats) {
        if (heartbeats[$scope.appliance.id]) {
            $scope.diskMax = $scope.convertToGig(heartbeats[$scope.appliance.id].stats.disk.total);
            var disk60 = $scope.diskMax * 0.6;
            var disk80 = $scope.diskMax * 0.8;
            $scope.ramMax = $scope.convertToGig(heartbeats[$scope.appliance.id].stats.memory.ram.total);
            var ram60 = $scope.ramMax * 0.6;
            var ram80 = $scope.ramMax * 0.8;
            $scope.thresholdOptionsDisk[0] = { color: 'green' };
            $scope.thresholdOptionsDisk[disk60] = { color: 'yellow' };
            $scope.thresholdOptionsDisk[disk80] = { color: 'red' };
            $scope.thresholdOptionsRAM[0] = { color: 'green' };
            $scope.thresholdOptionsRAM[ram60] = { color: 'yellow' };
            $scope.thresholdOptionsRAM[ram80] = { color: 'red' };
            $scope.thresholdOptionsCPU[0] = { color: 'green' };
            $scope.thresholdOptionsCPU[disk60] = { color: 'yellow' };
            $scope.thresholdOptionsCPU[disk80] = { color: 'red' };
        }
    });

    $scope.convertToGig = function (input) {
        var gig = 1024 * 1024 * 1024;
        if (!input || isNaN(input)) {
            return null;
        } else {
            return input / gig;
        }
    };

    $scope.setCustomer = function (customer) {
        var customerId = _.get(customer, 'id');
        if (customerId !== $scope.appliance.customer) {
            $scope.appliance.zone = GENERATE_ZONE;
        }
        $scope.appliance.customer = customerId;
        populateZones();
    };

    $scope.editAppliance = function (appliance) {
        appliance = angular.copy(appliance);
        if (appliance.internalIP === '') {
            delete appliance.internalIP;
        }
        if (appliance.zone === GENERATE_ZONE) {
            appliance.zone = null;
        }
        $scope.submission = Appliances.update(
            { applianceId: $scope.appliance.id },
            appliance,
            function (appliance) {
                globalDataService.updateAppliance(appliance);
                $scope.finish(appliance);
            },
            function (result) {
                modalService.openErrorResponse('Appliance could not be saved.', result, false);
            }
        );
    };

    $scope.retireAppliance = function (appliance) {
        modalService.openDelete('Retire appliance?', 'appliance "' + appliance.name + '"', appliance, function () {
            $scope.submission = Appliances.delete(
                { applianceId: appliance.id },
                function () {
                    appliance.retired = true;
                    $scope.gdata.updateAppliance(appliance);
                    $scope.finish(appliance);
                },
                function (err) {
                    modalService.openErrorResponse('Retiring appliance failed', err, false);
                }
            );
        });
    };

    $scope.finish = function (appliance) {
        $uibModalInstance.close(appliance);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancelled');
    };

    // Support being either a state modal or a non-state modal
    function modalClose() {
        if ($state.current.name.indexOf('editAppliance') > -1 && _.isNull($state.transition)) {
            $state.go('^');
        }
    }

    $uibModalInstance.result.then(modalClose, modalClose);

    function populateZones() {
        $scope.availableZones = _.filter($scope.gdata.targetZones, {
            customer: $scope.appliance.customer,
            external: false,
        });
        if (_.isNil($scope.gdata.applianceMap[$scope.appliance.id].zone)) {
            $scope.availableZones.unshift({ id: GENERATE_ZONE, name: GENERATE_ZONE });
        }
    }
});
