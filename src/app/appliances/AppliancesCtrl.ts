import { config } from '@cato/config';

import app from '../app';
import * as _ from 'lodash';

import '../common/actionsDropdownComp';
import '../customer/manage/CondensedCustomerFilterComp';

app.controller('HeartbeatCtrl', function ($scope, $uibModalInstance, Appliances, modalService, params) {
    $scope.submitNewHeartbeat = function () {
        Appliances.setInterval({ applianceId: params.appliance_id }, { temp_interval: $scope.interval }).$promise.then(function (res) {
            $uibModalInstance.close();
        }, modalService.openErrorResponseCB('Error setting heartbeat interval.'));
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});

app.controller('AppliancesCtrl', function ($rootScope, $scope, $state, $parse, $uibModal, modalService, socketService, Appliances, Operations) {
    $scope.heartbeats = Appliances.latestHeartbeats({ applianceId: 0 });

    const applianceUnsubscribe = socketService.subscribeByGroup('op-appliances', () => {});
    const heartbeatUnsubscribe = socketService.subscribeByGroup('op-heartbeats', function (hb) {
        //console.log('got heartbeat data back: ', hb);
        $scope.heartbeats[hb.appliance] = hb;
    });
    $scope.$on('$destroy', function () {
        applianceUnsubscribe();
        heartbeatUnsubscribe();
    });

    $scope.$watch('user.ui_data.selectedCustomer', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.setSelectedCustomer(newVal);
        }
    });

    $scope.actions = {
        showOnMap: {
            action: function (appliance) {
                var managedCustomer = _.get($rootScope.user, 'ui_data.selectedCustomer');
                $state.go('cato.loggedIn.dashboard.maps', {
                    appliance: appliance,
                    customer: $scope.gdata.customerMap[appliance.customer],
                    managedCustomerEmpty: managedCustomer === null,
                });
            },
            class: 'btn-white',
            icon: 'fa-map',
            label: 'Show on Map',
            show: function (appliance) {
                let heartbeat = $scope.heartbeats[appliance.id];
                let position = heartbeat ? heartbeat.position : undefined;
                return (
                    position &&
                    appliance.has_heartbeats &&
                    Object.keys(position).length > 0 &&
                    position.latitude !== 0 &&
                    position.longitude !== 0 &&
                    config.showMaps.toLowerCase() === 'true'
                );
            },
            tooltip: 'Show this appliance on a map',
        },
        setVolatileHeartbeat: {
            action: function (appliance) {
                $scope.setVolatileHeartbeat(appliance);
            },
            class: 'btn-white',
            icon: 'fa-clock-o',
            label: 'Heartbeat',
            show: function (appliance) {
                return $scope.gdata.isOperator && appliance.verified && !appliance.retired;
            },
            tooltip: 'Set Volatile Heartbeat',
        },
        rebootAppliance: {
            action: function (appliance) {
                $scope.rebootAppliance(appliance);
            },
            class: 'btn-white',
            icon: 'fa-fire-extinguisher',
            label: 'Reboot',
            show: function (appliance) {
                return $scope.gdata.isOperator && appliance.verified && !appliance.retired;
            },
            tooltip: 'Reboot this appliance',
        },
    };

    $scope.setSelectedCustomer = function (customer) {
        if (customer) {
            $scope.selectedCustomer = { id: customer.id, name: customer.name };
        } else {
            delete $scope.selectedCustomer;
        }
    };

    function doReboot(applianceId) {
        Appliances.reboot({ applianceId: applianceId }, {}).$promise.then(function (res) {
            modalService.openSuccess('Appliance rebooted!', 'Appliance will reboot on next heartbeat.', false);
        }, modalService.openErrorResponseCB('Unable to reboot this appliance!'));
    }

    $scope.checkPromises = {};
    $scope.rebootAppliance = function (appliance) {
        var customer = 'Unidentified ' + $rootScope.catotheme.customer;
        if (appliance.customer) {
            customer = $scope.gdata.customerMap[appliance.customer].name;
        }

        let msg = `${customer}'s appliance:<br>'${appliance.name}'${appliance.externalIP ? ` (IP: ${appliance.externalIP})` : ''}`;
        const p = Operations.paginateTasks({
            operationId: 0,
            operation__appliance: appliance.id,
            is_active: true,
            page_size: 1,
        }).$promise;
        p.then(function (results) {
            if (results.count !== 0) {
                msg += `?<br><br><br>WARNING: This appliance still has ${results.count} active task(s).
                        If any of those tasks are running when the appliance receives the reboot command, 
                        they will be interrupted and recorded as failed.
                        <br><br>Are you sure`;
            }
            modalService.openConfirm('Really reboot?', 'reboot', msg, appliance.id, doReboot);
        }, modalService.openErrorResponseCB('Could not check if appliance has any active tasks.'));
        $scope.checkPromises[appliance.id] = { promise: p, message: 'Checking for active tasks' };
    };

    $scope.setVolatileHeartbeat = function (appliance) {
        modalService.openConfirm(
            'Are you sure?',
            'want to change the interval?',
            'Shortening the callback interval ' +
                'increases the risk this appliance will be detected by network security systems or security ' +
                'personnel. Are you sure you wish to proceed',
            {},
            function () {
                $uibModal.open({
                    templateUrl: '/app/appliances/modalApplianceHeartbeat.html',
                    controller: 'HeartbeatCtrl',
                    size: 'lg',
                    scope: $scope,
                    resolve: {
                        params: {
                            appliance_id: appliance.id,
                            remove: false,
                        },
                    },
                });
            }
        );
    };

    $scope.notesModal = function (applianceId) {
        $uibModal.open({
            templateUrl: '/app/note/modalNoteDisplay.html',
            controller: 'NoteDisplayCtrl',
            size: 'lg',
            scope: $scope,
            resolve: {
                params: {
                    applianceId: applianceId,
                    type: 'appliance',
                },
            },
        });
    };

    $scope.setSelectedCustomer(_.get($rootScope.user, 'ui_data.selectedCustomer'));

    $scope.applianceCustomerFilter = function (appliance) {
        return !$scope.selectedCustomer || appliance.customer === null || appliance.customer === $scope.selectedCustomer.id;
    };

    $scope.getSortValue = function (sortType) {
        if (_.isUndefined(sortType)) {
            return undefined;
        }

        return function (item) {
            if (_.isString(sortType)) {
                return item[sortType];
            } else {
                return $parse(sortType['item'])($scope, { item: item });
            }
        };
    };
});
