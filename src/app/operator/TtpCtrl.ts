import app from '../app';

app.controller('TtpCtrl', function ($scope) {
    $scope.ttps = [
        { order: 1, description: 'Description 1', url: 'http://www.google.com?q=description1' },
        { order: 2, description: 'Description 2', url: 'http://www.google.com?q=description2' },
        { order: 3, description: 'Description 3', url: 'http://www.google.com?q=description3' },
        { order: 4, description: 'Description 4', url: 'http://www.google.com?q=description4' },
        { order: 5, description: 'Description 5', url: 'http://www.google.com?q=description5' },
    ];
});
