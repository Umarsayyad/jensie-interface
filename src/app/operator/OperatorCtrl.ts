import app from '../app';
import { config } from '@cato/config';
import * as _ from 'lodash';
import * as angular from 'angular';

import '../common/filters/applianceZoneOrExternal';
import '../common/operator/taskParameters';
import '../common/operator/TaskService';
import '../customer/manage/CondensedCustomerFilterComp';
import '../note/NoteCtrl';
import './TaskParametersCtrl';
import { getWebsocketUpdateCallback } from '../common/utils/getWebsocketUpdateCallback';
import { AttachmentService } from '../../ng-app/common/services/attachment.service';
import { MatDialog } from '@angular/material/dialog';
import { AttachmentComponent } from '../../ng-app/common/components/attachment-upload/attachment.component';

app.controller('OperatorCtrl', function (
    $scope,
    $state,
    $rootScope,
    $interval,
    $q,
    $timeout,
    $uibModal,
    MatDialog: MatDialog,
    AttachmentService: AttachmentService,
    globalDataService,
    modalService,
    socketService,
    applianceZoneOrExternalFilter,
    Appliances,
    OperationService,
    TaskService,
    CIDRBlocks,
    Findings,
    Messages,
    MetaTargets,
    Operations,
    Targets,
    ToolOutput
) {
    let outputUnsubscribe: () => void;
    const toolOutputMap = {};
    $scope.showWikiLink = config.linkToWiki;
    $scope.availableTargets = [];
    $scope.availableMetaTargets = [];
    $scope.availableAppliances = [];
    $scope.target = {};
    $scope.metatarget = {};
    $scope.appliance = {};
    $scope.busyMessage = 'Loading Operation';
    $scope.toutCurrentPage = 1;
    $scope.toutItemsPerPage = 10;

    if (_.isNil($scope.data)) {
        $scope.data = {};
    }

    $scope.toolOutputs = Operations.tooloutputsList({ operationId: $state.params.operation_id }, function (toolOutputs) {
        _.each(toolOutputs, setToolOutputFilename);
        outputUnsubscribe = socketService.subscribeByGroup(
            `op-operation-${$state.params.operation_id}-tooloutputs`,
            getWebsocketUpdateCallback($scope.toolOutputs, toolOutputMap, (toolOutput) => {
                setToolOutputFilename(toolOutput);
                $scope.toolOutputs.push(toolOutput);
                toolOutputMap[toolOutput.id] = toolOutput;
            })
        );
    });
    var my_user_id = $scope.user.id;
    $scope.results = {};
    $scope.fetchedLockedBy = false;
    $scope.lockingTargets = [];

    $scope.data.selectedFindings = [];

    $scope.$watch('results', function (newValue, oldValue) {
        $timeout(function () {
            $scope.results = {};
        }, 5000);
    });

    var newFindings = [];
    const applianceUnsubscribe = socketService.subscribeByGroup('op-appliances', () => {});
    $scope.$on('$destroy', function () {
        applianceUnsubscribe();
        if (outputUnsubscribe) outputUnsubscribe();
    });
    $scope.targetMap = {};

    $scope.targetnotes = [];

    function refreshTargetNotes() {
        if ($scope.data.operation.target) {
            Targets.getNotes({ targetId: $scope.data.operation.target.id, detail: true }, function (res) {
                $scope.targetnotes = res;
            });
        } else if ($scope.data.operation.metatarget) {
            MetaTargets.getNotes({ metaTargetId: $scope.data.operation.metatarget.id, detail: true }, function (res) {
                $scope.targetnotes = res;
            });
        }
    }

    $scope.deleteOperationNote = function (note) {
        modalService.openConfirm('Really delete?', 'delete this note', null, null, function () {
            Operations.deleteNote(
                { operationId: $scope.data.operation.id, noteId: note.id },
                note,
                function (note) {
                    _.remove($scope.data.operation.notes, { id: note.id });
                },
                function (res) {
                    modalService.openErrorResponse('Note could not be deleted.', res, false);
                }
            );
        });
    };

    $scope.deleteTargetNote = function (note) {
        modalService.openConfirm('Really delete?', 'delete this note', null, null, function () {
            if ($scope.data.operation.target) {
                Targets.deleteNote(
                    { targetId: $scope.target.id, noteId: note.id },
                    note,
                    function (note) {
                        _.remove($scope.data.operation.notes, { id: note.id });
                        refreshTargetNotes();
                    },
                    function (res) {
                        modalService.openErrorResponse('Note could not be deleted.', res, false);
                    }
                );
            } else if ($scope.data.operation.metatarget) {
                MetaTargets.deleteNote(
                    { metaTargetId: $scope.metatarget.id, noteId: note.id },
                    note,
                    function (note) {
                        _.remove($scope.data.operation.notes, { id: note.id });
                        refreshTargetNotes();
                    },
                    function (res) {
                        modalService.openErrorResponse('Note could not be deleted.', res, false);
                    }
                );
            }
        });
    };

    $scope.addNote = function (note, type) {
        note = angular.copy(note);
        if (type === 'target') {
            type = $scope.data.operation.target ? 'target' : 'metatarget';
        }

        var params = {
            note: note,
            type: type,
            operationId: undefined,
            targetId: undefined,
            metaTargetId: undefined,
        };

        if (type === 'operation') {
            params.operationId = $scope.data.operation.id;
        } else if (type === 'target') {
            params.targetId = $scope.data.operation.target.id;
        } else if (type === 'metatarget') {
            params.metaTargetId = $scope.data.operation.metatarget.id;
        }

        var modalInstance = $uibModal.open({
            templateUrl: '/app/note/modalNote.html',
            controller: 'NoteCtrl',
            size: 'lg',
            scope: $scope,
            resolve: {
                params: params,
            },
        });

        modalInstance.result.then(function (note) {
            var notes = null;
            if (type === 'operation') {
                notes = $scope.data.operation.notes;
            } else {
                notes = $scope.targetnotes;
            }

            _.remove(notes, { id: note.id });
            notes.push(note);
        });
    };

    function updateOperation(response) {
        //console.log('got OPERATION data back: ', response);
        $scope.updateObjOld($scope.data.operation, response);

        $scope.data.operation.startedBy = response.startedBy;
        //console.log('operation: ', $scope.data.operation);

        if (_.isNumber(response.target)) {
            Targets.get({ targetId: response.target }, function (target) {
                var t = _.find($scope.availableTargets, { id: response.target });
                $scope.updateObjOld(t, target);
                $scope.updateObjOld($scope.data.operation.target, target);
            });
        }

        if (_.isNumber(response.metatarget)) {
            MetaTargets.get({ metaTargetId: response.metatarget }, function (metatarget) {
                var mt: any = _.find($scope.availableMetaTargets, { id: response.metatarget });
                $scope.updateObjOld(mt, metatarget);
                $scope.updateObjOld($scope.data.operation.metatarget, metatarget);
                mt.targets = _.map(metatarget.targets, function (tid) {
                    return $scope.targetMap[tid];
                });
                $scope.data.operation.metatarget.targets = mt.targets; // Make sure they are both updated
                $scope.fetchedLockedBy = false;
            });
        }

        $scope.updateProgressBar();
    }

    function updateFindingList(finding) {
        // Only check if received finding is for the displayed op
        if (finding.operation === $scope.data.operation.id) {
            if ('deleted' in finding || finding.state !== 'new') {
                _.remove(newFindings, { id: finding.id });
                $scope.data.findingsContainNew = newFindings.length > 0;
            } else {
                var existingFinding = _.find(newFindings, { id: finding.id });
                if (existingFinding === undefined) {
                    newFindings.unshift(finding);
                }
                $scope.data.findingsContainNew = true;
            }
        }
    }

    function subscribeToChangesForOp(operation) {
        var operationUnsubscribe, findingUnsubscribe, tipoffeventUnsubscribe;
        if (
            $scope.gdata.isMissionDirector ||
            (operation.startedBy === $scope.user.id &&
                _.includes(['requested', 'in progress', 'paused', 'rejected', 'review', 'completed', 'canceled'], operation.state))
        ) {
            // Not using model subscription for author here so still get updates after op is requested
            // without reloading; will get duplicate messages for draft, fix up, and created states

            operationUnsubscribe = socketService.subscribeByModel('operation', function (operation) {
                //console.log('got operation data back: ', operation);
                if (operation.id === $scope.data.operation.id) {
                    // Only update if received op is the same as the displayed one
                    updateOperation(operation);
                }
            });
            findingUnsubscribe = socketService.subscribeByModel('finding', function (finding) {
                //console.log('got FINDING data back: ', finding);
                updateFindingList(finding);
            });
        } else if ($scope.gdata.isOperator) {
            // This operator is not already getting notified about this operation, so subscribe directly
            operationUnsubscribe = socketService.subscribeByGroup('op-operation-' + operation.id, updateOperation);
            findingUnsubscribe = socketService.subscribeByGroup('op-operation-' + operation.id + '-findings', function (finding) {
                //console.log('got FINDING data back: ', finding);
                updateFindingList(finding);
            });
            tipoffeventUnsubscribe = socketService.subscribeByGroup('op-operation-' + operation.id + '-tipoffevents', _.noop); // The NotificationService will handle the rest
        }

        const taskUnsubscribe = socketService.subscribeByGroup(
            `op-operation-${$state.params.operation_id}-tasks`,
            getWebsocketUpdateCallback(operation.tasks)
        );
        $scope.$on('$destroy', function () {
            operationUnsubscribe();
            findingUnsubscribe();
            taskUnsubscribe();
            if (tipoffeventUnsubscribe) {
                tipoffeventUnsubscribe();
            }
        });
    }

    $scope.viewRejected = function () {
        return (
            $scope.data.operation &&
            $scope.data.operation.state === 'rejected' &&
            ($scope.gdata.isMissionDirector || $scope.data.operation.startedBy === my_user_id)
        );
    };

    $scope.stateIndex = function (state) {
        return _.findIndex($scope.enabledStates, { label: state });
    };

    $scope.stateClass = OperationService.stateClass;

    $scope.updateProgressBar = function () {
        // get a fresh copy of the operation states
        var operationStates = _.cloneDeep(OperationService.STATES);
        // enable the current state
        var state: any = _.find(operationStates, { label: $scope.data.operation.state });
        state.enabled = true;
        // filter the states to only return the enabled ones
        $scope.enabledStates = _.filter(operationStates, { enabled: true });
        // determine the percentage width for the states based on those enabled
        $scope.interval = Math.round(100 / $scope.enabledStates.length);
    };

    function hoursMinutesSeconds(s) {
        var sec = s;
        var days = Math.floor(sec / (3600 * 24));
        var remainingHours = sec - days * 3600 * 24;
        var hours = Math.floor(remainingHours / 3600);
        var remainingMinutes = remainingHours - hours * 3600;
        var minutes = Math.floor(remainingMinutes / 60);
        var seconds = Math.floor(remainingMinutes - minutes * 60);

        //Replaced ifs with ternaries in order to not have to
        //redefine variables from numbers to strings
        var output =
            (hours < 10 ? '0' + hours : hours) + ':' + (minutes < 10 ? '0' + minutes : minutes) + ':' + (seconds < 10 ? '0' + seconds : seconds);
        if (days !== 0) {
            output = days + ' days ' + output;
        }
        return output;
    }

    $scope.viewDeleteOp = function (operation) {
        if (!operation) {
            return false;
        }

        if ($scope.gdata.isMissionDirector) {
            return !(operation.state === 'in progress' || operation.state === 'requested');
        }

        return (operation.state === 'created' || operation.state === 'draft') && $scope.gdata.isOperator;
    };

    $scope.viewCancelOp = function (operation) {
        if (operation) {
            // doing it this way so as not to get errors when operation is undefined
            return true;
        }
        return false;
    };

    $scope.viewApproveDisapprove = function (operation) {
        if (operation) {
            if ($scope.gdata.isMissionDirector && operation.state === 'draft') {
                return true;
            }
        }
        return false;
    };

    $scope.viewAuthorizeReject = function (operation) {
        if (operation) {
            if ($scope.gdata.isMissionDirector && operation.state === 'requested') return true;
        }
        return false;
    };

    $scope.actions = {
        // <button class="btn btn-default"
        //         ng-if="data.operation.retestData.length"
        //         uib-tooltip="Retest Finding Details"
        //         ng-click="showRetestDetails(data.operation)">
        //     <i class="fa fa-retweet"></i>
        //     <span ng-bind="data.operation.retestData.length"></span>
        // </button>
        retestDetails: {
            action: function (operation) {
                OperationService.showRetestDetails(operation);
            },
            class: 'btn-white',
            icon: 'fa-retweet',
            label: 'Retest',
            show: function (operation) {
                return operation && operation.retestData.length > 0;
            },
            tooltip: 'Retest Finding Details',
        },
        approveOperation: {
            action: function (operation) {
                Operations.approve(
                    { operationId: operation.id },
                    {},
                    function (op) {
                        $scope.updateObjOld($scope.data.operation, op);
                        $scope.updateProgressBar();
                    },
                    modalService.openErrorResponseCB()
                );
            },
            class: 'btn-white',
            icon: 'fa-thumbs-o-up text-success',
            label: 'Approve',
            show: $scope.viewApproveDisapprove,
            tooltip: 'Approve Operation',
        },
        authorizeOperation: {
            action: function (operation) {
                Operations.authorize(
                    { operationId: operation.id },
                    {},
                    function (op) {
                        $scope.updateObjOld($scope.data.operation, op);
                        $scope.updateProgressBar();
                    },
                    modalService.openErrorResponseCB()
                );
            },
            class: 'btn-white',
            icon: 'fa-thumbs-o-up text-success',
            label: 'Authorize',
            show: $scope.viewAuthorizeReject,
            tooltip: 'Authorize Operation',
        },
        cancelRequestStart: {
            action: function (operation) {
                Operations.cancel_request_start(
                    { operationId: operation.id },
                    {},
                    function (op) {
                        //console.log('op: ', op);
                        $scope.updateObjOld($scope.data.operation, op);
                        $scope.updateProgressBar();
                    },
                    modalService.openErrorResponseCB()
                );
            },
            class: 'btn-white',
            icon: 'fa-thumbs-o-up text-success',
            label: 'Release',
            show: $scope.viewRejected,
            tooltip: $scope.gdata.isMissionDirector ? 'Re-release Operation' : 'Acknowledge Operation Rejection',
        },
        cancelOperation: {
            action: function (operation) {
                modalService.openCancel('Confirmation', 'this operation', {}, function () {
                    $scope.busyMessage = 'Canceling Operation';
                    $scope.operationLoading = Operations.cancel(
                        { operationId: operation.id },
                        {},
                        function (op) {
                            $state.go('cato.loggedIn.dashboard');
                        },
                        modalService.openErrorResponseCB()
                    ).$promise;
                });
            },
            class: 'btn-white',
            icon: 'fa-times-circle-o text-danger',
            label: 'Cancel',
            show: $scope.viewCancelOp,
            tooltip: 'Cancel Operation',
        },
        deleteOperation: {
            action: function (operation) {
                modalService.openDelete('Confirmation', 'this operation', {}, function () {
                    $scope.busyMessage = 'Deleting Operation';
                    $scope.operationLoading = Operations.delete({ operationId: operation.id }, {}, function (op) {
                        $state.go('cato.loggedIn.dashboard.operations');
                    }).$promise;
                });
            },
            class: 'btn-white',
            icon: 'fa-trash-o text-danger',
            label: 'Delete',
            show: $scope.viewDeleteOp,
            tooltip: 'Delete Operation',
        },
        disapproveOperation: {
            action: function (operation) {
                if (operation) {
                    $scope.data.selectedDraftOperations = [{ id: operation.id }];
                }
                $uibModal.open({
                    templateUrl: '/app/operation/modalDisapproveOperation.html',
                    controller: 'RejectionCtrl',
                    size: 'lg',
                    scope: $scope,
                    resolve: {
                        params: {
                            selObjs: 'selectedDraftOperations',
                            objs: 'operation',
                            type: 'operation',
                            action: 'disapprove',
                            remove: false,
                        },
                    },
                });
            },
            class: 'btn-white',
            icon: 'fa-thumbs-o-down text-danger',
            label: 'Disapprove',
            show: $scope.viewApproveDisapprove,
            tooltip: 'Disapprove Operation',
        },
        duplicateOperation: {
            action: function (operation) {
                OperationService.duplicateOperation(operation);
            },
            class: 'btn-white',
            icon: 'fa-copy',
            label: 'Duplicate',
            show: true,
            tooltip: 'Duplicate Operation',
        },
        rejectOperation: {
            action: function (operation) {
                if (operation) {
                    $scope.data.selectedRequestedOperations = [{ id: operation.id }];
                }
                $uibModal.open({
                    templateUrl: '/app/md/modalRejectOperation.html',
                    controller: 'RejectionCtrl',
                    size: 'lg',
                    scope: $scope,
                    resolve: {
                        params: {
                            selObjs: 'selectedRequestedOperations',
                            objs: 'operation',
                            type: 'operation',
                            action: 'reject',
                            remove: false,
                        },
                    },
                });
            },
            class: 'btn-white',
            icon: 'fa-thumbs-o-down text-danger',
            label: 'Reject',
            show: $scope.viewAuthorizeReject,
            tooltip: 'Reject Operation',
        },
        // viewAnalysis: {
        //     action: function(operationId) {
        //         $uibModal.open({
        //             templateUrl: '/app/operator/modalAnalyzeResults.html',
        //             controller: 'AnalyzeResultsCtrl',
        //             size: 'lg',
        //             scope: $scope,
        //             resolve: {
        //                 params: {
        //                     operationId: operationId
        //                 }
        //             }
        //         });
        //     },
        //     class: "",
        //     icon: "fa-line-chart text-success",
        //     label: "Results",
        //     show: function(){
        //         var returnVal = $scope.data.operation ? $scope.data.operation.state === 'review' : false;
        //         return returnVal;
        //     },
        //     tooltip: "View Analysis Results"
        // },
        analyzeOperation: {
            action: function (operation) {
                Operations.analyze(
                    { operationId: operation.id },
                    {},
                    function (res) {
                        modalService.openSuccess('Analysis started successfully', angular.toJson(res, true));
                    },
                    function (err) {
                        modalService.openErrorResponse('Failed to Run Analysis', err, false);
                    }
                );
            },
            class: 'btn-white',
            icon: 'fa-line-chart text-success',
            label: 'Analyze',
            show: function () {
                var returnVal = $scope.data.operation
                    ? $scope.data.operation.state === 'review' || $scope.data.operation.state === 'completed'
                    : false;
                return returnVal;
            },
            tooltip: 'Analyze Operation',
        },
    };

    function changeTargetOrMetaTarget(operationUpdate) {
        /* Just FYI, you can't update the Target if the Operation is active, the backend won't let it. */

        /* clean out the extra stuff and set it up to be properly update-able. */

        operationUpdate.campaign = operationUpdate.campaign.id;
        operationUpdate.template = operationUpdate.template.id;

        if (operationUpdate.author) {
            operationUpdate.author = operationUpdate.author.id;
        } else {
            delete operationUpdate.author;
        }
        if (operationUpdate.startedBy) {
            operationUpdate.startedBy = operationUpdate.startedBy.id;
        } else {
            delete operationUpdate.startedBy;
        }
        if (operationUpdate.appliance) {
            operationUpdate.appliance = operationUpdate.appliance.id;
        } else {
            delete operationUpdate.appliance;
        }

        Operations.update({ operationId: $scope.data.operation.id }, operationUpdate)
            .$promise.then(function (result) {
                if (operationUpdate.target) {
                    $scope.data.operation.target = _.find($scope.availableTargets, { id: operationUpdate.target });
                    // The model allows for multiple cidr blocks, but our logic currently doesn't allow them to overlap, so effectively a target can only have one.
                } else {
                    $scope.data.operation.target = undefined;
                }
                if (operationUpdate.metatarget) {
                    $scope.data.operation.metatarget = _.find($scope.availableMetaTargets, { id: operationUpdate.metatarget });
                } else {
                    $scope.data.operation.metatarget = undefined;
                }
                $scope.fetchedLockedBy = false;

                refreshTargetNotes();
                loadNetworks();
            }, modalService.openErrorResponseCB())
            .catch(function (result) {
                console.log('failed: ' + JSON.stringify(result, null, 2));
            });
    }

    $scope.changeMetaTarget = function () {
        /* the operator is trying to change the MetaTarget. */
        var operationUpdate: any = {};
        angular.copy($scope.data.operation, operationUpdate);
        //console.log('changeMetaTarget: ' + JSON.stringify($scope.metatarget, null, 2));
        if ($scope.metatarget !== undefined) {
            operationUpdate.metatarget = $scope.metatarget.id || null;
        }

        delete operationUpdate.target;

        changeTargetOrMetaTarget(operationUpdate);
    };

    $scope.changeTarget = function () {
        /* the operator is trying to change the target. */
        var operationUpdate: any = {};
        angular.copy($scope.data.operation, operationUpdate);
        //console.log('changeTarget: ' + JSON.stringify($scope.target, null, 2));
        operationUpdate.target = $scope.target.id || null;

        changeTargetOrMetaTarget(operationUpdate);
    };

    $scope.changeAppliance = function (appliance) {
        /* the operator is trying to change the appliance. */
        var operationUpdate: any = {};
        // clean up the data for submission
        _.each($scope.data.operation, function (v, k) {
            if (_.isNil(v) || _.isArray(v)) {
                //skip
            } else if (_.isNumber(v.id)) {
                operationUpdate[k] = v.id;
            } else {
                operationUpdate[k] = v;
            }
        });

        operationUpdate.appliance = appliance.id || null;

        // Set the associated target or metatarget to null if it isn't compatible with the new appliance
        if (
            $scope.data.operation.target &&
            applianceZoneOrExternalFilter([$scope.data.operation.target], appliance.id, $scope.data.operation.customer.id).length === 0
        ) {
            operationUpdate.target = null;
        } else if (
            $scope.data.operation.metatarget &&
            applianceZoneOrExternalFilter([$scope.data.operation.metatarget], appliance.id, $scope.data.operation.customer.id).length === 0
        ) {
            operationUpdate.metatarget = null;
        }

        Operations.update(
            { operationId: $scope.data.operation.id },
            operationUpdate,
            function (result) {
                $scope.data.operation.appliance = $scope.gdata.applianceMap[result.appliance];
                $scope.appliance = $scope.data.operation.appliance;
                if (operationUpdate.target === null) {
                    $scope.data.operation.target = null;
                    $scope.target = {};
                }
                if (operationUpdate.metatarget === null) {
                    $scope.data.operation.metatarget = null;
                    $scope.metatarget = {};
                }
            },
            modalService.openErrorResponseCB('Could not change appliance')
        );
    };

    function targetIsUndefined(operation) {
        return _.isNil(_.get(operation, 'target.value'));
    }

    function metaTargetIsUndefined(operation) {
        return _.isNil(_.get(operation, 'metatarget.name'));
    }

    function targetIsLocked(operation) {
        return _.get(operation, 'target.locked');
    }

    function metaTargetIsLocked(operation) {
        $scope.metaTargetLockedBy(operation);
        return _.get(operation, 'metatarget.locked');
    }

    $scope.metaTargetLockedBy = function (operation) {
        if (!$scope.fetchedLockedBy) {
            $scope.lockingTargets = [];
            if (!_.isNil(operation.metatarget) && $scope.availableMetaTargets.length > 0) {
                var mt: any = _.find($scope.availableMetaTargets, { id: operation.metatarget.id });
                //console.log('mt: ', mt);
                if (!_.isNil(mt) && !_.isNumber(mt.targets[0])) {
                    $scope.fetchedLockedBy = true;
                    $scope.lockingTargets = _.filter(mt.targets, 'locked');
                }
            }
        }
    };

    $scope.isSubmitButtonDisabled = function (operation) {
        if (!operation) {
            return true;
        }

        if (!operation.active) {
            return true;
        } else if (targetIsUndefined(operation) && metaTargetIsUndefined(operation)) {
            return true;
        } else if (operation.startedBy !== my_user_id && !$scope.gdata.isMissionDirector) {
            return true;
        } else if (operation.state !== 'in progress' && !$scope.data.findingsContainNew && operation.state === 'review') {
            return true;
        } else if (operation.state !== 'in progress' && $scope.data.findingsContainNew && operation.state === 'review') {
            return false;
        } else if ($scope.hasActiveTasks($scope.data.operation)) {
            return true;
        } else if (operation.state !== 'in progress') {
            return true;
        } else {
            return false;
        }
    };

    $scope.manualTaskRunning = function (operation) {
        return (
            operation &&
            operation.tasks.some(function (task) {
                return task.status === TaskService.STATE_STARTED && task.process === 'manual';
            })
        );
    };

    $scope.previousTaskNotDone = function (operation, taskNumber) {
        if (taskNumber == 0) return false;
        var order = (taskNumber - 1).toString();
        var previousTask = _.find(operation.tasks, { order: order });
        return operation.active === false || (previousTask && !TaskService.taskCompleted(previousTask));
    };

    $timeout(function () {
        $scope.showStartTasks = false;
        $scope.showStopTasks = false;
    }, 10000);

    $scope.isStateButtonDisabled = function (operation) {
        if (!operation) {
            return true;
        }

        //console.log(operation.target)

        if ($rootScope.user.level < operation.template.requiredLevel) {
            return true;
        } else if (operation.target && !operation.target.active) {
            return true;
        } else if (operation.active && operation.state !== 'in progress') {
            return true;
        } else if (targetIsUndefined(operation) && metaTargetIsUndefined(operation)) {
            return true;
        } else if (
            (targetIsLocked(operation) || metaTargetIsLocked(operation) || $scope.lockingTargets.length > 0) &&
            operation.state !== 'in progress'
        ) {
            return true;
        } else if (operation.state !== 'created' && operation.state !== 'in progress' && operation.state !== 'paused') {
            return true;
        } else if ($scope.hasActiveTasks(operation)) {
            return true;
        } else if (!_.isNil(operation.startedBy) && operation.startedBy !== $scope.user.id) {
            if ($scope.gdata.isMissionDirector) {
                // allow the MD to take action, even if they don't own the operation
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    };

    function isTaskManualOrLocalOnly(task) {
        return task.process === 'manual' || task.is_local_only;
    }

    $scope.startTaskButtonDisabled = function (operation, task) {
        if (!operation) {
            return true;
        } else if (
            operation.startedBy !== $scope.user.id &&
            $scope.user.level < operation.template.requiredLevel &&
            !$scope.gdata.isMissionDirector
        ) {
            return 'You do not have permission to start this task.';
        } else if (operation.active === false) {
            return 'Operation is not active.';
        } else if (operation.state !== 'in progress') {
            return 'Operation is not in progress.';
        } else if (TaskService.taskStarted(task)) {
            return 'Task has already been started.';
        } else if ($scope.previousTaskNotDone(operation, task.order)) {
            return 'Previous task is not complete.';
        } else if (!_.isEmpty($scope.appliance) && $scope.appliance.reboot && !isTaskManualOrLocalOnly(task)) {
            return 'Operation appliance has a pending reboot.';
        } else if (
            !_.isEmpty($scope.appliance) &&
            ($scope.appliance.missed_beat_alert || !$scope.appliance.has_heartbeats) &&
            !isTaskManualOrLocalOnly(task)
        ) {
            return 'Operation appliance is missing heartbeats.';
        }
    };

    $scope.stopTaskButtonDisabled = function (operation, task) {
        if (!operation) {
            return 'Operation is not set.';
        } else if (
            operation.startedBy !== $scope.user.id &&
            $scope.user.level < operation.template.requiredLevel &&
            !$scope.gdata.isMissionDirector
        ) {
            return 'You do not have permission to stop this task.';
        } else if (operation.active === false) {
            return 'Operation is not active.';
        } else if (operation.state !== 'in progress') {
            return 'Operation is not in progress.';
        } else if (!TaskService.taskActive(task)) {
            return 'Task is not running.';
        }
    };

    $scope.showForceStopTaskButton = function (task) {
        return task.status === TaskService.STATE_ABORTING;
    };

    $scope.reportLockingOrInactive = function (titleString, err) {
        var errorString = err;
        if (_.has(err, 'locked_targets') && _.has(err, 'inactive_targets')) {
            var mapTargets = function (elem) {
                return _.get(err, elem, [])
                    .map(function (tar_id) {
                        return $scope.targetMap[tar_id];
                    })
                    .map(function (tar) {
                        return tar.value;
                    });
            };
            var locked_targets = _.join(mapTargets('locked_targets'), '\n');
            var inactive_targets = _.join(mapTargets('inactive_targets'), '\n');
            errorString = 'Attempted and failed to lock targets.\n';
            if (locked_targets) {
                errorString += 'Blocked by locked targets: \n' + locked_targets + '\n';
            }
            if (inactive_targets) {
                errorString += 'Blocked by inactive targets: \n' + inactive_targets + '\n';
            }
        }
        modalService.openErrorResponse(titleString, errorString, false);
    };

    $scope.assignAndStartOrPauseOp = function (operation) {
        var operationId = operation.id;
        if (operation.state === 'in progress') {
            $scope.busyMessage = 'Pausing Operation';
            $scope.operationLoading = Operations.pause({ operationId: operationId }, {}, _.noop, modalService.openErrorResponseCB()).$promise;
            return;
        } else if (operation.state === 'paused') {
            $scope.busyMessage = 'Unpausing Operation';
            $scope.operationLoading = Operations.unpause({ operationId: operationId }, {}, _.noop).$promise.catch(function (err) {
                $scope.reportLockingOrInactive('Error un-pausing operation', err);
            });
            return;
        }

        $scope.busyMessage = 'Requesting Operation';
        $scope.operationLoading = Operations.request_start({ operationId: operationId }, {}, function (op) {
            $scope.updateObjOld($scope.data.operation, op);
        }).$promise.catch(function (err) {
            $scope.reportLockingOrInactive('Error starting operation', err);
        });
    };

    $scope.stopOp = function (operationId) {
        /* Need to submit this for review, it will find all findings for this operation and if they're set as 'new'
         * it sets them as 'pending'.
         */
        function doRequestReview() {
            $scope.busyMessage = 'Submitting Findings';
            $scope.operationLoading = Operations.request_review(
                { operationId: operationId },
                {},
                function (op) {
                    $scope.data.findingsContainNew = false;
                    newFindings = [];
                    $scope.updateObjOld($scope.data.operation, op);
                },
                modalService.openErrorResponseCB()
            ).$promise;
        }

        if ($scope.data.operation.tasks.every(TaskService.taskCompleted)) {
            doRequestReview();
        } else {
            modalService.openConfirm('Tasks are not finished', '', 'submit this operation for review with incomplete tasks', null, doRequestReview);
        }
    };

    $scope.showLockingTargets = function (lockingTargets) {
        console.log('lockingTargets: ', lockingTargets);

        var modal = $uibModal.open({
            templateUrl: '/app/operator/modalLockingTargets.html',
            controller: 'LockingTargetsCtrl',
            size: 'lg',
            resolve: {
                params: {
                    lockingTargets: lockingTargets,
                },
            },
        });
    };

    $scope.viewParameters = function (task) {
        if (TaskService.taskStarted(task)) {
            $uibModal.open({
                templateUrl: '/app/operator/modalTaskParameters.html',
                controller: 'TaskParametersCtrl',
                size: 'lg',
                resolve: {
                    task: task,
                    readonly: true,
                },
            });
        }
    };

    $scope.viewLogs = function (task) {
        const operationId = $scope.data.operation.id;
        $uibModal.open({
            templateUrl: '/app/operator/modalTaskLogs.html',
            size: 'xl',
            controller: function ($scope) {
                $scope.logs = Operations.getTaskLogs({ operationId, taskId: task.id }, (logs) => {
                    if (logs.length > 0) logs[0].open = true;
                });
                $scope.task = task;
                $scope.toggleAll = () => {
                    $scope.logs.forEach((l) => {
                        l.open = $scope.allOpen;
                    });
                };
            },
        });
    };

    $scope.startTask = function (operation, task, target) {
        if ($scope.startTaskButtonDisabled(operation, task)) {
            console.log('Because: ', $scope.startTaskButtonDisabled(operation, task));
            return;
        }

        if (_.size(task.accepted_parameters) > 0) {
            var modal = $uibModal.open({
                templateUrl: '/app/operator/modalTaskParameters.html',
                controller: 'TaskParametersCtrl',
                size: 'lg',
                resolve: {
                    task: task,
                    readonly: false,
                },
            });
            modal.result.then(function (rtask) {
                Operations.startTask(
                    { operationId: operation.id, taskId: task.id },
                    { parameters: rtask.parameters },
                    function (ntask) {
                        $scope.updateObjOld(task, ntask);
                    },
                    function (err) {
                        modalService.openErrorResponse('Error running task', err, false);
                    }
                );
            });
        } else {
            Operations.startTask(
                { operationId: operation.id, taskId: task.id },
                {},
                function (ntask) {
                    $scope.updateObjOld(task, ntask);
                },
                function (err) {
                    modalService.openErrorResponse('Error running task', err, false);
                }
            );
        }
    };

    $scope.stopTask = function (operation, task) {
        if ($scope.stopTaskButtonDisabled(operation, task)) {
            return;
        }

        function doStopTask() {
            Operations.stopTask(
                { operationId: operation.id, taskId: task.id },
                {},
                function (ntask) {
                    $scope.updateObjOld(task, ntask);
                    if (!$scope.data.operation.taskLogTimes) {
                        $scope.data.operation.taskLogTimes = {};
                    }
                    $scope.data.operation.taskLogTimes[ntask.id] = ntask.logged;
                    $scope.data.operation.logged = Object.values($scope.data.operation.taskLogTimes).reduce((a: number, b: number) => a + b, 0);
                },
                modalService.openErrorResponseCB('Error stopping task')
            );
        }

        if (task.process === 'automated' || task.status !== TaskService.STATE_STARTED) {
            // If automated or manual and processing uploaded tool output
            modalService.openConfirm('Confirmation', 'abort', 'task ' + task.name, null, doStopTask);
        } else {
            doStopTask();
        }
    };

    $scope.forceStopTask = function (operationId, task) {
        var forceAbortWarning =
            '<div class="text-left"><br><br>Forcibly setting a task to aborted should only be used as a last resort if you are 100% positive that the task has already stopped running but is stuck in the "aborting" state because something prevented it from being reported as failed or aborted. <br>Improper use of this action has the potential to put the system into an undesired state where multiple tasks are executing against the target at the same time. <br>After aborting a task on an appliance, you <b>MUST NOT</b> use this action until after the appliance has had at least one heartbeat to receive the abort request, otherwise the appliance will not receive the abort request and the task will continue executing with no way to stop it other than to reboot the appliance. </div><br><br>Are you sure you want to proceed with this dangerous action';
        // TODO: once we can check that an appliance has received the abort request, update the message to say "You cannot forcibly abort a task on an appliance until after the appliance has had a heartbeat to receive the initial stop instruction."
        modalService.openConfirm('Confirmation', 'forcibly abort', 'task ' + task.name + '? ' + forceAbortWarning, null, function () {
            Operations.forceStopTask(
                { operationId: operationId, taskId: task.id },
                {},
                function (ntask) {
                    $scope.updateObjOld(task, ntask);
                },
                modalService.openErrorResponseCB('Error forcibly stopping task')
            );
        });
    };

    $scope.taskTimeLogged = function (time) {
        return hoursMinutesSeconds(time);
    };

    // We still need to retrieve and monitor findings to make sure we know about new findings beyond the first page
    // TODO: Use Findings.basic_list() once target visualization is merged (this will also deal with if there are more than 1000 new findings)
    Findings.get({ operation: $state.params.operation_id, state: 'new', page_size: 30 }, function (findings) {
        newFindings = findings.results;
        $scope.data.findingsContainNew = newFindings.length > 0;
    });

    function loadNetworks() {
        var ops = $scope.data.operation;
        if (ops.target && ops.target.type === 'ipaddress') {
            $scope.cidrBlocks = [];

            CIDRBlocks.get({ target: ops.target.id }, function (result) {
                $scope.cidrBlocks = result['results'];
            });
        }
    }

    $scope.busyMessage = 'Loading Operation';
    $scope.operationLoading = $scope.operationArr = Operations.basic({ operationId: $state.params.operation_id }, function (ops) {
        // NOTE: you will need `debugger` to troubleshoot this function, since the promise swallows errors

        if (ops.tasks.length > 0 && ops.tasks[0].process === 'manual') {
            ops.vmcreds = $scope.user.username; //[$scope.user.username, ops.name, ops.target.value].join('__').replace(/\W/g,'_')
        }

        //console.log('operation: ', ops);
        $scope.customer = ops.customer;
        Appliances.list({ customer: ops.customer.id }, (appliances) => {
            _.each(appliances, (appliance) => {
                globalDataService.updateAppliance(appliance);
            });
        });

        $scope.data.operation = ops;
        $scope.showStartTasks =
            !$scope.hasActiveTasks(ops) &&
            ops.state === 'in progress' &&
            !ops.tasks.every(TaskService.taskStarted) &&
            ($scope.user.id === ops.startedBy || $scope.gdata.isMissionDirector || $scope.user.level >= ops.template.requiredLevel);

        $scope.showStopTasks =
            $scope.hasActiveTasks(ops) &&
            ops.state === 'in progress' &&
            $scope.manualTaskRunning(ops) &&
            ($scope.user.id === ops.startedBy || $scope.gdata.isMissionDirector || $scope.user.level >= ops.template.requiredLevel);

        //console.log('indexOfState: ', $scope.states.indexOf($scope.data.operation.state.toUpperCase()));

        $scope.updateProgressBar();

        if (ops.target) {
            $scope.target = ops.target;
        }

        if (ops.metatarget) {
            $scope.metatarget = ops.metatarget;
        }

        var targetQueryParams = {
            active: true,
            type: $scope.data.operation.template.targettypes,
            customer: $scope.data.operation.customer.id,
        };

        loadNetworks();

        $scope.availableTargets = Targets.basicList(targetQueryParams, function (targets) {
            // Add 'Remove target' option to beginning of list
            targets.unshift({
                value: 'Remove Target',
                id: undefined,
                type: '',
                active: true,
                zone: $scope.gdata.customerExternalZoneMap[$scope.data.operation.customer.id].id,
            });
            _.each(targets, function (t) {
                $scope.targetMap[t.id] = t;
            });
        });

        $scope.availableMetaTargets = MetaTargets.list(targetQueryParams, function (metatargets) {
            // Add 'Remove metatarget' option to beginning of list
            metatargets.unshift({
                name: 'Remove MetaTarget',
                id: undefined,
                type: '',
                active: true,
                zone: $scope.gdata.customerExternalZoneMap[$scope.data.operation.customer.id].id,
            });
        });

        $q.all([$scope.availableTargets.$promise, $scope.availableMetaTargets.$promise]).then(function () {
            _.each($scope.availableMetaTargets, function (mt) {
                mt.targets = _.map(mt.targets, function (tid) {
                    return $scope.targetMap[tid];
                });
            });
        });

        $scope.gdata.appliancesByCustomerPromise.then(function () {
            // Add 'Remove appliance' option to beginning of list
            var appliances: any = _.filter($scope.gdata.appliancesByCustomer[$scope.data.operation.customer.id], {
                verified: true,
                retired: false,
            });
            appliances = _.sortBy(appliances, ['name']);
            appliances.unshift({ name: 'Remove Appliance', id: undefined, verified: true });
            $scope.availableAppliances = appliances;
        });
        $scope.appliance = _.get($scope.gdata.applianceMap, _.get($scope.data.operation, 'appliance.id'), {});

        _.each($scope.data.operation.tasks, function (task) {
            task.showUpload = task.process === 'manual' && task.application !== 'N/A'; // TODO: get default application from API or just keep up to date here
        });
        refreshTargetNotes();
        subscribeToChangesForOp($scope.data.operation);
    }).$promise;

    $scope.dangerClass = function () {
        if ($scope.isCanceled() || $scope.viewRejected()) {
            return 'text-danger';
        }
    };

    $scope.isCanceled = function () {
        return _.get($scope.data, 'operation.state') === 'canceled';
    };

    $scope.viewCancelOp = function (operation) {
        if (operation) {
            // doing it this way so as not to get errors when operation is undefined
            if (
                (!$scope.isCanceled && operation.startedBy === my_user_id) ||
                ((operation.state === 'created' || operation.state === 'draft') && operation.author === my_user_id) ||
                $scope.gdata.isMissionDirector
            ) {
                return true;
            }
        }
    };

    $scope.getNetworks = function () {
        if ($scope.cidrBlocks.length) {
            let networks = [];
            for (var i = 0; i < $scope.cidrBlocks.length; i++) {
                networks.push($scope.cidrBlocks[i].network_name);
            }
            return networks;
        }

        return [];
    };

    $scope.taskActive = function (task) {
        return TaskService.taskActive(task) && task.status !== TaskService.STATE_ABORTING;
    };
    $scope.taskDirty = function (task) {
        return TaskService.taskDirty(task);
    };
    $scope.taskSuccess = function (task) {
        return task.status === TaskService.STATE_FINISHED;
    };

    $scope.hasActiveTasks = function (operation) {
        if (!operation) {
            return false;
        }

        return _.some(operation.tasks, 'active');
    };

    $scope.getStateButtonLabel = function () {
        var operation = $scope.data.operation;
        if (!operation) {
            return 'Loading...';
        }

        if ($rootScope.user.level < operation.template.requiredLevel) {
            return 'Insufficient operator level';
        } else if ($scope.hasActiveTasks(operation)) {
            return 'Operation has active tasks';
        } else if (operation.target && !operation.target.active) {
            return 'Target is not active';
        } else if (operation.state === 'draft') {
            return 'Operation pending approval';
        } else if (operation.state === 'fix up') {
            return 'Operation creation has been rejected';
        } else if (operation.state === 'created' && targetIsUndefined(operation) && metaTargetIsUndefined(operation)) {
            if (targetIsUndefined(operation)) {
                return 'Target or MetaTarget is undefined';
            } else {
                return 'Request operation';
            }
        } else if (operation.state === 'created' && (targetIsLocked(operation) || metaTargetIsLocked(operation))) {
            if (targetIsLocked(operation)) {
                return 'Target is locked';
            } else if (metaTargetIsLocked(operation)) {
                return 'MetaTarget is locked';
            }
        } else if (operation.state === 'created') {
            return 'Request operation';
        } else if (operation.state === 'requested') {
            return 'Operation already requested';
        } else if (operation.state === 'rejected') {
            return 'Operation execution has been rejected';
        } else if (operation.state === 'in progress') {
            return 'Pause';
        } else if (operation.state === 'paused') {
            return 'Unpause';
        } else if (operation.state === 'review') {
            return 'Operation pending review';
        } else if (operation.state === 'canceled') {
            return 'Operation has been canceled';
        }
    };

    $scope.loadOutput = function (tooloutput) {
        if (tooloutput.result === undefined && !tooloutput.loadStarted) {
            tooloutput.loadStarted = true;
            tooloutput.$promise = Operations.tooloutput({ operationId: $state.params.operation_id, tooloutputId: tooloutput.id }).$promise.then(
                function (response) {
                    tooloutput.result = response.result;
                },
                function (error) {
                    modalService.openErrorResponse('Tool output could not be retrieved', error, false);
                }
            );
        }
    };

    $scope.upload = function upload($file, task) {
        if ($file !== null) {
            const reader = new FileReader();
            reader.addEventListener('loadend', function () {
                let result = <string>reader.result;
                if (task.binary_input && !task.uploadText) {
                    result = JSON.stringify(btoa(result));
                }
                const toolOutput = {
                    result: result,
                    task: task.id,
                };
                ToolOutput.create(toolOutput); // Websocket will handle adding it to the list
            });
            if (task.binary_input && !task.uploadText) {
                reader.readAsBinaryString($file);
            } else {
                reader.readAsText($file);
            }
        }
    };

    $scope.allowUpload = function (task, operation) {
        return task.showUpload && operation.active && (task.status === TaskService.STATE_STARTED || task.status === TaskService.STATE_AWAITING);
    };

    $scope.uploadAttachment = function () {
        MatDialog.open(AttachmentComponent)
            .afterClosed()
            .subscribe((queuedAttachments: File[]) => {
                ($scope.messageFormData.queuedAttachments = $scope.messageFormData.queuedAttachments || []).push(...queuedAttachments);
            });
    };

    $scope.submitNewMessage = function (msgObj, fromCompose) {
        var msg = _.clone($scope.messageFormData);

        if (!_.isNil(msgObj)) {
            msg['finding'] = msgObj.finding.id;
            delete msg.customthread;
        } else {
            msg['finding'] = $state.params.finding_id;
        }
        Messages.addMessage(msg)
            .$promise.then((msgResp) => {
                if ($scope.messageFormData.queuedAttachments) {
                    for (const attachment of $scope.messageFormData.queuedAttachments) {
                        AttachmentService.uploadAttachment(attachment, msgResp.id)
                            .toPromise()
                            .then((attachmentRes) => {
                                console.log(attachmentRes);
                            });
                    }
                }
                $scope.messageFormData = {};
            })
            .catch(modalService.openErrorResponseCB('Sending message failed'));
    };

    function setToolOutputFilename(t) {
        t.filename = t.application + '_' + t.created_at.replace(/[.:]/g, '-');
    }
});
