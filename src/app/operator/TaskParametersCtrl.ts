import app from '../app';
import * as _ from 'lodash';
import * as angular from 'angular';
import '../common/operator/taskParameters';

app.controller('TaskParametersCtrl', function ($scope, $uibModal, $uibModalInstance, TaskParameters, modalService, task, readonly) {
    var origTaskParameters;
    var originalName;
    $scope.editingTemplate = false;
    $scope.readonly = readonly;
    if (_.size(task) === 1 && !_.isNil(task.template)) {
        $scope.task = angular.copy(task.template);

        $scope.editingTemplate = true;
        task = $scope.task;
        $scope.modalTitle = 'Edit Task Parameter Template';
        $scope.paramTemplates = [];
        if (_.isArray(task.parameters)) {
            // For backwards compatibility, convert array of name,value objects to key:value object form
            var paramArray = task.parameters;
            task.parameters = {};
            _.each(paramArray, function (p) {
                task.parameters[p.name] = p.value;
            });
        }

        origTaskParameters = task.parameters;
        task.parameters = angular.copy(task.parameters);

        $uibModalInstance.result.catch(resetParameters);
    } else {
        $scope.task = task;
        $scope.modalTitle = 'Enter Task Parameters';
        TaskParameters.list(
            {
                application: task.application,
                workflow: task.workflow,
                process: task.process,
            },
            {},
            function (paramTemplates) {
                $scope.paramTemplates = paramTemplates;

                // Add the ability to go back to the defaults.
                var defaults = {
                    id: -1,
                    name: 'Default Template',
                    parameters: {},
                };
                for (var i = 0; i < task.accepted_parameters.length; i++) {
                    defaults.parameters[task.accepted_parameters[i].name] = task.accepted_parameters[i].default;
                }
                $scope.paramTemplates.push(defaults);
                $scope.selectedTemplate = -1;
            }
        );
    }

    function resetParameters() {
        task.parameters = origTaskParameters;
    }

    $scope.submitTask = function () {
        for (var i = 0; i < task.accepted_parameters.length; i++) {
            var param = task.accepted_parameters[i];

            var value = angular.element("input[name='input-" + param.name + "']").val();
            if (value && param.type === 'FILE' && param.value.length == 0) {
                var message = 'Parameter ' + param.name + ' has a file waiting to be uploaded.';
                if (!param.required) {
                    message += ' Clear selected file if not needed.';
                }
                modalService.openErrorResponse('File awaiting upload', message, false);
                return;
            }
        }

        // Copy parameter values into task.parameters
        task.parameters = {};
        _.each(task.accepted_parameters, function (param) {
            if (!_.isUndefined(param.value)) {
                task.parameters[param.name] = param.value;
            }
        });

        $uibModalInstance.close(task);
    };

    $scope.update = function (selectedParamTemplate) {
        // Get the associated parameters template
        var paramTemplate: any = _.find($scope.paramTemplates, { id: parseInt(selectedParamTemplate) });
        if (!_.isUndefined(paramTemplate)) {
            if (_.isArray(paramTemplate.parameters)) {
                // For backwards compatibility, convert array of name,value objects to key:value object form
                var paramArray = paramTemplate.parameters;
                paramTemplate.parameters = {};
                _.each(paramArray, function (p) {
                    paramTemplate.parameters[p.name] = p.value;
                });
            }

            // Fill in our existing model's values with the values we've mapped.
            for (var j = 0; j < $scope.task.accepted_parameters.length; j++) {
                var param = $scope.task.accepted_parameters[j];
                $scope.task.accepted_parameters[j].value = paramTemplate.parameters[param.name];
            }
        }
    };

    $scope.save = function (isNew) {
        if (isNew) {
            var saveModal = $uibModal.open({
                templateUrl: '/app/operator/modalTaskParametersSave.html',
                controller: 'taskParametersSaveCtrl',
                resolve: {
                    args: {
                        currentName: task.name,
                        workflow: task.workflow,
                        application: task.application,
                        process: task.process,
                        paramTemplates: $scope.paramTemplates,
                        parameters: task.accepted_parameters,
                    },
                },
            });
            saveModal.result.then(function (newTemplate) {
                if ($scope.editingTemplate && newTemplate.id !== task.id) {
                    resetParameters();
                    newTemplate.accepted_parameters = task.accepted_parameters;
                    newTemplate.all_templates = task.all_templates;
                    task = newTemplate;
                    $scope.task = newTemplate;
                }
                task.name = originalName;
                $scope.task.name = originalName;
                $scope.selectedTemplate = newTemplate.id;
                $scope.selectedParamTemplate = newTemplate.id;
                origTaskParameters = newTemplate.parameters;
            });
        } else {
            TaskParameters.update(
                { paramsId: task.id },
                {
                    name: $scope.task.name,
                    workflow: task.workflow,
                    application: task.application,
                    process: task.process,
                    parameters: task.accepted_parameters,
                },
                function (result) {
                    $uibModalInstance.close();
                }
            );
        }
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});

app.controller('taskParametersSaveCtrl', function ($scope, $uibModalInstance, TaskParameters, args) {
    $scope.currentName = args.currentName;

    $scope.saveTemplate = function () {
        // Create template and submit.
        var newname = $scope.currentName;

        var newParameters = {};
        for (var i = 0; i < args.parameters.length; i++) {
            newParameters[args.parameters[i].name] = args.parameters[i].value;
        }

        var template = {
            name: newname,
            workflow: args.workflow,
            application: args.application,
            process: args.process,
            parameters: newParameters,
        };

        TaskParameters.save(template, function (result) {
            $uibModalInstance.close(result);
            args.paramTemplates.push(result);
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});
