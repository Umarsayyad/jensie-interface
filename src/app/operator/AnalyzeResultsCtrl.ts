import app from '../app';
import * as _ from 'lodash';
import * as angular from 'angular';

app.controller('AnalyzeResultsCtrl', function ($scope, Operations, params, $uibModalInstance) {
    $scope.modalTitle = 'Analyze Operation Results';
    $scope.results = [];

    $scope.resultsLoading = Operations.analysis_results({ operationId: params.operationId }).$promise.then(function (results) {
        $scope.results = _.map(results, function (r) {
            return angular.toJson(r, true);
        });
        // It'd be nice if the API would just mark the analysis as viewed without an additional PUT/POST
    }).$promise;

    $scope.close = function () {
        $uibModalInstance.dismiss();
    };
});
