import app from '../app';

app.controller('LockingTargetsCtrl', function ($scope, params, $uibModalInstance) {
    $scope.modalTitle = 'Targets locking MetaTarget';
    $scope.lockingTargets = params.lockingTargets;
    console.log('params: ', params);

    $scope.close = function () {
        $uibModalInstance.dismiss();
    };
});
