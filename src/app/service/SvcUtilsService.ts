import app from '../app';

app.service('SvcUtilsService', function () {
    function breakIntoTiers(arr) {
        var newArr = [];
        $.each(arr, function (k, v) {
            var t = v.tier;
            newArr[t] = newArr[t] || [];
            newArr[t].push(v);
        });
        return newArr;
    }

    return {
        breakIntoTiers: breakIntoTiers,
        TIERS: [
            { name: 'BASIC', value: 0 },
            { name: 'BASIC+ / PROFESSIONAL', value: 1 },
            { name: 'ENTERPRISE', value: 2 },
        ],
    };
});
