import app from '../../app';
import * as _ from 'lodash';
import * as angular from 'angular';
import catotheme from '@cato/config/themes/catotheme';

import '../../finding/reference/referencesViewer';

app.controller('ServiceCatCtrl', function (
    $scope,
    $stateParams,
    $state,
    $anchorScroll,
    $location,
    $q,
    $uibModal,
    moment,
    FindingService,
    MessageService,
    RandomColorService,
    socketService,
    TargetService,
    Findings,
    Services,
    Operations
) {
    // Messages stuff
    $scope.msgIsMissionDirector = MessageService.msgIsMissionDirector;
    $scope.msgIsOperator = MessageService.msgIsOperator;
    $scope.isMe = MessageService.isMe;
    $scope.getInitials = MessageService.getInitials;
    $scope.userIconColor = RandomColorService.userIconColor;
    $scope.getNameOrRole = MessageService.getNameOrRole;
    $scope.userIconColor = RandomColorService.userIconColor;

    $scope.campaign_id = null;
    $scope.operation_id = null;
    $scope.finding_id = null;
    $scope.expandedCampaignRows = {};
    $scope.expandedFindingRows = {};

    $scope.data = {};
    $scope.data.findingMessages = {};
    $scope.serviceCampaigns = {};
    $scope.campaignMap = {};
    $scope.campaignOperations = {};
    $scope.operations = {};
    $scope.operationsInTimeFrame = {};
    $scope.campaignsInTimeFrame = {};
    $scope.findingsForOperation = {};
    $scope.findings = [];
    $scope.findingsMap = {};
    $scope.archived = false;
    $scope.showReport = false;
    $scope.showEmpty = false;
    $scope.loadTimestamp = new Date();
    $scope.reportLoadingPromises = [];
    $scope.report = { timeframe: { timeFrame: 'Past 30 days' } };
    var oldParams = angular.copy($state.params);
    $scope.getTargetTypeIcon = TargetService.getTargetTypeIcon;

    $scope.archivedFilter = $scope.archived ? { archived: $scope.archived } : {};

    var messageUnsubscribe = socketService.subscribeByModel('message', function (message) {
        //console.log('got MESSAGE data back for (MailboxCtrl): ', message);
        let findingMessages = $scope.data.findingMessages[message.finding.id];
        if (message.finding && message.relevant_customers.length && findingMessages) {
            const existingMessageIndex = findingMessages.findIndex((item) => item.id === message.id);
            if (existingMessageIndex >= 0) {
                findingMessages[existingMessageIndex] = message;
            } else {
                findingMessages.push(message);
            }
            var finding = $scope.findingsMap[message.finding.id];
            if (finding && finding.messages) {
                const existingMessageIndex = finding.messages.findIndex((item) => item.id === message.id);
                if (existingMessageIndex >= 0) {
                    finding.messages[existingMessageIndex] = message;
                } else {
                    finding.messages.push(message);
                }
            } else if (_.isNumber(finding.message_count)) {
                finding.message_count++;
            }
        }
    });

    $scope.$on('$destroy', function () {
        messageUnsubscribe();
    });

    var findingUnsubscribe = socketService.subscribeByModel('finding', function (finding) {
        //console.log('got FINDING data back: ', finding);
        if (finding.archived === true) return;
        var curFinding = $scope.findingsMap[finding.id];
        if (finding.deleted) {
            if (curFinding) {
                _.remove($scope.findings, { id: finding.id });
                _.remove($scope.findingsForOperation[finding.operation], { id: finding.id });
                delete $scope.findingsMap[finding.id];
            }
        } else {
            if (!curFinding) {
                $scope.findings.push(finding);
                $scope.findingsMap[finding.id] = finding;
                if ($scope.findingsForOperation[finding.operation]) {
                    $scope.findingsForOperation[finding.operation].push(finding);
                }
                curFinding = finding;
            }
            updateWithFindingDetail(curFinding, true);
        }
    });

    $scope.archiveFinding = function (finding) {
        FindingService.archiveFinding(finding).$promise.then(function (retFinding) {
            $scope.updateObjOld($scope.findingsMap[retFinding.id], retFinding);
        });
    };

    $scope.unarchiveFinding = function (finding) {
        FindingService.unarchiveFinding(finding).$promise.then(function (retFinding) {
            $scope.updateObjOld($scope.findingsMap[retFinding.id], retFinding);
        });
    };

    $scope.$on('$destroy', function () {
        findingUnsubscribe();
    });

    function updateWithFindingDetail(finding, force?: Boolean) {
        if (!finding.detailFetched || force) {
            Findings.detail(
                { findingId: finding.id },
                function (findingDetail) {
                    findingDetail.detailFetched = true;
                    findingDetail.operation = finding.operation; // avoid recursive loops and duplicate data, we will fetch operation info from $scope.operations
                    $scope.updateObjOld($scope.findingsMap[finding.id], findingDetail); // ensure we update the latest object
                    $scope.data.findingMessages[finding.id] = findingDetail.messages;
                },
                function (err) {
                    console.log('Could not fetch finding details: ', err);
                    finding.detailFetched = false;
                }
            );
            finding.detailFetched = true;
        }
    }

    function storeOperationFindings(operation) {
        operation.campaign = operation.campaign.id || operation.campaign; // Avoid keeping duplicate data, just store the ID
        $scope.updateObjOld($scope.operations[operation.id], operation);

        $scope.findingsForOperation[operation.id] = operation.findings;
        _.each(operation.findings, function (finding) {
            var existingFinding = $scope.findingsMap[finding.id];
            if (existingFinding) {
                // extraFinding had to be inserted by a websocket, so it should be fully up-to-date and have details already
                // If somehow this is called before the details call returns, we don't loose anything here and the details callback will reference our new object instead of the old one to store the details
                $scope.updateObjOld(finding, existingFinding); // store any already-fetched details
                var idx = _.findIndex($scope.findings, { id: finding.id });
                $scope.findings[idx] = finding; // overwrite the existing object
            } else {
                $scope.findings.push(finding);
            }
            $scope.findingsMap[finding.id] = finding;
        });
    }

    function updateExpandedRows(v, id) {
        v[id] = !v[id];
        return v[id];
    }

    $scope.campaignExpanded = function (id) {
        return $scope.expandedCampaignRows[id];
    };

    $scope.findingExpanded = function (id) {
        return $scope.expandedFindingRows[id];
    };

    $scope.setClickedCampaignRow = function (campaign_id, loading) {
        $scope.campaign_id = campaign_id;
        $scope.operation_id = null;
        $scope.finding_id = null;
        var nowExpanded = updateExpandedRows($scope.expandedCampaignRows, campaign_id);
        if (nowExpanded && !loading) {
            $state.go('cato.loggedIn.service.campaign', { campaign_id: campaign_id });
        }
    };
    $scope.setClickedOperationRow = function (operation_id, campaign_id, loading) {
        if ($scope.operation_id === operation_id) {
            return; // User just clicked operation that is already selected, don't do anything
        }
        $scope.campaign_id = campaign_id;
        $scope.operation_id = operation_id;
        $scope.finding_id = null;

        if (!loading) {
            $state.go('cato.loggedIn.service.campaign.operation', {
                campaign_id: campaign_id,
                operation_id: operation_id,
            });
        }

        // Load findings for this operation
        $scope.campaignsLoading.then(function () {
            if (!$scope.findingsForOperation[operation_id]) {
                $scope.findingsLoaded = Operations.detail({ operationId: operation_id }).$promise.then(function (operation) {
                    storeOperationFindings(operation);
                    if (loading && $state.params.finding_id) {
                        var finding = _.find(operation.findings, { id: parseInt($state.params.finding_id) });
                        if (finding) {
                            updateWithFindingDetail(finding);
                        }
                    }
                });
            }
        });

        $anchorScroll();
    };

    $scope.setClickedFindingRow = function (finding_id, operation_id, campaign_id, loading) {
        $scope.campaign_id = campaign_id;
        $scope.operation_id = operation_id;
        $scope.finding_id = finding_id;

        var nowExpanded = updateExpandedRows($scope.expandedFindingRows, finding_id);
        if (nowExpanded && !loading) {
            updateWithFindingDetail($scope.findingsMap[finding_id]);

            $state.go('cato.loggedIn.service.campaign.operation.findings', {
                campaign_id: campaign_id,
                operation_id: operation_id,
                finding_id: finding_id,
            });
        }
    };

    $scope.getSeverityLevel = FindingService.getSeverityLevel;
    $scope.getSeverityLevelFromFindings = FindingService.getSeverityLevelFromFindings;
    $scope.SEVERITY_LEVEL = FindingService.SEVERITY_LEVEL;
    $scope.retestFinding = FindingService.retestFinding;

    $scope.gdata.services.$promise.then(function () {
        $scope.serviceCat = $scope.gdata.serviceMap[$stateParams.service_name];
    });

    $scope.showNoneFindings = false;
    $scope.campaignsLoading = Services.campaigns({
        serviceId: $stateParams.service_name,
        min_operation_info: true,
    }).$promise.then(function (service_campaigns) {
        $scope.serviceCampaigns = service_campaigns;
        angular.forEach(service_campaigns, function (campaign) {
            $scope.campaignOperations[campaign.id] = campaign.operations;
            $scope.campaignMap[campaign.id] = campaign;
            // Key off the operation id to make it a map, then merge the two maps together
            var operations = _.keyBy($scope.campaignOperations[campaign.id], 'id');
            $scope.operations = _.extend($scope.operations, operations);
        });
    });
    $scope.reportLoadingPromises[0] = $scope.campaignsLoading;

    function getFindingDetails(startDate, endDate) {
        $scope.allFindingsLoaded = $q.defer();
        $scope.reportLoadingPromises[1] = $scope.allFindingsLoaded.promise;
        var operationsNeeded = [];
        _.each($scope.campaignOperations, function (operations) {
            _.each(operations, function (operation) {
                if (
                    !operation.findings &&
                    operation.findings_count > 0 &&
                    !(moment(operation.updated).isBefore(startDate) || moment(operation.added).isAfter(endDate))
                ) {
                    operationsNeeded.push(operation.id);
                }
            });
        });

        var operationsFetched = [];

        function getMoreOperations() {
            Operations.detail({ id__in: operationsNeeded.join(','), page_size: 100 }, function (operations) {
                operationsFetched = operationsFetched.concat(operations.results);
                // remove fetched operations from the needed array
                _.pullAll(operationsNeeded, _.map(operations.results, 'id'));

                if (operationsNeeded.length === 0 || operations.count === 0) {
                    // If we want more, but the API isn't returning them, there is no point in requesting them again
                    processOperations();
                } else {
                    getMoreOperations();
                }
            });
        }

        if (operationsNeeded.length > 0) {
            getMoreOperations();
        } else {
            processOperations();
        }

        function processOperations() {
            _.each(operationsFetched, storeOperationFindings);

            _.each($scope.operations, function (operation) {
                if (
                    operation.findings &&
                    !(moment(operation.updated).isBefore(startDate) || moment(operation.added).isAfter(endDate)) &&
                    operation.findings.length > 0
                ) {
                    $scope.operationsInTimeFrame[$scope.timeFrame].add(operation);
                    $scope.campaignsInTimeFrame[$scope.timeFrame].add(operation.campaign);
                }
            });
            $scope.allFindingsLoaded.resolve();
        }
    }

    $scope.reportView = function () {
        oldParams = angular.copy($state.params);
        $state.go('cato.loggedIn.service.report', { service_name: oldParams.service_name });
        $scope.showReport = !$scope.showReport;
        $('#help_now').hide();
        // Also gets the finding details
        $scope.setTimeFrame();
    };

    $scope.defaultView = function () {
        var targetState = 'cato.loggedIn.service';
        if (oldParams.campaign_id) {
            targetState += '.campaign';
            if (oldParams.operation_id) {
                targetState += '.operation';
                if (oldParams.finding_id) {
                    targetState += '.findings';
                }
            }
        }
        $state.go(targetState, oldParams);
        $scope.showReport = !$scope.showReport;
        $('#help_now').show();
    };

    $scope.timeFrames = [
        {
            timeFrame: 'Past hour',
        },
        {
            timeFrame: 'Past 30 days',
        },
        {
            timeFrame: 'Past 90 days',
        },
        {
            timeFrame: 'Previous week',
        },
        {
            timeFrame: 'Previous month',
        },
        {
            timeFrame: 'Previous quarter',
        },
        {
            timeFrame: 'Previous year',
        },
        {
            timeFrame: 'Year-to-date',
        },
        {
            timeFrame: 'Quarter-to-date',
        },
        {
            timeFrame: 'Month-to-date',
        },
    ];

    $scope.setTimeFrame = function () {
        $scope.timeFrame = $scope.report.timeframe.timeFrame;
        switch ($scope.timeFrame) {
            case 'Past hour':
                $scope.startDate = moment().subtract(1, 'hour');
                $scope.endDate = moment();
                break;
            case 'Past 90 days':
                $scope.startDate = moment().subtract(90, 'days').startOf('day');
                $scope.endDate = moment().endOf('day');
                break;
            case 'Previous week':
                $scope.startDate = moment().subtract(1, 'weeks').startOf('isoWeek');
                $scope.endDate = moment().subtract(1, 'weeks').endOf('isoWeek');
                break;
            case 'Previous month':
                $scope.startDate = moment().subtract(1, 'month').startOf('month');
                $scope.endDate = moment().subtract(1, 'month').endOf('month');
                break;
            case 'Previous quarter':
                $scope.startDate = moment().subtract(1, 'quarter').startOf('quarter');
                $scope.endDate = moment().subtract(1, 'quarter').endOf('quarter');
                break;
            case 'Previous year':
                $scope.startDate = moment().subtract(1, 'year').startOf('year');
                $scope.endDate = moment().subtract(1, 'year').endOf('year');
                break;
            case 'Year-to-date':
                $scope.startDate = moment().startOf('year');
                $scope.endDate = moment();
                break;
            case 'Quarter-to-date':
                $scope.startDate = moment().startOf('quarter');
                $scope.endDate = moment().endOf('quarter');
                break;
            case 'Month-to-date':
                $scope.startDate = moment().startOf('month');
                $scope.endDate = moment();
                break;
            default:
                $scope.timeFrame = 'Past 30 days';
                $scope.startDate = moment().subtract(30, 'days').startOf('day');
                $scope.endDate = moment().endOf('day');
        }
        //console.log('startDate: ', $scope.startDate);
        //console.log('endDate: ', $scope.endDate);

        if (_.isNil($scope.operationsInTimeFrame[$scope.timeFrame])) {
            $scope.operationsInTimeFrame[$scope.timeFrame] = new Set();
            $scope.campaignsInTimeFrame[$scope.timeFrame] = new Set();
        }
        // Get Finding details for this time period
        $scope.campaignsLoading.then(function () {
            getFindingDetails($scope.startDate, $scope.endDate);
        });
    };

    if ($state.current.name.indexOf('.report') > -1) {
        // For report (defaults to last 30 days):
        $scope.reportView();
    }

    $scope.showMessages = function (finding) {
        finding.showMessages = !finding.showMessages;
        if (!$scope.data.findingMessages[finding.id]) {
            $scope.data.findingMessages[finding.id] = FindingService.loadFindingMessages(finding.id);
        }
    };

    if ($state.params.finding_id && $state.params.operation_id && $state.params.campaign_id) {
        $scope.setClickedCampaignRow(parseInt($state.params.campaign_id), true);
        $scope.setClickedOperationRow(parseInt($state.params.operation_id), $scope.campaign_id, true);
        $scope.setClickedFindingRow(parseInt($state.params.finding_id), $scope.operation_id, $scope.campaign_id, true);
    } else if ($state.params.operation_id && $state.params.campaign_id) {
        $scope.setClickedCampaignRow(parseInt($state.params.campaign_id), true);
        $scope.setClickedOperationRow(parseInt($state.params.operation_id), $scope.campaign_id, true);
    } else if ($state.params.campaign_id) {
        $scope.setClickedCampaignRow(parseInt($state.params.campaign_id), true);
    }

    $scope.openShareFinding = function (finding) {
        $scope.finding = finding;
        $scope.baseUrl = $location.protocol() + '://' + $location.host();
        $scope.findingShare = {
            finding: finding,
            subject: `${catotheme.appName} Finding: ${finding.title}`,
            url: $state.href('cato.loggedIn.service.campaign.operation.findings', {
                service_name: $state.params.service_name,
                campaign_id: $state.params.campaign_id,
                operation_id: $state.params.operation_id,
                finding_id: finding.id,
            }),
        };

        $scope.operation = $scope.operations[finding.operation.id];

        var modalInstance = $uibModal.open({
            templateUrl: '/app/finding/shareFindingModal.html',
            controller: function ($scope, $uibModalInstance) {
                $scope.sendShareFinding = function () {
                    var findingShare = _.clone($scope.findingShare);
                    var findingShareSubmit = {
                        email: findingShare.email,
                        message: findingShare.message,
                        url: $scope.baseUrl + findingShare.url,
                    };

                    Findings.share(
                        { findingId: findingShare.finding.id },
                        findingShareSubmit,
                        function (findingShareResult) {
                            console.log('findingShare: ' + JSON.stringify(findingShareResult, null, 2));

                            /* we should tell the person something... like, yay. */
                            $scope.result = 'Finding shared!';
                            $uibModalInstance.dismiss('send');
                        },
                        function (result) {
                            $scope.result = JSON.stringify(result);
                        }
                    );
                };

                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            },
            size: 'lg',
            scope: $scope,
        });
    };
});
