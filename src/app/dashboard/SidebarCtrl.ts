import app from '../app';
import * as _ from 'lodash';

import { config } from '@cato/config';
import { ValidTheme } from '@cato/config/themes/valid-themes';
import { SidebarService } from '../../ng-app/dashboard/services/sidebar.service';

app.controller('SidebarCtrl', function ($scope, $rootScope, $state, $transitions, SidebarService: SidebarService) {
    $scope.toggleSidebar = function () {
        $scope.data.sideBarActive = !$scope.data.sideBarActive;
        $rootScope.$broadcast('sidebarCollapse');
        SidebarService.toggleCollapsed(); // TODO: use this service to manage the collapsed state instead of the current $scope variables
    };

    var stateChangeStartListener = $transitions.onBefore({}, function (transition) {
        //event, toState, toParams, fromState, fromParams, options
        var toState = transition.to();
        if (toState.name === 'cato.loggedIn.dashboard') {
            var targetState = 'matrix';
            _.each($scope.tabs, function (tab) {
                if (tab.show(tab)) {
                    targetState = tab.stateName;
                    return false;
                }
            });
            return $state.target('cato.loggedIn.dashboard.' + targetState, {}, { reload: 'cato.loggedIn.dashboard' });
        } else if (_.startsWith(toState.name, 'cato.loggedIn.dashboard.')) {
            $scope.activeTab = toState.name.split('.')[3];
        }
    });

    $scope.$on('$destroy', function () {
        stateChangeStartListener();
    });

    $scope.go = function (tab) {
        if (tab.resetNotificationsOnClick) $rootScope.newItemCounts[tab.notifications] = 0;
        var targetState = 'cato.loggedIn.dashboard.' + tab.stateName;
        if ($state.includes(targetState)) {
            if ($state.is(targetState)) {
                if (!$state.transition || !$state.transition.isActive()) {
                    // TODO: handle reloading a tab when clicking on the currently loaded tab, as long as the page isn't loading
                    // This should work but doesn't: $state.go($state.current.name, {}, {reload: 'cato.loggedIn.dashboard'});
                }
            }
            // We are already in target state or a child state of the target, don't do anything because the tab was likely activated while the page was still loading
            return;
        }
        if ($state.transition && _.startsWith($state.transition.to().name, targetState)) {
            // Already transitioning to target state, no need to do anything else
        } else {
            $state.go(targetState, {}, { inherit: false }); // Don't inherit parameters between tabs
        }
    };

    $scope.activeTab = $state.current.name.split('.')[3];
    if (typeof $scope.activeTab === 'undefined') {
        _.each($scope.tabs, function (tab) {
            if (tab.show(tab)) {
                $scope.activeTab = tab.stateName;
                return false;
            }
        });
    }

    $scope.data.sideBarActive = true;

    $scope.tabs = [
        {
            heading: 'Dashboard',
            icon: 'fa-th',
            show: function (tab) {
                return true;
            },
            viewName: 'dashboard',
            stateName: 'matrix',
            className: 'lefttabtxt',
        },
        {
            heading: 'Vulnerability Trends',
            icon: 'fa fa-line-chart',
            show: function (tab) {
                return true;
            },
            viewName: 'vulnerabilities',
            stateName: 'vulnerabilityTrends',
            className: 'lefttabtxt',
        },
        {
            heading: 'To-Do',
            icon: 'fa-list',
            show: function (tab) {
                return $scope.hasAnyRole(['operator', 'administrator', 'mission_director']);
            },
            viewName: 'todo',
            stateName: 'todo',
            className: 'lefttabtxt',
            notifications: 'toDoItems',
        },
        /*{
            heading: 'Attack Surface',
            icon: 'fa-shield',
            show: function (tab) {
                return $scope.gdata.isCustomer;
            },
            viewName: 'attackSurface',
            stateName: 'attackSurface',
            className: 'lefttabtxt',
        },*/
        /*{
            heading: 'Assessment',
            icon: 'fa-cog',
            show: function(tab) {
                return $scope.gdata.isCustomer;
            },
            viewName: 'services',
            stateName: 'services',
            className: 'lefttabtxt',
        },*/
        {
            heading: 'Campaigns',
            icon: 'fa-hand-spock-o',
            show: function (tab) {
                return $scope.hasAnyRole(['operator', 'administrator', 'mission_director']);
            },
            viewName: 'campaigns',
            stateName: 'campaigns',
            className: 'lefttabtxt',
        },
        {
            heading: 'Operations',
            icon: 'fa-cogs',
            show: function (tab) {
                return $scope.hasAnyRole(['customer', 'operator', 'administrator', 'mission_director']);
            },
            viewName: 'operations',
            stateName: 'operations',
            className: 'lefttabtxt',
        },
        {
            heading: 'Findings',
            icon: 'fa-list',
            show: function (tab) {
                return $scope.hasAnyRole(['customer', 'operator', 'administrator', 'mission_director']);
            },
            viewName: 'findingsTab',
            stateName: 'findings',
            className: 'lefttabtxt',
            notifications: 'findings',
            resetNotificationsOnClick: true,
        },
        /*{
            heading: 'Attack Surface',
            icon: 'fa-shield',
            show: function (tab) {
                return $scope.hasAnyRole(['operator', 'administrator', 'mission_director']);
            },
            viewName: 'attackSurface',
            stateName: 'attackSurface',
            className: 'lefttabtxt',
        },*/
        {
            heading: 'Messages',
            icon: 'fa-envelope-o',
            show: function (tab) {
                return $scope.hasAnyRole(['operator', 'administrator', 'mission_director']);
            },
            viewName: 'messages',
            stateName: 'messages',
            className: 'lefttabtxt',
            notifications: 'messages',
        },
        {
            heading: 'Templates',
            icon: 'fa-sitemap',
            show: function (tab) {
                return $scope.gdata.isMissionDirectorOrAdmin;
            },
            viewName: 'templates',
            stateName: 'templates',
            className: 'lefttabtxt',
        },
        {
            heading: 'Services List',
            icon: 'fa-handshake-o',
            show: function (tab) {
                //return $scope.gdata.isMissionDirectorOrAdmin;
                return false;
            },
            viewName: 'servicesList',
            stateName: 'servicesList',
            className: 'lefttabtxt',
        },
        {
            heading: 'Targets',
            icon: 'fa-bullseye',
            show: function (tab) {
                return $scope.hasAnyRole(['customer', 'operator', 'administrator', 'mission_director']);
            },
            viewName: 'target',
            stateName: 'targets',
            className: 'lefttabtxt',
        },
        {
            heading: 'Messages',
            icon: 'fa-envelope-o',
            show: function (tab) {
                return $scope.gdata.isCustomer;
            },
            viewName: 'messages',
            stateName: 'messages',
            className: 'lefttabtxt',
            notifications: 'messages',
        },
        {
            heading: 'Manage Users',
            icon: 'fa-users',
            show: function (tab) {
                return $scope.hasAnyRole(['customer', 'administrator', 'mission_director']);
            },
            viewName: 'users',
            stateName: 'users',
            className: 'lefttabtxt',
        },
        /*{
            heading: 'Feedback',
            icon: 'fa-comment-o',
            show: function(tab) {
                return $scope.gdata.isMissionDirectorOrAdmin;
            },
            viewName: 'feedback',
            stateName: 'feedback',
            className: 'lefttabtxt',
        },*/
        /*{
            heading: 'Needing Replies',
            icon: 'fa-exclamation-triangle',
            show: function(tab) {
                return $scope.gdata.isMissionDirectorOrAdmin;
            },
            viewName: 'needsreply',
            stateName: 'needsreply',
            className: 'lefttabtxt',
        },*/
        {
            heading: 'Appliances',
            icon: 'fa-microchip',
            show: function (tab) {
                return $scope.hasAnyRole(['operator', 'administrator', 'mission_director']);
            },
            viewName: 'appliances',
            stateName: 'appliances',
            className: 'lefttabtxt',
        },
        /*{
            heading: 'Education & Training',
            icon: 'fa-graduation-cap',
            show: function (tab) {
                return $scope.user.username === 'kgumtow' || !$scope.gdata.isCustomer;
                //return $scope.hasAnyRole(['customer', 'operator', 'administrator', 'mission_director']);
            },
            viewName: 'training',
            stateName: 'training',
            className: 'lefttabtxt',
        },*/
        {
            heading: 'Enrollment Stats',
            icon: 'fa fa-check-square-o',
            show: () => config.catotheme === ValidTheme.jensie && $scope.hasAnyRole(['insight']),
            viewName: 'stats',
            stateName: 'insightStats',
            className: 'lefttabtxt',
        },
        {
            heading: 'Mentor-Protégé',
            icon: 'fa fa-users',
            show: () => config.catotheme === ValidTheme.jensie && $scope.hasAnyRole(['insight']),
            viewName: 'mpp',
            stateName: 'mpp',
            className: 'lefttabtxt',
        },
        {
            heading: 'Pilot Participation',
            icon: 'fa fa-address-card',
            show: () => config.catotheme === ValidTheme.jensie && $scope.hasAnyRole(['insight']),
            viewName: 'pilotParticipation',
            stateName: 'pilotParticipation',
            className: 'lefttabtxt',
        },
        {
            heading: 'Maps',
            icon: 'fa fa-map',
            show: function (tab) {
                return config.showMaps.toLowerCase() === 'true' && !$scope.gdata.isInsight;
            },
            viewName: 'maps',
            stateName: 'maps',
            className: 'lefttabtxt',
        },
        {
            heading: 'Geographical View',
            icon: 'fa fa-map',
            show: (tab) => config.showMaps.toLowerCase() === 'true' && config.catotheme === ValidTheme.jensie && $scope.hasAnyRole(['insight']),
            viewName: 'insightmapsview',
            stateName: 'insightmaps',
            className: 'lefttabtxt',
        },
        {
            heading: 'Developer',
            icon: 'fa fa-file-code-o',
            show: function () {
                return $scope.hasAnyRole(['operator', 'administrator', 'mission_director']) && config.skipApprovalsMinLevel;
            },
            viewName: 'developer',
            stateName: 'developer',
            className: 'lefttabtxt',
        },
        {
            heading: 'CMMC',
            icon: 'fa fa-check-square-o',
            show: () => config.catotheme === ValidTheme.jensie && $scope.gdata.isCustomer,
            viewName: 'cmmc',
            stateName: 'compliance',
            className: 'lefttabtxt',
        },
    ];
});
