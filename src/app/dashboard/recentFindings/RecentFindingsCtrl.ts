import app from '../../app';

app.controller('RecentFindingsCtrl', function ($scope, Findings) {
    $scope.recent_findings = Findings.recent_findings();
});
