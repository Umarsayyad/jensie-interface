import app from '../../../app';
import * as _ from 'lodash';
import * as angular from 'angular';

app.controller('AddToDoCtrl', function ($scope, $rootScope, $uibModalInstance, modalService, moment, params, ToDo) {
    $scope.priorities = [
        {
            index: 0,
            label: 'LOW',
        },
        {
            index: 1,
            label: 'MEDIUM',
        },
        {
            index: 2,
            label: 'HIGH',
        },
    ];

    $scope.todo = angular.copy(params.todo);

    var isNew = false;
    if ($scope.todo == null) {
        isNew = true;
        $scope.todo = {
            priority: 0,
        };
    } else if ($scope.todo.due_date) {
        $scope.todo.due_date = new Date($scope.todo.due_date);
    }

    $scope.datePickers = {};
    $scope.dateOptions = {};
    $scope.pickDate = function () {
        $scope.todo.opened = true;
    };

    $scope.todo.relevant_parties = _.filter($scope.todo.relevant_parties, function (user_id) {
        return user_id !== $rootScope.user.id || user_id === $scope.todo.created_by;
    });

    $scope.gatherUsers = function () {
        $scope.availableUsers = _.filter($rootScope.gdata.users, function (user) {
            if (user.type !== 'operator' || user.id === $rootScope.user.id) {
                return false;
            }

            if (user.roles.indexOf('administrator') > -1) {
                return false;
            }

            if (!user.is_active) {
                return false;
            }

            if (user.assigned_customers.length && $scope.todo.customer && user.assigned_customers.indexOf($scope.todo.customer) === -1) {
                return false;
            }

            return true;
        });
    };

    $scope.gatherUsers();

    $scope.submitTodo = function () {
        // Current user must be a relevant party. If already there, the duplicate entry will be ignored.
        $scope.todo.relevant_parties.push($rootScope.user.id);

        if (isNew) {
            ToDo.create(
                $scope.todo,
                function (todo) {
                    $uibModalInstance.close(todo);
                },
                function (res) {
                    modalService.openErrorResponse('To Do item could not be added.', res, false);
                }
            );
        } else {
            ToDo.update(
                { toDoId: $scope.todo.id },
                $scope.todo,
                function (todo) {
                    $uibModalInstance.close(todo);
                },
                function (res) {
                    modalService.openErrorResponse('To Do item could not be added.', res, false);
                }
            );
        }
    };

    $scope.getDisplayDate = function (date) {
        return moment(date).format('LL');
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});
