import app from '../../../app';
import * as _ from 'lodash';

app.controller('toDoCtrl', function ($scope, $rootScope, $state, $timeout, $uibModal, modalService, moment, ToDo) {
    $scope.resolved = false;
    $scope.itemsPerPage = $state.params.page_size || 30;
    $scope.currentPage = $state.params.page || 1;
    $scope.orderByField = '-priority,due_date';
    $scope.reverseSort = false;
    $scope.gdata = $rootScope.gdata;
    $scope.catotheme = $rootScope.catotheme;

    $scope.loadItems = function (page) {
        var ordering = $scope.orderByField;

        if ($scope.reverseSort) {
            ordering = '-' + ordering;
        }

        var params = {
            ordering: ordering,
            page: page,
            page_size: $scope.itemsPerPage,
            state: undefined,
            due_date__gte: undefined,
            due_date__lte: undefined,
        };

        if ($scope.resolved) {
            params.state = 'resolved';
        } else {
            params.state = 'active';
        }

        if ($scope.due_starting !== '') {
            params.due_date__gte = $scope.due_starting;
        }

        if ($scope.due_ending !== '') {
            params.due_date__lte = $scope.due_ending;
        }

        ToDo.get(params).$promise.then(function (response) {
            $scope.items = response.results;
            $scope.count = response.count;
        });
    };

    $scope.loadItems(1);

    $scope.setOrder = function (fieldName) {
        if (fieldName === $scope.orderByField) {
            $scope.reverseSort = !$scope.reverseSort;
        } else {
            $scope.orderByField = fieldName;
        }
        $scope.loadItems(1);
    };

    $scope.setPageSize = function (size) {
        $scope.itemsPerPage = size;
        $scope.loadItems(1);
    };

    $scope.dueDateColor = function (date) {
        if (date && moment(date).add(1, 'days').valueOf() < moment().endOf('day')) {
            return 'red-txt';
        }
        return '';
    };

    $scope.dateOptions = {};

    $scope.due_starting = '';
    $scope.selectingStartingDate = false;
    $scope.pickStartingDate = function () {
        $scope.selectingStartingDate = true;
    };
    $scope.due_ending = '';
    $scope.selectingEndingDate = false;
    $scope.pickEndingDate = function () {
        $scope.selectingEndingDate = true;
    };

    $scope.resolveTodo = function (todo) {
        modalService.openConfirm('Really resolve?', 'resolve this To Do item?', 'Continue', null, function () {
            ToDo.patch({ toDoId: todo.id }, { state: 'resolved' }).$promise.then(function (response) {
                _.remove($scope.items, function (todoiter: any) {
                    return todoiter.id === todo.id;
                });
            });
        });
    };

    $scope.removeTodo = function (todo) {
        ToDo.delete({ toDoId: todo.id }).$promise.then(function () {
            _.remove($scope.items, function (todoiter: any) {
                return todoiter.id === todo.id;
            });
        });
    };

    $scope.showResolved = function () {
        $scope.resolved = !$scope.resolved;
        $scope.loadItems();
    };

    $scope.showButtonText = function () {
        if ($scope.resolved) {
            return 'Hide Resolved';
        } else {
            return 'Show Resolved';
        }
    };

    $scope.priorityNames = ['LOW', 'MEDIUM', 'HIGH'];

    var priorityColor = ['#99FF99', '#FFFF77', '#FF9999'];
    $scope.getBackground = function (todo) {
        if (todo.state === 'resolved') {
            return '#BBBBBB';
        }

        return priorityColor[todo.priority];
    };

    $scope.addEditTodo = function (todo) {
        var modalInstance = $uibModal.open({
            templateUrl: '/app/dashboard/widgets/todo/modalToDo.html',
            controller: 'AddToDoCtrl',
            size: 'lg',
            scope: $scope,
            resolve: {
                params: {
                    todo: todo,
                },
            },
        });

        modalInstance.result.then(function (todo) {
            $scope.loadItems($scope.currentPage);
        });
    };

    $scope.getDisplayDate = function (date) {
        if (date) {
            return moment(date).format('LL');
        }
        return '';
    };
});
