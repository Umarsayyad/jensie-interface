import app from '../../app';
import { Nvd3Api } from '../../common/widgets/nvd3-api';
import { IWidget } from '../../../ng-app/dashboard/models/widget.interface';
import { DashboardWidgets } from '../../../ng-app/dashboard/services/dashboard-widgets.service';

interface ApiChild {
    api: Nvd3Api;
} //an object containing an 'api' property of type Nvd3Api

//Use an AngularJS component to wrap the widget templates
class WidgetWrapperCtrl implements ng.IComponentController {
    templateUrl: string;
    widget: IWidget;
    status = {};

    getSeverityLevel;
    getSeverityLevelFromFindings;
    SEVERITY_LEVEL;
    retestFinding;

    //TODO: maybe some of DashboardCtrl's current code belongs here??

    public static $inject = ['DashboardWidgets', 'FindingService'];
    constructor(private dbWidgets: DashboardWidgets, private FindingService) {
        this.getSeverityLevel = this.FindingService.getSeverityLevel;
        this.getSeverityLevelFromFindings = this.FindingService.getSeverityLevelFromFindings;
        this.SEVERITY_LEVEL = this.FindingService.SEVERITY_LEVEL;
        this.retestFinding = this.FindingService.retestFinding;
    }

    //this opens the dropdown for the "service" widgets
    toggleDropdown($event, title) {
        $event.preventDefault();
        $event.stopPropagation();

        if (this.status[title]) this.status[title].isopen = !this.status[title].isopen;
        else this.status[title] = {};
    }

    chartReadyCallback(value: ApiChild) {
        value.api.update();
        this.widget.update = value.api.update;
    }

    hideWidget() {
        this.dbWidgets.hideWidget(this.widget);
    }
}

const bindings = {
    templateUrl: '<',
    widget: '<',
};

const WidgetWrapper: ng.IComponentOptions = {
    controller: WidgetWrapperCtrl,
    controllerAs: '$ctrl',
    template: `<div class="full-height" ng-include="$ctrl.templateUrl"></div>`,
    bindings,
};

app.component('widgetWrapper', WidgetWrapper);
