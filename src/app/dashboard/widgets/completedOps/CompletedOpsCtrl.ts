import app from '../../../app';

app.controller('CompletedOpsCtrl', function ($scope, $rootScope, Operations) {
    Operations.get({ state: 'completed' }).$promise.then(function (operations) {
        $scope.completedOpsCount = operations.count; // Should support roughly to 1M in a single width widget
    });
});
