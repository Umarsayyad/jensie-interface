import app from '../../../app';
import * as _ from 'lodash';
import * as angular from 'angular';
import * as d3 from 'd3';

app.controller('MvtCtrl', function ($scope, $state, $timeout, globalDataService, Targets, FindingService, PreferencesService) {
    const axisXLabelLength = 20;

    var severityMap = FindingService.SEVERITY_LEVEL_MAP;
    var mvtWidget = $scope.$ctrl.widget;
    var targetTypes;

    $scope.targets = [];
    $scope.mvtData = [];
    $scope.targetGroups = ['computer', 'person', 'industrial', 'group', 'UNKNOWN', 'software'];
    $scope.counts = [10, 25, 50];
    $scope.count = $scope.counts[0];
    var targetOptions = Targets.options(function (result) {
        targetTypes = _.keyBy(result.actions.POST.type.choices, 'value');
    });

    $scope.setTargetCount = function (count) {
        $scope.count = count;

        Targets.mostVulnerableTargets({ group: $scope.group.toLowerCase(), count: $scope.count }, parseToGraphData);
    };

    $scope.isAliasShown = false;
    $scope.targetViewPreference = PreferencesService.getTargetDisplayPrefSubscription().subscribe((preference) => {
        $scope.isAliasShown = preference;
        if ($scope.targets.length > 0) {
            parseToGraphData($scope.targets);
        }
    });

    $scope.$on('$destroy', function () {
        $scope.targetViewPreference.unsubscribe();
    });

    $scope.setTargetGroup = function (group) {
        $scope.group = _.capitalize(group);
        $scope.groupInfo = '<b>' + $scope.group + ' contains</b>: ';

        var groupList = globalDataService.targetGroups[group.toLowerCase()];
        $scope.groupInfo += groupList.join(', ');

        Targets.mostVulnerableTargets({ group: group.toLowerCase(), count: $scope.count }, parseToGraphData);
    };

    // Initial setting of Target Group
    $scope.setTargetGroup($scope.targetGroups[0]);
    mvtWidget.sizeY = 2;

    $scope.mvtOptions = {
        chart: {
            type: 'multiBarHorizontalChart',
            margin: {
                right: 15,
                bottom: 30,
                left: 120,
            },
            callback: function (chart) {
                chart.multibar.dispatch.on('elementClick', function (e) {
                    var target = target_value_map[e.data.value];
                    $state.go('cato.loggedIn.dashboard.findings', { target: target.id, severity: e.data.key });
                });
            },
            clipEdge: true,
            duration: 500,
            stacked: true,
            showControls: false,
            // Yes these are flipped...
            xAxis: {
                showMaxMin: false,
                tickFormat: function (d) {
                    if (d.length > axisXLabelLength) {
                        d = d.substring(0, axisXLabelLength - 3) + '...';
                    }
                    return d;
                },
            },
            yAxis: {
                axisLabel: 'Vulnerability',
                axisLabelDistance: -12,
                tickFormat: function (d) {
                    return d3.format(',d')(d);
                },
            },
            tooltip: {
                contentGenerator: function (e) {
                    return (
                        '<b>' +
                        e.data.type +
                        '</b>: ' +
                        e.data.value +
                        '<br/>' +
                        '<b>' +
                        e.data.key +
                        ' Findings:</b> ' +
                        e.data.findings +
                        '<br>' +
                        '<b>Vulnerability contribution:</b> ' +
                        e.data.y
                    );
                },
            },
        },
    };

    var target_value_map = {};

    function parseToGraphData(targets) {
        var severityData = {};
        $scope.mvtData = [];
        $scope.targets = targets;

        if (targets.length === 0) {
            return;
        }

        var countSize = {
            10: 2,
            25: 4,
            50: 6,
        };

        if (targets.length >= $scope.count) {
            mvtWidget.sizeY = countSize[$scope.count];
        }

        targetOptions.$promise.then(function () {
            angular.forEach(targets, function (target) {
                target_value_map[target.value] = target;

                if (target.vulnerability > 0) {
                    angular.forEach(target.severity, function (value, label) {
                        var severityValue = _.indexOf(FindingService.SEVERITY_LEVEL, label);
                        if (_.isNil(severityData[label])) {
                            severityData[label] = [];
                        }
                        severityData[label].push({
                            x: $scope.isAliasShown ? target.alias : target.value,
                            y: value * severityValue,
                            findings: value,
                            type: targetTypes[target.type].display_name,
                            value: target.value,
                        });
                    });
                }
            });

            if (!_.isEmpty(severityData)) {
                angular.forEach(severityMap, function (severity) {
                    if (severity.value > 1) {
                        $scope.mvtData.push({
                            key: severity.label,
                            color: severity.color,
                            values: severityData[severity.label],
                        });
                    }
                });
            }

            var api_update = mvtWidget.update;
            if (api_update) {
                $timeout(api_update, 300);
            }
        });
    }
});
