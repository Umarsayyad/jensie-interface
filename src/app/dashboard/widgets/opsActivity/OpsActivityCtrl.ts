import app from '../../../app';
import * as _ from 'lodash';
import * as d3 from 'd3';
import { DashboardWidgets } from '../../../../ng-app/dashboard/services/dashboard-widgets.service';

app.component('opsActivityChart', {
    bindings: {
        widget: '<',
    },
    templateUrl: '/app/dashboard/widgets/opsActivity/opsActivityTmpl.html',
    controller: function OpsActivityChartCtrl(
        $rootScope,
        $timeout,
        modalService,
        moment,
        Customers,
        Users,
        globalDataService,
        DashboardWidgets: DashboardWidgets
    ) {
        var ctrl: any = {
            gdata: globalDataService,
            loadedObserver: undefined,
            observerConfig: undefined,
            opsActivityChartData: [],
            dateChoice: '1 Week',
            opsActivityChartOptions: {
                chart: {
                    type: 'multiBarHorizontalChart',
                    margin: {
                        top: 20,
                        right: 20,
                        bottom: 45,
                        left: 140,
                    },
                    clipEdge: true,
                    //staggerLabels: true,
                    duration: 500,
                    stacked: true,
                    reduceXTicks: false,
                    x: function (d) {
                        return d['customer'];
                    },
                    y: function (d) {
                        return d['count'];
                    },
                    legend: {
                        margin: {
                            top: 3,
                            right: -10,
                            bottom: 0,
                            left: 0,
                        },
                        maxKeyLength: 200,
                    },
                    xAxis: {
                        // axisLabel: 'Time (ms)',
                        showMaxMin: false,
                        tickFormat: function (d) {
                            return d;
                        },
                    },
                    yAxis: {
                        // axisLabel: 'Y Axis',
                        axisLabelDistance: -20,
                        tickFormat: function (d) {
                            return d3.format(',')(d);
                        },
                    },
                },
            },
            $onInit: function () {
                $timeout(function () {
                    ctrl.getData(ctrl.dateChoice);
                }, 500);
            },
            $onChanges: function () {
                if (ctrl.api) {
                    $timeout(function () {
                        ctrl.api.update();
                    }, 500);
                }
            },
            callback: function (scope) {
                ctrl.widget.chart = ctrl.chart = scope.chart;
                ctrl.widget.chart.svg = ctrl.svg = scope.svg;
                ctrl.widget.chart.api = ctrl.api = scope.api;
                ctrl.widget.update = ctrl.api.update;
            },
            getData: function (dateString) {
                var date = ctrl.dateChoice;
                if (dateString === '1 Month') {
                    ctrl.dateChoice = '1 Month';
                    date = moment().subtract(1, 'M').format('YYYY-MM-DD');
                } else if (dateString === '1 Day') {
                    ctrl.dateChoice = '1 Day';
                    date = moment().subtract(1, 'd').format('YYYY-MM-DD');
                } else {
                    ctrl.dateChoice = '1 Week';
                    date = moment().subtract(7, 'd').format('YYYY-MM-DD');
                }

                Customers.recentOperationCounts({ since: date }).$promise.then(function (opCnts) {
                    ctrl.opsActivityChartData = ctrl.populateOpsActivityChartWidget(opCnts);
                    ctrl.api.update();
                });
            },
            hideWidget: function () {
                DashboardWidgets.hideWidget(ctrl.widget);
            },
            populateOpsActivityChartWidget: function (opCnts) {
                // var sansTop10Ports = [];
                var operationsCompleted = [];
                var operationsInReview = [];

                _.forEach(opCnts, function (opCnt) {
                    var custName = ctrl.gdata.customerMap[opCnt.id].nickname;
                    operationsCompleted.push({ customer: custName, count: opCnt.operations_completed });
                    operationsInReview.push({ customer: custName, count: opCnt.operations_in_review });
                });

                var datum = [
                    { key: 'Operations Completed', values: operationsCompleted },
                    { key: 'Operations In Review', values: operationsInReview },
                ];

                return datum;
            },
        };

        for (var prop in ctrl) {
            if (!ctrl.hasOwnProperty(prop)) {
                //The current property is not a direct property of ctrl
                continue;
            }
            this[prop] = ctrl[prop];
        }

        //Replace the initial ctrl object with the updated this object in order to prevent the two from getting out of sync
        ctrl = this;
    },
});
