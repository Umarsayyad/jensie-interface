import app from '../../../app';
import * as _ from 'lodash';

app.service('PhishingCannedDataService', function ($rootScope) {
    function generateOps() {
        var ops = [];
        var maxOps = Math.floor(Math.random() * 10 + 3);
        var now = Date.now();

        for (var i = 0; i < maxOps; i++) {
            ops.push({
                id: i,
                active: true,
                added: '2018-05-24T17:45:06.048425Z',
                customer: $rootScope.user.customer,
                description: 'Creates and starts a GoPhish Campaign (prepare and use existing GoPhish)',
                findings: [],
                name: 'GoPhish Phishing Campaign (using existing GoPhish)',
                service: 'socialeng',
                started: true,
                startedOn: new Date(now - Math.floor(Math.random() * 3600 * 60 + 3600 * 60 * 60 * 24)),
                state: 'completed',
                stoppedOn: new Date(now - Math.floor(Math.random() * 3600 * 60)),
                updated: '2018-05-24T17:45:39.465066Z',
            });
        }
        console.log('ops: ', ops);
        return ops;
    }

    function generatePhishingFindings() {
        var actions = ['Email Opened', 'Clicked Link', 'Submitted Data'];

        var findings = [];
        var maxFindings = Math.floor(Math.random() * 100 + 6);
        for (var i = 0; i < maxFindings; i++) {
            findings.push({
                id: i,
                email: $rootScope.uuid(),
                event_type: _.sample(actions),
            });
        }
        return findings;
    }

    return {
        generateOps: generateOps,
        generatePhishingFindings: generatePhishingFindings,
    };
});
