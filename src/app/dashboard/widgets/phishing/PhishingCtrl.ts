import app from '../../../app';
import * as _ from 'lodash';
import * as d3 from 'd3';

app.controller('PhishingCtrl', function ($scope, $rootScope, $q, $state, $timeout, moment, Operations, GoPhishResults, PhishingCannedDataService) {
    /* Example static phishing results */
    var loadStatic = false;
    var phishingWidget = $scope.$ctrl.widget;

    $scope.completedPhishingOps = [];
    $scope.allOperationsLoaded = $q.defer();

    function getMoreOps(page?: any) {
        if (!page) {
            page = 1;
        }
        Operations.get(
            {
                state: 'completed',
                page_size: 30,
                page: page,
                services: 'socialeng',
                ordering: '-startedOn',
            },
            function (res) {
                $scope.completedPhishingOps = $scope.completedPhishingOps.concat(res.results);
                if (res.next) {
                    getMoreOps(page + 1);
                } else {
                    $scope.allOperationsLoaded.resolve($scope.completedPhishingOps);
                    $scope.availableOpsWithFindings = _.filter($scope.completedPhishingOps, function (op) {
                        return op.findings.length > 0 && op.started;
                    });
                    if ($scope.availableOpsWithFindings.length > 0) {
                        $scope.phishingOp = $scope.availableOpsWithFindings[0];
                        $scope.updatePhishingGraph($scope.phishingOp.id);
                    }
                }
            }
        );
    }

    if (loadStatic) {
        console.warn('Loading canned phishing data, to shut off, set loadStatic = false');
        $scope.availableOpsWithFindings = PhishingCannedDataService.generateOps();
        $scope.phishingOp = $scope.availableOpsWithFindings[0];
        $scope.allOperationsLoaded.resolve($scope.availableOpsWithFindings);
    } else {
        getMoreOps();
    }

    $scope.phishingDonut = true;

    $scope.phishingDonutOptions = {
        chart: {
            type: 'pieChart',
            donut: true,
            x: function (d) {
                return d.label;
            },
            y: function (d) {
                return d.v;
            },
            showLabels: false,
            valueFormat: function (d) {
                return d3.format(',')(d);
            },
            duration: 500,
            legend: {
                margin: {
                    top: 5,
                    right: 35,
                    bottom: 5,
                    left: 0,
                },
            },
            pie: {
                dispatch: {
                    elementClick: function (input) {
                        switch (input.data.label) {
                            case 'Email Opened':
                                $state.go('cato.loggedIn.dashboard.findings', {
                                    severity: 'LOW',
                                    operation: $scope.phishingFindings[0].operation,
                                });
                                break;
                            case 'Clicked Link':
                                $state.go('cato.loggedIn.dashboard.findings', {
                                    severity: 'HIGH',
                                    operation: $scope.phishingFindings[0].operation,
                                });
                                break;
                            case 'Submitted Data':
                                $state.go('cato.loggedIn.dashboard.findings', {
                                    severity: 'CRITICAL',
                                    operation: $scope.phishingFindings[0].operation,
                                });
                                break;
                            default:
                            //Do nothing
                        }
                    },
                },
            },
        },
    };

    $scope.togglePhishingChart = function () {
        $scope.phishingDonut = !$scope.phishingDonut;
        if ($scope.phishingDonut) {
            // refresh donut chart
            var api_update = phishingWidget.update;
            if (api_update) {
                $timeout(api_update, 100);
            }
        }
    };

    function populatePhishingWidget(phishresults, operation_id) {
        const eventTypes = [
            { id: 0, label: 'Email Opened', className: 'label-low', color: '#c6dfb4', v: 0 },
            { id: 1, label: 'Clicked Link', className: 'label-medium', color: '#fffd9d', v: 0 },
            { id: 2, label: 'Submitted Data', className: 'label-critical', color: '#fe7c81', v: 0 },
        ];

        $scope.phishingData = [];

        if (loadStatic) {
            phishresults = PhishingCannedDataService.generatePhishingFindings();
        }

        for (var i = 0; i < phishresults.length; i++) {
            var p = phishresults[i];
            $scope.phishingFindings.push(p);

            var url = $state.href('cato.loggedIn.service.campaign.operation.findings', {
                service_name: 'socialeng',
                campaign_id: p.campaign,
                operation_id: p.operation,
                finding_id: p.finding,
            });
            var event: any = _.find(eventTypes, { label: p.event_type });
            if (!_.isNil(event) && p.operation === operation_id) {
                // We only care about the event types that are in our eventTypes array.
                // Sum up the events by type
                event.v++;

                var item = {
                    id: i,
                    group: event.id,
                    content: '<a class="' + event.className + ' label-xs" href=' + url + '>' + p.email + '</a>',
                    title: moment(p.time).fromNow(),
                    start: p.time,
                    type: 'box',
                    event: event,
                    url: url,
                    email: p.email,
                };

                $scope.phishingData.push(item);
            }
        }

        $scope.phishingDonutData = eventTypes;

        if (phishresults.length > 0) {
            $scope.showPhishing = true;
            var api_update = phishingWidget.update;
            if (api_update) {
                $timeout(api_update, 100);
            }
            $scope.phishingOp = $scope.availableOpsWithFindings.find((op) => op.id === operation_id);
        } else {
            $scope.showPhishing = false;
        }
    }

    $scope.phishingFindings = [];
    $scope.updatePhishingGraph = function (opId) {
        $scope.phishingFindings = [];

        $scope.opResultsPromise = GoPhishResults.list({ operation: opId })
            .$promise.then((phishresults) => populatePhishingWidget(phishresults, opId))
            .catch(console.error);
    };
});
