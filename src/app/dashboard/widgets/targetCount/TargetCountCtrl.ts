import app from '../../../app';
import * as _ from 'lodash';

app.controller('TargetCountCtrl', function ($scope, $rootScope, Targets) {
    Targets.basicList().$promise.then(function (targets) {
        $scope.targetCount = targets.length;
    });
});
