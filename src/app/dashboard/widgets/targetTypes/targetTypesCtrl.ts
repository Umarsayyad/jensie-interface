import app from '../../../app';
import * as _ from 'lodash';
import * as d3 from 'd3';

app.controller('targetTypesCtrl', function ($scope, $rootScope, $state, $timeout, Targets) {
    var targetsByType = {};

    $scope.targetTypesDonutOptions = {
        chart: {
            type: 'pieChart',
            donut: true,
            x: function (d) {
                return d.label;
            },
            y: function (d) {
                return d.v;
            },
            showLabels: false,
            valueFormat: function (d) {
                return d3.format(',')(d);
            },
            duration: 500,
            legend: {
                margin: {
                    top: 5,
                    right: 15,
                    bottom: 5,
                    left: 15,
                },
            },
            pie: {
                dispatch: {
                    elementClick: function (e) {
                        $state.go('cato.loggedIn.dashboard.targets', { targetType: e.data.labelKey });
                    },
                },
            },
        },
    };

    function populateTargetTypeWidget() {
        $scope.targetTypesDonutData = [];
        Targets.options(function (result) {
            var targetTypes = result.actions.POST.type.choices;
            for (var i = 0; i < targetTypes.length; i++) {
                var count = targetsByType[targetTypes[i].value];
                // The index of the target type determines the HSL angle for picking color.
                var angle = (i / targetTypes.length) * 360;

                if (count > 0) {
                    $scope.targetTypesDonutData.push({
                        id: i,
                        label: targetTypes[i].display_name,
                        labelKey: targetTypes[i].value,
                        className: 'label-low',
                        color: 'hsl(' + angle + ', 100%, 50%)',
                        v: count,
                    });
                }
            }
        });
    }

    Targets.basicList().$promise.then(function (targets) {
        _.forEach(targets, function (target, index, array) {
            if (targetsByType.hasOwnProperty(target.type)) {
                targetsByType[target.type]++;
            } else {
                targetsByType[target.type] = 1;
            }
        });
        populateTargetTypeWidget();
    });
});
