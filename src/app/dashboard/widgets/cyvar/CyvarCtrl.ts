import app from '../../../app';
import * as _ from 'lodash';
import * as angular from 'angular';
import * as d3 from 'd3';

app.controller('CyvarCtrl', function ($scope, $rootScope, $timeout, FindingService, Operations) {
    $scope.businessApps = ['Salesforce', 'Oracle', 'Netsuite', 'Workday', 'Ascentis'];

    $scope.attackTypes = [
        'Network',
        'WebApp',
        'Social Engineering',
        'Wireless',
        'External Connections',
        'Internal Connections',
        'Infrastructure',
        'Physical',
    ];

    var levels = [
        { name: 'GREEN', class: 'label-low' },
        { name: 'YELLOW', class: 'label-medium' },
        { name: 'RED', class: 'label-critical' },
    ];

    $scope.lossBy = {};
    $scope.lossInfo = {};
    $scope.cyvarData = {};
    $scope.histogram = false;

    $scope.showLossBy = function (loss) {
        $scope.loss = loss;
    };

    $scope.showLossBy($scope.businessApps[0]);

    Operations.query({ page_size: 1 }).$promise.then(function (o) {
        $scope.operations = o.results;
        angular.forEach($scope.businessApps, function (ba) {
            $scope.lossBy[ba] = [];
            $scope.lossInfo[ba] = { totalCVaR: 0, totalExposure: 0 };
            generateHistogramValues(ba);

            var sum = 0;
            angular.forEach($scope.attackTypes, function (type) {
                var data = generateLossData(type);
                $scope.lossBy[ba].push(data);
                sum += data.VaR;
            });
            var level = levels[Math.floor(Math.random() * 3)];
            $scope.lossInfo[ba].level = level.name;
            $scope.lossInfo[ba].levelClass = level.class;
            $scope.lossInfo[ba].totalCVaR = $scope.numberWithCommas(sum);
            $scope.lossInfo[ba].totalExposure = $scope.numberWithCommas(sum + Math.floor(Math.random() * 28000000));
        });
    });

    $scope.cyvarOptions = {
        title: {
            enable: true,
            text: 'Your Loss Distribution',
            className: 'graph-title',
        },
        chart: {
            type: 'historicalBarChart',
            height: 335,
            margin: {
                top: 20,
                right: 20,
                bottom: 65,
                left: 60,
            },
            x: function (d) {
                return d[0];
            },
            y: function (d) {
                return d[1];
            },
            showValues: true,
            valueFormat: function (d) {
                return d3.format(',.1f')(d);
            },
            duration: 100,
            xAxis: {
                axisLabel: 'Value (Millions of $)',
                tickFormat: function (d) {
                    return d3.format(',.1f')(d);
                },
                rotateLabels: -30,
                showMaxMin: false,
            },
            yAxis: {
                axisLabel: 'Probability (%)',
                axisLabelDistance: -10,
                tickFormat: function (d) {
                    return d3.format(',.1f')(d);
                },
            },
            tooltip: {
                keyFormatter: function (d) {
                    return 'Losses of <b>$' + d3.format(',d')(d) + 'M</b> are likely to occur with a probability of: ';
                },
            },
            zoom: {
                enabled: false,
            },
        },
    };

    function generateHistogramValues(loss) {
        var maxHistVal = Math.floor(Math.random() * (100 - 30) + 30);

        var max = 40;
        var vals = [];
        for (var i = 0; i <= max; i++) {
            vals.push(Math.random() * maxHistVal);
        }
        var sortedVals = _.sortBy(vals);
        var values = [];
        for (i = 0; i < max; i++) {
            values.push([i, sortedVals[max - i]]);
        }

        $scope.cyvarData[loss] = [
            {
                key: 'Quantity',
                bar: true,
                values: values,
            },
        ];
    }

    $scope.numberWithCommas = function (x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    };

    function getSeverityFromPercentage(v) {
        // minimum values for low, med, high, crit
        var ranges = [0, 25, 51, 81, 101];
        for (var i = 0; i < ranges.length; i++) {
            if (v < ranges[i] && v >= ranges[i - 1]) {
                return FindingService.getSeverityLevel(i);
            }
        }
    }

    function generateLossData(type) {
        // VaR, Campaign, Validation, Last Validated

        var year = Math.floor(Math.random() * 1) + 2017;
        var month = Math.floor(Math.random() * 12) + 1;
        var day = Math.floor(Math.random() * 28) + 1;
        var hour = Math.floor(Math.random() * 24);
        var min = Math.floor(Math.random() * 60);
        var sec = Math.floor(Math.random() * 60);

        var lastValidated = new Date(year, month, day, hour, min, sec);

        var validation = Math.floor(Math.random() * 101);
        var validationSeverity = getSeverityFromPercentage(validation);

        var operation = _.sample($scope.operations);

        return {
            type: type,
            VaR: Math.floor(Math.random() * 2000001),
            operation: operation,
            validation: validation, //validationGraphData(val),
            validationSeverity: validationSeverity,
            lastValidated: lastValidated,
        };
    }

    // awful hack to make the graph show
    $scope.toggleCyvarGraph = function () {
        $scope.histogram = !$scope.histogram;
        $timeout(function () {
            window.dispatchEvent(new Event('resize'));
        }, 200);
    };
});
