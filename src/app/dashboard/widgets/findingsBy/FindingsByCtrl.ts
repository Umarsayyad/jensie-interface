import app from '../../../app';
import * as _ from 'lodash';
import * as angular from 'angular';
import * as d3 from 'd3';
import { DashboardWidgets } from '../../../../ng-app/dashboard/services/dashboard-widgets.service';

app.controller('FindingsByCtrl', function (
    $scope,
    $rootScope,
    $state,
    $window,
    Findings,
    Services,
    FindingService,
    RandomColorService,
    DashboardWidgets: DashboardWidgets
) {
    $scope.FindingsByCtrl = {};
    $scope.catotheme = $rootScope.catotheme;

    function smallGuid() {
        var guid = $rootScope.guid();
        var guidNoDashes = guid.replace(/-/g, '');
        return guidNoDashes.substring(0, 13);
    }

    $rootScope.gdata.services.$promise.then(function () {
        $scope.FindingsByCtrl.graphFindingsBy($rootScope.catotheme.customerCapitalize);
    });

    $scope.FindingsByCtrl.graphFindingsBy = function (graphFindingsBy, force) {
        if ($scope.findingsBy === graphFindingsBy && !force) return;
        $scope.findingsBy = graphFindingsBy;

        var data = [];
        $scope.graphFindingsByData = [];

        var colors = FindingService.SEVERITY_COLORS;

        // Well, this is annoying...wish there were a way to use CSS classes...no time to keep searching
        var serviceColors = {
            netscan: '#78b22a',
            physical: '#598527',
            configscan: '#de6a08',
            dnsscan: '#00aeef',
            vulnscan: '#0054a6',
            webstackscan: '#92278f',
            insiderthreat: '#753fe7',
            socialeng: '#9e0b0f',
            socialmed: '#3c757d',
            wireless: '#e3ab1d',
            webapp: '#ec008c',
            dco: '#62878C',
            oco: '#74A159',
            complianceaudit: '#2D4471',
            threathunt: '#A33643',
        };

        switch (graphFindingsBy) {
            case $rootScope.catotheme.customerCapitalize:
                $scope.graphFindingsByData = [];
                Findings.customers().$promise.then(function (customerFindings) {
                    $scope.graphFindingsByData = [];
                    angular.forEach(customerFindings, function (cFindings) {
                        var values = [];
                        var total = 0;
                        var customer, color;
                        if (cFindings.customer === 'other') {
                            customer = smallGuid();
                            color = RandomColorService.randomColor({ seed: customer });
                        } else {
                            customer = $rootScope.gdata.customer.name;
                            color = '#f68220';
                        }
                        angular.forEach(cFindings.severity_counts, function (count, severity: string) {
                            if (severity && severity !== 'NONE') {
                                if (_.isNil(count)) {
                                    count = 0;
                                }
                                values.push({ label: severity, value: count });
                                total += count;
                            }
                        });
                        if (total > 0) {
                            var customerFindings = {
                                color: color,
                                values: values,
                                key: undefined,
                            };
                            if (cFindings.customer === 'other') {
                                customerFindings.key = 'Other ' + $rootScope.catotheme.customerCapitalize;
                                $scope.graphFindingsByData.push(customerFindings);
                            } else {
                                customerFindings.key = customer;
                                $scope.graphFindingsByData.unshift(customerFindings);
                            }
                        }
                    });
                });
                break;
            case 'Campaign':
                Services.campaign_findings({ serviceId: 0 }).$promise.then(function (campaigns) {
                    $scope.graphFindingsByData = [];
                    angular.forEach(campaigns, function (cFindings) {
                        var values = [];
                        var total = 0;
                        angular.forEach(cFindings.severity_counts, function (count, severity: string) {
                            if (severity && severity !== 'NONE') {
                                if (_.isNil(count)) {
                                    count = 0;
                                }
                                values.push({ label: severity, value: count });
                                total += count;
                            }
                        });
                        if (total > 0) {
                            var service = $rootScope.gdata.serviceMap[cFindings.service];
                            var campaignFindings = {
                                key: cFindings.campaign.title + ' (' + service.name + ')',
                                color: serviceColors[service.short_name],
                                values: values,
                            };
                            $scope.graphFindingsByData.push(campaignFindings);
                        }
                    });
                });
                break;
            case 'Service':
                $scope.graphFindingsByData = [];
                angular.forEach(DashboardWidgets.getServiceInfoMap(), function (service) {
                    var values = [];
                    var total = 0;
                    angular.forEach(service.severity_counts, function (count, severity: string) {
                        if (severity && severity !== 'NONE') {
                            if (_.isNil(count)) {
                                count = 0;
                            }
                            values.push({ label: severity, value: count });
                            total += count;
                        }
                    });
                    if (total > 0) {
                        var serviceFindings = {
                            key: service.name,
                            color: serviceColors[service.short_name],
                            values: values,
                        };
                        $scope.graphFindingsByData.push(serviceFindings);
                    }
                });
                break;
            default:
                console.warn('Unknown Findings by option "%s" specified', graphFindingsBy);
        }
    };

    $scope.findingsByDataOptions = {
        chart: {
            type: 'multiBarChart',
            margin: {
                top: 30,
                right: 20,
                bottom: 50,
                left: 55,
            },
            color: function (d, i) {
                return d.backgroundColor;
            },
            x: function (d) {
                return d.label;
            },
            y: function (d) {
                return d.value;
            },
            showValues: true,
            valueFormat: function (d) {
                return d3.format(',')(d);
            },
            stacked: true,
            duration: 500,
            xAxis: {
                axisLabel: 'Severity',
                axisLabelDistance: 5,
            },
            yAxis: {
                axisLabel: 'Number of Findings',
                axisLabelDistance: -10,
                tickFormat: d3.format('d'),
            },
            showLegend: false,
            reduceXTicks: false,
            wrapLabels: true,
            multibar: {
                dispatch: {
                    elementClick: function (e) {
                        console.log(e.data.label);
                        $window.scrollTo(0, 0);
                        $state.go('cato.loggedIn.dashboard.findings', { severity: e.data.label });
                    },
                },
            },
        },
    };
});
