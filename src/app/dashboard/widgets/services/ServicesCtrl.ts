import app from '../../../app';
import * as _ from 'lodash';
import * as angular from 'angular';

app.controller('ServicesCtrl', function ($scope, $rootScope, FindingService) {
    $scope.serviceWidget = {
        numberOfColumns: 3,
        columns: [1, 2, 3],
    };

    $scope.customerServices = [];
    angular.forEach($scope.gdata.customer.services, function (serviceId) {
        $scope.customerServices.push($scope.gdata.serviceMap[serviceId]);
    });

    $scope.getSeverityLevel = FindingService.getSeverityLevel;
});
