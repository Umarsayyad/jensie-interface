import app from '../../../app';
import * as _ from 'lodash';

import { IGeneralUtils } from '../../../common/generalUtils';
import { Nvd3Api } from '../../../common/widgets/nvd3-api';
import { ChartData } from './models/chart-data';
import { IWidget } from '../../../../ng-app/dashboard/models/widget.interface';
import { DashboardWidgets } from '../../../../ng-app/dashboard/services/dashboard-widgets.service';

interface ApiChild {
    api: Nvd3Api;
} //an object containing an 'api' property of type Nvd3Api

class ScoreHistoryCtrl implements ng.IComponentController {
    private readonly chartReady$: Promise<ApiChild>;
    private chartResolve: (value: ApiChild) => void;
    public api = {} as Nvd3Api;
    public customers;
    public widget: IWidget = null;
    public showPlaceholder: boolean = true;
    public placeholder: string = 'Loading Score History...';
    scoreHistoryChartData: ChartData[] = [];
    chartOptions = chartOptions;

    static inject$ = ['$rootScope', '$scope', 'Customers', 'GeneralUtils', 'DashboardWidgets'];
    constructor(
        private $rootScope: ng.IRootScopeService,
        private $scope: ng.IScope,
        private Customers: any,
        private GeneralUtils: IGeneralUtils,
        private DashboardWidgets: DashboardWidgets
    ) {
        this.chartReady$ = new Promise((resolve) => (this.chartResolve = resolve));
    }

    $onInit() {
        this.widget = this.DashboardWidgets.getWidget({ id: 'score-history' });
        this.$scope.$on('cleanup-widgets', (event, delay = 1000) => {
            this.chartReady$.then((v) => v.api.updateWithTimeout(delay));
        });
    }

    $onChanges() {
        this.populateScoreHistoryData();
    }

    chartReadyCallback(value: ApiChild) {
        this.chartResolve(value);
        this.api = value.api;
        this.api.update();
        this.widget.update = value.api.update;

        /*
        let scope=this.$scope as any;
        this.DashboardWidgets.addWidget(this.widget =
        {
            id: 'score-history',
            version: 1,
            templateUrl: '/app/dashboard/widgets/scoreHistory/score-history-loader.html',
            title: 'Finding Discovery and Resolution',
            sizeX: 4,
            sizeY: 2,
            chart: {api: value.api},
        });
*/
    }

    hide() {
        this.DashboardWidgets.hideWidget(this.widget);
    }

    private populateScoreHistoryData() {
        let low = {
            key: 'LOW',
            values: [],
            color: '#b2c8a2',
        };
        let medium = {
            key: 'MEDIUM',
            values: [],
            color: '#e5e38d',
        };
        let high = {
            key: 'HIGH',
            values: [],
            color: '#e5c161',
        };
        let critical = {
            key: 'CRITICAL',
            values: [],
            color: '#e46f74',
        };

        this.scoreHistoryChartData = [low, medium, high, critical];

        this.Customers.scoreHistory({ customerId: this.GeneralUtils.getCustomer().id }).$promise.then((scoreData) => {
            if (scoreData.length == 0) {
                this.placeholder = 'No Score History Available.';
            } else {
                var sorted = scoreData.sort(function (a, b) {
                    return a.recorded_on - b.recorded_on;
                });
                var i = 0;
                for (let scoreInstance of sorted) {
                    let recorded_on = scoreInstance['recorded_on'] * 1000;
                    low.values.push({ x: recorded_on, y: scoreInstance['LOW'] });
                    medium.values.push({ x: recorded_on, y: scoreInstance['MEDIUM'] });
                    high.values.push({ x: recorded_on, y: scoreInstance['HIGH'] });
                    critical.values.push({ x: recorded_on, y: scoreInstance['CRITICAL'] });
                    i++;
                }
                this.showPlaceholder = false;
                this.placeholder = '';
            }
        });
    }
}

const ScoreHistoryChart: ng.IComponentOptions = {
    controller: ScoreHistoryCtrl,
    controllerAs: '$ctrl',
    templateUrl: '/app/dashboard/widgets/scoreHistory/score-history.component.html',
    bindings: {
        widget: '<',
    },
};

app.component('scoreHistoryChart', ScoreHistoryChart);

var chartOptions =
    //forcing this to be hoisted using `var` for now
    {
        chart: {
            type: 'stackedAreaChart',
            useVoronoi: false,
            showControls: false,
            showLabels: true,
            rightAlignYAxis: true,
            useInteractiveGuideline: true,
            // height: 250,
            // width: 500,
            noData: '',
            margin: {
                top: 20,
                right: 90,
                bottom: 30,
                left: 40,
            },
            xAxis: {
                tickFormat: function (d) {
                    let date = new Date(d);
                    let year = date.getFullYear();
                    let month = (1 + date.getMonth()).toString().padStart(2, '0');
                    let day = date.getDate().toString().padStart(2, '0');

                    return month + '/' + day + '/' + year.toString().slice(-2);
                },
            },
            yAxis: {
                axisLabel: 'Finding Count',
            },
        },
    };
