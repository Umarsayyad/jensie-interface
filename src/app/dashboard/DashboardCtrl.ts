import app from '../app';
import * as _ from 'lodash';
import * as angular from 'angular';

import '../common/PendingItemsService';
import '../operation/modalAddOperation';
import './widgets/scoreHistory/score-history.component';
import './widgets/widget-wrapper.component';

app.controller('DashboardCtrl', function (
    $scope,
    $rootScope,
    $q,
    $timeout,
    FindingService,
    PendingItemsService,
    socketService,
    modalService,
    Services,
    Users
) {
    $scope.service = {};
    $scope.data = {};
    $scope.data.services = {};
    $scope.results = {};
    if (_.isNil($scope.data.findings)) {
        $scope.data.findings = [];
    }
    if (_.isNil($scope.data.rows)) {
        $scope.data.rows = [];
    }
    $scope.getSeverityLevel = FindingService.getSeverityLevel;
    $scope.getSeverityLevelFromFindings = FindingService.getSeverityLevelFromFindings;
    $scope.SEVERITY_LEVEL = FindingService.SEVERITY_LEVEL;
    $scope.retestFinding = FindingService.retestFinding;
    $rootScope.newItemCounts.findings = 0;
    $scope.hiddenServices = [];
    $scope.widgets = $rootScope.user.ui_data.widgets || [];
    $scope.currentPage = 1;
    $scope.status = {};

    PendingItemsService.refresh();

    $scope.toggleDropdown = function ($event, title) {
        console.log('event: ', $event);
        console.log('title: ', title);
        $event.preventDefault();
        $event.stopPropagation();
        $scope.status[title].isopen = !$scope.status[title].isopen;
    };

    // Have to set this here due to timing issues
    var gridsterColumns = 8;

    $scope.showArchived = false;

    var customerId = $scope.user.customer;
    var operationsConfig;

    $scope.setClickedCampaignRow = function (index) {
        $scope.selectedCampaignRow = index;
        $scope.selectedOperationRow = null;
        $scope.selectedFindingRow = null;
    };
    $scope.setClickedOperationRow = function (index) {
        $scope.selectedOperationRow = index;
        $scope.selectedFindingRow = index;
    };

    $scope.data.showSelectCustomer = false;

    $scope.setSelectedCustomer = function (customer) {
        var ui_data = _.clone($rootScope.user.ui_data);
        if (customer) {
            ui_data['selectedCustomer'] = { id: customer.id, name: customer.name };
        } else {
            ui_data['selectedCustomer'] = null;
            if ($rootScope.user.ui_data) {
                delete $rootScope.user.ui_data.selectedCustomer;
            }
        }
        $scope.data.showSelectCustomer = false;

        Users.patch(
            { userId: $rootScope.user.id },
            { ui_data: ui_data },
            function (user) {
                $rootScope.updateObjOld($rootScope.user, user);
            },
            function (res) {
                modalService.openErrorResponse('Failed to save ' + $rootScope.catotheme.customer + ' selection', res, false);
            }
        );
    };

    $scope.showWidget = function (widget) {
        var ui_data = $rootScope.user.ui_data;
        widget.show = true;
        ui_data['widgets'] = $scope.widgets;

        Users.patch(
            { userId: $rootScope.user.id },
            { ui_data: ui_data },
            function (user) {
                $rootScope.updateObjOld($rootScope.user, user);
            },
            function (res) {
                modalService.openErrorResponse("Failed to update user's shown services", res, false);
            }
        );
    };

    function updateSeverity(serviceId, severity) {
        var max_severity = $scope.data.services[serviceId].max_severity;
        var last_max_severity = $scope.data.services[serviceId].last_max_severity;
        $scope.data.services[serviceId].max_severity = max_severity > severity ? max_severity : severity;
        $scope.data.services[serviceId].last_max_severity = $scope.data.services[serviceId].max_severity;
    }

    if ($scope.gdata.isCustomer) {
        var findingUnsubscribe = socketService.subscribeByModel('finding', function (finding) {
            //console.log('got FINDING data back for ' + $rootScope.catotheme.customer + ' (DashboardCtrl): ', finding);
            if (finding.deleted) {
                _.remove($scope.data.findings[customerId], { id: finding.id });
                return;
            }
            if (finding.archived !== true) {
                $rootScope.newItemCounts.findings = $rootScope.newItemCounts.findings + 1;
            }

            //updateSeverity(finding.service, finding.severity);  // TODO: support this in new dashboard
        });

        $scope.$on('$destroy', function () {
            findingUnsubscribe();
        });
    }

    function getFindingSeverities() {
        $scope.data.findingSeveritiesLoaded = $q.defer();

        $scope.data.loadFindingSeverities = Services.findings().$promise.then(function (serviceFindingsWithMaxSeverity) {
            angular.forEach(serviceFindingsWithMaxSeverity, function (svc) {
                var id = svc.service;
                if (!$scope.data.services[id]) {
                    $scope.data.services[id] = $rootScope.gdata.serviceMap[id];
                }
                $scope.data.services[id].totalFindings = svc.findings; // Not even used anymore?
                $scope.data.services[id].max_severity = svc.max_severity;
                $scope.data.services[id].last_max_added = svc.last_max_added;
                $scope.data.services[id].latest_finding = svc.latest_finding;
                $scope.data.services[id].severity_counts = svc.severity_counts;
            });
            $scope.data.findingSeveritiesLoaded.resolve($scope.data.services);
        });
    }

    $scope.gridsterOpts = {
        columns: gridsterColumns,
        pushing: true,
        floating: true,
        swapping: true,
        width: 'auto',
        colWidth: 'auto',
        rowHeight: 'match',
        margins: [25, 25],
        outerMargin: false,
        defaultSizeX: 1,
        defaultSizeY: 1,
        minColumns: 1,
        minRows: 2,
        //mobileBreakPoint: 800,
        mobileModeEnabled: false,
        draggable: {
            enabled: true,
            handle: '.gridster-item-drag',
            stop: function (event, $element, widget) {
                var ui_data = $rootScope.ui_data || {};
                ui_data['widgets'] = $scope.widgets;
                Users.patch(
                    { userId: $rootScope.user.id },
                    { ui_data: ui_data },
                    function (user) {
                        $rootScope.updateObjOld($rootScope.user, user);
                    },
                    function (res) {
                        modalService.openErrorResponse("Failed to update user's shown services", res, false);
                    }
                );
            },
        },
        resizable: {
            enabled: true,
            handles: ['e', 's', 'w', 'se', 'sw', 'nw', 'nw'],
            stop: function (event, $element, widget) {
                var api = _.get(widget, 'chart.api');
                if (api) {
                    $timeout(function () {
                        api.update();
                    }, 500);
                }
                var ui_data = $rootScope.ui_data || {};
                ui_data['widgets'] = $scope.widgets;
                Users.patch(
                    { userId: $rootScope.user.id },
                    { ui_data: ui_data },
                    function (user) {
                        $rootScope.updateObjOld($rootScope.user, user);
                    },
                    function (res) {
                        modalService.openErrorResponse("Failed to update user's shown services", res, false);
                    }
                );
            },
            resize: function (event, $element, widget) {
                var api = _.get(widget, 'chart.api');
                if (api) {
                    api.update();
                } else {
                    $scope.data.updateNVD3GraphHeight();
                }
            },
        },
    };

    $scope.data.movedOrResized = function () {
        $scope.data.movedOrResizedVal = false;
        angular.forEach($rootScope.user.ui_data.widgets, function (widget) {
            if ($scope.data.movedOrResizedVal === false) {
                $scope.data.movedOrResizedVal =
                    widget.col !== widget.col_default ||
                    widget.row !== widget.row_default ||
                    widget.sizeX !== widget.sizeX_default ||
                    widget.sizeY !== widget.sizeY_default ||
                    widget.show === false;
            }
        });
        return $scope.data.movedOrResizedVal;
    };

    function updateWidgetGraph(widget) {
        var api_update = _.get(widget, 'chart.api.update');
        if (api_update) {
            api_update();
        }
    }

    $scope.data.updateNVD3GraphHeight = function (widget) {
        if (widget) {
            updateWidgetGraph(widget);
        } else {
            _.each($scope.widgets, function (widget) {
                updateWidgetGraph(widget);
            });
        }
    };

    var resizeListener = $rootScope.$on('resize', _.debounce($scope.data.updateNVD3GraphHeight, 500));

    $scope.$on('$destroy', function () {
        resizeListener();
    });

    // TODO: what is this for again?
    $scope.$watch('results', function (newValue, oldValue) {
        $timeout(function () {
            //console.log('resetting $scope.results from within DashboardCtrl');
            $scope.results = {};
        }, 5000);
    });
});
