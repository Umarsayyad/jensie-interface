import * as _ from 'lodash';
import * as angular from 'angular';

import '../../common/stateBuilder';

angular.module('cato.routes.missionDirector', ['cato.stateBuilder']).config(function ($stateProvider, stateBuilderProvider) {
    $stateProvider
        .state('cato.loggedIn.dashboard.feedback', {
            url: '/feedback',
            sticky: true,
            views: {
                feedback: {
                    templateUrl: '/app/feedback/feedbackList.html',
                    controller: 'FeedbackCtrl',
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + ' - Feedback';
                },
            },
        })

        .state('cato.loggedIn.dashboard.campaigns', {
            url: '/campaigns',
            sticky: true,
            views: {
                campaigns: {
                    templateUrl: '/app/campaigns/campaigns.html',
                    controller: 'CampaignsCtrl',
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + ' - Campaigns';
                },
            },
        })

        .state('cato.loggedIn.dashboard.campaigns.addCampaign', {
            url: '/addCampaign',
            controller: 'AddCampaignLoaderCtrl',
        })

        .state('cato.loggedIn.dashboard.campaigns.runAutomatedOperations', {
            url: '/run-automated-ops/{campaignId}',
            controller: 'runAutomatedOpsLoaderCtrl',
        })

        .state('cato.loggedIn.dashboard.developer.analysis', {
            url: '/analysismodule',
            sticky: true,
            views: {
                opTemplates: {
                    templateUrl: '/app/developer/analysismodule.html',
                    controller: 'OpAnalysisModuleCtrl',
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + ' - Operation Modules';
                },
            },
        })

        .state('cato.loggedIn.dashboard.appliances', {
            url: '/appliances',
            sticky: true,
            views: {
                appliances: {
                    templateUrl: '/app/appliances/appliances.html',
                    controller: 'AppliancesCtrl',
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + ' - Appliances';
                },
            },
        })

        .state('cato.loggedIn.dashboard.appliances.editAppliance', {
            url: '/editAppliance/{appliance_id}',
            controller: 'ApplianceEditLoaderCtrl',
        })

        .state('cato.loggedIn.dashboard.templates', {
            url: '/templates',
            sticky: true,
            deepStateRedirect: {
                default: { state: 'cato.loggedIn.dashboard.templates.operation' },
                fn: function ($dsr$) {
                    if ($dsr$.to.state === 'cato.loggedIn.dashboard.templates') {
                        if (_.endsWith($dsr$.redirect.state, '.addOperationTemplate')) {
                            return { state: 'cato.loggedIn.dashboard.templates.operation' };
                        }
                        return true;
                    }
                },
            },
            views: {
                templates: {
                    templateUrl: '/app/md/templatesTabContainer.html',
                    controller: function ($scope, $state, $rootScope) {
                        if (_.includes($state.current.name, 'templates.parameter')) {
                            $scope.templateActiveTab = 'parameter';
                        } else if (_.includes($state.current.name, 'templates.campaign')) {
                            $scope.templateActiveTab = 'campaign';
                        } else if (_.includes($state.current.name, 'templates.recommendation')) {
                            $scope.templateActiveTab = 'recommendation';
                        } else {
                            $scope.templateActiveTab = 'operation';
                        }
                        var stateChangeStartListener = $rootScope.$on('$stateChangeStart', function (
                            event,
                            toState,
                            toParams,
                            fromState,
                            fromParams,
                            options
                        ) {
                            if (_.startsWith(toState.name, 'cato.loggedIn.dashboard.templates.')) {
                                $scope.templateActiveTab = toState.name.split('.')[4];
                            }
                        });

                        $scope.$on('$destroy', function () {
                            stateChangeStartListener();
                        });
                    },
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + ' - Operation Planning';
                },
            },
        })

        .state('cato.loggedIn.dashboard.templates.operation', {
            url: '/operation',
            sticky: true,
            views: {
                opTemplates: {
                    templateUrl: '/app/operation/operationTemplates.html',
                    controller: 'OpTemplatesCtrl',
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + ' - Operation Templates';
                },
            },
        })

        .state('cato.loggedIn.dashboard.templates.campaign', {
            url: '/campaign',
            sticky: true,
            views: {
                campaignTemplates: {
                    templateUrl: '/app/campaign/campaignTemplates.html',
                    controller: 'CampaignTemplateCtrl',
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + ' - Campaign Templates';
                },
            },
        })

        .state('cato.loggedIn.dashboard.templates.campaign.addCampaign', {
            url: '/{templateId}/addCampaign',
            params: {
                template: undefined,
            },
            controller: 'AddCampaignLoaderCtrl',
        })

        .state('cato.loggedIn.dashboard.templates.parameter', {
            url: '/parameter',
            sticky: true,
            views: {
                paramTemplates: {
                    templateUrl: '/app/md/parameterTemplates.html',
                    controller: 'ParameterTemplateCtrl',
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + ' - Parameter Templates';
                },
            },
        })

        .state('cato.loggedIn.dashboard.templates.operation.addOperationTemplate', {
            url: '/addTemplate',
            controller: 'AddOperationTemplateLoaderCtrl',
        })

        .state('cato.loggedIn.dashboard.parameter-templates', {
            url: '/parameter-templates',
            sticky: true,
            views: {
                paramtemplate: {
                    templateUrl: '/app/md/parameterTemplates.html',
                    controller: 'ParameterTemplateCtrl',
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + ' - Parameter Templates';
                },
            },
        })

        .state('cato.loggedIn.dashboard.templates.recommendation', {
            url: '/recommendation',
            sticky: true,
            views: {
                recommendationTemplates: {
                    templateUrl: '/app/md/recommendationTemplates.html',
                    controller: 'RecommendationTemplateCtrl',
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + ' - Recommendation Templates';
                },
            },
        })

        .state('cato.loggedIn.dashboard.servicesList', {
            url: '/servicesList',
            sticky: true,
            views: {
                servicesList: {
                    templateUrl: '/app/service/servicesList.html',
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + ' - Services';
                },
            },
        })

        .state('cato.loggedIn.dashboard.users.addCustomer', {
            url: '/customer_add/{customer_id}',
            params: {
                customer_id: '',
            },
            controller: 'addCustomerLoaderCtrl',
        })

        .state('cato.loggedIn.dashboard.users.uploadExternalRoE', {
            url: '/uploadExternalRoE/{customer_id}',
            params: {
                customer_id: '',
            },
            component: stateBuilderProvider.routedComponentModalGenerator('modal-upload-external-roe', { customer: '<' }),
            bindings: {
                customer: 'customerId',
            },
            resolve: {
                customerId: function ($state) {
                    return $state.params.customer_id || $state.transition.params().customer_id;
                },
            },
        });
});
