import { config } from '@cato/config';
import * as angular from 'angular';
import * as _ from 'lodash';
import catotheme from '@cato/config/themes/catotheme';

angular.module('cato.routes.general', ['ui.router', 'ui.router.upgrade', 'ui.router.state.events']).config(function ($stateProvider) {
    $stateProvider
        .state('cato.loggedIn', {
            abstract: true,
            templateUrl: '/app/common/content.html',
            redirectTo: 'cato.loggedIn.dashboard',
            resolve: [
                {
                    token: 'userSession',
                    deps: ['$http', '$rootScope', '$transition$', '$urlRouter'],
                    resolveFn: function ($http, $rootScope, $transition$, $urlRouter) {
                        if ($rootScope.loginEvaluated) {
                            console.log('this should never happen');
                            var dest = $transition$.$to();
                            window.location.pathname = $urlRouter.href(dest.url, dest.params);
                        }

                        $rootScope.loginEvaluated = true;
                        return $http({
                            url: config.apiPath + 'user/me',
                        });
                    },
                },
                {
                    token: 'main',
                    deps: ['userSession', '$rootScope', '$state', '$location', 'sessionService'],
                    resolveFn: function (userSession, $rootScope, $state, $location, sessionService) {
                        if (userSession && userSession.data) {
                            if (true || _.size(_.filter(userSession.data.roles, { role: 'USER' })) > 0) {
                                return sessionService.loadSession(userSession.data);

                                /*if (userSession.data.acceptEula === true) {
                                    $state.go('dp.eula');
                                    return;
                                }*/
                            } else {
                                $rootScope.sessionEvaluated = true;
                                $rootScope.loggedIn = false;
                                $state.go(
                                    'cato.login',
                                    {
                                        redirectTo: $location.url(),
                                    },
                                    {
                                        location: 'replace',
                                    }
                                );
                            }
                        } else {
                            console.log('login...');
                            $rootScope.sessionEvaluated = true;

                            $state.go('cato.login', {
                                redirectTo: $location.url(),
                            });
                        }
                    },
                },
                {
                    token: '$title',
                    deps: [],
                    resolveFn: function () {
                        return catotheme.appName;
                    },
                },
            ],
            controller: function ($rootScope) {
                $rootScope.loadingDefer.resolve();
            },
        })
        .state('cato.loggedIn.profile.notifications', {
            url: '/notifications',
            views: {
                notifications: {
                    templateUrl: '/app/settings/notifications.html',
                    controller: 'NotificationsCtrl',
                    data: { pageTitle: 'notifications' },
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + ' - Notifications';
                },
            },
        })
        .state('cato.loggedIn.dashboard.attackSurface', {
            url: '/attacksurface',
            views: {
                attackSurface: {
                    component: 'attackSurface',
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + ' - Attack Surface';
                },
            },
        })
        .state('cato.loggedIn.dashboard', {
            url: 'dashboard',
            views: {
                '': {
                    templateUrl: '/app/dashboard/dashboard.html',
                    controller: 'DashboardCtrl',
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + ' - Dashboard';
                },
            },
        });

    if (config.prod !== 'true') {
        $stateProvider.state('cato.loggedIn.changelog', {
            url: 'changelog',
            views: {
                '': {
                    template: '<div class="wrapper wrapper-content"><div class="ibox ibox-content" ng-bind-html="changelog.data"></div></div>',
                    controller: function ($scope, $resource, $sce) {
                        $scope.changelog = $resource($scope.restUrl + 'changelog').get(function (res) {
                            res.data = $sce.trustAsHtml(res.data);
                        });
                    },
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + " - What's New";
                },
            },
        });
    }
});
