import * as _ from 'lodash';
import * as angular from 'angular';

import '../../common/stateBuilder';

angular.module('cato.routes.operator', ['cato.stateBuilder']).config(function ($stateProvider, stateBuilderProvider) {
    /*$stateProvider.state('cato.loggedIn.dashboard.operator', {
           abstract: true,
           views: {
               'operator': {
                   template: '<ui-view />',
                   //templateUrl: '/app/dashboard/servicesTable.html',
               }
           },
       })*/

    $stateProvider
        .state('cato.loggedIn.dashboard.operations', {
            url: '/operations',
            sticky: true,
            deepStateRedirect: {
                default: { state: 'cato.loggedIn.dashboard.operations.optable' },
                // TODO: figure out why deep state redirect isn't working
                /*fn: function($dsr$) {
    
                    if($dsr$.to.state === 'cato.loggedIn.dashboard.templates') {
                        if(_.endsWith($dsr$.redirect.state, '.addOperationTemplate')) {
                            return { state: 'cato.loggedIn.dashboard.templates.operation' };
                        }
                        return true;
                    }
                }*/
            },
            params: {
                customer: null,
            },
            views: {
                operations: {
                    templateUrl: '/app/operation/operationsTab/operationsTabContainer.html',
                    controller: function ($scope, $state, $rootScope, $transitions) {
                        if (_.includes($state.current.name, 'operations.rules')) {
                            $scope.operationActiveTab = 'rules';
                        } else if (_.includes($state.current.name, 'operations.calendar')) {
                            $scope.operationActiveTab = 'calendar';
                        } else {
                            $scope.operationActiveTab = 'optable';
                        }
                        var stateChangeStartListener = $transitions.onBefore({}, function (transition) {
                            //event, toState, toParams, fromState, fromParams, options
                            var toState = transition.to();
                            if (_.startsWith(toState.name, 'cato.loggedIn.dashboard.operations.')) {
                                $scope.operationActiveTab = toState.name.split('.')[4];
                            }
                        });

                        $scope.$on('$destroy', function () {
                            stateChangeStartListener();
                        });
                    },
                },
            },
            resolve: { $title: ($title) => $title + ' - Operation Planning' },
        })

        .state('cato.loggedIn.dashboard.operations.optable', {
            url: '/browse?campaign?page?page_size?type',
            sticky: true,
            params: {
                campaign: null,
                page: {
                    dynamic: true,
                    squash: true,
                    value: 1,
                    type: 'int',
                },
                page_size: {
                    dynamic: true,
                    squash: true,
                    value: '30',
                },
                type: {
                    dynamic: true,
                },
            },
            views: {
                optable: {
                    templateUrl: '/app/operation/operationsTab/operationsTable.html',
                    controller: 'OperationsTableCtrl',
                },
            },
            resolve: { $title: ($title) => $title + ' - Operations' },
        })

        .state('cato.loggedIn.dashboard.operations.rules', {
            url: '/rules',
            sticky: true,
            views: {
                rules: {
                    component: 'autoScheduleRulesTab',
                },
            },
            resolve: { $title: ($title) => $title + ' - Auto-Schedule Rules' },
        })

        .state('cato.loggedIn.dashboard.operations.calendar', {
            url: '/calendar',
            sticky: true,
            views: {
                calendar: {
                    templateUrl: '/app/operation/operationsTab/calendar.html',
                    controller: 'OperationsCalendarCtrl',
                },
            },
            resolve: { $title: ($title) => $title + ' - Calendar' },
        })

        .state('cato.loggedIn.dashboard.targets.addEditTarget', {
            url: '/addOrEditTarget/{targetId}',
            params: {
                targetId: '',
                target: null,
                selectedCustomer: null,
            },
            views: {
                targetmodal: {
                    controller: 'addTargetLoaderCtrl',
                },
            },
        })

        .state('cato.loggedIn.dashboard.targets.addEditMetaTarget', {
            url: '/addOrEditMetaTarget/{metaTargetId}',
            params: {
                metaTargetId: '',
                metatarget: null,
                selectedCustomer: null,
            },
            views: {
                targetmodal: {
                    controller: 'addTargetLoaderCtrl',
                },
            },
        })

        .state('cato.loggedIn.dashboard.targets.targetDetails', {
            url: '/details/{id}',
            views: {
                targetmodal: {
                    controller: 'TargetDetailsLoaderCtrl',
                },
            },
        })

        .state('cato.loggedIn.dashboard.targets.metaTargetDetails', {
            url: '/metaTargetDetails/{id}',
            views: {
                targetmodal: {
                    controller: 'MetaTargetDetailsLoaderCtrl',
                },
            },
        })

        .state('cato.loggedIn.operator.operatorTargetDetails', {
            url: '/details/{id}',
            controller: 'TargetDetailsLoaderCtrl',
        })

        .state('cato.loggedIn.dashboard.operations.optable.addOperation', stateBuilderProvider.operationModalState())

        .state('cato.loggedIn.operator', {
            url: 'operation/{operation_id}?page?page_size?search?severity?state',
            templateUrl: '/app/operator/operator.html',
            params: {
                // TODO: keep these in sync with cato.loggedIn.dashboard.findings
                page: {
                    dynamic: true,
                    squash: true,
                    value: 1,
                    type: 'int',
                },
                page_size: {
                    dynamic: true,
                    squash: true,
                    value: '30',
                },
                search: {
                    dynamic: true,
                    squash: true,
                    value: '',
                },
                severity: {
                    dynamic: true,
                    squash: true,
                    array: true,
                    value: [],
                },
                state: {
                    dynamic: true,
                    squash: true,
                    array: true,
                    value: [],
                },
            },
            data: {
                pageTitle: 'operation.details',
                messageParent: 'finding',
            },
            controller: 'OperatorCtrl',
        })

        .state('cato.loggedIn.operator.addFinding', {
            url: '/addFinding',
            controller: 'addFindingLoaderCtrl',
        })

        .state('cato.loggedIn.operator.findingDetail', {
            url: '/finding/{finding_id}',
            data: { pageTitle: 'finding' },
            resolve: { $title: ($title) => $title + ' - Findings' },
        })

        .state(
            'cato.loggedIn.operator.findingDetail.followUpOperation',
            stateBuilderProvider.operationModalState('follow-up', 'followUpOpCustomizer', 'addOperation@cato.loggedIn.operator')
        )
        .state(
            'cato.loggedIn.dashboard.findings.followUpOperation',
            stateBuilderProvider.operationModalState('follow-up', 'followUpOpCustomizer', 'addOperation@cato.loggedIn.dashboard.findings')
        )

        .state('cato.loggedIn.operator.findingDetail.editFinding', {
            url: '/edit',
            views: {
                'editFinding@cato.loggedIn.operator': {
                    controller: 'addFindingLoaderCtrl',
                },
            },
        })

        .state('cato.loggedIn.dashboard.todo', {
            url: '/todo',
            sticky: true,
            views: {
                todo: {
                    templateUrl: '/app/todo/todo.html',
                    controller: 'TodoCtrl',
                },
            },
            resolve: { $title: ($title) => $title + ' - To-Do' },
        })

        .state('cato.loggedIn.dashboard.todo.editFinding', {
            url: '/editFinding/{finding_id}',
            data: { messageParent: 'finding' },
            controller: 'addFindingLoaderCtrl',
        })
        .state('cato.loggedIn.dashboard.developer', {
            url: '/developer',
            // sticky: true,
            views: {
                developer: {
                    component: 'developerCmp',
                },
            },
            resolve: { $title: ($title) => $title + ' - Developer/Analyze' },
        });
});
