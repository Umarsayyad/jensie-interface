import { config } from '@cato/config';
import * as angular from 'angular';
import 'angular-wizard';

angular.module('cato.routes.customer', ['cato']).config(function ($stateProvider, ROE_STATE) {
    $stateProvider
        .state('cato.loggedIn.dashboard.training', {
            url: '/training',
            sticky: true,
            views: {
                training: {
                    templateUrl: '/app/training/educationAndTraining.html',
                    controller: 'TrainingCtrl',
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + ' - Education and Training';
                },
            },
        })

        .state('cato.loggedIn.dashboard.services', {
            url: '/services',
            sticky: true,
            views: {
                services: {
                    templateUrl: '/app/dashboard/services_table.html',
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + ' - Assessment';
                },
            },
        })

        .state('cato.loggedIn.dashboard.messages', {
            url: '/messages',
            sticky: true,
            views: {
                messages: {
                    templateUrl: '/app/message/mail.html',
                    controller: 'MailboxCtrl',
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + ' - Messages';
                },
            },
        })

        .state('cato.loggedIn.dashboard.messages.finding', {
            url: '/finding/{finding_id}',
            params: {
                finding_id: '',
            },
            data: { messageParent: 'finding' },
            views: {
                messages: {
                    templateUrl: '/app/message/mail.html',
                    controller: 'MailboxCtrl',
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + ' - Messages';
                },
            },
        })

        .state('cato.loggedIn.dashboard.findings', {
            url:
                '/findings/{finding_id}?page?page_size?search?svc?customer?appliance?severity?state?operation?target?operation_template?followup_requested',
            sticky: true,
            params: {
                // TODO: keep these in sync with cato.loggedIn.operator
                finding_id: {
                    dynamic: true,
                    squash: true,
                    value: '',
                },
                page: {
                    dynamic: true,
                    squash: true,
                    value: 1,
                    type: 'int',
                },
                page_size: {
                    dynamic: true,
                    squash: true,
                    value: '30',
                },
                search: {
                    dynamic: true,
                    squash: true,
                    value: '',
                },
                svc: {
                    dynamic: true,
                    squash: true,
                    value: '',
                },
                customer: {
                    dynamic: true,
                    squash: true,
                    value: '',
                },
                appliance: {
                    dynamic: true,
                    squash: true,
                    value: '',
                },
                severity: {
                    dynamic: true,
                    squash: true,
                    array: true,
                    value: [],
                },
                state: {
                    dynamic: true,
                    squash: true,
                    array: true,
                    value: [],
                },
                operation: {
                    dynamic: true,
                    squash: true,
                    value: undefined,
                },
                target: {
                    dynamic: true,
                    squash: true,
                    value: undefined,
                },
                operation_template: {
                    dynamic: true,
                    squash: true,
                    value: undefined,
                },
                followup_requested: {
                    dynamic: true,
                    squash: true,
                    value: 'either',
                },
            },
            data: { pageTitle: 'findings', messageParent: 'campaign' },
            views: {
                findingsTab: {
                    templateUrl: '/app/finding/findingsTab.html',
                    controller: 'FindingsTabCtrl',
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + ' - Findings';
                },
            },
        })
        .state('cato.loggedIn.dashboard.findings.editFinding', {
            url: '/edit',
            views: {
                'editFinding@cato.loggedIn.dashboard.findings': {
                    controller: 'addFindingLoaderCtrl',
                },
            },
            resolve: {},
        })

        .state('cato.loggedIn.dashboard.targets', {
            url: '/targets',
            params: {
                targetType: '',
            },
            sticky: true,
            views: {
                target: {
                    templateUrl: '/app/target/targets.html',
                    controller: 'TargetsCtrl',
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + ' - Targets';
                },
            },
        })

        .state('cato.loggedIn.dashboard.targets.bulkEdit', {
            url: '/bulk_edit',
            views: {
                bulk_edit: {
                    controller: 'AddTargetCtrl',
                },
            },
            resolve: {},
        })

        .state('cato.loggedIn.service', {
            url: 'service/{service_name}',
            views: {
                '': {
                    templateUrl: '/app/service/customerServices/service.html',
                    controller: 'ServiceCatCtrl',
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + ' - Service';
                },
            },
        })

        .state('cato.loggedIn.service.report', {
            url: '/report',
            sticky: true,
            data: { pageTitle: 'Report' },
            views: {
                reportContainer: {
                    templateUrl: '/app/finding/findingsReport.html',
                },
            },
        })

        .state('cato.loggedIn.service.campaign', {
            url: '/campaign/{campaign_id}',
            data: { pageTitle: 'campaign', messageParent: 'campaign' },
            views: {
                findingsContainer: {
                    templateUrl: '/app/finding/findingsTable.html',
                    data: {
                        messageParent: 'finding',
                    },
                },
            },
        })

        .state('cato.loggedIn.service.campaign.operation', {
            url: '/operation/{operation_id}',
            data: { pageTitle: 'operation', messageParent: 'operation' },
        })

        .state('cato.loggedIn.service.campaign.operation.findings', {
            url: '/finding/{finding_id}',
        })

        .state('cato.loggedIn.dashboard.users', {
            /* manage users is a tab for customers, operators, and md. */
            url: '/users',
            views: {
                users: {
                    templateUrl: '/app/user/manageusers.html',
                    controller: 'ManageUsersCtrl',
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + ' - Manage Users';
                },
            },
        })

        .state('cato.loggedIn.dashboard.users.addUser', {
            url: '/user_add/{user_id}',
            params: {
                user_id: '',
            },
            controller: 'addUserLoaderCtrl',
        })

        .state('cato.loggedIn.profile', {
            url: 'profile',
            templateUrl: '/app/settings/profile.html',
            controller: 'ProfileCtrl',
            data: { pageTitle: 'profile' },
            resolve: {
                $title: function ($title) {
                    return $title + ' - Profile';
                },
            },
        })

        .state('cato.loggedIn.profile.company', {
            url: '/company',
            views: {
                company: {
                    templateUrl: '/app/settings/company.html',
                    controller: 'CompanyCtrl',
                    data: { pageTitle: 'Company' },
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + ' - Company';
                },
            },
        })

        .state('cato.loggedIn.profile.services', {
            url: '/services',
            views: {
                services: {
                    templateUrl: '/app/settings/services.html',
                    controller: 'CompanyCtrl',
                    data: { pageTitle: 'Services' },
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + ' - Services';
                },
            },
        })

        .state('cato.loggedIn.profile.appliances', {
            url: '/appliances',
            sticky: true,
            views: {
                appliances: {
                    templateUrl: '/app/appliances/appliances.html',
                    controller: 'AppliancesCtrl',
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + ' - Appliances';
                },
            },
        })

        .state('cato.loggedIn.profile.appliances.editAppliance', {
            url: '/editAppliance/{appliance_id}',
            controller: 'ApplianceEditLoaderCtrl',
            resolve: {},
        })

        .state('cato.loggedIn.profile.roe', {
            url: '/roe',
            views: {
                roe: {
                    templateUrl: '/app/settings/rulesofengagement.html',
                    controller: 'CompanyCtrl',
                    data: { pageTitle: 'Rules of Engagement' },
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + ' - Rules of Engagement';
                },
            },
        })

        .state('cato.loggedIn.profile.changePassword', {
            url: '/change-password',
            views: {
                changePassword: {
                    component: 'changePassword',
                    data: { pageTitle: 'Change Your Password' },
                },
            },
        })

        .state('cato.loggedIn.profile.twofactor', {
            url: '/twofactor',
            views: {
                twofactor: {
                    templateUrl: '/app/settings/twoFactorDevices.html',
                    controller: 'TwoFactorDevicesCtrl',
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + ' - Two-Factor Devices';
                },
            },
        })

        .state('cato.loggedIn.profile.company.addPoC', {
            url: '/add',
            views: {
                profilemodal: {
                    controller: 'addPoCLoaderCtrl',
                },
            },
        })

        .state('cato.loggedIn.profile.company.delPoC', {
            url: '/del/{poc_id}',
            views: {
                profilemodal: {
                    controller: 'delPoCLoaderCtrl',
                },
            },
        })

        .state('cato.loggedIn.profile.company.addPoCAddr', {
            url: '/addinfo/{poc_id}',
            views: {
                profilemodal: {
                    controller: 'AddPoCAddressLoaderCtrl',
                },
            },
        })

        .state('cato.loggedIn.incident_reporting.dcise-icf.addPoCAddr', {
            url: '/addinfo/{poc_id}',
            views: {
                contactModal: {
                    controller: 'AddPoCAddressLoaderCtrl',
                },
            },
        })

        .state('cato.loggedIn.profile.company.editPoCAddr', {
            url: '/editinfo/{poc_id}/{addr_id}/{loc_id}',
            views: {
                profilemodal: {
                    controller: 'AddPoCAddressLoaderCtrl',
                },
            },
        })

        .state('cato.loggedIn.incident_reporting.dcise-icf.editPoCAddr', {
            url: '/editinfo/{poc_id}/{addr_id}/{loc_id}',
            views: {
                contactModal: {
                    controller: 'AddPoCAddressLoaderCtrl',
                },
            },
        })

        .state(ROE_STATE, {
            url: 'RulesOfEngagement',
            component: 'interactiveRoe',
            resolve: {
                $title: function ($title) {
                    return $title + ' - Rules of Engagement';
                },
            },
        })

        .state('cato.loggedIn.incident_reporting', {
            url: 'incident-reporting',
            templateUrl: '/app/incidentReporting/incidentReporting.html',
            controller: 'IncidentReportingCtrl',
            resolve: {
                $title: function ($title) {
                    return $title + ' - Incident Reporting';
                },
            },
        })

        .state('cato.loggedIn.incident_reporting.reports', {
            url: '/reports',
            views: {
                reports: {
                    templateUrl: '/app/incidentReporting/incidentReports.html',
                    controller: 'IncidentReportsCtrl',
                    data: { pageTitle: 'Submitted Reports' },
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + ' - Submitted Reports';
                },
            },
        })

        .state('cato.loggedIn.incident_reporting.dcise-icf', {
            url: '/dcise-icf',
            views: {
                'dcise-icf': {
                    templateUrl: '/app/incidentReporting/dcise-icf.html',
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + ' - File a DCISE ICF report';
                },
            },
        })

        .state('cato.loggedIn.library', {
            url: 'library',
            templateUrl: '/app/library/library.html',
            resolve: {
                $title: function ($title) {
                    return $title + ' - Library';
                },
            },
        });

    if (config.showMaps.toLowerCase() === 'true') {
        $stateProvider.state('cato.loggedIn.dashboard.maps', {
            params: {
                appliance: undefined,
                customer: undefined,
                managedCustomerEmpty: false,
                target: undefined,
                targets: undefined,
            },
            url: '/maps',
            views: {
                maps: {
                    component: 'maps',
                },
            },
            resolve: {
                $title: function ($title) {
                    return $title + ' - Maps';
                },
            },
        });
    }
});
