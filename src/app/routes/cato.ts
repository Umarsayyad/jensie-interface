import * as angular from 'angular';
import * as _ from 'lodash';
import catotheme from '@cato/config/themes/catotheme';

import uiRouter from '@uirouter/angularjs';
import { upgradeModule } from '@uirouter/angular-hybrid';
import '@uirouter/angularjs/release/stateEvents';

angular.module('cato.routes.cato', [uiRouter, upgradeModule.name, 'ui.router.state.events']).config(function ($stateProvider) {
    $stateProvider.state('cato', {
        url: '/',
        template: '<ui-view />',
        resolve: {
            $title: function () {
                return catotheme.appName;
            },
        },
        controller: function ($state) {
            if ($state.is('cato')) {
                $state.go('cato.login');
            }
        },
    });

    $stateProvider
        .state('cato.login', {
            url: 'login',
            params: {
                message: null,
                redirectTo: null,
            },
            templateUrl: '/app/login/login.html',
            resolve: {
                $title: ($title) => $title + ' - Login',
            },
            controller: 'loginController',
        })

        .state('cato.logout', {
            url: 'logout',
            templateUrl: '/app/partials/logout.html',
            data: { pageTitle: 'logout', specialClass: 'gray-bg' },

            controller: function ($scope, $rootScope, loginService, sessionService) {
                $scope.logoutMessage = 'Log Out Pending';
                $rootScope.loadingDefer.resolve();
                sessionService.endSession();
                loginService.logout().then(
                    function () {
                        $scope.logoutMessage = 'Log Out Successful!';
                    },
                    function (error) {
                        if (_.isError(error)) {
                            console.error(error);
                        } else {
                            console.log(error);
                        }
                        $scope.logoutMessage = 'Log Out Failed';
                        $scope.logoutSubMessage = 'Please refresh this page, and then exit your browser to ensure your information is protected.';
                        // We could use the message "Please refresh this page, clear your browser's data for this site, and then exit your browser..." but that makes it sound like the site is broken and the logout implementation is flawed, so I'm not including it even though it is the most secure action the user could take.  This situation should never be reached anyway, and if it is, hopefully only during development.
                    }
                );
            },
            resolve: { $title: ($title) => $title },
        })

        .state('cato.cato', {
            abstract: true,
            templateUrl: '/app/common/content.html',
            controller: function ($rootScope) {
                $rootScope.loadingDefer.resolve();
            },
        })

        .state('cato.cato.privacy-policy', {
            url: 'privacy-policy',
            templateUrl: '/app/common/boilerplate/privacyPolicy.html',
            resolve: {
                $title: function ($title) {
                    return $title + ' - Privacy Policy';
                },
            },
        })

        .state('cato.cato.terms-and-conditions', {
            url: 'terms-and-conditions',
            templateUrl: '/app/common/boilerplate/termsAndConditions.html',
            resolve: {
                $title: function ($title) {
                    return $title + ' - Terms and Conditions';
                },
            },
        })

        .state('error-404', {
            url: '/error-404',
            templateUrl: '/app/partials/error404.html',
            data: { pageTitle: '404', specialClass: 'cato-bg' },
        })
        .state('error-500', {
            url: '/error-500',
            templateUrl: '/app/partials/error500.html',
            data: { pageTitle: '500', specialClass: 'cato-bg' },
        })

        .state('cato.initialSetPassword', {
            url: 'initial-set-password',
            templateUrl: '/app/common/passwordManagement/initialSetPassword.html',
            controller: 'InitialSetPasswordCtrl',
            resolve: { $title: ($title) => $title },
        })

        .state('cato.forgotPassword', {
            url: 'forgot-password',
            templateUrl: '/app/common/passwordManagement/forgotPassword.html',
            controller: 'ForgotPasswordCtrl',
            resolve: { $title: ($title) => $title },
        })

        .state('cato.resetPassword', {
            url: 'reset-password/{token}/{userid}',
            templateUrl: '/app/common/passwordManagement/resetPassword.html',
            controller: 'ResetPasswordCtrl',
            resolve: { $title: ($title) => $title },
        })

        .state('cato.setPassword', {
            url: 'set-password/{token}/{userid}',
            templateUrl: '/app/common/passwordManagement/setPassword.html',
            controller: 'ResetPasswordCtrl',
            resolve: { $title: ($title) => $title },
        });
});
