$(function () {
    var h = $('#headernav'),
        c = h.find('a.current'),
        u = typeof window.curl != 'undefined' ? curl : window.location.href.toString().replace(/^[^\/]+\/\/[^\/]+/, '');
    if (c.length > 0) {
        h.find("a[href='" + c.eq(0).attr('href') + "']").addClass('current');
        h.find("a[href $='" + u + "']").addClass('current');
        h.find('a.current').parents('ul').addClass('ancestor');
    }
    $('.contactlist').prev('h3').append('  <img src="/_images/signup.png" alt="[Contact Information]" title="[Contact Information]" border="0" />');
    if ($('.h2toc,.h3toc').length > 0) {
        $('.h2toc .toc,.h3toc .toc').hide();
        $('.h2toc h2,.h3toc h3').each(maketoc);
        if ($('.h2toc.toggleall h2').length > 0 || $('.h3toc.toggleall h3').length > 0)
            $('.h2toc.toggleall,.h3toc.toggleall').prepend(
                '<a class="toctoggle small" href="#header" onclick="toggletoc(this);return false">Show all</a>'
            );
    }
    makesm();
    h.find('ul').prev('a').addClass('arrows');
    h.find('a').hover(tm, hm);
    $('input[type="checkbox"]').addClass('checkbox');
    $('td.country,.menu .country').each(function (p0, p) {
        if (
            $(p)
                .html()
                .match(/^[A-Z][A-Z]$/)
        ) {
            var cn = $(p).text();
            $(p).html('<img src="/_images/countries/' + cn.toLowerCase() + '.gif" alt="' + cn + '" title="' + cn + '" border="0" class="flag" />');
        }
    });
    $('.members_content').each(cmstoggle);

    //verify anchor
    if (window.location.href.toString().search(/#/) > -1) {
        var anchor = window.location.href.toString().substr(window.location.href.toString().search(/#/));
        showdesc(anchor);
    }

    var req = $('.sf_admin_form_row.required label'),
        p;
    req.each(function (i, o) {
        if ($('span.req', o).length == 0) {
            $(o).append('<span class="req" style="color:#cc0000" title="Mandatory">*</span>');
        }
    });
    req = $('input.required,select.required,textarea.required');
    req.each(function (i, o) {
        o = $(o);
        p = $('label[for="' + o.attr('id').replace(/_[0-9]+$/, '') + '"]', o.parents('.field').eq(0));
        if (p.length > 0 && $('span.req', p).length == 0) {
            p.append('<span class="req" style="color:#cc0000" title="Mandatory">*</span>');
        }
    });

    //alternates table
    $('table.alternate').load(alttable('table.alternate', 'even', 'odd'));

    // autosave
    var fa = $('form.autosave');
    if (fa.length > 0) autosave(fa);

    if (document.querySelectorAll('.banners.loop .banner').length > 1)
        (function () {
            var p = 0,
                t = 0;
            function b() {
                if (t) clearTimeout(t);
                var B = document.querySelectorAll('.banners.loop .banner'),
                    i = B.length,
                    C = document.querySelectorAll('#banner-control a');
                if (C.length == 0) {
                    var c = document.createElement('div'),
                        a;
                    C = [];
                    c.id = 'banner-control';
                    while (i-- > 0) {
                        a = document.createElement('a');
                        a.id = 'b' + C.length;
                        if (a.addEventListener) {
                            a.addEventListener('click', clk, false);
                        } else if (a.attachEvent) {
                            a.attachEvent('onclick', clk);
                        } else {
                            o['onclick'] = clk;
                        }
                        C.push(c.appendChild(a));
                        a = null;
                    }
                    C[0].className = 'active';
                    i = B.length;
                    B[0].parentNode.parentNode.insertBefore(c, B[0].parentNode);
                    c = null;
                    p = 0;
                } else {
                    if (p >= i) p = 0;
                    while (i-- > 0) {
                        if (i != p) {
                            if (B[i].className.search(/\bactive\b/) > -1) {
                                B[i].className = B[i].className.replace(/\s*\bactive\b/g, ' last');
                            } else if (B[i].className.search(/\blast\b/) > -1) {
                                B[i].className = B[i].className.replace(/\s*\blast\b/g, '');
                            }
                            if (C[i].className.search(/\bactive\b/) > -1) {
                                C[i].className = '';
                            }
                        } else {
                            if (B[i].className.search(/\bactive\b/) < 0) {
                                B[i].className += ' active';
                                C[i].className = 'active';
                            }
                        }
                    }
                }
                p++;
                t = setTimeout(b, 10000);
            }
            function clk() {
                clearTimeout(t);
                t = 0;
                p = parseInt(this.id.substr(1));
                b();
            }

            if (document.querySelectorAll('.banners.loop .banner').length > 1) b();
        })();
});

var maketoc = function (i, o) {
    var s = '',
        h = $(o),
        nn = h.get(0).nodeName,
        nxt = h.nextAll(),
        fnl = -1;
    if (nxt.length == 0) {
        return false;
    }
    nxt.each(function (ni, no) {
        if (fnl >= 0) {
            return false;
        } else if ($(no).get(0).nodeName == nn) {
            fnl = ni;
        }
    });
    if (fnl >= 0) {
        nxt = nxt.slice(0, fnl);
    }
    if (nxt.length < 1) {
        return true;
    }
    nxt.wrapAll('<div class="tocdesc"></div>');
    if (h.attr('class') == 'onload-open' || h.attr('class') == 'keep-open') {
        h.addClass('toctopic').next('.tocdesc');
    } else {
        h.addClass('toctopic').next('.tocdesc').hide('slow');
    }
    h.bind('click', showdesc);
};

var toggletoc = function (b) {
    b = $(b);
    var h = b.parent();
    if (b.text().search(/^Show/) > -1) {
        if (h.attr('class') != 'keep-open') {
            h.find('.tocdesc').show('slow');
        }
        h.find('.toctopic').addClass('orange');
        b.html('Hide all');
    } else {
        if (h.attr('class') != 'keep-open') {
            h.find('.tocdesc').hide('slow');
        }
        h.find('.toctopic').removeClass('orange');
        b.html('Show all');
    }
    return false;
};

var cmstoggle = function (i, o) {
    if (typeof i == 'object') {
        var b = $(i);
        if (b.hasClass('active')) return false;
        var m = b.parent('.toggle').next('.members_content'),
            p = m.next('.public_content');
        if (b.hasClass('members')) {
            p.hide('fast');
            m.show('fast');
        } else {
            m.hide('fast');
            p.show('fast');
        }
        b.parent('.toggle').find('a').toggleClass('active');
        return false;
    } else {
        var m = $(o),
            p = m.next('.public_content');
        m.before(
            '<span class="toggle small"><a class="members active" href="#members" onclick="cmstoggle(this);return false;">Members</a> | <a class="public" href="#public" onclick="cmstoggle(this);return false;">Public</a></span>'
        );
        var b = m.prev('.toggle');
        b.css('opacity', 0.4).hover(highlight, dim);
        if (
            m.find('.sympal_slot_content').length > 0 &&
            m
                .find('.sympal_slot_content')
                .html()
                .search(/^(\[[^<>]+\])?$/) > -1
        ) {
            b.find('a').toggleClass('active');
            m.hide();
        } else p.hide();
    }
};
var highlight = function () {
    $(this).css('opacity', 1);
};
var dim = function () {
    $(this).css('opacity', 0.4);
};
var showdesc = function () {
    var t = false;
    if (showdesc.arguments.length > 0 && showdesc.arguments[0].toString().substr(0, 1) == '#') {
        t = $(showdesc.arguments[0]);
        if (!t.hasClass('toctopic')) t = t.find('.toctopic').eq(0);
    } else t = $(this);
    if (showdesc.arguments.length > 1 && typeof showdesc.arguments[1] != 'object') {
        var show = showdesc.arguments[1];
        if (show) {
            t.addClass('orange').next('.tocdesc').show('slow');
        } else {
            t.removeClass('orange').next('.tocdesc').hide('slow');
        }
    } else t.toggleClass('orange').next('.tocdesc').toggle('slow');
};
var makesm = function () {
    $('.menu').addClass('dynamic').find('a:not(.ancestor) + ul').hide();
    $('.menu ul a').each(function (idx, dom) {
        //excont = '<span><img src="/_images/arrows.png" /></span>';
        excont = '<span><img src="/_images/blank.png" /></span>';
        sb = $(dom).next();
        hasmenu = $(dom).next().is('ul');
        if (hasmenu) {
            var sbname = 'sb' + idx;
            sb.addClass(sbname);
            $(dom).hover(smh, smout);
            //excont = '<span><a href="#" onclick="actionmenu(\''+sbname+'\');"><img src="/_images/arrows.png" /></a></span>';
            excont = '<span class="hasb"><a href="#" onclick="actionmenu(\'' + sbname + '\');"><img src="/_images/blank.png" /></a></span>';
        }
        $(dom).parent().prepend(excont);
    });
};
var smh = function () {
    var sbname = $(this).next('ul').attr('class');
    timer = setTimeout("actionmenu('" + sbname + "');", 500);
};
var smout = function () {
    clearTimeout(timer);
};
var tm = function (e) {
    clearTimeout(mt);
    e.stopPropagation();
    $('#headernav .display').removeClass('display');
    $(this).addClass('display');
    $(this).parents('ul').addClass('display');
    $(this).next('ul').addClass('display');
    $('#headernav ul.display').prev('a').addClass('display');
    $('#headernav ul ul:not(.display)').stop(true, true).fadeOut(to, te);
    $('#headernav ul.display').stop(true, true).delay(300).slideDown(to, te);
};
var to = 300;
var te = 'swing';
var mt = 0;
var hm = function (e) {
    mt = setTimeout(function () {
        $('#headernav .display').removeClass('display');
        $('#headernav ul ul:not(.display)').stop(true, true).fadeOut(to, te);
    }, 200);
};

var bm = function (s) {
    if (typeof s == 'undefined') return '';
    var r = '<ul>';
    for (var i = 0; i < s.length; i++) {
        if (typeof s[i].u != 'undefined') r += '<li><a href="' + s[i].u + '">' + s[i].t + '</a>';
        else r += '<li><a>' + s[i].t + '</a>';
        if (typeof s[i].m != 'undefined') r += bm(s[i].m);
        r += '</li>';
    }
    r += '</ul>';
    return r;
};

var actionmenu = function (o) {
    if (o != undefined) {
        var obj = $('.' + o);
        if (obj.parent('li').find('span:first').hasClass('sopen')) {
            obj.parent('li').find('span:first').removeClass('sopen');
        } else {
            obj.parent('li').find('span:first').addClass('sopen');
        }
        obj.slideToggle('fast', 'swing');
    }
};

/**
 * Alternates the class in the table row according to your order.
 */
var alttable = function (classobj, even, odd) {
    if (even != '') {
        $(classobj + ' tr:even').addClass(even);
    }

    if (odd != '') {
        $(classobj + ' tr:odd').addClass(odd);
    }
};
var timer = 0;

var f = {
    show: function (a, b) {
        if (arguments.length > 1) $(b).not(a).slideUp('fast');
        $(a).slideDown('normal');
        return false;
    },
};

var autosave = function (f) {
    $('input,select').change(autosaveForm);
};

var autosaveForm = function () {
    var f = $(this).parents('form');
    // wait a minute before submitting
    var t = new Date().getTime();
    if (!f.data('timeout')) {
        f.data('timeout', t + 30000);
        return true;
    } else if (f.data('timeout') > t) {
        return true;
    } else {
        f.data('timeout', t + 30000);
    }

    // check if any required field is empty
    var valid = true;
    $('.required input, .required select').each(function (i, o) {
        o = $(o);
        if (o.val() == '') valid = false;
    });

    if (valid) {
        $.ajax({
            type: 'POST',
            url: f.attr('action'),
            data: f.serialize(),
            dataType: 'html',
            headers: { 'Tdz-Return': 'form' },
            success: autosaveSync,
            context: f,
        });
    }
};

var autosaveSync = function (d) {
    var f0 = $(this),
        f = $('form', d);
    if (f.attr('action') != f0.attr('action')) f0.attr('action', f.attr('action'));

    // compare and update fields
};
