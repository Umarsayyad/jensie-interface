import app from '../app';
import * as _ from 'lodash';

app.service('NotificationService', function ($rootScope, socketService, FindingService, MessageService, Users) {
    function checkNotificationsPermitted() {
        requestNotifications();
        return $rootScope.notificationsPermitted;
    }

    function requestNotifications() {
        if ('Notification' in window) {
            if (Notification.permission === 'granted') {
                $rootScope.notificationsPermitted = true;
            } else if (Notification.permission === 'denied') {
                //console.log('notifications were denied');
                $rootScope.notificationsPermitted = false;
            } else {
                Notification.requestPermission(function (p) {
                    if (p === 'granted') {
                        $rootScope.notificationsPermitted = true;
                        _.defer(function () {
                            $rootScope.$apply(function () {
                                $rootScope.notificationsPermitted = true;
                            });
                        });
                    }
                });
            }
        }
    }

    // This function is more for one-off notification calls, not used when setting up notifications
    function notify(title, options) {
        if (checkNotificationsPermitted()) {
            var notification = new Notification(title, options);
        }
    }

    function subscribedTo(subscription, triggerVal?: any) {
        var sub = _.find($rootScope.gdata.notifications, function (n) {
            return n.notification_type.toLowerCase() === 'browser' && n.event_type.toLowerCase() === subscription.toLowerCase();
        });

        if (sub) {
            if (!_.isNil(triggerVal)) {
                return _.includes(sub.triggers, triggerVal);
            } else {
                return true;
            }
        }
    }

    // User Notifications
    function userNotification(subscription, user, additionalBody?: String) {
        if (checkNotificationsPermitted() && subscribedTo(subscription)) {
            var title = 'New ' + user.type + ' created';
            var body = user.first_name + ' ' + user.last_name + ': ' + user.email;
            if (additionalBody) {
                body += '\n' + additionalBody;
            }
            var options = {
                body: body,
                //icon: "img/envelope.png",
                tag: subscription + user.id,
            };
            var notification = new Notification(title, options);
            setTimeout(function () {
                notification.close();
            }, 5000);
        }
    }

    function subscribeToNewUser() {
        socketService.subscribeByModel('user', function (user) {
            var additionalBody = '';
            if (user.customer) {
                additionalBody += $rootScope.catotheme.customerCapitalize + ': ' + $rootScope.gdata.customerMap[user.customer].name;
            } else if (user.type === 'operator') {
                additionalBody += 'Level: ' + user.level;
            }

            // catotheme.customer users, once they get new user notifications, still will never see operators or admins added
            if (user.is_superuser && subscribedTo('new admin')) {
                userNotification('new admin', user);
            } else if (user.type === 'operator' && subscribedTo('new operator')) {
                userNotification('new operator', user, additionalBody);
            } else {
                userNotification('new user', user, additionalBody);
            }
        });
    }

    function userRequestNotification(subscription, user, add, additionalBody?: String) {
        if (checkNotificationsPermitted() && subscribedTo(subscription)) {
            var title = 'New User Request';
            var customerName = $rootScope.gdata.customerMap[user.customer].name;
            var body = user.first_name + ' ' + user.last_name + ': ' + user.email + ' for customer ' + customerName;
            if (additionalBody) {
                body += '\n' + additionalBody;
            }
            var options = {
                body: body,
                //icon: "img/envelope.png",
                tag: subscription + user.id,
            };
            var notification = new Notification(title, options);
            setTimeout(function () {
                notification.close();
            }, 5000);
        }
    }

    function subscribeToRequestedUser() {
        socketService.subscribeByGroup('md-users-requested', function (user) {
            userRequestNotification('new requested user', user, 'New requested user');
        });
    }

    // Operation Notifications
    function operationNotification(subscription, operation, title) {
        if (checkNotificationsPermitted() && subscribedTo(subscription)) {
            var body = operation.name + '\n' + operation.description;
            var options = {
                body: body,
                //icon: "img/envelope.png",
                tag: subscription + operation.id,
            };
            var notification = new Notification(title, options);
            setTimeout(function () {
                notification.close();
            }, 5000);
        }
    }

    function subscribeToNewOperation() {
        if ($rootScope.hasRole('mission_director')) {
            socketService.subscribeByModel('operation', function (operation) {
                if (operation.created) {
                    operationNotification('new operation', operation, 'New operation created');
                }
            });
        } else if ($rootScope.hasRole('operator')) {
            socketService.subscribeByGroup('op-operations-new', function (operation) {
                operationNotification('new operation', operation, 'New operation created');
            });
        }
    }

    function subscribeToAvailableOperation() {
        // Only allow customers to do this (for now)
        if ($rootScope.user.type === 'customer') {
            socketService.subscribeByModel('operation', function (operation) {
                operationNotification('available operation', operation, 'Operation available');
            });
        }
    }

    // TipOffEvent Notifications
    function tipoffNotification(subscription, tipoffevent, title) {
        if (checkNotificationsPermitted() && subscribedTo(subscription)) {
            var body = tipoffevent.tipoff.type + ' Tip-Off ' + tipoffevent.tipoff.name + '\nfor: ';
            body += stringify(tipoffevent.tipoff, ['target', 'target_type', 'target_value', 'keywords']);
            var options = {
                body: body,
                tag: subscription + tipoffevent.id,
            };
            var notification = new Notification(title, options);
            setTimeout(function () {
                notification.close();
            }, 5000);
        }
    }

    function subscribeToNewTipoff() {
        if ($rootScope.hasRole('operator')) {
            socketService.subscribeByModel('tipoffevent', function (tipoffevent) {
                tipoffNotification('new tipoff', tipoffevent, 'New tipoff');
            });
        }
    }

    // TargetEvent Notifications
    function targetNotification(subscription, targetevent, additionalBody) {
        if (checkNotificationsPermitted() && subscribedTo(subscription)) {
            var title = 'New Target';
            var was_created = targetevent.created ? 'created' : 'modified';
            var body = 'A ' + targetevent.type + ' Target for ' + targetevent.value + ' was ' + was_created + ':\n\n';
            if (additionalBody) {
                body += '\n' + additionalBody + '\n\n';
            }
            body += stringify(targetevent, ['active', 'cpe', 'hardware_address', 'hardware_type', 'locked', 'owner', 'position']);
            var options = {
                body: body,
                tag: subscription + targetevent.id,
            };
            var notification = new Notification(title, options);
            setTimeout(function () {
                notification.close();
            }, 5000);
        }
    }

    function stringify(json_object, filter_items) {
        return _(json_object)
            .pick(filter_items)
            .omitBy(_.isNil)
            .omitBy(function (v, k) {
                return _.isString(v) ? _.isEmpty(v) : false;
            })
            .map(function (v, k) {
                return k + ': ' + v;
            })
            .join(', \n');
    }

    // Target Notifications
    function subscribeToNewTarget() {
        socketService.subscribeByModel('target', function (targetevent) {
            if (targetevent.created) {
                var additionalBody = $rootScope.catotheme.customerCapitalize + ': ' + $rootScope.gdata.customerMap[targetevent.customer].name;
                targetNotification('new target', targetevent, additionalBody);
            }
        });
    }

    // Finding Notifications
    function findingNotification(subscription, finding) {
        if (checkNotificationsPermitted() && subscribedTo(subscription, finding.severity)) {
            var title = 'New Finding';
            var options = {
                body: finding.title + ' - ' + FindingService.SEVERITY_LEVEL[finding.severity] + '\n' + finding.description,
                //icon: "img/envelope.png",
                tag: subscription + finding.id,
            };
            var notification = new Notification(title, options);
            setTimeout(function () {
                notification.close();
            }, 5000);
        }
    }

    function subscribeToNewFinding() {
        socketService.subscribeByModel('finding', function (finding) {
            if ($rootScope.user.type === 'customer' && finding.approved) {
                findingNotification('new finding', finding);
            } else if ($rootScope.user.type === 'operator' && finding.created) {
                findingNotification('new finding', finding);
            }
        });
    }

    // Message Notifications
    function messageNotification(subscription, message) {
        if (checkNotificationsPermitted() && subscribedTo(subscription)) {
            var title = 'New Message';
            var options = {
                body: MessageService.getNameOrInitials(message) + ': ' + message.value,
                icon: 'img/envelope.png',
                tag: subscription + message.id,
            };
            var notification = new Notification(title, options);
            setTimeout(function () {
                notification.close();
            }, 5000);
        }
    }

    function subscribeToNewMessages() {
        socketService.subscribeByModel('message', function (message) {
            if (message.author.id !== $rootScope.user.id) {
                messageNotification('new message', message);
            }
        });
    }

    // Campaign Notifications
    function campaignNotification(subscription, campaign) {
        if (checkNotificationsPermitted() && subscribedTo(subscription)) {
            var title = 'New Campaign';
            var options = {
                body: campaign.title + ' for ' + $rootScope.gdata.customerMap[campaign.customer].name,
                tag: subscription + campaign.id,
            };
            var notification = new Notification(title, options);
            setTimeout(function () {
                notification.close();
            }, 5000);
        }
    }

    function subscribeToNewCampaigns() {
        if ($rootScope.hasRole('operator')) {
            socketService.subscribeByModel('campaign', function (campaign) {
                if (campaign.created) {
                    campaignNotification('New Campaign', campaign);
                }
            });
        }
    }

    function setupNotifications() {
        requestNotifications();
        // Subscribe to all notifications, handle whether or not the user receives the notification at the time of the notification
        // Whether or not the user is able to subscribe is handled in the subscription function
        subscribeToNewUser();
        subscribeToNewCampaigns();
        subscribeToRequestedUser();
        subscribeToNewOperation();
        subscribeToAvailableOperation();
        subscribeToNewFinding();
        subscribeToNewMessages();
        subscribeToNewTipoff();
        subscribeToNewTarget();
    }

    return {
        notify: notify,
        requestNotifications: requestNotifications,
        checkNotificationsPermitted: checkNotificationsPermitted,
        setupNotifications: setupNotifications,
    };
});
