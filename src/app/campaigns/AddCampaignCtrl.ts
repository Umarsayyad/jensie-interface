import app from '../app';
import * as _ from 'lodash';

import '../common/filters/applianceZoneOrExternal';
import '../customer/manage/CondensedCustomerFilterComp';
import '../roe/utils/viewRoE';

app.controller('AddCampaignLoaderCtrl', function ($scope, $uibModal) {
    $uibModal.open({
        templateUrl: '/app/campaigns/modalAddCampaign.html',
        controller: 'AddCampaignCtrl',
        size: 'lg',
        scope: $scope,
    });
});

app.controller('AddCampaignCtrl', function ($scope, $state, $uibModalInstance, modalService, Campaigns, Targets, MetaTargets, CampaignTemplates) {
    $scope.campaign = {};
    $scope.targets = {};
    $scope.metatargets = {};
    $scope.types = null;
    var old_title = '';

    if ($state.params.templateId) {
        $scope.campaign.campaign_template = $state.params.templateId;
        if (_.isUndefined($state.params.template)) {
            // page reloaded, direct link, etc.
            $scope.campaignTemplate = CampaignTemplates.getByID({ templateId: $scope.campaign.campaign_template }, function (campaign_template) {
                if (!$scope.campaign.title) {
                    $scope.campaign.title = campaign_template.title;
                }
                $scope.types = campaign_template.types;
            });
        } else {
            $scope.campaignTemplate = $state.params.template;
            if ($scope.campaign.title === '') {
                $scope.campaign.title = $scope.campaignTemplate.title;
            }
            $scope.types = $scope.campaignTemplate.types;
        }
    } else {
        $scope.campaignTemplates = CampaignTemplates.list();
    }

    Campaigns.options(function (result) {
        $scope.campaignConfig = result.actions.POST;
    });

    $scope.addCampaign = function () {
        $scope.data.campaignSubmission = Campaigns.add(
            $scope.campaign,
            function (campaign) {
                campaign.findings = [];
                $scope.finish(campaign);
            },
            function (result) {
                modalService.openErrorResponse('Campaign could not be saved.', result, false);
            }
        );
    };

    $scope.steps = [
        {
            templateUrl: '/app/campaigns/steps/creation.html',
            hasForm: true,
        },
    ];

    $scope.setCustomer = function (customer) {
        if (customer) {
            $scope.campaign.customer = customer.id;
            if (!$scope.targets[customer.id]) {
                $scope.targets[customer.id] = Targets.basicList({ customer: customer.id });
                $scope.metatargets[customer.id] = MetaTargets.list({ customer: customer.id });
            }
        } else {
            $scope.campaign.customer = undefined;
        }
    };
    $scope.setCustomer(_.get($scope.user, 'ui_data.selectedCustomer'));

    $scope.templateTargetFilter = function (item) {
        return item.active === true && _.includes($scope.types, item.type);
    };

    $scope.campaignTemplateChanged = function () {
        var campaign_template: any = _.find($scope.campaignTemplates, { id: $scope.campaign.campaign_template });
        if (campaign_template) {
            $scope.types = campaign_template.types;
            if (!$scope.campaign.title || $scope.campaign.title === old_title) {
                $scope.campaign.title = campaign_template.title;
            }
            old_title = campaign_template.title;
        } else {
            $scope.types = null;
        }
    };

    $scope.finish = function (campaign) {
        $uibModalInstance.close(campaign);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancelled');
    };

    $uibModalInstance.result.then(
        function () {
            $state.go('^');
        },
        function () {
            $state.go('^');
        }
    );
});
