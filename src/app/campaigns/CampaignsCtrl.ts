import app from '../app';
import * as _ from 'lodash';

import '../common/actionsDropdownComp';
import '../operation/OperationService';

app.controller('CampaignsCtrl', function (
    $scope,
    $rootScope,
    $sce,
    $state,
    NgTableParams,
    modalService,
    OperationService,
    ngTableService,
    socketService,
    Operations,
    Campaigns
) {
    $scope.config = {};
    $scope.filters = {};
    $scope.error = '';
    $scope.operationProcesses = [
        { id: 'automated', title: 'automated' },
        { id: 'manual', title: 'manual' },
        { id: 'both', title: 'both' },
        { id: 'empty', title: 'empty' },
    ];
    $scope.operationStates = _.map(OperationService.STATES, function (s) {
        return { id: s.label, title: s.label };
    });
    $scope.stateClass = OperationService.stateClass;
    $scope.stateIcons = {
        ready: 'fa-play',
        'manual only': 'fa-wrench',
        'in progress': 'fa-spinner fa-spin',
        completed: 'fa-check',
        empty: 'fa-minus',
    };

    $scope.data.campaigns = Campaigns.list({ findingcount: true }, function () {
        _.each($scope.data.campaigns, setUpCampaign);
        $scope.campaignTable.total($scope.data.campaigns.length);
        $scope.campaignTable.reload();
    });

    function filterCustomer(customer) {
        if (customer) {
            $scope.filters['customerName'] = customer.name;
        } else {
            delete $scope.filters['customerName'];
        }
    }

    $scope.$watch('user.ui_data.selectedCustomer', function (newVal, oldVal) {
        filterCustomer(newVal);
    });

    if (_.get($rootScope.user, 'ui_data.selectedCustomer')) {
        filterCustomer(_.get($rootScope.user, 'ui_data.selectedCustomer'));
    }

    $scope.campaignTable = new NgTableParams(
        {
            count: 25,
            filter: $scope.filters,
        },
        {
            dataset: $scope.data.campaigns,
        }
    );

    $scope.actions = {
        stopCampaign: {
            action: function (campaign) {
                modalService.openConfirm(
                    'Are you sure?',
                    'stop',
                    campaign.title +
                        '? Stopped operations CANNOT be resumed. This will delete any associated operations that are not completed or in review. Would you like to continue',
                    {},
                    function () {
                        Campaigns.stop(
                            { campaignId: campaign.id },
                            {},
                            function (result) {
                                $scope.updateObjOld(_.find($scope.data.campaigns, { id: campaign.id }), result);
                            },
                            function (err) {
                                modalService.openErrorResponse('Stop Error', err, false);
                            }
                        );
                    }
                );
            },
            class: 'btn-white',
            icon: 'fa-stop',
            label: 'Stop',
            tooltip: 'Stop this campaign',
        },
        deleteCampaign: {
            action: function (campaign) {
                modalService.openDelete('Confirmation', "campaign '" + campaign.title + "'", {}, function () {
                    Campaigns.delete(
                        { campaignId: campaign.id },
                        {},
                        function () {
                            _.remove($scope.data.campaigns, { id: campaign.id });
                            $scope.campaignTable.total($scope.data.campaigns.length);
                            $scope.campaignTable.reload();
                        },
                        function (err) {
                            modalService.openErrorResponse('Stop Error', err, false);
                        }
                    );
                });
            },
            class: 'btn-white',
            icon: 'fa-trash',
            label: 'Delete',
            tooltip: 'Delete this campaign',
        },
    };

    $scope.stopAllOperationsAndCampaign = function (campaign) {
        /*
        modalService.openConfirm("Are you sure?", "stop", campaign.title
                + "? Stopped operations CANNOT be resumed. This will delete any associated operations that are not completed or in review. Would you like to continue", {}, function(){
                 // Put in call to cancel all (tasks) and operations, and then the campaign...or have the API handle all that
                }
            });
        */
    };

    $scope.automatedCampaignStarted = function (updatedCampaign) {
        var campaign: any = _.find($scope.data.campaigns, { id: updatedCampaign.id });
        $scope.updateObj(campaign, updatedCampaign);
        campaign.automated = true;
    };

    $scope.automationButtonClicked = function (campaign) {
        if (campaign.state === 'ready') {
            $state.go('cato.loggedIn.dashboard.campaigns.runAutomatedOperations', { campaignId: campaign.id });
        }
    };

    var campaignUnsubscribe = socketService.subscribeByModel('campaign', function (updatedCampaign) {
        var campaign = _.find($scope.data.campaigns, { id: updatedCampaign.id });

        if (!campaign) {
            campaign = updatedCampaign;
            setUpCampaign(campaign);
            $scope.data.campaigns.push(campaign);
            $scope.campaignTable.total($scope.data.campaigns.length);
            $scope.campaignTable.reload();
        }

        Campaigns.get({ campaignId: campaign.id, findingcount: true }, function (campaignWithStatus) {
            $scope.updateObj(campaign, campaignWithStatus);
        });
    });

    //// TODO: update operations count and any displayed operations in popover
    //var operationUnsubscribe = socketService.subscribeByModel('operation', function(operation) {
    //
    //});

    $scope.$on('$destroy', function () {
        //operationUnsubscribe();
        campaignUnsubscribe();
    });

    $scope.toggleOperationQuickView = function (campaign) {
        campaign.popoverIsOpen = !campaign.popoverIsOpen;
    };

    function setUpCampaign(campaign) {
        campaign.operationTable = ngTableService.serverFilteredTable({
            resource: Operations.get,
            defaultParams: {
                campaign: campaign.id,
                include_automated: true,
            },
            extraInitialParams: {
                count: 20,
            },
            extraInitialSettings: {
                counts: [5, 10, 20, 40],
            },
        });

        campaign.customerName = $scope.gdata.customerMap[campaign.customer].name;
    }
});
