import app from '../../app';
import * as _ from 'lodash';

app.component('viewRoe', {
    bindings: {
        roeId: '<',
        customerId: '<',
        color: '<',
        header: '<',
    },
    template:
        "<span ng-if='$ctrl.customerId' style='display: inline-block'>" +
        "   <span class='form-control form-control-static'" +
        "       style='background-color:initial;border:none'>" +
        "           <a ng-if='$ctrl.roeId' href='#' ng-click='$ctrl.downloadRoE()'" +
        "           style='color:{{$ctrl.color}};text-decoration: underline'>" +
        "               <h3 ng-if='$ctrl.header'>View Current RoE</h3>" +
        "               <p ng-if='!$ctrl.header'>View Current RoE</p>" +
        '           </a>' +
        "           <h3 ng-if='!$ctrl.roeId && $ctrl.header' style='color:{{$ctrl.color}}'>No RoE Available</h3>" +
        "           <p ng-if='!$ctrl.roeId && !$ctrl.header' style='color:{{$ctrl.color}}'>No RoE Available</p>" +
        '   </span>' +
        '</span>',
    controller: function nextCtrl(RulesOfEngagements, FileSaver) {
        //console.log("in view RoE controller");
        var ctrl = this;

        ctrl.$onInit = function () {
            ctrl.color = ctrl.color ? ctrl.color : 'white';
        };

        ctrl.downloadRoE = function () {
            var response = RulesOfEngagements.downloadSigned({ customerId: ctrl.customerId, roeId: ctrl.roeId }).$promise;
            response.then(function (data) {
                FileSaver.saveAs(data.blob, 'Rules of Engagement');
            });
        };
    },
});
