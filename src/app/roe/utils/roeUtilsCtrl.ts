import app from '../../app';
import * as _ from 'lodash';
import * as angular from 'angular';

app.provider(
    'roeUtilsCtrl',
    class RoeUtilsCtrl {
        $get() {
            //console.log("in utils controller");
            return {
                calculateRoeHeight: function (window, heightContainer, adjustment, staticDiv1, staticDiv2) {
                    if (!(adjustment || staticDiv1 || staticDiv2)) {
                        var screenHeight = Number(angular.element('#wizardContainer').css('height').slice(0, -2));
                        angular.element(heightContainer).css('height', screenHeight - 50);
                        return;
                    } else {
                        adjustment = adjustment ? adjustment : (adjustment = 325);
                        if (staticDiv1) {
                            adjustment += Number(angular.element(staticDiv1).css('height').slice(0, -2));
                        }
                        if (staticDiv2) {
                            adjustment += Number(angular.element(staticDiv2).css('height').slice(0, -2));
                        }
                    }
                    var screenHeight = window.innerHeight - adjustment;
                    angular.element(heightContainer).css('height', screenHeight);
                },

                calculateRoeWidth: function (outerContainer, primaryContaier, secondaryContaier) {
                    if (angular.element(primaryContaier)[0] && angular.element(secondaryContaier)[0]) {
                        var primaryContainerWidth = angular.element(primaryContaier)[0].offsetWidth;
                        var outerContainerWidth = angular.element(outerContainer)[0].offsetWidth;
                        var secondaryContainerWidth = outerContainerWidth - primaryContainerWidth - 10;
                        angular.element(secondaryContaier).css('width', secondaryContainerWidth);
                    }
                },

                calculateRoeWindowDemensions: function (
                    window,
                    outerContainer,
                    primaryContaier,
                    secondaryContaier,
                    heightContainer,
                    heightAdjustment
                ) {
                    this.calculateRoeHeight(window, heightContainer, heightAdjustment);
                    this.calculateRoeWidth(outerContainer, primaryContaier, secondaryContaier);
                },
            };
        }
    }
);
