import app from '../../app';
import * as _ from 'lodash';

app.component('next', {
    bindings: {
        nextEnabled: '<',
    },
    template:
        "<div class='row text-center roe-buttons'>" +
        "<input ng-disabled='!$ctrl.nextEnabled' ng-click='$ctrl.nextPage()' class='btn btn-default' type='button' wz-next value='Next'/>" +
        '</div>',
    controller: function nextCtrl($rootScope) {
        //console.log("in next button controller");
        var ctrl = this;

        ctrl.$onInit = function () {
            ctrl.nextEnabled = ctrl.nextEnabled === undefined ? true : ctrl.nextEnabled;
        };

        ctrl.nextPage = function () {
            $rootScope.$emit('nextPage');
        };
    },
});
