import app from '../../app';
import * as _ from 'lodash';

app.component('backAndNext', {
    bindings: {
        nextEnabled: '<',
        submit: '&',
        resetPage: '&',
        nextLabel: '<',
    },
    template:
        "<div class='row text-center roe-buttons'>" +
        "<input class='btn btn-default' type='button' wz-previous ng-click='$ctrl.prevPage()' value='Back'/>" +
        "<input ng-disabled='!$ctrl.nextEnabled' class='btn btn-default' ng-click='$ctrl.nextPage(); $ctrl.submit()' type='button' wz-next value='{{$ctrl.nextLabel}}'/>" +
        '</div>',
    controller: function backAndNextCtrl($rootScope) {
        //console.log("in backAndNext button controller");
        var ctrl = this;

        ctrl.$onInit = function () {
            ctrl.nextEnabled = ctrl.nextEnabled === undefined ? true : ctrl.nextEnabled;
            ctrl.nextLabel = ctrl.nextLabel === undefined ? 'Next' : ctrl.nextLabel;
        };

        ctrl.nextPage = function () {
            $rootScope.$emit('nextPage');
        };

        ctrl.prevPage = function () {
            $rootScope.$emit('prevPage');
            if (ctrl.resetPage) {
                ctrl.resetPage();
            }
        };
    },
});
