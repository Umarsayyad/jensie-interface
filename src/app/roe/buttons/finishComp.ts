import app from '../../app';
import * as _ from 'lodash';

app.component('finish', {
    template:
        "<div class='row text-center roe-buttons' style='margin-top:3em'>" +
        "<a class='btn btn-default' ui-sref='cato.loggedIn.dashboard' ng-disabled='!$ctrl.roe.is_approved' wz-finish>FINISH</a>" +
        '</div>',
    bindings: {
        roe: '<',
    },
    controller: function finishCtrl() {
        //console.log("in finish button controller");
    },
});
