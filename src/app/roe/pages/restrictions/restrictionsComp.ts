import app from '../../../app';
import * as _ from 'lodash';
import * as angular from 'angular';

import '../../buttons/backAndNextComp';
import '../../pages/restrictions/addEditRestrictionModal/addEditRestrictionComp';
import '../../utils/roeUtilsCtrl';

app.component('restrictions', {
    templateUrl: '/app/roe/pages/restrictions/restrictions.html',
    bindings: {
        roe: '<',
        changePage: '<',
    },

    controller: function restrictionsCtrl(
        $window,
        $rootScope,
        $uibModal,
        stateBuilder,
        Restrictions,
        globalDataService,
        roeUtilsCtrl,
        NgTableParams,
        modalService,
        Targets,
        WizardHandler
    ) {
        //console.log("in restrictions controller");

        var ctrl = this;
        ctrl.gdata = globalDataService;
        ctrl.restrictions = [];
        ctrl.allRoeRestrictionsSelected = false;
        ctrl.restrictionsTable = new NgTableParams({}, {});
        ctrl.targetMap = {};
        ctrl.serviceNumsToNames = {};

        ctrl.data = {
            selectedRestrictions: [],
            restrictionsSelected: false,
        };

        var appWindow = angular.element($window);
        var setDimensions = function () {
            roeUtilsCtrl.calculateRoeHeight($window, '#restrictionsTableWrapper', 308, '#staticHeightContentRestrictions');
            roeUtilsCtrl.calculateRoeHeight($window, '#restrictionsTable', 450, '#staticHeightContentRestrictions');
        };

        ctrl.$onDestroy = function () {
            appWindow.unbind('resize', setDimensions);
        };

        ctrl.$onInit = function () {
            appWindow.bind('resize', setDimensions);
            ctrl.getCustomerTargets();
        };

        ctrl.$onChanges = function () {
            if (WizardHandler.wizard().currentStepTitle() === 'Restrictions') {
                setDimensions();

                if (ctrl.roe) {
                    ctrl.restrictions = [];
                    ctrl.serviceNumsToNames = ctrl.servicesNumsToNames();
                    Restrictions.list({ customerId: ctrl.gdata.customer.id, roeId: ctrl.roe.id }).$promise.then(function (restrictions) {
                        ctrl.getCustomerTargets();
                        _.forEach(restrictions, function (restriction, index, array) {
                            var targets = ctrl.processTargets(restriction);

                            ctrl.restrictions.push({
                                id: restriction.id,
                                is_global: restriction.is_global,
                                targets: targets,
                                targetsPreview: ctrl.createTargetsPreview(targets),
                                permitted: restriction.type === 'allow' ? true : false,
                                startDate: restriction.start_date,
                                startTime: restriction.start_time,
                                endDate: restriction.end_date,
                                endTime: restriction.end_time,
                                days_of_week: ctrl.processDaysOfWeek(restriction.days_of_week),
                                services: restriction.services,
                                servicesPreview: ctrl.serviceNumsToNames[restriction.services[0]] + (restriction.services.length > 1 ? '...' : ''),
                            });
                        });
                    });
                }
            }
        };

        ctrl.showDaysOfWeek = function (restriction) {
            if (typeof restriction.days_of_week === 'string') {
                return restriction.days_of_week;
            } else {
                restriction.days_of_week = ctrl.processDaysOfWeek(restriction.days_of_week);
                return restriction.days_of_week;
            }
            return '';
        };

        ctrl.processDaysOfWeek = function (days_of_week) {
            var days_of_weekOut = '';
            if (days_of_week.sunday) {
                days_of_weekOut += 'Sun ';
            }
            if (days_of_week.monday) {
                days_of_weekOut += 'Mon ';
            }
            if (days_of_week.tuesday) {
                days_of_weekOut += 'Tue ';
            }
            if (days_of_week.wednesday) {
                days_of_weekOut += 'Wed ';
            }
            if (days_of_week.thursday) {
                days_of_weekOut += 'Thu ';
            }
            if (days_of_week.friday) {
                days_of_weekOut += 'Fri ';
            }
            if (days_of_week.saturday) {
                days_of_weekOut += 'Sat ';
            }
            return days_of_weekOut;
        };

        ctrl.getCustomerTargets = function () {
            return (ctrl.targets = Targets.basicList(function (targets) {
                _.each(targets, function (target) {
                    ctrl.targetMap[target.id] = target;
                });
                ctrl.targetMap.initialized = true;
            }));
        };

        ctrl.removeRestrictions = function () {
            var selectedRestrictions = [];
            for (var i = 0; i < ctrl.restrictions.length; i++) {
                var restriction = ctrl.restrictions[i];
                if (restriction.selected) {
                    selectedRestrictions.push(restriction);
                }
            }

            modalService.openDelete(
                'Delete Restrictions?',
                'Are you sure that you want to delete these ' + selectedRestrictions.length + ' restrictions',
                selectedRestrictions,
                function () {
                    _.forEach(selectedRestrictions, function (restriction, index, array) {
                        Restrictions.delete({
                            customerId: ctrl.gdata.customer.id,
                            roeId: ctrl.roe.id,
                            restrictionId: restriction.id,
                        });
                        for (var j = 0; j < selectedRestrictions.length; j++) {
                            var selectedRestriction = selectedRestrictions[j];
                            for (var i = ctrl.restrictions.length - 1; i >= 0; i--) {
                                if (selectedRestriction.id === ctrl.restrictions[i].id) {
                                    ctrl.restrictions.splice(i, 1);
                                }
                            }
                        }
                        ctrl.allRoeRestrictionsSelected = false;
                        $rootScope.selectAll(ctrl.allRoeRestrictionsSelected, ctrl.restrictions);
                    });
                }
            );
        };

        ctrl.servicesNumsToNames = function () {
            var mapping = {};

            _.each(ctrl.gdata.services, function (service) {
                mapping[service.id] = service.name;
            });

            return mapping;
        };

        ctrl.processTargets = function (restriction) {
            var targets = [];
            if (restriction.targets.length === 0) {
                targets.push('Applies to All Targets');
            } else if (ctrl.targetMap.initialized) {
                for (var i = 0; i < restriction.targets.length; i++) {
                    targets.push(ctrl.targetMap[restriction.targets[i]].value);
                }
            }
            return targets;
        };

        ctrl.createTargetsPreview = function (targets) {
            var targetsPreview = '';

            for (var i = 0; i < targets.length && targetsPreview.length < 30; i++) {
                targetsPreview += targets[i] + (i < targets.length - 1 ? ', ' : '');
            }

            if (targetsPreview.length > 30) {
                targetsPreview = targetsPreview.slice(0, 30) + '...';
            }

            return targetsPreview;
        };

        ctrl.selectAllRoeRestrictions = function () {
            $rootScope.selectAll(ctrl.allRoeRestrictionsSelected, ctrl.restrictions);
        };

        ctrl.processRestriction = function (restriction) {
            restriction.targets = ctrl.processTargets(restriction);
            restriction.targetsPreview = ctrl.createTargetsPreview(restriction.targets);
            restriction.endTime = restriction.end_time;
            restriction.startTime = restriction.start_time;
            restriction.endDate = restriction.end_date;
            restriction.startDate = restriction.start_date;
            restriction.permitted = restriction.type === 'allow' ? true : false;
            restriction.services = restriction.services;
            restriction.servicesPreview = ctrl.serviceNumsToNames[restriction.services[0]] + (restriction.services.length > 1 ? '...' : '');
            return restriction;
        };

        ctrl.restrictionUpdated = function (restriction) {
            for (var i = ctrl.restrictions.length - 1; i >= 0; i--) {
                if (restriction.id === ctrl.restrictions[i].id) {
                    restriction = ctrl.processRestriction(restriction);
                    ctrl.restrictions.splice(i, 1, restriction);
                }
            }
        };

        ctrl.restrictionSaved = function (restriction) {
            restriction = ctrl.processRestriction(restriction);
            ctrl.restrictions.push(restriction);
        };

        ctrl.openModal = function (event) {
            var data = {
                roe: ctrl.roe,
                restriction: null,
                onUpdate: ctrl.restrictionUpdated,
                onInsert: ctrl.restrictionSaved,
                targetMap: ctrl.targetMap,
                targets: ctrl.targets,
                serviceNumsToNames: ctrl.serviceNumsToNames,
            };
            var bindings = {
                service: '<?',
                roe: '<',
                restriction: '<?',
                onUpdate: '<',
                onInsert: '<',
                targetMap: '<',
                targets: '<',
                serviceNumsToNames: '<',
            };

            var helper = function (bindings, data) {
                var modal = stateBuilder.componentModalGenerator(
                    'add-edit-restricton-modal',
                    bindings,
                    data,
                    'roe/pages/restrictions/addEditRestrictionModal/addEditRestrictionComp',
                    undefined,
                    'fixed-width-620'
                );
                ctrl.modalInstance = modal;
                $uibModal.open(modal);
            };

            if (event && event.currentTarget.value !== undefined) {
                var restrictionId = event.currentTarget.value;
                Restrictions.get({
                    customerId: ctrl.gdata.customer.id,
                    roeId: ctrl.roe.id,
                    restrictionId: restrictionId,
                }).$promise.then(function (existingRestriction) {
                    data.restriction = existingRestriction;
                    helper(bindings, data);
                });
            } else {
                helper(bindings, data);
            }
        };
    },
});
