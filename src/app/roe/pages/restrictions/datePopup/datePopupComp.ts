import app from '../../../../app';
import * as _ from 'lodash';
// import 'angular-bootstrap';
// import 'angular-animate';
// import 'angular-sanitize';

app.component('datePopup', {
    templateUrl: '/app/roe/pages/restrictions/datePopup/datePopup.html',
    bindings: {
        onUpdate: '&',
        dateName: '@',
        initVal: '<?',
    },

    controller: function () {
        var ctrl = this;

        ctrl.$onChanges = function () {
            ctrl.value = ctrl.initVal ? Date.parse(ctrl.initVal) : null;
        };

        ctrl.today = function () {
            ctrl.dt = new Date();
        };
        ctrl.today();

        ctrl.clear = function () {
            ctrl.dt = null;
        };

        ctrl.inlineOptions = {
            customClass: getDayClass,
            minDate: new Date(),
            showWeeks: true,
        };

        ctrl.dateOptions = {
            minDate: new Date(),
        };

        ctrl.toggleMin = function () {
            ctrl.inlineOptions.minDate = ctrl.inlineOptions.minDate ? null : new Date();
            ctrl.dateOptions.minDate = ctrl.inlineOptions.minDate;
        };

        ctrl.toggleMin();

        ctrl.open = function () {
            ctrl.popup.opened = true;
        };

        ctrl.setDate = function (year, month, day) {
            ctrl.dt = new Date(year, month, day);
        };

        ctrl.formats = ['yyyy-dd-MMM', 'dd-MMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'yyyy-mm-ddThh:mm:ssZ'];
        ctrl.format = ctrl.formats[0];
        ctrl.altInputFormats = ['M!/d!/yyyy'];

        ctrl.popup = {
            opened: false,
        };

        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var afterTomorrow = new Date();
        afterTomorrow.setDate(tomorrow.getDate() + 1);
        ctrl.events = [
            {
                date: tomorrow,
                status: 'full',
            },
            {
                date: afterTomorrow,
                status: 'partially',
            },
        ];

        function getDayClass(data) {
            var date = data.date,
                mode = data.mode;
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                for (var i = 0; i < ctrl.events.length; i++) {
                    var currentDay = new Date(ctrl.events[i].date).setHours(0, 0, 0, 0);

                    if (dayToCheck === currentDay) {
                        return ctrl.events[i].status;
                    }
                }
            }

            return '';
        }

        ctrl.update = function (prop, value) {
            this.onUpdate({ prop: prop, value: value });
        };
    },
});
