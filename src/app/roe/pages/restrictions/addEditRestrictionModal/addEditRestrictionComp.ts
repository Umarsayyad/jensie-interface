import app from '../../../../app';
import * as _ from 'lodash';
import * as angular from 'angular';

import '../datePopup/datePopupComp';
import '../timePopup/timePopupComp';

app.component('addEditRestrictonModal', {
    templateUrl: '/app/roe/pages/restrictions/addEditRestrictionModal/addEditRestriction.html',
    bindings: {
        modalInstance: '<',
        close: '<',
        dismiss: '<',
        roe: '<',
        restriction: '<?',
        onUpdate: '<',
        onInsert: '<',
        targetMap: '<',
        targets: '<',
    },

    controller: [
        'globalDataService',
        'Restrictions',
        'modalService',
        'CustomerSubscriptions',
        function (globalDataService, Restrictions, modalService, CustomerSubscriptions) {
            var ctrl = this;

            ctrl.$onInit = function () {
                // console.info("in handle $onInit");

                ctrl.gdata = globalDataService;
                ctrl.includedTargets = ctrl.restriction ? ctrl.restriction.targets : null;
                ctrl.formatStart = 'yyyy-MM-dd';
                ctrl.formatEnd = 'yyyy-MM-dd';
                ctrl.startDate = ctrl.restriction ? ctrl.restriction.start_date : new Date();
                ctrl.endDate = ctrl.restriction ? ctrl.restriction.end_date : null;
                ctrl.startTime = ctrl.restriction ? ctrl.restriction.start_time : null;
                ctrl.endTime = ctrl.restriction ? ctrl.restriction.end_time : null;

                ctrl.subscriptionEnds = null;

                ctrl.restrictByDateOrTime = ctrl.startTime ? true : false;

                ctrl.permitted = ctrl.restriction ? (ctrl.restriction.type === 'allow' ? true : false) : false;
                ctrl.is_global = ctrl.restriction ? ctrl.restriction.is_global : false;

                ctrl.subscribedServices = [];
                ctrl.selectedServices = ctrl.restriction ? ctrl.restriction.services : undefined;

                ctrl.days_of_week = ctrl.restriction
                    ? angular.copy(ctrl.restriction.days_of_week)
                    : {
                          sunday: true,
                          monday: true,
                          tuesday: true,
                          wednesday: true,
                          thursday: true,
                          friday: true,
                          saturday: true,
                      };

                ctrl.gdata.services.forEach(function (service) {
                    if (ctrl.gdata.customer.services.includes(service.id)) {
                        ctrl.subscribedServices.push({ ...service });
                    }
                });

                if (!ctrl.selectedServices) {
                    ctrl.selectedServices = _.map(ctrl.subscribedServices, 'id');
                } else {
                    ctrl.selectingServices = true;
                }

                var endDate;
                if (ctrl.endDate === null) {
                    endDate = new Date(ctrl.roe.period_of_performance_end_date);
                } else {
                    endDate = new Date(ctrl.endDate);
                }

                ctrl.endDateOrig = new Date(endDate.getUTCFullYear(), endDate.getUTCMonth(), endDate.getUTCDate());

                ctrl.endDate = _.clone(ctrl.endDateOrig);

                ctrl.allServicesSelected();
            };

            ctrl.handleClose = function () {
                // console.info("in handle close");
                ctrl.close();
            };
            ctrl.handleDismiss = function () {
                // console.info("in handle dismiss");
                ctrl.dismiss();
            };

            ctrl.allServicesSelected = function () {
                ctrl.selectingServices = ctrl.selectedServices.length !== ctrl.subscribedServices.length;
                return ctrl.selectingServices;
            };

            ctrl.resetDateTimeVals = function () {
                ctrl.startDate = ctrl.restriction ? ctrl.restriction.start_date : new Date();
                ctrl.endDate = ctrl.endDateOrig;
                ctrl.startTime = ctrl.restriction ? ctrl.restriction.start_time : null;
                ctrl.endTime = ctrl.restriction ? ctrl.restriction.end_time : null;
            };

            ctrl.updateProp = function (prop, value) {
                ctrl[prop] = value;
            };

            ctrl.formatDate = function (date) {
                if (typeof date === 'string') {
                    if (date.lastIndexOf('Z') !== date.length - 1) {
                        return date;
                    } else {
                        date = new Date(date);
                    }
                }

                var day = date.getDate();
                day = day < 10 ? '0' + day : day;
                var month = date.getMonth() + 1;
                month = month < 10 ? '0' + month : month;
                return date.getFullYear() + '-' + month + '-' + day;
            };

            ctrl.formatTime = function (time) {
                if (typeof time === 'string') {
                    return time;
                }

                var hours = time.getHours();
                hours = hours < 10 ? '0' + hours : hours;
                var minutes = time.getMinutes();
                minutes = minutes < 10 ? '0' + minutes : minutes;
                var seconds = time.getSeconds();
                seconds = seconds < 10 ? '0' + seconds : seconds;
                return hours + ':' + minutes + ':' + seconds;
            };

            ctrl.addOrUpdateRestriction = function () {
                if (!ctrl.selectingServices) {
                    ctrl.selectedServices = ctrl.subscribedServices.map((s) => s.id);
                }

                var restriction = {
                    id: ctrl.restriction ? ctrl.restriction.id : undefined,
                    targets: ctrl.includedTargets ? ctrl.includedTargets : [],
                    services: ctrl.selectedServices ? ctrl.selectedServices : [],
                    is_global: ctrl.is_global,
                    type: ctrl.permitted ? 'allow' : 'deny',
                    end_date: !ctrl.restrictByDateOrTime && ctrl.endDate ? ctrl.formatDate(ctrl.endDate) : null,
                    start_date: !ctrl.restrictByDateOrTime && ctrl.startDate ? ctrl.formatDate(ctrl.startDate) : null,
                    end_time: ctrl.restrictByDateOrTime && ctrl.endTime ? ctrl.formatTime(ctrl.endTime) : null,
                    start_time: ctrl.restrictByDateOrTime && ctrl.startTime ? ctrl.formatTime(ctrl.startTime) : null,
                    days_of_week: ctrl.days_of_week,
                };

                if (!ctrl.restriction) {
                    Restrictions.create(
                        {
                            customerId: ctrl.gdata.customer.id,
                            roeId: ctrl.roe.id,
                        },
                        restriction,
                        function (response) {
                            ctrl.insert(response);
                            ctrl.handleClose();
                        },
                        function (response) {
                            modalService.openErrorResponse('Unable to create new restriction: ', response);
                        }
                    );
                } else {
                    Restrictions.update(
                        {
                            customerId: ctrl.gdata.customer.id,
                            roeId: ctrl.roe.id,
                        },
                        restriction,
                        function (response) {
                            ctrl.update(response);
                            ctrl.handleClose();
                        },
                        function (response) {
                            modalService.openErrorResponse('Unable to update restriction: ', response);
                        }
                    );
                }
            };

            ctrl.update = function (restriction) {
                ctrl.onUpdate(restriction);
            };

            ctrl.insert = function (restriction) {
                ctrl.onInsert(restriction);
            };
        },
    ],
});
