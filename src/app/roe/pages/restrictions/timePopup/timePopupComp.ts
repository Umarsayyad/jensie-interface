import app from '../../../../app';
import * as _ from 'lodash';

app.component('timePopup', {
    templateUrl: '/app/roe/pages/restrictions/timePopup/timePopup.html',
    bindings: {
        onUpdate: '&',
        timeName: '@',
        initVal: '<?',
    },

    controller: function () {
        var ctrl = this;

        ctrl.read_only = false;
        ctrl.required = false;
        ctrl.value = 0;

        //Placed here for future purposes, not in use right now
        ctrl.validation = {
            max: undefined,
            min: undefined,
        };

        ctrl.$onInit = function () {
            ctrl.value = ctrl.initVal ? Date.parse('1970-01-01T' + ctrl.initVal + 'Z') : null;
        };

        ctrl.update = function (prop, value) {
            this.onUpdate({ prop: prop, value: value });
        };
    },
});
