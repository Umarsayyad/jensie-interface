import app from '../../../app';
import * as _ from 'lodash';
import * as angular from 'angular';

import '../../buttons/backAndNextComp';
import '../../utils/roeUtilsCtrl';
import catotheme from '@cato/config/themes/catotheme';

app.component('pocs', {
    templateUrl: '/app/roe/pages/pocs/pocs.html',
    bindings: {
        roe: '<',
        changePage: '<',
    },

    controller: function pocsCtrl(
        roeUtilsCtrl,
        $window,
        $rootScope,
        globalDataService,
        WizardHandler,
        modalService,
        PointsOfContact,
        PointOfContactAddresses,
        RulesOfEngagements
    ) {
        //console.log("in pocs controller");
        var ctrl = this;
        ctrl.catotheme = catotheme;
        ctrl.gdata = globalDataService;
        ctrl.customer = ctrl.gdata.customer;
        var ROE_POC_TYPES = ['technical', 'legal', 'executive']; // TODO: Define this as a constant somewhere
        var POINT_OF_CONTACT_STEP_TITLE = 'Point of Contact';

        var appWindow = angular.element($window);
        var setDimensions = function () {
            roeUtilsCtrl.calculateRoeHeight($window, '#PoCInfo', 322, '#PoCDescription');
        };

        ctrl.$onInit = function () {
            ctrl.pocs = PointsOfContact.query({ customerId: ctrl.customer.id, detail: true }, function (pocs) {
                ctrl.poc = _.find(pocs, { id: ctrl.roe.primary_point_of_contact });
                if (!ctrl.poc) {
                    ctrl.poc = _.find(pocs, function (poc) {
                        return poc.user === $rootScope.user.id && _.includes(ROE_POC_TYPES, poc.type);
                    });
                }
                ctrl.loadPoC();
            });
        };

        ctrl.$onChanges = function () {
            if (WizardHandler.wizard().currentStepTitle() === POINT_OF_CONTACT_STEP_TITLE) {
                setDimensions();
            }
        };

        ctrl.$onDestroy = function () {
            appWindow.unbind('resize', setDimensions);
        };

        appWindow.bind('resize', setDimensions);

        ctrl.loadPoC = function () {
            if (!ctrl.poc) return;

            ctrl.poc.phonenumber = {
                poc: ctrl.poc.id,
                type: 'phonenumber',
            };
            ctrl.poc.email = {
                poc: ctrl.poc.id,
                type: 'email',
                value: globalDataService.userMap[ctrl.poc.user].email,
            };

            _.each(ctrl.poc.pointofcontactaddresses, function (result) {
                if (result.type === 'phonenumber') {
                    ctrl.poc.phonenumber = result;
                    ctrl.poc.phonenumber.valid = true;
                } else if (result.type === 'email') {
                    ctrl.poc.email = result;
                    ctrl.poc.email.valid = true;
                }
            });

            if (_.isUndefined(ctrl.poc.email.id)) {
                ctrl.submitEmail(); // Save the prefilled email on load so it is persisted if they don't interact with the field
            }
        };

        ctrl.formValid = function () {
            return !!(ctrl.poc && ctrl.poc.phonenumber.value && ctrl.poc.phonenumber.valid && ctrl.poc.email.value && ctrl.poc.email.valid);
        };

        ctrl.submitPhone = function () {
            ctrl.addOrUpdateAddress(ctrl.poc.phonenumber);
        };

        ctrl.submitEmail = function () {
            ctrl.addOrUpdateAddress(ctrl.poc.email);
        };

        //Leaving this function's name alone to help keep it associated with
        //the original.
        ctrl.addOrUpdateAddress = function (location) {
            //A little simple validation logic goes a long way
            if (location.type !== 'phonenumber' && location.type !== 'email') {
                return;
            } else if (!ctrl.pocs) {
                return;
            } else if (!location.value) {
                return;
            }

            if (!location.id) {
                PointOfContactAddresses.create(
                    { customerId: ctrl.customer.id },
                    location,
                    function (addr) {
                        $rootScope.updateObj(ctrl.poc[location.type], addr);
                        ctrl.poc[location.type].valid = true;
                        ctrl.poc.pointofcontactaddresses.unshift(ctrl.poc[location.type]);
                    },
                    function (result) {
                        ctrl.poc[location.type].valid = false;
                        modalService.openErrorResponse('Point of contact ' + location.type + ' could not be added.', result, false);
                    }
                );
            } else {
                PointOfContactAddresses.update(
                    { customerId: ctrl.customer.id },
                    location,
                    function (addr) {
                        ctrl.poc[location.type].valid = true;
                    },
                    function (result) {
                        ctrl.poc[location.type].valid = false;
                        modalService.openErrorResponse('Point of contact ' + location.type + ' could not be updated.', result, false);
                    }
                );
            }
        };

        ctrl.submitPrimary = function () {
            // TODO: show a spinner here
            RulesOfEngagements.patch(
                { customerId: ctrl.customer.id, roeId: ctrl.roe.id },
                { primary_point_of_contact: ctrl.poc.id },
                _.noop,
                function (err) {
                    WizardHandler.wizard().goTo(POINT_OF_CONTACT_STEP_TITLE); // TODO: prevent navigating until request finishes
                    modalService.openErrorResponse('Could not set primary point of contact', err, false);
                }
            );
        };
    },
});
