import app from '../../../app';
import * as angular from 'angular';

import '../../buttons/backAndNextComp';
import '../../utils/roeUtilsCtrl';
import { config } from '@cato/config/config';
import { ValidTheme } from '@cato/config/themes/valid-themes';

app.component('services', {
    templateUrl: '/app/roe/pages/services/services.html',
    bindings: {
        roe: '<',
        changePage: '<',
    },

    controller: function servicesCtrl($rootScope, $window, globalDataService, roeUtilsCtrl, WizardHandler) {
        //console.log("in services controller");
        var ctrl = this;
        ctrl.gdata = globalDataService;
        ctrl.customer = ctrl.gdata.customer;
        ctrl.services = [];
        ctrl.displayedService = undefined;
        angular.forEach(ctrl.gdata.services, function (service) {
            ctrl.services.push({ ...service, subscribed: ctrl.customer.services.includes(service.id) });
        });
        if ($rootScope.catotheme.name === ValidTheme.jensie) {
            // TODO: add only if customer has CMMC feature
            ctrl.services.unshift({
                id: null,
                name: 'Cybersecurity Maturity Model Certification Assessment',
                service_description: `We will help you navigate your CMMC compliance journey, guiding your company's review of 
                the framework practices that must be fulfilled to reach your desired compliance certification level 
                and providing direction to meet those goals where you fall short.`,
                subscribed: true,
            });
        }

        var appWindow = angular.element($window);

        var setDimensions = function () {
            roeUtilsCtrl.calculateRoeHeight(
                $window,
                '#roeServicesTableContainer',
                337,
                '#staticHeightContentServices1',
                '#staticHeightContentServices2'
            );
            roeUtilsCtrl.calculateRoeHeight(
                $window,
                '#roeServicesDetailsContainer',
                337,
                '#staticHeightContentServices1',
                '#staticHeightContentServices2'
            );
        };

        ctrl.$onInit = function () {
            setDimensions();
        };

        ctrl.$onChanges = function () {
            if (WizardHandler.wizard().currentStepTitle() === 'Services') {
                setDimensions();
            }
        };

        ctrl.$onDestroy = function () {
            appWindow.unbind('resize', setDimensions);
        };

        appWindow.bind('resize', setDimensions);

        ctrl.showDescription = function (service) {
            if (ctrl.displayedService === service) {
                ctrl.displayedService = undefined;
            } else {
                ctrl.displayedService = service;
                angular.element('#roeServicesDetailsContainerDescriptions').animate({ scrollTop: 0 }, 'fast');
            }
        };
    },
});
