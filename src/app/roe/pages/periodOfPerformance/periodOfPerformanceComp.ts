import app from '../../../app';
import * as _ from 'lodash';
import * as angular from 'angular';

import '../../buttons/backAndNextComp';
import '../../utils/roeUtilsCtrl';
import catotheme from '@cato/config/themes/catotheme';

app.component('periodOfPerformance', {
    templateUrl: '/app/roe/pages/periodOfPerformance/periodOfPerformance.html',
    bindings: {
        roe: '<',
        changePage: '<',
    },

    controller: [
        'globalDataService',
        'RulesOfEngagements',
        'roeUtilsCtrl',
        '$window',
        'WizardHandler',
        function periodOfPerformanceCtrl(globalDataService, RulesOfEngagements, roeUtilsCtrl, $window, WizardHandler) {
            //console.log("in periodOfPerformance controller");
            var ctrl = this;
            ctrl.catotheme = catotheme;
            ctrl.gdata = globalDataService;
            ctrl.customer = ctrl.gdata.customer;
            ctrl.period_of_performance_acknowledged = false;
            ctrl.roeStartDate = null;
            ctrl.roeEndDate = null;
            ctrl.startWording = null;

            var appWindow = angular.element($window);
            var setDimensions = function () {
                roeUtilsCtrl.calculateRoeHeight($window, '#periodOfPerformance');
            };

            ctrl.submit = function () {};

            ctrl.$onInit = function () {
                var numsToNames = ctrl.servicesNumsToNames();

                var roeQuery = RulesOfEngagements.query({ customerId: ctrl.customer.id });
                roeQuery.$promise.then(function (roes) {
                    var currentRoe = roes[roes.length - 1];

                    var currentDate = new Date();
                    var roeStartDate = new Date(currentRoe.period_of_performance_start_date);
                    var roeEndDate = new Date(currentRoe.period_of_performance_end_date);

                    ctrl.startWording = roeStartDate < currentDate ? 'started' : 'will start';
                    ctrl.endWording = roeEndDate < currentDate ? 'ended' : 'will end';

                    ctrl.roeStartDate = new Date(roeStartDate.getUTCFullYear(), roeStartDate.getUTCMonth(), roeStartDate.getUTCDate()).toDateString();
                    ctrl.roeEndDate = new Date(roeEndDate.getUTCFullYear(), roeEndDate.getUTCMonth(), roeEndDate.getUTCDate()).toDateString();
                });
            };

            ctrl.$onDestroy = function () {
                appWindow.unbind('resize', setDimensions);
            };

            ctrl.$onChanges = function () {
                if (WizardHandler.wizard().currentStepTitle() === 'Period Of Performance') {
                    ctrl.period_of_performance_acknowledged = ctrl.roe && ctrl.roe.period_of_performance_acknowledged_by !== null;
                    setDimensions();
                }
            };

            appWindow.bind('resize', setDimensions);

            ctrl.servicesNumsToNames = function () {
                var mapping = {};

                _.each(ctrl.gdata.services, function (service) {
                    mapping[service.id] = service.name;
                });

                return mapping;
            };

            ctrl.agree = function () {
                if (ctrl.roe && ctrl.roe.period_of_performance_acknowledged_by === null) {
                    ctrl.roe.$acknowledgePeriodOfPerformance();
                    ctrl.period_of_performance_acknowledged = true;
                }
            };
        },
    ],
});
