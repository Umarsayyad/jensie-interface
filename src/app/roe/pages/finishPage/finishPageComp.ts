import app from '../../../app';
import * as _ from 'lodash';
import '../../buttons/finishComp';
import catotheme from '@cato/config/themes/catotheme';

app.component('finishpage', {
    templateUrl: '/app/roe/pages/finishPage/finishPage.html',
    bindings: {
        roe: '<',
        changePage: '<',
    },
    controller: function finishPageCtrl(FileSaver, globalDataService, RulesOfEngagements) {
        //console.log("in finishPage controller");
        var ctrl = this;
        ctrl.catotheme = catotheme;
        ctrl.gdata = globalDataService;
        ctrl.customer = ctrl.gdata.customer;

        ctrl.downloadRoE = function () {
            var response = RulesOfEngagements.downloadSigned({
                customerId: ctrl.gdata.customer.id,
                roeId: ctrl.roe.id,
            }).$promise;
            response.then(function (data) {
                FileSaver.saveAs(data.blob, 'Rules of Engagement');
            });
        };

        ctrl.submit = function () {};
    },
});
