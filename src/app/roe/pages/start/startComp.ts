import app from '../../../app';
import * as _ from 'lodash';
import * as angular from 'angular';

import '../../buttons/nextComp';
import '../../utils/roeUtilsCtrl';

app.component('start', {
    templateUrl: '/app/roe/pages/start/start.html',
    bindings: {
        changePage: '<',
    },
    controller: function startCtrl($rootScope, globalDataService, $window, roeUtilsCtrl, WizardHandler) {
        //console.log("in Start controller");
        var ctrl = this;
        ctrl.gdata = globalDataService;
        ctrl.customer = ctrl.gdata.customer;
        ctrl.nextEnabled = false;

        var appWindow = angular.element($window);
        var setDimensions = function () {
            roeUtilsCtrl.calculateRoeHeight($window, '#startContent');
        };

        ctrl.$onInit = function () {
            for (var i = 0; i < $rootScope.user.pocs.length; i++) {
                var poc = $rootScope.user.pocs[i];
                if (poc === 'executive' || poc === 'legal' || poc === 'technical') {
                    ctrl.nextEnabled = true;
                    break;
                }
            }
        };

        ctrl.$onDestroy = function () {
            appWindow.unbind('resize', setDimensions);
        };

        ctrl.$onChanges = function () {
            if (WizardHandler.wizard().currentStepTitle() === 'Start') {
                setDimensions();
            }
        };

        appWindow.bind('resize', setDimensions);
    },
});
