import app from '../../../app';
import * as _ from 'lodash';
import '../../buttons/backAndNextComp';

app.component('approval', {
    templateUrl: '/app/roe/pages/approval/approval.html',
    bindings: {
        roe: '<',
        prevPage: '&',
        changePage: '<',
        cgBusy: '<',
    },
    controller: function startCtrl(FileSaver, globalDataService, RulesOfEngagements, $q) {
        //console.log("in Approval controller");
        var ctrl = this;
        ctrl.gdata = globalDataService;
        ctrl.customer = ctrl.gdata.customer;
        ctrl.nextEnabled = false;
        ctrl.signature = null;

        ctrl.$onInit = function () {
            var counter = 0;
            var interval = setInterval(function () {
                if (ctrl.clear) {
                    clearInterval(interval);
                    ctrl.clear();
                } else if (counter < 200) {
                    counter++;
                } else {
                    clearInterval(interval);
                }
            }, 10);
        };

        ctrl.onGenerate = function () {
            ctrl.cgBusy.roeBusy = $q.defer();
        };

        ctrl.downloadRoE = function () {
            var response = RulesOfEngagements.downloadUnsigned({
                customerId: ctrl.gdata.customer.id,
                roeId: ctrl.roe.id,
            }).$promise;
            response.then(function (data) {
                FileSaver.saveAs(data.blob, 'Rules of Engagement');
            });
        };

        ctrl.updateSignature = function () {
            ctrl.signature = ctrl.accept();
        };

        ctrl.clearPad = function () {
            ctrl.clear();
            ctrl.signature = null;
        };

        ctrl.sign = function () {
            if (!ctrl.signature || ctrl.signature.isEmpty) {
                return;
            }
            RulesOfEngagements.sign(
                {
                    customerId: ctrl.gdata.customer.id,
                    roeId: ctrl.roe.id,
                },
                { dataurl: ctrl.signature.dataUrl }
            ).$promise;
            // ctrl.cgBusy.roeBusy = $q.defer();
        };

        ctrl.unlockRoe = function () {
            ctrl.prevPage();
            ctrl.roe.$unlock();
        };
    },
});
