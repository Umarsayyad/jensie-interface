import app from '../../../app';
import * as _ from 'lodash';
import * as angular from 'angular';

import '../../buttons/backAndNextComp';
import '../../utils/roeUtilsCtrl';
import catotheme from '@cato/config/themes/catotheme';

app.component('eula', {
    templateUrl: '/app/roe/pages/eula/eulaAcceptance.html',
    bindings: {
        roe: '<',
        changePage: '<',
    },

    controller: function eulaAcceptanceCtrl(roeUtilsCtrl, $window, WizardHandler) {
        // console.log("in eula controller");
        var ctrl = this;
        ctrl.catotheme = catotheme;

        var appWindow = angular.element($window);
        var setDimensions = function () {
            // roeUtilsCtrl.calculateRoeHeight($window, "#eulaAcceptanceWrapper");
            roeUtilsCtrl.calculateRoeHeight($window, '#eulaAcceptance');
        };

        ctrl.$onChanges = function () {
            if (WizardHandler.wizard().currentStepTitle() === 'EULA') {
                setDimensions();
            }
        };

        ctrl.$onDestroy = function () {
            appWindow.unbind('resize', setDimensions);
        };

        appWindow.bind('resize', setDimensions);
    },
});
