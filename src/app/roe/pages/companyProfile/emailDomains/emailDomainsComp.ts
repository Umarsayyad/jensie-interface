import app from '../../../../app';
import * as _ from 'lodash';

app.component('emailDomains', {
    templateUrl: '/app/roe/pages/companyProfile/emailDomains/emailDomains.html',
    bindings: {
        customer: '<',
    },

    controller: [
        'globalDataService',
        'EmailDomains',
        'modalService',
        function (globalDataService, EmailDomains, modalService) {
            var ctrl = this;

            ctrl.gdata = globalDataService;
            ctrl.config = {};
            ctrl.data = {};

            ctrl.$onInit = function () {
                EmailDomains.options().$promise.then(function (results) {
                    ctrl.config.emaildomain = results.actions.POST;
                });

                EmailDomains.query({ customerId: ctrl.customer.id }).$promise.then(function (domains) {
                    ctrl.data.emaildomains = domains;
                });
            };

            ctrl.addEmailDomain = function () {
                if (ctrl.data.emaildomains.length > 0) {
                    if (ctrl.data.emaildomains[0].id === undefined) {
                        return;
                    }
                }

                ctrl.data.emaildomains.unshift({});
            };

            ctrl.cancelDomain = function () {
                ctrl.data.emaildomains.shift();
            };

            ctrl.validateMinMax = function (data, min, max) {
                if (data.length < min) {
                    return 'Value is too short, minimum length: ' + min;
                }
                if (data.length > max) {
                    return 'Value is too long, maximum length: ' + max;
                }
            };

            ctrl.validatePattern = function (data, pattern) {
                if (!pattern.test(data)) {
                    return 'Invalid D-U-N-S, please enter as XX-XXX-XXXX (12-345-6789)';
                }
            };

            /* this code is basically generic... just picks a different property and different resource. */
            ctrl.saveOrUpdateDomain = function (domain) {
                var customer_id = ctrl.customer.id;

                if (domain.id === undefined) {
                    EmailDomains.create(
                        { customerId: customer_id },
                        domain,
                        function (result) {
                            ctrl.data.emaildomains[0] = result;
                        },
                        function (res) {
                            modalService.openErrorResponse('Unable to create new domain: ', res);
                        }
                    );
                } else {
                    EmailDomains.update(
                        { customerId: customer_id, domainId: domain.id },
                        domain,
                        function (result) {
                            for (var i = 0; i < ctrl.data.emaildomains.length; i++) {
                                if (ctrl.data.emaildomains[i].id === domain.id) {
                                    ctrl.data.emaildomains[i] = result;
                                    break;
                                }
                            }
                        },
                        function (res) {
                            modalService.openErrorResponse('Unable to update domain: ', res);
                        }
                    );
                }
            };
        },
    ],
});
