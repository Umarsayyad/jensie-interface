import app from '../../../app';
import * as _ from 'lodash';
import * as angular from 'angular';
import catotheme from '@cato/config/themes/catotheme';

import '../../../service/SvcUtilsService';
import '../../../settings/ProfileCtrl';
import '../../utils/roeUtilsCtrl';
import '../../buttons/backAndNextComp';
import '../../pages/companyProfile/emailDomains/emailDomainsComp';

import { NaicsIndustryService } from '../../../../ng-app/company-profile';
import { ValidTheme } from '@cato/config/themes/valid-themes';

app.component('companyProfile', {
    templateUrl: '/app/roe/pages/companyProfile/companyProfile.html',
    bindings: {
        roe: '<',
        changePage: '<',
    },

    controller: [
        'globalDataService',
        '$uibModal',
        'stateBuilder',
        'NaicsIndustryService',
        'modalService',
        'roeUtilsCtrl',
        '$window',
        'Locations',
        'Customers',
        'WizardHandler',
        function companyProfileCtrl(
            globalDataService,
            $uibModal,
            stateBuilder,
            NaicsIndustryService: NaicsIndustryService,
            modalService,
            roeUtilsCtrl,
            $window,
            Locations,
            Customers,
            WizardHandler
        ) {
            var ctrl = this;
            ctrl.catotheme = catotheme;
            ctrl.gdata = globalDataService;
            ctrl.customer = angular.copy(ctrl.gdata.customer);
            ctrl.customer.business_category = [];
            ctrl.customer = ctrl.gdata.setCustomerBusinessValues(ctrl.customer);
            ctrl.mentorPlaceholder = null;
            ctrl.nextEnabled = false;

            ctrl.locations = [];

            ctrl.setLocation = function (location) {
                ctrl.locations.push(location);
                ctrl.setNextEnabled();
            };

            ctrl.deleteLocation = function (location) {
                for (var i = 0; i < ctrl.locations.length; i++) {
                    if (ctrl.locations[i].id === location.id) {
                        ctrl.locations.splice(i, 1);
                        break;
                    }
                }
                ctrl.locationsRequest = Locations.delete({ customerId: ctrl.customer.id, locationId: location.id });
                ctrl.setNextEnabled();
            };

            ctrl.sizes = ['1-99', '100-249', '250-499', '500-999', '1000-1499', '1500-4999', '5000-9999', '10000+'];

            ctrl.mppDate = { opened: false };

            ctrl.dateOptions = {
                formatYear: 'yy',
                maxDate: new Date(),
                startingDay: 1,
            };

            ctrl.openMppDate = function () {
                ctrl.mppDate.opened = true;
            };

            var setDimensions = function () {
                roeUtilsCtrl.calculateRoeHeight($window, '#companyProfile', 339, '#companyProfileHeader1', '#companyProfileHeader2');
            };

            function openIndustrySelector(): Promise<string> {
                let modal = stateBuilder.ngComponentModalGenerator('industry-selector');
                return $uibModal.open(modal).result;
            }

            function selectIndustry() {
                openIndustrySelector().then((naicsChoice: string) => {
                    ctrl.customer.naics = naicsChoice;
                    ctrl.naicsName = NaicsIndustryService.getNaicsName(naicsChoice);
                });
                ctrl.setNextEnabled();
            }
            ctrl.selectIndustry = selectIndustry;

            ctrl.$onInit = function () {
                ctrl.availableMentors = Customers.availableMentors({}, function (results) {
                    for (let company of results) {
                        if (company.id === ctrl.customer.mentor_company) {
                            ctrl.mentorPlaceholder = company.name;
                            break;
                        }
                    }
                });

                ctrl.enforce2FA = catotheme.name == ValidTheme.jensie;

                if (ctrl.customer.mpp_start) {
                    ctrl.refreshMppDate();
                }

                ctrl.locationsRequest = Locations.list({ customerId: ctrl.customer.id });
                ctrl.locationsRequest.$promise.then(function (locations) {
                    ctrl.locations = locations;
                    ctrl.setNextEnabled();
                });

                ctrl.naicsName = NaicsIndustryService.getNaicsName(ctrl.customer.naics);
            };

            ctrl.refreshMppDate = function () {
                // Workaround known bug: https://github.com/angular-ui/bootstrap/issues/2628
                let d = new Date(ctrl.customer.mpp_start);
                d.setMinutes(d.getMinutes() + d.getTimezoneOffset());
                ctrl.customer.mpp_start = d;
            };

            ctrl.$onChanges = function () {
                if (WizardHandler.wizard().currentStepTitle() === 'Company Profile') {
                    setDimensions();
                }
            };
            ctrl.setNextEnabled = function () {
                if (ctrl.customer.has_current_participation_mentor_program && !ctrl.mentorPlaceholder) {
                    ctrl.nextEnabled = false;
                } else {
                    ctrl.nextEnabled = ctrl.customer.size && ctrl.customer.naics && ctrl.locations.length > 0;
                }
            };

            ctrl.setHasParticipated = function () {
                if (!ctrl.customer.has_participated_mentor_program) {
                    ctrl.customer.has_current_participation_mentor_program = false;
                    ctrl.mentor_company = null;
                    ctrl.customer.mpp_start = null;
                    ctrl.mentorPlaceholder = '';
                }
                ctrl.setNextEnabled();
            };

            ctrl.isParticipating = function () {
                ctrl.mentorPlaceholder = '';
                ctrl.mentor_company = null;
                ctrl.customer.mpp_start = null;
                ctrl.setNextEnabled();
            };

            ctrl.setMentor = function (item) {
                ctrl.customer.mentor_company = item.name;
            };

            ctrl.submit = function () {
                ctrl.customer = ctrl.gdata.setCustomerBusinessBooleans(ctrl.customer);

                if (ctrl.customer.mpp_start instanceof Date) {
                    ctrl.customer.mpp_start =
                        ctrl.customer.mpp_start.getFullYear() +
                        '-' +
                        (ctrl.customer.mpp_start.getMonth() + 1) +
                        '-' +
                        ctrl.customer.mpp_start.getDate();
                }

                if (ctrl.customer.has_current_participation_mentor_program && typeof (ctrl.customer.mentor_company === 'undefined')) {
                    let mentorCompany = (<HTMLInputElement>document.getElementById('mentor')).value;
                    let mentorRequest = Customers.createMentor({ name: mentorCompany });
                    mentorRequest.$promise.then(function (response) {
                        ctrl.customer.mentor_company = response.id;
                        ctrl.postCustomer();
                    });
                } else {
                    ctrl.postCustomer();
                }
            };

            ctrl.update2FA = function () {
                Customers.set2FA(
                    { customerId: ctrl.customer.id },
                    { enforce_2fa: ctrl.customer.enforce_2fa },
                    _.noop,
                    modalService.openErrorResponseCB('Unable to set 2FA enforcement')
                );
            };

            ctrl.postCustomer = function () {
                let customerResponseRequest = Customers.update({ customerId: ctrl.customer.id }, ctrl.customer);
                customerResponseRequest.$promise.then(function (customerResponse) {
                    if (!customerResponse.sam_verified && (customerResponse.duns || customerResponse.cagecode)) {
                        Customers.validateSam({ customerId: customerResponse.id }, {});
                    }
                    ctrl.gdata.updateCustomer(customerResponse);
                    ctrl.refreshMppDate();
                });
            };
        },
    ],
});
