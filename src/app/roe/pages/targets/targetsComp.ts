import app from '../../../app';
import * as _ from 'lodash';
import * as angular from 'angular';

import '../../../customer/manage/CondensedCustomerFilterComp';
import '../../../target/AddTargetCtrl';
import '../../../target/TargetService';
import '../../buttons/backAndNextComp';
import '../../utils/roeUtilsCtrl';
import catotheme from '@cato/config/themes/catotheme';

app.component('targets', {
    templateUrl: '/app/roe/pages/targets/targets.html',
    bindings: {
        roe: '<',
        changePage: '<',
    },

    controller: function targetsCtrl(
        $rootScope,
        $scope,
        $state,
        NgTableParams,
        globalDataService,
        ngTableService,
        socketService,
        Targets,
        $window,
        $uibModal,
        roeUtilsCtrl,
        modalService,
        WizardHandler
    ) {
        var ctrl: any = {
            catotheme: catotheme,
            gdata: globalDataService,
            types: [],
            targetTable: new NgTableParams({}, {}),
            appWindow: angular.element($window),
            selectedCustomer: undefined,
            bulkSubmission: undefined,
            config: undefined,
            targetUnsubscribe: undefined,

            data: {
                activeCategory: 'Target',
                allTargetsSelected: false,
                selectedTargets: [],
            },

            defaultParams: {
                owner_op: true,
            },

            tagFilterData: {},

            $onInit: function () {
                if (ctrl.gdata.customer) {
                    ctrl.gdata.customer.$promise.then(function () {
                        ctrl.loadTargets(ctrl.gdata.customer, $state.params.targetType);
                    });
                }

                Targets.options(function (result) {
                    ctrl.config = result.actions.POST;
                    angular.forEach(_.sortBy(ctrl.config.type.choices, 'display_name'), function (typeMap) {
                        ctrl.types.push({ id: typeMap.value, title: typeMap.display_name });
                    });
                });

                ctrl.targetUnsubscribe = socketService.subscribeByModel('target', function (newTarget) {
                    // TODO: address duplicated object changes when editing targets in the add/edit target modal
                    var curTarget: any = _.find(ctrl.targetTable.data, { id: newTarget.id });
                    if (curTarget) {
                        $rootScope.updateObj(curTarget, newTarget);
                    } else {
                        ctrl.targetTable.data.unshift(newTarget);
                        ctrl.targetTable.total(ctrl.targetTable.total() + 1);
                        return;
                    }
                });

                ctrl.appWindow.bind('resize', ctrl.setDimensions);
            },
            $onChanges: function () {
                if (WizardHandler.wizard().currentStepTitle() === 'Targets') {
                    ctrl.setDimensions();
                }
            },
            $onDestroy: function () {
                if (ctrl.targetUnsubscribe) {
                    ctrl.targetUnsubscribe();
                }
                ctrl.appWindow.unbind('resize', ctrl.setDimensions);
            },
            setDimensions: function () {
                roeUtilsCtrl.calculateRoeHeight($window, '#targetsTableWrapper');
                roeUtilsCtrl.calculateRoeHeight($window, '#targetsTable', 500, '#staticHeightContentTargets');
            },
            selectAllTargets: function () {
                $rootScope.selectAll(ctrl.data.allTargetsSelected, _.filter(ctrl.targetTable.data, { locked: false }));
            },
            removeTargets: function () {
                modalService.openDelete(
                    'Delete Targets?',
                    'Are you sure that you want to delete these ' + ctrl.data.selectedTargets.length + ' targets',
                    ctrl.data.selectedTargets,
                    function () {
                        _.forEach(ctrl.data.selectedTargets, function (target, index, array) {
                            Targets.delete({ targetId: target.id });
                            ctrl.data.selectedTargets = [];
                        });
                    }
                );
            },

            deleteTargets: function () {
                var customer = ctrl.selectedCustomer;
                var selected = _.map(ctrl.data.selectedTargets, 'id');

                modalService.openDelete('Confirmation', selected.length + " targets for '" + customer.name + "'", {}, function () {
                    ctrl.bulkSubmission = Targets.bulkDestroy(
                        {},
                        { targets: selected },
                        function () {
                            _.each(selected, function (target_id) {
                                _.remove(ctrl.targetTable.data, { id: target_id });
                                ctrl.removeFromMetatarget(target_id);
                            });
                        },
                        modalService.openErrorResponseCB('Unable to delete selected targets')
                    );
                });
            },
            deleteSpecificTarget: function (target) {
                modalService.openDelete('Delete Target', "target '" + target.value + "'", null, function () {
                    Targets.deleteSpecificTarget(
                        { targetId: target.id },
                        function () {
                            _.remove(ctrl.targetTable.data, { id: target.id });
                            ctrl.removeFromMetatarget(target.id);
                        },
                        modalService.openErrorResponseCB('Deleting Target failed')
                    );
                });
            },
            removeFromMetatarget: function (targetId) {
                _.eachRight($scope.metatargetTable.data, function (metatarget) {
                    if (metatarget.targets.length > 0) {
                        if (_.isNumber(metatarget.targets[0])) {
                            _.pull(metatarget.targets, targetId);
                        } else {
                            _.remove(metatarget.targets, { id: targetId });
                        }
                        if (metatarget.targets.length === 0) {
                            // We skipped over metatargets with no targets above so having no targets now means the only contained target was the one that was just deleted
                            _.remove($scope.metatargetTable.data, { id: metatarget.id });
                        }
                    }
                });
            },
            loadTargets: function (customer, targetType) {
                ctrl.selectedCustomer = customer;
                if (!customer) {
                    ctrl.selectedCustomer = null;
                    ctrl.targetTable = new NgTableParams({}, {});
                    return;
                }

                ctrl.tagFilterData.customer = customer.id;

                var extraInitialParams: any = {};

                if (targetType) {
                    extraInitialParams.filter = { type: targetType };
                }

                ctrl.defaultParams.customer = customer.id;

                ctrl.targetTable = ngTableService.serverFilteredTable({
                    resource: Targets.list,
                    defaultParams: ctrl.defaultParams,
                    extraInitialParams: extraInitialParams,
                    updateParamsCallback: _.noop,
                });
            },
            targetSaved: function (target) {
                if (ctrl.selectedCustomer && target.customer === ctrl.selectedCustomer.id) {
                    var existingTarget = _.find(ctrl.targetTable.data, { id: target.id });
                    if (existingTarget) {
                        $rootScope.updateObj(existingTarget, target);
                    } else {
                        ctrl.targetTable.data.push(target);
                        ctrl.targetTable.total(ctrl.targetTable.total() + 1);
                    }
                }
            },
            openAddEditModal: function (target) {
                $uibModal.open({
                    templateUrl: '/app/target/modalAddTarget.html',
                    controller: 'AddTargetCtrl',
                    resolve: {
                        params: {
                            target: target,
                            noState: true,
                        },
                    },
                });
            },
        };

        for (var prop in ctrl) {
            if (!ctrl.hasOwnProperty(prop)) {
                //The current property is not a direct property of ctrl
                continue;
            }
            this[prop] = ctrl[prop];
        }

        //Replace the initial ctrl object with the updated this object in order to prevent the two from getting out of sync
        ctrl = this;
    },
});
