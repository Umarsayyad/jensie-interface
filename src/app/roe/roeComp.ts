import app from '../app';
import * as _ from 'lodash';
import * as angular from 'angular';
import catotheme from '@cato/config/themes/catotheme';

import './pages/companyProfile/companyProfileComp';
import './pages/eula/eulaAcceptanceComp';
import './pages/pocs/pocsComp';
import './pages/finishPage/finishPageComp';
import './pages/restrictions/restrictionsComp';
import './pages/services/servicesComp';
import './pages/start/startComp';
import './pages/periodOfPerformance/periodOfPerformanceComp';
import './pages/targets/targetsComp';
import './pages/approval/approvalComp';
import './utils/roeUtilsCtrl';
import { ValidTheme } from '@cato/config/themes/valid-themes';

app.component('interactiveRoe', {
    templateUrl: '/app/roe/roe.html',

    controller: function roeCtrl(
        $rootScope,
        globalDataService,
        roeUtilsCtrl,
        $window,
        RulesOfEngagements,
        WizardHandler,
        $q,
        modalService,
        socketService
    ) {
        //console.log("in RoE controller");

        var ctrl = this;
        ctrl.roe = undefined;
        ctrl.catotheme = catotheme;
        ctrl.gdata = globalDataService;
        ctrl.customer = ctrl.gdata.customer;
        ctrl.destroySubscribeBy = undefined;
        ctrl.cgBusy = {
            roeBusy: null,
        };
        ctrl.changePage = 1;
        ctrl.isCmmc = catotheme.name === ValidTheme.jensie;

        var appWindow = angular.element($window);

        var setDimensions = function () {
            roeUtilsCtrl.calculateRoeHeight($window, '#wizardContainer', 300);
        };

        ctrl.$onInit = function () {
            ctrl.cgBusy.roeBusy = $q.defer();
            //Added to control background color of page-wrapper for just the RoE
            angular.element('#page-wrapper').css('background-color', 'white');
            setDimensions();

            ctrl.roeRequest = RulesOfEngagements.list({ customerId: ctrl.customer.id });
            ctrl.roeRequest.$promise.then(function (roes) {
                ctrl.cgBusy.roeBusy.resolve(true);
                ctrl.cgBusy.roeBusy = null;

                if (roes[0]) {
                    ctrl.roe = angular.copy(roes[0]);
                    if (ctrl.roe.accepted_by) {
                        ctrl.agreed = true;
                    }
                    if (ctrl.roe.is_finalized) {
                        WizardHandler.wizard().goTo('Finish');
                    } else if (ctrl.roe.is_locked) {
                        WizardHandler.wizard().goTo('Approval');
                    }

                    ctrl.destroySubscribeBy = socketService.subscribeByModel('roe', function (response) {
                        //console.log("inside  socketServce callback");
                        if (response.id === ctrl.roe.id) {
                            if (ctrl.cgBusy.roeBusy && ctrl.roe.is_locked && !response.is_locked) {
                                ctrl.cgBusy.roeBusy.reject();
                                ctrl.cgBusy.roeBusy = null;
                                modalService.openError(
                                    'Error Generating RoE Document',
                                    `An error occurred while attempting to generate your official Rules of Engagement document. If this continues to occur, please notify us at <a href="mailto:${catotheme.appName.toLowerCase()}@${
                                        catotheme.appCompanyDomain
                                    }">${catotheme.appName.toLowerCase()}@${catotheme.appCompanyDomain}</a>.`,
                                    false
                                );
                            }
                            $rootScope.updateObj(ctrl.roe, response);
                            if (response.is_generated && ctrl.cgBusy.roeBusy) {
                                ctrl.cgBusy.roeBusy.resolve(true);
                                ctrl.cgBusy.roeBusy = null;
                            }
                        }
                    });
                }
            });
        };

        ctrl.prevPage = function () {
            ctrl.changePage = -ctrl.changePage;
            if (ctrl.roe.is_locked) {
                WizardHandler.wizard().goTo('Approval');
            }
        };

        ctrl.nextPage = function () {
            ctrl.changePage = -ctrl.changePage;
            if (ctrl.roe.is_locked) {
                WizardHandler.wizard().goTo('Approval');
            }
        };

        const prevPageUnsubscribe = $rootScope.$on('prevPage', ctrl.prevPage);

        const nextPageUnsubscribe = $rootScope.$on('nextPage', ctrl.nextPage);

        ctrl.$onDestroy = function () {
            appWindow.unbind('resize', setDimensions);
            angular.element('#page-wrapper')[0].style.removeProperty('background-color');
            if (ctrl.destroySubscribeBy) {
                ctrl.destroySubscribeBy();
            }
            prevPageUnsubscribe();
            nextPageUnsubscribe();
        };

        appWindow.bind('resize', setDimensions);

        ctrl.enterValidation = function () {
            if (WizardHandler.wizard().currentStepTitle() === 'Finish') {
                return false;
            } else if (WizardHandler.wizard().currentStepTitle() !== 'Approval') {
                return true;
            } else if (ctrl.roe.is_locked) {
                return false;
            } else {
                return true;
            }
        };
    },
});
