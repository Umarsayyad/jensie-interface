import * as angular from 'angular';
import * as _ from 'lodash';
import catotheme from '../../installation/themes/catotheme';
import { config } from '@cato/config';
import { mainName } from './cato.module';

import 'angular-ui-bootstrap';

import './common/stateBuilder';
import './common/iboxTools/iboxTools';
import './routes/cato';
import './routes/logged-in/general';
import './routes/logged-in/customer';
import './routes/logged-in/operator';
import './routes/logged-in/mission-director';
import './common/twofactor/TwoFactorVerificationCtrl';

_.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

const app: any = angular.module(mainName, [
    'ui.bootstrap',
    /////////////////////
    'cato.stateBuilder',
    'cato.ibox-tools',
    'cato.2fa',
    'cato.routes.cato',
    'cato.routes.general',
    'cato.routes.customer',
    'cato.routes.operator',
    'cato.routes.missionDirector',
]);

app.config(function (
    $urlRouterProvider,
    $httpProvider,
    $locationProvider,
    $resourceProvider,
    $uibModalProvider,
    $uibTooltipProvider,
    tagsInputConfigProvider
) {
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false,
    });

    $uibModalProvider.options.backdrop = 'static';
    $uibTooltipProvider.options({ appendToBody: true });
    tagsInputConfigProvider.setActiveInterpolation('tagsInput', {
        placeholder: true,
    });

    $urlRouterProvider.otherwise('/');

    function depreciationWarningTransformRequest(action) {
        return function (data, headersGetter) {
            //console.warn(`The "${action}" default $resource action is deprecated and should be updated`);
            return $httpProvider.defaults.transformRequest[0](data, headersGetter);
        };
    }

    angular.extend($resourceProvider.defaults.actions, {
        // Add warning to aid converting existing code over to use the consistent and unambiguous action names
        save: { method: 'POST', transformRequest: depreciationWarningTransformRequest('save') },
        query: { method: 'GET', isArray: true, transformRequest: depreciationWarningTransformRequest('query') },
        remove: { method: 'DELETE', transformRequest: depreciationWarningTransformRequest('remove') },
    });
    angular.extend($resourceProvider.defaults.actions, {
        create: { method: 'POST' },
        update: { method: 'PUT' },
        patch: { method: 'PATCH' },
        get: { method: 'GET' },
        list: { method: 'GET', isArray: true },
        detail: { method: 'GET', params: { detail: 'true' } },
        details: { method: 'GET', params: { detail: 'true' }, isArray: true },
        options: {
            method: 'OPTIONS',
            transformResponse: function (data, headers, status) {
                data = $httpProvider.defaults.transformResponse[0](data, headers, status);
                return _.get(data, 'actions.POST') || _.get(data, 'actions.PUT', data);
            },
        },
        delete: { method: 'DELETE' },
    });
});

app.run(function ($state, $stateParams, $q, $rootScope, $location, $http, $anchorScroll, editableOptions) {
    $rootScope.$on('$stateChangeError', console.debug.bind(console));
    $rootScope.$on('$stateChangeStart', console.debug.bind(console));
    $rootScope.$on('$stateNotFound', console.log.bind(console));

    $rootScope.CONFIG = config;
    $rootScope.catotheme = catotheme;
    $rootScope.year = new Date().getFullYear();

    $rootScope.loadingDefer = $q.defer();
    $rootScope.notificationsPermitted = false;
    $rootScope.loadingPromise = $rootScope.loadingDefer.promise;
    $rootScope.loginEvaluated = false;
    $rootScope.sessionEvaluated = false;
    $rootScope.loggedIn = false;

    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;

    function setToken(token) {
        if (token) {
            $rootScope.token = token;
            sessionStorage.setItem('token', token);
            $http.defaults.headers.common['Authorization'] = 'Token ' + token;
        }
    }

    $rootScope.setToken = setToken;

    $rootScope.location = $location;
    $rootScope.restUrl = '/api/v1/';
    setToken(sessionStorage.getItem('token'));
    var login_user = sessionStorage.getItem('user');
    if (login_user && login_user !== Object().toString()) {
        $rootScope.login_user = JSON.parse(login_user);
    } else {
        $rootScope.login_user = {};
    }

    $anchorScroll.yOffset = 90;

    editableOptions.theme = 'bs3';

    $(window).on('resize', function () {
        $rootScope.$broadcast('resize');
    });
});

export default app as angular.IModule;
