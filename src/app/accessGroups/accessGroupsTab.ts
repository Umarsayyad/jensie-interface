import app from '../app';
import * as _ from 'lodash';

import '../customer/manage/CondensedCustomerFilterComp';
import '../accessGroups/modalManageAccessGroup';

app.component('accessGroupsTab', {
    bindings: {
        currentTab: '<',
    },
    templateUrl: '/app/accessGroups/accessGroupsTab.html',
    controller: function ($rootScope, $uibModal, NgTableParams, globalDataService, modalService, stateBuilder, AccessGroups) {
        var ctrl = this;

        var unregisterWatch;
        ctrl.gdata = globalDataService;
        ctrl.catotheme = $rootScope.catotheme;
        ctrl._extraData = {};
        var lastOpened = {};
        ctrl.customer = undefined;
        ctrl.user = $rootScope.user;
        ctrl.isOperator = globalDataService.isOperator;

        ctrl.$onInit = function () {
            closeLastOpen();

            if (globalDataService.isOperator) {
                ctrl.setCustomer(_.get($rootScope.user, 'ui_data.selectedCustomer'));
                unregisterWatch = $rootScope.$watch('user.ui_data.selectedCustomer', function (newVal, oldVal) {
                    if (newVal !== oldVal && !ctrl.catoNgDisabled) {
                        ctrl.setCustomer(newVal);
                    }
                });
            }
        };

        ctrl.$onChanges = function () {
            closeLastOpen();
        };

        ctrl.loadGroups = function () {
            var params = { detail: true };

            if (ctrl.customer) {
                params['customer'] = ctrl.customer;
            }

            ctrl.accessGroups = AccessGroups.list(params);
        };

        if (!globalDataService.isOperator) {
            ctrl.customer = $rootScope.user.customer.id;
            ctrl.loadGroups();
        }

        ctrl.$onDestroy = function () {
            if (unregisterWatch) {
                unregisterWatch();
            }
        };

        function closeLastOpen(group?: any, current?: any) {
            if (current !== 'user' || group !== lastOpened) {
                ctrl.extraData(lastOpened).userPopoverIsOpen = false;
            }
            lastOpened = group;
        }

        ctrl.extraData = function (group) {
            group = group || {};
            var extraData = ctrl._extraData[group.id];
            if (!extraData) {
                extraData = ctrl._extraData[group.id] = {};
            }
            return extraData;
        };

        ctrl.accessGroupActions = {
            editGroup: {
                action: function (group) {
                    closeLastOpen(group);

                    var data = {
                        group: group,
                    };

                    var modal = stateBuilder.componentModalGenerator(
                        'modal-manage-access-group',
                        stateBuilder.accessGroupModalBindings,
                        data,
                        'accessGroups/modalManageAccessGroup'
                    );
                    var modalInstance = $uibModal.open(modal);

                    modalInstance.result.then(function (result) {
                        if (!ctrl.accessGroups) {
                            ctrl.accessGroups = [];
                        }

                        var index = _.findIndex(ctrl.accessGroups, { id: result.id });
                        if (index >= 0) {
                            ctrl.accessGroups.splice(index, 1, result);
                        } else {
                            ctrl.accessGroups.push(result);
                        }
                    });
                },
                class: 'btn-white',
                icon: 'fa-pencil',
                label: 'Edit',
                show: function () {
                    return true;
                },
                tooltip: 'Edit Group',
            },
            deleteGroup: {
                action: function (group) {
                    modalService.openDelete('Confirmation', "Access Group '" + group.name + "'", group, function () {
                        AccessGroups.delete(
                            { accessGroupId: group.id },
                            group,
                            function () {
                                _.remove(ctrl.accessGroups, { id: group.id });
                            },
                            modalService.openErrorResponseCB('Error deleting Access Group')
                        );
                    });
                },
                class: 'btn-white',
                icon: 'fa-trash',
                label: 'Delete',
                show: function (group) {
                    return globalDataService.isMissionDirector;
                },
                tooltip: 'Delete Group',
            },
        };

        ctrl.toggleUserQuickView = function (group) {
            ctrl.extraData(group).userPopoverIsOpen = !ctrl.extraData(group).userPopoverIsOpen;
            if (ctrl.extraData(group).userPopoverIsOpen) {
                closeLastOpen(group, 'user');

                var user_permissions = _.map(group.permissions, 'user');
                var userlist = _.filter($rootScope.gdata.users, function (o) {
                    return user_permissions.indexOf(o.id) === -1;
                });
                ctrl.extraData(group).usersTable = new NgTableParams({
                    counts: [5, 10, 20, 40],
                    dataset: userlist,
                });
            }
        };

        ctrl.getSortValue = function (sortType) {
            if (_.isUndefined(sortType)) {
                return undefined;
            }

            return function (item) {
                if (sortType === 'customer') {
                    return ctrl.gdata.customerMap[item[sortType]].name;
                } else if (sortType === 'name') {
                    return item.name;
                }
            };
        };

        ctrl.setCustomer = function (customer) {
            ctrl.customer = customer ? customer.id : undefined;
            ctrl.loadGroups();
        };
    },
});
