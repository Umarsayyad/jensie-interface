import app from '../app';
import * as _ from 'lodash';

app.controller('AddPermissionCtrl', function (
    $scope,
    $state,
    $stateParams,
    $rootScope,
    AccessGroupPermissions,
    globalDataService,
    uibDateParser,
    modalService,
    $uibModal,
    $uibModalInstance,
    params
) {
    $scope.permission = $rootScope.makePlainCopy(params.permission);
    if ($scope.permission !== undefined) {
        $scope.permission = {
            read: false,
            write: false,
        };
        $scope.permission.mode = 'new';
    } else {
        $scope.permission.mode = 'edit';
    }

    $scope.userObjs = [];
    _.each(params.available_users, function (user_id) {
        $scope.userObjs.push($rootScope.gdata.userMap[user_id]);
    });

    if ($scope.permission.user) {
        $scope.permission.user = $scope.gdata.userMap[$scope.permission.user];
    }
    $scope.existing_user_permissions = params.existing_user_permissions;
    $scope.customer = params.customer;
    $scope.access_group = params.access_group;

    $scope.addPermission = function () {
        $scope.permission.user = $scope.permission.user.id;
        $uibModalInstance.close($scope.permission);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});
