import app from '../app';
import * as angular from 'angular';
import * as _ from 'lodash';

import '../customer/manage/CondensedCustomerFilterComp';

app.component('modalManageAccessGroup', {
    bindings: {
        group: '<?',
        groupSaved: '<?',
        modalTitle: '<?',
        close: '<',
        dismiss: '<',
    },
    templateUrl: '/app/accessGroups/modalManageAccessGroup.html',
    controller: function ($rootScope, globalDataService, modalService, Customers, Targets, AccessGroups, AccessGroupPermissions, $uibModal) {
        var ctrl = this;
        ctrl.isOperator = globalDataService.isOperator;
        ctrl.user = $rootScope.user;
        ctrl.gdata = globalDataService;
        ctrl.targets = undefined;
        ctrl.catotheme = $rootScope.catotheme;
        ctrl.permissionsToAdd = [];
        var isNew = false;

        ctrl.$onInit = function () {
            if (_.isUndefined(ctrl.group)) {
                isNew = true;
                ctrl.modalTitle = ctrl.modalTitle || 'Create New Group';
                ctrl.group = {};
                if (!ctrl.isOperator) {
                    ctrl.group.customer = ctrl.user.customer;
                } else {
                    ctrl.setCustomer(_.get($rootScope.user, 'ui_data.selectedCustomer'));
                }
            } else {
                ctrl.modalTitle = ctrl.modalTitle || 'Edit Group';
            }

            if (ctrl.group.customer) {
                ctrl.getCustomerTargets();
            }
        };

        ctrl.saveGroup = function () {
            var sendGroup = angular.copy(ctrl.group);

            if (!sendGroup.hasOwnProperty('targets')) {
                sendGroup.targets = [];
            }

            if (ctrl.group.id) {
                AccessGroups.update({ accessGroupId: sendGroup.id }, sendGroup, saveSuccess, saveGroupError);
            } else {
                AccessGroups.create(sendGroup, saveSuccess, saveGroupError);
            }
        };

        function saveSuccess(group) {
            /* The group was saved, now save the subordinate permissions. */

            _.each(ctrl.group.permissions, function (permission) {
                var sendPermission = angular.copy(permission);

                if (permission.mode === 'new') {
                    AccessGroupPermissions.create(
                        { accessGroupId: group.id },
                        sendPermission,
                        function (result) {
                            var new_permission = $rootScope.makePlainCopy(result);
                            ctrl.group.permissions.push(new_permission);
                        },
                        savePermissionError
                    );
                }
            });

            (ctrl.groupSaved || _.noop)(group);
            if (!ctrl.addAnother) {
                ctrl.close(group);
            } else {
                ctrl.message = 'Group saved successfully';
            }
        }

        function saveGroupError(result) {
            modalService.openErrorResponse('Group could not be saved.', result, false);
        }

        function savePermissionError(result) {
            modalService.openErrorResponse('Group could not be saved.', result, false);
        }

        ctrl.setCustomer = function (customer) {
            if (customer) {
                ctrl.group.customer = customer.id;
                ctrl.available_users = _.filter(ctrl.gdata.users, { customer: ctrl.group.customer });
                ctrl.getCustomerTargets();
            } else {
                ctrl.group.customer = undefined;
            }
        };

        ctrl.removePermission = function (permission) {
            modalService.openDelete('Delete permission', 'the permission for ' + ctrl.gdata.userMap[permission.user].username, permission, function (
                permission
            ) {
                AccessGroupPermissions.delete(
                    {
                        accessGroupId: ctrl.group.id,
                        accessGroupPermissionId: permission.id,
                    },
                    {},
                    function () {
                        _.remove(ctrl.group.permissions, function (i_perm?: any) {
                            return i_perm && permission.id === i_perm.id;
                        });
                    }
                );
            });
        };

        ctrl.addEditPermission = function (permission) {
            var customer_users = ctrl.gdata.users;
            if (ctrl.isOperator) {
                customer_users = _.filter(customer_users, { customer: ctrl.group.customer });
            }

            var existing_permission_users = _.map(ctrl.group.permissions, 'user');

            var available_users = _.difference(_.map(customer_users, 'id'), existing_permission_users);

            if (permission) {
                // If editing, add back the relevant user.
                available_users.push(permission.user);
            }

            var modalInstance = $uibModal.open({
                templateUrl: '/app/accessGroups/modalAddPermission.html',
                controller: 'AddPermissionCtrl',
                resolve: {
                    params: {
                        permission: permission,
                        existing_user_permissions: ctrl.group.permissions,
                        available_users: available_users,
                        customer: ctrl.group.customer,
                    },
                },
            });

            modalInstance.result.then(function (permission) {
                if (!ctrl.group.permissions) {
                    ctrl.group.permissions = [];
                }

                if (!ctrl.group.id) {
                    ctrl.group.permissions.push(permission);
                } else {
                    if (permission.mode === 'edit') {
                        AccessGroupPermissions.update(
                            { accessGroupPermissionId: permission.id },
                            permission,
                            function (updated_permission) {
                                _.remove(ctrl.group.permissions, {
                                    user: updated_permission.user,
                                });

                                ctrl.group.permissions.push(updated_permission);
                            },
                            savePermissionError
                        );
                    } else if (permission.mode === 'new') {
                        AccessGroupPermissions.create(
                            { accessGroupId: ctrl.group.id },
                            permission,
                            function (new_permission) {
                                ctrl.group.permissions.push(new_permission);
                            },
                            savePermissionError
                        );
                    }
                }
            });
        };

        ctrl.getCustomerTargets = function () {
            if (_.isEmpty(ctrl.targets)) {
                ctrl.targets = Targets.basicList({ customer: ctrl.group.customer });
            }
        };
    },
});
