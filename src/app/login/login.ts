import app from '../app';
import * as angular from 'angular';

import { config } from '@cato/config';
import { ValidTheme } from '@cato/config/themes/valid-themes';

app.controller('loginController', function ($rootScope, $scope, loginService, $state, $http, $stateParams, $location) {
    $rootScope.loadingDefer.resolve();
    $scope.ValidTheme = ValidTheme;
    $scope.mepInstance = config.mepInstance === 'true';

    // Go to initial-state if already logged in
    $http({ url: config.apiPath + 'user/me' })
        .then(function (data) {
            console.log(data);
            $state.go(config['initialState']);
        })
        .catch(console.log);

    $scope.invalidLogin = false;

    if ($stateParams.message) {
        $scope.message = $stateParams.message;
    }

    function success(response) {
        console.log('Logged in!', new Date());

        $rootScope.sessionEvaluated = true;
        $rootScope.loggedIn = true;

        console.log(response);

        sessionStorage.setItem('user', angular.toJson(response.data.user));
        $rootScope.login_user = response.data.user;

        $rootScope.setToken(response.data.token);

        if (
            $stateParams.redirectTo &&
            $stateParams.redirectTo !== '/login' &&
            $stateParams.redirectTo !== '/logout' &&
            $stateParams.redirectTo !== '/'
        ) {
            $location.url($stateParams.redirectTo);
        } else {
            $state.go('cato.loggedIn.dashboard');
        }
    }

    function error(data) {
        console.log(data);
        if (data === undefined) {
            alert('unknown error');
            return;
        }
        if (data.data) {
            data = data.data;
        }

        if (data.detail === 'Invalid token.' || data.detail === 'Authentication credentials were not provided.') {
            //ignore
        } else if (data.detail === 'CSRF Failed: CSRF token missing or incorrect.') {
            debugger;
            //$cookies.remove('csrftoken');
        } else if (data.status === 204) {
            //modalService.openError('Error!', data.message);
            alert('Error: ' + data);
        } else {
            $scope.invalidLogin = true;
            $scope.invalidLoginMessage = data.detail;
            alert('Error: ' + data.detail);

            setTimeout(function () {
                $scope.$apply(function () {
                    $scope.invalidLogin = false;
                });
            }, 5000);
        }
    }

    $scope.login = function () {
        loginService.login($scope.username.toLowerCase(), $scope.password, success, error);
        return false;
    };

    //loginService.checkLogin(success ,error);
})
.service('loginService', function ($http, $rootScope, globalDataService, modalService, socketService) {
    function b64EncodeUnicode(str) {
        return btoa(
            encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function (match, p1) {
                return String.fromCharCode(Number.parseInt(p1, 16));
            })
        );
    }

    function login(u, p, success, error) {
        $http({
            url: '/auth/login/',
            method: 'POST',
            headers: {
                Authorization: 'Basic ' + b64EncodeUnicode(u + ':' + p),
            },
        }).then(success, error);
    }

    function checkLogin(success, error) {
        console.log('checking login');
        $http({
            url: '/auth/login/',
            method: 'POST',
        }).then(success, error);
    }

    function logoutCleanup() {
        $rootScope.loginEvaluated = false;
        socketService.disconnect();
        delete $rootScope.token;
        delete $rootScope.user;
        delete $rootScope.login_user;
        delete $rootScope.username;
        $rootScope.newItemCounts = {};
        sessionStorage.removeItem('token');
        sessionStorage.removeItem('user');
        delete $http.defaults.headers.common['Authorization'];
        globalDataService.unload();
    }

    function logout() {
        return $http({
            url: '/auth/logout/',
            method: 'POST',
        }).then(
            function (res) {
                console.log(['successfully logged out', res]);
                logoutCleanup();
            },
            function (res) {
                console.log(['failed to log out', res]);
                logoutCleanup();
                if (res._status !== 401) {
                    modalService.openErrorResponse('Log out Failed', res, 10000); // 10 seconds
                    throw res; // Make sure any promises that are on .logout() see the failure too
                }
                // Don't throw on a 401, that is expected if the user isn't logged in when they try to log out
            }
        );
    }

    return {
        login: login,
        checkLogin: checkLogin,
        logout: logout,
    };
});
