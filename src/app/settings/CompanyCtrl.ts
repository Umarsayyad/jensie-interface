import app from '../app';
import * as _ from 'lodash';
import * as angular from 'angular';
import '../roe/utils/viewRoE';
import catotheme from '@cato/config/themes/catotheme';
import { ValidTheme } from '@cato/config/themes/valid-themes';
import { UibModal } from '../../ng-app/ngjs-upgrade/uib-modal';
import { INonDisclosureResult } from '../../ng-app/customer/models/non-disclosure.interface';

app.controller('CompanyCtrl', function (
    $scope,
    $filter,
    $state,
    $sce,
    $uibModal,
    stateBuilder,
    NaicsIndustryService,
    modalService,
    SvcUtilsService,
    Customers,
    CustomerSubscriptions,
    EmailDomains,
    Locations,
    PointsOfContact,
    PointOfContactAddresses,
    Services,
    Urls,
    Users,
    globalDataService,
    RulesOfEngagements,
    Restrictions,
    Targets,
    NonDisclosureResource
) {
    $scope.gdata = globalDataService;

    $scope.TIERS = SvcUtilsService.TIERS;

    $scope.data = {
        roe: {},
    };
    $scope.config = {
        customer: {},
        emaildomain: {},
        location: {},
        poc: {},
        pocaddr: {},
        url: {},
    };

    $scope.validateSam = false;

    $scope.roe = $sce.trustAsHtml('<p>Your signed rules of engagement is on file.</p>');

    $scope.mentorPlaceholder = '';

    $scope.getAcceptedBy = function () {
        if (!_.isNil($scope.acceptedBy)) {
            const user = $scope.gdata.userMap[$scope.acceptedBy];
            if (_.isNil(user)) return; // In case it isn't loaded yet
            return user.first_name + ' ' + user.last_name + ' (' + user.email + ')';
        }
    };

    function openIndustrySelector(): Promise<string> {
        const modal = stateBuilder.ngComponentModalGenerator('industry-selector');
        return $uibModal.open(modal).result;
    }

    function selectIndustry() {
        openIndustrySelector().then((naicsChoice: string) => {
            $scope.customer.naics = naicsChoice;
            $scope.naicsName = NaicsIndustryService.getNaicsName(naicsChoice);
            $scope.saveCustomer();
        });
    }
    $scope.selectIndustry = selectIndustry;

    $scope.icons = {
        phonenumber: 'fa-phone',
        email: 'fa-envelope-o',
        physical: 'fa-map-marker',
        legal: 'fa-balance-scale',
        technical: 'fa-terminal',
        billing: 'fa-usd',
        executive: 'fa-empire',
        'USG Program Manager': 'fa-gavel',
    };

    $scope.sizes = ['1-99', '100-249', '250-499', '500-999', '1000-1499', '1500-4999', '5000-9999', '10000+'];

    $scope.mppDate = { opened: false };

    $scope.dateOptions = {
        formatYear: 'yy',
        maxDate: new Date(),
        startingDay: 1,
    };

    $scope.openMppDate = function () {
        $scope.mppDate.opened = true;
    };

    $scope.availableMentors = Customers.availableMentors({}, function (results) {
        for (const company of results) {
            if (company.id === $scope.customer.mentor_company) {
                $scope.mentorPlaceholder = company.name;
                break;
            }
        }
    });

    $scope.setHasParticipated = function () {
        if (!$scope.customer.has_participated_mentor_program) {
            $scope.customer.has_current_participation_mentor_program = false;
            $scope.mentor_company = null;
            $scope.customer.mpp_start = null;
            $scope.mentorPlaceholder = '';
        }
        $scope.saveCustomer();
    };

    $scope.isParticipating = function () {
        if (!$scope.customer.has_current_participation_mentor_program) {
            $scope.mentorPlaceholder = '';
            $scope.mentor_company = null;
            $scope.customer.mpp_start = null;
        }
        $scope.saveCustomer();
    };

    $scope.setMentor = function () {
        const mentorCompany = (<HTMLInputElement>document.getElementById('mentor')).value;
        if (mentorCompany !== '') {
            const mentorRequest = Customers.createMentor({ name: mentorCompany });
            mentorRequest.$promise.then(function (response) {
                $scope.customer.mentor_company = response.id;
                $scope.saveCustomer();
            });
        }
    };

    $scope.countries = $scope.gdata.countries;

    $scope.otherLocationIsPrimary = function () {
        if ($scope.locations) {
            angular.forEach($scope.locations, function (loc) {
                if (loc.primaryLocation) {
                    return true;
                }
            });
        }
    };

    function loadNDAFiles(customerId) {
        NonDisclosureResource.list(customerId).subscribe((ndaFiles) => {
            $scope.ndaFiles = ndaFiles;
        });
    }

    $scope.manageNda = function (customerId) {
        const data = { customerId: customerId, ndaFiles: $scope.ndaFiles };
        const bindings = { customerId: '<', ndaFiles: '<' };

        const modal = stateBuilder.componentModalGenerator('modal-manage-nda', bindings, data);
        const modalInstance: UibModal<INonDisclosureResult> = $uibModal.open(modal);

        modalInstance.result.then(function (ndaFile) {
            if (ndaFile && ndaFile.uploaded && ndaFile.uploaded.length > 0) {
                $scope.ndaFiles = $scope.ndaFiles ? $scope.ndaFiles : [];
                ndaFile.uploaded.forEach((item) => {
                    $scope.ndaFiles.push(item);
                });
            }
        });
    };

    function loadAllOptions() {
        Customers.options().$promise.then(function (results) {
            $scope.config.customer = results.actions.POST;
            $scope.config.customer.duns.pattern = new RegExp($scope.config.customer.duns.pattern);
        });

        EmailDomains.options().$promise.then(function (results) {
            $scope.config.emaildomain = results.actions.POST;
        });

        Locations.options().$promise.then(function (results) {
            $scope.config.location = results.actions.POST;
        });

        Urls.options().$promise.then(function (results) {
            $scope.config.url = results.actions.POST;
        });

        PointsOfContact.options().$promise.then(function (results) {
            $scope.config.poc = results.actions.POST;
        });

        PointOfContactAddresses.options().$promise.then(function (results) {
            $scope.config.pocaddr = results.actions.POST;
        });
    }

    function buildContactCards(customerId) {
        /* For each PoC build a separate card, but if a user has multiple roles... we can try to merge them
         * nicely.
         */
        PointsOfContact.query({ customerId: customerId, detail: true }).$promise.then(function (pointsofcontact) {
            $scope.data.pocs = pointsofcontact;
        });
    }

    function loadCustomer(customerId) {
        Customers.get({ customerId: customerId })
            .$promise // TODO: eliminate unnecessary HTTP requests
            .then(function (customer) {
                $scope.customer = customer;
                $scope.customer.business_category = [];
                $scope.customer = $scope.gdata.setCustomerBusinessValues(customer);
                $scope.enforce2FA = catotheme.name == ValidTheme.jensie;
                $scope.naicsName = NaicsIndustryService.getNaicsName($scope.customer.naics);
                if ($scope.customer.mpp_start) {
                    // Workaround known bug: https://github.com/angular-ui/bootstrap/issues/2628
                    const d = new Date($scope.customer.mpp_start);
                    d.setMinutes(d.getMinutes() + d.getTimezoneOffset());
                    $scope.customer.new_mpp_start = d;
                }

                if (customer.current_rules_of_engagement) {
                    const roeQuery = RulesOfEngagements.query({ customerId: $scope.customer.id });
                    roeQuery.$promise.then(function (roes) {
                        const currentRoe = roes[roes.length - 1];
                        $scope.acceptedDate = currentRoe.period_of_performance_acknowledged_at;
                        $scope.acceptedBy = currentRoe.period_of_performance_acknowledged_by;
                        //The next line is related to services selected in the RoE, which has been removed.
                        //$scope.numServices = currentRoe.services.length;

                        Restrictions.list({ customerId: $scope.customer.id, roeId: currentRoe.id }).$promise.then(function (restrictions) {
                            $scope.numRestrictions = restrictions.length;
                        });

                        Targets.get({ customer: $scope.customer.id, ais: true, page_size: 1 }, function (targets) {
                            $scope.numTargets = targets.count;
                        });
                    });
                    $scope.getSubscriptions();
                }
            });

        EmailDomains.query({ customerId: customerId }).$promise.then(function (domains) {
            $scope.data.emaildomains = domains;
        });

        Locations.query({ customerId: customerId }).$promise.then(function (locations) {
            $scope.locations = _.filter(locations, { personalLocation: false });
            $scope.data.locationMap = _.keyBy(locations, 'id');
        });

        Urls.query({ customerId: customerId }).$promise.then(function (urls) {
            $scope.data.urls = urls;
        });
    }

    function customerServices() {
        $scope.customer = $scope.gdata.customer;
        if ($scope.customer.mpp_start) {
            const d = new Date($scope.customer.mpp_start);
            d.setMinutes(d.getMinutes() + d.getTimezoneOffset());
            $scope.customer.mpp_start = d;
        }

        const custServices = [];
        $scope.isChecked = [];

        Services.query()
            .$promise // TODO: eliminate unnecessary HTTP requests
            .then(function (services) {
                $.each(services, function (s, svc) {
                    custServices.push(svc);
                    if ($scope.customer.services.indexOf(svc.id) > -1) {
                        $scope.isChecked[svc.id] = true;
                    }
                });

                $scope.tieredServices = SvcUtilsService.breakIntoTiers(custServices);
            });
    }

    function loadUser() {
        Users.get({ userId: 'me' })
            .$promise // TODO: eliminate unnecessary HTTP requests
            .then(function (user) {
                return user.customer;
            })
            .then(function (customerId) {
                loadCustomer(customerId);
                buildContactCards(customerId);
                loadNDAFiles(customerId);
            });
    }

    function toggleService(service) {
        service.visible = !service.visible;
    }

    $scope.toggleService = toggleService;

    loadAllOptions();
    customerServices();
    loadUser();

    $scope.setLocationAsPrimary = function () {
        $scope.locationIsPrimary = true;
    };

    $scope.saveOrUpdateLocation = function (location) {
        const customer_id = $scope.customer.id;
        location['primaryLocation'] = $scope.locationIsPrimary;
        $scope.locationIsPrimary = false;

        if (location.id === undefined) {
            Locations.create(
                { customerId: customer_id },
                location,
                function (locationResult) {
                    $scope.locations[0] = locationResult;
                },
                function (res) {
                    modalService.openErrorResponseCB('Unable to create new location information: ', res);
                }
            );
        } else {
            Locations.update(
                { customerId: customer_id, locationId: location.id },
                location,
                function (locationResult) {
                    for (let i = 0; i < $scope.locations.length; i++) {
                        if ($scope.locations[i].id === location.id) {
                            $scope.locations[i] = locationResult;
                            break;
                        }
                    }
                },
                function (res) {
                    modalService.openErrorResponseCB('Unable to update location information: ', res);
                }
            );
        }
    };

    $scope.deleteLocation = function (location) {
        for (let i = 0; i < $scope.locations.length; i++) {
            if ($scope.locations[i].id === location.id) {
                $scope.locations.splice(i, 1);
                break;
            }
        }
        $scope.locationsRequest = Locations.delete({ customerId: $scope.customer.id, locationId: location.id });
    };

    $scope.cancelCreation = function () {
        $scope.locations.shift();
    };

    $scope.cancelLocation = function () {
        $scope.locations.shift();
    };

    $scope.saveOrUpdateUrl = function (url) {
        const customer_id = $scope.customer.id;

        if (url.id === undefined) {
            Urls.create(
                { customerId: customer_id },
                url,
                function (result) {
                    $scope.data.urls[0] = result;
                },
                function (res) {
                    $scope.urlErrors = true;
                    modalService.openErrorResponse('Unable to create new url: ', res);
                }
            );
        } else {
            Urls.update(
                { customerId: customer_id, urlId: url.id },
                url,
                function (result) {
                    for (let i = 0; i < $scope.data.urls.length; i++) {
                        if ($scope.data.urls[i].id === url.id) {
                            $scope.data.urls[i] = result;
                            break;
                        }
                    }
                },
                function (res) {
                    $scope.urlErrors = true;
                    modalService.openErrorResponse('Unable to update url: ', res);
                }
            );
        }
    };

    $scope.addUrl = function () {
        if ($scope.data.urls.length > 0) {
            if ($scope.data.urls[0].id === undefined) {
                return;
            }
        }

        $scope.data.urls.unshift({});
    };

    $scope.cancelUrl = function () {
        $scope.data.urls.shift();
    };

    /* this code is basically generic... just picks a different property and different resource. */
    $scope.saveOrUpdateDomain = function (domain) {
        const customer_id = $scope.customer.id;

        if (domain.id === undefined) {
            EmailDomains.create(
                { customerId: customer_id },
                domain,
                function (result) {
                    $scope.data.emaildomains[0] = result;
                },
                function (res) {
                    modalService.openErrorResponse('Unable to create new domain: ', res);
                }
            );
        } else {
            EmailDomains.update(
                { customerId: customer_id, domainId: domain.id },
                domain,
                function (result) {
                    for (let i = 0; i < $scope.data.emaildomains.length; i++) {
                        if ($scope.data.emaildomains[i].id === domain.id) {
                            $scope.data.emaildomains[i] = result;
                            break;
                        }
                    }
                },
                function (res) {
                    modalService.openErrorResponse('Unable to update domain: ', res);
                }
            );
        }
    };

    $scope.addEmailDomain = function () {
        if ($scope.data.emaildomains.length > 0) {
            if ($scope.data.emaildomains[0].id === undefined) {
                return;
            }
        }

        $scope.data.emaildomains.unshift({});
    };

    $scope.cancelDomain = function () {
        $scope.data.emaildomains.shift();
    };

    $scope.checkCage = function () {
        $scope.validateSam = true;
        $scope.saveCustomer();
    };

    $scope.checkDuns = function() {
        $scope.validateSam = true;
        $scope.saveCustomer();
    };

    $scope.validateMinMax = function (data, min, max) {
        if (data.length < min) {
            return 'Value is too short, minimum length: ' + min;
        }
        if (data.length > max) {
            return 'Value is too long, maximum length: ' + max;
        }
    };

    $scope.validatePattern = function (data, pattern) {
        if (!pattern.test(data)) {
            return 'Invalid D-U-N-S, please enter as XX-XXX-XXXX (12-345-6789)';
        }
    };

    $scope.updateMppDate = function () {
        $scope.customer.mpp_start =
            $scope.customer.new_mpp_start.getFullYear() +
            '-' +
            ($scope.customer.new_mpp_start.getMonth() + 1) +
            '-' +
            $scope.customer.new_mpp_start.getDate();

        $scope.saveCustomer();
    };

    $scope.saveCustomer = function () {
        $scope.customer = $scope.gdata.setCustomerBusinessBooleans($scope.customer);

        Customers.update({ customerId: $scope.customer.id }, $scope.customer)
            .$promise.then(function (result) {
                if ($scope.validateSam) {
                    Customers.validateSam({ customerId: result.id }, {});
                    $scope.validateSam = false;
                }

                const business_category = $scope.customer.business_category;
                $scope.customer = result;
                $scope.customer.business_category = business_category;
                if ($scope.customer.mpp_start) {
                    const d = new Date($scope.customer.mpp_start);
                    d.setMinutes(d.getMinutes() + d.getTimezoneOffset());
                    $scope.customer.new_mpp_start = d;
                }
            })
            .catch(function (result) {
                console.log('failing: ' + JSON.stringify(result, null, 2));
            });
    };

    /* Deal with the service stuff, maybe put in a different controller for the bottom half othe page. */
    $scope.recurranceOptions = [
        { value: 'monthly', text: 'Monthly' },
        { value: 'yearly', text: 'Yearly' },
    ];

    /*
    $scope.getBillingRecurrenceOptions = function() {
        var selected = $filter('filter')($scope.recurranceOptions, {value: $scope.company.billing.recurrance});
        return ($scope.company.billing.recurrance && selected.length) ? selected[0].text : 'Please select an option';
    };
     */

    $scope.editSummary = false;
    $scope.toggleEditSummary = function () {
        $scope.editSummary = $scope.editSummary === false;
    };

    $scope.editPicture = false;
    $scope.toggleEditPicture = function () {
        $scope.editPicture = $scope.editPicture === false;
    };

    $scope.saveSummary = function () {
        $scope.user.summary = $('.editSummary .note-editor .note-editable').text();
        $scope.toggleEditSummary();
    };
    $scope.cancelSummary = function () {
        $scope.toggleEditSummary();
    };

    $scope.update2FA = function () {
        Customers.set2FA(
            { customerId: $scope.customer.id },
            { enforce_2fa: $scope.customer.enforce_2fa },
            _.noop,
            modalService.openErrorResponseCB('Unable to set 2FA enforcement')
        );
    };

    $scope.getSubscriptions = function () {
        const numsToNames = $scope.servicesNumsToNames();

        $scope.subscriptions = CustomerSubscriptions.query({ customerId: $scope.customer.id });
        $scope.subscriptions.$promise.then(function (subscriptions) {
            $scope.subscriptionExpirations = {};
            $scope.subscriptionExpirationDatesOnly = {};
            _.each(subscriptions, function (subscription) {
                const endDate = new Date(subscription.expiration_date);
                $scope.subscriptionExpirations[subscription.service] =
                    'Service ' +
                    numsToNames[subscription.service] +
                    ' will run until ' +
                    new Date(endDate.getUTCFullYear(), endDate.getUTCMonth(), endDate.getUTCDate()).toDateString();
                $scope.subscriptionExpirationDatesOnly[subscription.service] = endDate;
            });
        });
    };

    $scope.servicesNumsToNames = function () {
        const mapping = {};

        _.each($scope.gdata.services, function (service) {
            mapping[service.id] = service.name;
        });

        return mapping;
    };
});
