import app from '../app';
import * as _ from 'lodash';

app.controller('NotificationsCtrl', function ($scope, Users, FindingService, modalService, NotificationService) {
    // Notifications are available through $scope.gdata.notifications
    console.log('gdata.notifications: ', $scope.gdata.notifications);

    $scope.config = {};

    $scope.notificationsForm = {};
    $scope.newNotification = {};
    $scope.showNewForm = false;

    if ($scope.gdata.isCustomer) {
        $scope.finding_severities = FindingService.SEVERITY_LEVEL_MAP.splice(1);
    } else {
        $scope.finding_severities = FindingService.SEVERITY_LEVEL_MAP;
    }

    $scope.getTriggersForEventType = function (event_type, notification) {
        // TODO: reorganize this function to not set a controller-wide $scope property on a per-row basis, that won't work when we have multiple types of triggers
        // We can make this return different types of trigger maps based on what the event type is
        // (optimally we'll get this from the API in the future)
        if (notification) {
            notification.event_type = event_type;
            $scope.gdata.updateNotification(notification);
        }

        if (event_type === 'New Finding') {
            $scope.triggers = $scope.finding_severities;
        } else {
            $scope.triggers = [];
        }

        console.log('triggers: ', $scope.triggers);
    };

    var operatorEmailOnly = {
        'Appliance Heartbeat Event': 'Appliance Heartbeat Event',
    };

    $scope.typeChanged = function () {
        if ($scope.gdata.isOperator) {
            // Simplify adding/removing by just removing by default, and adding back when needed.
            for (var key in operatorEmailOnly) {
                _.remove($scope.config.notifications.event_type.choices, function (event_type: any) {
                    return event_type.display_name === key;
                });
            }

            if ($scope.newNotification.notification_type === 'Email') {
                for (var key in operatorEmailOnly) {
                    $scope.config.notifications.event_type.choices.push({
                        display_name: key,
                        value: operatorEmailOnly[key],
                    });
                }
            }
        }
    };

    $scope.showTriggers = function (notification) {
        //$scope.getTriggersForEventType(notification.event_type);
        // TODO: make this generic once we have other types of triggers
        if (notification.event_type === 'New Finding') {
            var selected = _.map(notification.triggers, function (t) {
                return FindingService.SEVERITY_LEVEL[t];
            });
            return selected.length ? selected.join(', ') : 'None';
        }
        return 'N/A';
    };

    function loadAllOptions() {
        Users.notificationOptions({ userId: $scope.user.id }).$promise.then(function (results) {
            $scope.config.notifications = results.actions.POST;
        });
    }

    loadAllOptions();

    $scope.saveNewNotification = function () {
        var newNotification = $scope.newNotification;

        Users.addNotification(
            { userId: $scope.user.id },
            newNotification,
            function (notificationResult) {
                $scope.showNewForm = false;
                $scope.newNotification = {};
                $scope.gdata.addNotification(notificationResult);
            },
            function (res) {
                modalService.openErrorResponse('Unable to create new notification: ', res);
            }
        );
    };

    $scope.saveOrUpdateNotification = function (notification) {
        var notificationsForm = $scope.notificationsForm;
        Users.updateNotification(
            {
                userId: $scope.user.id,
                notificationId: notification.id,
            },
            notificationsForm,
            function (notificationResult) {
                $scope.gdata.updateNotification(notificationResult);
            },
            function (res) {
                modalService.openErrorResponse('Unable to update notification: ', res);
            }
        );
    };

    $scope.deleteNotification = function (notificationId) {
        var notification: any = _.find($scope.gdata.notifications, { id: notificationId });
        var content = 'the ' + notification.notification_type + ' notification for ' + notification.event_type + ' events';
        if (notification.triggers) {
            content += ' with criteria of: ' + $scope.showTriggers(notification);
        }
        modalService.openDelete('Delete Notification', content, notification, function (notification) {
            Users.deleteNotification(
                { userId: $scope.user.id, notificationId: notification.id },
                function () {
                    $scope.gdata.deleteNotification(notification.id);
                },
                function (res) {
                    modalService.openErrorResponse('Unable to delete notification: ', res);
                }
            );
        });
    };

    $scope.saveOriginalNotification = function (notification) {
        // Need do this so that we can get trigger-updates in our form and cancel out
        $scope.originalNotification = _.clone(notification);
    };

    $scope.resetNotification = function (notification) {
        $scope.gdata.updateNotification($scope.originalNotification);
    };

    $scope.addNotification = function () {
        $scope.showNewForm = true;
    };

    $scope.cancelCreation = function () {
        $scope.showNewForm = false;
        $scope.newNotification = {};
    };
});
