import app from '../app';
import * as _ from 'lodash';

app.controller('ProfileCtrl', function ($scope, $rootScope, $state) {
    $scope.tabs = [
        {
            heading: 'COMPANY PROFILE',
            icon: 'fa-building',
            show: function (tab) {
                return $scope.gdata.isCustomer;
            },
            viewName: 'company',
            stateName: 'company',
        },
        {
            heading: 'SERVICES',
            icon: 'fa-gears',
            show: function (tab) {
                return $scope.gdata.isCustomer;
            },
            viewName: 'services',
            stateName: 'services',
        },
        {
            heading: 'APPLIANCES',
            icon: 'fa-microchip',
            show: function (tab) {
                return $scope.gdata.isCustomer;
            },
            viewName: 'appliances',
            stateName: 'appliances',
        },
        {
            heading: 'DOCUMENTS',
            icon: 'fa-file-text-o',
            show: function (tab) {
                return $scope.gdata.isCustomer;
            },
            viewName: 'roe',
            stateName: 'roe',
        },
        {
            heading: 'NOTIFICATIONS',
            icon: 'fa-bullhorn',
            show: function (tab) {
                return $scope.hasAnyRole(['customer', 'operator', 'mission_director']);
            },
            viewName: 'notifications',
            stateName: 'notifications',
        },
        {
            heading: 'MANAGE 2FA DEVICES',
            icon: 'fa-key',
            show: function (tab) {
                return true;
            },
            viewName: 'twofactor',
            stateName: 'twofactor',
        },
        {
            heading: 'CHANGE PASSWORD',
            icon: 'fa-lock',
            show: function (tab) {
                return true;
            },
            viewName: 'changePassword',
            stateName: 'changePassword',
        },
    ];

    var stateChangeStartListener = $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, options) {
        if (toState.name === 'cato.loggedIn.profile') {
            var targetState;
            _.each($scope.tabs, function (tab) {
                if (tab.show(tab)) {
                    targetState = tab.stateName;
                    return false;
                }
            });
            event.preventDefault();
            $state.go('cato.loggedIn.profile.' + targetState, {}, { reload: true });
        } else if (_.startsWith(toState.name, 'cato.loggedIn.profile.')) {
            $scope.activeTab = toState.name.split('.')[3];
        }
    });

    $scope.$on('$destroy', function () {
        stateChangeStartListener();
    });

    $scope.go = function (tab) {
        var targetState = 'cato.loggedIn.profile.' + tab.stateName;
        if ($state.includes(targetState)) {
            if ($state.is(targetState)) {
                // TODO: handle reloading a tab when clicking on the currently loaded tab, as long as the page isn't loading
            } else {
                // We are already in a child state of the target, don't do anything because the tab was likely activated while the page was still loading
                return;
            }
        }
        $state.go(targetState);
    };

    $scope.activeTab = $state.current.name.split('.')[3];
    if (typeof $scope.activeTab === 'undefined') {
        _.each($scope.tabs, function (tab) {
            if (tab.show(tab)) {
                $scope.activeTab = tab.stateName;
                return false;
            }
        });
    }
});
