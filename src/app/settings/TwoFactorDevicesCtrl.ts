import app from '../app';
import * as _ from 'lodash';

import '../common/twofactor/TwoFactorDeviceSetupCtrl';

app.controller('TwoFactorDevicesCtrl', function ($scope, $uibModal, modalService, Users) {
    $scope.data = {
        user: {
            id: $scope.user.id,
            toString: function () {
                return '';
            },
        },
    };
    $scope.gdata.users.$promise.then(function () {
        $scope.data.user = $scope.gdata.userMap[$scope.user.id];
    });

    $scope.loadUserDevices = function () {
        $scope.twofactor = Users.listDevices({ userId: $scope.data.user.id });
    };
    $scope.loadUserDevices();

    $scope.addDevice = function () {
        $uibModal
            .open({
                templateUrl: '/app/common/twofactor/modalTwoFactorDeviceSetup.html',
                controller: 'TwoFactorDeviceSetupCtrl',
            })
            .result.then(function (newdevice) {
                if ($scope.data.user.id === newdevice.user) {
                    $scope.twofactor.devices.push(newdevice);
                }
            });
    };

    $scope.deleteDevice = function (device) {
        modalService.openDelete('Delete Secondary Authentication Device?', device.name, device, function () {
            Users.deleteDevice(
                { deviceId: device.id, type: device.type },
                function () {
                    _.remove($scope.twofactor.devices, { id: device.id });
                },
                modalService.openErrorResponseCB('Unable to delete device')
            );
        });
    };
});
