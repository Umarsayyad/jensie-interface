import app from '../../app';
import * as _ from 'lodash';
import * as angular from 'angular';

app.controller('delPoCLoaderCtrl', function ($scope, $q, $timeout, $uibModal) {
    // TODO: edit the parent controllers to store the data immediately so we can just wait for the promises to resolve
    function openModal() {
        $uibModal.open({
            templateUrl: '/app/settings/poc/delPocModal.html',
            controller: 'DelPoCCtrl',
            size: 'lg',
            scope: $scope,
            resolve: {
                pocs: function () {
                    return $scope.data.pocs;
                },
            },
        });
    }

    function waitForParentDataLoad() {
        if ($scope.customer && $scope.data.pocs) {
            var promises = _.pull([$scope.customer.$promise, $scope.data.pocs.$promise], undefined);
            if (promises.length > 0) {
                $q.all(promises).then(openModal);
            } else {
                openModal();
            }
        } else {
            $timeout(waitForParentDataLoad, 100);
        }
    }

    waitForParentDataLoad();
});

app.controller('DelPoCCtrl', function (
    $scope,
    $state,
    $stateParams,
    $rootScope,
    modalService,
    PointsOfContact,
    Users,
    $uibModal,
    $uibModalInstance,
    pocs
) {
    $scope.modalTitle = 'Edit Point of Contact';

    if (_.isNil($scope.data)) {
        $scope.data = {};
    }
    $scope.config = {
        poc: {},
    };
    $scope.poc = {};
    $scope.pocId = parseInt($state.params.poc_id);
    console.log('pocId: ', $scope.pocId);

    //TODO: Leaving this object with minimal alterations FOR NOW, but it needs
    //to be rewritten so that it is NOT DYNAMICALY declared
    var origPoc: any = {};

    $scope.filterPocsByUser = function (poc) {
        if (_.isNil($scope.poc.user)) return true;
        if (origPoc.user === $scope.poc.user.id && origPoc.type === poc) return true;
        return $scope.poc.user.pocs.indexOf(poc) === -1;
    };

    $scope.filterUsersByType = function (user) {
        if (_.isNil($scope.poc.type)) return true;
        if (origPoc.user === user.id && origPoc.type === $scope.poc.type) return true;
        return user.pocs.indexOf($scope.poc.type) === -1;
    };

    function loadAllOptions() {
        PointsOfContact.options().$promise.then(function (results) {
            $scope.config.poc = results.actions.POST;

            var choices = [];

            angular.forEach($scope.config.poc.type.choices, function (choice) {
                choices.push(choice.value);
            });

            $scope.config.poc.type.choices = choices;
        });
    }

    function loadUsers() {
        PointsOfContact.get({ customerId: $scope.customer.id, pocId: $state.params.poc_id }).$promise.then(function (poc) {
            $scope.poc.user = $rootScope.gdata.userMap[poc.user];
            $scope.poc.type = poc.type;
            origPoc = poc;
        });

        $scope.data.usersSel = [];
        angular.forEach($rootScope.gdata.users, function (user) {
            $scope.data.usersSel.push({
                label: user.first_name + ' ' + user.last_name,
                id: user.id,
                pocs: user.pocs,
            });
        });
    }

    loadAllOptions();
    loadUsers();

    $scope.deletePoc = function () {
        PointsOfContact.delete({ customerId: $scope.customer.id, pocId: $scope.pocId }).$promise.then(function (results) {
            var poc: any = _.find(pocs, { id: $scope.pocId });
            if (poc) {
                _.pull($scope.gdata.userMap[poc.user].pocs, poc.type);
            }

            for (var i = 0; i < pocs.length; i++) {
                if (pocs[i].id === $scope.pocId) {
                    pocs.splice(i, 1);
                    break;
                }
            }

            $uibModalInstance.dismiss();
        }, modalService.openErrorResponseCB('Error deleting PoC'));
    };

    $scope.updatePoC = function () {
        var newpoc = {
            customer: $scope.customer.id,
            user: $scope.poc.user.id,
            type: $scope.poc.type,
        };

        PointsOfContact.update({ customerId: $scope.customer.id, pocId: $scope.pocId }, newpoc).$promise.then(function (results) {
            var oldPoc: any = _.find(pocs, { id: $scope.pocId });
            if (oldPoc) {
                _.pull($scope.gdata.userMap[oldPoc.user].pocs, oldPoc.type);
                $scope.gdata.userMap[oldPoc.user].pocs.push(results.type);
            }

            for (var i = 0; i < pocs.length; i++) {
                if (pocs[i].id === $scope.pocId) {
                    pocs[i] = results;
                    break;
                }
            }

            $uibModalInstance.dismiss();
        }, modalService.openErrorResponseCB('Error updating PoC'));
    };

    $scope.cancel = function () {
        $uibModalInstance.close();
    };

    $uibModalInstance.result.then(
        function () {
            $state.go('^');
        },
        function () {
            $state.go('^');
        }
    );
});
