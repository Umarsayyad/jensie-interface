import app from '../../app';
import * as _ from 'lodash';
import * as angular from 'angular';

app.controller('addPoCLoaderCtrl', function ($scope, $q, $timeout, $uibModal) {
    // TODO: edit the parent controllers to store the data immediately so we can just wait for the promises to resolve
    function openModal() {
        $uibModal.open({
            templateUrl: '/app/settings/poc/addPocModal.html',
            controller: 'AddPoCCtrl',
            size: 'lg',
            scope: $scope,
            resolve: {
                pocs: function () {
                    return $scope.data.pocs;
                },
            },
        });
    }

    function waitForParentDataLoad() {
        if ($scope.customer && $scope.data.pocs) {
            var promises = _.pull([$scope.customer.$promise, $scope.data.pocs.$promise], undefined);
            if (promises.length > 0) {
                $q.all(promises).then(openModal);
            } else {
                openModal();
            }
        } else {
            $timeout(waitForParentDataLoad, 100);
        }
    }

    waitForParentDataLoad();
});

app.controller('AddPoCCtrl', function (
    $scope,
    $rootScope,
    $state,
    $stateParams,
    $uibModal,
    $uibModalInstance,
    modalService,
    PointsOfContact,
    Users,
    pocs
) {
    $scope.modalTitle = 'ADD A NEW POC';

    if (_.isNil($scope.data)) {
        $scope.data = {};
    }

    $scope.config = {
        poc: {},
    };
    $scope.poc = {};

    $scope.filterPocsByUser = function (poc) {
        if (_.isNil($scope.poc.user)) return true;
        return $scope.poc.user.pocs.indexOf(poc.value) === -1;
    };

    $scope.filterUsersByType = function (user) {
        if (user.pocs.length === 1) return false;
        if (_.isNil($scope.poc.type)) return true;
        return user.pocs.indexOf($scope.poc.type.value) === -1;
    };

    function loadAllOptions() {
        PointsOfContact.options().$promise.then(function (results) {
            $scope.config.poc = results.actions.POST;
        });
    }

    function loadUsers() {
        $scope.data.usersSel = [];
        angular.forEach($rootScope.gdata.users, function (user) {
            $scope.data.usersSel.push({
                label: user.first_name + ' ' + user.last_name,
                id: user.id,
                pocs: user.pocs,
            });
        });
    }

    loadAllOptions();
    loadUsers();

    $scope.addOrUpdatePoC = function () {
        /* XXX: For now only support creating new ones this way. */
        if (_.isNil(pocs)) {
            pocs = [];
        }

        var newpoc = {
            customer: $scope.customer.id,
            user: $scope.poc.user.id,
            type: $scope.poc.type.value,
        };

        //console.log('newpoc: ' + JSON.stringify(newpoc, null, 2));

        PointsOfContact.create({ customerId: $scope.customer.id }, newpoc).$promise.then(function (results) {
            console.log('New PoC created (or later updated)');
            //console.log('data.pocs: ' + JSON.stringify(pocs, null, 2));
            $scope.data.pocs.unshift(results);
            $scope.gdata.userMap[results.user].pocs.push(results.type);

            $uibModalInstance.dismiss();
        }, modalService.openErrorResponseCB('Error creating PoC'));
    };

    $scope.cancel = function () {
        $uibModalInstance.close();
    };

    $uibModalInstance.result.then(
        function () {
            $state.go('^');
        },
        function () {
            $state.go('^');
        }
    );
});
