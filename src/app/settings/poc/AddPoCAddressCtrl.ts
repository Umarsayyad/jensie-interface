import app from '../../app';
import * as _ from 'lodash';
import * as angular from 'angular';
// TODO: added personal locations cannot be edited or deleted
app.controller('AddPoCAddressLoaderCtrl', function ($scope, $timeout, $q, $uibModal) {
    function openModal() {
        $uibModal.open({
            templateUrl: '/app/settings/poc/addPocAddressModal.html',
            controller: 'AddPoCAddressCtrl',
            size: 'lg',
            scope: $scope,
            resolve: {
                pocs: function () {
                    return $scope.data.pocs;
                },
                customer: function () {
                    return $scope.customer;
                },
            },
        });
    }

    function waitForParentDataLoad() {
        if ($scope.customer && $scope.data.pocs) {
            var promises = _.pull([$scope.customer.$promise, $scope.data.pocs.$promise], undefined);
            if (promises.length > 0) {
                $q.all(promises).then(openModal);
            } else {
                openModal();
            }
        } else {
            $timeout(waitForParentDataLoad, 100);
        }
    }

    waitForParentDataLoad();
});

app.controller('AddPoCAddressCtrl', function (
    $scope,
    $state,
    $uibModalInstance,
    formatFactory,
    modalService,
    Locations,
    PointOfContactAddresses,
    pocs,
    customer
) {
    $scope.config = {
        pocaddr: {},
    };
    $scope.pocaddr = {};
    $scope.createNew = true;
    $scope.locations = [];

    var pocId = parseInt($state.params.poc_id);
    var locId = parseInt($state.params.loc_id);

    $scope.locationsAlreadyUsed = [];
    if ($scope.selectedPOC) {
        $scope.locationsAlreadyUsed = $scope.selectedPOC.pointofcontactaddresses;
    } else {
        var curPoc: any = _.find(pocs, { id: pocId });
        $scope.locationsAlreadyUsed = curPoc.pointofcontactaddresses;
    }

    if (_.isNil($state.params.addr_id)) {
        if ($state.current.type === 'physical') {
            $scope.modalTitle = 'Add new Address for PoC';
            $scope.pocaddr.type = 'physical';
        } else {
            $scope.modalTitle = 'Add new Contact Information for PoC';
        }
        console.log('customer:', $scope.user.customer);
        $scope.modal = true;
    } else {
        $scope.modalTitle = 'Update Contact Information for PoC';

        $scope.addrId = parseInt($state.params.addr_id);
        $scope.locId = parseInt($state.params.loc_id);

        //console.log('addrId: ', $scope.addrId);
        //console.log('locId: ', $scope.locId);
        $scope.createNew = false;

        //console.log('pocs: ', pocs);
        angular.forEach(pocs, function (poc, k) {
            if (poc.id === pocId) {
                angular.forEach(poc.pointofcontactaddresses, function (address) {
                    if (address.id === $scope.addrId) {
                        $scope.pocaddr = angular.copy(address);
                        $scope.pocaddr.name = $scope.pocaddr.value;
                        console.log('$scope.pocaddr: ', $scope.pocaddr);
                        console.log('$scope.pocaddr: ', JSON.stringify($scope.pocaddr));
                    }
                });
            }
        });
    }

    function loadAllOptions() {
        PointOfContactAddresses.options().$promise.then(function (results) {
            $scope.config.pocaddr = results.actions.POST;

            var choices = [];

            /* Because when loading it on edit, even though it had track by value,, the thing it was loading
             * didn't have that object, only a value.
             */
            angular.forEach($scope.config.pocaddr.type.choices, function (choice) {
                choices.push(choice.value);
            });

            $scope.config.pocaddr.type.choices = choices;
        });
    }

    loadAllOptions();

    $scope.inputTypes = angular.copy(formatFactory);
    $scope.inputTypes.phonenumber = $scope.inputTypes.tel;
    var formats = {
        phonenumber: {
            inputType: 'tel',
            label: 'Phone number',
            placeholder: 'Please enter a telephone number.',
        },
        email: {
            inputType: 'email',
            label: 'Email',
            placeholder: 'Please enter an email address',
        },
    };
    $scope.updateObjOld($scope.inputTypes, formats);

    Locations.query({ customerId: $scope.user.customer }).$promise.then(function (locations) {
        $scope.locations = _.filter(locations, { personalLocation: true });
        if ($scope.locations.length === 0 || _.filter(locations, { id: locId }).length === 0) {
            $scope.locations.unshift({ personalLocation: true });
        }
        return $scope.locations;
    });

    $scope.addOrUpdateAddress = function (location, replace) {
        if ($scope.addrform.$invalid && !location) {
            return;
        }
        if (_.isNil(location)) {
            location = $scope.pocaddr;
        }
        var address = {
            poc: pocId,
            value: location.name,
            type: location.type,
            id: location.id,
            location: undefined,
        };
        if (location.type === 'physical') {
            address.location = location.id;
        }

        var poc: any = _.find(pocs, { id: pocId });
        var addressNotFound = _.filter(poc.pointofcontactaddresses, { id: address.id }).length === 0;

        var alreadyHadTypes = _.filter(poc.pointofcontactaddresses, { type: address.type });
        if (alreadyHadTypes.length > 0 && replace) {
            angular.forEach(alreadyHadTypes, function (alreadyHad: any) {
                if (alreadyHad.id !== location.id) $scope.deleteAddress(alreadyHad);
            });
        }

        if (poc.pointofcontactaddresses.length === 0 || addressNotFound) {
            // Updating locations is not currently supported.
            address['id'] = undefined;
            PointOfContactAddresses.create(
                { customerId: $scope.user.customer, pocId: pocId },
                address,
                function (results) {
                    //console.log('pocs: ', pocs);
                    angular.forEach(pocs, function (poc) {
                        //console.log('poc: ', poc);
                        if (poc.id === pocId) {
                            poc.pointofcontactaddresses.unshift(results);
                        }
                    });
                    $uibModalInstance.dismiss();
                },
                function (result) {
                    modalService.openErrorResponse('Point of contact could not be added.', result, false);
                }
            );
        } else {
            PointOfContactAddresses.update(
                {
                    customerId: $scope.user.customer,
                    pocId: pocId,
                    pocAddrId: address.id,
                },
                address,
                function (results) {
                    angular.forEach(pocs, function (poc) {
                        if (poc.id === pocId) {
                            angular.forEach(poc.pointofcontactaddresses, function (pocAddress) {
                                if (pocAddress.id === address.id) {
                                    $scope.updateObjOld(pocAddress, results);
                                }
                            });
                        }
                    });
                    $uibModalInstance.close();
                },
                function (result) {
                    modalService.openErrorResponse('Point of contact could not be updated.', result, false);
                }
            );
        }
    };

    $scope.deleteAddress = function (alreadyHad) {
        var addrId = $scope.addrId;
        if (alreadyHad) {
            addrId = alreadyHad.id;
        }
        PointOfContactAddresses.delete({ customerId: $scope.customer.id, pocId: pocId, pocAddrId: addrId }).$promise.then(function (results) {
            var poc: any = _.find(pocs, { id: pocId });
            _.remove(poc.pointofcontactaddresses, { id: addrId });
            $uibModalInstance.close();
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $uibModalInstance.result.then(
        function () {
            $state.go('^');
        },
        function () {
            $state.go('^');
        }
    );
});
