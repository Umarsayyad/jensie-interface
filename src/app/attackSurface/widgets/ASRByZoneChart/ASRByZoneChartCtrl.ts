import app from '../../../app';
import * as angular from 'angular';
import * as _ from 'lodash';
import * as d3 from 'd3';
import '../../AttackSurfaceUtils';

app.component('asrByZone', {
    bindings: {
        selectedAggregates: '<',
        selectedZoneSnapshotMap: '<',
        setPopulateAsrByZoneChartWidget: '<',
    },
    templateUrl: '/app/attackSurface/widgets/ASRByZoneChart/ASRByZoneChartTmpl.html',
    controller: function ASRByZoneChartCtrl($interval, $rootScope, $scope, $timeout, AttackSurfaceUtils) {
        var ctrl: any = {
            loadedObserver: undefined,
            observerConfig: undefined,
            asrByZoneChartData: [],
            asrByZoneChartOptions: {
                chart: {
                    type: 'multiBarChart',
                    margin: {
                        top: 20,
                        right: 20,
                        bottom: 45,
                        left: 45,
                    },
                    clipEdge: true,
                    //staggerLabels: true,
                    duration: 500,
                    stacked: true,
                    reduceXTicks: false,
                    x: function (d) {
                        return d['zone'];
                    },
                    y: function (d) {
                        return d['count'];
                    },
                    legend: {
                        margin: {
                            top: 3,
                            right: -10,
                            bottom: 0,
                            left: 0,
                        },
                        maxKeyLength: 200,
                    },
                    xAxis: {
                        // axisLabel: 'Time (ms)',
                        showMaxMin: false,
                        tickFormat: function (d) {
                            return d;
                        },
                    },
                    yAxis: {
                        // axisLabel: 'Y Axis',
                        axisLabelDistance: -20,
                        tickFormat: function (d) {
                            return d3.format(',')(d);
                        },
                    },
                },
            },
            $onInit: function () {
                if (!ctrl.selectedAggregates) {
                    return;
                }
                $rootScope.$broadcast('add-to-widgets', 'ASRByZoneChart', ctrl);
                $scope.$on('cleanup-widgets', function (event, delay) {
                    ctrl.api.updateWithTimeout(delay ? delay : 1000);
                });
                ctrl.setupMutationObserver();
            },
            $onChanges: function () {
                ctrl.waitForSVG();
            },
            callback: function (scope) {
                AttackSurfaceUtils.chartCallback(ctrl, scope);
            },
            populateASRByZoneChartWidget: function (aggregates) {
                // var sansTop10Ports = [];
                var unreviewed = [];
                var accepted = [];
                var excluded = [];
                var evaluating = [];
                var remediated = [];
                var remediationConfirmed = [];

                for (var zoneID in aggregates) {
                    if (!aggregates.hasOwnProperty(zoneID)) {
                        //The current property is not a direct property of ASRByZoneChartCtrl
                        continue;
                    }

                    var zone = aggregates[zoneID];
                    // // sansTop10Ports.push({zone:ctrl.selectedZoneSnapshotMap[zoneID],count:zone.sansTop10Ports});
                    unreviewed.push({ zone: ctrl.selectedZoneSnapshotMap[zoneID], count: zone.services.unreviewed });
                    accepted.push({ zone: ctrl.selectedZoneSnapshotMap[zoneID], count: zone.services.accepted });
                    excluded.push({ zone: ctrl.selectedZoneSnapshotMap[zoneID], count: zone.services.excluded });
                    evaluating.push({ zone: ctrl.selectedZoneSnapshotMap[zoneID], count: zone.services.evaluating });
                    remediated.push({ zone: ctrl.selectedZoneSnapshotMap[zoneID], count: zone.services.remediated });
                    remediationConfirmed.push({
                        zone: ctrl.selectedZoneSnapshotMap[zoneID],
                        count: zone.services['remediation confirmed'],
                    });
                }

                var datum = [
                    // {key:"sansTop10Ports", values:sansTop10Ports},
                    { key: AttackSurfaceUtils.titleCase('unreviewed'), values: unreviewed },
                    { key: AttackSurfaceUtils.titleCase('accepted'), values: accepted },
                    { key: AttackSurfaceUtils.titleCase('excluded'), values: excluded },
                    { key: AttackSurfaceUtils.titleCase('evaluating'), values: evaluating },
                    { key: AttackSurfaceUtils.titleCase('remediated'), values: remediated },
                    { key: AttackSurfaceUtils.titleCase('remediationConfirmed'), values: remediationConfirmed },
                ];

                return datum;
            },
            resize: function () {
                var timeouts = 0;
                var widget = angular.element('#asrByZoneChart')[0];
                if (widget) {
                    var originalX = widget.clientWidth;
                    var originalY = widget.clientHeight;
                    var oldX = widget.clientWidth;
                    var oldY = widget.clientHeight;
                    var helper = function () {
                        var currentX = widget.clientWidth;
                        var currentY = widget.clientHeight;
                        // console.log("oldX:" + oldX + ",oldY:" + oldY);
                        // console.log("originalX:" + originalX + ",originalY:" + originalY);
                        // console.log("currentX:" + currentX + ",currentY:" + currentY);
                        if ((currentX === oldX && currentX !== originalX) || (currentY === oldY && currentY !== originalY)) {
                            // $timeout.cancel(promise);
                            ctrl.api.update();
                            // $timeout(helper, 100);
                        } else if (timeouts++ < 100) {
                            oldX = widget.clientWidth;
                            oldY = widget.clientHeight;
                            $timeout(helper, 100);
                        }
                    };
                    var promise = $timeout(helper, 100);
                }
            },
            setupMutationObserver: function () {
                // Select the node that will be observed for mutations
                var observerNode = angular.element('#asrByZoneChart')[0];

                // Options for the observer (which mutations to observe)
                ctrl.observerConfig = { attributes: true, childList: true, subtree: true };

                var callback = function (mutationsList) {
                    // console.log("Mutation Observer callback running");
                    for (var i = 0; i < mutationsList.length; i++) {
                        // console.log("mutation loops:" + i);
                        var mutation = mutationsList[i];
                        if (mutation.type == 'childList') {
                            if (mutation.addedNodes[0] && mutation.addedNodes[0].tagName === 'svg') {
                                // This interval is necessary to prevent the data from being processed before
                                // the svg element has been prepared.
                                ctrl.waitForSVG(mutation.addedNodes[0]);
                                ctrl.loadedObserver.disconnect();
                            }
                        }
                    }
                };
                // Create an observer instance linked to the callback function
                ctrl.loadedObserver = new MutationObserver(callback);

                // Start observing the target node for configured mutations
                ctrl.loadedObserver.observe(observerNode, ctrl.observerConfig);
            },
            waitForSVG: function (svg) {
                if (!svg) {
                    svg = angular.element('asr-by-zone svg')[0];
                    if (!svg) {
                        return;
                    }
                }
                var promise = $interval(
                    function (svg) {
                        // console.log("intervalNumber: " + (++intervalNumber) + " and main loop at: " + mainLoopStatus);
                        if (ctrl.selectedAggregates && svg.getBoundingClientRect().height > 0) {
                            if (ctrl.selectedAggregates.post_scan_results) {
                                ctrl.asrByZoneChartData = ctrl.populateASRByZoneChartWidget(
                                    ctrl.selectedAggregates.post_scan_results.per_access_group
                                );
                            } else {
                                // current
                                ctrl.asrByZoneChartData = ctrl.populateASRByZoneChartWidget(ctrl.selectedAggregates.current.per_access_group);
                            }
                            $interval.cancel(promise);
                        }
                    },
                    10,
                    1000,
                    true,
                    svg
                );
            },
        };

        for (var prop in ctrl) {
            if (!ctrl.hasOwnProperty(prop)) {
                //The current property is not a direct property of ctrl
                continue;
            }
            this[prop] = ctrl[prop];
        }

        //Replace the initial ctrl object with the updated this object in order to prevent the two from getting out of sync
        ctrl = this;
    },
});
