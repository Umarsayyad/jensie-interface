import * as d3 from 'd3';

import app from '../../../app';
import { Domain } from '../../models/domain';
import { Nvd3Api } from '../../../common/widgets/nvd3-api';
import { ChartData } from './models/chart-data';

interface ApiChild {
    api: Nvd3Api;
} //an object containing an 'api' property of type Nvd3Api

class DomainsCtrl implements ng.IComponentController {
    private readonly chartReady$: Promise<ApiChild>;
    private chartResolve: (value: ApiChild) => void;

    //this is not guaranteed to be defined; you should prefer the Promise, `chartReady$`, instead.
    //it's included only for compatibility.
    public api = {} as Nvd3Api;
    private atkSurfaceDomains: Domain[];
    private domainChartData: ChartData[] = [];
    chartOptions = chartOptions;

    static inject$ = ['$rootScope', '$scope'];
    constructor(private $rootScope: ng.IRootScopeService, private $scope: ng.IScope) {
        this.chartReady$ = new Promise((resolve) => (this.chartResolve = resolve));
    }

    $onInit() {
        this.$rootScope.$broadcast('add-to-widgets', 'domainsChart', this);
        this.$scope.$on('cleanup-widgets', (event, delay = 1000) => {
            this.chartReady$.then((v) => v.api.updateWithTimeout(delay));
        });
    }

    $onChanges() {
        this.populateDomainData();
    }

    chartReadyCallback(value: ApiChild) {
        this.chartResolve(value);
        this.api = value.api;
    }

    private populateDomainData() {
        this.domainChartData = [];
        if (!this.atkSurfaceDomains) return;

        // Build the map
        let zoneMap: { [key: string]: number } = {};
        for (let domain of this.atkSurfaceDomains) {
            for (let ag of domain.accessGroups) {
                if (zoneMap[ag]) {
                    zoneMap[ag]++;
                } else {
                    zoneMap[ag] = 1;
                }
            }
        }

        // Build the chart data from data in the map
        let i = 0;
        for (let zone of Object.keys(zoneMap).sort()) {
            this.domainChartData.push({
                id: i,
                label: zone + ' (' + zoneMap[zone] + ')',
                labelKey: zoneMap[zone],
                className: 'label-low',
                value: zoneMap[zone],
            });

            i++;
        }
    }
}

const bindings = {
    atkSurfaceDomains: '<',
};

const DomainsChart: ng.IComponentOptions = {
    controller: DomainsCtrl,
    controllerAs: '$ctrl',
    templateUrl: '/app/attackSurface/widgets/domains-chart/domains.component.html',
    bindings,
};

app.component('domainsChart', DomainsChart);

var chartOptions =
    //forcing this to be hoisted using `var` for now
    {
        chart: {
            type: 'pieChart',
            donut: false,
            width: 400,
            x: (d: ChartData) => d.label,
            y: (d: ChartData) => d.value,
            margin: {
                top: 0,
                right: 0,
                bottom: 5,
                left: -5,
            },

            showLabels: false,
            valueFormat: function (d) {
                return d3.format(',')(d);
            },
            duration: 500,

            legendPosition: 'right',
            legend: {
                maxKeyLength: 200,
                margin: {
                    top: 10,
                    right: 10,
                    bottom: 5,
                    left: 10,
                },
            },
        },
    };
