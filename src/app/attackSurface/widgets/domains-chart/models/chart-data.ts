export class ChartData {
    id: number;
    label: string;
    labelKey: number;
    value: number;
    className: string;
    color?: string;
}
