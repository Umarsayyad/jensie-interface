import app from '../../../app';
import * as _ from 'lodash';
import * as d3 from 'd3';
import '../../AttackSurfaceUtils';

app.component('portsChart', {
    bindings: {
        selectedSummary: '<',
        selectedAttackZone: '<',
    },
    templateUrl: '/app/attackSurface/widgets/portsChart/portsChartTmpl.html',
    controller: function portsChartCtrl($rootScope, $scope, AttackSurfaceUtils) {
        var ctrl: any = {
            portsChartData: [],
            measured_date_from: undefined,
            measured_date_to: undefined,
            total_ports: 0,
            portsChartOptions: {
                chart: {
                    type: 'pieChart',
                    donut: false,
                    x: function (d) {
                        return d.label;
                    },
                    y: function (d) {
                        return d.v;
                    },
                    margin: {
                        top: 0,
                        right: 0,
                        bottom: 5,
                        left: -5,
                    },
                    width: 450,
                    showLabels: false,
                    valueFormat: function (d) {
                        return d3.format(',')(d);
                    },
                    duration: 500,
                    legend: {
                        margin: {
                            top: 10,
                            right: -10,
                            bottom: 5,
                            left: 0,
                        },
                        maxKeyLength: 200,
                    },
                    legendPosition: 'right',
                },
            },
            $onInit: function () {
                $rootScope.$broadcast('add-to-widgets', 'portsChart', ctrl);
                $scope.$on('cleanup-widgets', function (event, delay) {
                    ctrl.api.updateWithTimeout(delay ? delay : 1000);
                });
            },
            $onChanges: function () {
                ctrl.populatePortsChartWidget();
            },
            callback: function (scope) {
                AttackSurfaceUtils.chartCallback(ctrl, scope);
            },
            populatePortsChartWidget: function () {
                ctrl.portsChartData = [];
                if (!ctrl.selectedSummary || !ctrl.selectedAttackZone) {
                    return;
                }

                var servicesObj;
                var selectedAggregateResults;
                // Specific run or current results
                if (ctrl.selectedSummary.aggregate_results.post_scan_results) {
                    selectedAggregateResults = ctrl.selectedSummary.aggregate_results.post_scan_results;
                } else {
                    selectedAggregateResults = ctrl.selectedSummary.aggregate_results.current;
                }
                //  All access groups or a specific one
                if (ctrl.selectedAttackZone.id === -1) {
                    servicesObj = selectedAggregateResults.all_access_groups.services;
                    ctrl.total_ports = selectedAggregateResults.all_access_groups.services.total_ports;
                } else {
                    var zoneID = ctrl.selectedAttackZone.id;
                    servicesObj = selectedAggregateResults.per_access_group[zoneID].services;
                    ctrl.total_ports = selectedAggregateResults.per_access_group[zoneID].services.total_ports;
                }
                var statuses = [
                    { count: servicesObj.unreviewed, name: 'Unreviewed' },
                    { count: servicesObj.accepted, name: 'Accepted' },
                    { count: servicesObj.excluded, name: 'Excluded' },
                    { count: servicesObj.evaluating, name: 'Evaluating' },
                    { count: servicesObj.remediated, name: 'Remediated' },
                    { count: servicesObj['remediation confirmed'], name: 'Remediation Confirmed' },
                ];

                for (var cnt = 0; cnt < statuses.length; cnt++) {
                    var status = statuses[cnt];

                    ctrl.portsChartData.push({
                        id: cnt,
                        label: status.name + ' (' + status.count + ')',
                        labelKey: status.count,
                        className: 'label-low',
                        // color: "hsl(" + (angle + 45) + ", 100%, 50%)",
                        v: status.count,
                    });
                }
            },
            resize: function () {
                AttackSurfaceUtils.resizeWidget('portsChart');
            },
            titleCase: function (str) {
                str = str.replace(/[A-Z]/g, function (t) {
                    return ' ' + t.toUpperCase();
                });
                return str.replace(/(^[a-z])|(\s[A-Z])/g, function (t) {
                    return t.toUpperCase();
                });
            },
        };

        for (var prop in ctrl) {
            if (!ctrl.hasOwnProperty(prop)) {
                //The current property is not a direct property of ctrl
                continue;
            }
            this[prop] = ctrl[prop];
        }

        //Replace the initial ctrl object with the updated this object in order to prevent the two from getting out of sync
        ctrl = this;
    },
});
