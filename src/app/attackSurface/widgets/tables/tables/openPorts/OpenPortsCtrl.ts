import app from '../../../../../app';
import * as _ from 'lodash';
import '../../../../AttackSurfaceUtils';

app.component('openPortsTable', {
    bindings: {
        attackSurfaceData: '<',
        selectedRun: '<',
        curRowPriVal: '<',
        rowCount: '<',
        currentWidget: '<',
        activePill: '<',
        focusedDomain: '<?',
        focusedIp: '<?',
        clearFocusedDomain: '<?',
        exposedServices: '<',
        selectedRows: '<',
        bulkStatuses: '<',
        setBulkStatusTooltip: '<',
        setResponseWrapper: '<',
        bulkSetResponses: '&',
    },
    templateUrl: '/app/attackSurface/widgets/tables/tables/openPorts/openPortsTmpl.html',
    controller: function openPortsTablesCtrl($rootScope, $scope, NgTableParams, moment, AttackSurfaceUtils, ngTableEventsChannel, globalDataService) {
        var ctrl: any = {
            allRowsSelected: false,
            $rootScope: $rootScope,
            $scope: $scope,
            gdata: globalDataService,
            moment: moment,
            numberOfLinesPerRow: 20,
            currentCount: 5,
            currentStatuses: {},
            filteredSelectedRows: [],
            table: new NgTableParams(
                {
                    count: 40, // initial page size
                },
                {
                    // page size buttons (right set of buttons in demo)
                    counts: [5, 10, 20, 40],
                    // determines the pager buttons (left set of buttons in demo)
                    paginationMaxBlocks: 13,
                    paginationMinBlocks: 2,
                    dataset: [],
                    filterOptions: AttackSurfaceUtils.filterOptions,
                }
            ),
            options: {
                is_active: [
                    { id: true, title: 'Open' },
                    { id: false, title: 'Closed' },
                ],
            },
            portsStatuses: AttackSurfaceUtils.portsStatuses,
            $onInit: function () {
                ctrl.table.count(ctrl.rowCount());

                ctrl.setResponseWrapper(ctrl.setResponse);

                ctrl.bulkStatuses.length = 0; //Empty the array without replacing it.
                for (var cnt = 0; cnt < ctrl.portsStatuses.length; cnt++) {
                    ctrl.bulkStatuses.push(ctrl.portsStatuses[cnt]);
                }
                ctrl.setBulkStatusTooltip('Choose a status to apply to the selected ports');

                ngTableEventsChannel.onAfterDataFiltered(
                    function (publisher, data) {
                        ctrl.filteredSelectedRows = data;
                        ctrl.trackSelectionChanges();
                    },
                    ctrl.$scope,
                    ctrl.table
                );

                ctrl.unregisterExposedServicesWatcher = $scope.$watch('$ctrl.exposedServices.length', function (newValue, oldValue) {
                    ctrl.dataset = ctrl.exposedServices;
                    ctrl.table.settings({ dataset: ctrl.dataset });
                    ctrl.table.reload();
                });

                ctrl.unregisterSelectedRowsWatcher = $scope.$watch('$ctrl.selectedRows.length', ctrl.trackSelectionChanges);
                ctrl.unregisterResizeListener = AttackSurfaceUtils.setupResizeListener(ctrl, 'attackSurfaceTableWidget', 'port', {
                    activePill: ctrl.activePill,
                    widget: ctrl.currentWidget,
                });
            },
            $onChanges: function () {
                ctrl.dataset = ctrl.exposedServices;
                for (let cnt = 0; cnt < ctrl.dataset.length; cnt++) {
                    let exposedPort = ctrl.dataset[cnt];
                    ctrl.currentStatuses[ctrl.dataset[cnt].id] = exposedPort.status;
                }

                ctrl.table.settings({ dataset: ctrl.dataset });

                if (ctrl.focusedDomain) {
                    ctrl.table.filter()['domains'] = ctrl.focusedDomain;
                }
                if (ctrl.focusedIp) {
                    ctrl.table.filter()['ip'] = ctrl.focusedIp;
                }
                ctrl.table.count(ctrl.rowCount());
            },
            $onDestroy: function () {
                ctrl.rowCount(ctrl.table.count());

                ctrl.selectAllRows(false);
                ctrl.selectedRows.length = 0;

                if (ctrl.unregisterResizeListener) {
                    ctrl.unregisterResizeListener();
                }
                if (ctrl.unregisterSelectedRowsWatcher) {
                    ctrl.unregisterSelectedRowsWatcher();
                }
                if (ctrl.unregisterExposedServicesWatcher) {
                    ctrl.unregisterExposedServicesWatcher();
                }

                ctrl.clearFocusedDomain();
            },
            isString: function (input) {
                return typeof input === 'string';
            },
            checkCanBeSelected: function (row) {
                if (row.status === 'remediation confirmed') {
                    row.selected = false;
                }
            },
            selectAllRows: function (allRowsSelected) {
                AttackSurfaceUtils.selectAll(ctrl, allRowsSelected);
            },
            //Hooking into AttackSurfaceUtils.selectTooltip this way just to
            //make access from the html that much easier
            selectTooltip: function (status) {
                return AttackSurfaceUtils.selectTooltip(status);
            },
            setResponse: function (response, service) {
                var services = [];

                ctrl.rowCount(ctrl.table.count(), true);

                if (service) {
                    services.push(service);
                } else {
                    var selectedServices = ctrl.filteredSelectedRows.length ? ctrl.filteredSelectedRows : ctrl.exposedServices;
                    for (var cnt = 0; cnt < selectedServices.length; cnt++) {
                        if (selectedServices[cnt].selected && selectedServices[cnt].status !== 'remediation confirmed') {
                            services.push(selectedServices[cnt].id);
                        }
                    }
                }

                ctrl.bulkSetResponses({
                    $event: {
                        response: response.toLowerCase(),
                        services: services,
                        callback: () => {
                            for (const svcId of services) {
                                ctrl.currentStatuses[svcId] = response;
                            }

                            ctrl.selectAllRows(false);
                            ctrl.allRowsSelected = false;
                            ctrl.selectedRows.length = 0;
                        },
                    },
                });
            },
            trackSelectionChanges: function () {
                let totalSelectable = 0;
                let totalSelected = 0;

                ctrl.filteredSelectedRows.forEach((row) => {
                    if (row.status !== 'remediation confirmed') {
                        totalSelectable++;
                        if (row.selected) {
                            totalSelected++;
                        }
                    } else {
                        if (row.selected) {
                            row.selected = false; // Keep unselectable rows from being highlighted as selected
                        }
                    }
                });
                ctrl.allRowsSelected = totalSelected === totalSelectable;
                ctrl.setBulkStatusTooltip('Choose a status to apply to the ' + totalSelected + ' selected ports');
            },
            titleCase: AttackSurfaceUtils.titleCase,
            accessGroupTooltip: AttackSurfaceUtils.accessGroupTooltip,
        };

        for (var prop in ctrl) {
            if (!ctrl.hasOwnProperty(prop)) {
                //The current property is not a direct property of ctrl
                continue;
            }
            this[prop] = ctrl[prop];
        }

        //Replace the initial ctrl object with the updated this object in order to prevent the two from getting out of sync
        ctrl = this;
    },
});
