import app from '../../../../../app';
import * as _ from 'lodash';
import '../../../../AttackSurfaceUtils';

app.component('ownershipTable', {
    bindings: {
        widget: '<',
        attackSurfaceData: '<',
        selectedRows: '<',
        bulkStatuses: '<',
        setBulkStatusTooltip: '<',
        setResponseWrapper: '<',
        rowCount: '<',
    },
    templateUrl: '/app/attackSurface/widgets/tables/tables/ownership/ownershipTmpl.html',
    controller: function ownershipTableCtrl(
        $rootScope,
        $scope,
        NgTableParams,
        moment,
        globalDataService,
        modalService,
        AttackSurfaceUtils,
        AttackSurfaceDomain,
        ngTableEventsChannel
    ) {
        var ctrl: any = {
            allRowsSelected: false,
            gdata: globalDataService,
            $rootScope: $rootScope,
            $scope: $scope,
            moment: moment,
            numberOfLinesPerRow: 20,
            currentStatuses: {},
            filteredSelectedRows: [],
            table: new NgTableParams(
                {
                    count: 40, // initial page size
                },
                {
                    // page size buttons (right set of buttons in demo)
                    counts: [5, 10, 20, 40],
                    // determines the pager buttons (left set of buttons in demo)
                    paginationMaxBlocks: 13,
                    paginationMinBlocks: 2,
                    dataset: [],
                    filterOptions: AttackSurfaceUtils.filterOptions,
                }
            ),
            dataset: [],
            ownershipStatuses: ['unconfirmed', 'ownership not known', 'confirmed owned', 'confirmed not owned'],
            $onInit: function () {
                ctrl.setResponseWrapper(ctrl.setResponse);

                for (var cnt = 0; cnt < ctrl.dataset.length; cnt++) {
                    ctrl.currentStatuses[ctrl.dataset[cnt].id] = ctrl.dataset[cnt].status;
                }

                ctrl.bulkStatuses.length = 0; //Empty the array without replacing it.
                for (var cnt = 0; cnt < ctrl.ownershipStatuses.length; cnt++) {
                    ctrl.bulkStatuses.push(ctrl.ownershipStatuses[cnt]);
                }
                ctrl.setBulkStatusTooltip('Choose a status to apply to the selected hosts');

                ngTableEventsChannel.onAfterDataFiltered(
                    function (publisher, data) {
                        ctrl.filteredSelectedRows = data;
                        ctrl.trackSelectionChanges();
                    },
                    ctrl.$scope,
                    ctrl.table
                );

                $scope.$watch('$ctrl.cols', function (newValue, oldValue) {
                    if (newValue === oldValue) {
                        return;
                    }
                    for (var cnt = 0; cnt < ctrl.cols.length; cnt++) {
                        var col = ctrl.cols[cnt];
                        if (col.title() === 'Zones') {
                            col.show($rootScope.user.pocs.length > 0 || ctrl.gdata.isOperator);
                        }
                    }
                });

                $scope.$watch('$ctrl.selectedRows.length', ctrl.trackSelectionChanges);
                ctrl.unregisterResizeListener = AttackSurfaceUtils.setupResizeListener(ctrl, 'attackSurfaceTableWidget', 'host', {
                    activePill: ctrl.activePill,
                    widget: ctrl.currentWidget,
                });
            },
            $onChanges: function () {
                ctrl.dataset = ctrl.attackSurfaceData.domains;
                ctrl.table.settings({ dataset: ctrl.dataset });
                ctrl.table.count(ctrl.rowCount());
            },
            $onDestroy: function () {
                ctrl.rowCount(ctrl.table.count());
                ctrl.selectAllRows(false);
                ctrl.selectedRows.length = 0;

                if (ctrl.unregisterResizeListener) {
                    ctrl.unregisterResizeListener();
                }
            },
            isString: function (input) {
                return typeof input === 'string';
            },
            selectAllRows: function (allRowsSelected) {
                AttackSurfaceUtils.selectAll(ctrl, allRowsSelected);
            },
            setResponse: function (response, domain) {
                var domains = [];
                if (domain) {
                    domains.push(domain);
                } else {
                    var selectedRows = ctrl.filteredSelectedRows.length ? ctrl.filteredSelectedRows : ctrl.attackSurfaceData.domains;
                    for (var cnt = 0; cnt < selectedRows.length; cnt++) {
                        if (selectedRows[cnt].selected) {
                            domains.push(selectedRows[cnt].id);
                        }
                    }
                }
                ctrl.attackSurfaceData._updateResponse = AttackSurfaceDomain.bulkUpdate(
                    {},
                    {
                        status: response.toLowerCase(),
                        domains: domains,
                    },
                    function (rows) {
                        for (var cnt = 0; cnt < domains.length; cnt++) {
                            ctrl.currentStatuses[domains[cnt]] = response;
                        }
                        for (var cnt = 0; cnt < rows.length; cnt++) {
                            $rootScope.updateObj(ctrl.attackSurfaceData.domainMap[rows[cnt].id], rows[cnt]);
                        }
                    },
                    function (serverResponse) {
                        modalService.openErrorResponse('Unable to set domains statuses: ', serverResponse);
                    }
                );
                ctrl.selectAllRows(false);
                ctrl.allRowsSelected = false;
                ctrl.selectedRows.length = 0;
            },
            trackSelectionChanges: function () {
                let totalSelectable = ctrl.filteredSelectedRows.length;
                let totalSelected = 0;
                for (let i = 0; i < ctrl.filteredSelectedRows.length; i++) {
                    if (ctrl.filteredSelectedRows[i].selected) {
                        totalSelected++;
                    }
                }
                ctrl.allRowsSelected = totalSelected === totalSelectable;
                ctrl.setBulkStatusTooltip('Choose a status to apply to the ' + totalSelected + ' selected hosts');
            },
            titleCase: AttackSurfaceUtils.titleCase,
        };

        for (var prop in ctrl) {
            if (!ctrl.hasOwnProperty(prop)) {
                //The current property is not a direct property of ctrl
                continue;
            }
            this[prop] = ctrl[prop];
        }

        //Replace the initial ctrl object with the updated this object in order to prevent the two from getting out of sync
        ctrl = this;
    },
});
