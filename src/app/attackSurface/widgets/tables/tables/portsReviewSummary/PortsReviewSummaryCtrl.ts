import app from '../../../../../app';
import * as _ from 'lodash';
import '../../../../AttackSurfaceUtils';

app.component('portsReviewSummaryTable', {
    bindings: {
        activePill: '<',
        currentWidget: '<',
        attackSurfaceData: '<',
        ipToDnsMapping: '<',
        rowCount: '<',
    },
    templateUrl: '/app/attackSurface/widgets/tables/tables/portsReviewSummary/portsReviewSummaryTmpl.html',
    controller: function portsReviewSummaryTableCtrl($rootScope, $scope, NgTableParams, moment, AttackSurfaceUtils) {
        var ctrl: any = {
            $rootScope: $rootScope,
            $scope: $scope,
            dataset: undefined,
            moment: moment,
            numberOfLinesPerRow: 20,
            table: new NgTableParams(
                {
                    count: 40, // initial page size
                },
                {
                    // page size buttons (right set of buttons in demo)
                    counts: [5, 10, 20, 40],
                    // determines the pager buttons (left set of buttons in demo)
                    paginationMaxBlocks: 13,
                    paginationMinBlocks: 2,
                    dataset: [],
                    filterOptions: AttackSurfaceUtils.filterOptions,
                }
            ),
            $onInit: function () {
                AttackSurfaceUtils.setupResizeListener(ctrl, 'attackSurfaceTableWidget', 'index', {
                    activePill: ctrl.activePill,
                    widget: ctrl.currentWidget,
                });
            },
            $onChanges: function () {
                ctrl.dataset = ctrl.processData();
                ctrl.table.settings({ dataset: ctrl.dataset });
                ctrl.table.count(ctrl.rowCount());
            },
            $onDestroy: function () {
                ctrl.rowCount(ctrl.table.count());
                if (ctrl.unregisterResizeListener) {
                    ctrl.unregisterResizeListener();
                }
            },
            processData: function () {
                var processedData = [];

                if (!(ctrl.attackSurfaceData && ctrl.attackSurfaceData.ipaddresses)) {
                    return;
                }

                for (var cnt = 0; cnt < ctrl.attackSurfaceData.ipaddresses.length; cnt++) {
                    var ip = ctrl.attackSurfaceData.ipaddresses[cnt];
                    var row = {
                        ips: [ip.address],
                        domains: ctrl.ipToDnsMapping[ip.id].domains,
                        totalPorts: 0,
                        sansTop10Ports: 0,
                        excluded: 0,
                        unreviewed: 0,
                        evaluating: 0,
                        'remediation confirmed': 0,
                        remediated: 0,
                        accepted: 0,
                    };
                    _.forEach(ip.serviceObjs, function (serviceObj) {
                        var response = serviceObj.new_update__response ? serviceObj.new_update__response : serviceObj.latest_response__response;
                        row[response]++;
                        row.totalPorts++;
                        if (ctrl.attackSurfaceData.sans_top10[serviceObj.port]) {
                            row.sansTop10Ports++;
                        }
                    });
                    processedData.push(row);
                }

                return processedData;
            },
            isString: function (input) {
                return typeof input === 'string';
            },
            //Hooking into AttackSurfaceUtils.selectTooltip this way just to
            //make access from the html that much easier
            selectTooltip: function (status) {
                return AttackSurfaceUtils.selectTooltip(status);
            },
        };

        for (var prop in ctrl) {
            if (!ctrl.hasOwnProperty(prop)) {
                //The current property is not a direct property of ctrl
                continue;
            }
            this[prop] = ctrl[prop];
        }

        //Replace the initial ctrl object with the updated this object in order to prevent the two from getting out of sync
        ctrl = this;
    },
});
