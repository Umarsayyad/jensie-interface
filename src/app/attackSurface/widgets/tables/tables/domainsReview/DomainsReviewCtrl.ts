import app from '../../../../../app';
import * as _ from 'lodash';
import '../../../../AttackSurfaceUtils';

const NONE_ACTIVE = { port: 'None currently open' };
const NONE = { port: 'None' };
const MORE = { port: '...' };

app.component('domainsReviewTable', {
    bindings: {
        gotoTable: '<',
        currentWidget: '<',
        activePill: '<',
        attackSurfaceData: '<',
        rowCount: '<',
        selectedRows: '<',
        bulkStatuses: '<',
        setBulkStatusTooltip: '<',
        setResponseWrapper: '<',
        bulkSetResponses: '&',
    },
    templateUrl: '/app/attackSurface/widgets/tables/tables/domainsReview/domainsReviewTmpl.html',
    controller: function domainsReviewTableCtrl(
        $scope,
        $rootScope,
        globalDataService,
        NgTableParams,
        ngTableEventsChannel,
        moment,
        AttackSurfaceUtils
    ) {
        // var fakeDataSet = [{value:}];

        let ctrl = {
            allRowsSelected: false,
            $rootScope: $rootScope,
            $scope: $scope,
            gdata: globalDataService,
            moment: moment,
            numberOfLinesPerRow: 20,
            filteredSelectedRows: [],
            selectedServices: new Set(),
            portsStatuses: AttackSurfaceUtils.portsStatuses,
            table: new NgTableParams(
                {
                    count: 40, // initial page size
                },
                {
                    // page size buttons (right set of buttons in demo)
                    counts: [5, 10, 20, 40],
                    // determines the pager buttons (left set of buttons in demo)
                    paginationMaxBlocks: 13,
                    paginationMinBlocks: 2,
                    dataset: [],
                    filterOptions: AttackSurfaceUtils.filterOptions,
                }
            ),
            dataset: [],
            options: {
                serves_http: [
                    { id: true, title: 'Yes' },
                    { id: false, title: 'No' },
                ],
                is_http_redir: [
                    { id: true, title: 'Yes' },
                    { id: false, title: 'No' },
                ],
            },
            cols: undefined,
            unregisterResizeListener: undefined,
            unregisterSelectedRowsWatcher: undefined,

            $onInit: function () {
                $scope.$watch('$ctrl.cols', function (newValue, oldValue) {
                    if (newValue === oldValue) {
                        return;
                    }
                    for (var cnt = 0; cnt < ctrl.cols.length; cnt++) {
                        var col = ctrl.cols[cnt];
                        if (col.title() === 'Zones') {
                            col.show($rootScope.user.pocs.length > 0 || ctrl.gdata.isOperator);
                        }
                    }
                });

                ctrl.unregisterResizeListener = AttackSurfaceUtils.setupResizeListener(ctrl, 'attackSurfaceTableWidget', 'domain', {
                    activePill: ctrl.activePill,
                    widget: ctrl.currentWidget,
                });

                ctrl.setResponseWrapper(ctrl.setResponse);

                // Replace the contents of the bulkStatuses array
                ctrl.bulkStatuses.splice(0, ctrl.bulkStatuses.length, ...ctrl.portsStatuses);
                ctrl.setBulkStatusTooltip('Choose a status to apply to the ports on the selected IP addresses');

                ngTableEventsChannel.onAfterDataFiltered(
                    function (publisher, data) {
                        ctrl.filteredSelectedRows = data;
                        ctrl.trackSelectionChanges();
                    },
                    ctrl.$scope,
                    ctrl.table
                );

                ctrl.unregisterSelectedRowsWatcher = $scope.$watch('$ctrl.selectedRows.length', ctrl.trackSelectionChanges);
            },
            $onChanges: function () {
                ctrl.dataset = ctrl.processToRows(ctrl.attackSurfaceData);
                ctrl.table.settings({ dataset: ctrl.dataset });
                ctrl.table.count(ctrl.rowCount());
            },
            $onDestroy: function () {
                ctrl.rowCount(ctrl.table.count());
                if (ctrl.unregisterResizeListener) {
                    ctrl.unregisterResizeListener();
                }

                ctrl.selectAllRows(false);
                ctrl.selectedRows.length = 0;

                if (ctrl.unregisterSelectedRowsWatcher) {
                    ctrl.unregisterSelectedRowsWatcher();
                }
            },
            processToRows: function (data) {
                const rows = [];
                _.each(data.domains, function (domain) {
                    if (domain.ipObjs.length) {
                        _.each(domain.ipObjs, function (ip) {
                            rows.push({
                                value: domain.value,
                                accessGroups: domain.accessGroups,
                                registrar: domain.registrar,
                                serves_http: domain.serves_http,
                                is_http_redir: domain.is_http_redir,
                                ipObj: ip,
                                domain: domain,
                            });
                        });
                    } else {
                        rows.push({
                            value: domain.value,
                            accessGroups: domain.accessGroups,
                            registrar: domain.registrar,
                            serves_http: domain.serves_http,
                            is_http_redir: domain.is_http_redir,
                            domain: domain,
                        });
                    }
                });
                return rows;
            },
            firstTenPorts: function (ipObj) {
                if (!ipObj || _.size(ipObj.serviceObjs) === 0) {
                    return [NONE];
                }
                const included = [];
                let moreThan10 = false;

                const sortedServices = _.sortBy(ipObj.serviceObjs, ['port']);

                for (const svc of sortedServices) {
                    if (!svc.active) {
                        continue;
                    }
                    if (included.length < 10) {
                        included.push(svc);
                    } else {
                        moreThan10 = true;
                        break;
                    }
                }

                if (included.length === 0) {
                    included.push(NONE_ACTIVE);
                }
                if (moreThan10) {
                    included.push(MORE);
                }
                return included;
            },
            generatePortTooltip: function (svc) {
                if (svc.id) {
                    return (
                        (svc.active ? 'Open' : 'Closed') +
                        '<br>' +
                        AttackSurfaceUtils.titleCase(
                            svc.latest_response__response || `${svc.previous_update__response} => ${svc.new_update__response}`
                        )
                    );
                } else {
                    return '';
                }
            },
            checkCanBeSelected: function (row) {
                if (!row.ipObj) {
                    row.selected = false;
                }
            },
            selectAllRows: function (allRowsSelected) {
                AttackSurfaceUtils.selectAll(ctrl, allRowsSelected);
            },
            setResponse: function (response) {
                ctrl.rowCount(ctrl.table.count(), true);

                ctrl.bulkSetResponses({
                    $event: {
                        response: response.toLowerCase(),
                        services: Array.from(ctrl.selectedServices),
                        callback: () => {
                            ctrl.selectAllRows(false);
                            ctrl.allRowsSelected = false;
                            ctrl.selectedRows.length = 0;
                        },
                    },
                });
            },
            storeServices: function (serviceObjs) {
                for (const svc of serviceObjs) {
                    if (svc.latest_response__response !== 'remediation confirmed') {
                        ctrl.selectedServices.add(svc.id);
                    }
                }
            },
            trackSelectionChanges: function () {
                let totalSelectable = 0;
                let totalSelected = 0;

                ctrl.selectedServices.clear();
                const ipSet = new Set();
                const domainSet = new Set();
                for (const row of ctrl.filteredSelectedRows) {
                    if (row.ipObj) {
                        totalSelectable++;
                        if (row.selected) {
                            totalSelected++;
                            ctrl.storeServices(row.ipObj.serviceObjs);
                            ipSet.add(row.ipObj.id);
                            domainSet.add(row.domain.id);
                        }
                    } else {
                        if (row.selected) {
                            row.selected = false; // Keep unselectable rows from being highlighted as selected
                        }
                    }
                }

                ctrl.allRowsSelected = totalSelected === totalSelectable;
                ctrl.setBulkStatusTooltip(
                    `Choose a status to apply to the ${ctrl.selectedServices.size} ports of the ${domainSet.size} selected domains' ${ipSet.size} IP addresses (in ${totalSelected} rows)`
                );
            },
            accessGroupTooltip: AttackSurfaceUtils.accessGroupTooltip,
            // bindings, to make TypeScript happy without converting everything to classes and interfaces
            gotoTable: undefined,
            currentWidget: undefined,
            activePill: undefined,
            attackSurfaceData: undefined,
            rowCount: undefined,
            selectedRows: undefined,
            bulkStatuses: undefined,
            setBulkStatusTooltip: undefined,
            setResponseWrapper: undefined,
            bulkSetResponses: undefined,
        };

        for (const prop of Object.keys(ctrl)) {
            this[prop] = ctrl[prop];
        }

        //Replace the initial ctrl object with the updated this object in order to prevent the two from getting out of sync
        ctrl = this;
    },
});
