import app from '../../../app';
import * as angular from 'angular';
import '../../AttackSurfaceUtils';

app.component('setBulkStatus', {
    bindings: {
        selectedRows: '<',
        setResponse: '<',
        statuses: '<',
        tooltip: '<',
    },
    templateUrl: '/app/attackSurface/widgets/tables/bulkStatusTmpl.html',
    controller: function BulkStatus($scope, AttackSurfaceUtils) {
        var ctrl: any = {
            currentStatus: 'choose an option',
            shownStatuses: [],
            $onInit: function () {
                ctrl.unregisterStatusesWatcher = $scope.$watch('$ctrl.statuses.length', function (newValue, oldValue) {
                    ctrl.shownStatuses = angular.copy(ctrl.statuses);
                    ctrl.shownStatuses.unshift('choose an option');
                });
            },
            titleCase: AttackSurfaceUtils.titleCase,
        };

        for (var prop in ctrl) {
            if (!ctrl.hasOwnProperty(prop)) {
                //The current property is not a direct property of ctrl
                continue;
            }
            this[prop] = ctrl[prop];
        }

        //Replace the initial ctrl object with the updated this object in order to prevent the two from getting out of sync
        ctrl = this;
    },
});
