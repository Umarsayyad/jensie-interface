import app from '../../../app';

import './tables/portsReviewSummary/PortsReviewSummaryCtrl';
import './tables/domainsReview/DomainsReviewCtrl';
import './tables/openPorts/OpenPortsCtrl';
import './tables/ownership/OwnershipCtrl';
import './bulkStatusCtrl';
import '../../AttackSurfaceUtils';

app.component('tables', {
    bindings: {
        currentTable: '<',
        setCurrentTable: '<',
        tableNames: '<',
        showTable: '<',
        activePill: '<',
        recordSizes: '<',
        currentWidget: '<',
        rowCount: '<',
        attackSurfaceData: '<',
        currentAttackSurfaceData: '<',
        widget: '<',
        exposedServices: '<',
        selectedAggregates: '<',
        attackZones: '<',
        ipToDnsMapping: '<',
        aggregateCurrentResults: '<',
        bulkSetResponses: '&',
        selectedRun: '<',
    },
    templateUrl: '/app/attackSurface/widgets/tables/tablesTmpl.html',
    controller: function TablesCtrl($rootScope, AttackSurfaceUtils, globalDataService) {
        var ctrl: any = {
            gdata: globalDataService,
            focusedDomain: undefined,
            focusedIp: undefined,
            curRowPriVal: 0,
            currentTableIndex: 0,
            selectedRows: [],
            selectedBulkStatus: undefined,
            setResponse: undefined,
            bulkStatuses: [],
            bulkStatusTooltip: undefined,
            tableSwitchedUnsubscribe: undefined,
            $onInit: function () {
                //Used to set currentTableIndex, DO NOT REMOVE
                for (ctrl.currentTableIndex = 0; ctrl.currentTableIndex < ctrl.tableNames.length; ctrl.currentTableIndex++) {
                    if (ctrl.tableNames[ctrl.currentTableIndex] === ctrl.currentTable) {
                        break;
                    }
                }

                ctrl.tableSwitchedUnsubscribe = $rootScope.$on('table-switched', function () {
                    AttackSurfaceUtils.calculateTableHeights();
                });
            },
            $onDestroy: function () {
                if (ctrl.tableSwitchedUnsubscribe) {
                    ctrl.tableSwitchedUnsubscribe();
                }
            },
            clearFocusedDomain: function () {
                ctrl.focusedDomain = undefined;
                ctrl.focusedIp = undefined;
                ctrl.curRowPriVal = undefined;
            },
            gotoTable: function (tableNameOrDirection, curRowPriVal, focusedDomain, focusedIp) {
                ctrl.recordSizes(ctrl.widget);
                ctrl.curRowPriVal = curRowPriVal;
                ctrl.focusedDomain = focusedDomain;
                ctrl.focusedIp = focusedIp;

                ctrl.selectedRows.length = 0;
                ctrl.bulkStatuses.length = 0;

                ctrl.currentTableIndex = ctrl.tableNames.indexOf(ctrl.currentTable);

                if (tableNameOrDirection === 'next') {
                    ctrl.currentTableIndex = ++ctrl.currentTableIndex % ctrl.tableNames.length;
                    ctrl.setCurrentTable(ctrl.tableNames[ctrl.currentTableIndex]);
                    ctrl.currentTable = ctrl.tableNames[ctrl.currentTableIndex];
                } else if (tableNameOrDirection === 'previous') {
                    ctrl.currentTableIndex = --ctrl.currentTableIndex >= 0 ? ctrl.currentTableIndex : ctrl.tableNames.length - 1;
                    ctrl.setCurrentTable(ctrl.tableNames[ctrl.currentTableIndex]);
                    ctrl.currentTable = ctrl.tableNames[ctrl.currentTableIndex];
                } else {
                    ctrl.setCurrentTable(tableNameOrDirection);
                    ctrl.currentTable = tableNameOrDirection;
                    for (ctrl.currentTableIndex = 0; ctrl.currentTableIndex < ctrl.tableNames.length; ctrl.currentTableIndex++) {
                        if (ctrl.tableNames[ctrl.currentTableIndex] === ctrl.currentTable) {
                            break;
                        }
                    }
                }

                if (ctrl.activePill !== 0 || curRowPriVal || focusedDomain || focusedIp) {
                    ctrl.showTable(ctrl.currentTable);
                }
            },
            setResponseWrapper: function (setResponse) {
                if (typeof setResponse === 'function') {
                    ctrl.setResponse = setResponse;
                }
            },
            setBulkStatusTooltip: function (tooltip) {
                if (typeof tooltip === 'string') {
                    ctrl.bulkStatusTooltip = tooltip;
                }
            },
        };

        for (var prop in ctrl) {
            if (!ctrl.hasOwnProperty(prop)) {
                //The current property is not a direct property of ctrl
                continue;
            }
            this[prop] = ctrl[prop];
        }

        //Replace the initial ctrl object with the updated this object in order to prevent the two from getting out of sync
        ctrl = this;
    },
});
