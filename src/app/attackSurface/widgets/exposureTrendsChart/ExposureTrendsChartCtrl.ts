import app from '../../../app';
import * as angular from 'angular';
import * as _ from 'lodash';
import * as d3 from 'd3';
import '../../AttackSurfaceUtils';

const EMPTY_SERVICE_COUNTS = {
    excluded: 0,
    total_sans_top10_ports: 0,
    total_ports: 0,
    unreviewed: 0,
    evaluating: 0,
    active_ports: 0,
    'remediation confirmed': 0,
    remediated: 0,
    accepted: 0,
};

app.component('exposureTrendsChart', {
    bindings: {
        selectedAttackZone: '<',
        attackSurfaceSummaryData: '<',
        setPopulateExposureTrendsChartWidget: '<',
    },
    templateUrl: '/app/attackSurface/widgets/exposureTrendsChart/exposureTrendsChartTmpl.html',
    controller: function exposureTrendsChartCtrl($interval, $rootScope, $scope, moment, AttackSurfaceUtils) {
        var ctrl: any = {
            exposureTrendsChartData: [],
            measuredDateFrom: undefined,
            measuredDateTo: undefined,
            loadedObserver: undefined,
            observerConfig: undefined,
            fromMinDate: undefined,
            fromMaxDate: undefined,
            toMinDate: undefined,
            toMaxDate: undefined,
            lastTimestamp: '',
            exposureTrendsChartOptions: {
                chart: {
                    type: 'stackedAreaChart',
                    x: function (d) {
                        return d[0];
                    },
                    y: function (d) {
                        return d[1];
                    },
                    useVoronoi: false,
                    clipEdge: true,
                    duration: 100,
                    useInteractiveGuideline: true,
                    xAxis: {
                        showMaxMin: false,
                        tickFormat: function (d) {
                            return d3['time'].format('%x')(new Date(d));
                        },
                    },
                    yAxis: {
                        tickFormat: function (d) {
                            return d3.format(',d')(d);
                        },
                    },
                    showControls: false,
                    legend: {
                        align: true,
                        maxKeyLength: 400,
                    },
                    zoom: {
                        enabled: false,
                        scaleExtent: [1, 10],
                        useFixedDomain: true,
                        useNiceScale: true,
                        horizontalOff: true,
                        verticalOff: true,
                        unzoomEventType: 'dblclick.zoom',
                    },
                },
            },
            $onInit: function () {
                ctrl.setupMutationObserver();
                $rootScope.$broadcast('add-to-widgets', 'exposureTrendsChart', ctrl);

                $scope.$on('cleanup-widgets', function (event, delay) {
                    ctrl.api.updateWithTimeout(delay ? delay : 1000);
                });
            },
            $onChanges: function () {
                if (!ctrl.attackSurfaceSummaryData || ctrl.attackSurfaceSummaryData.length === 0) {
                    return;
                }

                var startDateObj = moment(ctrl.attackSurfaceSummaryData[ctrl.attackSurfaceSummaryData.length - 1].start_timestamp).toDate();
                var latestRun = ctrl.attackSurfaceSummaryData[0];
                var endDateObj = moment(latestRun.timestamp || latestRun.stop_timestamp)
                    .add(1, 'month')
                    .toDate();

                ctrl.toMinDate = ctrl.fromMinDate = ctrl.measuredDateFrom = startDateObj;
                ctrl.fromMaxDate = ctrl.toMaxDate = ctrl.measuredDateTo = endDateObj;
                ctrl.lastTimestamp = latestRun.timestamp;

                ctrl.populateExposureTrendsChartWidget();
            },
            $doCheck: function () {
                if (ctrl.lastTimestamp && ctrl.attackSurfaceSummaryData && ctrl.attackSurfaceSummaryData.length > 0) {
                    const timestamp = ctrl.attackSurfaceSummaryData[0].timestamp;
                    if (timestamp && timestamp !== ctrl.lastTimestamp) {
                        ctrl.$onChanges();
                    }
                }
            },
            callback: function (scope) {
                AttackSurfaceUtils.chartCallback(ctrl, scope);
            },
            populateExposureTrendsChartWidget: function () {
                if (!ctrl.attackSurfaceSummaryData || ctrl.attackSurfaceSummaryData.length === 0 || !ctrl.selectedAttackZone) {
                    return;
                }

                var unreviewed = [];
                var accepted = [];
                var excluded = [];
                var evaluating = [];
                var remediated = [];
                var remediationConfirmed = [];

                var zoneID, currentServices, preServices, postServices;

                function storePoints(timestamp: number, services: any) {
                    unreviewed.push([timestamp, services.unreviewed]);
                    accepted.push([timestamp, services.accepted]);
                    excluded.push([timestamp, services.excluded]);
                    evaluating.push([timestamp, services.evaluating]);
                    remediated.push([timestamp, services.remediated]);
                    remediationConfirmed.push([timestamp, services['remediation confirmed']]);
                }

                for (var cnt = ctrl.attackSurfaceSummaryData.length - 1; cnt >= 0; cnt--) {
                    var run = ctrl.attackSurfaceSummaryData[cnt];
                    if (run.aggregate_results.current) {
                        var timestamp = new Date(run.timestamp).getTime();
                        if (timestamp > ctrl.measuredDateTo) {
                            continue;
                        }
                        if (ctrl.selectedAttackZone.id === -1) {
                            currentServices = run.aggregate_results.current.all_access_groups.services;
                        } else {
                            zoneID = ctrl.selectedAttackZone.id;
                            if (run.aggregate_results.current.per_access_group[zoneID]) {
                                currentServices = run.aggregate_results.current.per_access_group[zoneID].services;
                            } else {
                                // access group `zoneID` doesn't exist any more, just store 0s
                                currentServices = EMPTY_SERVICE_COUNTS;
                            }
                        }
                        storePoints(timestamp, currentServices);
                    } else {
                        var start_timestamp = new Date(run.start_timestamp).getTime();
                        var stop_timestamp = new Date(run.stop_timestamp).getTime();
                        if (stop_timestamp < start_timestamp || start_timestamp < ctrl.measuredDateFrom || stop_timestamp > ctrl.measuredDateTo) {
                            continue;
                        }

                        if (ctrl.selectedAttackZone && ctrl.selectedAttackZone.id === -1) {
                            preServices = run.aggregate_results.pre_scan_results.all_access_groups.services;
                            postServices = run.aggregate_results.post_scan_results.all_access_groups.services;
                        } else if (ctrl.selectedAttackZone && ctrl.selectedAttackZone.id !== undefined) {
                            zoneID = ctrl.selectedAttackZone.id;
                            if (run.aggregate_results.pre_scan_results.per_access_group[zoneID]) {
                                preServices = run.aggregate_results.pre_scan_results.per_access_group[zoneID].services;
                                postServices = run.aggregate_results.post_scan_results.per_access_group[zoneID].services;
                            } else {
                                // access group `zoneID` didn't exist for this run, just store 0s
                                preServices = EMPTY_SERVICE_COUNTS;
                                postServices = EMPTY_SERVICE_COUNTS;
                            }
                        }
                        storePoints(start_timestamp, preServices);
                        storePoints(stop_timestamp, postServices);
                    }
                }
                ctrl.exposureTrendsChartData = [
                    { key: 'Unreviewed', values: unreviewed },
                    { key: 'Accepted', values: accepted },
                    { key: 'Excluded', values: excluded },
                    { key: 'Evaluating', values: evaluating },
                    { key: 'Remediated', values: remediated },
                    { key: 'Remediation Confirmed', values: remediationConfirmed },
                ];
            },
            resize: function (wait) {
                if (wait) {
                    AttackSurfaceUtils.resizeWidget('exposureTrendsChart', ctrl.api.update);
                } else {
                    ctrl.api.update();
                }
            },
            setupMutationObserver: function () {
                // Select the node that will be observed for mutations
                var observerNode = angular.element('#exposureTrendsChart')[0];

                // Options for the observer (which mutations to observe)
                ctrl.observerConfig = { attributes: true, childList: true, subtree: true };

                var callback = function (mutationsList) {
                    // console.log("Mutation Observer callback running");
                    for (var i = 0; i < mutationsList.length; i++) {
                        // console.log("mutation loops:" + i);
                        var mutation = mutationsList[i];
                        if (mutation.type == 'childList') {
                            if (mutation.addedNodes[0] && mutation.addedNodes[0].tagName === 'svg') {
                                // This interval is necessary to prevent the data from being processed before
                                // the svg element has been prepared.
                                var promise = $interval(
                                    function (svg) {
                                        // console.log("intervalNumber: " + (++intervalNumber) + " and main loop at: " + mainLoopStatus);
                                        if (svg.getBoundingClientRect().height > 0) {
                                            ctrl.populateExposureTrendsChartWidget();
                                            $interval.cancel(promise);
                                        }
                                    },
                                    10,
                                    100,
                                    true,
                                    mutation.addedNodes[0]
                                );
                                ctrl.loadedObserver.disconnect();
                            }
                        }
                    }
                };
                // Create an observer instance linked to the callback function
                ctrl.loadedObserver = new MutationObserver(callback);

                // Start observing the target node for configured mutations
                ctrl.loadedObserver.observe(observerNode, ctrl.observerConfig);
            },
            updateProp: function (prop, val) {
                ctrl[prop] = val;

                ctrl.toMinDate = ctrl.measuredDateFrom;
                ctrl.fromMaxDate = ctrl.measuredDateTo;

                if (ctrl.measuredDateFrom.getTime() > ctrl.measuredDateTo.getTime()) {
                    ctrl.measuredDateTo = ctrl.measuredDateFrom;
                }

                ctrl.populateExposureTrendsChartWidget();
            },
        };

        for (var prop in ctrl) {
            if (!ctrl.hasOwnProperty(prop)) {
                //The current property is not a direct property of ctrl
                continue;
            }
            this[prop] = ctrl[prop];
        }

        //Replace the initial ctrl object with the updated this object in order to prevent the two from getting out of sync
        ctrl = this;
    },
});
