import app from '../app';
import * as angular from 'angular';
import * as _ from 'lodash';
import { WidgetParams } from './models/widget-params';

import './datePopup/datePopupComp';
import './widgets/portsChart/portsChartCtrl';
import './widgets/domains-chart/domains.component';
import './widgets/exposureTrendsChart/ExposureTrendsChartCtrl';
import './widgets/ASRByZoneChart/ASRByZoneChartCtrl';
import './widgets/tables/TablesCtrl';
import './AttackSurfaceUtils';

app.component('attackSurface', {
    templateUrl: '/app/attackSurface/attackSurface.html',
    controller: function attackSurfaceCtrl(
        $rootScope,
        $scope,
        $filter,
        $timeout,
        globalDataService,
        modalService,
        Users,
        moment,
        AttackSurfaceUtils,
        AttackSurfaceRun,
        AccessGroups,
        ResponseUpdate
    ) {
        //Since this is a new controller, I'm using this style of code organization in order to make the
        //code cleaner and easier to follow.
        //The idea is to build one large object, setting all of the properties in order and then assign all
        //of the references to the this object in one pass, after which the ctrl object gets replaced by the
        //this object so that the end result turns out the same product.

        var ctrl = {
            catotheme: $rootScope.catotheme,
            $rootScope: $rootScope,
            attackZones: undefined,
            selectedAttackZone: { id: -1 },
            gdata: globalDataService,
            service: {},
            data: {
                services: {},
                findings: [],
                rows: [],
                showSelectCustomer: false,
            },
            runs: [{ id: -1, display: 'Current data' }],
            attackSurfaceData: undefined,
            attackSurfaceSummaryData: undefined,
            attackSurfaceSummaryMap: undefined,
            attackSurfaceDataMap: undefined,
            currentAttackSurfaceData: undefined,
            selectedZoneSnapshotMap: undefined,
            exposedServices: undefined,
            exposedPortsMap: {},

            ipToDnsMapping: {},
            dnsTopIpMapping: {},
            customer: undefined,
            currentRunResponse: undefined,
            previousRunResponse: undefined,
            currentRunSummary: undefined,
            runListResponse: undefined,
            accessGroupsResponse: undefined,
            updateResponse: undefined,
            selectedRun: { id: -1, display: 'Current data' },
            selectedSummary: undefined,
            selectedAggregates: undefined,
            selectedAccessGroupSnapshot: {},
            //At the moment each pill is tableID + 1, this array allows that mapping to be different
            tablesAndChartsToPills: {
                'PORTS REVIEW SUMMARY': 1,
                'DOMAINS REVIEW': 2,
                OWNERSHIP: 3,
                'ALL OPEN PORTS': 4,
                // "PORTS": 6,
                'EXPOSURE TRENDS': 5,
                'ASR BY ZONE': 6,
            },
            currentWidget: undefined,
            tablesWidget: undefined,
            exposureTrendsWidget: undefined,
            asrByZoneWidget: undefined,
            portsChartWidget: undefined,
            widgets: [],
            widgetControllers: {},
            widgetIndexes: {},
            status: {},

            activePill: 0,
            showArchived: false,
            currentTable: 'PORTS REVIEW SUMMARY',
            tables: [
                'PORTS REVIEW SUMMARY',
                'DOMAINS REVIEW',
                'OWNERSHIP',
                'ALL OPEN PORTS',
                //Reminder, the CHARTS below are NOT tables, don't include them!
                // "ASR BY ZONE",
                // "PORTS",
                // "EXPOSURE TRENDS"
            ],
            currentReport: undefined,
            operationsConfig: undefined,
            unregisterSelectedCustomerWatch: undefined,

            defaultRowCount: 40,

            gridsterOpts: {
                columns: 8,
                pushing: true,
                floating: true,
                swapping: false,
                width: 'auto',
                colWidth: 'auto',
                rowHeight: 133,
                margins: [25, 25],
                outerMargin: false,
                defaultSizeX: 1,
                defaultSizeY: 1,
                minColumns: 1,
                minRows: 2,
                //mobileBreakPoint: 800,
                mobileModeEnabled: false,
                draggable: {
                    enabled: true,
                    handle: '.gridster-item-drag',
                    stop: function (event, $element, widget) {
                        var ui_data = $rootScope.user.ui_data || {};
                        ui_data['attack_surface_widgets'] = ctrl.widgets;
                        Users.patch(
                            { userId: $rootScope.user.id },
                            { ui_data: ui_data },
                            function (user) {
                                $rootScope.updateObjOld($rootScope.user, user);
                            },
                            function (res) {
                                modalService.openErrorResponse("Failed to update user's shown services", res, false);
                            }
                        );
                    },
                },
                resizable: {
                    enabled: true,
                    handles: ['s', 'w', 'se', 'sw', 'nw', 'nw'],
                    stop: function (event, $element, widget) {
                        ctrl.resizeWidget(widget);
                    },
                    resize: function (event, $element, widget) {
                        var api = _.get(widget, 'chart.api');
                        if (api && api.update) {
                            api.update();
                        }
                    },
                },
            },
            tabs: [
                {
                    heading: 'SUMMARY',
                    show: function (tab) {
                        return true;
                    },
                    deselectAction: function () {
                        ctrl.recordSizes(ctrl.tablesWidget);
                        ctrl.recordSizes(ctrl.exposureTrendsWidget);
                        if ($rootScope.user.pocs.length > 0 || ctrl.gdata.isOperator) {
                            ctrl.recordSizes(ctrl.asrByZoneWidget);
                        }
                        ctrl.recordSizes(ctrl.portsChartWidget);
                    },
                    selectAction: function () {
                        ctrl.loadSummary(true);
                    },
                },
                {
                    heading: 'PORTS REVIEW',
                    show: function (tab) {
                        return true;
                    },
                    deselectAction: function () {
                        ctrl.recordSizes(ctrl.tablesWidget);
                    },
                    selectAction: function () {
                        ctrl.showTable('PORTS REVIEW SUMMARY');
                    },
                },
                {
                    heading: 'DOMAINS REVIEW',
                    show: function (tab) {
                        return true;
                    },
                    deselectAction: function () {
                        ctrl.recordSizes(ctrl.tablesWidget);
                    },
                    selectAction: function () {
                        ctrl.showTable('DOMAINS REVIEW');
                    },
                },
                {
                    heading: 'OWNERSHIP',
                    show: function (tab) {
                        return true;
                    },
                    deselectAction: function () {
                        ctrl.recordSizes(ctrl.tablesWidget);
                    },
                    selectAction: function () {
                        ctrl.showTable('OWNERSHIP');
                    },
                },
                {
                    heading: 'ALL OPEN PORTS',
                    show: function (tab) {
                        return true;
                    },
                    deselectAction: function () {
                        ctrl.recordSizes(ctrl.tablesWidget);
                    },
                    selectAction: function () {
                        ctrl.showTable('ALL OPEN PORTS');
                    },
                },
                // {
                //     heading: 'PORTS',
                //     show: function(tab) {
                //         return true;
                //     },
                //     deselectAction: function() {
                //         ctrl.recordSizes(ctrl.portsChartWidget);
                //     },
                //     selectAction: function() {
                //         ctrl.showChart("PORTS", ctrl.portsChartWidget);
                //     }
                // },
                {
                    heading: 'EXPOSURE TRENDS',
                    show: function (tab) {
                        return true;
                    },
                    deselectAction: function () {
                        ctrl.recordSizes(ctrl.exposureTrendsWidget);
                    },
                    selectAction: function () {
                        ctrl.showChart('EXPOSURE TRENDS', ctrl.exposureTrendsWidget);
                    },
                },
                {
                    heading: 'ASR BY ZONE',
                    show: function (tab) {
                        return ($rootScope.user.pocs.length > 0 || ctrl.gdata.isOperator) && ctrl.selectedAttackZone.id === -1;
                    },
                    deselectAction: function () {
                        ctrl.recordSizes(ctrl.asrByZoneWidget);
                    },
                    selectAction: function () {
                        ctrl.showChart('ASR BY ZONE', ctrl.asrByZoneWidget);
                    },
                },
            ],

            $onInit: function () {
                ctrl.selectedAttackZone = { id: -1 };
                ctrl.attackZones = [
                    {
                        name: 'ALL ZONES',
                        short_name: 'ALL',
                        id: -1,
                    },
                ];

                if (globalDataService.isOperator) {
                    ctrl.setCustomer(_.get($rootScope.user, 'ui_data.selectedCustomer'));
                } else {
                    ctrl.setCustomer(globalDataService.customer);
                }

                ctrl.unregisterSelectedCustomerWatch = $rootScope.$watch('user.ui_data.selectedCustomer', function (newVal, oldVal) {
                    if (newVal !== oldVal) {
                        ctrl.setCustomer(newVal);
                    }
                });

                ctrl.tablesWidget = _.find($rootScope.user.ui_data.attack_surface_widgets, { title: titles.tablesTitle });
                ctrl.exposureTrendsWidget = _.find($rootScope.user.ui_data.attack_surface_widgets, { title: titles.exposureTrendsTitle });
                ctrl.asrByZoneWidget = _.find($rootScope.user.ui_data.attack_surface_widgets, { title: titles.asrByZoneTitle });
                ctrl.portsChartWidget = _.find($rootScope.user.ui_data.attack_surface_widgets, { title: titles.portsChartTitle });
                // ctrl.loadWidgets(false);
                ctrl.currentWidget = ctrl.tablesWidget;

                $scope.$on('add-to-widgets', function (event, widgetName, childCtrl) {
                    ctrl.widgetControllers[widgetName] = childCtrl;
                });

                ctrl.cancelResizeListener = $rootScope.$on('resize', _.debounce(ctrl.updateNVD3GraphHeight, 500));
            },
            $onDestroy: function () {
                ctrl.cancelResizeListener();
                ctrl.recordSizes(ctrl.tablesWidget);
                ctrl.recordSizes(ctrl.exposureTrendsWidget);
                if ($rootScope.user.pocs.length > 0 || ctrl.gdata.isOperator) {
                    ctrl.recordSizes(ctrl.asrByZoneWidget);
                }
                ctrl.recordSizes(ctrl.portsChartWidget);
                if (ctrl.unregisterSelectedCustomerWatch) {
                    ctrl.unregisterSelectedCustomerWatch();
                }
            },

            setCustomer: function (customer) {
                ctrl.attackSurfaceData = undefined;
                ctrl.attackSurfaceSummaryData = undefined;
                var oldCustomer = ctrl.customer;
                ctrl.widgetIndexes = {};
                ctrl.customer = customer ? globalDataService.customerMap[customer.id] : undefined;
                if (ctrl.selectedRun.id === -1 && ctrl.customer && ctrl.customer !== oldCustomer) {
                    ctrl.getCurrentData(ctrl.getAttackSurfaceRunList, true);
                }
            },
            determineDimensionsAndPositions: function (storedWidget, defaultSizeX, defaultSizeY, defaultCol, defaultRow, defaultRowCount) {
                var sizeX = defaultSizeX != undefined ? defaultSizeX : 1;
                var sizeY = defaultSizeY != undefined ? defaultSizeY : 1;
                var col = defaultCol != undefined ? defaultCol : undefined;
                var row = defaultRow != undefined ? defaultRow : undefined;
                var rowCount = defaultRowCount != undefined ? defaultRowCount : undefined;

                if (
                    ctrl.activePill === 0 &&
                    storedWidget &&
                    storedWidget.storedSummarySizesAndPositions &&
                    storedWidget.storedSummarySizesAndPositions.sizeX != undefined
                ) {
                    sizeX = storedWidget.storedSummarySizesAndPositions.sizeX;
                    sizeY = storedWidget.storedSummarySizesAndPositions.sizeY;
                    col = storedWidget.storedSummarySizesAndPositions.col;
                    row = storedWidget.storedSummarySizesAndPositions.row;
                    rowCount = storedWidget.storedSummarySizesAndPositions.rowCount;
                } else if (storedWidget && storedWidget.storedSizesAndPositions && storedWidget.storedSizesAndPositions[ctrl.currentTable]) {
                    sizeX = storedWidget.storedSizesAndPositions[ctrl.currentTable].sizeX;
                    sizeY = storedWidget.storedSizesAndPositions[ctrl.currentTable].sizeY;
                    col = storedWidget.storedSizesAndPositions[ctrl.currentTable].col;
                    row = storedWidget.storedSizesAndPositions[ctrl.currentTable].row;
                    rowCount = storedWidget.storedSizesAndPositions[ctrl.currentTable].rowCount;
                }

                return { sizeX: sizeX, sizeY: sizeY, col: col, row: row, rowCount: rowCount };
            },
            formatDate: function (date) {
                return moment(date).format('YYYY-MMM');
            },
            getData: function (callback, callbackParam) {
                if (ctrl.selectedRun.id === -1) {
                    ctrl.getCurrentData(callback, callbackParam);
                } else {
                    ctrl.getPreviousData(callback, callbackParam);
                }
                if (ctrl.selectedAttackZone.id !== -1) {
                    ctrl.hideWidget(ctrl.asrByZoneWidget, false);
                } else if (ctrl.activePill === 0 || ctrl.activePill === 6) {
                    ctrl.showWidget(ctrl.asrByZoneWidget, false);
                }
            },
            getCurrentData: function (callback, callbackParam) {
                var params = {};
                if (globalDataService.isOperator) {
                    params = {
                        customer: ctrl.customer.id,
                        access_group: ctrl.selectedAttackZone.id !== -1 ? ctrl.selectedAttackZone.id : undefined,
                    };
                } else if ($rootScope.user.pocs.length > 0) {
                    params = {
                        access_group: ctrl.selectedAttackZone.id !== -1 ? ctrl.selectedAttackZone.id : undefined,
                    };
                }

                ctrl.accessGroupsResponse = AccessGroups.list(params, function (result) {
                    ctrl.attackZones = [
                        {
                            name: 'ALL ZONES',
                            short_name: 'ALL',
                            id: -1,
                        },
                    ];

                    _.each(result, function (group) {
                        ctrl.attackZones.push(group);
                    });
                });
                ctrl.accessGroupsResponse.$promise.catch(console.error); // Don't let cg-busy silently swallow the error

                ctrl.currentRunResponse = AttackSurfaceRun.current(
                    params,
                    function (data) {
                        if (Object.keys(data).length === 2) {
                            return;
                        }

                        ctrl.populate(data);
                        ctrl.currentAttackSurfaceData = ctrl.attackSurfaceData;

                        if (callback) {
                            callback(callbackParam);
                        }

                        ctrl.accessGroupsResponse.$promise.then(function (accessGroups) {
                            _.each(accessGroups, function (ag) {
                                var ips = _.filter(_.at(ctrl.attackSurfaceData.ipTargetMap, ag.targets));
                                var domains = _.filter(_.at(ctrl.attackSurfaceData.domainTargetMap, ag.targets));
                                _.each(ips, function (ip: any) {
                                    ip.accessGroupObjs.push(ag);
                                    ip.accessGroups.push(ag.name);
                                });
                                _.each(domains, function (domain: any) {
                                    domain.accessGroupObjs.push(ag);
                                    domain.accessGroups.push(ag.name);
                                });
                            });
                        });
                    },
                    function (serverResponse) {
                        modalService.openErrorResponse('Unable to get Attack Surface Run Data: ', serverResponse);
                    }
                );
                ctrl.currentRunResponse.$promise.catch(console.error); // Don't let cg-busy silently swallow the error
            },
            getPreviousData: function (callback, callbackParam) {
                var params = {
                    id: ctrl.selectedRun.id,
                    access_group: ctrl.selectedAttackZone.id !== -1 ? ctrl.selectedAttackZone.id : undefined,
                };

                ctrl.attackZones = [
                    {
                        name: 'ALL ZONES',
                        short_name: 'ALL',
                        id: -1,
                    },
                ];

                var accessGroupIdMap = {};

                _.each(ctrl.attackSurfaceSummaryData[ctrl.attackSurfaceSummaryMap[ctrl.selectedRun.id]].access_group_snapshot, function (group) {
                    accessGroupIdMap[group.id] = group;
                    ctrl.attackZones.push(group);
                });

                ctrl.previousRunResponse = AttackSurfaceRun.detail(
                    params,
                    function (data) {
                        if (Object.keys(data).length === 2) {
                            return;
                        }

                        ctrl.populate(data);

                        _.each(data.ipaddresses, function (ip) {
                            ip.accessGroupObjs = _.at(accessGroupIdMap, ip.access_groups);
                            ip.accessGroups = _.map(ip.accessGroupObjs, 'name');
                        });
                        _.each(data.domains, function (domain) {
                            domain.accessGroupObjs = _.at(accessGroupIdMap, domain.access_groups);
                            domain.accessGroups = _.map(domain.accessGroupObjs, 'name');
                        });

                        if (callback) {
                            callback(callbackParam);
                        }
                    },
                    function (serverResponse) {
                        modalService.openErrorResponse('Unable to get Attack Surface Run Data: ', serverResponse);
                    }
                );
                ctrl.previousRunResponse.$promise.catch(console.error); // Don't let cg-busy silently swallow the error
            },
            getAttackSurfaceRunList: function (setSelectedRunAndCallHelpers) {
                var params = {};
                if (globalDataService.isOperator) {
                    params = { customer: ctrl.customer.id };
                }
                ctrl.runListResponse = AttackSurfaceRun.list(
                    params,
                    function (data) {
                        if (Object.keys(data).length === 2) {
                            return;
                        }

                        ctrl.attackSurfaceSummaryData = data;
                        ctrl.attackSurfaceSummaryMap = {};
                        ctrl.runs = [{ id: -1, display: 'Current data', accessGroups: { '-1': true } }];
                        ctrl.accessGroupsResponse.$promise.then(function (accessGroups) {
                            for (const ag of accessGroups) {
                                ctrl.runs[0].accessGroups[ag.id] = true;
                            }
                        });
                        ctrl.aggregateCurrentResults();
                        // Start at 1 because we inserted the current results summary at the front and don't need to process it again
                        for (var cnt = 1; cnt < ctrl.attackSurfaceSummaryData.length; cnt++) {
                            var run = ctrl.attackSurfaceSummaryData[cnt];
                            const runOption = {
                                id: run.id,
                                display: ctrl.formatDate(run.start_timestamp),
                                accessGroups: { '-1': true },
                            };
                            for (const ag of run.access_group_snapshot) {
                                runOption.accessGroups[ag.id] = true;
                            }
                            ctrl.runs.push(runOption);
                            ctrl.attackSurfaceSummaryMap[run.id] = cnt;
                        }
                        if (setSelectedRunAndCallHelpers && ctrl.attackSurfaceSummaryData.length > 0) {
                            ctrl.selectedRun = ctrl.runs[0];
                            ctrl.selectReport(false);
                            ctrl.loadWidgets();
                        }
                    },
                    function (serverResponse) {
                        modalService.openErrorResponse('Unable to get Attack Surface Summary Data: ', serverResponse);
                    }
                );
                ctrl.runListResponse.$promise.catch(console.error); // Don't let cg-busy silently swallow the error
            },
            aggregateCurrentResults: function (newTimestamp?: string) {
                ctrl.currentRunSummary = {
                    access_group_snapshot: ctrl.accessGroupsResponse,
                    aggregate_results: {
                        current: {
                            all_access_groups: ctrl.computeAggregateForItems(
                                ctrl.currentRunResponse.ipaddresses,
                                ctrl.currentRunResponse.domains,
                                ctrl.currentRunResponse.services,
                                'latest_response__response',
                                ctrl.currentRunResponse.sans_top10
                            ),
                            per_access_group: {},
                        },
                    },
                    customer: ctrl.customer.id,
                    id: -1,
                    sans_top10: ctrl.currentRunResponse.sans_top10,
                    timestamp: newTimestamp || ctrl.currentRunResponse.timestamp,
                };
                ctrl.accessGroupsResponse.$promise.then(function (accessGroups) {
                    // aggregate by access group here
                    _.each(accessGroups, function (ag) {
                        var ips = _.filter(_.at(ctrl.currentRunResponse.ipTargetMap, ag.targets));
                        var domains = _.filter(_.at(ctrl.currentRunResponse.domainTargetMap, ag.targets));
                        ctrl.currentRunSummary.aggregate_results.current.per_access_group[ag.id] = ctrl.computeAggregateForItems(
                            ips,
                            domains,
                            _.flatMap(ips, 'serviceObjs'),
                            'latest_response__response',
                            ctrl.currentRunResponse.sans_top10
                        );
                    });
                });
                if (ctrl.attackSurfaceSummaryData[0].id === -1) {
                    // Re-computing aggregates
                    ctrl.attackSurfaceSummaryData[0] = ctrl.currentRunSummary;
                } else {
                    ctrl.attackSurfaceSummaryData.unshift(ctrl.currentRunSummary);
                }
                ctrl.attackSurfaceSummaryMap[-1] = 0;

                if (ctrl.selectedSummary && ctrl.selectedSummary.id === -1) {
                    ctrl.selectReport(false);
                }
            },
            computeAggregateForItems: function (ips, domains, services, responseKey, sansTop10) {
                var aggregate = {
                    domains: {
                        'confirmed owned': 0,
                        'confirmed not owned': 0,
                        total_domains: 0,
                        'ownership not known': 0,
                        unconfirmed: 0,
                    },
                    services: {
                        excluded: 0,
                        total_sans_top10_ports: 0,
                        total_ports: 0,
                        unreviewed: 0,
                        evaluating: 0,
                        active_ports: 0,
                        'remediation confirmed': 0,
                        remediated: 0,
                        accepted: 0,
                    },
                    ipaddresses: {
                        total_ips: 0,
                    },
                };

                aggregate.domains.total_domains = domains.length;
                aggregate.services.total_ports = services.length;
                aggregate.ipaddresses.total_ips = ips.length;

                _.each(domains, function (domain) {
                    aggregate.domains[domain.status]++;
                });

                _.each(services, function (service) {
                    aggregate.services[service[responseKey]]++;
                    if (service.active) {
                        aggregate.services.active_ports++;
                    }
                    if (sansTop10[service.port]) {
                        aggregate.services.total_sans_top10_ports++;
                    }
                });

                return aggregate;
            },
            populate: function (data) {
                ctrl.attackSurfaceData = data;
                ctrl.attackSurfaceData.ipMap = {};
                ctrl.attackSurfaceData.domainMap = {};
                ctrl.attackSurfaceData.serviceMap = {};
                ctrl.attackSurfaceData.ipTargetMap = {};
                ctrl.attackSurfaceData.domainTargetMap = {};

                _.forEach(ctrl.attackSurfaceData.ipaddresses, function (ip) {
                    ctrl.ipToDnsMapping[ip.id] = { address: ip.address, domains: [] };
                    ctrl.attackSurfaceData.ipMap[ip.id] = ip;
                    ctrl.attackSurfaceData.ipTargetMap[ip.target] = ip;
                    ip.domainObjs = [];
                    ip.serviceObjs = [];
                    ip.accessGroupObjs = [];
                    ip.accessGroups = [];
                });

                _.forEach(ctrl.attackSurfaceData.domains, function (domain) {
                    ctrl.dnsTopIpMapping[domain.id] = { name: domain.value, ips: [] };
                    ctrl.attackSurfaceData.domainMap[domain.id] = domain;
                    ctrl.attackSurfaceData.domainTargetMap[domain.target] = domain;
                    domain.ipObjs = [];
                    domain.accessGroupObjs = [];
                    domain.accessGroups = [];
                });

                _.forEach(ctrl.attackSurfaceData.dns_resolutions, function (dns_resolution) {
                    var ip = ctrl.ipToDnsMapping[dns_resolution['ip_id']];
                    var domain = ctrl.dnsTopIpMapping[dns_resolution['domain_id']];
                    if (ip === undefined) {
                        console.debug('domain does not have an IP!!!!!!', dns_resolution);
                        return;
                    }
                    if (domain.name) {
                        ip.domains.push(domain.name);
                    }
                    if (ip.address) {
                        domain.ips.push(ip.address);
                    }
                    ip = ctrl.attackSurfaceData.ipMap[dns_resolution['ip_id']];
                    domain = ctrl.attackSurfaceData.domainMap[dns_resolution['domain_id']];
                    if (domain && ip) {
                        ip.domainObjs.push(domain);
                        domain.ipObjs.push(ip);
                    }
                });

                ctrl.populateExposedServices();
            },
            populateExposedServices: function () {
                ctrl.exposedServices = [];
                ctrl.exposedPortsMap = {};

                _.forEach(ctrl.attackSurfaceData.services, function (service) {
                    const exposedService = {
                        id: service.id,
                        customer: ctrl.gdata.customers[service.customer] ? ctrl.gdata.customers[service.customer].name : 'UKNOWN',
                        protocol: service.protocol,
                        last_update:
                            ctrl.selectedRun === undefined || ctrl.selectedRun.id === -1 ? service.last_update : service.new_update__timestamp,
                        ip: ctrl.ipToDnsMapping[service.ip] ? ctrl.ipToDnsMapping[service.ip].address : 'UNKNOWN',
                        ipAccessGroups: ctrl.attackSurfaceData.ipMap[service.ip].accessGroups,
                        status:
                            ctrl.selectedRun === undefined || ctrl.selectedRun.id === -1
                                ? service.latest_response__response
                                : service.previous_update__response + ' => ' + service.new_update__response,
                        port: service.port,
                        active: service.active,
                        sansTop10Ranking: ctrl.attackSurfaceData.sans_top10[service.port],
                        domains: ctrl.ipToDnsMapping[service.ip] ? ctrl.ipToDnsMapping[service.ip].domains : 'UNKNOWN',
                    };
                    ctrl.exposedServices.push(exposedService);
                    ctrl.exposedPortsMap[exposedService.id] = exposedService;
                    ctrl.attackSurfaceData.ipMap[service.ip].serviceObjs.push(service);
                    ctrl.attackSurfaceData.serviceMap[service.id] = service;
                });

                ctrl.exposedServices = $filter('orderBy')(ctrl.exposedServices, ['domains[0]', 'ip', 'port']);
            },
            selectReport: function (getData) {
                if (!ctrl.attackSurfaceSummaryMap) {
                    return;
                }
                ctrl.selectedZoneSnapshotMap = {};
                ctrl.selectedSummary = ctrl.attackSurfaceSummaryData[ctrl.attackSurfaceSummaryMap[ctrl.selectedRun.id]];
                ctrl.selectedAggregates = ctrl.selectedSummary.aggregate_results;
                var selectedAccessGroupSnapshot = ctrl.selectedSummary.access_group_snapshot;
                for (var cnt = 0; cnt < selectedAccessGroupSnapshot.length; cnt++) {
                    var zone = selectedAccessGroupSnapshot[cnt];
                    ctrl.selectedZoneSnapshotMap[zone.id] = zone.name;
                }
                if (getData) {
                    ctrl.getData();
                }
            },
            setCurrentTable: function (table) {
                if (typeof table === 'string' && ctrl.tables.indexOf(table) > -1) {
                    ctrl.currentTable = table;
                    return true;
                }
                return false;
            },
            showChart: function (selectedChart, chartWidget) {
                ctrl.prevChart = ctrl.currentChart;
                ctrl.currentChart = selectedChart;
                ctrl.activePill = ctrl.tablesAndChartsToPills[selectedChart];

                var table = _.find(ctrl.widgets, { title: titles.tablesTitle });
                var charts = _.filter(ctrl.widgets, { isChart: true });
                ctrl.hideWidget(table, false);
                angular.forEach(charts, function (widget) {
                    if (chartWidget !== widget) {
                        ctrl.hideWidget(widget, false);
                    }
                });
                ctrl.showWidget(chartWidget, false);
                if (chartWidget.resize) {
                    chartWidget.resize();
                }

                //The table.sizeX line is setup in case we ever want to let the users resize
                //the tables in this direction right now it should always produce a table with
                //an x size of 8
                chartWidget.sizeX =
                    chartWidget.storedSizesAndPositions && chartWidget.storedSizesAndPositions.sizeX ? chartWidget.storedSizesAndPositions.sizeX : 8;
                chartWidget.sizeY =
                    chartWidget.storedSizesAndPositions && chartWidget.storedSizesAndPositions.sizeY ? chartWidget.storedSizesAndPositions.sizeY : 3;
                $scope.$broadcast('cleanup-widgets', 500);
            },
            showTable: function (selectedTable) {
                ctrl.prevTable = ctrl.currentTable;
                ctrl.currentTable = selectedTable;
                ctrl.activePill = ctrl.tablesAndChartsToPills[selectedTable];

                var table: any = _.find(ctrl.widgets, { title: titles.tablesTitle });
                var charts: any = _.filter(ctrl.widgets, { isChart: true });

                angular.forEach(charts, function (widget) {
                    ctrl.hideWidget(widget, false);
                });
                table.minSizeX = 8;
                table.minSizeY = 5;
                ctrl.showWidget(table, false);

                //The table.sizeX line is set up in case we ever want to let the users resize
                //the tables in this direction right now it should always produce a table with
                //an x size of 8
                table.sizeX =
                    table.storedSizesAndPositions && table.storedSizesAndPositions[ctrl.currentTable]
                        ? table.storedSizesAndPositions[ctrl.currentTable].sizeX
                        : table.minSizeX;
                table.sizeY =
                    table.storedSizesAndPositions && table.storedSizesAndPositions[ctrl.currentTable]
                        ? table.storedSizesAndPositions[ctrl.currentTable].sizeY
                        : table.minSizeY;
                if (ctrl.currentTable != ctrl.prevTable) {
                    var unregister = $rootScope.$on('table-switched', function (event, args) {
                        $scope.$broadcast('tables-widget-resized', {
                            activePill: args.activePill,
                            widget: args.tablesWidget,
                        });
                        unregister();
                    });
                } else {
                    $scope.$broadcast('tables-widget-resized', {
                        activePill: ctrl.activePill,
                        widget: ctrl.tablesWidget,
                    });
                }
            },
            recordSizes: function (storedWidget) {
                if (ctrl.activePill === 0 && storedWidget.storedSummarySizesAndPositions) {
                    storedWidget.storedSummarySizesAndPositions = {
                        sizeX: storedWidget.sizeX,
                        sizeY: storedWidget.sizeY,
                        col: storedWidget.col,
                        row: storedWidget.row,
                    };
                } else if (storedWidget.storedSizesAndPositions && storedWidget.isChart) {
                    storedWidget.storedSizesAndPositions = {
                        sizeX: storedWidget.sizeX,
                        sizeY: storedWidget.sizeY,
                    };
                } else if (storedWidget.storedSizesAndPositions) {
                    storedWidget.storedSizesAndPositions[ctrl.currentTable] = {
                        sizeX: storedWidget.sizeX,
                        sizeY: storedWidget.sizeY,
                    };
                }

                var ui_data = $rootScope.user.ui_data || {};
                ui_data['attack_surface_widgets'] = ctrl.widgets;
                Users.patch(
                    { userId: $rootScope.user.id },
                    { ui_data: ui_data },
                    function (user) {},
                    function (res) {
                        modalService.openErrorResponse('Failed to save ' + $rootScope.catotheme.customer + ' selection', res, false);
                    }
                );
            },
            resizeWidget: function (widget) {
                ctrl.recordSizes(widget);

                var api = _.get(widget, 'chart.api');
                if (api && api.update) {
                    $timeout(function () {
                        api.update();
                    }, 500);
                }

                if (widget === ctrl.tablesWidget) {
                    $scope.$broadcast('tables-widget-resized', {
                        activePill: ctrl.activePill,
                        widget: ctrl.tablesWidget,
                    });
                }

                if (widget.isChart) {
                    $scope.$broadcast('cleanup-widgets');
                }

                var ui_data = $rootScope.user.ui_data || {};
                ui_data['attack_surface_widgets'] = ctrl.widgets;
                Users.patch(
                    { userId: $rootScope.user.id },
                    { ui_data: ui_data },
                    function (user) {
                        $rootScope.updateObjOld($rootScope.user, user);
                    },
                    function (res) {
                        modalService.openErrorResponse("Failed to update user's shown services", res, false);
                    }
                );

                AttackSurfaceUtils.calculateTableHeights();
            },
            rowCount: function (count, useCurrentTable) {
                if (useCurrentTable ? !ctrl.currentTable : !ctrl.prevTable) {
                    return;
                }

                if (ctrl.tablesWidget.storedSizesAndPositions[useCurrentTable ? ctrl.currentTable : ctrl.prevTable] && count) {
                    ctrl.tablesWidget.storedSizesAndPositions[useCurrentTable ? ctrl.currentTable : ctrl.prevTable].rowCount = count;
                    var ui_data = $rootScope.user.ui_data || {};
                    ui_data['attack_surface_widgets'] = ctrl.widgets;
                    Users.patch(
                        { userId: $rootScope.user.id },
                        { ui_data: ui_data },
                        function (user) {
                            $rootScope.updateObjOld($rootScope.user, user);
                        },
                        function (res) {
                            modalService.openErrorResponse("Failed to update user's shown services", res, false);
                        }
                    );
                    return undefined;
                } else if (ctrl.tablesWidget.storedSizesAndPositions[ctrl.currentTable]) {
                    var returnVal = ctrl.tablesWidget.storedSizesAndPositions[ctrl.currentTable].rowCount || ctrl.defaultRowCount;
                    return returnVal;
                }
                return undefined;
            },
            hideWidget: function (widget, patch: boolean) {
                var ui_data = $rootScope.user.ui_data;
                widget.show = false;
                ui_data['attack_surface_widgets'] = ctrl.widgets;

                if (patch) {
                    Users.patch(
                        { userId: $rootScope.user.id },
                        { ui_data: ui_data },
                        function (user) {
                            $rootScope.updateObjOld($rootScope.user, user);
                        },
                        function (res) {
                            modalService.openErrorResponse("Failed to update user's shown services", res, false);
                        }
                    );
                }
            },
            showWidget: function (widget, patch: boolean) {
                var ui_data = $rootScope.user.ui_data;
                widget.show = true;
                ui_data['attack_surface_widgets'] = ctrl.widgets;

                if (patch) {
                    Users.patch(
                        { userId: $rootScope.user.id },
                        { ui_data: ui_data },
                        function (user) {
                            $rootScope.updateObjOld($rootScope.user, user);
                        },
                        function (res) {
                            modalService.openErrorResponse("Failed to update user's shown services", res, false);
                        }
                    );
                }
            },
            movedOrResized: function () {
                var movedOrResizedVal = false;
                var widgets = $rootScope.user.ui_data.attack_surface_widgets;
                for (var cnt = 0; !movedOrResizedVal && widgets && cnt < widgets.length; cnt++) {
                    var widget = widgets[cnt];
                    movedOrResizedVal =
                        widget.col !== widget.col_default ||
                        widget.row !== widget.row_default ||
                        widget.sizeX !== widget.sizeX_default ||
                        widget.sizeY !== widget.sizeY_default ||
                        widget.show === false;
                }
                return movedOrResizedVal;
            },
            lastRowForWidgetInCol0: function () {
                var lastWidgetRow = 0;
                angular.forEach(ctrl.widgets, function (widget) {
                    if (widget.col === 0) {
                        if (widget.row >= lastWidgetRow) {
                            lastWidgetRow = widget.row + widget.sizeY;
                        }
                    }
                });
                return lastWidgetRow;
            },
            getNextColAndRowForWidget: function (nextWidget) {
                /* TODO: This code makes assumptions about the order of widgets in ctrl.widgets and ignores some
                 *       collisions that can occur when widgets of different sizes are mixed.  See the following links for
                 *       more robust implementations:
                 * https://github.com/ManifestWebDesign/angular-gridster/blob/fe28af0bf31f7a25d875ec6868c407fde2fa52b7/src/angular-gridster.js#L157-L175
                 * https://github.com/dsmorse/gridster.js/blob/adfd1ffce1f9077e1bb9f651ab8c4cc692e1b22b/src/jquery.gridster.js#L986-L1026
                 */
                var lastWidget = ctrl.widgets[ctrl.widgets.length - 1];
                var col = 0;
                var row = 0;
                var columns = ctrl.gridsterOpts.columns;

                if (_.isNil(lastWidget)) {
                    // Happy with the defaults;
                } else if (lastWidget.col >= columns - 1 || lastWidget.col + lastWidget.sizeX + nextWidget.sizeX > columns) {
                    col = 0;
                    row = ctrl.lastRowForWidgetInCol0();
                } else {
                    col = lastWidget.col + lastWidget.sizeX;
                    row = lastWidget.row;
                }
                nextWidget.col = col;
                nextWidget.row = row;
                nextWidget.col_default = col;
                nextWidget.row_default = row;
                return nextWidget;
            },
            getWidget: function (params: WidgetParams) {
                if (!params.tmpl) {
                    /*  */
                    return false;
                }
                var nextWidget;
                // Defaults
                var storedWidget = _.find($rootScope.user.ui_data.attack_surface_widgets, { title: params.title });
                var sizesAndPositions = ctrl.determineDimensionsAndPositions(
                    storedWidget,
                    params.sizeX,
                    params.sizeY,
                    params.col,
                    params.row,
                    params.rowCount
                );

                var show = params.show ? params.show : true;

                if (!_.isNil(params.resize) && params.resize === false) {
                    params.gridsterClasses.push('no-resize');
                }

                var widget: any = _.find(ctrl.widgets, { title: params.title });
                if (!widget) {
                    widget = {
                        id: params.id,
                        version: params.version,
                        title: params.title,
                        templateUrl: params.tmpl,
                        gridsterClasses: params.gridsterClasses ? params.gridsterClasses : [''],
                        sizeX: sizesAndPositions.sizeX,
                        sizeY: sizesAndPositions.sizeY,
                        sizeX_default: sizesAndPositions.sizeX,
                        sizeY_default: sizesAndPositions.sizeY,
                        col: sizesAndPositions.col,
                        row: sizesAndPositions.row,
                        rowCount: sizesAndPositions.rowCount,
                        col_default: params.col_default !== undefined ? params.col_default : -1,
                        row_default: params.row_default !== undefined ? params.row_default : -1,
                        minSizeX: params.lockX ? sizesAndPositions.sizeX : 1,
                        maxSizeX: params.lockX ? sizesAndPositions.sizeX : undefined,
                        minSizeY: params.lockY ? sizesAndPositions.sizeY : 1,
                        maxSizeY: params.lockY ? sizesAndPositions.sizeY : undefined,
                        storedSizesAndPositions: params.storedSizesAndPositions,
                        storedSummarySizesAndPositions: params.storedSummarySizesAndPositions,
                        show: show,
                        isChart: params.chart ? true : false,
                    };

                    // For some widgets we need to load the chart api
                    if (params.chart) {
                        widget.chart = params.chart;
                    }

                    nextWidget = widget.col >= 0 && widget.row >= 0 ? widget : ctrl.getNextColAndRowForWidget(widget);
                    ctrl.widgetIndexes[nextWidget.title] = ctrl.widgets.length;
                    ctrl.widgets.push(nextWidget);
                }

                if (!_.isEmpty(params.gridsterClasses) && !angular.equals(params.gridsterClasses, widget.gridsterClasses)) {
                    widget.gridsterClasses = params.gridsterClasses;
                }

                if (!_.isEmpty(params.additionalParams)) {
                    angular.forEach(params.additionalParams, function (v, k) {
                        widget[k] = v;
                    });
                }

                return nextWidget;
            },
            resetWidgets: function ($event) {
                if ($event.altKey && $event.shiftKey) {
                    $rootScope.user.ui_data.attack_surface_widgets = [];
                    Users.patch(
                        { userId: $rootScope.user.id },
                        { ui_data: $rootScope.user.ui_data },
                        function (user) {
                            $rootScope.updateObjOld($rootScope.user, user);
                            ctrl._resetWidgets();
                        },
                        function (res) {
                            modalService.openErrorResponse('Failed to save ' + $rootScope.catotheme.customer + ' selection', res, false);
                        }
                    );
                } else {
                    ctrl._resetWidgets();
                }
            },
            _resetWidgets: function () {
                ctrl.widgets = [];
                ctrl.tablesWidget = undefined;
                ctrl.exposureTrendsWidget = undefined;
                ctrl.asrByZoneWidget = undefined;
                ctrl.portsChartWidget = undefined;
                ctrl.currentWidget = undefined;
                ctrl.loadWidgets(true);
                for (var childCtrl in ctrl.widgetControllers) {
                    if (!ctrl.widgetControllers.hasOwnProperty(childCtrl)) {
                        //The current property is not a direct property of ctrl
                        continue;
                    }
                    ctrl.widgetControllers[childCtrl].api.updateWithTimeout(1000);
                }
            },
            loadSummary: function (b?: boolean) {
                for (var cnt = 0; cnt < ctrl.widgets.length; cnt++) {
                    var widget = ctrl.widgets[cnt];
                    if (ctrl.selectedAttackZone.id !== -1 && widget.title === titles.asrByZoneTitle) {
                        continue;
                    }
                    widget.sizeX = widget.storedSummarySizesAndPositions.sizeX ? widget.storedSummarySizesAndPositions.sizeX : widget.sizeX_default;
                    widget.sizeY = widget.storedSummarySizesAndPositions.sizeY ? widget.storedSummarySizesAndPositions.sizeY : widget.sizeY_default;
                    widget.col = widget.storedSummarySizesAndPositions.col ? widget.storedSummarySizesAndPositions.col : widget.col_default;
                    widget.row = widget.storedSummarySizesAndPositions.row ? widget.storedSummarySizesAndPositions.row : widget.row_default;
                    ctrl.showWidget(widget, false);

                    $timeout(function () {
                        ctrl.updateNVD3GraphHeight(ctrl.widgets[cnt]);
                    }, 500);
                }
                ctrl.activePill = 0;
                $scope.$broadcast('cleanup-widgets');
                AttackSurfaceUtils.calculateTableHeights();

                let table: any = _.find(ctrl.widgets, { title: titles.tablesTitle });

                table.minSizeX = 8;
                table.minSizeY = 3;
                table.sizeY = 3;

                ctrl.showWidget(table, false);
            },
            loadWidget: function (params: WidgetParams) {
                // TODO: replace with direct calls to getWidget
                return ctrl.getWidget(params);
            },
            loadPortsChartWidget: function (reset) {
                if (!reset && ctrl.portsChartWidget) {
                    ctrl.widgetIndexes[ctrl.portsChartWidget.title] = ctrl.widgets.length;
                    ctrl.widgets.push(ctrl.portsChartWidget);
                    return;
                }

                ctrl.portsChartWidget = ctrl.loadWidget(widgetDefaults.ports);
            },
            loadDomainsChartWidget: function (reset) {
                if (!reset && ctrl.domainsChartWidget) {
                    ctrl.widgetIndexes[ctrl.domainsChartWidget.title] = ctrl.widgets.length;
                    ctrl.widgets.push(ctrl.domainsChartWidget);
                    return;
                }

                ctrl.domainsChartWidget = ctrl.loadWidget(widgetDefaults.domains);
            },
            loadASRByZoneChartWidget: function (reset) {
                if (!reset && ctrl.asrByZoneWidget) {
                    ctrl.widgetIndexes[ctrl.asrByZoneWidget.title] = ctrl.widgets.length;
                    ctrl.widgets.push(ctrl.asrByZoneWidget);
                    return;
                }

                ctrl.asrByZoneWidget = ctrl.loadWidget(widgetDefaults.asrByZone);
            },
            loadExposureTrendsChartWidget: function (reset) {
                if (!reset && ctrl.exposureTrendsWidget) {
                    ctrl.widgetIndexes[ctrl.exposureTrendsWidget.title] = ctrl.widgets.length;
                    ctrl.widgets.push(ctrl.exposureTrendsWidget);
                    return;
                }

                ctrl.exposureTrendsWidget = ctrl.loadWidget(widgetDefaults.exposureTrends);
            },
            loadTablesWidget: function (reset) {
                if (!reset && ctrl.tablesWidget) {
                    if (ctrl.widgetIndexes[ctrl.tablesWidget.title] === undefined) {
                        ctrl.widgetIndexes[ctrl.tablesWidget.title] = ctrl.widgets.length;
                    }
                    ctrl.widgets.push(ctrl.tablesWidget);
                    return;
                }

                ctrl.tablesWidget = ctrl.loadWidget(widgetDefaults.tables);
            },
            loadWidgets: function (reset) {
                ctrl.widgets.length = 0;
                ctrl.loadPortsChartWidget(reset);
                ctrl.loadDomainsChartWidget(reset);
                ctrl.loadTablesWidget(reset);
                ctrl.loadExposureTrendsChartWidget(reset);
                if (($rootScope.user.pocs.length > 0 || ctrl.gdata.isOperator) && ctrl.selectedAttackZone.id === -1) {
                    ctrl.loadASRByZoneChartWidget(reset);
                }
                ctrl.loadSummary(reset);
            },
            updateWidgetGraph: function (widget) {
                var api_update = _.get(widget, 'chart.api.update');
                if (api_update) {
                    api_update();
                }
            },
            updateNVD3GraphHeight: function (widget) {
                if (widget) {
                    ctrl.updateWidgetGraph(widget);
                } else {
                    _.each(ctrl.widgets, function (widget) {
                        ctrl.updateWidgetGraph(widget);
                    });
                }
            },
            bulkSetResponses: function (data) {
                ctrl.attackSurfaceData._updateResponse = ResponseUpdate.bulkSet(
                    {},
                    data,
                    function (updates) {
                        for (const resp of updates) {
                            const exposedPort = ctrl.exposedPortsMap[resp.service];
                            const serviceObj = ctrl.attackSurfaceData.serviceMap[resp.service];

                            exposedPort.last_update = resp.timestamp;
                            exposedPort.status = resp.response;
                            serviceObj.last_update = resp.timestamp;
                            serviceObj.latest_response__response = resp.response;
                            serviceObj.latest_response = resp.id;
                        }

                        let newTimestamp: string;
                        if (updates.length > 0) {
                            newTimestamp = updates[0].timestamp;
                        }

                        ctrl.aggregateCurrentResults(newTimestamp);

                        if (data.callback) {
                            data.callback(updates);
                        }
                    },
                    modalService.openErrorResponseCB('Unable to set statuses')
                );
            },
        };

        for (var prop in ctrl) {
            if (!ctrl.hasOwnProperty(prop)) {
                //The current property is not a direct property of ctrl
                continue;
            }
            this[prop] = ctrl[prop];
        }

        //Replace the initial ctrl object with the updated this object in order to prevent the two from getting out of sync
        ctrl = this;
    },
});

const titles = {
    tablesTitle: 'Attack Surface Table Widget',
    exposureTrendsTitle: 'Exposure Trends',
    asrByZoneTitle: 'ASR by Zone',
    portsChartTitle: 'Ports',
    domainsChartTitle: 'Domains by Zone',
};

const widgetDefaults: { [key: string]: WidgetParams } = {
    ports: {
        id: 'ports-chart',
        version: 1,
        tmpl: '/app/attackSurface/widgets/portsChart/portsChartLoader.html',
        title: titles.portsChartTitle,
        col: 4,
        row: 3,
        col_default: 4,
        row_default: 3,
        sizeX: 4,
        sizeY: 2,
        storedSummarySizesAndPositions: {},
        storedSizesAndPositions: {},
        lockY: true,
        lockX: true,
        gridsterClasses: ['graph-link', 'show-slow'],
        chart: { api: {} },
        resize: false,
    },
    domains: {
        id: 'domains-chart',
        version: 1,
        tmpl: '/app/attackSurface/widgets/domains-chart/domains-chart-loader.html',
        title: titles.domainsChartTitle,
        col: 0,
        row: 3,
        col_default: 0,
        row_default: 3,
        sizeX: 4,
        sizeY: 2,
        storedSummarySizesAndPositions: {},
        storedSizesAndPositions: {},
        lockY: true,
        lockX: true,
        gridsterClasses: ['graph-link', 'show-slow'],
        chart: { api: {} },
        resize: false,
    },
    asrByZone: {
        id: 'asr-by-zone',
        version: 1,
        tmpl: '/app/attackSurface/widgets/ASRByZoneChart/ASRByZoneChartLoader.html',
        title: titles.asrByZoneTitle,
        col: 0,
        row: 5,
        col_default: 0,
        row_default: 5,
        sizeX: 8,
        sizeY: 2,
        storedSummarySizesAndPositions: {},
        storedSizesAndPositions: {},
        lockX: true,
        lockY: false,
        gridsterClasses: ['graph-link', 'show-slow'],
        chart: { api: {} },
        resize: true,
    },
    exposureTrends: {
        id: 'exposure-trends',
        version: 1,
        tmpl: '/app/attackSurface/widgets/exposureTrendsChart/exposureTrendsChartLoader.html',
        title: titles.exposureTrendsTitle,
        col: 0,
        row: 7,
        col_default: 0,
        row_default: 7,
        sizeX: 8,
        sizeY: 3,
        storedSummarySizesAndPositions: {},
        storedSizesAndPositions: {},
        lockX: false,
        lockY: false,
        gridsterClasses: ['graph-link', 'show-slow'],
        chart: { api: {} },
        resize: true,
    },
    tables: {
        id: 'attack-surface-table',
        version: 1,
        tmpl: '/app/attackSurface/widgets/tables/tablesLoader.html',
        title: titles.tablesTitle,
        col: 0,
        row: 0,
        col_default: 0,
        row_default: 0,
        sizeX: 8,
        sizeY: 3,
        storedSummarySizesAndPositions: {},
        storedSizesAndPositions: {},
        rowCount: 10,
        lockX: true,
        lockY: false,
        gridsterClasses: ['graph-link'],
        resize: true,
    },
};
