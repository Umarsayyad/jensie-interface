import app from '../app';
import * as _ from 'lodash';
import * as angular from 'angular';
import catotheme from '@cato/config/themes/catotheme';

app.provider(
    'AttackSurfaceUtils',
    class AttackSurfaceUtils {
        $get($rootScope, $interval, $timeout, ngTableEventsChannel) {
            //console.log("in AttackSurfaceUtils controller");
            var AttackSurfaceUtils = {
                portsStatuses: ['unreviewed', 'accepted', 'excluded', 'evaluating', 'remediated'],
                prevContianerHeight: -1,
                chartCallback: function (ctrl, scope) {
                    ctrl.chart = scope.chart;
                    ctrl.svg = scope.svg;
                    ctrl.api = scope.api;
                },
                calculateTableHeights: function () {
                    var interval = $interval(
                        function () {
                            if (AttackSurfaceUtils._calculateTableHeights()) {
                                $interval.cancel(interval);
                            }
                        },
                        100,
                        100
                    );
                },
                _calculateTableHeights: function (containerId?: any) {
                    containerId = containerId ? containerId : 'attackSurfaceTableWidget';
                    var container;
                    if ((container = angular.element('#' + containerId)).length === 0) {
                        return false;
                    }

                    var containerHeight = Number(container.css('height').slice(0, -2));

                    // console.log("AttackSurfaceUtils.prevContianerHeight: "+AttackSurfaceUtils.prevContianerHeight);
                    // console.log("containerHeight: "+containerHeight);
                    if (containerHeight !== AttackSurfaceUtils.prevContianerHeight) {
                        AttackSurfaceUtils.prevContianerHeight = containerHeight;
                        return false;
                    }

                    var titleHeight = Number(container.find('> .graph-widget-body > .graph-widget-title').css('height').slice(0, -2));
                    var tablesHeight = containerHeight - titleHeight;
                    var tables = container.find(' > .graph-widget-body > .attack-surface-tables');
                    tables.css('height', tablesHeight);

                    var tableComponent;
                    var paginator;
                    var tableHeight;
                    if ((tableComponent = angular.element('.attack-surface-table')).length === 0) {
                        return false;
                    }
                    var tableComponentHeight = Number(tableComponent.css('height').slice(0, -2));

                    angular.element('.attack-surface-table > table > tbody').css('height', 0);

                    if ((paginator = tableComponent.find(' > div')).length === 0) {
                        return false;
                    }
                    var paginatorHeight = Number(paginator.css('height').slice(0, -2));
                    paginatorHeight = paginatorHeight > 50 ? paginatorHeight : 50;

                    tableHeight = tableComponentHeight - paginatorHeight;
                    var table;
                    if ((table = tableComponent.find(' > table')).length === 0) {
                        return false;
                    }
                    table.css('height', tableHeight);

                    var thead;
                    if ((thead = angular.element('.attack-surface-table> table > thead')).length === 0) {
                        return false;
                    }
                    var headerHeight = Number(thead.css('height').slice(0, -2));

                    var bodyHeight = tableHeight - headerHeight;
                    angular.element('.attack-surface-table > table > tbody').css('height', bodyHeight);

                    AttackSurfaceUtils.prevContianerHeight = -1;

                    return true;
                },
                filterOptions: {
                    filterComparator: function (value, searchTerm) {
                        if (typeof value === 'boolean') {
                            return value === Boolean(searchTerm);
                        } else if (!(value && searchTerm) && value !== 0) {
                            return undefined;
                        } else if (typeof value === 'string') {
                            return (value.toLowerCase() || '').indexOf(searchTerm.toLowerCase()) === 0;
                        } else if (typeof value === 'number') {
                            return value === Number(searchTerm);
                        } else if (value[0]) {
                            return undefined;
                        } else {
                            return undefined;
                        }
                    },
                },
                setupResizeListener: function (ctrl, containerId, rowIdPrefix, args) {
                    ctrl.unregisterResizeListener = ctrl.$scope.$on('tables-widget-resized', function (event, args) {
                        //Do the resize
                        // if (args.activePill === 0) {
                        //     return;
                        // }
                        var lastFilteredData;
                        ngTableEventsChannel.onAfterDataFiltered(
                            function (publisher, data) {
                                lastFilteredData = data;
                            },
                            ctrl.$scope,
                            ctrl.table
                        );
                        var interval = $interval(
                            function () {
                                if (AttackSurfaceUtils._calculateTableHeights(containerId)) {
                                    //When resize if successful, if there is a primary value for the current row
                                    //use it to set the current row in the table.
                                    $interval.cancel(interval);
                                    if (ctrl.curRowPriVal) {
                                        var curRowPriValIndex = _.findIndex(lastFilteredData, {
                                            port: ctrl.curRowPriVal,
                                            ip: ctrl.focusedIp,
                                        });
                                        if (curRowPriValIndex !== -1) {
                                            var pageSize = ctrl.table.count();
                                            var currentPage = Math.floor(curRowPriValIndex / pageSize) + 1;
                                            ctrl.table.page(currentPage);
                                            var objToSelect = lastFilteredData[curRowPriValIndex];
                                            interval = $interval(
                                                function () {
                                                    var row;
                                                    if ((row = angular.element('#' + rowIdPrefix + '_' + ctrl.curRowPriVal)).length) {
                                                        row.select();
                                                        $interval.cancel(interval);
                                                        var top = row[0].offsetTop; //Getting Y of target element

                                                        //TODO: fix
                                                        // angular.element("#" + tableComponentId + " > table > tbody")[0].scrollTop = top - Number(row.css('height').slice(0, -2)) * 2;

                                                        objToSelect.selected = true;
                                                    }
                                                },
                                                100,
                                                100
                                            );
                                        }
                                    }
                                }
                            },
                            500,
                            1000
                        );
                    });
                    $rootScope.$broadcast('table-switched', { activePill: args.activePill, widget: args.tablesWidget });
                    return ctrl.unregisterResizeListener;
                },
                resizeWidget: function (widgetID, updateChart) {
                    var timeouts = 0;
                    var widget = angular.element('#' + widgetID)[0];
                    if (widget) {
                        var originalX = widget.clientWidth;
                        var originalY = widget.clientHeight;
                        var oldX = widget.clientWidth;
                        var oldY = widget.clientHeight;
                        var helper = function () {
                            var currentX = widget.clientWidth;
                            var currentY = widget.clientHeight;
                            //console.log("oldX:" + oldX + ",oldY:" + oldY);
                            //console.log("originalX:" + originalX + ",originalY:" + originalY);
                            //console.log("currentX:" + currentX + ",currentY:" + currentY);
                            if ((currentX === oldX && currentX !== originalX) || (currentY === oldY && currentY !== originalY)) {
                                // $timeout.cancel(promise);
                                updateChart();
                                // $timeout(helper, 100);
                            } else if (timeouts++ < 100) {
                                oldX = widget.clientWidth;
                                oldY = widget.clientHeight;
                                $timeout(helper, 100);
                            }
                        };
                        var promise = $timeout(helper, 100);
                    }
                },
                selectAll: function (ctrl, allRowsSelected) {
                    $rootScope.selectAll(allRowsSelected !== undefined ? allRowsSelected : ctrl.allRowsSelected, ctrl.dataset);
                },
                selectTooltip: function (status) {
                    status = status.toLowerCase();
                    let returnString = '';
                    switch (status) {
                        case 'unreviewed':
                            returnString = 'No action yet taken';
                            break;
                        case 'accepted':
                            returnString = 'Acceptable to leave open for intended use';
                            break;
                        case 'excluded':
                            returnString = 'Cannot be changed (e.g. asset not owned)';
                            break;
                        case 'evaluating':
                            returnString = 'Currently under review, port should not be open';
                            break;
                        case 'remediated':
                            returnString = 'Closed the port (or took down the server)';
                            break;
                        case 'remediation confirmed':
                            returnString = `Subsequent ${catotheme.appName} scan confirms port was closed`;
                            break;
                        case 'sans top 10':
                            returnString = 'Number of discovered ports on SANS Top 10 attack list';
                            break;
                        case 'all ports':
                            returnString = 'Total number of discovered open ports';
                            break;
                        default:
                            break;
                    }
                    return returnString;
                },
                titleCase: function (str) {
                    str = str.replace(/_/g, ' ');
                    str = str.replace(/[A-Z]/g, function (t) {
                        return ' ' + t.toUpperCase();
                    });
                    return str.replace(/(^[a-z])|(\s[a-zA-Z])/g, function (t) {
                        return t.toUpperCase();
                    });
                },
                accessGroupTooltip: function (accessGroups) {
                    return '<h5>IP Access Groups</h5>' + _(accessGroups).sortBy().join('<br>');
                },
            };

            return AttackSurfaceUtils;
        }
    }
);
