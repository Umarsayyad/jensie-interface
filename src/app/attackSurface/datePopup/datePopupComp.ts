import app from '../../app';

app.component('datePopupAttackSurface', {
    templateUrl: '/app/attackSurface/datePopup/datePopup.html',
    bindings: {
        onUpdate: '&',
        dateName: '@',
        initVal: '<?',
        minDate: '<',
        maxDate: '<',
    },

    controller: function () {
        var ctrl: any = {
            dateOptions: undefined,
            dt: null,
            events: [],
            altInputFormats: ['MM/yyyy'],
            formats: ['yyyy-MMM', 'MMM-yyyy', 'yyyy/MM', 'MM.yyyy'],
            format: 'yyyy-MMM',
            inlineOptions: {
                minDate: new Date(),
                showWeeks: true,
            },
            popup: {
                opened: false,
            },
            tomorrow: undefined,
            afterTomorrow: undefined,
            value: null,
            $onInit: function () {
                ctrl.dateOptions = {
                    minDate: ctrl.minDate,
                    maxDate: ctrl.maxDate,
                    datepickerMode: 'month',
                    minMode: 'month',
                };
                ctrl.today();
                ctrl.tomorrow = new Date();
                ctrl.tomorrow.setDate(ctrl.tomorrow.getDate() + 1);
                ctrl.afterTomorrow = new Date();
                ctrl.afterTomorrow.setDate(ctrl.tomorrow.getDate() + 1);
                ctrl.events = [
                    {
                        date: ctrl.tomorrow,
                        status: 'full',
                    },
                    {
                        date: ctrl.afterTomorrow,
                        status: 'partially',
                    },
                ];
            },
            $onChanges: function () {
                ctrl.value = ctrl.initVal ? Date.parse(ctrl.initVal) : null;
                ctrl.dateOptions = {
                    minDate: ctrl.minDate,
                    maxDate: ctrl.maxDate,
                    datepickerMode: 'month',
                    minMode: 'month',
                };
            },
            today: function () {
                ctrl.dt = new Date();
            },
            clear: function () {
                ctrl.dt = null;
            },
            open: function () {
                ctrl.popup.opened = true;
            },
            setDate: function (year, month, day) {
                ctrl.dt = new Date(year, month, day);
            },
            update: function (prop, value) {
                this.onUpdate({ prop: prop, value: value });
            },
        };

        for (var prop in ctrl) {
            if (!ctrl.hasOwnProperty(prop)) {
                //The current property is not a direct property of ctrl
                continue;
            }
            this[prop] = ctrl[prop];
        }

        //Replace the initial ctrl object with the updated this object in order to prevent the two from getting out of sync
        ctrl = this;
    },
});
