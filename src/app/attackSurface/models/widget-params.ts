export interface WidgetParams {
    id: string;
    version: number;
    tmpl: string;
    title: string;
    col: number;
    row: number;
    col_default: number;
    row_default: number;
    sizeX: number;
    sizeY: number;
    storedSummarySizesAndPositions: object;
    storedSizesAndPositions: object;
    lockY: boolean;
    lockX: boolean;
    gridsterClasses: string[];
    resize: boolean;

    additionalParams?: object;
    chart?: { api: object };
    rowCount?: number;
    show?: boolean;
}
