export class Domain {
    id: number;
    accessGroupObjs: any[];
    accessGroups: string[];
    customer: number;
    ipObjs: any[];
    registrar: string | null;
    serves_http: boolean;
    serves_https: boolean;
    status: string;
    target: number;
    value: string;
}
