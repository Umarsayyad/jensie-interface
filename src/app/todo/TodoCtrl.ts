import app from '../app';
import * as _ from 'lodash';
import * as angular from 'angular';

import '../common/actionsDropdownComp';
import '../common/PendingItemsService';
import '../customer/manage/CondensedCustomerFilterComp';
import '../finding/reference/referencesEditor';
import '../finding/reference/referencesViewer';

app.controller('TodoCtrl', function (
    $rootScope,
    $scope,
    $timeout,
    $uibModal,
    $state,
    globalDataService,
    FindingService,
    TargetService,
    OperationService,
    OperationTemplates,
    PendingItemsService,
    MessageService,
    modalService,
    sessionService,
    socketService,
    Findings,
    Users,
    Operations
) {
    $scope.data = {
        customers: $scope.gdata.customers,
        customerLookup: $scope.gdata.customerMap,
        findingsToProcess: [],
        runningOperations: [],
        operations: [],
        draftOperations: [],
        requestedOperations: [],
        requestedUsers: [],
        operators: {},
        operationTemplates: [],
        references: [],
    };

    $scope.customer = $scope.gdata.customerMap[$state.params.customer] || _.get($rootScope.user, 'ui_data.selectedCustomer') || $scope.gdata.customer;

    $scope.operationTemplates = OperationTemplates.list({ basic: true });

    $scope.allSelected = {};
    $scope.data.selectedDraftOperations = [];
    $scope.data.selectedRequestedOperations = [];
    $scope.data.selectedRequestedUsers = [];
    $scope.data.selectedFindings = [];
    $scope.customer = _.get($rootScope.user, 'ui_data.selectedCustomer');

    $scope.reviewFindings = true;
    $scope.operationsLoaded = false;
    $scope.authorOperationsLoaded = false;
    $scope.startedByOperationsLoaded = false;
    $scope.usersLoaded = false;
    $scope.isFindingsSearch = false;
    $scope.findingsReviewScope = null;

    $scope.getTargetTypeIcon = TargetService.getTargetTypeIcon;

    $scope.showRetestDetails = OperationService.showRetestDetails;

    $scope.getNameOrRoleFromId = MessageService.getNameOrRoleFromId;

    $scope.activeTab = 'runningOperations';
    if ($scope.gdata.isMissionDirector) $scope.activeTab = 'findingsToReview';

    $scope.tabs = [
        {
            heading: 'RUNNING OPERATIONS',
            show: function (tab) {
                return $scope.gdata.isOperator && !$scope.gdata.isMissionDirector;
            },
            notifications: $scope.data.runningOperations,
            stateName: 'runningOperations',
            pendingKey: 'running_operations',
            include: '/app/todo/tabs/runningOperations.html',
        },
        {
            heading: 'FINDINGS TO REVIEW',
            show: function (tab) {
                return $scope.gdata.isOperator;
            },
            notifications: { length: 0 },
            stateName: 'findingsToReview',
            pendingKey: 'findings_to_review',
            include: '/app/todo/tabs/findingsForReview.html',
        },
        {
            heading: 'REJECTED OPERATIONS',
            show: function (tab) {
                return $scope.gdata.isOperator && !$scope.gdata.isMissionDirector;
            },
            notifications: $scope.data.operations,
            stateName: 'rejectedOperations',
            pendingKey: 'rejected_operations',
            include: '/app/todo/tabs/operationRequests.html',
        },
        {
            heading: 'DRAFT OPERATIONS',
            show: function (tab) {
                return $scope.gdata.isMissionDirector;
            },
            notifications: $scope.data.draftOperations,
            stateName: 'draftOperations',
            pendingKey: 'draft_operations',
            include: '/app/todo/tabs/draftOperations.html',
        },
        {
            heading: 'REQUESTED OPERATIONS',
            show: function (tab) {
                return $scope.gdata.isMissionDirector;
            },
            notifications: $scope.data.requestedOperations,
            stateName: 'requestedOperations',
            pendingKey: 'requested_operations',
            include: '/app/todo/tabs/requestedOperations.html',
        },
        {
            heading: 'MY TRAINING',
            show: function (tab) {
                return false;
            },
            notifications: null,
            stateName: 'myTraining',
            pendingKey: 'my_training',
            include: '/app/md/todo/tabs/myTraining.html',
        },
        {
            heading: 'REQUESTED USERS',
            show: function (tab) {
                return $scope.gdata.isMissionDirector;
            },
            notifications: $scope.data.requestedUsers,
            stateName: 'requestedUsers',
            pendingKey: 'requested_users',
            include: '/app/todo/tabs/requestedUsers.html',
        },
    ];

    $scope.go = function (tab) {
        $scope.activeTab = tab.stateName;
    };

    var my_user_id = $scope.user.id;
    var knownOps = {};

    function sumToDoItems() {
        if ($scope.gdata.isMissionDirector) {
            $rootScope.newItemCounts.toDoItems =
                $scope.data.draftOperations.length +
                $scope.data.requestedOperations.length +
                $scope.data.findingsCount +
                $scope.data.requestedUsers.length;
        } else {
            $rootScope.newItemCounts.toDoItems = $scope.data.runningOperations.length + $scope.data.operations.length + $scope.data.findingsCount;
        }
    }

    sumToDoItems();

    $scope.$watchGroup(
        [
            'data.runningOperations.length',
            'data.operations.length',
            'data.draftOperations.length',
            'data.requestedOperations.length',
            'data.requestedUsers.length',
            'data.findingsCount',
        ],
        function (newValue, oldValue) {
            sumToDoItems();
        },
        true
    );

    function updateOperationList(response) {
        var opTypes = [
            { state: ['draft'], arr: $scope.data.draftOperations },
            { state: ['requested'], arr: $scope.data.requestedOperations },
            { state: ['rejected'], arr: $scope.data.operations },
            { state: ['in progress', 'paused'], arr: $scope.data.runningOperations },
        ];

        response.customer = _.find($scope.gdata.customers, { id: response.customer });
        // ensure operation isn't on any of the other arrays, and IS on the array we're looking for
        _.forEachRight(opTypes, function (opType) {
            if (opType.state.indexOf(response.state) > -1) {
                // Add to the array if needed, or update the obj
                var idx = _.findIndex(opType.arr, { id: response.id });
                if (idx === -1) {
                    opType.arr.unshift(response);
                } else {
                    // Update the object
                    console.log('updating the obj');
                    opType.arr[idx] = response;
                }
            } else {
                // Remove from the array if it exists
                _.remove(opType.arr, { id: response.id });
            }
        });
    }

    var operationUnsubscribe = socketService.subscribeByModel('operation', updateOperationList);

    $scope.$on('$destroy', function () {
        operationUnsubscribe();
    });

    function pushUniqueOperation(operation) {
        for (var i = 0; i < $scope.data.operations.length; i++) {
            if ($scope.data.operations[i].id === operation.id) {
                return;
            }
        }

        $scope.data.operations.push(operation);
    }

    function loadInitialItems() {
        /* load everything to start with */

        if ($scope.gdata.isMissionDirector) {
            $scope.data.operations.length = 0;

            Users.query({ request_pending: true }).$promise.then(function (result) {
                angular.forEach(result, function (user) {
                    var foundUser = _.find($scope.data.requestedUsers, { id: user.id });
                    if (!foundUser) {
                        $scope.data.requestedUsers.push(user);
                    } else {
                        $rootScope.updateObjOld(foundUser, user);
                    }
                });
                $scope.usersLoaded = true;
            });

            /* a requested Operation is one that the Operator wanted to handle,
             * a draft Operation is one that the Operator is trying to create.
             */
            Operations.query({ state: ['requested', 'draft'] }).$promise.then(function (result) {
                $scope.data.requestedOperations.length = 0;
                $scope.data.draftOperations.length = 0;
                for (var i = 0; i < result.results.length; i++) {
                    //console.log('operation id: ' + result.results[i].id);
                    if (result.results[i].state === 'requested') {
                        $scope.data.requestedOperations.push(result.results[i]);
                    } else if (result.results[i].state === 'draft') {
                        //console.log('adding to draftOperations: ', result.results[i]);
                        $scope.data.draftOperations.push(result.results[i]);
                    }
                }
                $scope.operationsLoaded = true;
                //$scope.data.operations = result.results;
            });
        } else if ($scope.gdata.isOperator) {
            var user_id = sessionService.getSession().id;
            $scope.data.operations.length = 0;

            /* This operation you wanted was rejected; check the reasoning field. */
            Operations.query({ author: user_id, state: ['rejected', 'fix up'] })
                .$promise.then(function (result) {
                    for (var i = 0; i < result.results.length; i++) {
                        pushUniqueOperation(result.results[i]);
                    }
                })
                .then(function () {
                    //console.log('setting operationsLoaded to TRUE for author');
                    $scope.authorOperationsLoaded = true;
                });
            Operations.query({ startedBy: user_id, state: ['rejected', 'fix up'] })
                .$promise.then(function (result) {
                    for (var i = 0; i < result.results.length; i++) {
                        pushUniqueOperation(result.results[i]);
                    }
                })
                .then(function () {
                    //console.log('setting operationsLoaded to TRUE for startedBy');
                    $scope.startedByOperationsLoaded = true;
                });
            Operations.query({ startedBy: user_id, state: ['in progress', 'paused'] })
                .$promise.then(function (result) {
                    console.log('running operation results: ', result);
                    $scope.data.runningOperations = result.results;
                    let tab: any = _.find($scope.tabs, { stateName: 'runningOperations' });
                    tab.notifications = result.results;
                })
                .then(function () {
                    console.log('setting runningOperations to TRUE');
                    $scope.runningOperationsLoaded = true;
                });
        }
    }

    loadInitialItems();

    $scope.getName = function (user_id) {
        if (!_.isUndefined($scope.gdata.operators[user_id])) {
            return $scope.gdata.operators[user_id].first_name + ' ' + $scope.gdata.operators[user_id].last_name;
        }
        return '...';
    };

    function approveUser(id, send_welcome) {
        Users.approve(
            { userId: id, welcome_email: send_welcome },
            {},
            function (result) {
                globalDataService.updateUser(result);

                _.remove($scope.data.requestedUsers, function (reqUser: any) {
                    return reqUser.id == id;
                });
                PendingItemsService.markRequestedUserHandled(id);
            },
            function (result) {
                modalService.openErrorResponse('Could not approve user', result, false);
            }
        );
    }

    /** User scope handlers. */
    $scope.approveUser = function (id) {
        modalService.openYesNoCancel(
            'Send welcome email?',
            'Send welcome email?',
            function () {
                approveUser(id, true);
            },
            function () {
                approveUser(id, false);
            }
        );
    };

    function bulkApproveUsers(ids, send_welcome) {
        Users.bulk_approve(
            { welcome_email: send_welcome },
            { ids: ids },
            function (result) {
                _.each(ids, function (id) {
                    globalDataService.updateUser({ id: id, is_active: true, request_pending: false });
                    PendingItemsService.markRequestedUserHandled(id);
                });

                _.remove($scope.data.requestedUsers, function (reqUser: any) {
                    return ids.indexOf(reqUser.id) >= 0;
                });
            },
            function (result) {
                modalService.openErrorResponse('Could not approve user', result, false);
            }
        );
    }

    $scope.bulkRejectUsers = function () {
        var ids = _.map($scope.data.selectedRequestedUsers, 'id');
        modalService.openConfirm('Reject Users?', 'reject', 'these users', null, function () {
            Users.bulk_reject(
                {},
                { ids: ids },
                function (result) {
                    _.each(ids, function (id) {
                        globalDataService.deleteUser(id);
                        PendingItemsService.markRequestedUserHandled(id);
                    });

                    _.remove($scope.data.requestedUsers, function (reqUser: any) {
                        return ids.indexOf(reqUser.id) >= 0;
                    });
                },
                function (result) {
                    modalService.openErrorResponse('Could not reject user', result, false);
                }
            );
        });
    };

    $scope.bulkApproveUsers = function () {
        var ids = _.map($scope.data.selectedRequestedUsers, 'id');

        modalService.openYesNoCancel(
            'Send welcome emails?',
            'Send welcome emails?',
            function () {
                bulkApproveUsers(ids, true);
            },
            function () {
                bulkApproveUsers(ids, false);
            }
        );
    };

    $scope.rejectUser = function (id) {
        modalService.openConfirm('Reject User?', 'reject', 'this user', id, function () {
            Users.reject(
                { userId: id },
                {},
                function (result) {
                    globalDataService.deleteUser(id);

                    _.remove($scope.data.requestedUsers, function (reqUser: any) {
                        return reqUser.id == id;
                    });
                    PendingItemsService.markRequestedUserHandled(id);
                },
                function (result) {
                    modalService.openErrorResponse('Could not reject user', result, false);
                }
            );
        });
    };

    $scope.viewApproveReject = function () {
        if ($scope.gdata.isMissionDirector) {
            return true;
        }
        return false;
    };

    $scope.findingsForReviewBulkActions = {
        bulkEditFindings: {
            action: function (finding, event) {
                FindingService.bulkEditFindings(event, $scope.data.selectedFindings, $scope);
            },
            class: 'btn-white',
            icon: 'fa-pencil',
            label: 'Edit',
            show: true,
            tooltip: 'Perform bulk edits',
        },
        bulkApproveFindings: {
            action: function (finding, event) {
                FindingService.bulkApproveFindings(event, $scope.data.selectedFindings, $scope.data);
            },
            class: 'btn-success',
            icon: 'fa-thumbs-up',
            label: 'Approve',
            show: $scope.viewApproveReject,
            tooltip: 'Perform bulk approvals',
        },
        bulkRejectFindings: {
            action: function (finding, event) {
                FindingService.bulkRejectFindings(event, $scope.data.selectedFindings, true, $scope);
            },
            class: 'btn-danger',
            icon: 'fa-thumbs-down',
            label: 'Reject',
            show: $scope.viewApproveReject,
            tooltip: 'Perform bulk rejections',
        },
        bulkDeleteFindings: {
            action: function (finding, event) {
                FindingService.bulkDeleteFindings(event, $scope.data.selectedFindings, $scope.data);
            },
            class: 'btn-black',
            icon: 'fa-times',
            label: 'Delete',
            show: true,
            tooltip: 'Perform bulk deletions',
        },
        bulkRequestFollowup: {
            action: function (finding, event) {
                FindingService.bulkRequestFollowup(event, $scope.data.selectedFindings, $scope.data);
            },
            class: 'btn-info',
            icon: 'fa-calendar-check-o',
            label: 'Request Follow-up',
            tooltip: 'Request a follow-up',
        },
        bulkUnrequestFollowup: {
            action: function (finding, event) {
                FindingService.bulkUnrequestFollowup(event, $scope.data.selectedFindings, $scope.data);
            },
            class: 'btn-info',
            icon: 'fa-calendar-times-o',
            label: 'Unrequest Follow-up',
            tooltip: 'Unrequest a follow-up',
        },
    };

    $scope.findingsForReviewActions = {
        approveFinding: {
            action: function (finding, event) {
                FindingService.approveFinding(finding.id);
            },
            class: 'btn-success',
            icon: 'fa-thumbs-up',
            label: 'Approve',
            show: $scope.viewApproveReject,
            tooltip: 'Approve',
        },
        rejectFinding: {
            action: function (finding, event) {
                FindingService.bulkRejectFindings(event, [finding], true, $scope);
            },
            class: 'btn-danger',
            icon: 'fa-thumbs-down',
            label: 'Reject',
            show: $scope.viewApproveReject,
            tooltip: 'Rejection',
        },
        deleteFinding: {
            action: function (finding) {
                FindingService.deleteFinding(finding);
            },
            class: 'btn-black',
            icon: 'fa-times',
            label: 'Delete',
            show: true,
            tooltip: 'Delete',
        },
        bulkRequestFollowup: {
            action: function (finding, event) {
                FindingService.bulkRequestFollowup(event, [finding], $scope.data);
            },
            class: 'btn-info',
            icon: 'fa-calendar-check-o',
            label: 'Request Follow-up',
            tooltip: 'Request a follow-up',
        },
        bulkUnrequestFollowup: {
            action: function (finding, event) {
                FindingService.bulkUnrequestFollowup(event, [finding], $scope.data);
            },
            class: 'btn-info',
            icon: 'fa-calendar-times-o',
            label: 'Unrequest Follow-up',
            tooltip: 'Unrequest a follow-up',
        },
    };

    /** Operation scope handlers. */
    $scope.authorizeOperation = function (operationId) {
        Operations.authorize({ operationId: operationId }, {}).$promise.then(function (result) {
            for (var i = 0; i < $scope.data.requestedOperations.length; i++) {
                if ($scope.data.requestedOperations[i].id === operationId) {
                    $scope.data.requestedOperations.splice(i, 1);
                    break;
                }
            }
        });
    };

    $scope.approveOperation = function (operationId) {
        Operations.approve({ operationId: operationId }, {}).$promise.then(function (result) {
            for (var i = 0; i < $scope.data.draftOperations.length; i++) {
                if ($scope.data.draftOperations[i].id === operationId) {
                    $scope.data.draftOperations.splice(i, 1);
                    break;
                }
            }
        });
    };

    $scope.selectAllDraftOperations = function () {
        $rootScope.selectAll($scope.data.allDraftOperationsSelected, $scope.data.draftOperations);
    };

    $scope.selectAllRequestedOperations = function () {
        $rootScope.selectAll($scope.data.allRequestedOperationsSelected, $scope.data.requestedOperations);
    };

    $scope.selectAllRequestedUsers = function () {
        $rootScope.selectAll($scope.data.allRequestedUsersSelected, $scope.data.requestedUsers);
    };

    $scope.selectAllFindings = function () {
        $rootScope.selectAll($scope.data.allFindingsSelected, $scope.data.findingsToProcess);
    };

    $scope.$watch('data.selectedDraftOperations.length', function (newValue, oldValue) {
        $scope.data.allDraftOperationsSelected = newValue !== 0 && newValue === $scope.data.draftOperations.length;
    });

    $scope.$watch('data.selectedRequestedOperations.length', function (newValue, oldValue) {
        $scope.data.allRequestedOperationsSelected = newValue !== 0 && newValue === $scope.data.requestedOperations.length;
    });

    $scope.$watch('data.selectedRequestedUsers.length', function (newValue, oldValue) {
        $scope.data.allRequestedUsersSelected = newValue !== 0 && newValue === $scope.data.requestedUsers.length;
    });

    $scope.bulkDisapproveDraftOperations = function (operationId) {
        if (operationId) {
            $scope.data.selectedDraftOperations = [{ id: operationId }];
        }
        $uibModal.open({
            templateUrl: '/app/operation/modalDisapproveOperation.html',
            controller: 'RejectionCtrl',
            size: 'lg',
            scope: $scope,
            resolve: {
                params: {
                    selObjs: 'selectedDraftOperations',
                    objs: 'draftOperations',
                    type: 'operation',
                    action: 'disapprove',
                    remove: true,
                },
            },
        });
    };

    $scope.bulkRejectRequestedOperations = function (operationId) {
        if (operationId) {
            $scope.data.selectedRequestedOperations = [{ id: operationId }];
        }
        $uibModal.open({
            templateUrl: '/app/md/modalRejectOperation.html',
            controller: 'RejectionCtrl',
            size: 'lg',
            scope: $scope,
            resolve: {
                params: {
                    selObjs: 'selectedRequestedOperations',
                    objs: 'requestedOperations',
                    type: 'operation',
                    action: 'reject',
                    remove: true,
                },
            },
        });
    };

    var bulkApproveOperations = function (operations) {
        angular.forEach(operations, function (operation, k) {
            if (operation.state === 'draft') {
                $scope.approveOperation(operation.id);
            }
            if (operation.state === 'requested') {
                $scope.authorizeOperation(operation.id);
            }
        });
    };

    $scope.bulkApproveDraftOperations = function () {
        bulkApproveOperations($scope.data.selectedDraftOperations);
    };

    $scope.bulkApproveRequestedOperations = function () {
        bulkApproveOperations($scope.data.selectedRequestedOperations);
    };

    $scope.cancelRequestStart = function (operation) {
        Operations.cancel_request_start({ operationId: operation.id }, {}, function (op) {
            _.remove($scope.data.operations, { id: operation.id });
        });
    };

    $scope.viewRejected = function (operation) {
        return !!(operation.state === 'rejected' && ($scope.gdata.isMissionDirector || operation.startedBy === my_user_id));
    };
});
