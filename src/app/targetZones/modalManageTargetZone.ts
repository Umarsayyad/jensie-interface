import app from '../app';
import * as _ from 'lodash';
import '../customer/manage/CondensedCustomerFilterComp';

app.component('modalManageTargetZone', {
    bindings: {
        zone: '<?',
        zoneSaved: '<?',
        modalTitle: '<?',
        close: '<',
        dismiss: '<',
    },
    templateUrl: '/app/targetZones/modalManageTargetZone.html',
    controller: function ($rootScope, globalDataService, modalService, TargetZones) {
        var ctrl = this;
        ctrl.isOperator = globalDataService.isOperator;
        ctrl.gdata = globalDataService;
        ctrl.catotheme = $rootScope.catotheme;

        ctrl.$onInit = function () {
            // Bindings are now available
            if (_.isUndefined(ctrl.zone)) {
                ctrl.modalTitle = ctrl.modalTitle || 'Create New Zone';
                ctrl.zone = {};
                ctrl.config = TargetZones.options();
                ctrl.setCustomer(_.get($rootScope.user, 'ui_data.selectedCustomer'));
            } else {
                ctrl.modalTitle = ctrl.modalTitle || 'Edit Zone';
                ctrl.zone = $rootScope.makePlainCopy(ctrl.zone);
                ctrl.config = TargetZones.options({ id: ctrl.zone.id });
            }
        };

        // Save/submit handlers
        ctrl.saveZone = function () {
            var sendZone = $rootScope.copyForRequest(ctrl.zone, ctrl.config);

            if (ctrl.zone.id) {
                ctrl.submission = TargetZones.update(sendZone, saveSuccess, saveError);
            } else {
                ctrl.submission = TargetZones.create(sendZone, saveSuccess, saveError);
            }
        };

        function saveSuccess(zone) {
            (ctrl.zoneSaved || _.noop)(zone);
            if (!ctrl.addAnother) {
                ctrl.close(zone);
            } else {
                ctrl.message = 'Zone saved successfully';
            }
        }

        function saveError(result) {
            modalService.openErrorResponse('Zone could not be saved.', result, false);
        }

        ctrl.setCustomer = function (customer) {
            if (customer) {
                ctrl.zone.customer = customer.id;
            } else {
                ctrl.zone.customer = undefined;
            }
        };
    },
});
