import app from '../app';
import * as _ from 'lodash';

import '../customer/manage/CondensedCustomerFilterComp';
import './modalManageTargetZone';

app.component('targetZonesTab', {
    bindings: {
        currentTab: '<',
    },
    templateUrl: '/app/targetZones/targetZonesTab.html',
    controller: function ($rootScope, $uibModal, NgTableParams, globalDataService, modalService, stateBuilder, MetaTargets, Targets, TargetZones) {
        var ctrl = this;
        var unregisterWatch;
        ctrl.userIsCustomer = false;
        ctrl.gdata = globalDataService;
        ctrl.catotheme = $rootScope.catotheme;
        ctrl._extraData = {};
        ctrl.targetTypes = [];
        var lastOpened = {};
        ctrl.customer = undefined;

        ctrl.$onInit = function () {
            closeLastOpen();

            if (globalDataService.isOperator) {
                ctrl.setCustomer(_.get($rootScope.user, 'ui_data.selectedCustomer'));
                unregisterWatch = $rootScope.$watch('user.ui_data.selectedCustomer', function (newVal, oldVal) {
                    if (newVal !== oldVal && !ctrl.catoNgDisabled) {
                        ctrl.setCustomer(newVal);
                    }
                });
            }
        };

        ctrl.$onChanges = function () {
            closeLastOpen();
        };

        Targets.options(function (result) {
            ctrl.targetTypes = _.map(_.sortBy(result.actions.POST.type.choices, 'display_name'), function (type) {
                return { id: type.value, title: type.display_name };
            });
        });

        ctrl.$onDestroy = function () {
            if (unregisterWatch) {
                unregisterWatch();
            }
        };

        ctrl.targetZoneActions = {
            editZone: {
                action: function (zone) {
                    closeLastOpen(zone);

                    var modal = stateBuilder.componentModalGenerator('modal-manage-target-zone', stateBuilder.zoneModalBindings, { zone });
                    $uibModal.open(modal);
                },
                class: 'btn-white',
                icon: 'fa-pencil',
                label: 'Edit',
                show: function () {
                    return ctrl.gdata.isMissionDirector || ctrl.gdata.isCustomer;
                },
                tooltip: 'Edit Zone',
            },
            deleteZone: {
                action: function (zone) {
                    modalService.openDelete('Confirmation', "Target Zone '" + zone.name + "'", {}, function () {
                        TargetZones.delete({ id: zone.id }, function () {}, modalService.openErrorResponseCB('Error deleting Target Zone'));
                    });
                },
                class: 'btn-white',
                icon: 'fa-trash',
                label: 'Delete',
                show: function (zone) {
                    return !(zone.external || (zone.targets && zone.targets.length)) && globalDataService.isMissionDirector;
                },
                tooltip: 'Delete Zone',
            },
        };

        ctrl.extraData = function (zone) {
            zone = zone || {};
            var extraData = ctrl._extraData[zone.id];
            if (!extraData) {
                extraData = ctrl._extraData[zone.id] = {};
            }
            return extraData;
        };

        ctrl.getSortValue = function (sortType) {
            if (_.isUndefined(sortType)) {
                return undefined;
            }

            return function (item) {
                if (sortType === 'customer') {
                    return ctrl.gdata.customerMap[item[sortType]].name;
                } else if (sortType === 'name') {
                    return item.name;
                }
            };
        };

        ctrl.setCustomer = function (customer) {
            ctrl.customer = customer ? customer.id : undefined;
        };

        ctrl.toggleTargetQuickView = function (zone) {
            ctrl.extraData(zone).targetPopoverIsOpen = !ctrl.extraData(zone).targetPopoverIsOpen;
            if (ctrl.extraData(zone).targetPopoverIsOpen) {
                closeLastOpen(zone, 'target');
                buildTargetsTable(zone, false);
            }
        };

        ctrl.toggleMetaTargetQuickView = function (zone) {
            ctrl.extraData(zone).metaTargetPopoverIsOpen = !ctrl.extraData(zone).metaTargetPopoverIsOpen;
            if (ctrl.extraData(zone).metaTargetPopoverIsOpen) {
                closeLastOpen(zone, 'metaTarget');
                buildTargetsTable(zone, true);
            }
        };

        function closeLastOpen(zone?: any, current?: any) {
            if (current !== 'target' || zone !== lastOpened) {
                ctrl.extraData(lastOpened).targetPopoverIsOpen = false;
            }
            if (current !== 'metaTarget' || zone !== lastOpened) {
                ctrl.extraData(lastOpened).metaTargetPopoverIsOpen = false;
            }
            lastOpened = zone;
        }

        function buildTargetsTable(zone, metaTarget) {
            // Reset the table so old targets or metatargets don't display before the request completes
            ctrl.extraData(zone).targetsTable = new NgTableParams({}, {});

            function processList(targets) {
                if (!metaTarget) {
                    // Copy target.value to target.name so the quick-view popover table can sort and filter targets
                    _.each(targets, function (target) {
                        target.name = target.value;
                    });
                }
                ctrl.extraData(zone).targetsTable = new NgTableParams(
                    {
                        count: 20,
                    },
                    {
                        counts: [5, 10, 20, 40],
                        dataset: targets,
                    }
                );
            }

            if (metaTarget) {
                ctrl.extraData(zone).resource = MetaTargets.list({ zone: zone.id }, processList);
            } else {
                ctrl.extraData(zone).resource = Targets.basicList({ zone: zone.id }, processList);
            }
        }
    },
});
