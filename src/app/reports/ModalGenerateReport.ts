import app from '../app';
import * as _ from 'lodash';

import '../common/applianceFilter/applianceFilterCtrl';
import '../customer/manage/CondensedCustomerFilterComp';

app.component('modalGenerateReport', {
    bindings: {
        service: '<?',
        close: '<',
        dismiss: '<',
    },
    templateUrl: '/app/reports/modalGenerateReport.html',
    controller: function ($q, $rootScope, FileSaver, moment, globalDataService, modalService, socketService, Reports) {
        var ctrl = this;
        const initialProgressMessage = 'Please wait while your report is generated...';
        ctrl.gdata = globalDataService;
        ctrl.applianceMap = globalDataService.applianceMap;
        ctrl.catotheme = $rootScope.catotheme;
        ctrl.user = $rootScope.user;
        ctrl.progressMessage = initialProgressMessage;
        ctrl.runningReports = {};
        ctrl.RUN_INDEX = 'run';
        ctrl.EXISTING_INDEX = 'existing';
        ctrl.currentTab = ctrl.RUN_INDEX;
        ctrl.customerId = undefined;
        ctrl.appliances = $rootScope.loadFilterableAppliances(this.gdata.appliances);

        ctrl.report_info = Reports.runnableReports();

        const handleUpdate = (reportUpdate, firstCheck: boolean = true) => {
            const report = ctrl.runningReports[reportUpdate.report_execution];
            if (report) {
                if (reportUpdate.status) {
                    ctrl.progressMessage += ` ${reportUpdate.status}...`;
                } else {
                    delete ctrl.runningReports[reportUpdate.report_execution];
                    if (reportUpdate.error) {
                        ctrl.reportRunningDeferred.reject(reportUpdate.error);
                        modalService.openErrorResponse('Report generation failed', reportUpdate.error, false);
                    } else {
                        ctrl.reportRunningDeferred.resolve(reportUpdate);
                        ctrl.downloadExisting(reportUpdate, true, false);
                    }
                }
            } else if (ctrl.runReportPromise && firstCheck) {
                ctrl.runReportPromise.then(() => handleUpdate(reportUpdate, false));
            }
        };
        const reportUnsubscribe = socketService.subscribeByModel('GeneratedReport', handleUpdate);

        ctrl.$onDestroy = function () {
            reportUnsubscribe();
        };

        // Model change functions
        ctrl.selectReport = function (report) {
            ctrl.report = report;
            if (ctrl.currentTab === ctrl.RUN_INDEX) {
                ctrl.loadReportParameters();
            } else if (ctrl.currentTab === ctrl.EXISTING_INDEX) {
                ctrl.loadExistingReports();
            }
        };

        // Model change functions
        ctrl.selectFormat = function (format) {
            ctrl.format = format ? format : 'pdf';
        };

        ctrl.loadReportParameters = function () {
            if (!ctrl.report.parameters) {
                ctrl.report.parameters = Reports.reportParameters(
                    {
                        report: ctrl.report.id,
                        provider: ctrl.report.provider,
                    },
                    function (parameters) {
                        _.each(parameters, function (param) {
                            let format;
                            if (param.type === 'date') {
                                format = 'YYYY-MM-DD';
                            } else if (param.type === 'datetime') {
                                format = 'YYYY-MM-DDThh:mm:ss';
                                param.default = moment(param.default).toDate();
                            } else if (param.type === 'time') {
                                format = 'hh:mm:ss';
                            }
                            if (format) {
                                if (param.validation.min) {
                                    param.validation.min = moment(param.validation.min, format).toDate();
                                }
                                if (param.validation.max) {
                                    param.validation.max = moment(param.validation.max, format).toDate();
                                }
                            }
                            if (param.id === 'service_id' && ctrl.service) {
                                param.value = ctrl.service.id;
                                param.hidden = true;
                            }
                            if (param.id === 'ApplianceID') {
                                param.label = 'Appliance';
                                param.hide_form = true;
                            }
                            if (param.id === 'ApplianceName') {
                                param.hidden = true;
                            }
                            if (ctrl.gdata.isOperator && param.id === 'customer_id') {
                                param.label = 'Customer';
                                param.hide_form = true;
                                param.value = _.get($rootScope.user, 'ui_data.selectedCustomer.id');
                            }
                        });
                    }
                );
            }
        };

        ctrl.loadExistingReports = function () {
            if (!ctrl.report.existing) {
                ctrl.report.existing = Reports.list({ provider_report_id: ctrl.report.id });
            }
        };

        ctrl.setCustomerParamId = function (customer, param) {
            var id = _.get(customer, 'id');
            param.value = id;
            ctrl.customer = customer;

            if (id) {
                ctrl.appliances = $rootScope.loadFilterableAppliances(this.gdata.appliancesByCustomer[id]);
            } else {
                ctrl.appliances = $rootScope.loadFilterableAppliances(this.gdata.appliances);
            }
        };

        ctrl.setApplianceParamId = function (appliance, param) {
            var id = _.get(appliance, 'id');
            param.value = id;
        };

        // Save/submit handlers
        ctrl.runReport = function () {
            var parameters = {};
            _.each(ctrl.report.parameters, function (param) {
                if (param.type === 'date') {
                    parameters[param.id] = moment(param.value).format('YYYY-MM-DD');
                } else if (param.type === 'datetime') {
                    parameters[param.id] = moment(param.date).format('YYYY-MM-DD') + 'T' + moment(param.time).format('HH:mm:ssZ');
                } else if (param.type === 'time') {
                    parameters[param.id] = moment(param.value).format('HH:mm:ss');
                } else if (param.id === 'ApplianceID') {
                    if (param.value === 'external') {
                        parameters[param.id] = -1;
                    } else {
                        parameters[param.id] = param.value;
                    }
                } else {
                    parameters[param.id] = param.value;
                }
            });
            var body = {
                parameters: parameters,
                format: ctrl.format,
                report: ctrl.report.id,
                provider: ctrl.report.provider,
            };
            ctrl.reportRunningDeferred = $q.defer();
            ctrl.runReportPromise = Reports.runReport(body).$promise.then(
                function (reportExecution) {
                    ctrl.runningReports[reportExecution.id] = reportExecution;
                    ctrl.progressMessage = `${initialProgressMessage} ${reportExecution.provider_execution_status}...`;
                },
                function (result) {
                    ctrl.reportRunningDeferred.reject(result);
                    modalService.openErrorResponse('Report could not be generated.', result, false);
                }
            );
        };

        ctrl.downloadExisting = function (report, close, addTimestamp) {
            Reports.download(
                { id: report.id },
                function (download) {
                    var name = report.name;
                    if (addTimestamp) {
                        var timestamp = moment(report.timestamp).local().format('YYYY-MM-DD hh-mm-ss A');
                        var split_name = name.split('.');
                        split_name.splice(split_name.length - 1, 0, timestamp);
                        name = split_name.join('.');
                    }
                    FileSaver.saveAs(download.blob, name);
                    if (close) {
                        ctrl.close(download);
                    }
                },
                modalService.openErrorResponseCB('Report download failed')
            );
        };
    },
});
