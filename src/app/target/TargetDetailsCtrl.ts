import app from '../app';
import * as _ from 'lodash';
import * as angular from 'angular';

import '../finding/reference/referencesViewer';
import '../note/NoteCtrl';
import '../operation/OperationService';
import '../tag/tagsEditor';
import './AddAccessGroupCtrl';

app.controller('TargetDetailsLoaderCtrl', function ($scope, $state, $uibModal) {
    if ($scope.gdata.isOperator) {
        $uibModal.open({
            templateUrl: '/app/target/modalTargetDetails.html',
            controller: 'TargetDetailsCtrl',
            size: 'xl',
            scope: $scope,
        });
    }
});

app.controller('TargetDetailsCtrl', function (
    $filter,
    $q,
    $scope,
    $state,
    $timeout,
    $uibModal,
    $uibModalInstance,
    ngTableService,
    modalService,
    socketService,
    AccessGroups,
    Findings,
    References,
    Operations,
    Targets,
    Conclusions,
    CIDRBlocks,
    FindingService,
    OperationService,
    TargetService
) {
    var targetId = parseInt($state.params.id);
    $scope.showFollowOnOperationLink = false;
    $scope.hideOperationCustomer = true;

    $scope.itemsPerPage = 25;
    $scope.currentFindingsPage = 1;
    $scope.currentOperationsPage = 1;
    $scope.operationState = 'created';
    $scope.operationMap = {};
    $scope.showTargetColumn = false;
    var lastStartedOn = null;
    $scope.lastOperation = null;

    $scope.alerts = [];
    $scope.conclusion = {};
    if ($scope.data === undefined) {
        $scope.data = {};
    }

    $scope.states = ['draft', 'created', 'requested', 'in progress', 'review', 'completed'];
    var targetMap = {};

    $scope.retestFinding = FindingService.retestFinding;
    $scope.stateClass = OperationService.stateClass;
    $scope.getTargetTypeIcon = TargetService.getTargetTypeIcon;
    $scope.setOperationState = function (state) {
        $scope.operationState = state;
        $scope.loadOperationPage(1);
    };

    if (_.isNil($scope.data.cpeOptions)) {
        $scope.data.cpeOptions = $scope.gdata.getCPE();
    }

    function loadReferences() {
        $scope.referencesTable = ngTableService.serverFilteredTable({
            resource: References.get,
            defaultParams: {
                target: $scope.target.id,
            },
        });
    }

    $scope.target = Targets.get({ targetId: targetId, detail: 'conclusions' });
    var targets = Targets.basicList(function (targets) {
        _.each(targets, function (t) {
            targetMap[t.id] = t;
        });
    });

    AccessGroups.list({ abbreviated: true }, function (result) {
        $scope.accessGroupMap = {};
        _.each(result, function (group) {
            $scope.accessGroupMap['gid-' + group.id] = group;
        });
    });

    $scope.getAccessGroupName = function (group_id) {
        if ($scope.accessGroupMap) {
            var group = $scope.accessGroupMap['gid-' + group_id];
            return group.name;
        }
    };

    CIDRBlocks.get({ target: targetId }, function (result) {
        $scope.cidrBlockMap = {};
        _.each(result['results'], function (block) {
            $scope.cidrBlockMap[block.id] = block;
        });
    });

    var targetUnsubscribe = socketService.subscribeByModel('target', function (target) {
        if (target.id === $scope.target.id) {
            $scope.updateObj($scope.target, target);
        }
        var oldTarget = targetMap[target.id];
        if (oldTarget) {
            $scope.updateObj(oldTarget, target);
        } else {
            targets.push(target);
            targetMap[target.id] = target;
        }
    });
    $scope.$on('$destroy', function () {
        targetUnsubscribe();
    });

    $scope.notes = Targets.getNotes({ targetId: targetId });

    $scope.operations = [];
    var allOperationsLoaded = $q.defer();

    function getMoreOps(page) {
        Operations.get({ target_multi: targetId, page_size: 30, page: page, target_detail: true }, function (res) {
            $scope.operations = $scope.operations.concat(res.results);
            if (res.next) {
                getMoreOps(page + 1);
            } else {
                allOperationsLoaded.resolve($scope.operations);
            }
        });
    }

    getMoreOps(1);
    $scope.findings = [];
    var allFindingsLoaded = $q.defer();

    function getMoreFindings(page) {
        Findings.get({ target_multi: targetId, page_size: 30, page: page, detail: 'attachments' }, function (res) {
            $scope.findings = $scope.findings.concat(res.results);
            if (res.next) {
                getMoreFindings(page + 1);
            } else {
                allFindingsLoaded.resolve($scope.findings);
            }
        });
    }

    getMoreFindings(1);
    $scope.targetDetailsLoading = $q
        .all([$scope.target.$promise, targets.$promise, allOperationsLoaded.promise, allFindingsLoaded.promise])
        .then(function () {
            $scope.originalTarget = { ...$scope.target };
            $scope.target.cpeTitle = $scope.getCPETitle($scope.target.cpe);
            $scope.cpeType = TargetService.getCPEType($scope.target.cpeTitle);
            $scope.cpeIcon = TargetService.getCPEIcon($scope.target.cpeTitle);
            _.each($scope.operations, function (operation) {
                $scope.operationMap[operation.id] = operation;
                if (operation.startedOn !== null) {
                    if (lastStartedOn === null) {
                        lastStartedOn = operation.startedOn;
                        $scope.lastOperation = operation;
                    } else {
                        if (operation.startedOn > lastStartedOn) {
                            lastStartedOn = operation.startedOn;
                            $scope.lastOperation = operation;
                        }
                    }
                }
            });

            loadReferences();

            _.each($scope.findings, function (finding) {
                var op = $scope.operationMap[finding.operation];
                if (op) {
                    finding.operation = op; // TODO: un-nest operation from finding in other places the /app/finding/finding_details.html template is used
                }
                finding.target = targetMap[finding.target];
                finding.discovered_target = targetMap[finding.discovered_target];
                finding.operator = $scope.gdata.userMap[finding.operator];
            });

            $scope.findingsCount = $scope.findings.length;
            $scope.numFindingPages = Math.ceil($scope.findingsCount / $scope.itemsPerPage);

            $scope.operationsCount = $scope.operations.length;
            $scope.numOperationPages = Math.ceil($scope.operationsCount / $scope.itemsPerPage);
            $scope.loadOperationPage(1);
        });

    $scope.loadOperationPage = function (page) {
        $scope.currentOperationsPage = page;
        $scope.filteredOperations = $filter('filter')($scope.operations, { state: $scope.operationState });
        //                <tr ng-repeat="target in customer.targets | limitTo : itemsPerPage : (currentPage - 1) * itemsPerPage" ng-class="{'inactive': !data.targets[target].active}">
        $scope.data.operations = $filter('limitTo')(
            $filter('orderBy')($scope.filteredOperations, $scope.orderByField, $scope.reverseSort),
            $scope.itemsPerPage,
            ($scope.currentOperationsPage - 1) * $scope.itemsPerPage
        );
    };

    $scope.getState = function () {
        return 'cato.loggedIn.operator';
    };

    $scope.getParams = function (operation) {
        return { service_name: operation.service, campaign_id: operation.campaign, operation_id: operation.id };
    };

    $scope.deleteFinding = function (finding) {
        modalService.openDelete('Delete Finding', 'Finding: <i>' + finding.title + '</i>', finding, function (finding) {
            Findings.delete({ findingId: finding.id }, {}, function () {
                _.remove($scope.findings, { id: finding.id });
            });
        });
    };

    $scope.deleteNote = function (note) {
        modalService.openConfirm('Really delete?', 'delete this note', null, null, function () {
            Targets.deleteNote(
                { targetId: $scope.target.id, noteId: note.id },
                note,
                function (note) {
                    _.remove($scope.notes, { id: note.id });
                },
                function (res) {
                    modalService.openErrorResponse('Note could not be deleted.', res, false);
                }
            );
        });
    };

    $scope.addNote = function (note) {
        var modalInstance = $uibModal.open({
            templateUrl: '/app/note/modalNote.html',
            controller: 'NoteCtrl',
            size: 'lg',
            scope: $scope,
            resolve: {
                params: {
                    targetId: $scope.target.id,
                    note: note,
                    type: 'target',
                },
            },
        });

        modalInstance.result.then(function (note) {
            _.remove($scope.notes, { id: note.id });
            $scope.notes.push(note);
        });
    };

    function updateCPE(cpeName, cpeTitle) {
        $scope.target.cpe = cpeName;
        var targetSubmit = angular.copy($scope.target);
        delete targetSubmit['operations'];
        delete targetSubmit.owner;
        delete targetSubmit.locked;

        Targets.update(
            { targetId: $scope.target.id },
            targetSubmit,
            function (target) {
                var alert = {
                    id: 'cpe' + Math.floor(Math.random() * 200),
                    type: 'success',
                    msg: 'Target CPE updated. ' + new Date(),
                };
                $scope.alerts.push(alert);
                $scope.cpeIcon = TargetService.getCPEIcon(cpeTitle);
                $scope.cpeType = TargetService.getCPEType(cpeTitle);
                $scope.target.cpeTitle = cpeTitle;
                if ($scope.targetTable) {
                    var oldTarget = _.find($scope.targetTable.data, { id: target.id });
                    $scope.updateObjOld(oldTarget, target);
                }
            },
            function (err) {
                var alert = {
                    id: 'cpe' + Math.floor(Math.random() * 200),
                    type: 'danger',
                    msg: 'Error updating CPE: ' + $scope.parseErrorResponse(err),
                };
                $scope.alerts.push(alert);
            }
        );
        $scope.editingRawCPE = false;
    }

    $scope.quickUpdateCPE = function (cpestring) {
        $scope.target.cpe = cpestring;
        updateCPE($scope.target.cpe, $scope.getCPETitle($scope.target.cpe));
    };

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.getCPETitle = function (cpeName) {
        if (cpeName === undefined || cpeName.length < 1) return;
        if (!$scope.data.cpeOptions.$resolved) {
            $scope.data.cpeOptions.$promise.then(function () {
                $scope.target.cpeTitle = $scope.getCPETitle(cpeName);
                $scope.cpeType = TargetService.getCPEType($scope.target.cpeTitle);
                if ($scope.cpeType === 'Unknown') {
                    $scope.cpeType = TargetService.getCPETypeByName(cpeName);
                }
                $scope.cpeIcon = TargetService.getCPEIcon($scope.cpeTitle);
            });
            return;
        }
        var cpeEntry: any = _.find($scope.data.cpeOptions, { name: cpeName });
        if (cpeEntry) {
            return cpeEntry.title;
        }
        return cpeName;
    };

    $scope.selectCPE = function ($item, $model, $label, $event) {
        updateCPE($item.name, $item.title);
    };

    $scope.deleteCPE = function () {
        updateCPE('', null);
    };

    $scope.setCPEbyName = function (cpeName) {
        updateCPE(cpeName, $scope.getCPETitle(cpeName));
    };

    $scope.editRawCPE = function () {
        $scope.editingRawCPE = true;
    };

    $scope.addToAccessGroup = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/app/target/modalAddAccessGroup.html',
            controller: 'AddAccessGroupCtrl',
            size: 'lg',
            scope: $scope,
            resolve: {
                params: {
                    target: $scope.target,
                },
            },
        });

        modalInstance.result.then(function (group) {
            $scope.accessGroupMap['gid-' + group.id] = group;
            $scope.target.access_groups.push(group.id);
        });
    };

    $scope.removeAccessGroup = function (access_group) {
        var groups = [...$scope.target.access_groups];

        var idx = groups.indexOf(access_group);
        groups.splice(idx, 1);

        Targets.patch(
            { targetId: targetId },
            { access_groups: groups },
            function () {
                $scope.target.access_groups = groups;
            },
            function (res) {
                modalService.openErrorResponse('Group could not be removed.', res, false);
            }
        );
    };

    function removeAccessGroup(result) {
        modalService.openErrorResponse('Access group could not be removed.', result, false);
    }

    $scope.clearConclusion = function () {
        $scope.conclusion.text = '';
    };

    $scope.saveConclusion = function (conclusionText) {
        var conclusion = {
            target: $scope.target.id,
            value: conclusionText,
        };
        Conclusions.add(conclusion).$promise.then(function (conclusion) {
            $scope.target.conclusions.unshift(conclusion);
            $scope.clearConclusion();
        });
    };

    $scope.markConclusionAs = function (action, conclusionId) {
        if (action === 'invalid') {
            Conclusions.invalidate({ conclusionId: conclusionId }).$promise.then(function (conclusion) {
                $scope.updateObjOld(_.find($scope.target.conclusions, { id: conclusionId }), conclusion);
            });
        } else {
            Conclusions.validate({ conclusionId: conclusionId }).$promise.then(function (conclusion) {
                $scope.updateObjOld(_.find($scope.target.conclusions, { id: conclusionId }), conclusion);
            });
        }
    };

    $scope.updateTargetsTags = function () {
        Targets.patch(
            { targetId: $scope.target.id },
            { tags: $scope.target.tags },
            _.noop,
            modalService.openErrorResponseCB("Could not update target's tags")
        );
    };

    $scope.submitTarget = function () {
        let targetSubmit: any = {};
        if ($scope.target.is_explicitly_aliased) {
            targetSubmit.alias = $scope.target.alias;
        } else if ($scope.target.explicit_alias) {
            targetSubmit.alias = $scope.target.explicit_alias;
        }
        targetSubmit.is_explicitly_aliased = !(targetSubmit.alias === undefined || targetSubmit.alias === '');
        if (targetSubmit.alias !== $scope.originalTarget.alias) {
            Targets.patch({ targetId: $scope.target.id }, targetSubmit).$promise.then(function (target) {
                if ($scope.targetTable) {
                    let oldTarget = _.find($scope.targetTable.data, { id: target.id });
                    $scope.updateObjOld(oldTarget, target);
                }
                $uibModalInstance.close();
            }, modalService.openErrorResponseCB('Error updating the Target.'));
        } else {
            $uibModalInstance.close();
        }
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };

    function modalClose() {
        if (/targetDetails/i.test($state.current.name) && _.isNull($state.transition)) {
            $state.go('^');
        }
    }

    $uibModalInstance.result.then(modalClose, modalClose);
});
