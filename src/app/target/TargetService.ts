import app from '../app';
import * as _ from 'lodash';

app.service('TargetService', function () {
    function getTargetTypeIcon(targetType) {
        switch (targetType) {
            case 'ipaddress':
                return 'fa-location-arrow';
                break;
            case 'bluetoothdevice':
                return 'fa-bluetooth';
                break;
            case 'location':
                return 'fa-map-marker';
                break;
            case 'wirelessnetwork':
                return 'fa-wifi';
                break;
            case 'wirelessdevice':
                return 'fa-tablet';
                break;
            case 'cidrblock':
                return 'fa-linode';
                break;
            case 'emailaddress':
                return 'fa-envelope-o';
                break;
            case 'domain':
                return 'fa-building-o';
                break;
            case 'hostname':
                return 'fa-address-card-o';
                break;
            case 'fqdn':
                return 'fa-building';
                break;
            case 'url':
                return 'fa-link';
                break;
            case 'metatarget':
                return 'fa-cubes';
                break;
            case 'company':
                return 'fa-building';
                break;
            case 'person':
                return 'fa-user';
                break;
            case 'useraccount':
                return 'fa-terminal';
                break;
            case 'database':
                return 'fa-database';
                break;
            case 'application':
                return 'fa-server';
                break;
            case 'macaddress':
                return 'fa-laptop';
                break;
            case 'service':
                return 'fa-gears';
                break;
            case 'device':
                return 'fa-mobile';
                break;
            case 'icsmonitoreddevice':
                return 'fa-briefcase';
                break;
            case 'icsmonitoringappliance':
                return 'fa-podcast';
                break;
            default:
                // unknown
                return 'fa-question-circle';
        }
    }

    function getTargetTypeIconUnicodeValue(targetType) {
        switch (targetType) {
            case 'appliance':
                return '\uf2db ';
                break;
            case 'ipaddress':
                return '\uf124 ';
                break;
            case 'bluetoothdevice':
                return '\uf293 ';
                break;
            case 'location':
                return '\uf3c5 ';
                break;
            case 'wirelessnetwork':
                return '\uf1eb ';
                break;
            case 'wirelessdevice':
                return '\uf10a ';
                break;
            case 'cidrblock':
                return '\uf2b8 ';
                break;
            case 'emailaddress':
                return '\uf003 ';
                break;
            case 'domain':
                return '\uf0f7 ';
                break;
            case 'hostname':
                return '\uf2bc ';
                break;
            case 'fqdn':
                return '\uf1ad ';
                break;
            case 'url':
                return '\uf0c1 ';
                break;
            case 'metatarget':
                return '\uf1b3 ';
                break;
            case 'company':
                return '\uf1ad ';
                break;
            case 'person':
                return '\uf007 ';
                break;
            case 'useraccount':
                return '\uf120 ';
                break;
            case 'database':
                return '\uf1c0 ';
                break;
            case 'application':
                return '\uf233 ';
                break;
            case 'macaddress':
                return '\uf109 ';
                break;
            case 'service':
                return '\uf085 ';
                break;
            case 'device':
                return '\uf10b ';
                break;
            case 'icsmonitoreddevice':
                return '\uf0b1 ';
                break;
            case 'icsmonitoringappliance':
                return '\uf2ce ';
                break;
            default:
                // unknown
                return '\uf29c ';
        }
    }

    function getCPEType(cpeTitle) {
        if (_.isNil(cpeTitle)) {
            return null;
        }
        if (cpeTitle.match(/linux/i) !== null) {
            return 'Linux';
        } else if (cpeTitle.match(/windows/i) !== null) {
            return 'Windows';
        } else if (cpeTitle.match(/mac/i) !== null || cpeTitle.match(/apple/i) !== null) {
            return 'Apple';
        } else if (cpeTitle.match(/android/i) !== null) {
            return 'Android';
        } else {
            return 'Unknown';
        }
    }

    function getCPETypeByName(cpeName) {
        if (cpeName.match(/cpe\:\/a/) !== null) {
            return 'APP';
        } else if (cpeName.match(/cpe:\/h/) !== null) {
            return 'HW';
        } else if (cpeName.match(/cpe\:\/o/) !== null) {
            return 'OS';
        } else {
            return 'Unknown';
        }
    }

    function getCPEIcon(cpeTitle) {
        var cpeType = getCPEType(cpeTitle);
        return getCPEIconByType(cpeType);
    }

    function getCPEIconByType(cpeType) {
        if (cpeType === null) {
            return '';
        }
        cpeType = cpeType.toLowerCase();
        if (cpeType !== 'unknown') {
            return 'fa-' + cpeType;
        } else {
            return 'fa-question-circle';
        }
    }

    function getCPEIconUnicode(cpeType) {
        if (cpeType === null) {
            return '';
        }
        cpeType = cpeType.toLowerCase();

        switch (cpeType) {
            case 'linux':
                return '\uf17c';
            case 'windows':
                return '\uf17a';
            case 'apple':
                return '\uf179';
            case 'android':
                return '\uf17b';
            default:
                return '\uf059';
        }
    }

    return {
        getTargetTypeIcon: getTargetTypeIcon,
        getTargetTypeIconUnicodeValue: getTargetTypeIconUnicodeValue,
        getCPEType: getCPEType,
        getCPETypeByName: getCPETypeByName,
        getCPEIcon: getCPEIcon,
        getCPEIconByType: getCPEIconByType,
        getCPEIconUnicode: getCPEIconUnicode,
    };
});
