import app from '../app';
import { config } from '@cato/config';
import * as _ from 'lodash';
import * as angular from 'angular';
import catotheme from '@cato/config/themes/catotheme';

import * as cytoscape from 'cytoscape';
import { CytoscapeOptions } from 'cytoscape';
import * as dagre from 'dagre';
import * as panzoom from 'cytoscape-panzoom';
import * as cydagre from 'cytoscape-dagre';
import * as spread from 'cytoscape-spread';
import * as cxtmenu from 'cytoscape-cxtmenu';

import '../accessGroups/accessGroupsTab';
import '../cidr/cidrTab';
import '../common/actionsDropdownComp';
import '../customer/manage/CondensedCustomerFilterComp';
import '../finding/reference/referencesViewer';
import '../tag/modalTagTargets';
import '../tag/tagsEditor';
import '../tag/targetTagsTab';
import '../targetZones/targetZonesTab';

app.controller('TargetsCtrl', function (
    $q,
    $scope,
    $rootScope,
    $state,
    $timeout,
    $uibModal,
    $window,
    modalService,
    socketService,
    stateBuilder,
    FileSaver,
    Findings,
    FindingService,
    globalDataService,
    MessageService,
    Operations,
    Targets,
    TargetCsv,
    TargetLinks,
    TargetService,
    MetaTargets,
    ngTableService,
    NgTableParams
) {
    $scope.downloadCsv = function () {
        var timestamp = new Date().toISOString();
        var nickname = $rootScope.gdata.customerMap[$scope.selectedCustomer.id].nickname;
        TargetCsv.download(
            { customer: $scope.selectedCustomer.id },
            function (data) {
                FileSaver.saveAs(data.blob, `${catotheme.appName.toLowerCase()}_targets_${nickname}_${timestamp}.csv`);
            },
            modalService.openErrorResponseCB('Unable to download CSV')
        );
    };

    $scope.primaryScope = !$scope.primaryScope ? $scope : $scope.primaryScope;

    $scope.types = [];
    $scope.tagFilterData = {};
    $scope.showInactive = 'active';
    $scope.showThirdParty = 'all';
    var operationCache = {};
    $scope.getCyNodeInterval = null;
    $scope.graphBusy = null;

    $scope.data = {
        selectedTargets: [],
        targetsSelected: false,
    };

    $scope.nodes = [];
    $scope.edges = [];

    $scope.already_loaded = [];
    $scope.which_port_scan = {};

    $scope.should_show_graph_tools = false;

    panzoom(cytoscape); // register extension
    cydagre(cytoscape, dagre); // register extension
    spread(cytoscape);

    // JAC: I have not found a way to test to see if cxtmenu is already
    // registered without just trying
    // The error produced as a result is:
    //
    // cytoscape.min.js?version=VERSION:1 Can not register `cxtmenu` for
    // `core` since `cxtmenu` already exists in the prototype and can
    // not be overridden
    //
    // This is an issue with the cytoscape library and not something we
    // can fix without taking on their codebase.
    //
    // if (!cytoscape.cxtmenu) {
    //     cytoscape.use(cxtmenu);     // register extension
    // }

    cytoscape.use(cxtmenu);

    var appWindow = angular.element($window);

    $scope.consoleLog = function (input) {
        //console.log(input);
        return true;
    };

    $scope.severities = [
        { id: '0', title: 'NONE' },
        { id: '1', title: 'LOW' },
        { id: '2', title: 'MEDIUM' },
        { id: '3', title: 'HIGH' },
        { id: '4', title: 'CRITICAL' },
    ];
    $scope.findingsTableData = [];
    $scope.numberOfFindings = -1;
    $scope.findingsProcessed = 0;
    $scope.findingsTableParams = {
        count: 14, // initial page size
        // total: 1
    };
    $scope.findingsTableSettings = {
        // page size buttons (right set of buttons in demo)
        counts: [],
        dataOptions: { applySort: true },
        // determines the pager buttons (left set of buttons in demo)
        paginationMaxBlocks: 4,
        paginationMinBlocks: 2,
        dataset: $scope.findingsTableData,
    };

    $scope.$watch('findingsProcessed === numberOfFindings', function (newValue, oldValue) {
        //this how we prevent second call
        if ($scope.numberOfFindings > -1 && newValue != oldValue) {
            $scope.findingsProcessed = 0;
            $scope.numberOfFindings = -1;
            //console.log("Reloading findings table.");
            $scope.findingsTableSettings.dataset = $scope.findingsTableData;
            $scope.findingsTableSettings.data = $scope.findingsTableData;
            // $scope.findingsTable.reload();
            $scope.findingsTable = new NgTableParams($scope.findingsTableParams, $scope.findingsTableSettings);
            //console.log("Reloaded findings table.");
        }
    });

    $scope.setupMutationObserver = function () {
        // Select the node that will be observed for mutations
        var targetNode = angular.element('#visualizeTargetSubTab')[0];

        // Options for the observer (which mutations to observe)
        var config = { attributes: true, childList: true, subtree: true };

        // Callback function to execute when mutations are observed
        var callback = function (mutationsList) {
            //console.log("Mutation Observer callback running");
            for (var i = 0; i < mutationsList.length; i++) {
                var mutation = mutationsList[i];
                if (mutation.type == 'childList') {
                    if (angular.element('#findingsTable') && angular.element('#findingsTable')[0]) {
                        $scope.calculateCytoWidth();
                    }
                    //
                    if (angular.element('#cytoscape_container') && angular.element('#cytoscape_container')[0]) {
                        $scope.calculateCytoHeight();
                    }
                }
            }
        };

        // Create an observer instance linked to the callback function
        var observer = new MutationObserver(callback);

        // Start observing the target node for configured mutations
        observer.observe(targetNode, config);
    };

    $scope.setupMutationObserver();

    function reloadGraphOnResize() {
        $scope.calculateCytoHeight();
        $scope.calculateCytoWidth();
        if ($scope.data.activeCategory === 'Visualize' && $scope.getCyNodeInterval === null && $scope.graphBusy === null) {
            $scope.reloadGraph(false);
        }
    }
    appWindow.bind('resize', reloadGraphOnResize);

    $scope.$on('sidebarCollapse', function () {
        $scope.reloadGraph(false);
    });

    $scope.calculateCytoHeight = function () {
        if ($scope.data.activeCategory === 'Visualize') {
            var screenHeight = $window.innerHeight - 300;
            $scope.findingsTableParams.count = (screenHeight - 135) / 40;
            // return screenHeight;
            angular.element('#cytoscape_container').css('height', screenHeight);
        }
    };

    $scope.calculateCytoWidth = function () {
        if ($scope.data.activeCategory === 'Visualize' && $scope.showFindingsInBar && angular.element('#graphContainer')[0]) {
            var findingsTableWidth = angular.element('#findingsTable')[0] ? angular.element('#findingsTable')[0].offsetWidth + 15 : 0;
            var graphContainerWidth = angular.element('#graphContainer')[0].offsetWidth;
            var cytoscapeContainerWidth = graphContainerWidth - findingsTableWidth;

            //JAC: This first condition is for when the findingsBar is open when the user swtiches to
            //Another Targets subtab and back to Visualize.  Under that scenario the calculations for
            //the width of the graph were not running correctly, so I decided to just capture the
            //original value and reuse it.  This seems to work.  I just hope we always have a previous
            //value whenever the graphContainerWidth <= 0.
            if (graphContainerWidth <= 0 && $scope.prevCytoscapeContainerWidth) {
                angular.element('#cytoscape_container').css('width', $scope.prevCytoscapeContainerWidth);
            } else if (findingsTableWidth > 0) {
                angular.element('#cytoscape_container').css('width', cytoscapeContainerWidth);
                $scope.prevCytoscapeContainerWidth = cytoscapeContainerWidth;
            } else {
                angular.element('#cytoscape_container').css('width', '100%');
            }
        }
    };

    $scope.searchBox = {
        history: [],
        text: '',
        placeholder: 'Search for a target',
        submit: function () {
            if ($scope.searchBox.text) {
                $scope.bfs($scope.searchBox.text, true);
            }
        },
    };

    $scope.bfs = function (value, exactMatchNotNeeded) {
        // $scope = $scope.$parent.$parent;
        if (!value || (value && typeof value !== 'string' && !value.value && typeof value.value !== 'string' && $scope.target)) {
            value = $scope.target.value;
        }
        // $scope.cy.nodes().unselect();
        var bfs = $scope.cy.elements().bfs({
            roots: $scope.cy.nodes(),
            visit: function (v, e, u, i, depth) {
                //Determine whether an icon has been added to the front of the label and if so take it off
                var labelWithoutIcon = v.data().label;
                if (labelWithoutIcon.charCodeAt(0) > 255 && labelWithoutIcon.indexOf(' ') === 1) {
                    labelWithoutIcon = labelWithoutIcon.slice(2);
                }
                if (
                    v.data().extraData.type !== undefined &&
                    ((!exactMatchNotNeeded && labelWithoutIcon && labelWithoutIcon === value) ||
                        (exactMatchNotNeeded && labelWithoutIcon && labelWithoutIcon.indexOf(value) > -1))
                ) {
                    if ($scope.searchBox.history.indexOf(v) > -1 || v.css('display') === 'none') {
                        return;
                    } else {
                        $scope.searchBox.history.push(v);
                    }

                    $scope.targetNode = v;
                    $scope.target = v.data().extraData;
                    $scope.collectFindings($scope.target);
                    $scope.cy.zoom(1.25);
                    $scope.cy.center(v);
                    v.select();

                    if ($scope.visualizeBusy) {
                        $scope.visualizeBusy.resolve(true);
                        $scope.visualizeBusy = null;
                    }

                    return true;
                }
            },
            directed: false,
        });
        if (bfs.found.length === 0 && $scope.searchBox.history.length > 0) {
            $scope.searchBox.history = [];
            $scope.bfs(value, exactMatchNotNeeded);
        } else {
            bfs.path.select();
        }
    };

    $scope.collectFindings = function (target) {
        $scope.primaryScope.target = target;
        Findings.basicList({ target: target.id, vis_data_only: true }).$promise.then(function (findings) {
            console.log('retrieved findings');
            // var NodesLoaded = false;
            $scope.primaryScope.numberOfFindings = findings.length;
            $scope.primaryScope.findingsTableData = [];
            $scope.primaryScope.findingsProcessed = 0;
            _.forEach(findings, function (el, index, arr) {
                $scope.primaryScope.findingsProcessed++;

                if (el.title.indexOf('Machine-readable ') === 0 || el.title.indexOf('Added new IP Address Target(s)') === 0) {
                    return;
                }

                var title = el.title.replace(' on ', '\non ');
                if (title.length > 25) {
                    title = title.slice(0, 22) + '...';
                }

                var severity = FindingService.SEVERITY_LEVEL_MAP[el.severity];
                el = angular.extend({}, el, {
                    label: severity.label,
                    title: title,
                    style: severity.style,
                });
                $scope.primaryScope.findingsTableData.push(el);
            });
        });
    };

    function displayFindingsDetailsModal(finding) {
        Findings.get({ findingId: finding.id }).$promise.then(function (finding) {
            getOp(0, finding.operation).$promise.then(function (operation) {
                finding.operation = operation.results[0];
                finding.target = _.find($scope.targetTable.data, { id: finding.target });
                var data = {
                    finding: finding,
                    gdata: globalDataService,
                    catotheme: $rootScope.catotheme,
                    testBool: false,
                };
                var dataURL = '/app/target/findingDetailsModal.html';
                modalService.openDataURL(data, dataURL);
            });
        });
    }

    $scope.displayFindingsDetailsModal = displayFindingsDetailsModal;

    $scope.graphLayoutObj = {
        name: 'dagre',
        nodeSep: 20,
        padding: 10,
        minLen: function (edge) {
            //console.log([edge,edge.source().outdegree()])
            var numSiblings = edge.source().outdegree();
            var numTargetChildren = edge.target().outdegree();
            if (numSiblings <= 2 || numSiblings + numTargetChildren <= 5) {
                return 1;
            } else {
                //console.log([edge.data().label, numSiblings, numTargetChildren, Math.floor(Math.sqrt(numSiblings)), edge]);
                if (numSiblings < 5) {
                    numSiblings = 1;
                } else if (numSiblings > 64) {
                    numSiblings = 64 + Math.floor(Math.sqrt(numSiblings - 64));
                }
                if (numTargetChildren <= 5) {
                    numTargetChildren = 0;
                } else if (numTargetChildren <= 10) {
                    numTargetChildren = 1;
                } else {
                    numTargetChildren = 2;
                }
                return numSiblings + numTargetChildren;
                //return Math.ceil(numSiblings) + 0.5 + Math.floor(Math.sqrt(numTargetChildren));
                //return Math.ceil(numSiblings/2) + Math.floor(Math.sqrt(numTargetChildren));
                //return numSiblings/2 + Math.floor(Math.sqrt(numTargetChildren));
            }
        },
        //name: 'breadthfirst', directed: 'true', maximalAdjustments: 50,
    };

    var panzoom_defaults = {
        zoomFactor: 0.05, // zoom factor per zoom tick
        zoomDelay: 45, // how many ms between zoom ticks
        minZoom: 0.1, // min zoom level
        maxZoom: 10, // max zoom level
        fitPadding: 50, // padding when fitting
        panSpeed: 10, // how many ms in between pan ticks
        panDistance: 10, // max pan distance per tick
        panDragAreaSize: 75, // the length of the pan drag box in which the vector for panning is calculated (bigger = finer control of pan speed and direction)
        panMinPercentSpeed: 0.25, // the slowest speed we can pan by (as a percent of panSpeed)
        panInactiveArea: 8, // radius of inactive area in pan drag box
        panIndicatorMinOpacity: 0.5, // min opacity of pan indicator (the draggable nib); scales from this to 1.0
        zoomOnly: false, // a minimal version of the ui only with zooming (useful on systems with bad mousewheel resolution)
        fitSelector: undefined, // selector of elements to fit
        animateOnFit: function () {
            // whether to animate on fit
            return false;
        },
        fitAnimationDuration: 1000, // duration of animation on fit

        // icon class names
        sliderHandleIcon: 'fa fa-minus',
        zoomInIcon: 'fa fa-plus',
        zoomOutIcon: 'fa fa-minus',
        resetIcon: 'fa fa-expand',
    };

    $scope.operations = [];
    var allOperationsLoaded = $q.defer();

    function getOp(page, operationID) {
        return Operations.get({ id__in: operationID, target_detail: true }, function (res) {
            $scope.operations = $scope.operations.concat(res.results);
            if (res.next) {
                getOp(page + 1, operationID);
            } else {
                allOperationsLoaded.resolve($scope.operations);
            }
        });
    }

    // the default values of each option are outlined below:
    var cxtmenu_defaults = {
        menuRadius: 100, // the radius of the circular menu in pixels
        selector: 'node.target', // elements matching this Cytoscape.js selector will trigger cxtmenus
        commands: [
            // an array of commands to list in the menu or a function that returns the array

            // { // prev port scan
            //     fillColor: 'rgba(200, 200, 200, 0.75)', // optional: custom background color for item
            //     content: 'Prev Port Scan', // html/text content to be displayed in the menu
            //     contentStyle: {}, // css key:value pairs to set the command's css in js if you want
            //     select: function (ele) { // a function to execute when the command is selected
            //         if (ele.data()['extraData'].id in $scope.which_port_scan) {
            //             console.log("retrieve prior port scan")
            //             Targets.retrieve_port_scan({targetId: ele.data()['extraData'].id, scan_number: $scope.which_port_scan[ele.data()['extraData'].id]}).$promise
            //                 .then(function (result) {
            //                     ports = result.services;
            //                     _.forEach(ports, function (el, index, arr) {
            //                         el.original_id = el.id;
            //                         $scope.nodes.push({data: {id: el.port, label: el.transport_protocol + ":" + el.port, extraData: el}, classes: el.transport_protocol});
            //                         $scope.edges.push({data: {target: el.port, source: ele.data()['id']}});
            //                     });
            //                     $scope.loadGraph();
            //                     $scope.which_port_scan[ele.data()['extraData'].id] = $scope.which_port_scan[ele.data()['extraData'].id] + 1
            //                 }, function () {
            //                     modalService.openErrorResponseCB('no previous port scan found bro');
            //                 });
            //         } else {
            //             $scope.which_port_scan[ele.data()['extraData'].id] = 1;
            //             if (ele.data()['extraData']['type'] == 'ipaddress' || ele.data()['type'] == 'fqdn' || ele.data()['type'] == 'hostname' || ele.data()['type'] == 'domain') {
            //                 console.log('retrieve latest port scan');
            //                 Targets.latest_port_scan({targetId: ele.data()['extraData'].id}).$promise
            //                     .then(function (result) {
            //                         ports = result.services;
            //                         _.forEach(ports, function (el, index, arr) {
            //                             el.original_id = el.id;
            //                             $scope.nodes.push({data: {id: el.port, label: el.transport_protocol + ":" + el.port, extraData: el}, classes: el.transport_protocol});
            //                             $scope.edges.push({data: {target: el.port, source: ele.data()['id']}});
            //                         });
            //                         $scope.loadGraph();
            //                     }, function () {
            //                         modalService.openErrorResponseCB('No Port Scan data exists for this target');
            //                     });
            //             }
            //         }
            //     },
            //     enabled: true // whether the command is selectable
            // },
            {
                // load details
                fillColor: 'rgba(200, 200, 200, 0.75)', // optional: custom background color for item
                content: 'Details', // html/text content to be displayed in the menu
                contentStyle: {}, // css key:value pairs to set the command's css in js if you want
                select: function (ele) {
                    // a function to execute when the command is selected
                    //console.log(ele.id()) // `ele` holds the reference to the active element
                    $state.go('.targetDetails', { id: ele.data()['extraData'].id });
                },
                enabled: true, // whether the command is selectable
            },
            {
                // show findings
                fillColor: 'rgba(200, 200, 200, 0.75)', // optional: custom background color for item
                content: 'Show Findings', // html/text content to be displayed in the menu
                contentStyle: {}, // css key:value pairs to set the command's css in js if you want
                select: function (ele) {
                    // a function to execute when the command is selected
                    //console.log(ele.id()) // `ele` holds the reference to the active element
                    $scope.load_targets_for_visualization_with_findings(ele.data()['extraData']);
                },
                enabled: true,
            },
            {
                // hide children
                fillColor: 'rgba(200, 200, 200, 0.75)', // optional: custom background color for item
                content: 'Collapse', // html/text content to be displayed in the menu
                contentStyle: {}, // css key:value pairs to set the command's css in js if you want
                select: function (ele) {
                    // a function to execute when the command is selected
                    //console.log(ele.id()) // `ele` holds the reference to the active element
                    if (ele.connectedEdges().targets().length > 0) {
                        if (ele.connectedEdges().targets()[0].style('display') == 'none') {
                            //show the nodes and edges
                            ele.connectedEdges().targets().style('display', 'element');
                        } else {
                            //hide the children nodes and edges recursively
                            ele.successors().targets().style('display', 'none');
                        }
                    }
                },
                enabled: true, // whether the command is selectable
            },
            {
                // remove from graph
                fillColor: 'rgba(200, 200, 200, 0.75)', // optional: custom background color for item
                content: 'Remove', // html/text content to be displayed in the menu
                contentStyle: {}, // css key:value pairs to set the command's css in js if you want
                select: function (ele) {
                    // a function to execute when the command is selected
                    if (ele.connectedEdges().targets().length > 0) {
                        if (ele.connectedEdges().targets()[0].style('display') == 'none') {
                            //show the nodes and edges
                            ele.connectedEdges().targets().style('display', 'element');
                        } else {
                            //hide the children nodes and edges recursively
                            ele.successors().targets().style('display', 'none');
                        }
                    }
                    ele.style('display', 'none');
                },
                enabled: true, // whether the command is selectable
            },
        ], // function( ele ){ return [ /*...*/ ] }, // example function for commands
        fillColor: 'rgba(0, 0, 0, 0.75)', // the background colour of the menu
        activeFillColor: 'rgba(1, 105, 217, 0.75)', // the colour used to indicate the selected command
        activePadding: 20, // additional size in pixels for the active command
        indicatorSize: 24, // the size in pixels of the pointer to the active command
        separatorWidth: 3, // the empty spacing in pixels between successive commands
        spotlightPadding: 4, // extra spacing in pixels between the element and the spotlight
        minSpotlightRadius: 24, // the minimum radius in pixels of the spotlight
        maxSpotlightRadius: 38, // the maximum radius in pixels of the spotlight
        openMenuEvents: 'cxttapstart taphold', // space-separated cytoscape events that will open the menu; only `cxttapstart` and/or `taphold` work here
        itemColor: 'white', // the colour of text in the command's content
        itemTextShadowColor: 'transparent', // the text shadow colour of the command's content
        zIndex: 9999, // the z-index of the ui div
        atMouse: false, // draw menu at mouse position
    };

    // the default values of each option are outlined below:
    var cxtmenu_finding = {
        menuRadius: 100, // the radius of the circular menu in pixels
        selector: '.finding', // elements matching this Cytoscape.js selector will trigger cxtmenus
        commands: [
            // an array of commands to list in the menu or a function that returns the array

            {
                // load detailed finding
                fillColor: 'rgba(200, 200, 200, 0.75)', // optional: custom background color for item
                content: 'Finding Details', // html/text content to be displayed in the menu
                contentStyle: {}, // css key:value pairs to set the command's css in js if you want
                select: function (ele) {
                    // a function to execute when the command is selected
                    //console.log(ele.id()) // `ele` holds the reference to the active element
                    var finding = Findings.get({ findingId: ele.data().id }).$promise.then(displayFindingsDetailsModal);
                    //ele.data().id ? $state.go("cato.loggedIn.dashboard.todo.editFinding."+ele.data().id, {}) : null;
                },
                enabled: true, // whether the command is selectable
            },
            {
                // hide this node
                fillColor: 'rgba(200, 200, 200, 0.75)', // optional: custom background color for item
                content: 'Hide', // html/text content to be displayed in the menu
                contentStyle: {}, // css key:value pairs to set the command's css in js if you want
                select: function (ele) {
                    // a function to execute when the command is selected
                    if (ele.connectedEdges().targets().length > 0) {
                        if (ele.style('display') == 'none') {
                            //show the nodes and edges
                            ele.connectedEdges().targets().style('display', 'element');
                        } else {
                            //hide the children nodes and edges recursively
                            ele.style('display', 'none');
                        }
                    }
                    ele.style('display', 'none');
                },
                enabled: true, // whether the command is selectable
            },
        ], // function( ele ){ return [ /*...*/ ] }, // example function for commands
        fillColor: 'rgba(0, 0, 0, 0.75)', // the background colour of the menu
        activeFillColor: 'rgba(1, 105, 217, 0.75)', // the colour used to indicate the selected command
        activePadding: 20, // additional size in pixels for the active command
        indicatorSize: 24, // the size in pixels of the pointer to the active command
        separatorWidth: 3, // the empty spacing in pixels between successive commands
        spotlightPadding: 4, // extra spacing in pixels between the element and the spotlight
        minSpotlightRadius: 24, // the minimum radius in pixels of the spotlight
        maxSpotlightRadius: 38, // the maximum radius in pixels of the spotlight
        openMenuEvents: 'cxttapstart taphold', // space-separated cytoscape events that will open the menu; only `cxttapstart` and/or `taphold` work here
        itemColor: 'white', // the colour of text in the command's content
        itemTextShadowColor: 'transparent', // the text shadow colour of the command's content
        zIndex: 9999, // the z-index of the ui div
        atMouse: true, // draw menu at mouse position
    };

    $scope.spread_layout_defaults = {
        animate: false, // Whether to show the layout as it's running
        ready: undefined, // Callback on layoutready
        stop: undefined, // Callback on layoutstop
        fit: true, // Reset viewport to fit default simulationBounds
        // avoidOverlap: false, // prevents node overlap, may overflow boundingBox if not enough space
        minDist: 60, // Minimum distance between nodes
        padding: 30, // Padding
        expandingFactor: -1.0, // If the network does not satisfy the minDist
        // criterium then it expands the network of this amount
        // If it is set to -1.0 the amount of expansion is automatically
        // calculated based on the minDist, the aspect ratio and the
        // number of nodes
        // prelayout: {name: 'cose'},
        // name: 'spread', // Layout options for the first phase
        name: 'grid', // Layout options for the first phase
        maxExpandIterations: 10, // Maximum number of expanding iterations
        // Constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
        boundingBox: undefined,
        atMouse: true, // draw menu at mouse position
        // randomize: false, // Uses random initial node positions on true
        // minNodeSpacing: 1,
        // padding: -1
    };

    function createCyObj(): CytoscapeOptions {
        let cyObj = {
            container: document.getElementById('cytoscape_container'),

            boxSelectionEnabled: false,
            autounselectify: true,

            hideEdgesOnViewport: true,
            pixelRatio: 1,
            textureOnViewport: true,

            layout: $scope.spread_layout_defaults,
            minZoom: 0.25,
            maxZoom: 15,
            style: [
                {
                    selector: '.targetElement',
                    style: {
                        'overlay-color': 'black',
                        'overlay-opacity': '0.2',
                        'overlay-padding': '5',
                        'z-index': '99999',
                    },
                },
                {
                    selector: 'node',
                    style: {
                        content: 'data(label)',
                        'text-opacity': 0.8,
                        'text-valign': 'center',
                        'text-halign': 'center',
                        'text-wrap': 'wrap',
                        'background-color': '#464646',
                        width: 'label',
                        height: 'label',
                        shape: 'roundrectangle',
                        color: 'black',
                        'font-family': '"Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif, FontAwesome',
                        'font-weight': 'bold',
                        'padding-left': '10px',
                        'padding-right': '10px',
                        'padding-top': '10px',
                        'padding-bottom': '10px',
                        'min-zoomed-font-size': '14',
                    },
                },

                //This does not go well with the grey overlay
                // {
                //     selector: 'node.targetElement',
                //     style: {
                //         'color': 'white'
                //     }
                // },
                {
                    selector: 'edge',
                    style: {
                        //'content': 'data(label)',
                        width: 4,
                        'line-color': '#b7b7b7',
                        'target-arrow-color': '#b7b7b7',
                        'curve-style': 'haystack',
                    },
                },
                {
                    selector: '.selectedEdge',
                    style: {
                        //'content': 'data(label)',
                        width: 4,
                        'target-arrow-shape': 'triangle',
                        'line-color': '#464646',
                        'target-arrow-color': '#464646',
                    },
                },
                {
                    selector: '.edgeHover',
                    style: {
                        content: 'data(label)',
                        'text-valign': 'center',
                        'text-halign': 'right',
                    },
                },
                {
                    selector: '.tcp',
                    style: {
                        shape: 'hexagon',
                        'background-color': '#D9D6D6',
                    },
                },
                {
                    selector: '.udp',
                    style: {
                        shape: 'hexagon',
                        'background-color': '#8C8B8A',
                    },
                },
                {
                    selector: '.ipaddress',
                    style: {
                        shape: 'roundrectangle',
                        'background-color': '#a9a9a9',
                    },
                },
                {
                    selector: '.ipaddress.targetElement',
                    style: {
                        shape: 'roundrectangle',
                        'background-color': '#767676',
                    },
                },
                {
                    selector: '.cidrblock',
                    style: {
                        shape: 'ellipse',
                        'background-color': '#588777',
                    },
                },
                {
                    selector: '.cidrblock.targetElement',
                    style: {
                        shape: 'ellipse',
                        'background-color': '#00875a',
                    },
                },
                {
                    selector: '.domain',
                    style: {
                        shape: 'barrel',
                        'background-color': '#f5aa6a',
                    },
                },
                {
                    selector: '.domain.targetElement',
                    style: {
                        shape: 'barrel',
                        'background-color': '#f58b31',
                    },
                },
                {
                    selector: '.fqdn',
                    style: {
                        shape: 'roundrectangle',
                        'background-color': '#fe5757',
                    },
                },
                {
                    selector: '.fqdn.targetElement',
                    style: {
                        shape: 'roundrectangle',
                        'background-color': '#fe3131',
                    },
                },
                {
                    selector: '.hostname',
                    style: {
                        shape: 'roundrectangle',
                        'background-color': '#57e3fe',
                    },
                },
                {
                    selector: '.hostname.targetElement',
                    style: {
                        shape: 'roundrectangle',
                        'background-color': '#0BD6FE',
                    },
                },
                {
                    selector: '.finding',
                    style: {
                        shape: 'rectangle',
                        padding: '20px',
                        color: 'black',
                        'border-width': 1,
                        'border-color': 'black',
                    },
                },
                {
                    selector: '.finding.label-none',
                    style: {
                        'background-color': '#f5f4f4',
                    },
                },
                {
                    selector: '.finding.label-low',
                    style: {
                        'background-color': '#b2c8a2',
                    },
                },
                {
                    selector: '.finding.label-medium',
                    style: {
                        'background-color': '#e5e38d',
                    },
                },
                {
                    selector: '.finding.label-high',
                    style: {
                        'background-color': '#e5c161',
                    },
                },
                {
                    selector: '.finding.label-critical',
                    style: {
                        'background-color': '#e46f74',
                    },
                },
            ],

            elements: {
                nodes: $scope.nodes,
                edges: $scope.edges,
            },
        };

        return cyObj as CytoscapeOptions;
    }

    $scope.finishSettingUpGraph = function (cy, readyCallBack) {
        cy.on('tap', '.target', function (ele) {
            $scope.load_targets_for_visualization_with_findings(ele.target.data()['extraData']);
        });

        cy.cxtmenu(cxtmenu_finding);
        cy.cxtmenu(cxtmenu_defaults);

        if (readyCallBack) {
            readyCallBack();
        }
        return cy;
    };

    // $scope.rerunLayout = function () {
    //     $scope.graphBusy = $timeout(function () {
    //         return $scope.cy.layout($scope.graphLayoutObj);
    //     }, 50);
    // }

    $scope.loadGraphProxy = function () {
        console.log('loading target visualization by proxy');
        if ($scope.data.activeCategory != 'visualize') {
            $scope.load_targets_for_visualization();
        }
        $scope.should_show_graph_tools = true;
        // $scope.loadGraph();
    };

    $scope.loadGraph = function (readyCallBack) {
        console.log('loading target visualization');

        //Clear the canvas
        if (!$scope.selectedCustomer) {
            if ($scope.cy) {
                $scope.nodes = [];
                $scope.edges = [];

                $scope.cy.destroy();
            }
            return;
        }

        Targets.get(
            {
                customer: $scope.selectedCustomer.id,
                ais: true,
                page_size: 1,
                active: getActive(),
                third_party: getThirdParty(),
            },
            function (res) {
                if ($scope.getCyNodeInterval) {
                    return;
                }

                var cyObj = createCyObj();
                var cyNode = angular.element('#cytoscape_container')[0];
                var passes = 0;
                // var startedIntervalsAt = new Date().getTime();
                $scope.graphBusy = $q.defer();
                $scope.getCyNodeInterval = setInterval(function () {
                    if (cyNode && cyNode.clientWidth > 0) {
                        clearInterval($scope.getCyNodeInterval);
                        $scope.getCyNodeInterval = null;
                        console.log('Working with ' + res.count + ' targets');
                        $scope.spread_layout_defaults.name = res.count > 500 ? 'grid' : 'spread';
                        $scope.cy = cytoscape(cyObj).ready(function (event) {
                            var cy = $scope.finishSettingUpGraph(event.cy, readyCallBack);
                            if (readyCallBack) {
                                cy.zoom(1.25);
                                cy.center($scope.targetNode);
                            }
                            $scope.graphBusy.resolve(true);
                            $scope.graphBusy = null;
                            // var finishedIntervalsAt = new Date().getTime();
                            // console.log("Successefully created graph after " + (finishedIntervalsAt - startedIntervalsAt) + "ms!");
                        });
                    } else if (passes > 100) {
                        clearInterval($scope.getCyNodeInterval);
                        $scope.getCyNodeInterval = null;
                        modalService.openErrorResponse('TimeOut Error', 'Unable to load the graph in the allocated time.', false);
                    } else {
                        passes++;
                    }
                }, 10);
            }
        );
    };

    $scope.selectAllTargets = function () {
        $rootScope.selectAll($scope.data.targetsSelected, _.filter($scope.targetTable.data, { locked: false }));
    };

    var operationUnsubscribe = socketService.subscribeByModel('operation', function (operation) {
        operationCache[operation.id] = operation; // TODO: limit stored operations to prevent memory leak
    });

    var targetUnsubscribe = socketService.subscribeByModel('target', function (newTarget) {
        // TODO: address duplicated object changes when editing targets in the add/edit target modal
        var curTarget: any = _.find($scope.targetTable.data, { id: newTarget.id });
        if (curTarget) {
            $scope.updateObj(curTarget, newTarget);
        } else {
            return; // We can't easily tell if the new target should be displayed, so just ignore it
        }

        // Handle making sure the owner is present and up to date
        if (curTarget.locked && (_.isNumber(curTarget.owner) || _.get(curTarget.owner, 'id') !== newTarget.owner)) {
            if (!setOwnerDetails(newTarget.owner, curTarget)) {
                // Wait a moment and try again in case the operation websocket update will come in after the target
                $timeout(function () {
                    if (!setOwnerDetails(newTarget.owner, curTarget)) {
                        // Still didn't get websocket update, just make an HTTP request
                        operationCache[newTarget.owner] = Operations.get({ operationId: newTarget.owner }, function () {
                            setOwnerDetails(newTarget.owner, curTarget);
                        });
                    }
                }, 100);
            }
        }
    });

    $scope.$on('$destroy', function () {
        operationUnsubscribe();
        targetUnsubscribe();
        appWindow.unbind('resize', reloadGraphOnResize);
    });

    function setOwnerDetails(opId, target) {
        if (operationCache[opId]) {
            target.owner = operationCache[opId];
            return true;
        }
        return false;
    }

    $scope.$watch('data.selectedTargets.length', function (newValue, oldValue) {
        var selectableTargets = _.reject($scope.targetTable.data, { locked: true });
        $scope.data.allSelected = newValue !== 0 && newValue === selectableTargets.length;
    });

    $scope.checkCanBeSelected = function (target) {
        if (target.locked && target.selected) {
            target.selected = false;
        }
    };

    if ($scope.gdata.isOperator) {
        $scope.$watch('user.ui_data.selectedCustomer', function (newVal, oldVal) {
            if (oldVal !== newVal) {
                $scope.selectedCustomer = newVal;
                $scope.loadTargets($scope.selectedCustomer, $state.params.targetType);
            }
        });
        $scope.selectedCustomer = _.get($rootScope.user, 'ui_data.selectedCustomer');
    } else {
        $scope.gdata.customer.$promise.then(function () {
            $scope.loadTargets($scope.gdata.customer, $state.params.targetType);
        });
    }

    var defaultParams: any = {
        owner_op: true,
    };

    $scope.loadTargets = function (customer, targetType) {
        $scope.selectedCustomer = customer;
        if (!customer) {
            $scope.selectedCustomer = null;
            $scope.targetTable = new NgTableParams({}, {});
            $scope.metatargetTable = new NgTableParams({}, {});

            $scope.loadGraph();

            return;
        }

        $scope.tagFilterData.customer = customer.id;

        //TODO: Come back and use the ngTables defined Interface
        var extraInitialParams: any = {};
        if (targetType) {
            extraInitialParams.filter = { type: targetType };
        }

        defaultParams.customer = customer.id;

        $scope.targetTable = ngTableService.serverFilteredTable({
            resource: Targets.list,
            defaultParams: defaultParams,
            extraInitialParams: extraInitialParams,
            updateParamsCallback: updateParamsCallback,
        });
        $scope.metatargetTable = ngTableService.serverFilteredTable({
            resource: MetaTargets.paginate,
            defaultParams: defaultParams,
            extraInitialParams: extraInitialParams,
            updateParamsCallback: updateParamsCallback,
        });

        if ($scope.data.activeCategory === 'Visualize') {
            $scope.load_targets_for_visualization();
        }
    };

    $scope.load_targets_for_visualization = function (event) {
        if (event) {
            event.currentTarget.blur();
        }
        $scope.nodes = [];
        $scope.edges = [];
        if ($scope.selectedCustomer != undefined) {
            $scope.visualizeBusy = $q.defer();
            console.log('loading targets for visualization');
            Targets.get_ais_targets({
                customerId: $scope.selectedCustomer.id,
                active: getActive(),
                third_party: getThirdParty(),
            }).$promise.then(function (ais_targets) {
                //console.log("retrieved targets");
                var nodes = {};
                _.forEach(ais_targets, function (el, index, arr) {
                    nodes[el.id] = el;
                    el.original_id = el.id;
                    var label = TargetService.getTargetTypeIconUnicodeValue(el.type) + el.value;
                    var currentNode = {
                        data: { id: el.value, label: label, extraData: el },
                        classes: el.type + ' target',
                    };
                    $scope.nodes.push(currentNode);
                });

                var pushEdges = function () {
                    _.forEach($scope.link_targets, function (el, index, arr) {
                        if ($scope.showInactive === 'active' && el.active === false) {
                            return;
                        }
                        $scope.edges.push({ data: { target: nodes[el.child].value, source: nodes[el.owner].value } });
                    });
                    $scope.loadGraph();
                    $scope.visualizeBusy.resolve(true);
                    $scope.visualizeBusy = null;
                };

                var cancelRender = function () {
                    $scope.visualizeBusy.resolve(true);
                    $scope.visualizeBusy = null;
                    $scope.showInactive = 'inactive';
                };

                var queryEdges = function () {
                    TargetLinks.basicList({
                        customer: $scope.selectedCustomer.id,
                        active: getActive(),
                        third_party: getThirdParty(),
                    }).$promise.then(function (link_targets) {
                        $scope.link_targets = link_targets;
                        if ($scope.link_targets.length > 5000) {
                            modalService.openConfirm(
                                'Show ' + $scope.link_targets.length + ' links?',
                                '',
                                ' show ' + $scope.link_targets.length + ' links? It could take a very long time to display. Do you wish to proceed',
                                null,
                                pushEdges,
                                cancelRender
                            );
                        } else {
                            pushEdges();
                        }
                    });
                };

                if ($scope.nodes.length > 5000) {
                    modalService.openConfirm(
                        'Show ' + ais_targets.length + ' targets?',
                        '',
                        ' show ' + ais_targets.length + ' targets? It could take a very long time to display. Do you wish to proceed',
                        null,
                        queryEdges,
                        cancelRender
                    );
                } else {
                    queryEdges();
                }
            });
        } else {
            $scope.loadGraph();
        }
    };

    $scope.load_targets_for_visualization_with_findings = function (target) {
        if (!target && $scope.target) {
            target = $scope.target;
        } else if (!target && $scope.target) {
            target = $scope.target.targetElement;
        }

        $scope.target = target;

        $scope.nodes = [];
        $scope.edges = [];
        if ($scope.selectedCustomer != undefined) {
            $scope.visualizeBusy = $q.defer();
            console.log('loading targets for visualization with findings');
            Targets.get_ais_targets({
                customerId: $scope.selectedCustomer.id,
                active: getActive(),
                third_party: getThirdParty(),
            }).$promise.then(function (ais_targets) {
                //console.log("retrieved targets");

                var cancelRender = function () {
                    $scope.visualizeBusy.resolve(true);
                    $scope.visualizeBusy = null;
                    $scope.showInactive = 'inactive';
                };

                var queryNodes = function () {
                    _.forEach(ais_targets, function (el, index, arr) {
                        if ($scope.showInactive !== 'inactive' && el.active === false) {
                            return;
                        }

                        el.original_id = el.id;
                        var label = TargetService.getTargetTypeIconUnicodeValue(el.type) + el.value;
                        if (el.value !== target.value) {
                            $scope.nodes.push({
                                data: { id: el.value, label: label, extraData: el },
                                classes: el.type + ' target',
                            });
                        } else {
                            $scope.target = el;
                            $scope.nodes.push({
                                data: { id: el.value, label: label, extraData: el },
                                classes: el.type + ' target targetElement',
                            });
                        }
                    });

                    TargetLinks.basicList({
                        customer: $scope.selectedCustomer.id,
                        active: getActive(),
                        third_party: getThirdParty(),
                    }).$promise.then(function (link_targets) {
                        //console.log("retrieved links");
                        $scope.link_targets = link_targets;
                        if ($scope.link_targets.length > 5000) {
                            modalService.openConfirm(
                                'Show ' + $scope.link_targets.length + ' links?',
                                '',
                                ' show ' + $scope.link_targets.length + ' links? It could take a very long time to display. Do you wish to proceed',
                                null,
                                queryEdges,
                                cancelRender
                            );
                        } else {
                            queryEdges();
                        }
                    });
                };

                var queryEdges = function () {
                    _.forEach($scope.link_targets, function (el, index, arr) {
                        if ($scope.showInactive === 'active' && el.active === false) {
                            return;
                        }
                        $scope.edges.push({ data: { target: el.child.value, source: el.owner.value } });
                    });

                    if ($scope.showFindingsInBar) {
                        $scope.loadGraph($scope.bfs);
                    } else {
                        Findings.basicList({ target: target.id, vis_data_only: true }).$promise.then(function (findings) {
                            _.forEach(findings, function (el, index, arr) {
                                if (
                                    el.title.indexOf('Machine-readable ') === 0 ||
                                    el.title.indexOf('Added new IP Address Target(s)') === 0 ||
                                    el.title.indexOf('Found host at IP:') === 0
                                ) {
                                    return;
                                }

                                var title = el.title.replace(' on ', '\non ');

                                var severity = FindingService.SEVERITY_LEVEL_MAP[el.severity];
                                $scope.nodes.push({
                                    data: { id: el.id, label: title, extraData: el },
                                    classes: 'finding ' + severity.class,
                                });
                                $scope.edges.push({
                                    data: { target: target.value, source: el.id },
                                    classes: 'selectedEdge',
                                });
                            });
                            $scope.loadGraph($scope.bfs);
                        });
                    }
                };

                if (ais_targets.length > 5000) {
                    modalService.openConfirm(
                        'Show ' + ais_targets.length + ' targets?',
                        '',
                        ' show ' + ais_targets.length + ' targets? It could take a very long time to display. Do you wish to proceed',
                        null,
                        queryNodes,
                        cancelRender
                    );
                } else {
                    queryNodes();
                }
            });
        }
    };

    Targets.options(function (result) {
        $scope.config = result.actions.POST;
        angular.forEach(_.sortBy($scope.config.type.choices, 'display_name'), function (typeMap) {
            $scope.types.push({ id: typeMap.value, title: typeMap.display_name });
        });
    });

    $scope.loadTargets($scope.selectedCustomer);

    function updateParamsCallback(params) {
        // Changing parameters will change which targets are display, so uncheck the 'select all' box.
        $scope.data.targetsSelected = false;

        params.third_party = getThirdParty();

        if ($scope.showInactive === 'inactive') {
            params.active = false;
        } else if ($scope.showInactive === 'active') {
            params.active = true;
        } else if ($scope.showInactive === 'all') {
            delete params.active;
        }

        if ($scope.showThirdParty === 'all') {
            delete params.third_party;
        } else if ($scope.showThirdParty === 'third-party') {
            params.third_party = true;
        }
    }

    // This is the handler for target counts on the targets page
    $scope.data.activeCategory = 'Target';

    $scope.getCount = function (category) {
        return $scope[category.toLowerCase() + 'Table'].total();
    };

    $scope.reloadVisuals = function () {
        if ($scope.data.activeCategory === 'Visualize') {
            $scope.reloadGraph(false);
        } else {
            $scope.targetTable.reload();
            $scope.metatargetTable.reload();
        }
    };

    $scope.selectTargetsTab = function (type) {
        $scope.should_show_graph_tools = false;
        $scope.data.activeCategory = type;
        $scope.reloadVisuals();
    };

    $scope.showFindingsInBar = false;
    $scope.reloadGraph = function (toggleFindingsBar) {
        if ($scope.visualizeBusy) {
            return;
        }

        $scope.showFindingsInBar = toggleFindingsBar ? !$scope.showFindingsInBar : $scope.showFindingsInBar;
        if ($scope.target) {
            $scope.load_targets_for_visualization_with_findings($scope.target);
        } else {
            $scope.load_targets_for_visualization();
        }
    };

    function removeFromMetatarget(targetId) {
        _.eachRight($scope.metatargetTable.data, function (metatarget) {
            if (metatarget.targets.length > 0) {
                if (_.isNumber(metatarget.targets[0])) {
                    _.pull(metatarget.targets, targetId);
                } else {
                    _.remove(metatarget.targets, { id: targetId });
                }
                if (metatarget.targets.length === 0) {
                    // We skipped over metatargets with no targets above so having no targets now means the only contained target was the one that was just deleted
                    _.remove($scope.metatargetTable.data, { id: metatarget.id });
                }
            }
        });
    }

    function getActive() {
        if ($scope.showInactive === 'active') {
            return true;
        } else if ($scope.showInactive === 'all') {
            return undefined;
        } else if ($scope.showInactive === 'inactive') {
            return false;
        }
    }

    function getThirdParty() {
        if ($scope.showThirdParty === 'all') {
            return undefined;
        } else if ($scope.showThirdParty === 'third-party') {
            return true;
        }
    }

    $scope.targetActions = {
        forceUnlockTarget: {
            action: function (target) {
                modalService.openConfirm(
                    'Force Unlock',
                    'forcefully unlock',
                    target.value +
                        '? The operation locking it' +
                        ' will be forcefully paused, and may put the system into an inconsistent state. Do you want to proceed',
                    null,
                    function () {
                        Targets.unlock(
                            { targetId: target.id },
                            {},
                            function (res) {
                                $scope.updateObjOld(target, res);
                            },
                            modalService.openErrorResponseCB('Unlocking Target failed')
                        );
                    }
                );
            },
            class: 'btn-white',
            icon: 'fa-unlock-alt',
            label: 'Force Unlock',
            show: function (target) {
                return $scope.gdata.isMissionDirector && target.locked;
            },
            tooltip: 'Force Unlock Target',
        },
        showOnMap: {
            action: function (target) {
                var managedCustomer = _.get($rootScope.user, 'ui_data.selectedCustomer');
                $state.go('cato.loggedIn.dashboard.maps', {
                    target: target,
                    customer: $scope.selectedCustomer,
                    managedCustomerEmpty: managedCustomer === null,
                });
            },
            class: 'btn-white',
            icon: 'fa-map',
            label: 'Show on Map',
            show: function (target) {
                return (target.position ? true : false) && config.showMaps.toLowerCase() === 'true';
            },
            tooltip: 'Show this target on a map',
        },
        editTarget: {
            action: function (target) {
                $state.go('.addEditTarget', {
                    targetId: target.id,
                    target: target,
                    selectedCustomer: $scope.selectedCustomer,
                });
            },
            class: 'btn-white',
            icon: 'fa-pencil',
            label: 'Edit',
            show: function (target) {
                return !target.locked;
            },
            tooltip: 'Edit Target',
        },
        deactivateTarget: {
            action: function (target) {
                Targets.patch({ targetId: target.id }, { active: false }, function (res) {
                    $scope.updateObjOld(target, res);
                });
            },
            class: 'btn-white',
            icon: 'fa-times',
            label: 'Deactivate',
            show: function (target) {
                return target.active && !target.locked;
            },
            tooltip: 'Deactivate Target',
        },
        activateTarget: {
            action: function (target) {
                Targets.patch(
                    { targetId: target.id },
                    { active: true },
                    function (res) {
                        $scope.updateObjOld(target, res);
                    },
                    modalService.openErrorResponseCB('Activating Target failed')
                );
            },
            class: 'btn-white',
            icon: 'fa-power-off',
            label: 'Activate',
            show: function (target) {
                return !(target.active || target.locked);
            },
            tooltip: 'Activate Target',
        },
        deleteSpecificTarget: {
            action: function (target) {
                modalService.openDelete('Delete Target', "target '" + target.value + "'", null, function () {
                    Targets.deleteSpecificTarget(
                        { targetId: target.id },
                        function () {
                            _.remove($scope.targetTable.data, { id: target.id });
                            $scope.targetTable.total($scope.targetTable.total() - 1);
                            removeFromMetatarget(target.id);
                        },
                        modalService.openErrorResponseCB('Deleting Target failed')
                    );
                });
            },
            class: 'btn-white',
            icon: 'fa-trash',
            label: 'Delete',
            show: function (target) {
                return ($scope.gdata.isMissionDirector || $scope.gdata.isOperator) && !target.locked;
            },
            tooltip: 'Delete Target',
        },
    };

    $scope.bulkTargetActions = {
        showOnMap: {
            action: function (target) {
                var managedCustomer = _.get($rootScope.user, 'ui_data.selectedCustomer');
                var selected = _.map($scope.data.selectedTargets, 'id');
                $state.go('cato.loggedIn.dashboard.maps', {
                    targets: selected,
                    customer: $scope.selectedCustomer,
                    managedCustomerEmpty: managedCustomer === null,
                });
            },
            class: 'btn-white',
            icon: 'fa-map',
            label: 'Show on Map',
            show: function (target) {
                return config.showMaps.toLowerCase() === 'true';
            },
            tooltip: 'Show this target on a map',
        },
        deactivateTargets: {
            action: function () {
                // If the user has for some reason selected targets that are already deactivated, this will
                // have no effect on them.
                var selected = _.map($scope.data.selectedTargets, 'id');
                var targets = $scope.data.selectedTargets.concat();

                $scope.bulkSubmission = Targets.bulkUpdate(
                    {},
                    { active: false, targets: selected },
                    function () {
                        _.each(targets, function (target) {
                            target.active = false;
                        });
                    },
                    modalService.openErrorResponseCB('Unable to deactivate selected targets')
                );
            },
            class: 'btn-white',
            icon: 'fa-times fa-fw',
            label: 'Deactivate',
            tooltip: 'Deactivate Selected Targets',
        },

        activateTargets: {
            action: function () {
                // If the user has for some reason selected targets that are already activated, this will
                // have no effect on them.
                var selected = _.map($scope.data.selectedTargets, 'id');
                var targets = $scope.data.selectedTargets.concat();

                $scope.bulkSubmission = Targets.bulkUpdate(
                    {},
                    { active: true, targets: selected },
                    function () {
                        _.each(targets, function (target) {
                            target.active = true;
                        });
                    },
                    modalService.openErrorResponseCB('Unable to activate selected targets')
                );
            },
            class: 'btn-white',
            icon: 'fa-power-off fa-fw',
            label: 'Activate',
            tooltip: 'Activate selected Targets',
        },

        deleteTargets: {
            action: function () {
                var customer = $scope.selectedCustomer;
                var selected = _.map($scope.data.selectedTargets, 'id');

                modalService.openDelete('Confirmation', selected.length + " targets for '" + customer.name + "'", {}, function () {
                    $scope.bulkSubmission = Targets.bulkDestroy(
                        {},
                        { targets: selected },
                        function () {
                            _.each(selected, function (target_id) {
                                _.remove($scope.targetTable.data, { id: target_id });
                                removeFromMetatarget(target_id);
                            });
                        },
                        modalService.openErrorResponseCB('Unable to delete selected targets')
                    );
                });
            },
            class: 'btn-white',
            icon: 'fa-trash fa-fw',
            label: 'Delete',
            show: $scope.gdata.isOperator,
            tooltip: 'Delete Selected Targets',
        },

        bulkEditTargets: {
            action: function () {
                $state.go('.bulkEdit');
                $scope.modalInstance = $uibModal.open({
                    templateUrl: '/app/target/modalAddTarget.html',
                    controller: 'AddTargetCtrl',
                    scope: $scope,
                    resolve: {
                        params: {
                            selectedTargets: $scope.data.selectedTargets,
                            bulkEdit: true,
                        },
                    },
                });
            },
            class: 'btn-white',
            icon: 'fa-pencil fa-fw',
            label: 'Edit',
            tooltip: 'Edit Targets',
        },

        bulkTagTargets: {
            action: function () {
                var data = {
                    targets: $scope.data.selectedTargets,
                };
                var modal = stateBuilder.componentModalGenerator('modal-tag-targets', { targets: '<' }, data, 'tag/modalTagTargets');
                $uibModal.open(modal);
            },
            class: 'btn-white',
            icon: 'fa-tags fa-fw',
            label: 'Tag',
            tooltip: 'Tag Targets',
        },
    };

    $scope.metaTargetActions = {
        editMetaTarget: {
            action: function (metatarget) {
                $state.go('.addEditMetaTarget', {
                    metaTargetId: metatarget.id,
                    metatarget: metatarget,
                    selectedCustomer: $scope.selectedCustomer,
                });
            },
            class: 'btn-white',
            icon: 'fa-pencil fa-fw',
            label: 'Edit',
            show: function (metatarget) {
                return !metatarget.locked;
            },
            tooltip: 'Edit Meta Target',
        },
        forceUnlockMetaTarget: {
            action: function (metatarget) {
                modalService.openConfirm(
                    'Force Unlock',
                    'forcefully unlock',
                    metatarget.name +
                        '? The operation locking it' +
                        ' will be forcefully paused, and may put the system into an inconsistent state. Do you want to proceed',
                    null,
                    function () {
                        MetaTargets.unlock(
                            { metaTargetId: metatarget.id },
                            {},
                            function (res) {
                                $scope.updateObjOld(metatarget, res);
                            },
                            modalService.openErrorResponseCB('Unlocking MetaTarget failed')
                        );
                    }
                );
            },
            class: 'btn-white',
            icon: 'fa-unlock-alt fa-fw',
            label: 'Force Unlock',
            show: function (metatarget) {
                return metatarget.locked;
            },
            tooltip: 'Force Unlock Meta Target',
        },

        deleteSpecificMetaTarget: {
            action: function (metatarget) {
                modalService.openDelete('Delete MetaTarget', metatarget.name, null, function () {
                    MetaTargets.deleteSpecificMetaTarget(
                        { metaTargetId: metatarget.id },
                        function () {
                            _.remove($scope.metatargetTable.data, { id: metatarget.id });
                            $scope.metatargetTable.total($scope.metatargetTable.total() - 1);
                        },
                        modalService.openErrorResponseCB('Deleting MetaTarget failed')
                    );
                });
            },
            class: 'btn-white',
            icon: 'fa-trash fa-fw',
            label: 'Delete',
            show: function (metatarget) {
                return !($scope.gdata.isMissionDirector && metatarget.locked);
            },
            tooltip: 'Delete Selected Target',
        },
    };
    $scope.targetSaved = function (target) {
        if ($scope.selectedCustomer && target.customer === $scope.selectedCustomer.id) {
            var existingTarget = _.find($scope.targetTable.data, { id: target.id });
            if (existingTarget) {
                $scope.updateObj(existingTarget, target);
            } else {
                $scope.targetTable.data.push(target);
                $scope.targetTable.total($scope.targetTable.total() + 1);
            }
        }
    };

    $scope.metatargetSaved = function (metatarget) {
        if ($scope.selectedCustomer && metatarget.customer === $scope.selectedCustomer.id) {
            var existingMetaTarget = _.find($scope.metatargetTable.data, { id: metatarget.id });
            if (existingMetaTarget) {
                $scope.updateObj(existingMetaTarget, metatarget);
            } else {
                $scope.metatargetTable.data.push(metatarget);
                $scope.metatargetTable.total($scope.metatargetTable.total() + 1);
            }
        }
    };

    $scope.getCreatorName = function (selectedId) {
        return MessageService.getNameOrRoleFromId(selectedId);
    };
});
