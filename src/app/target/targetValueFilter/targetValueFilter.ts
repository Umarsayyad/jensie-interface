import app from '../../app';
import * as _ from 'lodash';

app.component('targetValueFilter', {
    bindings: {
        filterValue: '<',
        filterType: '<',
        id: '@?',
        valueChanged: '&',
        typeChanged: '&',
    },
    controller: function () {
        var ctrl = this;
        ctrl.types = {
            iexact: 'Exact',
            icontains: 'Contains',
            istartswith: 'Starts With',
            iendswith: 'Ends With',
            iregex: 'Regex',
        };

        ctrl.$onInit = function () {
            // Bindings are now available
            ctrl.id = ctrl.id || 'targetValue';
            if (!ctrl.filterType) {
                ctrl.selectType('icontains');
            }
        };

        // Action handler functions
        ctrl.selectType = function (type) {
            ctrl.filterType = type;
            ctrl.typeChanged({ type: type });
        };
    },
    templateUrl: '/app/target/targetValueFilter/targetValueFilter.html',
});
