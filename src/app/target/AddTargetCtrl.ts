import app from '../app';
import * as _ from 'lodash';

import '../customer/manage/CondensedCustomerFilterComp';
import '../tag/tagsEditor';

app.controller('addTargetLoaderCtrl', function ($scope, $state, $uibModal) {
    $uibModal.open({
        templateUrl: '/app/target/modalAddTarget.html',
        controller: 'AddTargetCtrl',
        size: 'lg',
        scope: $scope,
        resolve: {
            params: {},
            customers: $scope.gdata.customers.$promise, // This guarantees $scope.gdata.customerMap and $scope.gdata.customer are populated before the modal controller runs
        },
    });
});

app.controller('AddTargetCtrl', function (
    $rootScope,
    $sce,
    $scope,
    $state,
    $uibModalInstance,
    modalService,
    AccessGroups,
    Targets,
    TargetZones,
    MetaTargets,
    params
) {
    $scope.zoneChangesDisabled = true;
    var previousZone;
    $scope.targetMap = {};
    $scope.target = {
        active: true,
        isMetaTarget: false,
        confirmTypes: false,
        showBulkCreate: false,
        bulkTargets: '',
        updatedTags: [],
    };
    $scope.targetsLoaded = false;
    if ($scope.hasRole('customer')) {
        $scope.target.customer = $scope.gdata.customer;
    } else {
        $scope.target.customer = $state.params.selectedCustomer || _.get($scope.user, 'ui_data.selectedCustomer') || '';
    }
    loadAvailableResponsibleUsers();
    loadAvailableAccessGroups();
    $scope.edit = false;
    $scope.bulkEdit = false;
    $scope.infos = [];
    $scope.errors = [];
    $scope.successes = [];

    if (_.isNil($scope.data)) {
        $scope.data = {
            cpeOptions: $scope.gdata.getCPE(),
        };
    } else if (_.isNil($scope.data.cpeOptions)) {
        $scope.data.cpeOptions = $scope.gdata.getCPE();
    }

    function setUpTarget(target) {
        $scope.updateObj($scope.target, target);
        if (target.customer !== $scope.target.customer.id) {
            $scope.target.customer = $scope.gdata.customerMap[target.customer];
        }
        $scope.target.updatedTags = _.clone($scope.target.tags);
        loadAvailableResponsibleUsers();
        loadAvailableAccessGroups();
        $scope.setZoneChangeDisabledTypeAndZone();
    }

    $scope.loading = Targets.options(function (result) {
        $scope.config = result.actions.POST;
        if ($state.params.targetId) {
            $scope.edit = true;
            $scope.target.isMetaTarget = false;
            $scope.loading = Targets.get({ targetId: $state.params.targetId }, setUpTarget);
        } else if (params.target) {
            $scope.edit = true;
            setUpTarget(params.target);
        }

        // Default to creating a metatarget when the add modal is opened from the metatargets tab
        $scope.target.isMetaTarget = !!($state.params.metaTargetId || $scope.data.activeCategory === 'MetaTarget');

        if ($state.params.metaTargetId) {
            $scope.edit = true;

            if ($state.params.metatarget) {
                $scope.updateObjOld($scope.target, $state.params.metatarget);
                if ($state.params.metatarget.customer !== $scope.target.customer.id) {
                    $scope.target.customer = $scope.gdata.customerMap[$state.params.metatarget.customer];
                }
                loadAvailableResponsibleUsers();
                loadMetaTargetTargetsIds();
                $scope.setZoneChangeDisabledTypeAndZone();
            } else {
                $scope.loading = MetaTargets.get({ metaTargetId: $state.params.metaTargetId }, function (metatarget) {
                    $scope.updateObjOld($scope.target, metatarget);
                    if (metatarget.customer !== $scope.target.customer.id) {
                        $scope.target.customer = $scope.gdata.customerMap[metatarget.customer];
                    }
                    loadAvailableResponsibleUsers();
                    loadMetaTargetTargetsIds();
                    $scope.setZoneChangeDisabledTypeAndZone();
                });
            }
        } else if ($scope.target.isMetaTarget) {
            $scope.getCustomerTargets();
        }
    });

    $scope.getCustomerTargets = function () {
        if ($scope.target.isMetaTarget && _.get($scope.target, 'customer.id')) {
            if (_.isEmpty($scope.targetMap[$scope.target.customer.id])) {
                $scope.targetsLoaded = false;
                Targets.basicList({ customer: $scope.target.customer.id }, function (targets) {
                    $scope.targetMap[$scope.target.customer.id] = {};
                    _.each($scope.config.type.choices, function (targetType) {
                        $scope.targetMap[$scope.target.customer.id][targetType.value] = { all: [] };
                    });
                    _.each(targets, function (target) {
                        if (!$scope.targetMap[$scope.target.customer.id][target.type][target.zone]) {
                            $scope.targetMap[$scope.target.customer.id][target.type][target.zone] = [];
                        }
                        $scope.targetMap[$scope.target.customer.id][target.type][target.zone].push(target);
                        $scope.targetMap[$scope.target.customer.id][target.type]['all'].push(target);
                    });
                    $scope.targetsLoaded = true;
                });
            }
        }
    };

    $scope.setCustomer = function (customer) {
        if (_.get(customer, 'id') !== $scope.target.customer.id) {
            $scope.target.customer = customer || '';
            $scope.getCustomerTargets();
        }

        loadAvailableResponsibleUsers();
        loadAvailableAccessGroups();
    };

    if (params.bulkEdit) {
        $scope.edit = true;
        $scope.bulkEdit = true;
        var customer = $scope.gdata.customerMap[params.selectedTargets[0].customer];
        $scope.target = {
            // Not currently bulk setting type or value, so give them values to let the Submit button enable.
            type: 0,
            value: '',
            customer: customer,
        };
        console.log($scope.target);
        $scope.setCustomer(customer);
    }

    function loadMetaTargetTargetsIds() {
        $scope.getCustomerTargets();
        $scope.target.includedTargets = $scope.target.targets.concat();
    }

    function loadAvailableResponsibleUsers() {
        $scope.available_responsible_users = _.filter($scope.gdata.users, { customer: $scope.target.customer.id });
    }

    function loadAvailableAccessGroups() {
        if ($rootScope.user.pocs.length > 0 || $scope.gdata.isOperator) {
            if ($scope.target.customer) {
                $scope.availableAccessGroups = AccessGroups.list(
                    {
                        abbreviated: true,
                        customer: $scope.target.customer.id,
                    },
                    function (result) {
                        $scope.accessGroupIdMap = _.map($scope.availableAccessGroups, 'id');
                        $scope.accessGroupMap = {};
                        _.each($scope.availableAccessGroups, function (group) {
                            $scope.accessGroupMap['gid-' + group.id] = group;
                        });
                    }
                );
            }
        }
    }

    $scope.getAccessGroupName = function (group_id) {
        var group = $scope.accessGroupMap['gid-' + group_id];
        return group.name;
    };

    function saveTarget() {
        let targetSubmit: any = {
            customer: $scope.target.customer.id,
            type: $scope.target.type,
            responsible_user: $scope.target.responsible_user,
            active: $scope.target.active,
            value: $scope.target.value,
            cpe: $scope.target.cpe,
            tags: $scope.target.updatedTags,
            access_groups: $scope.target.access_groups,
            zone: $scope.target.zone,
            hardware_address: $scope.target.hardware_address,
        };
        if ($scope.target.is_explicitly_aliased) {
            targetSubmit.alias = $scope.target.alias;
        } else if ($scope.target.explicit_alias) {
            targetSubmit.alias = $scope.target.explicit_alias;
        }
        targetSubmit.is_explicitly_aliased = !(targetSubmit.alias === undefined || targetSubmit.alias === '');

        $scope.submission = Targets.add(
            targetSubmit,
            function (target) {
                ($scope.targetSaved || _.noop)(target);

                // Added to refresh the graph when adding targets
                if ($scope.data.activeCategory === 'visualize') {
                    $scope.load_targets_for_visualization();
                }

                $uibModalInstance.close(target);
            },
            function (e) {
                $scope.errors = $scope.parseErrorResponseToList(e);
            }
        );
    }

    function saveMetaTarget() {
        var targetSubmit = {
            customer: $scope.target.customer.id,
            type: $scope.target.type,
            name: $scope.target.name,
            targets: $scope.target.includedTargets || [],
            zone: $scope.target.zone,
        };

        $scope.submission = MetaTargets.add(
            targetSubmit,
            function (metatarget) {
                ($scope.metatargetSaved || _.noop)(metatarget);

                // Added to refresh the graph when adding targets
                if ($scope.data.activeCategory === 'visualize') {
                    $scope.load_targets_for_visualization();
                }

                $uibModalInstance.close(metatarget);
            },
            function (e) {
                $scope.errors = $scope.parseErrorResponseToList(e);
            }
        );
    }

    function updateTarget() {
        var targetSubmit;
        if ($scope.bulkEdit) {
            var target_ids = _.map(params.selectedTargets, 'id');
            targetSubmit = {
                responsible_user: $scope.target.responsible_user,
                targets: target_ids,
                cpe: $scope.target.cpe,
                tags: $scope.target.updatedTags,
                access_groups: $scope.target.access_groups,
                append_tags: $scope.target.appendTags,
                zone: $scope.target.zone,
            };

            $scope.submission = Targets.bulkUpdate(
                {},
                targetSubmit,
                function () {
                    $uibModalInstance.close('a');
                },
                function (e) {
                    $scope.errors = $scope.parseErrorResponseToList(e);
                }
            );
        } else {
            targetSubmit = {
                type: $scope.target.type,
                active: $scope.target.active,
                value: $scope.target.value,
                cpe: $scope.target.cpe,
                responsible_user: $scope.target.responsible_user,
                tags: $scope.target.updatedTags,
                access_groups: $scope.target.access_groups,
                zone: $scope.target.zone,
                hardware_address: $scope.target.hardware_address,
            };
            if ($scope.target.is_explicitly_aliased) {
                targetSubmit.alias = $scope.target.alias;
            } else if ($scope.target.explicit_alias) {
                targetSubmit.alias = $scope.target.explicit_alias;
            }
            targetSubmit.is_explicitly_aliased = !(targetSubmit.alias === undefined || targetSubmit.alias === '');

            $scope.submission = Targets.update(
                { targetId: $state.params.targetId || $scope.target.id },
                targetSubmit,
                function (target) {
                    ($scope.targetSaved || _.noop)(target);

                    if ($scope.target.access_groups) {
                    }

                    $uibModalInstance.close(target);
                },
                function (e) {
                    $scope.errors = $scope.parseErrorResponseToList(e);
                }
            );
        }
    }

    function updateMetaTarget() {
        var targetSubmit = {
            type: $scope.target.type,
            name: $scope.target.name,
            targets: $scope.target.includedTargets || [],
            zone: $scope.target.zone,
        };

        $scope.submission = MetaTargets.update(
            { metaTargetId: $state.params.metaTargetId },
            targetSubmit,
            function (metatarget) {
                ($scope.metatargetSaved || _.noop)(metatarget);
                $uibModalInstance.close(metatarget);
            },
            function (e) {
                $scope.errors = $scope.parseErrorResponseToList(e);
            }
        );
    }

    function bulkCreateTargets() {
        var targetSubmit = {
            customer: $scope.target.customer.id,
            active: $scope.target.active,
            responsible_user: $scope.target.responsible_user,
            type: $scope.target.type,
            createMetaTarget: $scope.target.isMetaTarget,
            name: $scope.target.name,
            tags: $scope.target.updatedTags,
            cpe: $scope.target.cpe,
            access_groups: $scope.target.access_groups,
            zone: $scope.target.zone,
            targets: undefined,
        };

        var targetLinesTemp = $scope.target.bulkTargets.split('\n');
        var targetLines = [];
        // Strip whitespace and ignore empty lines
        _.each(targetLinesTemp, function (line) {
            line = line.trim();
            if (line.length > 0) {
                targetLines.push(line);
            }
        });
        targetLines = _.uniq(targetLines);

        if ((_.isNil(targetSubmit.type) || targetSubmit.type === 'UNKNOWN') && !$scope.target.confirmTypes) {
            delete targetSubmit.type;
            // Auto-detect target types
            var newLines = _.map(targetLines, function (line, i) {
                var parts = line.split(/\s+/);
                if (_.isUndefined(parts[1])) {
                    // Autodetect for line
                    parts[1] = '';
                    if (line.match(/^(?:[a-z0-9\.\-\+]*):\/\//i)) {
                        parts[1] = 'url';
                    } else if (
                        line.match(
                            /^(((?:25[0-5]|2[0-4]\d|[0-1]?\d?\d)(?:\.(?:25[0-5]|2[0-4]\d|[0-1]?\d?\d)){3})|(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])))$/i
                        )
                    ) {
                        parts[1] = 'ipaddress';
                    } else if (
                        line.match(
                            /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\/([0-9]|[1-2][0-9]|3[0-2]))$/i
                        )
                    ) {
                        parts[1] = 'cidrblock';
                    } else if (line.match(/[^@]{2,}@[^@]{2,}/i)) {
                        parts[1] = 'emailaddress';
                    } else {
                        if (
                            line.match(/^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$/i)
                        ) {
                            parts[1] = 'hostname';
                            if (line.indexOf('.') > -1) {
                                parts[1] += ',domain,fqdn';
                            }
                        } else {
                            parts[1] = 'UNKNOWN';
                        }
                    }
                }
                return parts.join('\t');
            });

            $scope.target.bulkTargets = newLines.join('\n');
            $scope.target.confirmTypes =
                'Please verify that all targets are labeled with the correct type.  If any targets have multiple types listed, you must edit it to have only one type or it will not be created.';
            return;
        } else if (targetSubmit.type === 'emailaddress' && !$scope.target.confirmTypes) {
            // Accept a list of `"first last" <email@domain.tld> ...` style addresses as well as plain emails
            //var emailRegex = /(([-!#$%&'*+/=?^_`{}|~0-9A-Z]+(\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*)|("([\001-\010\013\014\016-\037!#-\[\]-\177]|\\[\001-\011\013\014\016-\177])*")@((((?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+)(?:[A-Z0-9-]{2,63}(?<!-)))|(\[([A-f0-9:\.]+)\])))/gi;
            // From Django 1.5.1 (https://stackoverflow.com/a/18368609)
            // This won't match emails that just use a hostname (example@localhost), but it should match any other valid email
            var emailRegex = /(([-!#$%&'*+/=?^_`{}|~0-9A-Z]+(\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*|"([\001-\010\013\014\016-\037!#-\[\]-\177]|\\[\001-\011\013\014\016-\177])*")@((?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?))|(\[([A-f0-9:\.]+)\]))/gi;
            targetLines = $scope.target.bulkTargets.match(emailRegex);

            $scope.target.bulkTargets = targetLines.join('\n');
            $scope.target.confirmTypes = 'Please verify that all extracted email addresses are correct.';
            return;
        }

        // Convert whitespace separated <targetValue targetType> lines to objects
        targetSubmit.targets = _.map(targetLines, function (line) {
            var parts = line.split(/\s+/, 2);
            return {
                value: parts[0],
                type: parts[1],
                cpe: parts[2],
            };
        });

        $scope.submission = Targets.bulkAdd(
            targetSubmit,
            function (res) {
                $scope.target.confirmTypes = false;
                _.each(res.errors, function (err) {
                    if (err.message) {
                        $scope.errors.push(err.value + ': ' + $scope.parseErrorResponse(err.message));
                    } else {
                        $scope.errors.push($scope.parseErrorResponse(err));
                    }
                });
                _.each(res.infos, function (msg) {
                    if (msg.message) {
                        $scope.infos.push(msg.value + ': ' + $scope.parseErrorResponse(msg.message));
                    } else {
                        $scope.infos.push($scope.parseErrorResponse(msg));
                    }
                });
                _.each(res.successes, function (msg) {
                    if (msg.message) {
                        $scope.successes.push(msg.value + ': ' + $scope.parseErrorResponse(msg.message));
                    } else {
                        $scope.successes.push($scope.parseErrorResponse(msg));
                    }
                });
                _.each(res.new_targets, function (target) {
                    ($scope.targetSaved || _.noop)(target);
                });
                if (res.metatarget) {
                    ($scope.metatargetSaved || _.noop)(res.metatarget);
                }
                if ($scope.errors.length === 0 && $scope.infos.length === 0 && $scope.successes.length === 0) {
                    // Added to refresh the graph when adding targets
                    if ($scope.data.activeCategory === 'visualize') {
                        $scope.load_targets_for_visualization();
                    }

                    $uibModalInstance.close(res);
                }
            },
            function (err) {
                if (_.isArray(err.errors)) {
                    var errors = [];
                    _.each(err.errors, function (target) {
                        if (target.error) {
                            errors.push(target.value + ': ' + $scope.parseErrorResponse(target.error, ';\n'));
                        } else {
                            errors.push($scope.parseErrorResponse(target, ';\n'));
                        }
                    });
                    modalService.openError('Failed to create any targets', errors.join('<br/>\n'), false);
                } else {
                    modalService.openErrorResponse('Failed to create any targets', err, false);
                }
            }
        );
    }

    $scope.addOrUpdateTarget = function () {
        $scope.infos = [];
        $scope.errors = [];
        $scope.successes = [];

        if ($scope.target.showBulkCreate) {
            bulkCreateTargets();
        } else if ($scope.edit) {
            if (!$scope.target.isMetaTarget) {
                updateTarget();
            } else {
                updateMetaTarget();
            }
        } else {
            if ($scope.target.isMetaTarget) {
                saveMetaTarget();
            } else {
                saveTarget();
            }
        }
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };

    if (!params.noState) {
        $uibModalInstance.result.then(
            function () {
                $state.go('^');
            },
            function () {
                $state.go('^');
            }
        );
    }

    $scope.processTargets = function ($files) {
        _.each($files, function ($file) {
            if ($file !== null) {
                var reader = new FileReader();
                reader.addEventListener('loadend', function () {
                    $scope.target.bulkTargets += reader.result;
                });
                reader.readAsText($file);
            }
        });
    };

    $scope.cleanBulkCreateState = function () {
        $scope.target.confirmTypes = false;
        $scope.infos = [];
        $scope.errors = [];
        $scope.successes = [];
    };

    $scope.toggleZoneChanges = function () {
        previousZone = $scope.target.zone;
        $scope.zoneChangesDisabled = !$scope.zoneChangesDisabled;
    };

    $scope.setZoneChangeDisabledTypeAndZone = function () {
        previousZone = $scope.target.zone;

        var type = $scope.target.type;
        $scope.internalDisabled = $scope.config.zone.INTERNAL_ALLOWED_TARGET_TYPES.indexOf(type) === -1;
        $scope.target.zone = $scope.internalDisabled ? $scope.gdata.customerExternalZoneMap[$scope.target.customer.id].id : previousZone;
        $scope.zoneChangesDisabled = $scope.edit || $scope.internalDisabled;
    };

    $scope.changeZone = function () {
        var reloadTable = function () {
            if ($scope.metatargetTable) {
                $scope.metatargetTable.reload();
            }
            if ($scope.target.isMetaTarget) {
                var includedTargets = _.remove($scope.targetMap[$scope.target.customer.id][$scope.target.type][previousZone], function (target: any) {
                    return $scope.target.targets.indexOf(target.id) !== -1;
                });
                _.each(includedTargets, function (target: any) {
                    target.zone = $scope.target.zone;
                    if (!$scope.targetMap[$scope.target.customer.id][target.type][target.zone]) {
                        $scope.targetMap[$scope.target.customer.id][target.type][target.zone] = [];
                    }
                    $scope.targetMap[$scope.target.customer.id][$scope.target.type][target.zone].push(target);
                });
            }
            $scope.zoneChangesDisabled = true;
        };

        var helper = function (reply) {
            console.log(reply);

            var targets = "<div style='display: inline-block;text-align:left;vertical-align: top'>Targets:<br>";
            var metatargets = "<div style='display: inline-block;text-align:left;vertical-align: top'>MetaTargets:<br>";
            var message = '';

            _.each(reply.required_targets, function (target) {
                targets += target.value + '<br>';
            });
            targets += '</div>';

            _.each(reply.required_metatargets, function (target) {
                metatargets += target.name + '<br>';
            });
            metatargets += '</div>';

            var action = 'change ' + reply.required_targets.length + ' Targets';
            action += $scope.gdata.isOperator ? ' and ' + reply.required_metatargets.length + ' MetaTargets' : '';

            if ($scope.gdata.isOperator) {
                message =
                    "<div style='display: inline-block'><p>In order to change the zone of this " +
                    ($scope.target.isMetaTarget ? 'MetaTarget,' : 'Target,') +
                    ' you will also have to change the following other</p>' +
                    targets +
                    "<div style='display: inline-block;margin:0 1em;vertical-align: top'>and</div>" +
                    metatargets +
                    '</div>';
            } else {
                message =
                    '<p>In order to change the zone of this Target,' + ' you will also have to change the following other Targets:</p><br>' + targets;
            }

            message += '<br>Is this OK';

            message = $sce.trustAsHtml(message);
            modalService.openConfirm(
                null,
                action,
                message,
                null,
                function () {
                    if ($scope.target.isMetaTarget) {
                        TargetZones.bulkChangeZone(
                            {
                                id: $scope.target.zone,
                                metatarget: $scope.target.id,
                                metatargets: _.map(reply.required_metatargets, 'id'),
                                targets: _.map(reply.required_targets, 'id'),
                            },
                            reloadTable,
                            $scope.changeZone
                        );
                    } else {
                        TargetZones.bulkChangeZone(
                            {
                                id: $scope.target.zone,
                                target: $scope.target.id,
                                metatargets: _.map(reply.required_metatargets, 'id'),
                                targets: _.map(reply.required_targets, 'id'),
                            },
                            reloadTable,
                            $scope.changeZone
                        );
                    }
                },
                function () {
                    $scope.target.zone = previousZone;
                }
            );
        };

        if ($scope.target.id && $scope.target.isMetaTarget) {
            TargetZones.bulkChangeZone(
                {
                    id: $scope.target.zone,
                    metatarget: $scope.target.id,
                    metatargets: [$scope.target.id],
                },
                reloadTable,
                helper
            );
        } else if ($scope.target.id) {
            TargetZones.bulkChangeZone(
                {
                    id: $scope.target.zone,
                    target: $scope.target.id,
                    targets: [$scope.target.id],
                },
                _.noop,
                helper
            );
        }
    };
});
