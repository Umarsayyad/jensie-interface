import app from '../app';
import * as _ from 'lodash';

import '../note/NoteCtrl';
import '../tag/tagsEditor';

app.controller('MetaTargetDetailsLoaderCtrl', function ($scope, $state, $uibModal) {
    if ($scope.gdata.isOperator) {
        $uibModal.open({
            templateUrl: '/app/target/modalMetaTargetDetails.html',
            controller: 'MetaTargetDetailsCtrl',
            size: 'xl',
            scope: $scope,
        });
    }
});

app.controller('MetaTargetDetailsCtrl', function (
    $filter,
    $q,
    $scope,
    $state,
    $timeout,
    $uibModal,
    $uibModalInstance,
    modalService,
    socketService,
    Operations,
    MetaTargets,
    Conclusions,
    OperationService,
    TargetService
) {
    var metaTargetId = parseInt($state.params.id);

    $scope.itemsPerPage = 25;
    $scope.currentOperationsPage = 1;
    $scope.operationState = 'created';
    $scope.operationMap = {};
    $scope.hideOperationCustomer = true;
    var lastStartedOn = null;
    $scope.lastOperation = null;

    if ($scope.data === undefined) {
        $scope.data = {};
    }

    $scope.states = ['draft', 'created', 'requested', 'in progress', 'review', 'completed'];

    $scope.getTargetTypeIcon = TargetService.getTargetTypeIcon;
    $scope.setOperationState = function (state) {
        $scope.operationState = state;
        $scope.loadOperationPage(1);
    };

    $scope.metatarget = MetaTargets.get({ metaTargetId: metaTargetId, detail: 'conclusions' });

    var metatargetUnsubscribe = socketService.subscribeByModel('metatarget', function (metatarget) {
        if (metatarget.id === $scope.metatarget.id) {
            $scope.updateObj($scope.metatarget, metatarget);
        }
    });
    $scope.$on('$destroy', function () {
        metatargetUnsubscribe();
    });

    $scope.notes = MetaTargets.getNotes({ metaTargetId: metaTargetId });

    $scope.operations = [];
    var allOperationsLoaded = $q.defer();

    function getMoreOps(page) {
        Operations.get(
            {
                metatarget: metaTargetId,
                page_size: 1000,
                page: page,
                target_detail: true,
            },
            function (res) {
                $scope.operations = $scope.operations.concat(res.results);
                if (res.next) {
                    getMoreOps(page + 1);
                } else {
                    allOperationsLoaded.resolve($scope.operations);
                }
            }
        );
    }

    getMoreOps(1);

    $scope.metaTargetDetailsLoading = $q.all([$scope.metatarget.$promise, allOperationsLoaded.promise]).then(function () {
        _.each($scope.operations, function (operation) {
            $scope.operationMap[operation.id] = operation;
            if (operation.startedOn !== null) {
                if (lastStartedOn === null) {
                    lastStartedOn = operation.startedOn;
                    $scope.lastOperation = operation;
                } else {
                    if (operation.startedOn > lastStartedOn) {
                        lastStartedOn = operation.startedOn;
                        $scope.lastOperation = operation;
                    }
                }
            }
        });

        $scope.operationsCount = $scope.operations.length;
        $scope.numOperationPages = Math.ceil($scope.operationsCount / $scope.itemsPerPage);
        $scope.loadOperationPage(1);
    });

    $scope.loadOperationPage = function (page) {
        $scope.currentOperationsPage = page;
        $scope.filteredOperations = $filter('filter')($scope.operations, { state: $scope.operationState });
        //                <tr ng-repeat="target in customer.targets | limitTo : itemsPerPage : (currentPage - 1) * itemsPerPage" ng-class="{'inactive': !data.targets[target].active}">
        $scope.data.operations = $filter('limitTo')(
            $filter('orderBy')($scope.filteredOperations, $scope.orderByField, $scope.reverseSort),
            $scope.itemsPerPage,
            ($scope.currentOperationsPage - 1) * $scope.itemsPerPage
        );
    };

    $scope.getState = function () {
        return 'cato.loggedIn.operator';
    };

    $scope.getParams = function (operation) {
        return { service_name: operation.service, campaign_id: operation.campaign, operation_id: operation.id };
    };

    $scope.deleteNote = function (note) {
        modalService.openConfirm('Really delete?', 'delete this note', null, null, function () {
            MetaTargets.deleteNote(
                { metaTargetId: $scope.metatarget.id, noteId: note.id },
                note,
                function (note) {
                    _.remove($scope.notes, { id: note.id });
                },
                function (res) {
                    modalService.openErrorResponse('Note could not be deleted.', res, false);
                }
            );
        });
    };

    $scope.addNote = function (note) {
        var modalInstance = $uibModal.open({
            templateUrl: '/app/note/modalNote.html',
            controller: 'NoteCtrl',
            size: 'lg',
            scope: $scope,
            resolve: {
                params: {
                    metaTargetId: $scope.metatarget.id,
                    note: note,
                    type: 'metatarget',
                },
            },
        });

        modalInstance.result.then(function (note) {
            _.remove($scope.notes, { id: note.id });
            $scope.notes.push(note);
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };

    function modalClose() {
        if (/metaTargetDetails/i.test($state.current.name) && _.isNull($state.transition)) {
            $state.go('^');
        }
    }

    $uibModalInstance.result.then(modalClose, modalClose);
});
