import app from '../app';
import * as _ from 'lodash';

app.controller('AddAccessGroupCtrl', function ($scope, $uibModalInstance, modalService, params, AccessGroups) {
    $scope.target = params.target;
    $scope.groups = [];
    AccessGroups.list({ customer: $scope.target.customer.id || $scope.target.customer }, function (groups) {
        _.each(groups, function (group) {
            if ($scope.target.access_groups.indexOf(group.id) < 0) {
                $scope.groups.push(group);
            }
        });
    });

    _.remove($scope.groups, function (group: any) {
        return $scope.target.access_groups.indexOf(group.id);
    });

    $scope.submit = function () {
        $scope.add_to_access_group.targets.push($scope.target.id);

        AccessGroups.update(
            { accessGroupId: $scope.add_to_access_group.id },
            $scope.add_to_access_group,
            function (access_group) {
                $uibModalInstance.close(access_group);
            },
            function (res) {
                modalService.openErrorResponse('Group could not be added.', res, false);
            }
        );
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});
