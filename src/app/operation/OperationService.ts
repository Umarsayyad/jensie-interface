import app from '../app';
import * as _ from 'lodash';

import '../finding/FindingService';
import './modalAddOperation';

app.service('OperationService', function ($uibModal, stateBuilder, Operations) {
    var operationsConfig;
    var STATES = [
        {
            label: 'draft',
            type: 'warning',
            enabled: true,
        },
        {
            label: 'fix up',
            type: 'danger',
            enabled: false,
        },
        {
            label: 'created',
            type: '',
            enabled: true,
        },
        {
            label: 'requested',
            type: 'violet',
            enabled: true,
        },
        {
            label: 'rejected',
            type: 'danger',
            enabled: false,
        },
        {
            label: 'in progress',
            type: 'info',
            enabled: true,
        },
        {
            label: 'paused',
            type: 'danger',
            enabled: false,
        },
        {
            label: 'review',
            type: 'primary',
            enabled: true,
        },
        {
            label: 'completed',
            type: 'success',
            enabled: true,
        },
        {
            label: 'canceled',
            type: '',
            enabled: false,
        },
    ];

    function stateIndex(state) {
        return _.findIndex(STATES, { label: state });
    }

    function stateClass(state, currentState) {
        var index = stateIndex(state);
        if (index > stateIndex(currentState) && currentState) {
            return 'disabled';
        } else {
            // Don't want to re-find the state
            return 'default-' + STATES[index].type;
        }
    }

    function showRetestDetails(operation) {
        $uibModal.open({
            templateUrl: '/app/operation/modalRetestDetails.html',
            controller: function ($scope, FindingService) {
                $scope.operation = operation;
                $scope.findings = {};
                _.map(operation.retesting_findings, function (finding) {
                    $scope.findings[finding.id] = finding;
                });

                $scope.getSeverityLevel = FindingService.getSeverityLevel;
                $scope.SEVERITY_LEVEL = FindingService.SEVERITY_LEVEL;
            },
            size: 'lg',
        });
    }

    function duplicateOperation(operation) {
        if (!operationsConfig) operationsConfig = Operations.options();

        var data = {
            operation: operation,
            config: operationsConfig.$promise,
        };

        var modal = stateBuilder.componentModalGenerator(
            'modal-add-operation',
            stateBuilder.operationModalBindings,
            data,
            'operation/modalAddOperation',
            'duplicateOpCustomizer'
        );
        $uibModal.open(modal);
    }

    return {
        STATES: STATES,
        stateClass: stateClass,
        showRetestDetails: showRetestDetails,
        duplicateOperation: duplicateOperation,
    };
});
