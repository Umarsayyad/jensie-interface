import app from '../app';
import * as _ from 'lodash';
import * as angular from 'angular';

import { renameDuplicate } from '../common/utilities/rename-duplicate';

import '../common/filters/applianceZoneOrExternal';
import '../roe/utils/viewRoE';
import '../customer/manage/CondensedCustomerFilterComp';

app.factory('followUpOpCustomizer', function () {
    return {
        modalScopePopulated: function ($scope) {
            $scope.operation = angular.copy($scope.operation);
            delete $scope.operation.id;
            delete $scope.operation.state;
            $scope.operation.description = 'Follow-up for finding: ' + $scope.finding.title;
            $scope.operation.name = $scope.operation.name + ' follow-up';
            $scope.reasoningPlaceholder = 'Follow-up requested for operation finding.';
            $scope.modalTitle = 'Create Follow-up Operation';
        },
    };
});

app.factory('duplicateOpCustomizer', function () {
    return {
        modalScopePopulated: function ($scope) {
            $scope.operation = angular.copy($scope.operation);
            delete $scope.operation.id;
            delete $scope.operation.state;
            $scope.operation.name = renameDuplicate($scope.operation.name);
            $scope.modalTitle = 'Create Duplicate Operation';
        },
    };
});

app.component('modalAddOperation', {
    bindings: {
        operation: '<?',
        followUpOperation: '<?',
        finding: '<?',
        operationSaved: '<?',
        reasoningPlaceholder: '<?',
        modalTitle: '<?',
        config: '<',
        close: '<',
        dismiss: '<',
    },
    templateUrl: '/app/operation/modalAddOperation.html',
    controller: function ($rootScope, globalDataService, modalService, Campaigns, MetaTargets, Operations, OperationTemplates, Targets) {
        var ctrl = this;
        ctrl.gdata = globalDataService;
        ctrl.catotheme = $rootScope.catotheme;
        ctrl.user = $rootScope.user;
        ctrl.templateMap = {};

        ctrl.operationTemplates = OperationTemplates.details({ state: 'active' }, function (operationTemplates) {
            // TODO: have op templates annotated with process in API instead of fetching details
            _.forEach(operationTemplates, function (template) {
                template.service = template.service.id;
                if (_.find(template.tasktemplates, { process: 'manual' })) {
                    template.process = 'Manual';
                } else {
                    template.process = 'Automated';
                }
                ctrl.templateMap[template.id] = template;
            });
        });

        ctrl.$onInit = function () {
            // Bindings are now available
            ctrl.reasoningPlaceholder = ctrl.reasoningPlaceholder || 'New operation requested by client.';
            ctrl.modalTitle = ctrl.modalTitle || 'Create New Operation';

            if (_.isUndefined(ctrl.operation)) {
                ctrl.operation = {};
                ctrl.setCustomer(_.get($rootScope.user, 'ui_data.selectedCustomer'));
                ctrl.new = true;
            } else {
                ctrl.operation = $rootScope.makePlainCopy(ctrl.operation);
                if (ctrl.operation.service) {
                    ctrl.operation.service = _.get(globalDataService.serviceMap[ctrl.operation.service], 'id');
                } else {
                    ctrl.operationTemplates.$promise.then(function () {
                        ctrl.operation.service = _.get(globalDataService.serviceMap[ctrl.templateMap[ctrl.operation.template].service], 'id');
                    });
                }
                ctrl.setCustomer({ id: ctrl.operation.customer }, true);
            }
        };

        // Filters
        ctrl.targetFilter = function (value, index, array) {
            if (ctrl.operation.template && _.size(ctrl.templateMap)) {
                return _.includes(ctrl.templateMap[ctrl.operation.template].targettypes, value.type);
            }
            return true;
        };

        // Model change functions
        ctrl.templateSet = function () {
            // Do nothing if no template is selected.
            if (typeof ctrl.operation.template === 'undefined') {
                return;
            }

            if (!ctrl.operation.service) {
                ctrl.operation.service = ctrl.templateMap[ctrl.operation.template].service;
            }
            ctrl.operation.requiredLevel = ctrl.templateMap[ctrl.operation.template].requiredLevel;
        };

        ctrl.setCustomer = function (customer, loadData) {
            var id = _.get(customer, 'id');
            if (id && (ctrl.operation.customer !== id || loadData)) {
                ctrl.campaigns = Campaigns.list({ customer: id, stopped: false }, function (response) {
                    _.each(response, function (campaign) {
                        campaign.display_title = campaign.title + (campaign.started ? ' (started)' : '');
                    });
                });
                ctrl.targets = Targets.basicList({ customer: id });
                ctrl.metatargets = MetaTargets.list({ customer: id });
            }
            ctrl.customer = globalDataService.customerMap[id];
            ctrl.operation.customer = id;
        };

        // Save/submit handlers
        ctrl.saveOperation = function () {
            var sendOp = $rootScope.copyForRequest(ctrl.operation, ctrl.config);
            if (!ctrl.operation.reasoning) {
                // Only default to the placeholder if the original value was unset
                sendOp.reasoning = ctrl.reasoningPlaceholder;
            }

            if (ctrl.operation.id) {
                ctrl.submission = Operations.update(sendOp, saveSuccess, saveError);
            } else {
                ctrl.submission = Operations.create(sendOp, saveSuccess, saveError);
            }
        };

        function saveSuccess(op) {
            op._customer = globalDataService.customerMap[op.customer];
            op._template = ctrl.templateMap[op.template];
            op._service = globalDataService.serviceMap[ctrl.operation.service];
            op._target = _.find(ctrl.targets, { id: op.target });
            op._metatarget = _.find(ctrl.metatargets, { id: op.metatarget });

            (ctrl.operationSaved || _.noop)(op);
            if (!ctrl.addAnother) {
                ctrl.close(op);
            } else {
                ctrl.message = 'Operation saved successfully';
            }
        }

        function saveError(result) {
            modalService.openErrorResponse('Operation could not be saved.', result, false);
        }
    },
});
