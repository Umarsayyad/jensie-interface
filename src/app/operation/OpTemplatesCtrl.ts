import app from '../app';
import * as _ from 'lodash';
import * as angular from 'angular';

import '../common/operator/taskParameters';

app.controller('OpTemplatesCtrl', function (
    $scope,
    $uibModal,
    FileSaver,
    Blob,
    modalService,
    OperationTemplates,
    TaskTemplates,
    ngTableService,
    Services
) {
    $scope.task = {};
    $scope.template = {};
    $scope.data.selectedTemplates = [];
    $scope.allSelectWrapper = {};

    $scope.templatesTable = ngTableService.serverFilteredTable({
        resource: OperationTemplates.paginate,
        loadingCallback: loadingCallback,
    });

    function loadingCallback() {
        $scope.allSelectWrapper.allSelected = false;
    }

    $scope.validateMinMax = function (data, min, max) {
        if (data.length < min) {
            return 'Value is too short, minimum length: ' + min;
        }
        if (data.length > max) {
            return 'Value is too long, maximum length: ' + max;
        }
    };

    $scope.stateIcon = function (state) {
        if (state === 'draft') {
            return 'fa-pencil-square-o text-danger';
        } else if (state === 'active') {
            return 'fa-check-circle-o text-success';
        } else if (state === 'retired') {
            return 'fa-archive text-muted';
        }
    };

    $scope.addOrUpdateOperationTemplate = function (data, id) {
        if (id === undefined) {
            OperationTemplates.addTemplate(
                { templateId: id },
                data,
                function (template) {
                    Services.list().$promise.then(function (services) {
                        console.log('WARNING: MODIFYING $scope.gdata.services!');
                        $scope.gdata.services = services;
                        template.service = $scope.getServiceById(template.service);
                        $scope.updateObjOld(_.find($scope.templatesTable.data, { id: id }), template);
                    });
                },
                function (result) {
                    modalService.openErrorResponse('Operation Template could not be saved.', result, false);
                }
            );
        } else {
            data['id'] = id;
            OperationTemplates.updateTemplate(
                { templateId: id },
                data,
                function (template) {
                    // TODO: This is somehow updating $scope.gdata.services, before we get here...don't know how, though.
                    Services.list().$promise.then(function (services) {
                        console.log('WARNING: MODIFYING $scope.gdata.services!');
                        $scope.gdata.services = services;
                        template.service = $scope.getServiceById(data.service);
                        $scope.updateObjOld(_.find($scope.templatesTable.data, { id: id }), template);
                    });
                },
                function (result) {
                    modalService.openErrorResponse('Operation Template could not be edited.', result, false);
                }
            );
        }
    };

    $scope.deleteSpecificTemplate = function (templateId) {
        if (templateId !== undefined) {
            modalService.openDelete('Confirmation', 'this template', {}, function () {
                var res = OperationTemplates.deleteSpecificTemplate({ templateId: templateId }, {}, function () {
                    var index = _.findIndex($scope.templatesTable.data, { id: templateId });
                    $scope.templatesTable.data.splice(index, 1);
                }).$promise;
            });
        } else {
            $scope.templatesTable.data.shift();
        }
    };

    $scope.getUserById = function (id) {
        return _.find($scope.gdata.users, { id: id });
    };

    $scope.getServiceById = function (id) {
        return _.find($scope.gdata.services, { id: id });
    };

    $scope.toggleTasks = function () {
        // Load templates if they haven't been fetched yet
        if (_.size(this.template.tasktemplatesData) === 0) {
            this.template.tasktemplatesData = OperationTemplates.tasks({ templateId: this.template.id }, function (tasktemplates) {
                // Need to sort the task templates by their order
                tasktemplates.sort(function (a, b) {
                    return a.order - b.order;
                });
            });
        }
        this.showtasks = !this.showtasks;
    };

    OperationTemplates.options(function (result) {
        $scope.operationTemplateConfig = result.actions.POST;
    });

    /* Modal handlers. */
    $scope.deleteTaskTemplate = function (template, taskTemplate) {
        modalService.openDelete('Confirmation', 'this task template', {}, function () {
            OperationTemplates.deleteTask({ templateId: template.id, taskId: taskTemplate.id }, function (result) {
                _.remove(template.tasktemplatesData, { id: taskTemplate.id });
                _.remove(template.tasktemplates, taskTemplate.id);
            });
        });
    };

    $scope.editTaskTemplate = function (template, taskTemplate) {
        $uibModal.open({
            templateUrl: '/app/md/planning/modalEditTasktemplate.html',
            controller: 'editTaskTemplateCtrl',
            size: 'lg',
            scope: $scope,
            resolve: {
                template: template,
                taskTemplate: taskTemplate,
                /* since I have to resolve the templates so I can edit them in the modal's controller, I figured
                 * just adding this method into the primary controller instead of using states and a secondary
                 * loader controller seemed like a reasonable approach.
                 *
                 * XXX: However, by doing it this way, it's officially inconsistent with other pieces of the
                 * application. :) However, :) it's really trivial to convert back and forth between these two
                 * approaches, and this approach generates fewer lines of code.  Since, I imagine nobody is going to
                 * try to go to a modal state directly, it doesn't make as much sense to have them be URL states.
                 */
            },
        });
    };

    $scope.importTemplates = function ($file) {
        if ($file !== null) {
            var reader = new FileReader();
            reader.addEventListener('loadend', function () {
                function displayImportResults(res) {
                    $uibModal.open({
                        templateUrl: '/app/md/planning/modalImportResult.html',
                        controller: function ($scope, $uibModalInstance) {
                            $scope.title = 'Import Result';
                            $scope.logmsgs = res.log;

                            $scope.errors = _.map(res.errors, function (err) {
                                if (!_.isString(err)) {
                                    return angular.toJson(err);
                                }
                                return err;
                            });

                            $scope.ok = function () {
                                $uibModalInstance.dismiss('ok');
                            };
                        },
                        size: 'lg',
                        scope: $scope,
                    });
                }

                OperationTemplates.importTemplates(
                    reader.result,
                    function (res) {
                        displayImportResults(res);
                        $scope.templatesTable.reload();
                    },
                    function (err) {
                        displayImportResults(err);
                    }
                );
            });
            reader.readAsText($file);
        }
    };

    $scope.exportTemplates = function () {
        if ($scope.data.selectedTemplates.length > 0) {
            var ids = _.map($scope.data.selectedTemplates, 'id');
            OperationTemplates.exportTemplates(
                {},
                ids,
                function (res) {
                    var data = new Blob([angular.toJson(res, true)], { type: 'application/json;charset=utf-8' });
                    FileSaver.saveAs(data, 'export.json');
                },
                function (err) {
                    console.log('export error: ', err);
                    modalService.openErrorResponse('Export Error', err, false);
                }
            );
        }
    };

    $scope.$watch('data.selectedTemplates.length', function (newValue, oldValue) {
        $scope.allSelectWrapper.allSelected = newValue !== 0 && newValue === $scope.templatesTable.data.length;
    });

    $scope.dragControlListeners = {
        orderChanged: function (event) {
            angular.forEach(event.dest.sortableScope.modelValue, function (obj, key) {
                obj.order = key;
                TaskTemplates.partial_update({ templateId: obj.template, taskTemplateId: obj.id }, { order: key });
            });
        },
        accept: function (sourceItemHandleScope, destSortableScope) {
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id;
        },
    };
});

/* I include this file here just for convenience, it could be placed in its own file. */
app.controller('editTaskTemplateCtrl', function (
    $scope,
    $state,
    $stateParams,
    $uibModal,
    $uibModalInstance,
    $sce,
    OperationTemplates,
    TaskTemplates,
    modalService,
    template,
    taskTemplate
) {
    $scope.config = {};
    $scope.modalTitle = 'Create or Edit a TaskTemplate';
    $scope.thisTaskTemplate = angular.copy(taskTemplate, {});

    TaskTemplates.options(function (result) {
        $scope.taskConfig = result.actions.POST;
        $scope.taskConfig.workflow.options = {};

        var supported_applications = [];

        _.each($scope.taskConfig.application.choices, function (application) {
            // Check if all of the template's targettypes are supported by the application
            if (_.intersection(template.targettypes, application.supported).length === template.targettypes.length) {
                supported_applications.push(application);
            }

            $scope.taskConfig.workflow.options[application.value] = application.workflows;
        });

        /* fix it so it only lists the ones you can use. */
        $scope.taskConfig.application.choices = supported_applications;
    });

    $scope.saveTaskTemplate = function () {
        if (_.isNull($scope.thisTaskTemplate.workflow)) {
            $scope.thisTaskTemplate.workflow = '';
        }
        if ($scope.thisTaskTemplate.description === undefined) {
            $scope.thisTaskTemplate.description = $scope.thisTaskTemplate.name;
        }

        if ($scope.shouldShowParameterSelection()) {
            // Copy parameter values into thisTaskTemplate.parameters
            var task_params =
                $scope.taskConfig.workflow.options[$scope.thisTaskTemplate.application][$scope.thisTaskTemplate.process][
                    $scope.thisTaskTemplate.workflow
                ].template_parameters;
            $scope.thisTaskTemplate.parameters = {};
            _.each(task_params, function (param) {
                if (!_.isUndefined(param.value)) {
                    $scope.thisTaskTemplate.parameters[param.name] = param.value;
                }
            });
        }

        if (taskTemplate === null) {
            /* Create a new one, add it to the template on success. */
            $scope.thisTaskTemplate.order = template.tasktemplates.length + 1;

            OperationTemplates.addTask(
                { templateId: template.id },
                $scope.thisTaskTemplate,
                function (result) {
                    template.tasktemplatesData.push(result);
                    template.tasktemplates.push(result.id);
                    $uibModalInstance.dismiss();
                },
                function (result) {
                    console.log('result: ' + JSON.stringify(result, null, 2));
                    modalService.openErrorResponse('TaskTemplate could not be saved.', result, false);
                }
            );
        } else {
            /* Update a current one, then update it in the parent modal only on success. */

            OperationTemplates.updateTask(
                { templateId: template.id, taskId: taskTemplate.id },
                $scope.thisTaskTemplate,
                function (result) {
                    /* The changes have been accepted, let's copy the parameter into the parent scope. */
                    angular.copy($scope.thisTaskTemplate, taskTemplate);
                    $uibModalInstance.dismiss();
                },
                function (result) {
                    console.log('result: ' + result);
                    modalService.openErrorResponse('TaskTemplate could not be saved.', result, false);
                }
            );
        }
    };

    $scope.cancel = function () {
        $uibModalInstance.close();
    };

    $scope.shouldShowParameterSelection = function () {
        if (_.has($scope.taskConfig, ['workflow', 'options', $scope.thisTaskTemplate.application, $scope.thisTaskTemplate.process])) {
            var workflows =
                $scope.taskConfig.workflow.options[$scope.thisTaskTemplate.application][$scope.thisTaskTemplate.process][
                    $scope.thisTaskTemplate.workflow
                ];
            return _.size(workflows) > 0 && _.size(workflows.template_parameters) > 0;
        }
        return false;
    };
});
