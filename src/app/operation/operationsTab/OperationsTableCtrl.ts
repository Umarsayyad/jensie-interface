import app from '../../app';
import * as _ from 'lodash';

import '../../common/actionsDropdownComp';
import '../../customer/manage/CondensedCustomerFilterComp';
import '../../target/targetValueFilter/targetValueFilter';

app.controller('OperationsTableCtrl', function (
    $scope,
    $state,
    $rootScope,
    $timeout,
    Campaigns,
    Operations,
    OperationTemplates,
    OperationService,
    MessageService,
    modalService,
    TargetService,
    $uibModal
) {
    $scope.operationsLoaded = false;
    $scope.data = {
        customers: $rootScope.gdata.customers,
        operations: [],
        selectedOperations: [],
        allOperationselected: false,
    };
    $scope.results = {};
    $scope.itemsPerPage = $state.params.page_size || 30;
    $scope.currentPage = $state.params.page || 1;
    $scope.radioModel = $state.params.type || undefined;
    $scope.operationFilters = {};
    $scope.operationTypes = [];
    $scope.operationTypeFilterTags = [];
    $scope.serviceSource = [];
    $scope.serviceFilterTags = [];
    $scope.orderByField = 'updated';
    $scope.showTargetColumn = true;
    $scope.reverseSort = true;
    $scope.showActions = true;
    $scope.getNameOrRoleFromId = MessageService.getNameOrRoleFromId;
    $scope.count = 1e10; // Needed so uib-pagination thinks currentPage exists until we get the real count and doesn't try to jump to page 1

    $scope.stateClass = OperationService.stateClass;
    $scope.operationStates = _.map(OperationService.STATES, 'label');

    var OP_STATES_BY_OP_CONDITION = {
        'Created by Me': ['draft', 'fix up', 'rejected', 'created', 'requested', 'in progress', 'paused'],
        'Assigned to Me': ['in progress', 'fix up', 'paused'],
        'Submitted by Me': ['review'],
        'Recently Started': ['in progress', 'paused', 'review'],
        Unassigned: ['created'],
        Completed: ['completed'],
        Rejected: ['rejected'],
        All: $scope.operationStates,
    };

    var startedByMe = ['Assigned to Me', 'Submitted by Me'];
    if (!$scope.gdata.isMissionDirector) {
        startedByMe.push('Rejected');
    }

    $scope.getProgressBar = function (state) {
        return _.find(OperationService.STATES, { label: state });
    };

    var my_user = $scope.user;
    var my_user_id = my_user.id;
    var my_operator_level = $scope.user.level;

    $scope.getOpUser = function (id) {
        var user: any = _.find($scope.gdata.users, { id: id });
        return user.username;
    };

    $scope.getTargetTypeIcon = TargetService.getTargetTypeIcon;

    $scope.slider = {
        minValue: 1,
        maxValue: my_operator_level,
        options: {
            floor: 1,
            ceil: 10,
            maxLimit: my_operator_level,
            noSwitching: true,
            translate: function (value, sliderId, label) {
                switch (label) {
                    case 'model':
                        return '<b>Min Level:</b> ' + value;
                    case 'high':
                        return '<b>Max Level:</b> ' + value;
                    default:
                        return value;
                }
            },
        },
    };

    $scope.$watch('user.ui_data.selectedCustomer', function (newVal, oldVal) {
        if (newVal !== oldVal && !$state.params.campaign) {
            $scope.customer = newVal;
            loadCustomerOperations(1);
            loadAppliances($scope.customer);
        }
    });

    $scope.$watch('data.selectedOperations.length', function (newValue, oldValue) {
        if ($scope.data.operations === undefined) {
            $scope.data.operations = [];
        }
        $scope.data.allOperationsSelected = newValue !== 0 && newValue === $scope.data.operations.length;
    });

    $scope.customer = _.get($rootScope.user, 'ui_data.selectedCustomer');

    if ($state.params.campaign) {
        $scope.campaign = Campaigns.get({ campaignId: $state.params.campaign }, function () {
            $scope.gdata.customers.$promise.then(function () {
                $scope.customer = $scope.gdata.customerMap[$scope.campaign.customer];
            });
        });
    }

    $scope.clearCampaignFilter = function () {
        $state.go($state.current.name, { campaign: null });
    };

    $scope.selectAllOperations = function () {
        $scope.data.operations = $rootScope.selectAll($scope.data.allOperationsSelected, $scope.data.operations);
    };

    $scope.viewDeleteOp = function (operation) {
        if (!operation) {
            return false;
        }

        if ($scope.gdata.isMissionDirector) {
            return !(operation.state === 'in progress' || operation.state === 'requested');
        }

        return (operation.state === 'created' || operation.state === 'draft') && $scope.gdata.isOperator;
    };

    $scope.viewCancelOp = function (operation) {
        if (operation) {
            // doing it this way so as not to get errors when operation is undefined
            return true;
        }
        return false;
    };

    $scope.viewApproveDisapprove = function (operation) {
        if (operation) {
            if ($scope.gdata.isMissionDirector && operation.state === 'draft') {
                return true;
            }
        }
        return false;
    };

    $scope.viewAuthorizeReject = function (operation) {
        if (operation) {
            if ($scope.gdata.isMissionDirector && operation.state === 'requested') return true;
        }
        return false;
    };

    $scope.viewFixUp = function (operation) {
        if (operation) {
            if (operation.state === 'fix up' && ($scope.gdata.isMissionDirector || operation.author === my_user_id)) {
                return true;
            }
        }
        return false;
    };

    $scope.viewRejected = function (operation) {
        if (operation) {
            if (operation.state === 'rejected' && ($scope.gdata.isMissionDirector || operation.startedBy === my_user_id)) {
                return true;
            }
        }
        return false;
    };

    $scope.showBulkDelete = function () {
        for (const key in $scope.data.selectedOperations) {
            let operation = $scope.data.selectedOperations[key];

            if (!$scope.viewDeleteOp(operation)) {
                return false;
            }
        }
        return true;
    };

    $scope.bulkActions = {
        deleteOperation: {
            action: function () {
                let selected = $scope.data.selectedOperations.map((op) => op.id);

                modalService.openDelete('Confirmation', 'these ' + selected.length + ' operations', {}, function () {
                    $scope.bulkSubmission = Operations.bulkDestroy(
                        {},
                        { ids: selected },
                        function () {
                            for (let i = 0; i < selected.length; i++) {
                                let target_id = selected[i];

                                for (var j = 0; j < $scope.data.operations.length; j++) {
                                    if ($scope.data.operations[j].id === target_id) {
                                        $scope.data.operations.splice(j, 1);
                                        break;
                                    }
                                }

                                // If the list of operations is now zero, refresh the list.
                                if ($scope.data.operations.length === 0) {
                                    if ($scope.numPages === $scope.currentPage && $scope.currentPage !== 1) {
                                        $scope.currentPage--;
                                    }
                                    if ($scope.count) {
                                        loadCustomerOperations($scope.currentPage, true);
                                    }
                                }
                            }
                        },
                        modalService.openErrorResponseCB('Unable to delete selected targets')
                    );
                });
            },
            class: 'btn-white',
            icon: 'fa-trash-o text-danger',
            label: 'Delete',
            show: $scope.showBulkDelete,
            tooltip: 'Bulk Delete Operations',
        },
    };

    $scope.actions = {
        approveOperation: {
            action: function (operation) {
                Operations.approve({ operationId: operation.id }, {}).$promise.then(function (result) {
                    var op: any = _.find($scope.data.operations, { id: operation.id });
                    op.state = result.state;
                });
            },
            class: 'btn-white',
            icon: 'fa-thumbs-o-up text-success',
            label: 'Approve',
            show: $scope.viewApproveDisapprove,
            tooltip: 'Approve Operation',
        },
        authorizeOperation: {
            action: function (operation) {
                Operations.authorize({ operationId: operation.id }, {}).$promise.then(function (result) {
                    var op: any = _.find($scope.data.operations, { id: operation.id });
                    op.state = result.state;
                });
            },
            class: 'btn-white',
            icon: 'fa-thumbs-o-up text-success',
            label: 'Authorize',
            show: $scope.viewAuthorizeReject,
            tooltip: 'Authorize Operation',
        },
        cancelRequestStart: {
            action: function (operation) {
                Operations.cancel_request_start({ operationId: operation.id }, {}).$promise.then(function (result) {
                    var op: any = _.find($scope.data.operations, { id: operation.id });
                    op.state = result.state;
                });
            },
            class: 'btn-white',
            icon: 'fa-thumbs-o-up text-success',
            label: 'Release',
            show: $scope.viewRejected,
            tooltip: $scope.gdata.isMissionDirector ? 'Re-release Operation' : 'Acknowledge Operation Rejection',
        },
        cancelOperation: {
            action: function (operation) {
                modalService.openCancel('Confirmation', 'this operation', {}, function () {
                    $scope.operationLoading = Operations.cancel({ operationId: operation.id }, {}, function (op) {
                        _.remove($scope.data.operations, { id: operation.id });
                    }).$promise;
                });
            },
            class: 'btn-white',
            icon: 'fa-times-circle-o text-danger',
            label: 'Cancel',
            show: $scope.viewCancelOp,
            tooltip: 'Cancel Operation',
        },
        deleteOperation: {
            action: function (operation) {
                modalService.openDelete('Confirmation', 'this operation', {}, function () {
                    $scope.operationLoading = Operations.delete({ operationId: operation.id }, {}, function (op) {
                        _.remove($scope.data.operations, { id: operation.id });

                        // If the list of operations is now zero, refresh the list.
                        if ($scope.data.operations.length === 0) {
                            if ($scope.numPages === $scope.currentPage && $scope.currentPage !== 1) {
                                $scope.currentPage--;
                            }
                            if ($scope.count) {
                                loadCustomerOperations($scope.currentPage, true);
                            }
                        }
                    }).$promise;
                });
            },
            class: 'btn-white',
            icon: 'fa-trash-o text-danger',
            label: 'Delete',
            show: $scope.viewDeleteOp,
            tooltip: 'Delete Operation',
        },
        disapproveOperation: {
            action: function (operation) {
                if (operation) {
                    $scope.data.selectedDraftOperations = [{ id: operation.id }];
                }
                $uibModal.open({
                    templateUrl: '/app/operation/modalDisapproveOperation.html',
                    controller: 'RejectionCtrl',
                    size: 'lg',
                    scope: $scope,
                    resolve: {
                        params: {
                            selObjs: 'selectedDraftOperations',
                            objs: 'operations',
                            type: 'operation',
                            action: 'disapprove',
                            remove: false,
                        },
                    },
                });
            },
            class: 'btn-white',
            icon: 'fa-thumbs-o-down text-danger',
            label: 'Disapprove',
            show: $scope.viewApproveDisapprove,
            tooltip: 'Disapprove Operation',
        },
        duplicateOperation: {
            action: function (operation) {
                OperationService.duplicateOperation(operation);
            },
            class: 'btn-white',
            icon: 'fa-copy',
            label: 'Duplicate',
            show: true,
            tooltip: 'Duplicate Operation',
        },
        fixOperation: {
            action: function (operation) {
                Operations.fix({ operationId: operation.id }, {}).$promise.then(function (result) {
                    var op: any = _.find($scope.data.operations, { id: operation.id });
                    op.state = result.state;
                });
            },
            class: 'btn-white',
            icon: 'label-fix',
            label: 'Fix',
            show: $scope.viewFixUp,
            tooltip: 'Fix Operation',
        },
        rejectOperation: {
            action: function (operation) {
                if (operation) {
                    $scope.data.selectedRequestedOperations = [{ id: operation.id }];
                }
                var modal = $uibModal.open({
                    templateUrl: '/app/md/modalRejectOperation.html',
                    controller: 'RejectionCtrl',
                    size: 'lg',
                    scope: $scope,
                    resolve: {
                        params: {
                            selObjs: 'selectedRequestedOperations',
                            objs: 'operations',
                            type: 'operation',
                            action: 'reject',
                            remove: false,
                        },
                    },
                });
                modal.result.then(function (op) {
                    op.customer = operation.customer;
                    op.target = operation.target;
                    op.template = operation.template;
                    $scope.data.operations.splice(_.findIndex($scope.data.operations, { id: op.id }), 1, op);
                });
            },
            class: 'btn-white',
            icon: 'fa-thumbs-o-down text-danger',
            label: 'Reject',
            show: $scope.viewAuthorizeReject,
            tooltip: 'Reject Operation',
        },
    };

    var loadCustomerOperations = function (page, initial?: any) {
        // Clear out operations
        $scope.data.operations.length = 0;

        // Default params
        var params = {
            ordering: '-added',
            page: page,
            page_size: $scope.itemsPerPage,
            min_level: $scope.slider.minValue,
            max_level: $scope.slider.maxValue,
            target_detail: true,
            customer: undefined,
            state: undefined,
        };

        // Default view selections
        if ($state.params.campaign) {
            params['campaign'] = $state.params.campaign;
            $scope.radioModel = $scope.radioModel || 'All';
        } else {
            params.customer = $scope.customer ? $scope.customer.id : undefined;
        }
        if (_.isNil($scope.radioModel)) {
            if ($scope.gdata.isMissionDirector) {
                $scope.radioModel = 'Recently Started';
            } else {
                $scope.radioModel = 'Unassigned';
            }
        }

        $scope.opStates = OP_STATES_BY_OP_CONDITION[$scope.radioModel];

        // Set up the operation state filters
        var opFilters = _.clone($scope.operationFilters);
        opFilters.state = $scope.operationStateFilters || OP_STATES_BY_OP_CONDITION[$scope.radioModel];

        // Merge opFilters into params (later items override earlier items)
        params = _.assign({}, params, opFilters);

        // For certain operation conditions we want to set special params.
        if ($scope.radioModel === 'Created by Me') {
            params['author'] = my_user_id;
        } else if ($scope.radioModel === 'Unassigned') {
            params['startedBy__isnull'] = 'True';
        }
        if (_.includes(startedByMe, $scope.radioModel)) {
            params['startedBy'] = my_user_id;
        }

        // Save the state filters back into the $scope.operationStateFilters
        $scope.operationStateFilters = params.state;

        if ($scope.backendsearch) {
            params['search'] = $scope.backendsearch;
        }
        if ($scope.targetValue) {
            params['target_lookup_multi_0'] = $scope.targetValue;
            params['target_lookup_multi_1'] = $scope.targetFilterType;
        }

        //console.log('params: ' + JSON.stringify(params, null, 2));

        if ($scope.gdata.isCustomer) {
            params = {
                state: ['completed'],
                page: page,
                page_size: $scope.itemsPerPage,

                //I wish that there was a better way to meet TS requirements
                //but for now, we have to pass all of these undefines
                ordering: undefined,
                min_level: undefined,
                max_level: undefined,
                target_detail: undefined,
                customer: undefined,
            };
            $state.go('.', { page: page, page_size: $scope.itemsPerPage }, initial ? { location: 'replace' } : {});
        } else {
            $state.go('.', { page: page, page_size: $scope.itemsPerPage, type: $scope.radioModel }, initial ? { location: 'replace' } : {});
        }

        $scope.operationsLoaded = false;

        $scope.operationsLoading = Operations.get(params).$promise.then(function (operations) {
            _.each(operations.results, function (op) {
                op.customer = $scope.gdata.customerMap[op.customer];
            });
            _.each(operations.results, function (op) {
                op.serviceName = $scope.gdata.serviceMap[op.service].name;
            });

            $scope.data.operations = operations.results;
            $scope.count = operations.count;
            $scope.numPages = Math.ceil(operations.count / $scope.itemsPerPage);
            $scope.currentPage = page;
            $scope.operationsLoaded = true;
        });
    };

    $scope.getState = function () {
        var state = '';
        if ($scope.gdata.isOperator) {
            state = 'cato.loggedIn.operator';
        } else {
            state = 'cato.loggedIn.service.campaign.operation';
        }
        return state;
    };

    $scope.getParams = function (operation) {
        return { service_name: operation.service, campaign_id: operation.campaign, operation_id: operation.id };
    };

    if ($state.params.customer !== null) {
        $scope.customer = $state.params.customer;
        loadCustomerOperations($scope.currentPage, true);
    } else {
        /* amusing, but just in case we decide we do want a different behavior. */
        loadCustomerOperations($scope.currentPage, true);
    }

    var loadAppliances = function (customer) {
        $scope.gdataAppliances = customer == null ? $rootScope.gdata.appliances : $rootScope.gdata.appliancesByCustomer[customer.id];
        $scope.appliances = $rootScope.loadFilterableAppliances($scope.gdataAppliances);
    };
    loadAppliances($scope.customer);

    $scope.setCustomer = function (customer) {
        $scope.customer = customer;

        loadAppliances(customer);
    };

    $scope.applyTypesFilterRetain = function () {
        loadCustomerOperations(1);
    };

    $scope.applyTypesFilter = function () {
        /* I could have it filter from what was already returned to us instead of hitting the endpoint again, but
         * this is a faster implementation for now.
         *
         * If they're applying filters, reset the page number.
         */
        $scope.operationFilters = {};
        delete $scope.operationStateFilters;
        $scope.applyTypesFilterRetain();
    };

    $scope.loadPage = function (page) {
        loadCustomerOperations(page);
    };

    $scope.setPageSize = function (size) {
        $scope.itemsPerPage = size;
        $scope.loadPage(1);
    };

    OperationTemplates.options(function (result) {
        $scope.operationTypes = _.map(result.actions.POST.operationType.choices, 'value');
    });

    $scope.deleteOpFilterIfEmpty = function (filter) {
        if (!$scope.operationFilters[filter]) delete $scope.operationFilters[filter];
    };

    $scope.operationSaved = function (op) {
        op.target = op._target;
        op.metatarget = op._metatarget;
        op.customer = op._customer;
        op.serviceName = op._service.name;
        op.customerName = op._customer.name;
        $scope.data.operations.unshift(op);
    };

    $scope.$watch('results', function (newValue, oldValue) {
        $timeout(function () {
            $scope.results = {};
        }, 5000);
    });
});
