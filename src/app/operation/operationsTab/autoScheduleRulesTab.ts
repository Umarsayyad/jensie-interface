import app from '../../app';
import * as _ from 'lodash';
import * as angular from 'angular';

import { renameDuplicate } from '../../common/utilities/rename-duplicate';

import '../../common/actionsDropdownComp';
import './modalAutoScheduleRule';

app.component('autoScheduleRulesTab', {
    //bindings: { rules: '<' },
    templateUrl: '/app/operation/operationsTab/autoScheduleRulesTab.html',
    controller: function ($rootScope, $uibModal, globalDataService, modalService, AutoScheduleRules, Campaigns, OperationTemplates) {
        var ctrl = this;
        ctrl.gdata = globalDataService;

        var config;

        // Yes, this could be loaded from the state definition and passed in as a binding, but I don't want to have to inject all of the $resource objects in the state definition, nor do I want to block loading the state until the potentially long-running request to get the data finishes
        ctrl.rules = AutoScheduleRules.list();

        ctrl.actions = {
            editRule: {
                action: function (rule) {
                    ctrl.editRule(rule, false);
                },
                class: 'btn-white',
                icon: 'fa-pencil',
                label: 'Edit',
                tooltip: 'Edit Auto-Schedule Rule',
            },
            copyRule: {
                action: function (rule) {
                    ctrl.editRule(rule, true);
                },
                class: 'btn-white',
                icon: 'fa-copy',
                label: 'Duplicate',
                tooltip: 'Duplicate Auto-Schedule Rule',
            },
            deleteRule: {
                action: function (rule) {
                    ctrl.deleteRule(rule);
                },
                class: 'btn-white',
                icon: 'fa-trash',
                label: 'Delete',
                tooltip: 'Delete this rule',
            },
        };

        ctrl.deleteRule = function (rule) {
            modalService.openDelete('Confirmation', "Auto-Schedule Rule '" + (rule.name || rule.id) + "'", {}, function () {
                rule.$delete(function () {
                    _.remove(ctrl.rules, { id: rule.id });
                }, modalService.openErrorResponseCB('Error deleting Auto-Schedule Rule'));
            });
        };

        ctrl.editRule = function (rule, copy) {
            if (!config) config = AutoScheduleRules.options();

            if (copy && rule) {
                rule = angular.copy(rule);
                rule.name = renameDuplicate(rule.name);
                delete rule.id;
            }

            var scope = $rootScope.$new(true);
            scope['rule'] = rule;
            scope['ruleSaved'] = ruleSaved;

            $uibModal.open({
                // Proxy access to the component via an inline template and controller.  Once we update angular-ui-bootstrap, we can load the component directly in the modal
                template:
                    '<modal-auto-schedule-rule rule="rule" on-rule-saved="ruleSaved(rule)" config="config" uib-modal-instance="$uibModalInstance"></modal-auto-schedule-rule>',
                controller: function ($scope, $uibModalInstance, config) {
                    $scope.$uibModalInstance = $uibModalInstance;
                    $scope.config = config;
                },
                size: 'lg',
                scope: scope,
                resolve: {
                    config: config.$promise,
                },
            });
        };

        function ruleSaved(rule) {
            var curRule = _.find(ctrl.rules, { id: rule.id });
            if (_.isUndefined(curRule)) {
                // the rule is new
                ctrl.rules.push(rule);
            } else {
                $rootScope.updateObjOld(curRule, rule);
            }
        }
    },
});
