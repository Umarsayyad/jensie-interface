import app from '../../app';
import * as _ from 'lodash';
import * as angular from 'angular';
import '../../customer/manage/CondensedCustomerFilterComp';

app.component('modalAutoScheduleRule', {
    templateUrl: '/app/operation/operationsTab/modalAutoScheduleRule.html',
    bindings: {
        rule: '<',
        onRuleSaved: '&',
        uibModalInstance: '<',
        config: '<',
        operationTemplates: '<?',
        campaigns: '<?',
    },
    controller: function (
        $scope,
        $rootScope,
        $state,
        $uibModal,
        $sce,
        modalService,
        globalDataService,
        AutoScheduleRules,
        Campaigns,
        MetaTargets,
        OperationTemplates,
        Targets
    ) {
        var ctrl = this;
        ctrl.gdata = globalDataService;
        ctrl.operationTemplateTypeMap = { undefined: [] };
        ctrl.mtargetsByCustomer = {};
        ctrl.templateKeyTooltip = $sce.trustAsHtml(
            'Name prefixes:<br>A: contains automated tasks<br>M: contains manual tasks<br>B: contains both automated and manual tasks<br>E: empty, contains no tasks'
        );
        var $uibModalInstance;
        var oldName = '';
        ctrl.cpeOptions = globalDataService.getCPE();
        ctrl.catotheme = $rootScope.catotheme;
        ctrl.aAn = $rootScope.aAn;

        ctrl.$onInit = function () {
            // Bindings are now available
            $uibModalInstance = ctrl.uibModalInstance;
            // Initialize or copy rule object
            if (_.isUndefined(ctrl.rule)) {
                ctrl.modalTitle = 'ADD AUTO-SCHEDULE RULE';
                ctrl.rule = {
                    name: '',
                    // TODO: OPTIONS metadata doesn't include default values, so keep these in sync with the model
                    interval: 7,
                    maximum_count: 0,
                    cpe_match_method: 'exact',
                };
                ctrl.new = true;
            } else {
                ctrl.modalTitle = 'EDIT AUTO-SCHEDULE RULE';
                ctrl.rule = angular.copy(ctrl.rule);

                // TODO: handle being passed a rule that doesn't already have the target or metatarget relationships expanded (if present)
                if (ctrl.rule.target) {
                    ctrl.selected_target_type = ctrl.rule.target.type;
                } else if (ctrl.rule.metatarget) {
                    ctrl.selected_target_type = ctrl.rule.metatarget.type;
                }
            }

            // Convert numberic IDs to objects that Angular can match with list of objects in ng-options
            $rootScope.idsToObjs(ctrl.rule, ['campaign', 'target', 'metatarget', 'template']);

            // Fetch data for select fields that wasn't passed as a binding
            if (_.isNil(ctrl.operationTemplates)) {
                ctrl.operationTemplates = OperationTemplates.details(processOperationTemplates);
            } else {
                processOperationTemplates();
            }
            if (_.isNil(ctrl.campaigns)) {
                ctrl.campaigns = Campaigns.list(campaignsLoaded);
            } else {
                campaignsLoaded();
            }

            // Prepare operationTemplateTypeMap with possible target types
            _.each(ctrl.config.target_type.choices, function (type) {
                ctrl.operationTemplateTypeMap[type.value] = [];
            });
        };

        function processOperationTemplates() {
            if (ctrl.rule.template && !ctrl.rule.template.targettypes) {
                ctrl.rule.template = _.find(ctrl.operationTemplates, ctrl.rule.template);
            }
            // Generate displayName for each operation template and construct operationTemplateTypeMap
            _.each(ctrl.operationTemplates, function (opTemplate) {
                var processes = {
                    automated: false,
                    manual: false,
                };
                _.each(opTemplate.tasktemplates, function (task) {
                    processes[task.process] = true;
                });
                if (processes.automated && processes.manual) {
                    opTemplate.displayName = 'B: ' + opTemplate.name;
                } else if (processes.automated) {
                    opTemplate.displayName = 'A: ' + opTemplate.name;
                } else if (processes.manual) {
                    opTemplate.displayName = 'M: ' + opTemplate.name;
                } else {
                    opTemplate.displayName = 'E: ' + opTemplate.name;
                }
            });

            _.each(ctrl.operationTemplates, function (optemp) {
                _.each(optemp.targettypes, function (type) {
                    ctrl.operationTemplateTypeMap[type].push(optemp);
                });
            });
            ctrl.operationTemplateTypeMap['undefined'] = ctrl.operationTemplates;
            ctrl.operationTemplateTypeMap['null'] = ctrl.operationTemplates;
            ctrl.generateRuleName();
        }

        function campaignsLoaded() {
            if (ctrl.rule.campaign) {
                ctrl.rule.campaign = _.find(ctrl.campaigns, ctrl.rule.campaign);

                ctrl.customerChanged(globalDataService.customerMap[ctrl.rule.campaign.customer]);
                ctrl.customer_name = globalDataService.customerMap[ctrl.rule.campaign.customer].name;
            }
        }

        // Model change functions
        ctrl.customerChanged = function ($item) {
            ctrl.customer = $item;
            if (!ctrl.mtargetsByCustomer[$item.id]) {
                ctrl.mtargetsByCustomer[$item.id] = {
                    targets: Targets.basicList({ customer: $item.id }),
                    metatargets: MetaTargets.list({ customer: $item.id }),
                };
            }
            ctrl.generateRuleName();
        };

        ctrl.mtargetChanged = function (objtype) {
            ctrl.selected_target_type = _.get(ctrl.rule[objtype], 'type');
            ctrl.generateRuleName();
        };

        ctrl.generateRuleName = function () {
            var newName = 'Rule for ' + ctrl.customer.name;
            if (ctrl.rule.target) {
                //newName += "'s Target " + ctrl.rule.target.value;
            } else if (ctrl.rule.metatarget) {
                //newName += "'s MetaTarget " + ctrl.rule.metatarget.name;
            } else {
                newName += "'s targets";
                if (ctrl.rule.target_type) {
                    newName += ' of type ' + ctrl.rule.target_type;
                }
                if (ctrl.rule.cpe) {
                    newName += ' with CPE matching ' + ctrl.rule.cpe;
                }
            }
            if (ctrl.rule.template) {
                newName += ' to create operation for template ' + ctrl.rule.template.name;
            }
            if (!ctrl.rule.name || ctrl.rule.name === oldName) {
                ctrl.rule.name = newName;
            }
            oldName = newName;
        };

        // Filters
        ctrl.targetFilter = function (value, index, array) {
            if (ctrl.rule.target_type) {
                return value.type === ctrl.rule.target_type;
            }
            if (ctrl.rule.template) {
                return _.includes(ctrl.rule.template.targettypes, value.type);
            }
            // TODO: optionally filter by CPE?
            return true;
        };

        ctrl.templateFilter = function (value, index, array) {
            if (ctrl.rule.target_type) {
                return _.includes(value.targettypes, ctrl.rule.target_type);
            }
            if (ctrl.selected_target_type) {
                return _.includes(value.targettypes, ctrl.selected_target_type);
            }
            return true;
        };

        // Preview matched targets
        ctrl.previewTargets = function () {
            var params = {
                customer: ctrl.rule.campaign.customer,
            };
            params['cpe__i' + ctrl.rule.cpe_match_method] = ctrl.rule.cpe;
            if (ctrl.rule.target_type) {
                params['type'] = ctrl.rule.target_type;
            } else if (ctrl.rule.template.targettypes) {
                params['type'] = ctrl.rule.template.targettypes;
            }

            ctrl.targetsForPreview = Targets.basicList(params);

            $uibModal.open({
                templateUrl: '/app/operation/operationsTab/modalRuleMatchedTargets.html',
                controller: function (targets) {
                    this.targets = targets;
                    this.rule = ctrl.rule;
                },
                controllerAs: '$ctrl',
                size: 'lg',
                resolve: {
                    targets: ctrl.targetsForPreview.$promise,
                },
            });
        };

        // Save/submit handlers
        ctrl.addOrUpdateRule = function () {
            var sendRule = $rootScope.copyForRequest(ctrl.rule, ctrl.config);

            if (_.isUndefined(ctrl.rule.id)) {
                ctrl.submission = AutoScheduleRules.add(sendRule, ruleSaveSuccess, ruleSaveError);
            } else {
                ctrl.submission = AutoScheduleRules.update(sendRule, ruleSaveSuccess, ruleSaveError);
            }
        };

        function ruleSaveSuccess(rule) {
            rule.target = ctrl.rule.target;
            rule.metatarget = ctrl.rule.metatarget;
            ctrl.onRuleSaved({ rule: rule });
            if (!ctrl.addAnother) {
                $uibModalInstance.close(rule);
            }
        }

        function ruleSaveError(result) {
            modalService.openErrorResponse('Auto-Schedule Rule could not be saved.', result, false);
        }

        ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    },
});
