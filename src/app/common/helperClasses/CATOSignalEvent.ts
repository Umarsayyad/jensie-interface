import app from '../../app';

export interface ICATOSignalEvent<T> {
    subscribe(handler: { (data?: any): void });
    emit(data?: T): any;
}

export class CATOSignalEvent<T> implements ICATOSignalEvent<T> {
    private handlers: { (data?: T): any }[] = [];

    public subscribe(handler: { (data?: any): void }) {
        this.handlers.push(handler);
        let isSubscribed = true;
        return () => {
            if (!isSubscribed) {
                return;
            }
            isSubscribed = false;
            this.handlers = this.handlers.filter((filterHandler) => filterHandler !== handler);
        };
    }

    public emit(data?: T) {
        if (this.handlers) {
            this.handlers.slice(0).forEach((handler) => handler(data));
        }
    }
}
