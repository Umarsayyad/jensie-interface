import app from '../../app';

app.component('applianceFilter', {
    templateUrl: '/app/common/applianceFilter/applianceFilterForm.html',
    bindings: {
        setAppliance: '&',
        clearAppliance: '&?',
        initialAppliance: '<?',
        catoNgDisabled: '<?',
        appliances: '<',
    },
    controller: function applianceFilterCtrl($rootScope, globalDataService, $attrs) {
        //console.log("in Appliance Filter controller");

        var ctrl = this;

        var applianceCleared = true;
        ctrl.gdata = globalDataService;
        ctrl.showSelectAppliance = false;
        ctrl.required = 'required' in $attrs;
        ctrl.form = {};

        ctrl.$onChanges = function (changes) {
            var keepApplianceValue = false;
            if (changes.appliances && ctrl.form.appliance) {
                var currentAppliances = changes.appliances.currentValue;
                for (var i = 0; !keepApplianceValue && i < currentAppliances.length; i++) {
                    if (currentAppliances[i].customer === ctrl.form.appliance.customer) {
                        keepApplianceValue = true;
                    }
                }
            }

            if (!keepApplianceValue) {
                ctrl.clear();
            }
        };

        ctrl.set = function (item) {
            ctrl.showSelectAppliance = false;
            applianceCleared = false;
            ctrl.setAppliance({ appliance: item });
            ctrl.form.appliance = item;
        };

        ctrl.clear = function () {
            if (ctrl.catoNgDisabled) {
                return;
            }
            delete ctrl.form.appliance;
            ctrl.showSelectAppliance = true;
            applianceCleared = true;
            (ctrl.clearAppliance || ctrl.setAppliance)({ appliance: undefined });
        };

        ctrl.checkBlur = function () {
            if (!ctrl.form.appliance && !applianceCleared) {
                // if appliance isn't set and clear hasn't been called (or some other external change)
                ctrl.clear();
            }
        };
    },
});
