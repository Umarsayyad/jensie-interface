import 'jquery';
import app from '../app';
import * as _ from 'lodash';
import * as angular from 'angular';
import catotheme from '@cato/config/themes/catotheme';

import { WebSocketBridge } from 'django-channels';

import { config } from '@cato/config';
import { INewSessionService } from './sessionService/NewSessionService.service';

import './resources';
import './sessionService/NewSessionService.service';
import '../roe/utils/viewRoE';
import { updateObj } from './utils/updateObj';
import { getWebsocketUpdateCallback, IWebsocketObject } from './utils/getWebsocketUpdateCallback';
import { MatDialog } from '@angular/material/dialog';
import { AttachmentComponent } from '../../ng-app/common/components/attachment-upload/attachment.component';
import { AttachmentService } from '../../ng-app/common/services/attachment.service';

app.constant('CATO_API', config.apiPath);

app.controller('IndexCtrl', function (
    $rootScope,
    $state,
    $transitions,
    $q,
    sessionService,
    globalDataService,
    socketService,
    parseErrorResponseService,
    ROE_STATE,
    Users,
    modalService
) {
    $rootScope.sessionService = sessionService;
    $rootScope.MAXIMUM_OPERATOR_LEVEL = 10;

    $rootScope.getRndInteger = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };

    $rootScope.hasRole = function (role) {
        return sessionService.hasRole(role);
    };

    $rootScope.hasAnyRole = function (roles) {
        return sessionService.hasAnyRole(roles);
    };

    $rootScope.startsWithVowel = function (string) {
        return /^[aeiou]/i.test(string);
    };

    $rootScope.aAn = function (string) {
        if ($rootScope.startsWithVowel(string)) {
            return 'an';
        } else {
            return 'a';
        }
    };

    $rootScope.dismissMotd = function () {
        $rootScope.motdIsDismissed = true;
        localStorage.setItem('motdDismissed', 'true');
    };

    $rootScope.notificationPermission = Notification ? Notification.permission : 'denied';

    $rootScope.dismissNotificationWarning = function () {
        $rootScope.notificationWarningIsDismissed = true;
        localStorage.setItem('notificationWarningDismissed', 'true');
    };

    $rootScope.requestNotificationPermission = function () {
        Notification.requestPermission(function (e) {
            if (e === 'granted') {
                $rootScope.notificationsPermitted = true;
                _.defer(function () {
                    $rootScope.$apply(function () {
                        $rootScope.notificationsPermitted = true;
                    });
                });
            }
        });
    };

    $rootScope.switchCustomer = function (customer_id) {
        if ($rootScope.user.customer !== customer_id) {
            Users.patch(
                { userId: $rootScope.user.id },
                { customer: customer_id },
                function (user) {
                    socketService.disconnect();
                    $rootScope.loadingDefer = $q.defer();
                    $rootScope.loadingPromise = $rootScope.loadingDefer.promise;
                    sessionService.loadSession(user).then(function () {
                        $rootScope.loadingDefer.resolve();
                        if ($state.includes('cato.loggedIn.service')) {
                            $state.go('cato.loggedIn.service', {}, { reload: 'cato.loggedIn.service' });
                        } else {
                            $state.reload($state.current.name.split('.', 3).join('.'));
                        }
                    });
                },
                function (res) {
                    modalService.openErrorResponse("Failed to change user's customer.", res, false);
                }
            );
        }
    };

    $rootScope.selectAll = function (selected, objs) {
        _.each(objs, function (o) {
            o.selected = selected;
        });
        return objs;
    };

    if (localStorage.getItem('notificationWarningDismissed')) {
        $rootScope.notificationWarningIsDismissed = true;
    }

    $rootScope.newItemCounts = {};

    function updateObjOld(object, other) {
        if (_.isArray(object) && !_.isArray(other)) {
            // Merging an object into an array is almost certainly incorrect usage so throw an exception to inform the developer of the bug.
            var msg = 'You are trying to merge an object into an array.  Did you mean to merge into an object contained in the array?';
            console.error(msg, 'array:', object, 'object:', other);
            console.trace();
            throw msg;
        }

        function customizer(objValue, srcValue, key, object, source, stack) {
            // object/objValue, destination object getting merged into
            // source/srcValue, object being merged into destination
            if ((_.isNull(objValue) && _.isNumber(srcValue)) || (_.isNumber(objValue) && _.isNull(srcValue))) {
                // Allow null to overwrite and be overwritten by numbers
                return undefined; // Default action
            }
            if (!_.isNumber(objValue) && _.isNumber(srcValue)) {
                // Numbers don't overwrite non-numbers
                return objValue;
            }
            if (_.isString(key) && key[0] === '$') {
                // Don't overwrite properties that start with $
                return objValue;
            }
            if (_.isArray(objValue) && _.isArray(srcValue) && _.size(objValue) > 0 && !_.isNumber(objValue[0]) && _.isNumber(srcValue[0])) {
                return objValue;
            }
            return undefined;
        }

        return _.mergeWith(object, other, customizer);
    }

    $rootScope.updateObjOld = updateObjOld;

    function loadFilterableAppliances(applianceList) {
        let appliances = _.sortBy(applianceList, ['name']);
        appliances.unshift({ name: 'No Appliance Used', id: 'external', verified: true });
        return appliances;
    }

    $rootScope.loadFilterableAppliances = loadFilterableAppliances;

    $rootScope.updateObj = updateObj;

    function uuid() {
        // NOT A REAL UUID/GUID!
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }

        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    $rootScope.uuid = uuid;

    function guid() {
        return uuid();
    }

    $rootScope.guid = guid;

    $rootScope.parseErrorResponse = parseErrorResponseService.parseErrorResponse;
    $rootScope.parseErrorResponseToList = parseErrorResponseService.parseErrorResponseToList;

    /**
     * Simplify and clean up object to send to the API as an HTTP request
     * Only non-read-only fields accepted by the endpoint and those specified in `keep` or `preserve` are included in the returned object, and nested objects with `id` fields are replaced with just the `id` value.
     * @param {Object} obj - The object containing user-provided data to copy and simplify/prepare for sending
     * @param {Object} config - The endpoint configuration from an OPTIONS request to the API
     * @param {string[]} [keep=['id']] - Optional list of additional fields to include on the returned object (defaults to ['id'])
     * @param {string[]} [preserve] - Optional list of fields that should not be modified when simplifying nested objects
     * @returns {Object} Object ready to be sent as request data
     */
    $rootScope.copyForRequest = function (obj, config, keep, preserve) {
        if (_.isUndefined(keep)) {
            keep = ['id'];
        }
        if (_.isArray(preserve)) {
            keep = keep.concat(preserve);
        }

        var sendObj = _.pick(obj, _.keys(config));
        sendObj = _.pickBy(sendObj, function (v, k) {
            return _.includes(keep, k) || (!config[k].read_only && k[0] !== '$');
        });

        // cleanup objects to IDs
        $rootScope.makePlain(sendObj, preserve);
        return sendObj;
    };

    $rootScope.idsToObjs = function (obj, props) {
        _.each(props, function (p) {
            if (_.isNumber(obj[p])) {
                obj[p] = { id: obj[p] };
            }
        });
    };

    $rootScope.makePlainCopy = function (obj, dest, preserve) {
        if (_.isUndefined(dest)) {
            dest = {};
        }
        _.each(obj, function (v, k) {
            if (preserve && _.includes(preserve, k)) {
                dest[k] = v;
                return;
            }
            var v2;
            if (_.isArray(v)) {
                v2 = _.map(v, function (vv) {
                    return _.get(vv, 'id') || vv;
                });
            } else if (_.isObject(v)) {
                v2 = _.get(v, 'id');
            }
            dest[k] = v2 || v;
        });
        return dest;
    };

    $rootScope.makePlain = function (obj, preserve) {
        return $rootScope.makePlainCopy(obj, obj, preserve);
    };

    function restrictStateAccessWithoutRoE(transition) {
        if (globalDataService.isCustomer) {
            if (!globalDataService.customer.current_rules_of_engagement) {
                var state = transition.$to();
                if (!state.includes[ROE_STATE] && !state.includes['cato.logout']) {
                    // Customer does not have RoE and user is going to a non-RoE page, send them to fill out the RoE
                    return $state.target(ROE_STATE, {}, { location: 'replace' });
                }
            }
        }
    }

    $transitions.onBefore({ to: 'cato.loggedIn.**' }, restrictStateAccessWithoutRoE); // onBefore prevents fetching dependencies for destination state that won't be entered when the user clicks a link
    $transitions.onEnter({ to: 'cato.loggedIn.**' }, restrictStateAccessWithoutRoE); // onEnter is needed to redirect when loading the page because onBefore is called before the globalDataService is loaded

    var fix_height = _.throttle(function () {
        $('#page-wrapper').css('min-height', $(window).height() - 80 + 'px');
    }, 100);

    $(window).bind('resize', fix_height);

    $rootScope.$on('$viewContentLoaded', fix_height);
});

app.constant('SESSION_TIMEOUT', 30); // Minutes

app.service('sessionService', function ($rootScope, $injector, $state, NewSessionService: INewSessionService) {
    var sessionData: any = {};
    var activityInterval;

    $rootScope._sessionData = sessionData;

    $rootScope.$on('updateActivity', function () {
        sessionData.lastActive = new Date().valueOf();
        localStorage.setItem('lastActive', sessionData.lastActive);
        console.debug(sessionData.lastActive, new Date().valueOf() - sessionData.lastActive);
    });

    function setSessionData(_session) {
        sessionData = _session;
        // startActivityTimer();

        if (sessionData.motd && sessionData.motd.message) {
            $rootScope.motd = sessionData.motd.message;
        }

        $rootScope.username = sessionData.username;

        $rootScope.$broadcast('user-session-loaded');
    }

    function endSession() {
        clearInterval(activityInterval);
        $rootScope.loggedIn = false;
        $rootScope.loginEvaluated = false;
        sessionData = {};
        delete $rootScope.motdIsDismissed;
        delete $rootScope.notificationWarningIsDismissed;
        $state.go('cato.logout');
    }

    function filterRoles(f, roles) {
        return _.filter(roles ? roles : sessionData.roles, f);
    }

    return {
        setSessionData: setSessionData,
        // startActivityTimer: startActivityTimer,
        /*refresh: function () {
            $http({
                url: CATO_API + '/user/prefs'
            }).success(function (userSession) {  // $http().success() is removed in Angular 1.6
                sessionService.setSessionData(userSession.data);
            });
        },*/
        updateActivity: function () {
            sessionData.lastActive = new Date().valueOf();
            localStorage.setItem('lastActive', sessionData.lastActive);
        },
        getLastActive: function () {
            return sessionData.lastActive;
        },
        getSession: function () {
            return sessionData;
        },
        getUsername: function () {
            return sessionData.username;
        },
        getRoles: function () {
            return sessionData.roles;
        },
        filterRoles: filterRoles,
        hasFilteredRoles: function (f, roles) {
            return _.size(filterRoles(f, roles)) > 0;
        },
        hasRole: function (role) {
            return _.includes(sessionData.roles, role.toLowerCase());
        },
        hasAnyRole: function (roles) {
            return (
                _.size(
                    _.intersection(
                        sessionData.roles,
                        _.map(roles, function (role) {
                            return role.toLowerCase();
                        })
                    )
                ) > 0
            );
        },
        getOptions: function () {
            return sessionData.ui_data;
        },
        setOptions: function (options) {
            sessionData.ui_data = options;
            //TODO: update DB
        },
        setKey: function (k, v) {
            sessionData[k] = v;
        },
        getKey: function (k) {
            return sessionData[k];
        },

        ///////////////////////////////////////////////
        //REMOVED TO MAKE WAY FOR NewSessionService
        //THE COMPATIBILITY SHIM IS BELOW THIS SEGMENT
        ///////////////////////////////////////////////
        // loadSession: function(user) {
        //     setSessionData(user);
        //     if ($rootScope.user && $rootScope.user.id === user.id) {
        //         $rootScope.updateObjOld($rootScope.user, user);
        //     }
        //     else {
        //         $rootScope.user = user;
        //     }
        //     var globalDataService = $injector.get('globalDataService');
        //     $rootScope.gdata = globalDataService.load();

        //     if (user.type === 'customer') {
        //         globalDataService.customer.$promise.then(function() {
        //             if (!globalDataService.customer.current_rules_of_engagement && !$state.transition && !$state.$current.includes[ROE_STATE]) {
        //                 // If there isn't a transition active, such as when an account manager changes customers, we need to check for the customer's RoE and redirect if needed
        //                 $state.go(ROE_STATE, {}, {location: 'replace'});
        //             }
        //         });
        //     }

        //     $rootScope.sessionEvaluated = true;
        //     $rootScope.loggedIn = true;

        //     socketService.connect();

        //     // On the API side knox only saves 'token[:CONSTANTS.TOKEN_KEY_LENGTH]', which defaults to 8.
        //     socketService.subscribeByGroup('u-' + user.id + '-session-watch-' + $rootScope.token.substring(0, 8),
        //         function() {
        //             $http({
        //                 url: config.apiPath + 'user/me'
        //             });
        //         }
        //     );
        //     // No need to unsubscribe; on logout, the UI disconnects the websocket, destroying all subscriptions
        //     return globalDataService.promise;
        // },
        loadSession: function (user) {
            NewSessionService.loadSession(user);

            setSessionData(user);
            if ($rootScope.user && $rootScope.user.id === user.id) {
                $rootScope.updateObjOld($rootScope.user, user);
            } else {
                $rootScope.user = user;
            }
            var globalDataService = $injector.get('globalDataService');
            $rootScope.gdata = globalDataService;

            $rootScope.loggedIn = true;

            return globalDataService.promise;
        },
        endSession: endSession,
    };
});

app.service('dpNotificationService', function () {
    return {
        notify: function (title, body, on, options) {
            options = options || {};
            options.body = body;
            if (!options.tag) {
                options.tag = _.uniqueId();
            }
            if (Notification) {
                var n = new Notification(title, options);
                _.each(['error', 'click', 'show', 'close'], function (e) {
                    if (on && on[e]) {
                        n['on' + e] = on[e];
                    }
                });
                return n;
            }
        },
    };
});

app.service('parseErrorResponseService', function () {
    return {
        parseErrorResponse: function parseErrorResponse(response, divider) {
            if (_.isUndefined(divider)) {
                divider = '\n<br/>';
            }
            return this.parseErrorResponseToList(response, divider).join(divider);
        },
        parseErrorResponseToList: function parseErrorResponseToList(response, divider) {
            var _this = this;
            if (_.isError(response)) {
                console.error(response);
                return ['An unknown error occurred.'];
            } else if (_.isString(response)) {
                return [response];
            } else if (_.isArray(response)) {
                return _.pull(
                    _.map(response, function (item) {
                        if (_.isString(item)) {
                            return item;
                        } else {
                            return _this.parseErrorResponse(item, divider);
                        }
                    }),
                    undefined
                );
            } else {
                // response is a dictionary of error messages
                return _.pull(
                    _.map(response, function (v, k) {
                        if (k === 'non_field_errors') {
                            return _.isArray(v) ? v.join(' ') : v;
                        } else if (_.isPlainObject(v)) {
                            return k + ': ' + _this.parseErrorResponse(v, divider);
                        } else if (_.isArray(v)) {
                            var res = _this.parseErrorResponse(v, divider);
                            return res.length ? k + ': ' + (_.isArray(res) ? res.join(' ') : res) : undefined;
                        } else if (_.isFunction(v)) {
                            return undefined;
                        } else if (k === 'resource' && _.get(v, 'constructor.name') === 'Resource' && _.isFunction(v.toJSON)) {
                            return undefined;
                        }
                        return k + ': ' + v;
                    }),
                    undefined
                );
            }
        },
    };
});

app.service('modalService', function ($uibModal, $sce, $timeout, $interpolate, parseErrorResponseService) {
    function dismiss(inst, timeout) {
        if (timeout === undefined) {
            timeout = 4000;
        }

        $timeout(function () {
            inst.dismiss('ok');
        }, timeout);
    }

    function modalController(title, message) {
        return function ($scope, $uibModalInstance) {
            $scope.title = title || 'Confirmation';
            $scope.message = message;

            $scope.ok = function () {
                $uibModalInstance.dismiss('ok');
            };
        };
    }

    function yesNoCancelController(title, message, yesCallback, noCallback) {
        return function ($scope, $uibModalInstance) {
            $scope.title = title;
            $scope.message = message;

            $scope.yes = function () {
                yesCallback();
                $uibModalInstance.close('yes');
            };

            $scope.no = function () {
                noCallback();
                $uibModalInstance.close('no');
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
    }

    function confirmationController(title, message, action) {
        return function ($scope, $uibModalInstance) {
            $scope.title = title || 'Confirm';
            $scope.message = message;
            $scope.action = action;

            $scope.yes = function () {
                $uibModalInstance.close('ok');
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
    }

    function dataController(data) {
        // TODO: generalize this so `data.finding` doesn't need to be defined
        return function ($scope, $uibModalInstance) {
            $scope.title = data.finding.title || '';
            $scope.data = data;
            $scope.finding = data.finding;

            $scope.ok = function () {
                $uibModalInstance.dismiss('ok');
            };
        };
    }

    return {
        openSuccess: function (title, message, timeout) {
            //message = $sce.trustAsHtml(message);
            var inst = $uibModal.open({
                templateUrl: '/app/partials/successModal.html',
                controller: modalController(title, message),
            });

            if (timeout !== false) {
                dismiss(inst, timeout);
            }

            return inst;
        },
        openError: function (title, message, timeout) {
            //message = $sce.trustAsHtml(message);
            var inst = $uibModal.open({
                templateUrl: '/app/partials/errorModal.html',
                controller: modalController(title || 'Error', message),
            });

            if (timeout !== false) {
                dismiss(inst, timeout);
            }

            return inst;
        },
        openErrorResponse: function (title, response, timeout) {
            var message = parseErrorResponseService.parseErrorResponse(response);
            return this.openError(title, message, timeout);
        },
        openErrorResponseCB: function (title, timeout?, callback?) {
            var modalService = this;
            return function (response) {
                var message = parseErrorResponseService.parseErrorResponse(response);
                if (_.isUndefined(timeout)) {
                    timeout = false;
                }
                modalService.openError(title, message, timeout);
                (callback || _.noop)(response);
            };
        },
        openDelete: function (title, message, data, confirmedCb) {
            return this.openConfirm(title, 'delete', message, data, confirmedCb);
        },
        openCancel: function (title, message, data, confirmedCb) {
            return this.openConfirm(title, 'cancel', message, data, confirmedCb);
        },
        openYesNoCancel: function (title, message, yesCallback, noCallback) {
            var inst = $uibModal.open({
                templateUrl: '/app/partials/yesnocancelModal.html',
                controller: yesNoCancelController(title, message, yesCallback, noCallback),
            });

            return inst;
        },
        openConfirm: function (title, action, message, data, confirmedCb, cancelCb) {
            /*
             * Open confirmation modal with body:
             * Are you sure you want to {{::action}} <span ng-bind-html="::message"></span>?
             */
            //message = $sce.trustAsHtml(message);
            var inst = $uibModal.open({
                templateUrl: '/app/partials/confirmModal.html',
                controller: confirmationController(title, message, action),
            });

            inst.result.then(
                function success(result) {
                    (confirmedCb || _.noop)(data);
                },
                function dismissed(result) {
                    if (cancelCb) {
                        cancelCb();
                    }
                    console.log('modal dismissed.');
                }
            );

            return inst;
        },
        openData: function (title, message, data) {
            message = $interpolate(message)(data);
            message = $sce.trustAsHtml(message);
            return $uibModal.open({
                templateUrl: '/app/partials/dataModal.html',
                controller: modalController(title, message),
                size: 'xl',
            });
        },
        openDataURL: function (data, dataURL) {
            // TODO: generalize dataController so `data.finding` doesn't need to be defined
            return $uibModal.open({
                templateUrl: dataURL,
                controller: dataController(data),
            });
        },
    };
});

app.service('globalDataService', function (
    $rootScope,
    $q,
    modalService,
    socketService,
    formatFactory,
    Appliances,
    CPE,
    Customers,
    Findings,
    Messages,
    Providers,
    Reports,
    Services,
    Targets,
    TargetTags,
    TargetZones,
    Users,
    NewSessionService: INewSessionService
) {
    // TODO: need to handle updating this object any time its contents are updated
    var globalData: any = {};
    var initialKeys: any;
    var rootScopeKeys: any;

    function handleError(err) {
        modalService.openErrorResponse(`An error occurred while loading ${catotheme.appName}; please refresh the page.`, err, false);
    }

    /**
     * Listen for changes to a given model via websocket and update the stored data, optionally using model-specific functions
     * @param {string} model - The type of object for which to listen for changes, will be passed to socketService.subscribeByModel
     * @param {Array} [objArray] - Array of existing model objects (Required unless add and delete functions are passed)
     * @param {Object} objMap - Map of known models keyed by ID, used to determine whether to add or update
     * @param {function} [addFunction] - Function to call when a new object is received to store it
     * @param {function} [updateFunction] - Function to call when new data for an existing object is received
     * @param {function} [deleteFunction] - Function to call when an object is deleted
     */
    function registerObjectWebsocketUpdates<T extends IWebsocketObject>(
        model: string,
        objArray: T[],
        objMap: { any: T },
        addFunction?: (item: T) => void,
        updateFunction?: (item: T) => void,
        deleteFunction?: (item: T) => void
    ) {
        socketService.subscribeByModel(model, getWebsocketUpdateCallback(objArray, objMap, addFunction, updateFunction, deleteFunction));
    }

    function updateCustomerZonesWithoutAppliances(customerId) {
        var zonesWithAppliancesIds = _.map(globalData.appliancesByCustomer[customerId], 'zone');
        var customerZoneIds = _.map(_.filter(globalData.targetZones, { customer: customerId }), 'id');
        globalData.zonesWithoutAppliancesByCustomer[customerId] = _.difference(customerZoneIds, zonesWithAppliancesIds);
    }

    function populateUserInfo(user) {
        // Update the user object so that we have a combination of user info for our type-ahead
        user['userInfo'] = user.first_name + ' ' + user.last_name + ' (' + user.email + ')';
    }

    globalData.unload = function () {
        /*
         * Delete all of the properties of the globalDataService that weren't present before loading data
         * so everything is reset back to its initial state when the user logs out and nothing is leaked
         * between users or to unauthorized parties via the browser's developer console
         */
        var addedKeys = _.difference(_.keys(globalData), initialKeys);
        _.each(addedKeys, function (k) {
            delete globalData[k];
        });
        _.each(rootScopeKeys, function (k) {
            delete $rootScope[k];
        });
    };

    globalData.load = function () {
        var promises = [];
        let user = NewSessionService.getSession();
        // Helper properties
        globalData.isCustomer = NewSessionService.hasRole('customer');
        globalData.isAccountManager = NewSessionService.hasRole('account_manager');
        globalData.isOperator = NewSessionService.hasRole('operator'); // All mission directors are operators. If that changes, the globalDataService will need to be updated.
        globalData.isInsight = NewSessionService.hasRole('insight');
        globalData.isMissionDirector = NewSessionService.hasRole('mission_director');
        globalData.isMissionDirectorOrAdmin = NewSessionService.hasAnyRole(['administrator', 'mission_director']);
        globalData.isPointOfContact = user.pocs !== undefined && user.pocs.length > 0;
        rootScopeKeys = _.difference(_.keys(globalData), initialKeys);

        globalData.reportingEnabled = $rootScope.reportingEnabled = Reports.enabled();

        // General items for all users
        globalData.serviceMap = {};
        globalData.services = Services.list(function (services) {
            _.each(services, function (service) {
                globalData.serviceMap[service.id] = service;
                globalData.serviceMap[service.short_name] = service;
            });
        }, handleError);
        promises.push(globalData.services.$promise);

        globalData.targetGroups = Targets.groups();

        globalData.messageOptions = Messages.options(function (result) {
            angular.extend(globalData.messageOptions, result.actions.POST);
        }, handleError);
        promises.push(globalData.messageOptions.$promise);

        globalData.customerOptions = Customers.options(function (result) {
            angular.extend(globalData.customerOptions, result.actions.POST);
        }, handleError);
        promises.push(globalData.customerOptions.$promise);

        globalData.findingOptions = Findings.options(
            {},
            function (result) {
                var config = result.actions.POST;
                angular.extend(globalData.findingOptions, config);
                globalData.findingStateFilters = _.map(config.state.choices, 'value');
            },
            handleError
        );
        promises.push(globalData.findingOptions.$promise);

        globalData.notifications = Users.notifications({ userId: user.id }, _.noop, handleError);
        promises.push(globalData.notifications.$promise);

        globalData.userMap = {};
        globalData.operators = {};

        globalData.users = Users.list(
            {},
            function (users) {
                _.each(globalData.users, function (user) {
                    populateUserInfo(user);
                    globalData.userMap[user.id] = user;
                    if (user.type === 'operator') {
                        // TODO: should this use groups instead of type?
                        globalData.operators[user.id] = user;
                    }
                });
            },
            handleError
        );
        promises.push(globalData.users.$promise);

        globalData.applianceMap = {};
        globalData.appliances = Appliances.list(
            {},
            function (appliances) {
                _.each(appliances, function (appliance) {
                    globalData.applianceMap[appliance.id] = appliance;
                });
            },
            handleError
        );
        promises.push(globalData.appliances.$promise);
        registerObjectWebsocketUpdates('appliance', globalData.appliances, globalData.applianceMap, undefined, globalData.updateAppliance);

        globalData.targetTagMap = {};
        globalData.targetTags = TargetTags.list(
            {},
            function (tags) {
                _.each(tags, function (tag) {
                    globalData.targetTagMap[tag.id] = tag;
                });
            },
            handleError
        );
        promises.push(globalData.targetTags.$promise);
        registerObjectWebsocketUpdates('targettag', globalData.targetTags, globalData.targetTagMap);

        globalData.targetZoneMap = {};
        globalData.customerExternalZoneMap = {};
        globalData.targetZones = TargetZones.list(
            {},
            function (zones) {
                _.each(zones, function (zone) {
                    globalData.targetZoneMap[zone.id] = zone;
                    if (zone.external) {
                        globalData.customerExternalZoneMap[zone.customer] = zone;
                    }
                });
            },
            handleError
        );
        promises.push(globalData.targetZones.$promise);
        registerObjectWebsocketUpdates('targetzone', globalData.targetZones, globalData.targetZoneMap, function (zone: any) {
            globalData.targetZones.push(zone);
            globalData.targetZoneMap[zone.id] = zone;
            if (zone.external) {
                globalData.customerExternalZoneMap[zone.customer] = zone;
            }
        });

        globalData.providerMap = {};
        globalData.providerNameMap = {};
        globalData.providers = Providers.list(
            {},
            function (providers) {
                _.each(providers, function (provider) {
                    globalData.providerMap[provider.id] = provider;
                    globalData.providerNameMap[provider.name] = provider;
                });
            },
            handleError
        );
        promises.push(globalData.providers.$promise);
        registerObjectWebsocketUpdates(
            'provider',
            globalData.providers,
            globalData.providerMap,
            function (provider: any) {
                globalData.providers.unshift(provider);
                globalData.providerMap[provider.id] = provider;
                globalData.providerNameMap[provider.name] = provider;
            },
            function (provider: any) {
                var oldName = globalData.providerMap[provider.id].name;
                $rootScope.updateObj(globalData.providerMap[provider.id], provider);
                if (oldName !== provider.name) {
                    globalData.providerNameMap[provider.name] = globalData.providerMap[provider.id];
                }
            }
        );

        globalData.customerMap = {};
        // Items for customers only
        if (globalData.isAccountManager) {
            globalData.customers = Customers.list(
                {},
                function (customers) {
                    _.each(customers, function (customer) {
                        globalData.customerMap[customer.id] = customer;
                    });
                    globalData.customer = globalData.customerMap[$rootScope.user.customer];
                    globalData.customer.$promise = globalData.customers.$promise;
                },
                handleError
            );
            globalData.customer = { $promise: globalData.customers.$promise };
            promises.push(globalData.customers.$promise);
        } else if (globalData.isCustomer) {
            globalData.customer = Customers.get(
                { customerId: user.customer },
                function (customer) {
                    globalData.customerMap[customer.id] = customer;
                },
                handleError
            );
            globalData.customers = [globalData.customer];
            globalData.customers.$promise = globalData.customer.$promise;
            promises.push(globalData.customers.$promise);
        }
        // Items for operators only
        // TODO: Branch logic here should be split if we eventually need insight/operator-specific logic
        else if (globalData.isOperator || globalData.isInsight) {
            globalData.customers = Customers.list(
                {},
                function (customers) {
                    _.each(customers, function (customer) {
                        globalData.customerMap[customer.id] = customer;
                    });
                },
                handleError
            );
            promises.push(globalData.customers.$promise);
        }
        registerObjectWebsocketUpdates(
            'customer',
            globalData.customers,
            globalData.customerMap,
            globalData.addCustomer,
            globalData.updateCustomer,
            _.noop
        );

        globalData.appliancesByCustomer = {};
        globalData.appliancesByCustomerPromise = $q.all([globalData.appliances.$promise, globalData.customers.$promise]).then(function () {
            _.each(globalData.customers, function (customer) {
                globalData.appliancesByCustomer[customer.id] = [];
            });
            _.each(globalData.appliances, function (appliance) {
                if (appliance.customer) {
                    globalData.appliancesByCustomer[appliance.customer].push(appliance);
                }
            });
        });

        globalData.customerInfoPromise = $q.all([globalData.users.$promise, globalData.customers.$promise]).then(function () {
            _.each(globalData.users, function (user) {
                if (user.customer) {
                    user.customerInfo = globalData.customerMap[user.customer];
                }
            });
            // Copy the array so we can add to it without polluting the main array
            globalData.messagingUsers = globalData.users.concat();
            if (user.type === 'customer') {
                var companyAndOperatorGroup = {
                    userInfo: 'Everyone at ' + globalData.customer.name,
                    customer: globalData.customer,
                    isCustomer: true,
                    for_operators: false,
                };
                globalData.messagingUsers.unshift(companyAndOperatorGroup);
                companyAndOperatorGroup = {
                    userInfo: 'Everyone at ' + globalData.customer.name + ' and Operators',
                    customer: globalData.customer,
                    isCustomer: true,
                    for_operators: true,
                };
                globalData.messagingUsers.unshift(companyAndOperatorGroup);
            }
            // Add 'All Operators'
            var operatorGroup = { userInfo: 'All Operators', for_operators: true };
            globalData.messagingUsers.unshift(operatorGroup);
        });

        globalData.zonesWithoutAppliancesByCustomer = {};
        globalData.zonesWithoutAppliancesByCustomerPromise = $q
            .all([globalData.appliancesByCustomerPromise, globalData.targetZones.$promise])
            .then(function () {
                _.each(globalData.customers, function (customer) {
                    updateCustomerZonesWithoutAppliances(customer.id);
                });
            });

        globalData.promise = $q.all(promises);

        return globalData;
    };

    globalData.updateAppliance = function (appliance) {
        var oldZone = globalData.applianceMap[appliance.id].zone;
        $rootScope.updateObjOld(globalData.applianceMap[appliance.id], appliance);
        if (
            appliance.customer &&
            globalData.appliancesByCustomer &&
            !_.find(globalData.appliancesByCustomer[appliance.customer], { id: appliance.id })
        ) {
            globalData.appliancesByCustomer[appliance.customer].push(globalData.applianceMap[appliance.id]);
        }
        if (oldZone !== appliance.zone) {
            updateCustomerZonesWithoutAppliances(appliance.customer);
        }
    };

    globalData.addCustomer = function (customer) {
        if (globalData.customerMap[customer.id]) {
            return; // Protect against double-add
        }
        globalData.customers.push(customer);
        globalData.customerMap[customer.id] = customer;
        globalData.appliancesByCustomer[customer.id] = [];
    };

    globalData.updateCustomer = function (customer) {
        $rootScope.updateObj(globalData.customerMap[customer.id], customer);
    };

    globalData.addUser = function (user) {
        if (user.customer) {
            user.customerInfo = globalData.customerMap[user.customer];
        }
        populateUserInfo(user);
        globalData.users.push(user);
        globalData.messagingUsers.push(user);
        globalData.userMap[user.id] = user;
        if (user.type === 'operator') globalData.operators[user.id] = user;
    };

    globalData.updateUser = function (user) {
        if (user.customer) {
            user.customerInfo = globalData.customerMap[user.customer];
        }
        populateUserInfo(user);
        $rootScope.updateObjOld(globalData.userMap[user.id], user);
    };

    globalData.deleteUser = function (userId) {
        _.remove(globalData.users, { id: userId });
        _.remove(globalData.messagingUsers, { id: userId });
        delete globalData.userMap[userId];
        delete globalData.operators[userId];
    };

    globalData.addNotification = function (notification) {
        globalData.notifications.push(notification);
    };

    globalData.updateNotification = function (notification) {
        $rootScope.updateObjOld(_.find(globalData.notifications, { id: notification.id }), notification);
    };

    globalData.deleteNotification = function (notificationId) {
        _.remove(globalData.notifications, { id: notificationId });
    };

    globalData.CPE = undefined;
    globalData.getCPE = function () {
        if (!globalData.CPE) {
            globalData.CPE = CPE.get();
        }
        return globalData.CPE;
    };

    enum BUSINESS {
        WOSB = 'Women-Owned Small Business',
        VOSB = 'Veteran-Owned Small Business',
        EIGHTA = '8(a) Small Business',
        HUBZONE = 'Historically Underutilized Business (HUBZone)',
        SDVOSB = 'Service-Disabled Veteran-Owned Small Business',
        IEE = 'Indian Economic Enterprise',
    }
    globalData.business_category = [BUSINESS.WOSB, BUSINESS.VOSB, BUSINESS.EIGHTA, BUSINESS.HUBZONE, BUSINESS.SDVOSB, BUSINESS.IEE];

    globalData.setCustomerBusinessValues = function (customer) {
        customer.business_category = [];
        if (customer.has_women_owned_business) {
            customer.business_category.push(BUSINESS.WOSB);
        }

        if (customer.has_veteran_owned_business) {
            customer.business_category.push(BUSINESS.VOSB);
        }
        if (customer.has_8a_small_business) {
            customer.business_category.push(BUSINESS.EIGHTA);
        }
        if (customer.has_service_disabled_veteran_business) {
            customer.business_category.push(BUSINESS.SDVOSB);
        }
        if (customer.has_hubzone_business) {
            customer.business_category.push(BUSINESS.HUBZONE);
        }
        if (customer.has_indian_economic_enterprise) {
            customer.business_category.push(BUSINESS.IEE);
        }

        return customer;
    };

    globalData.setCustomerBusinessBooleans = function (customer) {
        //must reset booleans to ensure they are only set based on what's in business_category array
        customer.has_women_owned_business = false;
        customer.has_veteran_owned_business = false;
        customer.has_8a_small_business = false;
        customer.has_service_disabled_veteran_business = false;
        customer.has_hubzone_business = false;
        customer.has_indian_economic_enterprise = false;

        for (var i = 0; i < customer.business_category.length; i++) {
            var businessCategory = customer.business_category[i];

            if (businessCategory == BUSINESS.WOSB) {
                customer.has_women_owned_business = true;
            } else if (businessCategory == BUSINESS.VOSB) {
                customer.has_veteran_owned_business = true;
            } else if (businessCategory == BUSINESS.EIGHTA) {
                customer.has_8a_small_business = true;
            } else if (businessCategory == BUSINESS.SDVOSB) {
                customer.has_service_disabled_veteran_business = true;
            } else if (businessCategory == BUSINESS.HUBZONE) {
                customer.has_hubzone_business = true;
            } else if (businessCategory == BUSINESS.IEE) {
                customer.has_indian_economic_enterprise = true;
            }
        }

        return customer;
    };

    globalData.naics = [
        { sector: '11', name: 'Agriculture, Forestry, Fishing and Hunting' },
        { sector: '21', name: 'Mining, Quarrying, and Oil and Gas Extraction' },
        { sector: '22', name: 'Utilities' },
        { sector: '23', name: 'Construction' },
        { sector: '31-33', name: 'Manufacturing' },
        { sector: '42', name: 'Wholesale Trade' },
        { sector: '44-45', name: 'Retail Trade' },
        { sector: '48-49', name: 'Transportation and Warehousing' },
        { sector: '51', name: 'Information' },
        { sector: '52', name: 'Finance and Insurance' },
        { sector: '53', name: 'Real Estate and Rental and Leasing' },
        { sector: '54', name: 'Professional, Scientific, and Technical Services' },
        { sector: '55', name: 'Management of Companies and Enterprises' },
        { sector: '56', name: 'Administrative and Support and Waste Management and Remediation Services' },
        { sector: '61', name: 'Educational Services' },
        { sector: '62', name: 'Health Care and Social Assistance' },
        { sector: '71', name: 'Arts, Entertainment, and Recreation' },
        { sector: '72', name: 'Accommodation and Food Services' },
        { sector: '81', name: 'Other Services (except Public Administration)' },
        { sector: '92', name: 'Public Administration' },
    ];

    globalData.naicsMap = {};
    _.each(globalData.naics, function (code) {
        globalData.naicsMap[code.sector] = code.name + ' (' + code.sector + ')';
    });

    globalData.countries = [
        'Afghanistan',
        'Albania',
        'Algeria',
        'Andorra',
        'Angola',
        'Anguilla',
        'Antigua & Barbuda',
        'Argentina',
        'Armenia',
        'Aruba',
        'Australia',
        'Austria',
        'Azerbaijan',
        'Bahamas',
        'Bahrain',
        'Bangladesh',
        'Barbados',
        'Belarus',
        'Belgium',
        'Belize',
        'Benin',
        'Bermuda',
        'Bhutan',
        'Bolivia',
        'Bosnia & Herzegovina',
        'Botswana',
        'Brazil',
        'British Virgin Islands',
        'Brunei',
        'Bulgaria',
        'Burkina Faso',
        'Burundi',
        'Cambodia',
        'Cameroon',
        'Canada',
        'Cape Verde',
        'Cayman Islands',
        'Chad',
        'Chile',
        'China',
        'Colombia',
        'Congo',
        'Cook Islands',
        'Costa Rica',
        'Cote D Ivoire',
        'Croatia',
        'Cruise Ship',
        'Cuba',
        'Cyprus',
        'Czech Republic',
        'Denmark',
        'Djibouti',
        'Dominica',
        'Dominican Republic',
        'Ecuador',
        'Egypt',
        'El Salvador',
        'Equatorial Guinea',
        'Estonia',
        'Ethiopia',
        'Falkland Islands',
        'Faroe Islands',
        'Fiji',
        'Finland',
        'France',
        'French Polynesia',
        'French West Indies',
        'Gabon',
        'Gambia',
        'Georgia',
        'Germany',
        'Ghana',
        'Gibraltar',
        'Greece',
        'Greenland',
        'Grenada',
        'Guam',
        'Guatemala',
        'Guernsey',
        'Guinea',
        'Guinea Bissau',
        'Guyana',
        'Haiti',
        'Honduras',
        'Hong Kong',
        'Hungary',
        'Iceland',
        'India',
        'Indonesia',
        'Iran',
        'Iraq',
        'Ireland',
        'Isle of Man',
        'Israel',
        'Italy',
        'Jamaica',
        'Japan',
        'Jersey',
        'Jordan',
        'Kazakhstan',
        'Kenya',
        'Kuwait',
        'Kyrgyz Republic',
        'Laos',
        'Latvia',
        'Lebanon',
        'Lesotho',
        'Liberia',
        'Libya',
        'Liechtenstein',
        'Lithuania',
        'Luxembourg',
        'Macau',
        'Macedonia',
        'Madagascar',
        'Malawi',
        'Malaysia',
        'Maldives',
        'Mali',
        'Malta',
        'Mauritania',
        'Mauritius',
        'Mexico',
        'Moldova',
        'Monaco',
        'Mongolia',
        'Montenegro',
        'Montserrat',
        'Morocco',
        'Mozambique',
        'Namibia',
        'Nepal',
        'Netherlands',
        'Netherlands Antilles',
        'New Caledonia',
        'New Zealand',
        'Nicaragua',
        'Niger',
        'Nigeria',
        'Norway',
        'Oman',
        'Pakistan',
        'Palestine',
        'Panama',
        'Papua New Guinea',
        'Paraguay',
        'Peru',
        'Philippines',
        'Poland',
        'Portugal',
        'Puerto Rico',
        'Qatar',
        'Reunion',
        'Romania',
        'Russia',
        'Rwanda',
        'Saint Pierre & Miquelon',
        'Samoa',
        'San Marino',
        'Satellite',
        'Saudi Arabia',
        'Senegal',
        'Serbia',
        'Seychelles',
        'Sierra Leone',
        'Singapore',
        'Slovakia',
        'Slovenia',
        'South Africa',
        'South Korea',
        'Spain',
        'Sri Lanka',
        'St Kitts & Nevis',
        'St Lucia',
        'St Vincent',
        'St. Lucia',
        'Sudan',
        'Suriname',
        'Swaziland',
        'Sweden',
        'Switzerland',
        'Syria',
        'Taiwan',
        'Tajikistan',
        'Tanzania',
        'Thailand',
        "Timor L'Este",
        'Togo',
        'Tonga',
        'Trinidad & Tobago',
        'Tunisia',
        'Turkey',
        'Turkmenistan',
        'Turks & Caicos',
        'Uganda',
        'Ukraine',
        'United Arab Emirates',
        'United Kingdom',
        'United States',
        'United States Minor Outlying Islands',
        'Uruguay',
        'Uzbekistan',
        'Venezuela',
        'Vietnam',
        'Virgin Islands (US)',
        'Yemen',
        'Zambia',
        'Zimbabwe',
    ];

    globalData.formatFactory = formatFactory;

    initialKeys = _.keys(globalData);
    return globalData;
});

app.service('socketService', function ($rootScope, Users) {
    var socketBridge,
        isConnected,
        _connected,
        _disconnected,
        requestConnected,
        subscribeQueue = [],
        sendQueue = [],
        subscriptions = {
            group: {},
            model: {},
        };

    function connect(__connected, __disconnected) {
        if (socketBridge) {
            console.error('socketService.connect() was called without calling .disconnect() first!  Ignoring connect request.');
            return;
        }
        _connected = __connected;
        _disconnected = -__disconnected;
        requestConnected = true;

        socketBridge = new WebSocketBridge();
        console.debug('socketBridge: ', socketBridge);

        var options = {
            maxReconnectionDelay: 2 * 60 * 1000, // 2 minutes
            debug: config.prod !== 'true',
        };
        socketBridge.connect('/api/ws/', [], options);
        socketBridge.socket.addEventListener('error', function (e) {
            console.log('connection error: ', e);
        });
        socketBridge.socket.addEventListener('open', doWSauth);
        socketBridge.socket.addEventListener('close', disconnected);
        socketBridge.demultiplex('auth', connected);
        socketBridge.demultiplex('unsubscribe', function (response) {
            console.log('Unsubscribe result: ', response);
        });
        socketBridge.demultiplex('updates', function (response) {
            $rootScope.$applyAsync(function () {
                _.each(subscriptions['model'][response.model], function (cb) {
                    cb(angular.copy(response.data));
                });
                _.each(subscriptions['group'][response.group], function (cb) {
                    cb(angular.copy(response.data));
                });
            });
        });

        socketBridge.listen();

        function doWSauth() {
            if ($rootScope.loggedIn) {
                // Do auth if logged in, otherwise wait for for the login controller to request auth
                Users.getWSauth(
                    function (res) {
                        socketBridge.stream('auth').send({ token: res.token });
                    },
                    function (err) {
                        console.log('Auth request error: ', err);
                        // If requesting an auth token failed, the websocket connection was probably lost too
                        // so this will be called again once it reconnects
                    }
                );
            }
        }

        function connected() {
            if (!isConnected) {
                isConnected = true;
                if (_connected) {
                    _connected();
                }

                _.defer(function () {
                    // Re-request any existing subscriptions on reconnect
                    _.each(subscriptions.group, function (cbs: any, group) {
                        if (cbs.length > 0) {
                            subscribeByGroup(group, cbs, true);
                        }
                    });
                    while (subscribeQueue.length) {
                        subscribeByGroup.apply(undefined, subscribeQueue.pop());
                    }
                });

                $rootScope.$broadcast('connected');
                console.log('Connected!');
            }
        }
    }

    function disconnect() {
        requestConnected = false;

        // Clear queues and subscriptions
        subscribeQueue = [];
        sendQueue = [];
        subscriptions = {
            group: {},
            model: {},
        };

        if (socketBridge && socketBridge.socket) {
            // Calling the default close causes the ReconnectingWebsocket to retry!
            var closeOptions = { keepClosed: true };
            socketBridge.socket.close(1000, 'Closing normally!', closeOptions);
        }
        // We check to see if the socketBridge is defined whenever we initiate the connect call
        socketBridge = undefined;
    }

    function disconnected() {
        isConnected = false;
        if (_disconnected) {
            _disconnected();
        }

        $rootScope.$broadcast('disconnected');
        console.log('Disconnected!');
    }

    function subscribeByGroup(group: string, callback: Function, onlySend?: boolean) {
        var subscribeArguments = arguments;
        if (isConnected) {
            if (_.isUndefined(subscriptions['group'][group])) {
                subscriptions['group'][group] = [];
            }
            if (!onlySend) {
                subscriptions['group'][group].push(callback);
            }

            // Tell the API we want data for this group
            socketBridge.stream('subscribe').send({ group: group });
        } else {
            subscribeQueue.push(arguments);
        }

        return function unsubscribe() {
            _.pull(subscribeQueue, subscribeArguments); // Remove from queue if still there

            // Can't use _.remove because it tries to call the callback
            var sub = subscriptions['group'][group];
            if (sub) {
                var idx = sub.indexOf(callback);
                if (idx > -1) {
                    subscriptions['group'][group].splice(idx, 1);
                }
            }

            if (isConnected) {
                // Tell the API we no longer want data for this group
                socketBridge.stream('unsubscribe').send({ group: group });
            }
            // If no longer connected, nothing needs to be done
        };
    }

    function subscribeByModel(model: string, callback: Function, onlySend?: boolean) {
        // Model subscriptions are pre-registered in the API, so we just need to keep track of the callback functions

        if (_.isUndefined(subscriptions['model'][model])) {
            subscriptions['model'][model] = [];
        }
        subscriptions['model'][model].push(callback);

        return function unsubscribe() {
            // Can't use _.remove because it tries to call the callback
            var sub = subscriptions['model'][model];
            if (sub) {
                var idx = sub.indexOf(callback);
                if (idx > -1) {
                    subscriptions['model'][model].splice(idx, 1);
                }
            }
        };
    }

    function send(route, body) {
        if (isConnected) {
            socketBridge.stream(route).send(body);
        } else {
            sendQueue.push(arguments);
        }
    }

    return {
        connect: connect,
        disconnect: disconnect,
        subscribeByGroup: subscribeByGroup,
        subscribeByModel: subscribeByModel,
        send: send,
        isConnected: function () {
            return isConnected;
        },
    };
});

app.service('importExportService', function ($q, $uibModal, FileSaver, Blob, modalService) {
    this.importer = function (provider, importComplete, importFailed) {
        return function ($file) {
            var result = $q.defer();
            if ($file !== null) {
                var reader = new FileReader();
                reader.addEventListener('loadend', function () {
                    var resource = provider(reader.result);
                    $uibModal.open({
                        templateUrl: '/app/md/planning/modalImportResult.html',
                        controller: function ($scope, $uibModalInstance, parseErrorResponseService) {
                            $scope.title = 'Import Result';
                            $scope.submission = resource.$promise;

                            function displayResult(response) {
                                if (response.log || response.errors) {
                                    $scope.logmsgs = response.log;

                                    $scope.errors = _.map(response.errors, function (err) {
                                        if (!_.isString(err)) {
                                            return angular.toJson(err);
                                        }
                                        return err;
                                    });
                                } else {
                                    $scope.errors = parseErrorResponseService.parseErrorResponseToList(response);
                                }
                            }

                            resource.$promise.then(
                                function (response) {
                                    result.resolve(response);
                                    (importComplete || _.noop)(response);
                                    displayResult(response);
                                },
                                function (response) {
                                    result.reject(response);
                                    (importFailed || _.noop)(response);
                                    displayResult(response);
                                }
                            );

                            $scope.ok = function () {
                                $uibModalInstance.dismiss('ok');
                            };
                        },
                        size: 'lg',
                    });
                });
                reader.readAsText($file);
            }
        };
    };

    this.exporter = function (provider) {
        return function (toExport) {
            if (toExport.length > 0) {
                if (_.isObject(toExport[0])) {
                    toExport = _.map(toExport, 'id');
                }
                provider(
                    {},
                    toExport,
                    function (res) {
                        var data = new Blob([angular.toJson(res, true)], { type: 'application/json;charset=utf-8' });
                        FileSaver.saveAs(data, 'export.json');
                    },
                    modalService.openErrorResponseCB('Export Error')
                );
            }
        };
    };
});

app.directive('lowercase', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ModelCtrl) {
            ModelCtrl.$parsers.push(function (input) {
                //console.log('input: ', input);
                return input ? input.toLowerCase() : '';
            });
            element.css('text-transform', 'lowercase');
        },
    };
});

app.directive('validateequal', function () {
    return {
        require: 'ngModel',
        link: function (scope, elem, attrs, ctrl) {
            var firstValue = '#' + attrs.validateequal;
            elem.add(firstValue).on('keyup', function () {
                scope.$apply(function () {
                    ctrl.$setValidity('match', elem.val() === $(firstValue).val());
                });
            });
        },
    };
});

app.directive('emailparser', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ModelCtrl) {
            ModelCtrl.$parsers.push(function (input) {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                var emails = input.split(',');
                var isValid = true;
                $('#findingShare_email').removeClass('has-error');

                _.each(emails, function (email) {
                    if (!re.test(_.trim(email))) {
                        isValid = false;
                        $('#findingShare_email').addClass('has-error');
                    }
                });
                ModelCtrl.$setValidity('validEmail', isValid);

                return input;
            });
        },
    };
});

app.directive('typeaheadShowOnFocus', function () {
    return {
        require: 'ngModel',
        link: function ($scope, element, attrs, ngModel) {
            element.bind('focus', function () {
                ngModel.$setViewValue();
                $(element).trigger('input');
                $(element).trigger('change');
            });
        },
    };
});

app.directive('focusMe', function ($timeout) {
    return {
        scope: { trigger: '@focusMe' },
        link: function (scope, element) {
            scope.$watch('trigger', function (value) {
                if (value === 'true') {
                    $timeout(function () {
                        element[0].focus();
                    });
                }
            });
        },
    };
});

app.filter('orderObjectBy', function () {
    return function (items, field, reverse) {
        var filtered = [];
        angular.forEach(items, function (item) {
            filtered.push(item);
        });
        filtered.sort(function (a, b) {
            return a[field] > b[field] ? 1 : -1;
        });
        if (reverse) filtered.reverse();
        return filtered;
    };
});

app.filter('findingDateRange', function (moment) {
    return function (findings, startDate, endDate) {
        //an undefined startDate is converted to the beginning of time
        startDate = startDate || 0;

        var inDateRange = [];
        angular.forEach(findings, function (finding) {
            if (moment(finding.added).isBetween(startDate, endDate) || moment(finding.updated).isBetween(startDate, endDate)) {
                inDateRange.push(finding);
            }
        });
        return inDateRange;
    };
});

app.filter('shownServices', function () {
    return function (services, hiddenServices) {
        if (!hiddenServices) return services;
        var serviceList = [];

        angular.forEach(services, function (service) {
            if (hiddenServices.indexOf(service.short_name) === -1) {
                serviceList.push(service);
            }
        });
        return serviceList;
    };
});

app.filter('noDupesForNotificationType', function () {
    return function (notification_types, event_type, notifications) {
        var filtered = [];

        if (!event_type) {
            return notification_types;
        }
        angular.forEach(notification_types, function (notification_type) {
            if (!_.find(notifications, { event_type: event_type, notification_type: notification_type.display_name })) {
                filtered.push(notification_type);
            }
        });

        return filtered;
    };
});

app.filter('noDupesForEventType', function () {
    return function (event_types, notification_type, notifications) {
        var filtered = [];

        if (!notification_type) {
            return event_types;
        }
        angular.forEach(event_types, function (event_type) {
            if (!_.find(notifications, { event_type: event_type.display_name, notification_type: notification_type })) {
                filtered.push(event_type);
            }
        });

        return filtered;
    };
});

app.filter('filterChildlessObject', function () {
    return function (items, showEmpty, paths) {
        if (showEmpty) {
            return items;
        } else {
            return _.filter(items, function (item) {
                for (var i = 0; i < paths.length; i++) {
                    var toCheck = _.get(item, paths[i]);
                    if (_.isNumber(toCheck)) {
                        return toCheck > 0; // Number greater than 0
                    }
                    if (_.isArray(toCheck)) {
                        return toCheck.length > 0; // Array with non-zero length
                    }
                }
                return false; // Don't include if we couldn't match anything that indicated it has children
            });
        }
    };
});

app.directive('attachmentList', function ($sce, $uibModal, FileSaver, modalService, Attachments) {
    function link(scope, element, attrs) {
        if (_.isNil(scope.downloadResource)) {
            scope.downloadResource = Attachments;
        }

        if (!_.isFunction(scope.deleteOnRemove)) {
            scope.deleteOnRemove = function () {
                return true;
            };
        }

        scope.downloadAttachment = function (attachment) {
            scope.downloadResource.download(
                { id: attachment.id },
                function (data) {
                    FileSaver.saveAs(data.blob, attachment.name);
                },
                modalService.openErrorResponseCB('Unable to download attachment')
            );
        };

        scope.viewAttachment = function (attachment) {
            scope.downloadResource.download(
                { id: attachment.id },
                function (data) {
                    $uibModal.open({
                        templateUrl: '/app/common/modalPreviewAttachment.html',
                        controller: function ($scope) {
                            $scope.filename = attachment.name;
                            $scope.isImage = data.blob.type.indexOf('image') === 0;
                            if ($scope.isImage) {
                                $scope.url = window.URL.createObjectURL(data.blob);
                            } else if (data.blob.type.indexOf('video') === 0 || data.blob.type.indexOf('audio') === 0) {
                                $scope.unsupported = true;
                            } else {
                                var reader = new FileReader();
                                reader.addEventListener('loadend', function () {
                                    $scope.data = reader.result;
                                });
                                reader.readAsText(data.blob);
                            }

                            $scope.$on('$destroy', function () {
                                window.URL.revokeObjectURL($scope.url);
                            });
                        },
                        size: 'xl',
                    });
                },
                modalService.openErrorResponseCB('Unable to preview attachment')
            );
        };

        scope.removeAttachment = function (attachment) {
            modalService.openConfirm('Confirm removal of file', 'remove', attachment.name, attachment, function () {
                if (scope.deleteOnRemove(attachment)) {
                    scope.downloadResource.delete(
                        { id: attachment.id },
                        function (response) {
                            _.remove(scope.attachments, { id: attachment.id });
                        },
                        modalService.openErrorResponseCB('Unable to delete attachment')
                    );
                } else {
                    _.remove(scope.attachments, { id: attachment.id });
                }
            });
        };
    }

    return {
        restrict: 'E',
        scope: {
            attachments: '=',
            showRemoveAttachment: '&?',
            downloadResource: '=?',
            deleteOnRemove: '=?',
        },
        templateUrl: '/app/common/attachmentList.html',
        link: link,
    };
});

app.directive('userIcon', function (globalDataService, MessageService, RandomColorService, sessionService) {
    function link(scope, element, attrs) {
        function updateIcon() {
            if (scope.user) {
                if (sessionService.hasRole('customer') && _.indexOf(scope.user.roles, 'mission_director') > -1) {
                    scope.initials = 'MD';
                    scope.name = 'Mission Director';
                    scope.colorCSS = RandomColorService.userIconColor('MD');
                } else {
                    globalDataService.users.$promise.then(function () {
                        var userId = scope.user.id || scope.user;
                        scope.initials = MessageService.getInitials(userId);
                        scope.name = MessageService.getNameOrRoleFromId(userId);
                        scope.colorCSS = RandomColorService.userIconColor(MessageService.getInitials(userId));
                    });
                }
            }
        }

        scope.$watch('user', updateIcon);
    }

    return {
        restrict: 'E',
        scope: {
            user: '<',
        },
        templateUrl: '/app/message/userIcon.html',
        link: link,
    };
});

// A 'simplified' regex for emails.
var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
app.constant('emailRegex', emailRegex);

function PhysicalAddressCtrl($scope, $rootScope, $element, $attrs, $state, modalService, sessionService, Locations) {
    var ctrl = this;
    if (ctrl.locations === undefined) ctrl.locations = [];

    ctrl.required = 'required' in $attrs;

    ctrl.countries = $rootScope.gdata.countries;
    ctrl.pocId = parseInt($state.params.poc_id);
    ctrl.addrId = parseInt($state.params.addr_id);
    ctrl.locId = parseInt($state.params.loc_id);

    ctrl.modal = false;
    ctrl.locationFormVisible = false;
    ctrl.zipCodeFormVisible = undefined;
    ctrl.modalClass = 'col-lg-3';

    if (ctrl.pocId) {
        // TODO: don't detect this from $state.params because it makes company profile location display behave like it is in the edit modal if the page is reloaded while the modal is open to edit a location
        ctrl.modal = true;
        ctrl.modalClass = 'col-lg-12';
    }

    var user = sessionService.getSession();
    var customerId = user.customer;

    ctrl.close = function () {
        ctrl.onClose();
    };

    // For POCs
    ctrl.setAddress = function (location) {
        location.type = 'physical';
        console.log('setPocAddress: ', location);
        ctrl.setPocAddress({ location: location, replace: true });
    };

    ctrl.addAddress = function (location) {
        location.type = 'physical';
        console.log('addPocAddress: ', location);
        ctrl.setPocAddress({ location: location });
    };

    // For Incident Reporting
    ctrl.addIncidentAddress = function (location) {
        ctrl.addIncidentLocation({ location: location });
    };

    ctrl.addLocation = function () {
        if (ctrl.locations.length > 0) {
            if (ctrl.locations[0].id === undefined) {
                return;
            }
        }
        ctrl.locations.unshift({ personalLocation: ctrl.modal });
    };

    ctrl.locNotAlreadyUsed = function (locId) {
        return !_.find(ctrl.alreadyUsedLocations, { location: locId });
    };

    ctrl.saveLocation = function (location) {
        location['personalLocation'] = ctrl.modal || ctrl.incidentReporting;

        if (location.id === undefined) {
            // TODO: Good for now, but want more logic at some point if a location is removed, etc. rather than first location defined
            if (ctrl.locations.length == 1) {
                location.primaryLocation = true;
            }
            Locations.create(
                { customerId: customerId },
                location,
                function (locationResult) {
                    ctrl.locations[0] = locationResult;
                    ctrl.setAddress(locationResult);
                    ctrl.close();
                },
                modalService.openErrorResponseCB('Unable to create new location information')
            );
        } else {
            Locations.update(
                { customerId: customerId, locationId: location.id },
                location,
                function (locationResult) {
                    // TODO: use callbacks instead of modifying the array passed to the component
                    for (var i = 0; i < ctrl.locations.length; i++) {
                        if (ctrl.locations[i].id === location.id) {
                            ctrl.locations[i] = locationResult;
                            break;
                        }
                    }
                    ctrl.close();
                },
                modalService.openErrorResponseCB('Unable to update location information')
            );
        }
    };

    ctrl.cancelCreation = function () {
        ctrl.locations.shift();
        ctrl.locationFormVisible = false;
        console.log('canceled creation');
    };

    ctrl.deleteLocation = function (location) {
        console.log('called deleteLocation: ', location);
        ctrl.delLocation({ location: location });
    };

    ctrl.setPrimaryLocation = function (locId) {
        ctrl.primaryLocation = !ctrl.primaryLocation;
        var idx = _.findIndex(ctrl.locations, { id: locId });
        if (idx !== -1) {
            ctrl.locations[idx].primaryLocation = !ctrl.locations[idx].primaryLocation;
        }
    };

    ctrl.onCountrySelect = function (country, location) {
        ctrl.zipCodeFormVisible = country === 'United States';
        if (!ctrl.zipCodeFormVisible) {
            delete location.zipCode;
        }
    };

    ctrl.filterLocation = function (location) {
        if (ctrl.alreadyUsedLocations === undefined) return true;
        return location.id === ctrl.locId || !_.find(ctrl.alreadyUsedLocations, { id: location.id });
    };
}

app.component('physicalAddress', {
    templateUrl: '/app/settings/physicalAddress.html',
    controller: PhysicalAddressCtrl,
    bindings: {
        onClose: '&',
        setPocAddress: '&',
        locations: '<',
        addIncidentLocation: '&',
        alreadyUsedLocations: '<',
        delLocation: '&',
        incidentReporting: '<',
    },
});

app.service('requiredAddressesService', function () {
    var requiredList = [
        { name: 'physical', label: 'Physical address' },
        { name: 'phonenumber', label: 'Phone number' },
        { name: 'email', label: 'Email address' },
    ];

    function getRequired(addresses) {
        var required = _.clone(requiredList);
        angular.forEach(addresses, function (address) {
            _.remove(required, { name: address.type });
        });
        return required;
    }

    return {
        getRequired: getRequired,
    };
});

function RequiredAddressListCtrl(requiredAddressesService) {
    var ctrl = this;

    ctrl.$onInit = function () {
        // Bindings are now available
        ctrl.required = requiredAddressesService.getRequired(ctrl.addresses);
        ctrl.originalAddresses = _.clone(ctrl.addresses);
    };

    ctrl.$doCheck = function () {
        // Do an obj comparison, and only if they differ, should we update/run RequiredAddressesCtrl.getRequired()
        if (!angular.equals(ctrl.addresses, ctrl.originalAddresses)) {
            ctrl.required = requiredAddressesService.getRequired(ctrl.addresses);
            ctrl.originalAddresses = _.clone(ctrl.addresses);
        }
    };
}

app.component('requiredAddresses', {
    templateUrl: '/app/settings/requiredAddresses.html',
    controller: RequiredAddressListCtrl,
    bindings: {
        addresses: '<',
    },
});

app.filter('propsFilter', function () {
    return function (items, props) {
        var out = [];

        if (angular.isArray(items)) {
            var keys = Object.keys(props);

            items.forEach(function (item) {
                var itemMatches = false;

                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop] !== undefined && item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});

app.component('targetIcon', {
    bindings: {
        type: '<',
    },
    controller: function (TargetService) {
        this.getTargetTypeIcon = TargetService.getTargetTypeIcon;
    },
    template: '<i class="fa" ng-class="$ctrl.getTargetTypeIcon($ctrl.type)" aria-hidden="true" uib-tooltip="{{$ctrl.type}}"></i>',
});

app.component('tagViewer', {
    bindings: {
        tags: '<',
        type: '<?',
    },
    controller: function (globalDataService) {
        var ctrl = this;
        ctrl.gdata = globalDataService;

        ctrl.$onInit = function () {
            // Bindings are now available
            ctrl.type = ctrl.type || 'targetTag';
        };
    },
    template:
        '<span ng-repeat="tag in $ctrl.tags" class="tag force-wrap" ng-class="{\'tag-new\': $ctrl.gdata[$ctrl.type+\'Map\'][tag]===undefined}" ng-bind="$ctrl.gdata[$ctrl.type+\'Map\'][tag].value || tag"></span>',
});

app.component('dualPaneMultiSelect', {
    require: {
        ngModelCtrl: '?ngModel',
    },
    bindings: {
        ngModel: '<',
        availableObjs: '<',
        availableMap: '<?',
        availableLabelTooltipHtml: '<?',
        objsName: '@',
        labelAttribute: '@',
        noAvailableExtraText: '@?',
        trackBy: '@?',
        orderBy: '<?',
        groupBy: '&?',
        disableOptionWhen: '&?',
        availableFilter: '<?',
        includedFilter: '<?',
    },
    controller: function () {
        var ctrl = this;
        ctrl.selectedIncluded = [];
        ctrl.selectedAvailable = [];
        var includedMap = {};
        var availableMap = {};

        ctrl.$onInit = function () {
            //noinspection JSPrimitiveTypeWrapperUsage
            ctrl.ngModelCtrl.$render = function () {
                if (ctrl.availableObjs && ctrl.availableObjs.$promise && ctrl.availableObjs.$resolved === false) {
                    // ctrl.availableObjs not loaded yet, listen on $promise to re-render
                    ctrl.availableObjs.$promise.then(ctrl.ngModelCtrl.$render);
                    ctrl.includedObjs = [];
                    return;
                }
                if (_.isEmpty(ctrl.availableMap)) {
                    availableMap = _.keyBy(ctrl.availableObjs, ctrl.trackBy);
                }
                if (ctrl.ngModelCtrl.$viewValue) {
                    ctrl.includedObjs = _.at(getAvailableMap(), ctrl.ngModelCtrl.$viewValue);
                    populateIncludedMap();

                    if (_.some(ctrl.includedObjs, _.isUndefined)) {
                        if (!ctrl.includedObjs.every(_.isUndefined)) {
                            console.debug(
                                'Not all included objects could be found in the available data. ' +
                                    'Previously included objects that could not be found will be removed. ' +
                                    'model:%o, availableObjs:%o, availableMap:%o, found included:%o',
                                _.clone(ctrl.ngModelCtrl.$viewValue),
                                _.clone(ctrl.availableObjs),
                                _.clone(ctrl.availableMap),
                                _.clone(ctrl.includedObjs)
                            );
                        }
                        if ((ctrl.availableObjs && ctrl.availableObjs.$resolved !== false) || ctrl.availableMap) {
                            _.pull(ctrl.includedObjs, undefined);
                            ctrl.ngModelCtrl.$setViewValue(_.map(ctrl.includedObjs, ctrl.trackBy));
                        }
                    }
                } else {
                    ctrl.includedObjs = [];
                    includedMap = {};
                }
            };

            ctrl.orderBy = ctrl.orderBy || ctrl.labelAttribute;
            ctrl.disableOptionWhen = ctrl.disableOptionWhen || angular.noop;
            ctrl.groupBy = ctrl.groupBy || angular.noop;
            ctrl.trackBy = ctrl.trackBy || 'id';
            if (ctrl.noAvailableExtraText) {
                ctrl.noAvailableExtraTextPadded = ' ' + ctrl.noAvailableExtraText;
            }

            availableMap = _.keyBy(ctrl.availableObjs, ctrl.trackBy);
        };

        ctrl.$onChanges = function (changes) {
            if (changes.availableObjs || changes.availableMap) {
                // Available objects changed, need to re-render
                ctrl.ngModelCtrl.$render();
            }
        };

        ctrl.filterAlreadySelected = function (item) {
            return !includedMap[item[ctrl.trackBy]];
        };
        ctrl.addSelected = function () {
            ctrl.includedObjs = ctrl.includedObjs.concat(ctrl.selectedAvailable);
            ctrl.ngModelCtrl.$setViewValue(_.map(ctrl.includedObjs, ctrl.trackBy));
            populateIncludedMap();
        };
        ctrl.removeSelected = function () {
            var selectedIncludedMap = _.keyBy(ctrl.selectedIncluded, ctrl.trackBy);
            _.remove(ctrl.includedObjs, function (o) {
                if (selectedIncludedMap[o[ctrl.trackBy]]) {
                    if (!ctrl.disableOptionWhen({ obj: o, available: true })) {
                        ctrl.selectedAvailable.push(o);
                    }
                    return true;
                }
            });
            ctrl.ngModelCtrl.$setViewValue(_.map(ctrl.includedObjs, ctrl.trackBy));
            populateIncludedMap();
        };
        ctrl.addAll = function () {
            var toAdd = _.filter(ctrl.availableObjs, function (obj) {
                // objects that are not already included and are not disabled
                return ctrl.filterAlreadySelected(obj) && !ctrl.disableOptionWhen({ obj: obj, available: true });
            });
            ctrl.includedObjs = ctrl.includedObjs.concat(toAdd);
            ctrl.ngModelCtrl.$setViewValue(_.map(ctrl.includedObjs, ctrl.trackBy));
            populateIncludedMap();
        };
        ctrl.removeAll = function () {
            ctrl.includedObjs = _.filter(ctrl.includedObjs, function (obj) {
                return ctrl.disableOptionWhen({ obj: obj, included: true });
            });
            ctrl.ngModelCtrl.$setViewValue(_.map(ctrl.includedObjs, ctrl.trackBy));
            populateIncludedMap();
        };

        function getAvailableMap() {
            return ctrl.availableMap || availableMap;
        }

        function populateIncludedMap() {
            includedMap = _.pick(getAvailableMap(), ctrl.ngModelCtrl.$viewValue);
        }
    },
    templateUrl: '/app/common/dualPaneMultiSelect.html',
});

const ipSegment = '(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])';
const cidrPrefix = '(3[0-2]|[0-2]?[0-9])'; // 0-32
app.constant('formatFactory', {
    tel: {
        pattern: /^\+?1?([- ()]*\d){9,15}$/,
        patternError: 'This field must be formatted as a phone number.<br/>A sample valid input looks like (123)-456-7890',
        replace: /[-.()\sa-zA-Z]/g,
    },
    email: {
        pattern: emailRegex,
        patternError: 'This field must be formatted as an email address.<br/>A sample valid input looks like john@doe.com',
    },
    cagecode: {
        pattern: /^[a-zA-Z0-9]{5}$/,
        patternError: 'A valid CAGE Code consists of 5 alphanumeric characters',
    },
    ipAddress: {
        pattern: new RegExp(`^${ipSegment}\.${ipSegment}\.${ipSegment}\.${ipSegment}$`),
        patternError: 'This field must be formatted as an IP address',
    },
    cidrBlock: {
        pattern: new RegExp(`^${ipSegment}\.${ipSegment}\.${ipSegment}\.${ipSegment}\/${cidrPrefix}$`),
        patternError: 'This field must be formatted as a CIDR block',
    },
});

app.component('advancedPagination', {
    bindings: {
        onPageChange: '&',
        onPageSizeChange: '&',
        itemsPerPage: '<',
        totalItems: '<',
        currentPage: '=',
    },
    controller: function (modalService) {
        var ctrl = this;

        ctrl.pageSizes = [10, 30, 50, 100, 200, 500];

        ctrl.jump = function (newPage) {
            var newPageInteger = parseInt(newPage);
            var numPages = Math.ceil(ctrl.totalItems / ctrl.itemsPerPage);
            if (newPageInteger < 1 || newPageInteger > numPages) {
                modalService.openErrorResponse(
                    'Failed to jump to requested page',
                    'Page number must be between the page range of 1 and ' + numPages,
                    false
                );
            } else {
                ctrl.currentPage = newPage;
                ctrl.load();
            }
        };

        ctrl.load = function () {
            ctrl.onPageChange({ currentPage: ctrl.currentPage });
        };
    },
    templateUrl: '/app/common/advPagination/advancedPagination.html',
});

function CustomerFilterCtrl($attrs, $rootScope, globalDataService, RulesOfEngagements, FileSaver) {
    var ctrl = this;

    var unregisterWatch;
    var customerCleared = true;
    ctrl.gdata = globalDataService;
    ctrl.customerMap = globalDataService.customerMap;
    ctrl.customers = globalDataService.customers;
    ctrl.globalUser = _.get($rootScope.user, 'ui_data.selectedCustomer');
    ctrl.catotheme = $rootScope.catotheme;
    ctrl.aAn = $rootScope.aAn;
    ctrl.showSelectCustomer = false;
    ctrl.required = 'required' in $attrs;
    ctrl.noMonitor = 'noMonitorGlobal' in $attrs;
    ctrl.form = {};

    ctrl.$onInit = function () {
        // Bindings are now available
        // Set the default for the form (whoever we're managing)
        ctrl.form.customer = ctrl.initialCustomer || ctrl.globalUser;
        globalDataService.customers.$promise.then(function () {
            if (ctrl.form.customer) {
                ctrl.form.customer = globalDataService.customerMap[ctrl.form.customer.id || ctrl.form.customer];
            }
            customerCleared = !ctrl.form.customer;
        });
        if (!ctrl.noMonitor) {
            unregisterWatch = $rootScope.$watch('user.ui_data.selectedCustomer', function (newVal, oldVal) {
                if (newVal !== oldVal && !ctrl.catoNgDisabled) {
                    ctrl.form.customer = newVal;
                    ctrl.globalUser = newVal;
                    customerCleared = !ctrl.form.customer;
                }
            });
        }
    };

    ctrl.$doCheck = function () {
        if ('required' in $attrs) {
            ctrl.required = $attrs.required || _.isString($attrs.required);
        }
    };

    ctrl.$onChanges = function (changesObj) {
        if (changesObj.initialCustomer && changesObj.initialCustomer.currentValue) {
            // If initialCustomer changed and is currently truthy
            var customerId = changesObj.initialCustomer.currentValue.id || changesObj.initialCustomer.currentValue;
            if (!ctrl.form.customer || customerId !== ctrl.form.customer.id) {
                // If customer is not set on form or customer id on form does not match the id of the newly changed customer in the attribute
                globalDataService.customers.$promise.then(function () {
                    ctrl.form.customer = globalDataService.customerMap[customerId];
                    customerCleared = !ctrl.form.customer;
                });
            }
        }
    };

    ctrl.$onDestroy = function () {
        if (unregisterWatch) {
            unregisterWatch();
        }
    };

    ctrl.set = function (item) {
        ctrl.showSelectCustomer = false;
        customerCleared = false;
        ctrl.setCustomer({ customer: item });
        ctrl.form.customer = item;
    };

    ctrl.clear = function () {
        if (ctrl.catoNgDisabled) {
            return;
        }
        delete ctrl.form.customer;
        ctrl.showSelectCustomer = true;
        customerCleared = true;
        (ctrl.clearCustomer || ctrl.setCustomer)({ customer: undefined });
    };

    ctrl.checkBlur = function () {
        if (!ctrl.form.customer && !customerCleared) {
            // if customer isn't set and clear hasn't been called (or some other external change)
            ctrl.clear();
        }
    };

    ctrl.downloadRoE = function () {
        var response = RulesOfEngagements.downloadSigned({
            customerId: ctrl.form.customer.id,
            roeId: ctrl.customerMap[ctrl.form.customer.id].current_rules_of_engagement,
        }).$promise;
        response.then(function (data) {
            FileSaver.saveAs(data.blob, 'Rules of Engagement');
        });
    };
}

app.component('customerFilter', {
    controller: CustomerFilterCtrl,
    templateUrl: '/app/customer/manage/customerFilterForm.html',
    bindings: {
        setCustomer: '&',
        clearCustomer: '&?',
        placeholder: '<',
        initialCustomer: '<?',
        catoNgDisabled: '<?',
    },
});

app.component('manageCustomer', {
    controller: CustomerFilterCtrl,
    templateUrl: '/app/customer/manage/manageCustomerForm.html',
    bindings: {
        setCustomer: '&',
        clearCustomer: '&?',
        placeholder: '<',
    },
});

app.service('ngTableService', function ($filter, $q, NgTableParams) {
    // Example: $scope.targetTable = ngTableService.clientFilteredTable({resource: Targets.list, defaultParams: defaultParams, scope: $scope.data, scopeField: 'targets',});
    function clientFilteredTable(parameters) {
        // TODO: this isn't fully implemented but is kept to be used as a starting point in the future
        var resource = parameters.resource;
        var defaultParams = parameters.defaultParams;
        var scope = parameters.scope;
        var scopeField = parameters.scopeField;
        var initialParams = {
            // TODO: up this, this is just for testing/verification
            count: 50, // initial page size
        };
        var initialSettings = {
            // page size buttons (right set of buttons in demo)
            counts: [],
            // determines the pager buttons (left set of buttons in demo)
            paginationMaxBlocks: 5,
            paginationMinBlocks: 2,
            getData: function (params) {
                var dataDefer = $q.defer();
                scope[scopeField] = resource(defaultParams, function (data) {
                    var filteredData = $filter('filter')(data, _.pickBy(params.filter(), _.identity));
                    var filteredAndSortedData = $filter('orderBy')(filteredData, params.sorting());
                    data = filteredAndSortedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                    params.total(data.length);
                    dataDefer.resolve(data);
                });
                return dataDefer.promise;
            },
        };
        return new NgTableParams(initialParams, initialSettings);
    }

    function serverFilteredTable(parameters) {
        var resource = parameters.resource;
        var defaultParams = parameters.defaultParams;
        var extraInitialParams = parameters.extraInitialParams;
        var extraInitialSettings = parameters.extraInitialSettings;
        const extraFilter = parameters.extraFilter;
        var updateParamsCallback = parameters.updateParamsCallback;
        var loadingCallback = parameters.loadingCallback;
        var loadedCallback = parameters.loadedCallback;
        let ignoreSecondLoadIfDuplicate = parameters.ignoreSecondLoadIfDuplicate;
        let firstMergedParams;

        var initialParams = {
            count: 25, // initial page size
        };
        angular.extend(initialParams, extraInitialParams);
        var initialSettings = {
            // page size buttons
            counts: [10, 25, 50, 100, 500, 1000],
            // determines the pager buttons (blocks in center around current):  round((max-min)/2)
            paginationMaxBlocks: 7,
            paginationMinBlocks: 0,
            getData: function (params) {
                let res: any;
                var mergedParams = angular.extend({}, defaultParams, params.filter(true));
                mergedParams.page_size = params.count();
                mergedParams.page = params.page();
                mergedParams.ordering = _.map(params.orderBy(), function (v: string) {
                    if (v[0] === '-' && v.indexOf(',') > -1) {
                        v = v.replace(/,/g, ',-');
                    }
                    return _.trimStart(v, '+');
                }).join(',');
                (updateParamsCallback || angular.noop)(mergedParams);

                if (ignoreSecondLoadIfDuplicate) {
                    if (!firstMergedParams) {
                        firstMergedParams = mergedParams;
                    } else {
                        ignoreSecondLoadIfDuplicate = false; // if duplicate, we will ignore below; if not, don't bother checking again
                        if (angular.equals(firstMergedParams, mergedParams)) {
                            firstMergedParams = undefined;
                            return $q.resolve(params.data);
                        } else {
                            firstMergedParams = undefined;
                        }
                    }
                }

                if (extraFilter) {
                    res = resource(extraFilter, mergedParams);
                } else {
                    res = resource(mergedParams);
                }
                params._catoPromise = res.$promise;
                (loadingCallback || angular.noop)(res);
                return res.$promise.then(function (data) {
                    params._catoIsFiltered = params.hasFilter();
                    params.total(data.count);
                    params.data = data.results;
                    (loadedCallback || angular.noop)(data.results, data.count);
                    return data.results;
                });
            },
        };
        angular.extend(initialSettings, extraInitialSettings);
        return new NgTableParams(initialParams, initialSettings);
    }

    return {
        clientFilteredTable: clientFilteredTable,
        serverFilteredTable: serverFilteredTable,
    };
});

// TODO: convert to a component once template HTML is updated
app.directive('messageCompose', function () {
    return {
        restrict: 'E',
        scope: {
            compose: '<?',
            for: '<?',
            type: '@',
            onMessageSent: '&?',
        },
        controller: function ($scope, modalService, Messages, MatDialog: MatDialog, AttachmentService: AttachmentService) {
            // TODO: implement logic for full compose and not just reply

            $scope.messageFormData = {};

            $scope.uploadAttachment = function () {
                MatDialog.open(AttachmentComponent)
                    .afterClosed()
                    .subscribe((queuedAttachments: File[]) => {
                        ($scope.messageFormData.queuedAttachments = $scope.messageFormData.queuedAttachments || []).push(...queuedAttachments);
                    });
            };

            $scope.submitNewMessage = function () {
                var msg = _.clone($scope.messageFormData);
                msg[$scope.type] = $scope.for;

                Messages.addMessage(msg)
                    .$promise.then((msgResp) => {
                        if ($scope.messageFormData.queuedAttachments) {
                            for (const attachment of $scope.messageFormData.queuedAttachments) {
                                AttachmentService.uploadAttachment(attachment, msgResp.id)
                                    .toPromise()
                                    .then((attachmentRes) => {
                                        console.log(attachmentRes);
                                    });
                            }
                        }
                        $scope.messageFormData = {};
                        if ($scope.onMessageSent) {
                            $scope.onMessageSent({ message: msg });
                        }
                    })
                    .catch(modalService.openErrorResponseCB('Sending message failed'));
            };
        },
        templateUrl: '/app/message/msgCompose.html',
    };
});

app.component('dataviewer', {
    bindings: {
        value: '<',
        name: '<',
    },
    controller: function ($filter, FileSaver) {
        var ctrl = this;
        ctrl.isJSON = false;
        ctrl.maybeXML = false;
        ctrl.plain = true;
        ctrl.options = {
            mode: 'view',
            modes: ['view', 'code', 'text'],
        };

        // lifecycle hooks
        ctrl.$onInit = function () {
            // Bindings are now available
            loadData();
        };

        ctrl.$onChanges = function (changesObj) {
            if (changesObj.value && changesObj.value.currentValue) {
                loadData();
            }
            if (changesObj.name && changesObj.name.currentValue) {
                ctrl.name = changesObj.name.currentValue;
            }
        };

        // action functions
        ctrl.format = function () {
            if (ctrl.maybeXML) {
                ctrl.text = $filter('prettyXml')(ctrl.value);
            } else if (ctrl.isJSON) {
                ctrl.text = angular.toJson(ctrl.parsed, true);
            }
        };

        ctrl.reset = loadData;

        ctrl.download = function () {
            var type = 'text/plain';
            var ext = '.txt';
            if (ctrl.isJSON) {
                type = 'application/json';
                ext = '.json';
            } else if (ctrl.maybeXML) {
                type = 'text/xml';
                ext = '.xml';
            }
            if (!_.includes(ctrl.name, '.')) {
                ctrl.name += ext;
            }
            var data = new Blob([ctrl.text], { type: type + ';charset=utf-8' });
            FileSaver.saveAs(data, ctrl.name);
        };

        // utility functions
        function loadData() {
            ctrl.isJSON = false;
            ctrl.maybeXML = false;
            ctrl.plain = true;
            if (_.isString(ctrl.value)) {
                try {
                    ctrl.parsed = angular.fromJson(ctrl.value);
                    ctrl.text = ctrl.value;
                    ctrl.isJSON = true;
                    ctrl.plain = false;
                    ctrl.language = 'JSON';
                } catch (e) {
                    ctrl.text = ctrl.value;
                    var gtidx = ctrl.text.indexOf('<');
                    var ltidx = ctrl.text.indexOf('>');
                    if (gtidx !== -1 && ltidx !== -1 && ltidx + 1 > gtidx) {
                        // data contains '<' followed by '>' and separated by at least one character
                        ctrl.maybeXML = true;
                        ctrl.language = 'XML';
                    }
                }
            } else if (_.isObjectLike(ctrl.value)) {
                ctrl.parsed = ctrl.value;
                ctrl.text = angular.toJson(ctrl.value, true);
                ctrl.isJSON = true;
                ctrl.plain = false;
                ctrl.language = 'JSON';
            } else {
                ctrl.text = ctrl.value;
            }
        }
    },
    templateUrl: '/app/common/dataviewer.html',
});
