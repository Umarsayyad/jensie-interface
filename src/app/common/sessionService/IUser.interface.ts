//JAC TODO: Is this the correct place to put this interface?
export interface IUser {
    type: String;
    updated: String;
    level: Number;
    customer: any;
    email: String;
    assigned_customers: any[];
    username: String;
    schedules: Number[];
    request_pending: Boolean;
    notification_preferences: Number[];
    access_groups: any[];
    // password: any,
    first_name: String;
    last_name: String;
    comments: String;
    ui_data: any;
    id: Number;
    roles: String[];
    date_joined: String;
    pocs: String[];
    last_login: String;
    is_active: Boolean;
    title: String;
}
