import app from '../../app';
import { config } from '@cato/config';
import { CATOSignalEvent } from '../helperClasses/CATOSignalEvent';
import { IUser } from './IUser.interface';
import * as _ from 'lodash';

export interface INewSessionService {
    //JAC TODO: PLEASE come back and replace these 'any's with proper types
    //ALSO, is this the best place for this interface defintion?
    gdata: any;
    AnalysisModule: any;
    modules: any[];
    sessionData: any;
    user: IUser;
    socketService: any;
    $state: any;
    ROE_STATE: string;
    loggedIn: boolean;
    $http: ng.IHttpService;
    endSession: Function;
    filterRoles: Function;
    getKey: Function;
    getLastActive: Function;
    getOptions: Function;
    getRoles: Function;
    getSession: Function;
    getUsername: Function;
    hasAnyRole: Function;
    hasFilteredRoles: Function;
    hasRole: Function;
    loadSession: Function;
    setKey: Function;
    setOptions: Function;
    setSessionData: Function;
    updateActivity: Function;
    updateObjOld: Function;
    $injector: ng.auto.IInjectorService;
}

export class NewSessionService implements INewSessionService {
    public gdata: any;
    private activityInterval: number;
    public AnalysisModule: any;
    public modules: any = [];
    public sessionData: any = {};
    public user: IUser;
    public $http: ng.IHttpService;
    public $state: any;
    public ROE_STATE: string;
    public socketService: any;
    public loggedIn: boolean = false;

    public signalEvent$: CATOSignalEvent<string>;

    $injector: ng.auto.IInjectorService;

    // injection :
    static $inject = ['$http', '$injector', '$state', 'ROE_STATE', 'socketService', 'DashboardWidgets'];

    constructor($http, $injector, $state, ROE_STATE, socketService, private DashboardWidgets) {
        this.signalEvent$ = new CATOSignalEvent<string>();
        this.$http = $http;
        this.$injector = $injector;
        this.$state = $state;
        this.ROE_STATE = ROE_STATE;
        this.socketService = socketService;
    }

    endSession() {
        clearInterval(this.activityInterval);

        this.loggedIn = false;
        this.sessionData = {};
        this.$state.go('cato.logout');
    }

    filterRoles(collectionToIterateOver, roles) {
        return _.filter(roles ? roles : this.sessionData.roles, collectionToIterateOver);
    }

    updateActivity() {
        this.sessionData.lastActive = new Date().valueOf();
        localStorage.setItem('lastActive', this.sessionData.lastActive);
    }

    getLastActive() {
        return this.sessionData.lastActive;
    }

    getSession() {
        return this.sessionData;
    }
    getUsername() {
        return this.sessionData.username;
    }
    getRoles() {
        return this.sessionData.roles;
    }

    hasFilteredRoles(collectionToInspect, roles) {
        return _.size(this.filterRoles(collectionToInspect, roles)) > 0;
    }
    hasRole(role: string) {
        return _.includes(this.sessionData.roles, role.toLowerCase());
    }
    hasAnyRole(roles: string[]) {
        return (
            _.size(
                _.intersection(
                    this.sessionData.roles,
                    roles.map((role) => role.toLowerCase())
                )
            ) > 0
        );
    }
    getOptions() {
        return this.sessionData.ui_data;
    }
    setOptions(options) {
        this.sessionData.ui_data = options;
        //TODO: update DB
    }
    setKey(key, value) {
        this.sessionData[key] = value;
    }
    getKey(key) {
        return this.sessionData[key];
    }

    loadSession(user: IUser) {
        if (!sessionStorage.getItem('token') && !(this.$state.is('cato.login') || this.$state.is('cato.logout'))) {
            // This will only ever happen on dev systems, where the API accepts session cookies for auth
            location.pathname = '/logout';
        }
        this.setSessionData(user);
        if (this.user && this.user.id === user.id) {
            this.updateObjOld(this.user, user);
        } else {
            this.user = user;
        }

        let globalDataService: any = this.$injector.get('globalDataService');
        this.gdata = globalDataService.load();

        if (user.type === 'customer') {
            this.gdata.customer.$promise.then(() => {
                if (!this.gdata.customer.current_rules_of_engagement && !this.$state.transition && !this.$state.$current.includes[this.ROE_STATE]) {
                    // If there isn't a transition active, such as when an account manager changes customers, we need to check for the customer's RoE and redirect if needed
                    this.$state.go(this.ROE_STATE, {}, { location: 'replace' });
                }
            });
        }

        this.loggedIn = true;

        this.socketService.connect();

        // On the API side knox only saves 'token[:CONSTANTS.TOKEN_KEY_LENGTH]', which defaults to 8.
        this.socketService.subscribeByGroup('u-' + user.id + '-session-watch-' + sessionStorage.getItem('token').substring(0, 8), () => {
            this.$http({
                url: config.apiPath + 'user/me',
                method: 'GET',
            });
        });

        this.DashboardWidgets.init(this.user);

        // No need to unsubscribe; on logout, the UI disconnects the websocket, destroying all subscriptions
        return this.gdata.promise;
    }

    setSessionData(session: any) {
        this.sessionData = session;
        this.sessionData.username = this.sessionData.username;
        this.signalEvent$.emit('user-session-loaded');
    }

    updateObjOld(object, other) {
        if (Array.isArray(object) && !Array.isArray(other)) {
            // Merging an object into an array is almost certainly incorrect usage so throw an exception to inform the developer of the bug.
            var msg = 'You are trying to merge an object into an array.  Did you mean to merge into an object contained in the array?';
            console.error(msg, 'array:', object, 'object:', other);
            console.trace();
            throw msg;
        }

        function customizer(objValue, srcValue, key) {
            // object/objValue, destination object getting merged into
            // source/srcValue, object being merged into destination
            if ((objValue === null && !Number.isNaN(srcValue)) || (!Number.isNaN(objValue) && srcValue === null)) {
                // Allow null to overwrite and be overwritten by numbers
                return undefined; // Default action
            }
            if (Number.isNaN(objValue) && !Number.isNaN(srcValue)) {
                // Numbers don't overwrite non-numbers
                return objValue;
            }
            if (typeof key === 'string' && key[0] === '$') {
                // Don't overwrite properties that start with $
                return objValue;
            }
            if (
                Array.isArray(objValue) &&
                Array.isArray(srcValue) &&
                objValue.length > 0 &&
                Number.isNaN(objValue[0]) &&
                !Number.isNaN(srcValue[0])
            ) {
                return objValue;
            }
            return undefined;
        }
        return _.mergeWith(object, other, customizer);
    }
}

app.service('NewSessionService', NewSessionService);
