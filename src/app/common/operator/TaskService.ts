/*global define,_,angular */
import app from '../../app';

app.service('TaskService', function () {
    var TaskService = this;

    // TODO: keep this in sync with models.py or populate dynamically from the API
    this.STATE_UNSTARTED = 'unstarted';
    this.STATE_SCHEDULED = 'scheduled';
    this.STATE_STORED = 'stored';
    this.STATE_RETRIEVED = 'retrieved';
    this.STATE_PENDING = 'pending';
    this.STATE_STARTED = 'started';
    this.STATE_AWAITING = 'awaiting';
    this.STATE_ANALYSIS_PENDING = 'analysis pending';
    this.STATE_ANALYZING = 'analyzing';
    this.STATE_ABORTING = 'aborting';
    this.STATE_ABORTED = 'aborted';
    this.STATE_FINISHED = 'finished';
    this.STATE_FAILED = 'failed';

    /*
def is_active(self):
    """
    Any state other than unstarted, aborted, failed, and finished is considered active
    """
    # This currently considers scheduled to be active, but we may wish to change that later
    return self.status not in [Task.STATE_UNSTARTED, Task.STATE_ABORTED, Task.STATE_FAILED, Task.STATE_FINISHED]

def is_started(self):
    """
    Anything after unstarted is considered started
    """
    # This currently considers scheduled to be started, but we may wish to change that later
    return self.status not in [Task.STATE_UNSTARTED]

def is_completed(self):
    """
    The only completed states are aborted, finished, and failed
    """
    return self.status in [Task.STATE_ABORTED, Task.STATE_FINISHED, Task.STATE_FAILED]

     */
    this.taskActive = function (task) {
        return (
            task &&
            !(
                task.status === TaskService.STATE_UNSTARTED ||
                task.status === TaskService.STATE_ABORTED ||
                task.status === TaskService.STATE_FINISHED ||
                task.status === TaskService.STATE_FAILED
            )
        );
    };

    this.taskStarted = function (task) {
        return task && task.status !== TaskService.STATE_UNSTARTED;
    };

    this.taskCompleted = function (task) {
        return (
            task &&
            (task.status === TaskService.STATE_ABORTED || task.status === TaskService.STATE_FINISHED || task.status === TaskService.STATE_FAILED)
        );
    };

    this.taskDirty = function (task) {
        return (
            task &&
            (task.status === TaskService.STATE_ABORTED || task.status === TaskService.STATE_ABORTING || task.status === TaskService.STATE_FAILED)
        );
    };
});
