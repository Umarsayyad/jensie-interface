import app from '../../app';
import { config } from '@cato/config';
import * as angular from 'angular';
import * as _ from 'lodash';

import 'ace-builds/src-noconflict/mode-html';

app.directive('filesInput', function () {
    return {
        require: 'ngModel',
        link: function postLink(scope, elem: any, attrs, ngModel) {
            elem.on('change', function (e) {
                var files = [];
                Array.prototype.push.apply(files, elem[0].files);
                ngModel.$setViewValue(files);
            });
        },
    };
});

app.directive('taskParameters', function ($sce, Upload, modalService, ParameterFiles) {
    function link(scope, element, attrs) {
        scope.ParameterFiles = ParameterFiles;
        scope.deleteOnRemove = function (file) {
            return file.justUploaded;
        };

        function cleanParameters() {
            _.each(scope.parameters, function (parameter) {
                parameter.type = parameter.type.toUpperCase();
                if (_.isString(parameter.description)) {
                    parameter.description = $sce.trustAsHtml(parameter.description);
                }
                if (_.includes(['SELECT', 'MULTISELECT', 'TYPEAHEAD'], parameter.type)) {
                    var options = [];
                    _.each(parameter.options, function (option) {
                        if (!_.isUndefined(option.label) && !_.isUndefined(option.value)) {
                            options.push(option);
                        } else {
                            options.push({
                                value: option,
                                label: option,
                            });
                        }
                    });
                    parameter.options = options;
                }
            });
        }

        cleanParameters();
        scope.$watch('parameters', cleanParameters);

        scope.clearSelected = function (param) {
            param.toUpload = null;
            angular.element("input[name='input-" + param.name + "']").val(null);
        };

        scope.uploadFiles = function (taskT, param) {
            if (!param.multiple && param.value.length) {
                modalService.openConfirm('Confirm removal of file', 'replace', param.value[0].name, null, function () {
                    if (param.value[0].justUploaded) {
                        ParameterFiles.delete(
                            { id: param.value[0].id },
                            function () {
                                param.value = [];
                                doUpload(taskT, param);
                            },
                            modalService.openErrorResponseCB('Unable to delete current file')
                        );
                    } else {
                        doUpload(taskT, param);
                    }
                });
            } else {
                doUpload(taskT, param);
            }
        };

        function doUpload(taskT, param) {
            _.each(param.toUpload, function (file) {
                Upload.upload({
                    url: config.apiPath + 'parameterfile',
                    data: {
                        file: file,
                        name: file.name,
                        application: taskT.application,
                        workflow: taskT.workflow,
                        parameter: param.name,
                    },
                }).then(
                    function (resp) {
                        if (_.isEmpty(param.value)) {
                            param.value = [];
                        }

                        var fileData = {
                            id: resp.data.id,
                            name: resp.data.name,
                            size: resp.data.size,
                            version: resp.data.version,
                        };
                        Object.defineProperty(fileData, 'justUploaded', { enumerable: false, value: true });
                        if (param.multiple) {
                            param.value.push(fileData);
                        } else {
                            param.value = [fileData];
                        }

                        // Remove files from the model so they can't be uploaded again without re-selecting them
                        _.pull(param.toUpload, file); // TODO: also make sure the input itself is reset

                        if (param.toUpload.length === 0) {
                            console.log('All uploads completed successfully');
                        }

                        console.log('Success ' + resp.config.data.file.name + ' uploaded. Response: ', resp);
                    },
                    modalService.openErrorResponseCB('File upload failed'),
                    function (evt) {
                        if (evt !== undefined) {
                            var progressPercentage = Math.floor((100.0 * evt.loaded) / evt.total);
                            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                        }
                    }
                );
            });
        }

        scope.aceLoaded = function (_editor) {
            _editor.$blockScrolling = Infinity;
        };
    }

    return {
        restrict: 'E',
        scope: {
            taskT: '=task', // Can be a Task or TaskTemplate object
            parameters: '=parameters',
            form: '=form',
            readonly: '=readonly',
        },
        templateUrl: '/app/common/operator/taskParameters.html',
        link: link,
    };
});
