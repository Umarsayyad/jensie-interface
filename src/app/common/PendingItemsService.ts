/*global define,_,angular */
import app from '../app';
import * as _ from 'lodash';

app.factory('PendingItemsService', function ($rootScope, globalDataService, moment, socketService, PendingItems) {
    var itemCounts;
    var itemIds;
    var lastFetch;
    var lastCustomer;

    var PendingItemsService = {
        init: function () {
            lastFetch = moment();
            lastCustomer = $rootScope.user.customer;
            itemCounts = { todo: {} };
            itemIds = PendingItems.get(function (data) {
                loadData(data);
                initializeWebsocketMonitor();
            });
            $rootScope.pendingItemCounts = itemCounts;
            return itemCounts;
        },
        refresh: function () {
            // Only re-fetch from the API if it has been more than 30 minutes since the last request
            if (lastFetch && (lastFetch.isBefore(moment().subtract(30, 'm')) || lastCustomer !== $rootScope.user.customer)) {
                lastFetch = moment();
                lastCustomer = $rootScope.user.customer;
                itemIds = PendingItems.get(loadData);
            }
        },
        markMessageRead: function (message) {
            var messageId = message.id || message;
            if (itemIds.messages.has(messageId)) {
                itemIds.messages.delete(messageId);
                itemCounts.messages--;
            }
        },
        markMessageUnread: function (message) {
            if (message.author && message.author.id === $rootScope.user.id) {
                return; // Message is from the current user, don't count it as unread
            }
            var messageId = message.id || message;
            if (!itemIds.messages.has(messageId)) {
                itemIds.messages.add(messageId);
                itemCounts.messages++;
            }
        },
        markRequestedUserHandled: function (userId) {
            userId = userId.id || userId;
            if (itemIds.todo.requested_users.has(userId)) {
                itemIds.todo.requested_users.delete(userId);
                itemCounts.todo.requested_users--;
                itemCounts.toDoItems--;
            }
        },
    };

    function initializeWebsocketMonitor() {
        // No need to unregister; will be cleaned up automatically on logout
        if (globalDataService.isOperator) {
            socketService.subscribeByModel('operation', function (operation) {
                if (itemIds.todo.draft_operations) {
                    if (operation.state === 'draft' && !itemIds.todo.draft_operations.has(operation.id)) {
                        itemIds.todo.draft_operations.add(operation.id);
                        itemCounts.todo.draft_operations++;
                        itemCounts.toDoItems++;
                    } else if (operation.state !== 'draft' && itemIds.todo.draft_operations.has(operation.id)) {
                        itemIds.todo.draft_operations.delete(operation.id);
                        itemCounts.todo.draft_operations--;
                        itemCounts.toDoItems--;
                    }
                }

                if (itemIds.todo.requested_operations) {
                    if (operation.state === 'requested' && !itemIds.todo.requested_operations.has(operation.id)) {
                        itemIds.todo.requested_operations.add(operation.id);
                        itemCounts.todo.requested_operations++;
                        itemCounts.toDoItems++;
                    } else if (operation.state !== 'requested' && itemIds.todo.requested_operations.has(operation.id)) {
                        itemIds.todo.requested_operations.delete(operation.id);
                        itemCounts.todo.requested_operations--;
                        itemCounts.toDoItems--;
                    }
                }

                if (itemIds.todo.running_operations) {
                    if ((operation.state === 'in progress' || operation.state === 'paused') && !itemIds.todo.running_operations.has(operation.id)) {
                        itemIds.todo.running_operations.add(operation.id);
                        itemCounts.todo.running_operations++;
                        itemCounts.toDoItems++;
                    } else if (
                        operation.state !== 'in progress' &&
                        operation.state !== 'paused' &&
                        itemIds.todo.running_operations.has(operation.id)
                    ) {
                        itemIds.todo.running_operations.delete(operation.id);
                        itemCounts.todo.running_operations--;
                        itemCounts.toDoItems--;
                    }
                }

                if (itemIds.todo.rejected_operations) {
                    if (
                        ((operation.state === 'fix up' && operation.author === $rootScope.user.id) ||
                            (operation.state === 'rejected' && operation.startedBy === $rootScope.user.id)) &&
                        !itemIds.todo.rejected_operations.has(operation.id)
                    ) {
                        itemIds.todo.rejected_operations.add(operation.id);
                        itemCounts.todo.rejected_operations++;
                        itemCounts.toDoItems++;
                    } else if (
                        ((operation.state !== 'fix up' && operation.author === $rootScope.user.id) ||
                            (operation.state !== 'rejected' && operation.startedBy === $rootScope.user.id)) &&
                        itemIds.todo.rejected_operations.has(operation.id)
                    ) {
                        itemIds.todo.rejected_operations.delete(operation.id);
                        itemCounts.todo.rejected_operations--;
                        itemCounts.toDoItems--;
                    }
                }
            });

            socketService.subscribeByModel('finding', function (finding) {
                if (globalDataService.isMissionDirector) {
                    if (finding.state === 'pending' && !itemIds.todo.findings_to_review.has(finding.id)) {
                        itemIds.todo.findings_to_review.add(finding.id);
                        itemCounts.todo.findings_to_review++;
                        itemCounts.toDoItems++;
                    }
                    if (finding.state !== 'pending' && itemIds.todo.findings_to_review.has(finding.id)) {
                        itemIds.todo.findings_to_review.delete(finding.id);
                        itemCounts.todo.findings_to_review--;
                        itemCounts.toDoItems--;
                    }
                } else if (globalDataService.isOperator) {
                    if (finding.state === 'fix' && !itemIds.todo.findings_to_review.has(finding.id)) {
                        itemIds.todo.findings_to_review.add(finding.id);
                        itemCounts.todo.findings_to_review++;
                        itemCounts.toDoItems++;
                    }
                    if (finding.state !== 'fix' && itemIds.todo.findings_to_review.has(finding.id)) {
                        itemIds.todo.findings_to_review.delete(finding.id);
                        itemCounts.todo.findings_to_review--;
                        itemCounts.toDoItems--;
                    }
                }
            });

            socketService.subscribeByModel('user', function (user) {
                if (itemIds.todo.requested_users) {
                    if (user.request_pending && !itemIds.todo.requested_users.has(user.id)) {
                        itemIds.todo.requested_users.add(user.id);
                        itemCounts.todo.requested_users++;
                        itemCounts.toDoItems++;
                    }
                }
            });
        }

        socketService.subscribeByModel('message', PendingItemsService.markMessageUnread);
    }

    function loadData(data) {
        if ($rootScope.hasAnyRole(['insight'])) {
            // TODO: Should we do this here on the frontend, or allow the request to the backend and return empty lists?
            data = {
                messages: [],
                todo: [],
            };
            return PendingItemsService;
        }
        itemCounts.messages = data.messages.length;
        itemCounts.toDoItems = _(data.todo).values().map('length').sum();

        data.messages = new Set(data.messages);
        _.each(data.todo, function (ids, k) {
            data.todo[k] = new Set(ids);
            itemCounts.todo[k] = ids.length;
        });
    }

    return PendingItemsService;
});
