// This file is for the result of an HTTP request.  It is *not* the response
// (otherwise it would be named such); rather, it is only the data portion.

export interface PaginatedResult<T> {
    count: number;
    next: string | null; //a URL
    previous: string | null; //a URL
    results: T[];
}
