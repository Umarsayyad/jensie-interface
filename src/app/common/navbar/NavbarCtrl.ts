import app from '../../app';
import * as angular from 'angular';
import * as _ from 'lodash';
import catotheme from '@cato/config/themes/catotheme';

import '../PendingItemsService';
import '../../reports/ModalGenerateReport';
import '../../finding/reference/modalProviders';
import '../../finding/reference/modalReferences';
import { PreferencesModalComponent } from '../../../ng-app/preferences/preferences-modal/preferences-modal.component';
import { MatDialog } from '@angular/material/dialog';

app.controller('NavbarCtrl', function (
    $scope,
    $rootScope,
    $state,
    $transitions,
    $uibModal,
    modalService,
    socketService,
    FileSaver,
    MatDialog,
    NotificationService,
    PendingItemsService,
    stateBuilder,
    BackupService
) {
    if (!$rootScope.user) {
        return; // Don't try to do anything if the user is not logged in (needed for the privacy policy and terms and conditions)
    }

    let stateChangeSuccessListener;
    NotificationService.setupNotifications();
    PendingItemsService.init();

    $scope.jumpForm = { jumpTo: parseInt($state.params.operation_id) || undefined };
    $scope.jumpToOperation = function () {
        $state.go('cato.loggedIn.operator', { operation_id: $scope.jumpForm.jumpTo }, { inherit: false });
        $scope.jumpForm = {};
    };
    if ($rootScope.gdata.isOperator) {
        stateChangeSuccessListener = $transitions.onSuccess({}, (transition) => {
            const params = transition.params();
            if (params.operation_id && transition.$to().includes['cato.loggedIn.operator']) {
                $scope.jumpForm.jumpTo = parseInt(params.operation_id) || undefined;
            } else {
                $scope.jumpForm.jumpTo = undefined;
            }
        });
    }

    $scope.onlyAssigned = function (customer) {
        return _.includes($scope.user.assigned_customers, customer.id);
    };

    $scope.clearCustomer = function () {
        $scope.previousCustomer = $scope.user.customer;
        return ($scope.user.customer = null);
    };

    $scope.resetCustomerIfBlank = function () {
        if (_.isEmpty($scope.user.customer)) {
            $scope.user.customer = $scope.previousCustomer;
        }
    };

    $scope.updateManagedCustomer = function (customer) {
        if (_.isEmpty(customer)) {
            $scope.resetCustomerIfBlank();
            return false;
        }
        $rootScope.switchCustomer(customer.id);
        return false;
    };

    $scope.openFeedback = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/app/feedback/feedbackForm.html',
            size: 'lg',
            controller: function ($uibModalInstance, $rootScope, $scope, $http, Feedback) {
                // options = Feedback.options
                // options['actions']['POST']['message']
                //
                $scope.config = {};

                function loadFeedbackOptions() {
                    Feedback.options().$promise.then(function (values) {
                        /* config.message.min_length, config.message.max_length */
                        $scope.config.message = values.actions.POST.message;
                    });
                }

                loadFeedbackOptions();

                $scope.sendFeedback = function () {
                    Feedback.save({ referrer: location.href, message: $scope.message }).$promise.then(
                        function (res) {
                            $uibModalInstance.close();
                            modalService.openSuccess('Thank you!', 'Feedback submitted successfully.', 5000);
                        },
                        function (res) {
                            console.log(res);
                            alert(
                                `Sorry, your feedback could not be submitted, please try again later or contact ${catotheme.appName.toLowerCase()}@${
                                    catotheme.appCompanyDomain
                                } \n${angular.toJson(res)}`
                            );
                        }
                    );
                };

                $scope.ok = function () {
                    $uibModalInstance.close();
                };

                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            },
        });
    };

    $scope.openTools = () => {
        let modal = stateBuilder.ngComponentModalGenerator('tools-window');
        $uibModal.open(modal);
    };

    $scope.openGenerateReport = function () {
        var bindings = {
            service: '<?',
        };

        var modal = stateBuilder.componentModalGenerator('modal-generate-report', bindings);
        $uibModal.open(modal);
    };

    $scope.openPreferenceComponent = function () {
        MatDialog.open(PreferencesModalComponent, { width: '50vw', panelClass: 'gray-bg' });
    };

    $scope.openReferenceProvidersModal = function () {
        var modal = stateBuilder.componentModalGenerator('modal-providers', {}, {}, 'finding/reference/modalProviders', undefined, 'xl');
        $uibModal.open(modal);
    };

    $scope.openReferencesModal = function () {
        var modal = stateBuilder.componentModalGenerator('modal-references', {}, {}, 'finding/reference/modalReferences', undefined, 'xl');
        $uibModal.open(modal);
    };

    const backupsUnsubscribe = socketService.subscribeByGroup('md-backups', function (backup) {
        $scope.backups.push(getBackupObject(backup.filename));
    });

    $scope.$on('$destroy', function () {
        backupsUnsubscribe();
        if (stateChangeSuccessListener) stateChangeSuccessListener();
    });

    $scope.backUpNow = function () {
        BackupService.backUpNow()
            .then((res) => {
                modalService.openSuccess('Backup Started', 'A backup of the schema has started.', false);
            })
            .catch((error) => {
                modalService.openErrorResponse('Could not start a backup.', error, false);
            });
    };

    $scope.downloadBackup = function (backup: { filename: string; timestamp: string }) {
        BackupService.downloadBackup(backup.timestamp)
            .then((res) => {
                let blob = new Blob([res]);
                FileSaver.saveAs(blob, backup.filename);
            })
            .catch((error) => {
                modalService.openErrorResponse('Could not download backup.', error, false);
            });
    };

    function getBackupObject(filename: string) {
        let dateStr = filename.replace('cato-schema-backup-', '').replace('.sql.gz', '');
        let parsedDate = new Date(dateStr);

        return {
            date: parsedDate,
            timestamp: dateStr,
            filename: filename,
        };
    }

    if ($scope.gdata.isMissionDirectorOrAdmin) {
        BackupService.list().then((res) => {
            $scope.backups = [];

            for (var i = 0; i < res.length; i++) {
                $scope.backups.push(getBackupObject(res[i]));
            }
        });
    }
});
