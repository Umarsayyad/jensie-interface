import app from '../app';

app.service('RandomColorService', function () {
    // Seed to get repeatable colors
    var seed = null;

    var colors = [
        { color: 'navy', hex: '#001F3F' },
        { color: 'blue', hex: '#0074D9' },
        { color: 'aqua', hex: '#7FDBFF' },
        { color: 'teal', hex: '#39CCCC' },
        { color: 'olive', hex: '#3D9970' },
        { color: 'green', hex: '#2ECC40' },
        { color: 'lime', hex: '#01FF70' },
        { color: 'yellow', hex: '#FFDC00' },
        //{color: 'orange',     hex: "#FF851B" },
        { color: 'red', hex: '#FF4136' },
        { color: 'fuchsia', hex: '#F012BE' },
        { color: 'purple', hex: '#B10DC9' },
        { color: 'maroon', hex: '#85144B' },
        //{color: 'white',      hex: "#FFFFFF" },
        { color: 'silver', hex: '#DDDDDD' },
        { color: 'gray', hex: '#AAAAAA' },
        { color: 'black', hex: '#111111' },
    ];

    var randomColor = function (options) {
        options = options || {};

        // Check if there is a seed and ensure it's an
        // integer. Otherwise, reset the seed value.
        if (options.seed && options.seed === parseInt(options.seed, 10)) {
            seed = options.seed;

            // A string was passed as a seed
        } else if (typeof options.seed === 'string') {
            seed = stringToInteger(options.seed);

            // Something was passed as a seed but it wasn't an integer or string
        } else if (options.seed !== undefined && options.seed !== null) {
            throw new TypeError('The seed value must be an integer');

            // No seed, reset the value outside.
        } else {
            seed = null;
        }

        if (seed) {
            return colors[seed].hex;
        } else {
            return colors[Math.floor(Math.random() * Math.floor(colors.length))].hex;
        }
    };

    function stringToInteger(string) {
        var total = 0;
        for (var i = 0; i !== string.length; i++) {
            if (total >= Number.MAX_SAFE_INTEGER) break;
            total += string.charCodeAt(i);
        }

        if (total > colors.length) {
            total = total % colors.length;
        }
        return total;
    }

    function userIconColor(initials) {
        return { background: randomColor({ seed: initials }) };
    }

    return {
        userIconColor: userIconColor,
        randomColor: randomColor,
    };
});
