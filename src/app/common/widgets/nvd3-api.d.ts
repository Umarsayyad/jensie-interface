export declare interface Nvd3Api {
    refresh(): void; //Completely refresh directive.
    update(): void; //Update chart layout (for example if container is resized). [v0.1.0+]
    refreshWithTimeout(t: number): void; //Completely refresh directive after specified timeout t. [v1.0.4+]
    updateWithTimeout(t: number): void; //Update chart layout after specified timeout t. [v1.0.4+]
    updateWithOptions(options: any): void; //Update chart with new options json.
    updateWithData(data: any): void; //Update chart with new data json.
    clearElement(): void; //Clear completely directive element.
    getScope(): ng.IScope; //Get access to the internal directive scope. For example, we can get chart object via $scope.api.getScope().chart. [v0.1.0+]
    getElement(): Element; //Get directive DOM element. [v1.0.5+]
}
