import app from '../app';
import { Injectable } from '@angular/core';
import * as angular from 'angular';
import * as _ from 'lodash';
import { INewSessionService } from './sessionService/NewSessionService.service';

export interface IGeneralUtils {
    aAn: Function;
    getCustomer: Function;
    getRndInteger: Function;
    hasAnyRole: Function;
    hasRole: Function;
    setDivHeight: Function;
    setDivHeightBasedOnSiblings: Function;
    startsWithVowel: Function;
}

@Injectable()
export class GeneralUtils implements IGeneralUtils {
    MAXIMUM_OPERATOR_LEVEL = 10;
    private NewSessionService: INewSessionService;
    private $state: any;
    private $window: Window;
    private $timeout: Function;

    private resizeLevels = {};
    private currentResizeLevel = 0;
    private maxResizeLevel = 0;
    private lastResize = -1;
    private setHeightTimeout = 10;
    private setDivHeightRetries = 0;
    private setDivHeightRetriesLimit = 500;
    private callsDueToAncestorNotBeingReadyLimit = 500;
    public defaultCompensationValue = 170;

    // Define `socketSerivce` as a dependency.
    static $inject = ['NewSessionService', '$state', '$window', '$timeout'];
    constructor(NewSessionService, $state, $window, $timeout) {
        this.NewSessionService = NewSessionService;
        this.$state = $state;
        this.$window = $window;
        this.$timeout = $timeout;
    }

    getRndInteger(min: number, max: number) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    hasRole(role) {
        return this.NewSessionService.hasRole(role);
    }

    hasAnyRole(roles) {
        return this.NewSessionService.hasAnyRole(roles);
    }

    startsWithVowel(string: string) {
        return /^[aeiou]/i.test(string);
    }

    aAn(string) {
        if (this.startsWithVowel(string)) {
            return 'an';
        } else {
            return 'a';
        }
    }

    getCustomer() {
        var customer = {};
        var managedCustomer = _.get(this.NewSessionService.user, 'ui_data.selectedCustomer');
        if (this.$state.params.customer && (!managedCustomer || this.$state.params.customer.id !== managedCustomer.id)) {
            customer = this.$state.params.customer;
        } else if (this.NewSessionService.gdata.isOperator) {
            customer = managedCustomer;
        } else if (this.NewSessionService.gdata.isInsight) {
            customer = undefined;
        } else {
            customer = this.NewSessionService.gdata.customer;
        }
        return customer;
    }

    setDivHeightBasedOnSiblings(selector, ancestor, siblings, callback, callbackContext) {
        return this.setDivHeight(selector, null, ancestor, siblings, callback, callbackContext);
    }

    setDivHeight(selector, incomingCompensation, ancestor, siblings, callback, callbackContext) {
        let element = angular.element(selector);

        if (element.length === 0) {
            this.setDivHeightRetries++;
            if (this.setDivHeightRetries > this.setDivHeightRetriesLimit) {
                console.error('Attempted to set height on an element that never showed up!');
                return;
            }

            this.$timeout(() => {
                this.setDivHeight(selector, incomingCompensation, ancestor, siblings, callback, callbackContext);
            }, 10);
            return;
        }

        this.setDivHeightRetries = 0;

        let compensation = incomingCompensation && !isNaN(incomingCompensation) ? incomingCompensation : 0;
        let level;

        if (this.currentResizeLevel === undefined) {
            this.currentResizeLevel = 0;
        }

        if (!isNaN(this.resizeLevels[selector])) {
            level = this.resizeLevels[selector];
            if (level === 0) {
                this.currentResizeLevel = 0;
            }
        } else {
            level = this.maxResizeLevel;
            this.resizeLevels[selector] = this.maxResizeLevel;

            this.maxResizeLevel++;
        }

        let setHeight = () => {
            if (level === 0) {
                this.currentResizeLevel = 0;
            }

            let ancestorElement;
            let callsToCalulateCompensation = 0;
            let callsDueToAncestorNotBeingReady = 0;

            if (ancestor) {
                ancestorElement = angular.element(ancestor);
                if (ancestorElement.length === 0) {
                    if (++callsDueToAncestorNotBeingReady < this.callsDueToAncestorNotBeingReadyLimit) {
                        this.$timeout(setHeight, 10);
                    } else {
                        console.error('Ran out of time waiting for ancestor!');
                    }
                    return;
                }
            }

            let _setHeight = () => {
                let currentTime = new Date().getTime();

                if ((this.resizeLevels[selector] !== 0 && this.lastResize === -1) || currentTime - this.lastResize < this.setHeightTimeout) {
                    this.$timeout(_setHeight, this.setHeightTimeout - (currentTime - this.lastResize));
                    return;
                }

                if (this.resizeLevels[selector] === 0) {
                    this.currentResizeLevel = 0;
                }

                let ancestorHeight = ancestor ? Number(ancestorElement.css('height').slice(0, -2)) : this.$window.innerHeight;
                let element = angular.element(selector);
                element.css('height', ancestorHeight - compensation > -1 ? ancestorHeight - compensation : 0);
                element.css('min-height', 0);
                // element.css('overflow-y','auto');
                this.currentResizeLevel++;
                if (this.currentResizeLevel == this.maxResizeLevel) {
                    this.currentResizeLevel = 0;
                }
                this.lastResize = new Date().getTime();

                if (callback) {
                    callback.call(callbackContext);
                }
            };

            if (siblings) {
                let calulateCompensation = () => {
                    if (level < this.currentResizeLevel) {
                        callsToCalulateCompensation++;
                        this.$timeout(calulateCompensation, 100);
                        return;
                    }

                    let siblingsCalculated = 0;
                    compensation = incomingCompensation && !isNaN(incomingCompensation) ? incomingCompensation : 0;
                    if (typeof siblings === 'string') {
                        let siblingElement = angular.element(siblings);
                        let siblingHeight = Number(siblingElement.css('height').slice(0, -2));
                        if (siblingHeight > 0) {
                            compensation += siblingHeight;
                            siblingsCalculated = 1;
                        } else {
                            console.log('Tried to add a 0 height sibling');
                        }
                    } else if (angular.isArray(siblings) && siblingsCalculated < siblings.length) {
                        siblings.forEach((sibling) => {
                            let siblingElement = angular.element(sibling);
                            if (siblingElement.css('height')) {
                                let siblingHeight = Number(siblingElement.css('height').slice(0, -2));
                                if (siblingHeight > 0) {
                                    compensation += siblingHeight;
                                    siblingsCalculated++;
                                } else {
                                    console.log('Tried to add a 0 height sibling');
                                }
                            }
                        });
                    }

                    if (
                        (typeof siblings === 'string' && siblingsCalculated === 1) ||
                        (angular.isArray(siblings) && siblingsCalculated === siblings.length)
                    ) {
                        _setHeight();
                        callsToCalulateCompensation = 0;
                    } else if (callsToCalulateCompensation < 500) {
                        callsToCalulateCompensation++;
                        this.$timeout(calulateCompensation, 10);
                    } else {
                        console.error('Ran out of callsToCalulateCompensation!');
                        return;
                    }
                };
                calulateCompensation();
            } else {
                _setHeight();
            }
        };

        let debouncedHeight = _.debounce(setHeight, 1000);

        setHeight();

        angular.element(this.$window).on('resize', debouncedHeight);

        return () => {
            angular.element(this.$window).off('resize', debouncedHeight);
        };
    }
}

app.service('GeneralUtils', GeneralUtils);
