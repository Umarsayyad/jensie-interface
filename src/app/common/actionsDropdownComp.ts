import app from '../app';

app.component('actions', {
    bindings: {
        actions: '<',
        tooltipOverride: '<?',
        iconOverride: '<?',
        disabled: '<?',
        noright: '<?',
        onSuccess: '&',
        onFailure: '&',
        target: '<?',
        //showCount is used rather than the existance of selected, so that if selected is 0 it still shows
        showCount: '<?',
        selected: '<?',
        total: '<?',
        buttonStyle: '<?',
    },
    templateUrl: '/app/common/actionsDropdown.html',
    controller: function (globalDataService) {
        var ctrl = this;
        ctrl.gdata = globalDataService;

        ctrl.showActions = false;
        ctrl.showAction = function (actionObject, index) {
            if (index === 0) {
                ctrl.showActions = 0;
            }

            var returnVal = false;
            if (typeof actionObject.show === 'function') {
                returnVal = actionObject.show(ctrl.target, ctrl.gdata);
            } else if (typeof actionObject.show === 'boolean') {
                returnVal = actionObject.show;
            } else {
                returnVal = true;
            }

            ctrl.showActions += returnVal ? 1 : 0;

            return returnVal;
        };

        ctrl.$onChanges = function (changes) {
            ctrl.icon = ctrl.iconOverride ? ctrl.iconOverride : 'fa-gear';
            ctrl.actionTooltip = ctrl.tooltipOverride ? ctrl.tooltipOverride : 'Actions';
        };

        ctrl.performAction = function (actionObject, event) {
            actionObject.action.call(actionObject, ctrl.target, event, ctrl.onSuccess, ctrl.onFailure);
        };
    },
});
