import * as angular from 'angular';

// This import depends on app.ts, which prevents TwoFactorVerificationCtrl.ts (which contains the cato.2fa module which
// is a dependency of the CATO AngularJS module) from being loaded before app.ts.  This only works because AngularJS
// isn't bootstrapped (which is when AngularJS module resolution happens) until after this file is downloaded as a
// dependency of common/general.ts.
import './TwoFactorDeviceSetupCtrl';

angular
    .module('cato.2fa', ['cato.resources', 'ui.bootstrap'])
    .controller('TwoFactorVerificationCtrl', function ($scope, $rootScope, $http, $uibModal, $uibModalInstance, modalService, Users) {
        $scope.show = false;
        $scope.needsDevice = false;

        // Check if the user has an unconfirmed device so we can either confirm it or delete it and create a new one
        $http({
            method: 'GET',
            url: $rootScope.restUrl + 'user/' + $rootScope.login_user.id + '/devices',
            ignoreErrors: true,
        }).then(
            function (res) {
                //console.log(res);
                if (res.data.devices.length > 0) {
                    // Found existing unconfirmed device, check if the user wants to confirm it or if they want to
                    // delete it and create a new one
                    $scope.device = res.data.devices[0];
                } else {
                    // No existing devices, they need to create one
                    $scope.needsDevice = true;
                }
            },
            function (err) {
                // The user already has a confirmed device, no need to do anything extra
            }
        );

        $scope.verifyToken = function () {
            if ($scope.device) {
                Users.confirmDevice(
                    { userId: 0, deviceId: $scope.device.id },
                    { otp_token: $scope.otp_token },
                    function (res) {
                        $rootScope.twoFactorVerified = res.am_verified;
                        $uibModalInstance.close(res);
                    },
                    function (err) {
                        $scope.errorMsg = $rootScope.parseErrorResponse(err);
                    }
                );
            } else {
                Users.verifyOTP(
                    { userId: 0 },
                    { otp_token: $scope.otp_token },
                    function (res) {
                        $rootScope.twoFactorVerified = res.am_verified;
                        $uibModalInstance.close(res);
                    },
                    function (err) {
                        $scope.errorMsg = $rootScope.parseErrorResponse(err);
                    }
                );
            }
        };

        $scope.setup2FA = function () {
            $uibModal
                .open({
                    templateUrl: '/app/common/twofactor/modalTwoFactorDeviceSetup.html',
                    controller: 'TwoFactorDeviceSetupCtrl',
                })
                .result.then($uibModalInstance.close, function () {
                    $scope.needsDevice = true;
                });
        };

        $scope.deleteDevice = function () {
            Users.deleteDevice(
                { deviceId: $scope.device.id, type: 'totp' },
                function () {
                    delete $scope.device;
                    $scope.setup2FA();
                },
                modalService.openErrorResponseCB('Unable to delete device')
            );
        };
    });
