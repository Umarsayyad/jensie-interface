import app from '../../app';

app.controller('TwoFactorDeviceSetupCtrl', function ($scope, $uibModal, $uibModalInstance, modalService, Users) {
    $scope.steps = [
        {
            templateUrl: '/app/common/twofactor/setupSteps/name.html',
            hasForm: true,
        },
        {
            templateUrl: '/app/common/twofactor/setupSteps/confirm.html',
            hasForm: true,
        },
        {
            templateUrl: '/app/common/twofactor/setupSteps/done.html',
        },
    ];

    $scope.createDevice = function () {
        //Get $nextStep function from scope used to call this function
        var $nextStep = this.$nextStep;

        var device = {};
        if (this.name) {
            device['name'] = this.name;
        }
        Users.addDevice(
            device,
            function (device) {
                //console.log('QR Code URL: ' + device.qr_url);
                $scope.device = device;
                $nextStep();
            },
            modalService.openErrorResponseCB('Error creating TOTP device')
        );
    };

    $scope.confirmDevice = function () {
        //Get $nextStep function from scope used to call this function
        var $nextStep = this.$nextStep;
        Users.confirmDevice(
            { deviceId: $scope.device.id },
            { otp_token: this.otp_token },
            function (res) {
                //console.log(res);
                $scope.device.confirmed = true;
                $nextStep();
            },
            function (err) {
                $scope.errorMsg = $scope.parseErrorResponse(err);
            }
        );
    };

    $scope.finish = function () {
        $uibModalInstance.close($scope.device);
    };

    $scope.cancel = function () {
        // Roll-back (delete the device if it exists)
        if ($scope.device) {
            Users.deleteDevice({ deviceId: $scope.device.id, type: 'totp' });
        }
        this.$dismiss();
    };
});
