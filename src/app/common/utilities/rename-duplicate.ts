export function renameDuplicate(name: string) {
    const regex = /\(copy ([0-9]+)\)/;
    if (!regex.test(name)) return name + ' (copy 1)';
    else return name.replace(regex, (s, num) => `(copy ${+num + 1})`);
}

export function renameDuplicateFile(name: string) {
    const regex = /\.copy-([0-9]+)/;
    if (!regex.test(name)) return name + '.copy-1';
    else return name.replace(regex, (s, num) => `.copy-${+num + 1}`);
}
