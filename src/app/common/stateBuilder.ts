import * as _ from 'lodash';
import * as angular from 'angular';

import '../operation/modalAddOperation';

angular.module('cato.stateBuilder', []).provider('stateBuilder', [
    '$compileProvider',
    function StateBuilderProvider($compileProvider) {
        function componentModalGenerator(componentName, bindings, ctrl = {}, componentPath?, customizerName?, size = 'lg') {
            var resolve: any = {};
            if (componentPath) {
                console.warn(
                    `Lazy loading via ng-couch-potato is no longer supported.  Make sure the controller (${componentPath}) is imported properly instead.  If your component loaded properly, you can ignore this warning.`
                );
            }

            var template = '<' + componentName + ' close="$close" dismiss="$dismiss"';
            _.each(bindings, function (type, binding) {
                // TODO: Support passing through function (&) bindings
                var bindingAttribute = _.kebabCase(binding);
                if (type.indexOf('@') > -1) {
                    binding = '{{' + binding + '}}';
                }
                template += ` ${bindingAttribute}="${binding}"`;

                // add any passed promises to resolve so we wait for them to complete
                if ((_.isObject(ctrl[binding]) || _.isFunction(ctrl[binding])) && _.isFunction(ctrl[binding].then)) {
                    resolve[binding] = ctrl[binding];
                }
            });
            template += '></' + componentName + '>';

            function modalCtrl($scope, customizer) {
                _.each(bindings, function (type, binding) {
                    $scope[binding] = $scope.$resolve[binding] || ctrl[binding];
                });
                if (customizer && customizer.modalScopePopulated) {
                    customizer.modalScopePopulated($scope, ctrl);
                }
            }

            modalCtrl.$inject = ['$scope'];
            if (customizerName) {
                modalCtrl.$inject.push(customizerName);
            }

            return {
                controller: modalCtrl,
                size,
                resolve,
                template,
            };
        }

        function ngComponentModalGenerator(componentName, size = 'lg') {
            let template = '<' + componentName + ' [close]="$close" [dismiss]="$dismiss"';

            template += '></' + componentName + '>';

            function modalCtrl($scope) {}
            modalCtrl.$inject = ['$scope'];

            return {
                controller: modalCtrl,
                size,
                template,
            };
        }

        function routedComponentModalGenerator(componentName, bindings, customizerName) {
            var loaderName = _.camelCase(componentName) + 'Loader' + _.uniqueId();

            function componentCtrl($uibModal, $state, customizer) {
                var ctrl = this;
                ctrl.$onInit = function () {
                    var modal = componentModalGenerator(componentName, bindings, ctrl, undefined, customizerName);
                    if (customizer && customizer.beforeOpen) {
                        customizer.beforeOpen(modal, ctrl);
                    }

                    $uibModal.open(modal).result.then(
                        function () {
                            $state.go('^');
                        },
                        function () {
                            $state.go('^');
                        }
                    );
                };
            }

            componentCtrl.$inject = ['$uibModal', '$state'];
            if (customizerName) {
                componentCtrl.$inject.push(customizerName);
            }

            $compileProvider.component(loaderName, {
                bindings: bindings,
                controller: componentCtrl,
            });
            return loaderName;
        }

        var operationModalBindings = {
            operation: '<?',
            finding: '<?',
            operationSaved: '<?',
            reasoningPlaceholder: '<?',
            modalTitle: '<?',
            config: '<',
        };

        function operationModalState(url, customizer, viewName) {
            url = url || 'addOperation';
            viewName = viewName || 'addOperation';
            var state = {
                url: '/' + url,
                views: {},
                resolve: {
                    operationsConfig: function (Operations) {
                        return Operations.options().$promise;
                    },
                },
            };
            state.views[viewName] = {
                component: routedComponentModalGenerator('modal-add-operation', operationModalBindings, customizer),
                bindings: {
                    config: 'operationsConfig',
                },
            };
            return state;
        }

        var tagModalBindings = {
            tag: '<?',
            tagSaved: '<?',
            modalTitle: '<?',
            config: '<',
        };

        var zoneModalBindings = {
            zone: '<?',
            zoneSaved: '<?',
            modalTitle: '<?',
        };

        var accessGroupModalBindings = {
            group: '<?',
            groupSaved: '<?',
            modalTitle: '<?',
        };

        var cirdBlockModalBindings = {
            block: '<?',
            blockSaved: '<?',
            owners: '<?',
            modalTitle: '<?',
            config: '<',
        };

        var editProviderModalBindings = {
            provider: '<?',
            providerSaved: '<?',
            modalTitle: '<?',
            config: '<',
        };

        var editReferenceModalBindings = {
            reference: '<?',
            referenceSaved: '<?',
            modalTitle: '<?',
            config: '<',
        };

        var referencesModalBindings = {
            provider: '<?',
        };

        var blockOwnerModalBindings = {
            owner: '<?',
        };

        this.componentModalGenerator = componentModalGenerator;
        this.ngComponentModalGenerator = ngComponentModalGenerator;
        this.routedComponentModalGenerator = routedComponentModalGenerator;
        this.operationModalState = operationModalState;
        this.operationModalBindings = operationModalBindings;
        this.tagModalBindings = tagModalBindings;
        this.zoneModalBindings = zoneModalBindings;
        this.accessGroupModalBindings = accessGroupModalBindings;
        this.cirdBlockModalBindings = cirdBlockModalBindings;
        this.blockOwnerModalBindings = blockOwnerModalBindings;
        this.editProviderModalBindings = editProviderModalBindings;
        this.editReferenceModalBindings = editReferenceModalBindings;
        this.referencesModalBindings = referencesModalBindings;

        this.$get = () => new StateBuilderProvider($compileProvider);
    },
]);
