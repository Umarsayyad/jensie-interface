import app from '../../app';

app.filter('applianceZoneOrExternal', function (globalDataService) {
    /**
     *
     * @param {Array} items - Array of objects to filter
     * @param {Object|Number} appliance - Appliance object or ID to use to use for filtering by zone
     * @param {Number} [customerId] - If passed, and `appliance` is undefined, do not include any items that are in a zone that contains an appliance
     */
    return function applianceZoneOrExternal(items, appliance, customerId) {
        var externalZone;
        if (!appliance) {
            if (customerId) {
                externalZone = globalDataService.customerExternalZoneMap[customerId];
            } else {
                return items;
            }

            // Since we don't have the appliance selected, we want to see all external targets
            return items.filter(function (item) {
                return item.zone === externalZone.id || globalDataService.zonesWithoutAppliancesByCustomer[customerId].indexOf(item.zone) !== -1;
            });
        } else {
            if (!appliance.id) {
                appliance = globalDataService.applianceMap[appliance];
            }

            // Since we have an appliance selected, make sure we're looking at all targets within that appliance zone
            return items.filter(function (item) {
                return (appliance && item.zone === appliance.zone) || item.id === undefined;
            });
        }
    };
});
