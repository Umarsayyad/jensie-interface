import * as angular from 'angular';

angular
    .module('cato.resources', ['ngResource'])
    .factory('transformResponseDownloadFile', [
        'defaultHttpResponseTransform',
        function (defaultHttpResponseTransform) {
            return function (data, headers, statusCode) {
                // derived from http://blog.neilni.com/2016/04/23/download-file-in-angular-js/#comment-3105521454
                if (statusCode === 200) {
                    return {
                        blob: new Blob([data], { type: headers('content-type') }),
                    };
                } else {
                    // TODO: Using `Array.apply` will break if the data is too large (>100k bytes)
                    /* This may or may not be faster, but it doesn't have stack size issues
                    var charCodeArray = _.map(new Uint8Array(data), function (c) {
                        return String.fromCharCode(c);
                    });
                    var resultE = _.join(charCodeArray, '');
                    */

                    var charCodeArray = Array.apply(null, new Uint8Array(data));
                    var resultE = '';
                    var code = null;
                    for (var i = 0, len = charCodeArray.length; i < len; i++) {
                        code = charCodeArray[i];
                        resultE += String.fromCharCode(code);
                    }
                    if (resultE) resultE = defaultHttpResponseTransform(resultE, headers, statusCode);

                    return resultE;
                }
            };
        },
    ])
    .factory('AccessGroups', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
                $rootScope.restUrl + 'accessgroup/:accessGroupId',
                {},
                {
                    get: { method: 'GET', params: { format: 'json' } },
                    list: { method: 'GET', params: { format: 'json' }, isArray: true },
                    query: { method: 'GET', params: { format: 'json', detail: 'true' } },
                    detail: { method: 'GET', params: { format: 'json', detail: 'true' } },
                    options: { method: 'OPTIONS', params: { format: 'json' } },
                    delete: { method: 'DELETE', params: { format: 'json' } },
                }
            );
        },
    ])
    .factory('AccessGroupPermissions', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
                $rootScope.restUrl + 'accessgroup/:accessGroupId/permission/:accessGroupPermissionId',
                {},
                {
                    get: { method: 'GET', params: { format: 'json' } },
                    list: { method: 'GET', params: { format: 'json' }, isArray: true },
                    query: { method: 'GET', params: { format: 'json', detail: 'true' } },
                    detail: { method: 'GET', params: { format: 'json', detail: 'true' } },
                    options: { method: 'OPTIONS', params: { format: 'json' } },
                    delete: { method: 'DELETE', params: { format: 'json' } },
                }
            );
        },
    ])
    .factory('Appliances', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
                $rootScope.restUrl + 'appliance/:applianceId',
                {},
                {
                    query: { method: 'GET', params: { format: 'json' } },
                    get: { method: 'GET', params: { format: 'json' } },
                    update: { method: 'PUT', params: { format: 'json' } },
                    options: { method: 'OPTIONS', params: { format: 'json' } },
                    delete: { method: 'DELETE', params: { format: 'json' } },
                    latestHeartbeats: {
                        url: $rootScope.restUrl + 'appliance/:applianceId/heartbeat/latest',
                        method: 'GET',
                    },
                    lastCountHeartbeats: {
                        url: $rootScope.restUrl + 'appliance/:applianceId/heartbeat/lastCount',
                        method: 'GET',
                        params: { format: 'json' },
                        isArray: true,
                    },
                    reboot: {
                        url: $rootScope.restUrl + 'appliance/:applianceId/reboot',
                        method: 'POST',
                    },
                    setInterval: {
                        url: $rootScope.restUrl + 'appliance/:applianceId/set_interval',
                        method: 'POST',
                    },
                    getNotes: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'appliance/:applianceId/note',
                        params: { format: 'json' },
                        isArray: true,
                    },
                    addNote: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'appliance/:applianceId/note',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    editNote: {
                        method: 'PUT',
                        url: $rootScope.restUrl + 'appliance/:applianceId/note/:noteId',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    deleteNote: {
                        method: 'DELETE',
                        url: $rootScope.restUrl + 'appliance/:applianceId/note/:noteId',
                    },
                }
            );
        },
    ])
    .factory('AnalysisModule', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            const baseURL = $rootScope.restUrl + 'analysismodule/:analysisModuleId';
            return $resource(
                baseURL,
                { analysisModuleId: '@id' },
                {
                    query: { method: 'GET', params: { format: 'json' } },
                    get: { method: 'GET', params: { format: 'json' } },
                    update: { method: 'PUT', params: { format: 'json' } },
                    options: { method: 'OPTIONS', params: { format: 'json' } },
                    delete: { method: 'DELETE', params: { format: 'json' } },
                    test: { method: 'POST', url: baseURL + '/test', isArray: false },
                    import: { method: 'POST', url: baseURL + '/0/import', isArray: false },
                    export: { method: 'POST', url: baseURL + '/0/export', isArray: false },
                }
            );
        },
    ])
    .factory('Attachments', [
        '$resource',
        '$rootScope',
        'transformResponseDownloadFile',
        function ($resource, $rootScope, transformResponseDownloadFile) {
            return $resource(
                $rootScope.restUrl + 'attachment/:id',
                { id: '@id' },
                {
                    get: { method: 'GET', params: { format: 'json' }, isArray: false },
                    query: { method: 'GET', params: { format: 'json' } },
                    update: { method: 'PUT', params: { format: 'json' } },
                    delete: { method: 'DELETE', params: { format: 'json' } },
                    options: { method: 'OPTIONS', params: { format: 'json' } },
                    download: {
                        url: $rootScope.restUrl + 'attachment/:id/download',
                        method: 'GET',
                        responseType: 'arraybuffer',
                        transformResponse: transformResponseDownloadFile,
                    },
                }
            );
        },
    ])
    .factory('ParameterFiles', [
        '$resource',
        '$rootScope',
        'transformResponseDownloadFile',
        function ($resource, $rootScope, transformResponseDownloadFile) {
            return $resource(
                $rootScope.restUrl + 'parameterfile/:id',
                { id: '@id' },
                {
                    update: { method: 'PUT' },
                    options: { method: 'OPTIONS' },
                    download: {
                        url: $rootScope.restUrl + 'parameterfile/:id/download',
                        method: 'GET',
                        responseType: 'arraybuffer',
                        transformResponse: transformResponseDownloadFile,
                    },
                }
            );
        },
    ])
    .factory('TargetCsv', [
        '$resource',
        '$rootScope',
        'transformResponseDownloadFile',
        function ($resource, $rootScope, transformResponseDownloadFile) {
            return $resource(
                $rootScope.restUrl + 'targetcsv',
                {},
                {
                    download: {
                        url: $rootScope.restUrl + 'targetcsv',
                        params: { format: 'csv' },
                        isArray: false,
                        method: 'GET',
                        responseType: 'arraybuffer',
                        transformResponse: transformResponseDownloadFile,
                    },
                }
            );
        },
    ])
    .factory('FindingCsv', [
        '$resource',
        '$rootScope',
        'transformResponseDownloadFile',
        function ($resource, $rootScope, transformResponseDownloadFile) {
            return $resource(
                $rootScope.restUrl + 'findingcsv',
                {},
                {
                    download: {
                        url: $rootScope.restUrl + 'findingcsv',
                        params: { format: 'csv' },
                        isArray: false,
                        method: 'GET',
                        responseType: 'arraybuffer',
                        transformResponse: transformResponseDownloadFile,
                    },
                }
            );
        },
    ])
    .factory('Campaigns', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
                $rootScope.restUrl + 'campaign/:campaignId/',
                {},
                {
                    query: { method: 'GET', params: { format: 'json' } },
                    list: { method: 'GET', params: { format: 'json' }, isArray: true },
                    options: { method: 'OPTIONS', params: { format: 'json' } },
                    add: { method: 'POST', params: { format: 'json' } },
                    update: { method: 'PUT', params: { format: 'json' } },
                    delete: { method: 'DELETE', params: { format: 'json' } },
                    operations: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'campaign/:campaignId/operation',
                        params: { format: 'json' },
                        isArray: true,
                    },
                    operationDetails: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'campaign/:campaignId/operation',
                        params: { format: 'json', detail: 'true' },
                        isArray: true,
                    },
                    findings: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'campaign/:campaignId/findings',
                        params: { format: 'json' },
                        isArray: false,
                    },
                    stop: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'campaign/:campaignId/stop',
                        params: { format: 'json' },
                        isArray: false,
                    },
                    runAutomatedOps: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'autocampaign/autocampaign/:campaignId/start',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                }
            );
        },
    ])
    .factory('AutomatedCampaigns', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
                $rootScope.restUrl + 'autocampaign/autocampaign/:campaignId/',
                { campaignId: '@id' },
                {
                    start: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'autocampaign/autocampaign/:campaignId/start',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    stop: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'autocampaign/autocampaign/:campaignId/stop',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                }
            );
        },
    ])
    .factory('AutomatedOperations', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource($rootScope.restUrl + 'autocampaign/autocampaign/:campaignId/autooperation/:id', {
                campaignId: '@automated_campaign',
                id: '@id',
            });
        },
    ])
    .factory('CampaignTemplates', [
        '$resource',
        '$rootScope',
        'defaultHttpResponseTransform',
        function ($resource, $rootScope, defaultHttpResponseTransform) {
            return $resource(
                $rootScope.restUrl + 'campaigntemplate/:templateId',
                { templateId: '@id' },
                {
                    details: { method: 'GET', params: { format: 'json', detail: 'true' }, isArray: true },
                    options: {
                        method: 'OPTIONS',
                        transformResponse: function (data, headers, status) {
                            data = defaultHttpResponseTransform(data, headers, status);
                            return data.actions.POST;
                        },
                    },
                    delete: { method: 'DELETE', params: { format: 'json' } },
                    getByID: { method: 'GET', isArray: false },
                    addTemplate: {
                        method: 'POST',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    updateTemplate: {
                        method: 'PUT',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    importTemplates: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'campaigntemplate/0/import',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    exportTemplates: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'campaigntemplate/0/export',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                }
            );
        },
    ])
    .factory('BlockOwners', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
                $rootScope.restUrl + 'blockowner/:blockOwnerId',
                {},
                {
                    query: { method: 'GET', params: { format: 'json', detail: 'true' } },
                    detail: { method: 'GET', params: { format: 'json', detail: 'true' } },
                }
            );
        },
    ])
    .factory('CIDRBlocks', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
                $rootScope.restUrl + 'cidrblock/:cidrBlockId',
                {},
                {
                    query: { method: 'GET', params: { format: 'json', detail: 'true' } },
                    detail: { method: 'GET', params: { format: 'json', detail: 'true' } },
                    bulkCreate: {
                        method: 'POST',
                        params: { format: 'json' },
                        url: $rootScope.restUrl + 'cidrblock/0/bulk_create',
                        isArray: true,
                    },
                    bulkDestroy: {
                        method: 'PATCH',
                        url: $rootScope.restUrl + 'cidrblock/bulk_destroy',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    importBlocks: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'cidrblock/0/import',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    exportBlocks: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'cidrblock/0/export',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                }
            );
        },
    ])
    .factory('Conclusions', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
                $rootScope.restUrl + 'conclusion/:conclusionId',
                {},
                {
                    get: { method: 'GET', isArray: true },
                    add: { method: 'POST', params: { format: 'json' } },
                    validate: {
                        url: $rootScope.restUrl + 'conclusion/:conclusionId/validate',
                        method: 'GET',
                        params: { format: 'json' },
                    },
                    invalidate: {
                        url: $rootScope.restUrl + 'conclusion/:conclusionId/invalidate',
                        method: 'GET',
                        params: { format: 'json' },
                    },
                }
            );
        },
    ])
    .factory('CPE', [
        '$resource',
        '$rootScope',
        function ($resource) {
            return $resource(
                '/app/data/cpe.2.3.all.min.json',
                {},
                {
                    get: { method: 'GET', isArray: true },
                }
            );
        },
    ])
    .factory('Customers', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
                $rootScope.restUrl + 'customer/:customerId',
                {},
                {
                    add: { method: 'POST', params: { format: 'json' } },
                    query: { method: 'GET', params: { format: 'json' }, isArray: true },
                    get: { method: 'GET', params: { format: 'json' } },
                    options: { method: 'OPTIONS', params: { format: 'json' } },
                    addService: { method: 'POST', params: { format: 'json' } },
                    deleteService: { method: 'DELETE', params: { format: 'json' } },
                    patch: {
                        method: 'PATCH',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    roe: {
                        method: 'GET',
                        params: { format: 'json' },
                        url: $rootScope.restUrl + 'customer/:customerId/roe',
                    },
                    acceptROE: {
                        method: 'POST',
                        params: { format: 'json' },
                        url: $rootScope.restUrl + 'customer/0/accept_roe',
                    },
                    set2FA: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'customer/:customerId/set_2fa',
                        ignoreErrors: true,
                    },
                    setRequireAccessGroups: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'customer/:customerId/set_require_access_groups',
                        ignoreErrors: true,
                    },
                    recentOperationCounts: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'customer/recent_operation_counts',
                        isArray: true,
                    },
                    scoreHistory: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'customer/:customerId/score_history',
                        isArray: true,
                    },
                    availableMentors: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'customer/available_mentors',
                        isArray: true,
                    },
                    createMentor: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'customer/create_mentor',
                    },
                    fetchSamData: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'customer/fetch_sam_data'
                    },
                    validateSam: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'customer/:customerId/validate_sam'
                    }
                }
            );
        },
    ])
    .factory('CustomerSubscriptions', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
                $rootScope.restUrl + 'customer/:customerId/subscription/:subscriptionId',
                { customerId: '@customerId', subscriptionId: '@subscriptionId' },
                {
                    add: { method: 'POST', params: { format: 'json' } },
                    get: { method: 'GET', params: { format: 'json' } },
                    update: { method: 'PUT', params: { format: 'json' } },
                    delete: { method: 'DELETE', params: { format: 'json' } },
                    query: { method: 'GET', params: { format: 'json' }, isArray: true },
                }
            );
        },
    ])
    .factory('Feedback', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
                $rootScope.restUrl + 'feedback/:feedbackId',
                {},
                {
                    retrieve: { method: 'GET', params: { format: 'json' } },
                    options: { method: 'OPTIONS', params: { format: 'json' } },
                }
            );
        },
    ])
    .factory('Findings', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
                $rootScope.restUrl + 'finding/:findingId',
                {},
                {
                    get: { method: 'GET', params: { format: 'json' } },
                    query: { method: 'GET', params: { format: 'json', detail: 'true' } },
                    detail: { method: 'GET', params: { format: 'json', detail: 'true' } },
                    options: { method: 'OPTIONS', params: { format: 'json' } },
                    delete: { method: 'DELETE', params: { format: 'json' } },
                    basicList: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'finding/basic_list',
                        params: { format: 'json' },
                        isArray: true,
                    },
                    recent_findings: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'finding/recent',
                        params: { format: 'json' },
                        isArray: true,
                    },
                    setArchived: {
                        url: $rootScope.restUrl + 'finding/:findingId/archive',
                        method: 'POST',
                    },
                    setUnArchived: {
                        url: $rootScope.restUrl + 'finding/:findingId/unarchive',
                        method: 'POST',
                    },
                    messages: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'finding/:findingId/message/:messageId',
                        params: { format: 'json', sort: 'updated_on' },
                        isArray: true,
                    },
                    retest: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'finding/:findingId/retest/',
                        params: { format: 'json' },
                    },
                    save: {
                        method: 'POST',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    update: {
                        method: 'PUT',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    approved: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'finding',
                        params: { format: 'json', state: ['approved'] },
                    },
                    unapproved: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'finding',
                        params: { format: 'json', detail: 'true', state: ['pending'] },
                        cancellable: true,
                    },
                    rejected: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'finding',
                        params: { format: 'json', detail: 'true', state: ['fix'] },
                        cancellable: true,
                    },
                    unapprovedIgnoreErrors: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'finding',
                        params: { format: 'json', detail: 'true', state: ['pending'] },
                        cancellable: true,
                        ignoreErrors: true,
                    },
                    rejectedIgnoreErrors: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'finding',
                        params: { format: 'json', detail: 'true', state: ['fix'] },
                        cancellable: true,
                        ignoreErrors: true,
                    },
                    approve: {
                        url: $rootScope.restUrl + 'finding/:findingId/approve',
                        method: 'PUT',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    bulk_approve: {
                        url: $rootScope.restUrl + 'finding/:findingId/bulk_approve',
                        method: 'PUT',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    reject: {
                        url: $rootScope.restUrl + 'finding/:findingId/unapprove',
                        method: 'PUT',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    bulk_reject: {
                        url: $rootScope.restUrl + 'finding/:findingId/bulk_unapprove',
                        method: 'PUT',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    bulk_requestfollowup: {
                        url: $rootScope.restUrl + 'finding/:findingId/bulk_requestfollowup',
                        method: 'PUT',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    share: {
                        url: $rootScope.restUrl + 'finding/:findingId/share',
                        method: 'POST',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    customers: {
                        url: $rootScope.restUrl + 'finding/customers',
                        method: 'GET',
                        params: { format: 'json' },
                        isArray: true,
                    },
                }
            );
        },
    ])
    .factory('MetaTargets', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
                $rootScope.restUrl + 'metatarget/:metaTargetId',
                {},
                {
                    get: { method: 'GET', params: { format: 'json' } },
                    paginate: { method: 'GET', url: $rootScope.restUrl + 'metatarget/paginate' },
                    detail: { method: 'GET', params: { format: 'json', detail: 'true' } },
                    details: { method: 'GET', params: { format: 'json', detail: 'true' }, isArray: true },
                    add: {
                        method: 'POST',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                        isArray: false,
                    },
                    unlock: {
                        method: 'PATCH',
                        url: $rootScope.restUrl + 'metatarget/:metaTargetId/unlock',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    deleteSpecificMetaTarget: { method: 'DELETE', params: { format: 'json' } },
                    update: {
                        method: 'PUT',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    getNotes: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'metatarget/:metaTargetId/note',
                        params: { format: 'json' },
                        isArray: true,
                    },
                    addNote: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'metatarget/:metaTargetId/note',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    editNote: {
                        method: 'PUT',
                        url: $rootScope.restUrl + 'metatarget/:metaTargetId/note/:noteId',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    deleteNote: {
                        method: 'DELETE',
                        url: $rootScope.restUrl + 'metatarget/:metaTargetId/note/:noteId',
                    },
                }
            );
        },
    ])
    .factory('Messages', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
                $rootScope.restUrl + 'message/:messageId',
                {},
                {
                    details: { method: 'GET', params: { format: 'json', detail: 'true' }, isArray: true },
                    options: { method: 'OPTIONS', params: { format: 'json' } },
                    addMessage: {
                        method: 'POST',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                        isArray: false,
                    },
                    setRead: {
                        url: $rootScope.restUrl + 'message/:messageId/read',
                        method: 'PUT',
                    },
                    setUnRead: {
                        url: $rootScope.restUrl + 'message/:messageId/unread',
                        method: 'PUT',
                    },
                    setArchived: {
                        url: $rootScope.restUrl + 'message/:messageId/hide',
                        method: 'PUT',
                    },
                    setUnArchived: {
                        url: $rootScope.restUrl + 'message/:messageId/show',
                        method: 'PUT',
                    },
                }
            );
        },
    ])
    .factory('Operations', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
                $rootScope.restUrl + 'operation/:operationId',
                {},
                {
                    get: { method: 'GET', params: { format: 'json' }, isArray: false },
                    query: { method: 'GET', params: { format: 'json', detail: 'true' } },
                    basic: { method: 'GET', params: { format: 'json', basic: 'true' } },
                    detail: { method: 'GET', params: { format: 'json', detail: 'true' } },
                    bulkDestroy: {
                        method: 'PATCH',
                        url: $rootScope.restUrl + 'operation/bulk_destroy',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    analyze: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'operation/:operationId/analyze',
                    },
                    analysis_results: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'operation/:operationId/analysis',
                        params: { format: 'json' },
                        isArray: true,
                    },
                    save: {
                        method: 'POST',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    update: {
                        method: 'PUT',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    addNote: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'operation/:operationId/note',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    editNote: {
                        method: 'PUT',
                        url: $rootScope.restUrl + 'operation/:operationId/note/:noteId',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    deleteNote: {
                        method: 'DELETE',
                        url: $rootScope.restUrl + 'operation/:operationId/note/:noteId',
                    },
                    authorize: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'operation/:operationId/authorize_start',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    approve: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'operation/:operationId/approve_creation',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    disapprove: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'operation/:operationId/disapprove_creation',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    reject: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'operation/:operationId/reject_start',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    fix: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'operation/:operationId/fix',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    request_review: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'operation/:operationId/request_review',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    request_start: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'operation/:operationId/request_start',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    cancel_request_start: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'operation/:operationId/cancel_request_start',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    complete: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'operation/:operationId/complete',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    pause: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'operation/:operationId/pause',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    unpause: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'operation/:operationId/unpause',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    startTask: {
                        method: 'PUT',
                        url: $rootScope.restUrl + 'operation/:operationId/task/:taskId/start',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    stopTask: {
                        method: 'PUT',
                        url: $rootScope.restUrl + 'operation/:operationId/task/:taskId/stop',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    forceStopTask: {
                        method: 'PUT',
                        url: $rootScope.restUrl + 'operation/:operationId/task/:taskId/force_stop',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    getTask: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'operation/:operationId/task/:taskId',
                        params: { format: 'json' },
                    },
                    paginateTasks: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'operation/:operationId/task/paginate',
                        params: { format: 'json' },
                        isArray: false,
                    },
                    getTaskLogs: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'operation/:operationId/task/:taskId/logs',
                        isArray: true,
                    },
                    tooloutputsList: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'operation/:operationId/tooloutput',
                        params: { format: 'json' },
                        isArray: true,
                    },
                    tooloutput: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'operation/:operationId/tooloutput/:tooloutputId',
                        params: { format: 'json' },
                    },
                    cancel: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'operation/:operationId/cancel',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                }
            );
        },
    ])
    .factory('OperationTemplates', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
                $rootScope.restUrl + 'operationtemplate/:templateId',
                {},
                {
                    details: { method: 'GET', params: { format: 'json', detail: 'true' }, isArray: true },
                    list: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'operationtemplate',
                        params: { format: 'json' },
                        isArray: true,
                    },
                    paginate: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'operationtemplate/paginate',
                        params: { format: 'json', basic: 'true' },
                    },
                    options: { method: 'OPTIONS', params: { format: 'json' } },
                    deleteSpecificTemplate: { method: 'DELETE', params: { format: 'json' } },
                    getByID: { method: 'GET', isArray: false },
                    addTemplate: {
                        method: 'POST',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    updateTemplate: {
                        method: 'PUT',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    tasks: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'operationtemplate/:templateId/tasktemplate',
                        isArray: true,
                    },
                    getTask: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'operationtemplate/:templateId/tasktemplate/:taskId',
                        isArray: false,
                    },
                    addTask: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'operationtemplate/:templateId/tasktemplate',
                        isArray: false,
                    },
                    updateTask: {
                        method: 'PUT',
                        url: $rootScope.restUrl + 'operationtemplate/:templateId/tasktemplate/:taskId',
                        isArray: false,
                    },
                    deleteTask: {
                        method: 'DELETE',
                        url: $rootScope.restUrl + 'operationtemplate/:templateId/tasktemplate/:taskId',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    importTemplates: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'operationtemplate/0/import',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    exportTemplates: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'operationtemplate/0/export',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                }
            );
        },
    ])
    .factory('PendingItems', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource($rootScope.restUrl + 'pending');
        },
    ])
    .factory('Providers', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            var baseURL = $rootScope.restUrl + 'referenceprovider/:id';
            return $resource(
                baseURL,
                { id: '@id' },
                {
                    referenceCount: { method: 'GET', url: baseURL + '/reference_count', isArray: true },
                    import: { method: 'POST', url: baseURL + '/import', params: { id: 0 } },
                    export: { method: 'POST', url: baseURL + '/export', params: { id: 0 } },
                }
            );
        },
    ])
    .factory('References', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            var baseURL = $rootScope.restUrl + 'reference/:id';
            return $resource(
                baseURL,
                { id: '@id' },
                {
                    listIds: {
                        method: 'POST',
                        url: baseURL + '/list_ids',
                        isArray: true,
                    },
                    options: {
                        method: 'OPTIONS',
                        url: $rootScope.restUrl + 'reference',
                        params: { format: 'json' },
                    },
                }
            );
        },
    ])
    .factory('TaskParameters', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
                $rootScope.restUrl + 'taskparametertemplate/:paramsId',
                {},
                {
                    options: { method: 'OPTIONS', params: { format: 'json' } },
                    get: { method: 'GET', params: { format: 'json' }, isArray: false },
                    save: {
                        method: 'POST',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    update: {
                        method: 'PUT',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    importTemplates: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'taskparametertemplate/0/import',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    exportTemplates: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'taskparametertemplate/0/export',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    availableTaskParameters: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'taskparametertemplate/:paramsId/parameters',
                        params: { format: 'json' },
                        isArray: true,
                    },
                }
            );
        },
    ])
    .factory('TaskTemplates', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
                $rootScope.restUrl + 'operationtemplate/:templateId',
                {},
                {
                    options: {
                        method: 'OPTIONS',
                        url: $rootScope.restUrl + 'operationtemplate/0/tasktemplate',
                        params: { format: 'json' },
                    },
                    partial_update: {
                        method: 'PATCH',
                        url: $rootScope.restUrl + 'operationtemplate/:templateId/tasktemplate/:taskTemplateId',
                        params: { format: 'json' },
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                }
            );
        },
    ])
    .factory('EmailDomains', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
                $rootScope.restUrl + 'customer/:customerId/emaildomain/:domainId',
                {},
                {
                    create: {
                        method: 'POST',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    update: {
                        method: 'PUT',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    options: {
                        method: 'OPTIONS',
                        url: $rootScope.restUrl + 'customer/0/emaildomain',
                        params: { format: 'json' },
                    },
                }
            );
        },
    ])
    .factory('Locations', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
                $rootScope.restUrl + 'customer/:customerId/location/:locationId',
                {},
                {
                    create: {
                        method: 'POST',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    update: {
                        method: 'PUT',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    options: {
                        method: 'OPTIONS',
                        url: $rootScope.restUrl + 'customer/0/location',
                        params: { format: 'json' },
                    },
                }
            );
        },
    ])
    .factory('Urls', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
                $rootScope.restUrl + 'customer/:customerId/url/:urlId',
                {},
                {
                    create: {
                        method: 'POST',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    update: {
                        method: 'PUT',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    options: {
                        method: 'OPTIONS',
                        url: $rootScope.restUrl + 'customer/0/url',
                        params: { format: 'json' },
                    },
                }
            );
        },
    ])
    .factory('PointsOfContact', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
                $rootScope.restUrl + 'customer/:customerId/pointofcontact/:pocId',
                {},
                {
                    create: {
                        method: 'POST',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    update: {
                        method: 'PUT',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    options: {
                        method: 'OPTIONS',
                        url: $rootScope.restUrl + 'customer/0/pointofcontact',
                        params: { format: 'json' },
                    },
                }
            );
        },
    ])
    .factory('PointOfContactAddresses', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
                $rootScope.restUrl + 'customer/:customerId/pointofcontact/:pocId/pointofcontactaddress/:pocAddrId',
                {
                    pocAddrId: '@id',
                    pocId: '@poc',
                },
                {
                    create: {
                        method: 'POST',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    update: {
                        method: 'PUT',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    options: {
                        method: 'OPTIONS',
                        url: $rootScope.restUrl + 'customer/0/pointofcontact/0/pointofcontactaddress',
                        params: { format: 'json' },
                    },
                }
            );
        },
    ])
    .factory('Recommendations', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource($rootScope.restUrl + 'recommendation/:recommendationId', { recommendationId: '@id' });
        },
    ])
    .factory('Recommendationtemplates', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            var baseURL = $rootScope.restUrl + 'recommendationtemplate/:templateId';
            return $resource(
                baseURL,
                { templateId: '@id' },
                {
                    importTemplates: { method: 'POST', url: baseURL + '/import', params: { templateId: 0 } },
                    exportTemplates: { method: 'POST', url: baseURL + '/export', params: { templateId: 0 } },
                }
            );
        },
    ])
    .factory('Restrictions', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource($rootScope.restUrl + 'customer/:customerId/roe/:roeId/restriction/:restrictionId', {
                restrictionId: '@id',
                roeId: '@rules_of_engagement',
                customerId: '@customer',
            });
        },
    ])
    .factory('RulesOfEngagements', [
        '$resource',
        '$rootScope',
        'transformResponseDownloadFile',
        function ($resource, $rootScope, transformResponseDownloadFile) {
            var baseURL = $rootScope.restUrl + 'customer/:customerId/roe/:roeId';
            return $resource(
                baseURL,
                { roeId: '@id', customerId: '@customer' },
                {
                    uploadExternal: { method: 'PUT', url: baseURL + '/upload_external' },
                    acknowledgePeriodOfPerformance: { method: 'PUT', url: baseURL + '/acknowledge' },
                    generateDocument: { method: 'PUT', url: baseURL + '/generate_document' },
                    sign: { method: 'PUT', url: baseURL + '/sign' },
                    unlock: { method: 'PUT', url: baseURL + '/unlock' },
                    approve: { method: 'PUT', url: baseURL + '/approve' },
                    reject: { method: 'PUT', url: baseURL + '/reject' },
                    downloadUnsigned: {
                        method: 'GET',
                        url: baseURL + '/download_unsigned',
                        responseType: 'arraybuffer',
                        transformResponse: transformResponseDownloadFile,
                    },
                    downloadSigned: {
                        method: 'GET',
                        url: baseURL + '/download_signed',
                        responseType: 'arraybuffer',
                        transformResponse: transformResponseDownloadFile,
                    },
                }
            );
        },
    ])
    .factory('Services', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
                $rootScope.restUrl + 'service/:serviceId',
                {},
                {
                    options: { method: 'OPTIONS', params: { format: 'json' } },
                    getByShortName: { method: 'GET', params: { format: 'json' } },
                    campaigns: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'service/:serviceId/campaigns',
                        isArray: true,
                    },
                    findings: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'service/:serviceId/findings',
                        params: { format: 'json' },
                        isArray: true,
                    },
                    campaign_findings: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'service/campaign_findings',
                        params: { format: 'json' },
                        isArray: true,
                    },
                    severities: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'service/:serviceId/severities',
                        params: { format: 'json' },
                        isArray: true,
                    },
                    create: {
                        method: 'POST',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                }
            );
        },
    ])
    .factory('Targets', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
                $rootScope.restUrl + 'target/:targetId',
                {},
                {
                    get: { method: 'GET', params: { format: 'json' } },
                    details: { method: 'GET', params: { format: 'json', detail: 'true' } },
                    list: { method: 'GET', isArray: false },
                    basicList: { method: 'GET', url: $rootScope.restUrl + 'target/basic_list', isArray: true },
                    basicListIds: { method: 'POST', url: $rootScope.restUrl + 'target/basic_list', isArray: true },
                    options: { method: 'OPTIONS', params: { format: 'json' } },
                    groups: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'target/0/groups',
                        isArray: false,
                    },
                    deleteSpecificTarget: { method: 'DELETE', params: { format: 'json' } },
                    add: {
                        method: 'POST',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    update: {
                        method: 'PUT',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    unlock: {
                        method: 'PATCH',
                        url: $rootScope.restUrl + 'target/:targetId/unlock',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    patch: {
                        method: 'PATCH',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    bulkAdd: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'target/0/bulk_create',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    bulkUpdate: {
                        // Only setting the active flag, responsible users, and tags are supported
                        method: 'PATCH',
                        url: $rootScope.restUrl + 'target/bulk_update',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    bulkDestroy: {
                        method: 'PATCH',
                        url: $rootScope.restUrl + 'target/0/bulk_destroy',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    mostVulnerableTargets: {
                        url: $rootScope.restUrl + 'target/0/most_vulnerable',
                        method: 'GET',
                        params: { format: 'json' },
                        isArray: true,
                    },
                    position_history: {
                        method: 'GET',
                        params: { format: 'json' },
                        url: $rootScope.restUrl + 'target/:targetId/position_history',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                        isArray: true,
                    },
                    get_with_attack_surface: {
                        method: 'GET',
                        params: { format: 'json', detail: 'attack_surface', has_attacksurfaces: true },
                        isArray: false,
                    },
                    get_ais_targets: {
                        method: 'GET',
                        params: { format: 'json', ais: true },
                        isArray: true,
                        url: $rootScope.restUrl + 'target/basic_list?customer=:customerId',
                    },
                    getNotes: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'target/:targetId/note',
                        params: { format: 'json' },
                        isArray: true,
                    },
                    addNote: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'target/:targetId/note',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    editNote: {
                        method: 'PUT',
                        url: $rootScope.restUrl + 'target/:targetId/note/:noteId',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    deleteNote: {
                        method: 'DELETE',
                        url: $rootScope.restUrl + 'target/:targetId/note/:noteId',
                    },
                }
            );
        },
    ])
    .factory('TargetTags', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource($rootScope.restUrl + 'targettag/:id', { id: '@id' });
        },
    ])
    .factory('TargetLinks', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
                $rootScope.restUrl + 'targetlink/:id',
                { id: '@id' },
                {
                    basicList: { method: 'GET', url: $rootScope.restUrl + 'targetlink/basic_list', isArray: true },
                }
            );
        },
    ])
    .factory('TargetZones', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            var baseURL = $rootScope.restUrl + 'targetzone/:id';
            return $resource(
                baseURL,
                { id: '@id' },
                {
                    bulkChangeZone: { method: 'PUT', url: baseURL + '/bulk_change_zone' },
                }
            );
        },
    ])
    .factory('ToDo', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
                $rootScope.restUrl + 'todo/:toDoId',
                {},
                {
                    query: { method: 'GET', params: { format: 'json' }, isArray: true },
                }
            );
        },
    ])
    .factory('ToolOutput', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
                $rootScope.restUrl + 'operation/0/tooloutput/:itemId',
                {},
                {
                    query: { method: 'GET', params: { format: 'json' }, isArray: true },
                    options: { method: 'OPTIONS', params: { format: 'json' } },
                    create: {
                        method: 'POST',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                }
            );
        },
    ])
    .factory('Users', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
                $rootScope.restUrl + 'user/:userId',
                {},
                {
                    detail: {
                        method: 'GET',
                        params: { format: 'json', detail: 'true' },
                        isArray: true,
                    },
                    query: { method: 'GET', params: { format: 'json' }, isArray: true },
                    add: { method: 'POST', params: { format: 'json' } },
                    options: { method: 'OPTIONS', params: { format: 'json' } },
                    addGroup: {
                        method: 'PUT',
                        url: $rootScope.restUrl + 'user/:userId/group/:type',
                        params: { format: 'json' },
                        isArray: true,
                    },
                    update: {
                        method: 'PUT',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    patch: {
                        method: 'PATCH',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    getToken: {
                        method: 'GET',
                        params: { format: 'json' },
                        url: $rootScope.restUrl + 'user/0/password/0/get_token',
                    },
                    requestReset: {
                        method: 'GET',
                        params: { format: 'json' },
                        url: $rootScope.restUrl + 'user/0/password/0/request_reset',
                    },
                    resetPassword: {
                        method: 'POST',
                        params: { format: 'json' },
                        url: $rootScope.restUrl + 'user/0/password/0/reset',
                    },
                    validateToken: {
                        method: 'GET',
                        params: { format: 'json' },
                        url: $rootScope.restUrl + 'user/0/password/0/validate_token',
                    },
                    getWSauth: {
                        method: 'GET',
                        params: { format: 'json' },
                        url: $rootScope.restUrl + 'user/0/ws_auth',
                    },
                    bulkAdd: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'user/0/bulk_create',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    listDevices: {
                        method: 'GET',
                        url: $rootScope.restUrl + 'user/:userId/devices',
                    },
                    verifyOTP: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'user/0/devices/0/verify_token',
                    },
                    confirmDevice: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'user/0/devices/:deviceId/confirm_device',
                    },
                    addDevice: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'user/0/devices',
                    },
                    deleteDevice: {
                        method: 'DELETE',
                        url: $rootScope.restUrl + 'user/0/devices/:deviceId',
                    },
                    notifications: {
                        method: 'GET',
                        params: { format: 'json' },
                        url: $rootScope.restUrl + 'user/:userId/notification',
                        isArray: true,
                    },
                    notificationOptions: {
                        method: 'OPTIONS',
                        url: $rootScope.restUrl + 'user/:userId/notification',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    addNotification: {
                        method: 'POST',
                        url: $rootScope.restUrl + 'user/:userId/notification',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    updateNotification: {
                        method: 'PUT',
                        url: $rootScope.restUrl + 'user/:userId/notification',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    deleteNotification: {
                        method: 'DELETE',
                        url: $rootScope.restUrl + 'user/:userId/notification/:notificationId',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    approve: {
                        url: $rootScope.restUrl + 'user/:userId/approve',
                        method: 'PUT',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    reject: {
                        url: $rootScope.restUrl + 'user/:userId/reject',
                        method: 'PUT',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    bulk_approve: {
                        url: $rootScope.restUrl + 'user/0/bulk_approve',
                        method: 'PUT',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                    bulk_reject: {
                        url: $rootScope.restUrl + 'user/0/bulk_reject',
                        method: 'PUT',
                        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
                    },
                }
            );
        },
    ])

    // non-rest_api endpoints below

    // task_attacksurface
    .factory('AttackSurfaceRun', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            var baseUrl = $rootScope.restUrl + 'attacksurface/attacksurfacerun/:id';
            return $resource(
                baseUrl,
                { id: '@id' },
                {
                    current: { method: 'GET', url: baseUrl + '/current', isArray: false },
                    detail: { method: 'GET', url: baseUrl + '/detail', isArray: false },
                }
            );
        },
    ])
    .factory('AttackSurfaceDomain', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            var baseUrl = $rootScope.restUrl + 'attacksurface/domain/:id';
            return $resource(
                baseUrl,
                { id: '@id' },
                {
                    bulkUpdate: { method: 'PATCH', url: baseUrl + '/bulk_update', isArray: true },
                }
            );
        },
    ])
    .factory('ResponseUpdate', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            var baseUrl = $rootScope.restUrl + 'attacksurface/responseupdate/:id';
            return $resource(
                baseUrl,
                { id: '@id' },
                {
                    bulkSet: { method: 'PATCH', url: baseUrl + '/bulk_set_response', isArray: true },
                }
            );
        },
    ])

    // task_gophish
    .factory('GoPhishResults', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            return $resource($rootScope.restUrl + 'gophish/results', {}, {});
        },
    ])

    // cato_scheduler
    .factory('AutoScheduleRules', [
        '$resource',
        '$rootScope',
        'defaultHttpResponseTransform',
        function ($resource, $rootScope, defaultHttpResponseTransform) {
            return $resource(
                $rootScope.restUrl + 'scheduler/autoschedulerule/:id',
                { id: '@id' },
                {
                    add: { method: 'POST' },
                    update: { method: 'PUT' },
                    options: {
                        method: 'OPTIONS',
                        transformResponse: function (data, headers, status) {
                            data = defaultHttpResponseTransform(data, headers, status);
                            return data.actions.POST;
                        },
                    },
                }
            );
        },
    ])

    // cato_reporting
    .factory('Reports', [
        '$resource',
        '$rootScope',
        'transformResponseDownloadFile',
        function ($resource, $rootScope, transformResponseDownloadFile) {
            const baseURL = $rootScope.restUrl + 'reports/:id';
            return $resource(
                baseURL,
                { id: '@id' },
                {
                    enabled: { method: 'GET', url: baseURL + '/enabled', isArray: false },
                    runnableReports: { method: 'GET', url: baseURL + '/runnable_reports', isArray: false },
                    reportParameters: { method: 'GET', url: baseURL + '/report_parameters', isArray: true },
                    runReport: { method: 'POST', url: baseURL + '/run_report' },
                    download: {
                        method: 'GET',
                        url: baseURL + '/download',
                        responseType: 'arraybuffer',
                        transformResponse: transformResponseDownloadFile,
                    },
                }
            );
        },
    ])

    // task_ugs
    .factory('UGSs', [
        '$resource',
        '$rootScope',
        function ($resource, $rootScope) {
            const baseURL = $rootScope.restUrl + 'ugs/ugs/:id';
            return $resource(
                baseURL,
                { id: '@id' },
                {
                    positionHistory: { method: 'GET', url: baseURL + '/position_history', isArray: true },
                }
            );
        },
    ]);
