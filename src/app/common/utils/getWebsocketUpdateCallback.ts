import * as _ from 'lodash';
import { updateObj } from './updateObj';

export interface IWebsocketObject {
    id: any;
    deleted?: boolean;
}

export interface IObjMap<T extends IWebsocketObject> {
    [id: string]: T;
}

/**
 * Generate callback for websocket to handle updating the given stored data, optionally using model-specific functions
 * @param {Array} objArray - Array of existing model objects (Required unless add and delete functions are passed) (TODO: support this with the types)
 * @param {Object} objMap - Map of known models keyed by ID, used to determine whether to add or update
 * @param {function} [addFunction] - Function to call when a new object is received to store it
 * @param {function} [updateFunction] - Function to call when new data for an existing object is received
 * @param {function} [deleteFunction] - Function to call when an object is deleted
 */
export function getWebsocketUpdateCallback<T extends IWebsocketObject>(
    objArray: T[],
    objMap?: IObjMap<T>,
    addFunction?: (item: T) => void,
    updateFunction?: (item: T) => void,
    deleteFunction?: (item: T) => void
): (item: T) => void {
    if (typeof objMap === 'undefined' && typeof objArray !== 'undefined') {
        objMap = {} as IObjMap<T>;
        for (let item of objArray) {
            objMap[item.id] = item;
        }
    }
    return function (newObj) {
        if (newObj.deleted) {
            if (deleteFunction) {
                deleteFunction(newObj);
            } else {
                _.remove(objArray, { id: newObj.id } as any);
                delete objMap[newObj.id];
            }
        } else {
            const oldObj = objMap[newObj.id];
            if (oldObj) {
                if (updateFunction) {
                    updateFunction(newObj);
                } else {
                    updateObj(oldObj, newObj);
                }
            } else {
                if (addFunction) {
                    addFunction(newObj);
                } else {
                    objArray.push(newObj);
                    objMap[newObj.id] = newObj;
                }
            }
        }
    };
}
