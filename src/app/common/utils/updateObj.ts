import * as _ from 'lodash';

// TODO: add documentation for parameters and tests
export function updateObj(object: any, other: any, parameters?) {
    parameters = parameters || {};
    var append = parameters.append;
    var objIdentifier = parameters.objIdentifier || 'id';
    var disconnect = parameters.disconnect;
    //var disconnect = _.isUndefined(parameters.disconnect) ? updateObj.DEEP : parameters.disconnect; // This defaults to deep, which is more resource intensive but better matches lodash's native behavior
    var nestedParameters = parameters.nested || {};

    if (_.isArray(object) && !_.isArray(other)) {
        // Merging an object into an array is almost certainly incorrect usage so throw an exception to inform the developer of the bug.
        var msg = 'You are trying to merge an object into an array.  Did you mean to merge into an object contained in the array?';
        console.error(msg, 'array:', object, 'object:', other);
        console.trace();
        throw msg;
    }

    function handleDisconnect(obj) {
        if (disconnect === updateObj.DEEP) {
            return _.cloneDeep(obj);
        } else if (disconnect) {
            // shallow
            if (_.isArray(obj)) {
                return obj.concat();
            } else {
                return _.clone(obj);
            }
        } else {
            return obj;
        }
    }

    function customizer(objValue, srcValue, key, object, source, stack) {
        // object/objValue, destination object getting merged into
        // source/srcValue, object being merged into destination
        if ((_.isNull(objValue) && _.isNumber(srcValue)) || (_.isNumber(objValue) && _.isNull(srcValue))) {
            // Allow null to overwrite and be overwritten by numbers
            return undefined; // Default action
        }
        if (!_.isNumber(objValue) && _.isNumber(srcValue)) {
            // Numbers don't overwrite non-numbers
            return objValue;
        }
        if (_.isString(key) && key[0] === '$') {
            // Don't overwrite properties that start with $
            return objValue;
        }
        if (_.isFunction(objValue) || _.isFunction(srcValue)) {
            // If either are functions, just use the current value
            return objValue;
        }
        if (_.isUndefined(objValue)) {
            // If destination is undefined, no need to continue smart merge (but do copy if desired)
            return handleDisconnect(srcValue);
        }
        //console.log('objValue:%o, srcValue:%o, key:%o, object:%o, source:%o, stack:%o', objValue, srcValue, key, object, source, stack)
        if (_.isArray(objValue) && _.isArray(srcValue)) {
            // If both are arrays
            if (_.some(objValue, _.isUndefined)) {
                console.warn(
                    'Array we are merging into has undefined elements, removing them so we can try to merge cleanly (this may break things though if IDs are used as array indices); object:%o, key:%o, array:%o',
                    _.cloneDeep(object),
                    key,
                    _.cloneDeep(objValue)
                );
                objValue = _.without(objValue, undefined);
            }
            if (objValue.length === 0 && srcValue.length > 0) {
                // If the old array is empty and the new one has data, just use the new
                return handleDisconnect(srcValue);
            }
            if (
                _.every(objValue, function (value) {
                    return _.isNumber(value) || _.isString(value);
                }) &&
                _.every(srcValue, function (value) {
                    return _.isNumber(value) || _.isString(value);
                })
            ) {
                // Mixed number and string arrays are acceptable and are merged as if all numbers or all strings (necessary for tags)
                // TODO: preserve objValue array instance?
                if (append === updateObj.KEEP_DUPLICATES) {
                    return objValue.concat(srcValue);
                } else if (append) {
                    return _.union(objValue, srcValue);
                } else {
                    // If not appending, overwrite with the new items
                    return handleDisconnect(srcValue);
                }
            }
            if (
                _.some(objValue, function (value) {
                    return typeof value !== typeof objValue[0];
                })
            ) {
                // If destination array contains multiple types, we can't merge cleanly
                console.error(
                    'Array we are merging into has elements of multiple types, we cannot merge cleanly; object:%o, key:%o, array:%o',
                    _.cloneDeep(object),
                    key,
                    _.cloneDeep(objValue)
                );
                if (_.isNumber(srcValue[0]) && !_.isNumber(objValue[0])) {
                    // If the new values are numbers and the old values aren't, keep the old
                    return objValue;
                } else {
                    // Otherwise use the new values, they are hopefully more sane
                    return handleDisconnect(srcValue);
                }
            }
            if (_.every(objValue, _.isBoolean) && _.every(srcValue, _.isBoolean)) {
                // All values are booleans; keeping only unique values or concatenating are probably not right, just use the new array
                // TODO: preserve objValue array instance?
                return handleDisconnect(srcValue);
            }
            if (_.every(objValue, _.isArray) && _.every(srcValue, _.isArray)) {
                // Arrays of arrays can't be matched by ID, hopefully the indices align
                return undefined; // Let the default implementation merge by index
            }
            if (_.every(objValue, _.isObjectLike) && _.every(srcValue, _.isObjectLike)) {
                // Array of objects; match by ID before merging; this replaces lodash's normal logic for merging arrays
                var newDest = []; // This array stores updated objects and will be used as the merged/updated array
                var unknown = []; // Objects from the new array for which we couldn't get an ID
                var destCopy = objValue.concat(); // Copy so we can remove items and see what is left over (for when appending)
                _.each(srcValue, function (src) {
                    // For each new object  // TODO: loop over old/destination objects instead so we can preserve order (and indices as long as objects aren't removed)?
                    var id = _.get(src, objIdentifier);
                    if (_.isUndefined(id)) {
                        // Store in unknown if we can't get an ID
                        unknown.push(src);
                    } else {
                        var filter = {};
                        filter[objIdentifier] = id;
                        var dstArr = _.remove(destCopy, filter); // Find the matching object(s) in the array we are updating/merging into
                        if (dstArr.length > 0) {
                            if (dstArr.length > 1) {
                                console.warn(
                                    'More than one object with ID "%s" in array we are merging into.  Each copy will get updated with the same source object.  duplicates:%o, object:%o, key:%o, array:%o, array copy:%o',
                                    id,
                                    _.cloneDeep(dstArr),
                                    _.cloneDeep(object),
                                    key,
                                    _.cloneDeep(objValue),
                                    _.cloneDeep(destCopy)
                                );
                            }
                            // Manually calling updateObj below breaks lodash's circular reference tracking.  Clear the destination array that we are merging into so if there is a reference back up to the object containing the array or one of its parents, there is nothing to loop over when we reach it again
                            objValue.length = 0; // TODO: is there a better way to handle this?
                            _.each(dstArr, function (dst) {
                                // There will normally only be one object in this array, but loop over all of them just in case
                                // Manually calling this breaks lodash's circular reference tracking, so we emptied the destination array above to at least prevent infinite recursion
                                updateObj(dst, src, nestedParameters[key] || nestedParameters.__default || parameters); // recursively update object (optionally with different parameters)
                                newDest.push(dst);
                            });
                        } else {
                            // New object doesn't have a match, just add it
                            newDest.push(handleDisconnect(src));
                        }
                    }
                }); // End loop over new objects
                if (append) {
                    // If we are appending, keep any objects from the destination array that weren't in source
                    _.each(destCopy, function (dst) {
                        newDest.push(dst); // Add them to the end; new and updated objects are probably more interesting to display first if some other ordering isn't being applied
                    });
                }
                if (unknown.length > 0) {
                    _.each(unknown, function (unk) {
                        newDest.push(unk); // Add them to the end; new and updated objects are probably more interesting to display first if some other ordering isn't being applied
                    });
                    console.warn(
                        'Some new objects could not have an ID retrieved from the "%s" property.  They will be included at the end of the merged array so that they are not lost.  unknowns:%o, new/source object:%o, key:%o, array:%o, output array:%o',
                        objIdentifier,
                        unknown,
                        source,
                        key,
                        srcValue,
                        newDest
                    );
                }
                return newDest;
            } // End array of objects handling
            if (_.every(objValue, _.isNumber) && _.every(srcValue, _.negate(_.isNumber))) {
                // Just use the new values if they are non-numbers and old/dest values are all numbers
                return handleDisconnect(srcValue);
            }
            if (_.every(objValue, _.negate(_.isNumber)) && _.every(srcValue, _.isNumber)) {
                // If old/dest values are not numbers and new values are numbers, just keep the old
                return objValue;
            }
        } // End if both are arrays
        return undefined;
    }

    return _.mergeWith(object, other, customizer);
}

updateObj.KEEP_DUPLICATES = 'keep duplicates';
updateObj.DEEP = 'deep';
