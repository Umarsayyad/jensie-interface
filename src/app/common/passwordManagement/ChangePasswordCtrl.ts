import app from '../../app';

var ChangePasswordCtrl = function (Users, $rootScope, globalDataService, modalService) {
    var ctrl = this;
    ctrl.gdata = globalDataService;

    ctrl.showPassword = false;

    ctrl.passwords = {
        newPassword: '',
        oldPassword: '',
        newPasswordConfirm: '',
    };

    const invalid = 'fa fa-times-circle-o invalid';
    const valid = 'fa fa-check-circle-o valid';
    reset();

    function reset() {
        ctrl.invalid = false;
        ctrl.letter = invalid;
        ctrl.capital = invalid;
        ctrl.number = invalid;
        ctrl.special = invalid;
        ctrl.length = invalid;
        ctrl.passwordsDontMatch = false;
        ctrl.isSubmitable = false;
    }

    function checkInvalid(pass: string): boolean {
        var regex = new RegExp('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^A-Za-z0-9]).{8,}$');
        return pass.match(regex) == null;
    }

    function passContains(regex: RegExp, pass: string): string {
        if (pass.length > 0) {
            return pass.match(regex) ? valid : invalid;
        } else {
            return 'invalid';
        }
    }

    function hasLetter(pass: string) {
        const regex = new RegExp(/[a-z]/);
        ctrl.letter = passContains(regex, pass);
    }

    function hasCapital(pass: string) {
        const regex = new RegExp(/[A-Z]/);
        ctrl.capital = passContains(regex, pass);
    }

    function hasNumber(pass: string) {
        const regex = new RegExp(/[0-9]/);
        ctrl.number = passContains(regex, pass);
    }

    function hasSpecial(pass: string) {
        const regex = new RegExp(/[^A-Za-z0-9]/);
        ctrl.special = passContains(regex, pass);
    }

    function length(pass: string) {
        const regex = new RegExp(/.{8,}/);
        ctrl.length = passContains(regex, pass);
    }

    ctrl.validatePass = function () {
        let oldPass = ctrl.passwords.oldPassword;
        let pass = ctrl.passwords.newPassword;

        // include this type guard to prevent unwanted values from slipping through
        // if pass (or oldPass) is not a string, it's invalid
        if (typeof pass !== 'string' || typeof oldPass !== 'string') {
            ctrl.invalid = true;
            reset();
            return;
        }

        // remove the pop-over if it's empty
        ctrl.invalid = pass.length > 0 && oldPass.length > 0 ? checkInvalid(pass) : false;
        hasLetter(pass);
        hasCapital(pass);
        hasNumber(pass);
        hasSpecial(pass);
        length(pass);
        if (!ctrl.invalid) {
            ctrl.passwordsDontMatch = pass !== ctrl.passwords.newPasswordConfirm;
        }
        ctrl.submitable();
    };

    ctrl.submitable = function () {
        ctrl.isSubmitable =
            !ctrl.invalid && !ctrl.passwordsDontMatch && ctrl.passwords.newPassword.length > 0 && ctrl.passwords.oldPassword.length > 0;
    };

    ctrl.changePassword = function () {
        var args = {
            new_password: ctrl.passwords.newPassword,
            old_password: ctrl.passwords.oldPassword,
        };

        var user = $rootScope.user;

        ctrl.submission = Users.patch(
            { userId: user.id },
            args,
            function (user) {
                ctrl.gdata.updateUser(user);

                modalService.openSuccess('Password Changed', 'Your password has been changed.', false);

                ctrl.passwords = {
                    newPassword: '',
                    oldPassword: '',
                    newPasswordConfirm: '',
                };
            },
            function (res) {
                modalService.openErrorResponse('Failed to update your password', res, false);
            }
        );
    };
};

app.component('changePassword', {
    templateUrl: '/app/common/passwordManagement/modalChangePassword.html',
    controller: ChangePasswordCtrl,
});
