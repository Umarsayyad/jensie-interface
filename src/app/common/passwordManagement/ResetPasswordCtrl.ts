import app from '../../app';

app.controller('ResetPasswordCtrl', function ($scope, $state, $stateParams, $location, Users) {
    $scope.loadingDefer.resolve();

    $scope.data = {};
    $scope.enteredPassword = '';
    var invalid = 'fa fa-times-circle-o invalid';
    var valid = 'fa fa-check-circle-o valid';
    $scope.invalid = false;
    $scope.letter = invalid;
    $scope.capital = invalid;
    $scope.number = invalid;
    $scope.special = invalid;
    $scope.length = invalid;

    $scope.validToken = false;
    Users.validateToken({
        userid: $stateParams.userid,
        token: $stateParams.token,
    })
        .$promise.then(function (ret) {
            console.log('ret:', ret);
            $scope.validToken = true;
        })
        .catch(function (err) {
            console.log('failed:', err);
            $scope.validToken = false;
        });

    var checkInvalid = function (pass) {
        var regex = new RegExp('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^A-Za-z0-9]).{8,}$');
        return pass.match(regex) == null;
    };

    var passContains = function (regex) {
        var $password: string = $scope.data.password || '';
        if ($password.length > 0) {
            return $password.match(regex) ? valid : invalid;
        } else {
            return 'invalid';
        }
    };

    var letter = function () {
        var regex = new RegExp(/[a-z]/);
        $scope.letter = passContains(regex);
    };

    var capital = function () {
        var regex = new RegExp(/[A-Z]/);
        $scope.capital = passContains(regex);
    };

    var number = function () {
        var regex = new RegExp(/[0-9]/);
        $scope.number = passContains(regex);
    };

    var special = function () {
        var regex = new RegExp(/[^A-Za-z0-9]/);
        $scope.special = passContains(regex);
    };

    var length = function () {
        var regex = new RegExp(/.{8,}/);
        $scope.length = passContains(regex);
    };

    $scope.validatePass = function () {
        let pass: string = $scope.data.password || '';
        // remove the pop-over if it's empty
        $scope.invalid = pass.length > 0 ? checkInvalid(pass) : false;
        letter();
        capital();
        number();
        special();
        length();
    };

    $scope.resetPassword = function () {
        Users.resetPassword({
            userid: $stateParams.userid,
            token: $stateParams.token,
            pin: $scope.data.pin,
            password: $scope.data.password,
        }).$promise.then(function () {
            $state.go('cato.login');
        });
    };
});
