import app from '../../app';

app.controller('ForgotPasswordCtrl', function ($scope, $state, $stateParams, Users) {
    $scope.loadingDefer.resolve();

    $scope.data = {};
    $scope.sent = false;
    $scope.forgotPassword = function () {
        Users.getToken().$promise.then(function (token) {
            Users.requestReset({ username: $scope.data.username.toLowerCase(), temp_token: token.token }).$promise.then(function () {
                console.log('sent');
                $scope.sent = true;
            });
        });
    };
});
