import app from '../../app';

app.controller('InitialSetPasswordCtrl', function ($scope, $state, $stateParams, Users) {
    $scope.loadingDefer.resolve();

    var username = new RegExp('[?&]username=([^&#]*)').exec(window.location.href)[1];
    console.log('username: ' + username);

    $scope.data = {};
    $scope.sent = false;
    $scope.sendSetPasswordEmail = function () {
        Users.getToken().$promise.then(function (token) {
            Users.requestReset({ username: username, temp_token: token.token, account_creation: true }).$promise.then(function () {
                $scope.sent = true;
            });
        });
    };
});
