import app from '../app';
import * as _ from 'lodash';

import { Feature, Overlay, View } from 'ol';
import { boundingExtent, createEmpty, extend, getCenter, getWidth, getHeight } from 'ol/extent';
import { fromLonLat, transform, transformExtent } from 'ol/proj';
import Control from 'ol/control/Control';
import VectorLayer from 'ol/layer/Vector';
import LineString from 'ol/geom/LineString';
import Point from 'ol/geom/Point';
import Cluster from 'ol/source/Cluster';
import VectorSource from 'ol/source/Vector';
import { Circle as CircleStyle, Fill, Icon, Stroke, Style, Text } from 'ol/style';

import olms from 'ol-mapbox-style';
import * as ContextMenu from 'ol-contextmenu';

import './mapsUtils';
import '../common/generalUtils';
import { IGeneralUtils } from '../common/generalUtils';

app.component('maps', {
    templateUrl: '/app/maps/maps.html',
    controller: function MapsCmp(
        $q,
        $rootScope,
        $state,
        $timeout,
        Appliances,
        modalService,
        Targets,
        TargetService,
        UGSs,
        globalDataService,
        mapsUtils,
        GeneralUtils: IGeneralUtils
    ) {
        const textFill = new Fill({
            color: '#fff',
        });
        const textStroke = new Stroke({
            color: 'rgba(0, 0, 0, 0.6)',
            width: 3,
        });
        const invisibleFill = new Fill({
            color: 'rgba(255, 255, 255, 0.01)',
        });

        var ctrl: any = {
            gdata: globalDataService,
            catotheme: $rootScope.catotheme,
            aAn: $rootScope.aAn,
            cpeOptions: undefined,
            // appliancesInitialized: false,
            // latestApplianceHeartbeats: {},
            focusedColor: 'red',
            defaultButtonColor: 'rgba(0,60,136,.5)',
            map: undefined,
            mapsBusy: null,
            //Based on 15 seconds between heartbeats and a survey time of between an hour or two
            maxHeartBeats: 500,
            maxResolution: undefined,
            maxZoom: 24,
            tilesLoading: 0,
            tilesLoaded: 0,
            postrenderKey: undefined,
            changeSizeKey: undefined,
            contextMenuBeforeopenKey: undefined,
            contextMenuCloseKey: undefined,
            customer: undefined,
            showingAppliances: true,
            showingAppliancesHistory: false,
            showingTargets: true,
            showingTargetsHistory: false,
            showingUgsDevices: true,
            showOnlyActiveTargets: true,
            appliancesButton: undefined,
            targetsButton: undefined,
            ugsButton: undefined,
            inactiveTargetsButton: undefined,
            tileloadstartKey: undefined,
            tileloadendKey: undefined,
            unregisterSelectedCustomer: undefined,
            unregisterSelectedCustomerWatch: undefined,
            origExtent: transformExtent([-125.3321, 23.8991, -65.7421, 49.4325], 'EPSG:4326', 'EPSG:3857'),
            vectorLayers: {
                appliancesVectorLayer: undefined,
                appliancesArrowVectorLayer: undefined,
                appliancesHistoryVectorLayer: undefined,
                focusedAppliancesVectorLayer: undefined,

                targetVectorLayer: undefined,
                targetHistoryVectorLayer: undefined,
                targeArrowVectorLayer: undefined,
                focusedTargetVectorLayer: undefined,

                ugsVectorLayer: undefined,
                ugsHistoryVectorLayer: undefined,
                ugsArrowVectorLayer: undefined,
            },
            vectorSources: {
                appliancesSourceVector: new VectorSource(),
                appliancesArrowSourceVector: new VectorSource(),
                appliancesHistorySourceVector: new VectorSource(),
                focusedAppliancesSourceVector: new VectorSource(),

                targetSourceVector: new VectorSource(),
                targetHistorySourceVector: new VectorSource(),
                targetArrowSourceVector: new VectorSource(),
                focusedTargetsSourceVector: new VectorSource(),

                ugsSourceVector: new VectorSource(),
                ugsHistorySourceVector: new VectorSource(),
                ugsArrowSourceVector: new VectorSource(),
            },
            defaultCenterOnZoom: 6,
            hitTolerance: 10,
            menuLengthLimit: 5,
            currentResolution: undefined,
            contextmenu: undefined,
            featuresWithShownHistory: [],
            startingPosition: undefined,
            $onInit: function () {
                ctrl.customer = ctrl.getCustomer();

                ctrl.mapsBusy = $q.defer();

                ctrl.unregisterSelectedCustomerWatch = $rootScope.$watch('user.ui_data.selectedCustomer', function (newVal, oldVal) {
                    if (newVal !== oldVal) {
                        ctrl.customer = ctrl.getCustomer();
                        if (!ctrl.map) {
                            $timeout(ctrl.callOlms, 0);
                        }
                    }
                });

                if (_.isNil(ctrl.cpeOptions)) {
                    ctrl.cpeOptions = ctrl.gdata.getCPE();
                }

                if (!ctrl.customer) {
                    return;
                }

                ctrl.callOlms();

                ctrl.stopSizingTab = GeneralUtils.setDivHeight('.dashboard-tab-maps', 170);
            },
            $onDestroy: function () {
                if (ctrl.stopSizingMap) {
                    ctrl.stopSizingMap();
                }

                if (ctrl.stopSizingTab) {
                    ctrl.stopSizingTab();
                }

                if (!ctrl.map) {
                    return;
                }

                $rootScope.user._previousMapView = ctrl.map.getView();

                ctrl.map.un('change:size', ctrl.changeSizeKey.listener);
                ctrl.map.un('beforeopen', ctrl.contextMenuBeforeopenKey.listener);
                ctrl.map.un('close', ctrl.contextMenuCloseKey.listener);
                ctrl.map.un('tileloadstart', ctrl.tileloadstartKey.listener);
                ctrl.map.un('tileloadend', ctrl.tileloadendKey.listener);
                ctrl.unregisterSelectedCustomer();

                //Clear out the current vectorSources
                for (let vectorSource in ctrl.vectorSources) {
                    if (!ctrl.vectorSources.hasOwnProperty(vectorSource)) {
                        //The current property is not a direct property of ctrl
                        continue;
                    }
                    ctrl.vectorSources[vectorSource].clear(false);
                }

                mapsUtils.cleanupUtils();
            },
            callOlms: function () {
                //'/app/map-styles/osm-bright-style.json'
                olms('map', '/maps/styles/osm-bright/style.json').then(function (map) {
                    ctrl.map = map;

                    ctrl.map.getControls().extend([new ctrl.Toggle()]);
                    var managedCustomer = _.get($rootScope.user, 'ui_data.selectedCustomer');
                    if ($state.params.customer && (!managedCustomer || $state.params.customer.id !== managedCustomer.id)) {
                        ctrl.map.getControls().extend([new ctrl.ShowingCustomerLabel()]);
                    }

                    ctrl.addInitialLayers(map);

                    map.getView().setMaxZoom(ctrl.maxZoom);

                    ctrl.setupEventsAndWatches(map);

                    // JAC: Used to prove the resolution is changing on scroll wheel
                    // map.on('wheel', function(evt) {
                    //     console.log(map.getView().getResolution());
                    // });

                    if ($rootScope.user._previousMapView) {
                        ctrl.map.setView($rootScope.user._previousMapView);
                        ctrl.map.getView().on('change:resolution', ctrl.resChange);
                    } else {
                        ctrl.openMap();
                    }

                    if ($state.params.appliance) {
                        ctrl.addSingleFocusedAppliance(true);
                        ctrl.toggleTargets(false);
                        ctrl.toggleAppliances(false);
                        ctrl.toggleUgsDevices(false);
                    } else if ($state.params.target) {
                        ctrl.addSingleFocusedTarget(true);
                        ctrl.toggleTargets(false);
                        ctrl.toggleAppliances(false);
                        ctrl.toggleUgsDevices(false);
                    } else if ($state.params.targets) {
                        ctrl.addMultipleFocusedTargets(true);
                        ctrl.toggleTargets(false);
                        ctrl.toggleAppliances(false);
                        ctrl.toggleUgsDevices(false);
                    }
                    ctrl.loadTargets(ctrl.customer, true);
                    ctrl.loadAppliances(ctrl.customer);
                    ctrl.loadUGSs();

                    ctrl.setupTooltip();

                    ctrl.setupContextMenu();

                    ctrl.stopSizingMap = GeneralUtils.setDivHeightBasedOnSiblings(
                        '#map',
                        '#mapsTabContent > div',
                        null,
                        ctrl.map.updateSize,
                        ctrl.map
                    );

                    if (ctrl.mapsBusy) {
                        ctrl.mapsBusy.resolve(true);
                        ctrl.mapsBusy = null;
                    }
                });
            },
            setupContextMenu: function () {
                let addedMenuItems = [];
                ctrl.contextmenu = new ContextMenu({
                    eventType: 'click',
                    width: 200,
                    defaultItems: true, // defaultItems are (for now) Zoom In/Zoom Out
                    items: [
                        {
                            text: 'Center Map Here',
                            classname: 'maps-text-indent-15 ol-ctx-menu-center ol-ctx-menu-icon', // add some CSS rules
                            callback: ctrl.centerMapOnCoordinates, // `center` is your callback function
                        },
                        '-', // this is a separator
                    ],
                });

                ctrl.contextMenuBeforeopenKey = ctrl.contextmenu.on('beforeopen', (evt) => {
                    ctrl.contextmenu.disable();
                    let addLabels = true;
                    let focusedIds = {};

                    let featuresAtPixel = ctrl.map.getFeaturesAtPixel(evt.pixel, {
                        hitTolerance: ctrl.hitTolerance,
                        layerFilter: ctrl.layerFilter,
                    });

                    if (featuresAtPixel) {
                        featuresAtPixel.forEach((feature) => {
                            if (!feature.get('id') && feature.get('features') && feature.get('features').length === 1) {
                                feature = feature.get('features')[0];
                            }

                            if (!feature.get('type') || feature.get('historical')) {
                                return;
                            }

                            if (feature.get('id') && !focusedIds[feature.get('id') + feature.get('type')]) {
                                focusedIds[feature.get('id') + feature.get('type')] = {
                                    type: feature.get('type'),
                                    count: 1,
                                };
                            } else if (feature.get('id') && focusedIds[feature.get('id') + feature.get('type')]) {
                                focusedIds[feature.get('id') + feature.get('type')].count++;
                            }
                        });
                    }

                    ctrl.map.forEachFeatureAtPixel(
                        evt.pixel,
                        (feature, layer) => {
                            let layerIsFocused = layer.get('name').indexOf('focused') === 0;

                            if (layerIsFocused && focusedIds[feature.get('id') + feature.get('type')].count > 1) {
                                return;
                            }

                            if (feature && !feature.get('historical')) {
                                let features = feature.get('features');
                                if (!features) {
                                    features = [feature];
                                }

                                let itemsToAdd = ctrl.generateMenu(features, addLabels);
                                addedMenuItems = addedMenuItems.concat(itemsToAdd);
                                if (itemsToAdd.length > 0) {
                                    addLabels = false;
                                }
                            }
                        },
                        {
                            hitTolerance: ctrl.hitTolerance,
                            layerFilter: ctrl.layerFilter,
                        }
                    );

                    if (addedMenuItems.length > 0) {
                        ctrl.contextmenu.extend(addedMenuItems);
                        ctrl.contextmenu.enable();
                    }
                });

                ctrl.contextMenuCloseKey = ctrl.contextmenu.on('close', () => {
                    for (let cnt = 0; addedMenuItems && cnt < addedMenuItems.length; cnt++) {
                        ctrl.contextmenu.pop();
                    }
                    if (addedMenuItems) {
                        addedMenuItems.length = 0;
                    }
                });

                ctrl.map.addControl(ctrl.contextmenu);
            },
            layerFilter: function (layer) {
                for (let includedLayer in ctrl.vectorLayers) {
                    if (!ctrl.vectorLayers.hasOwnProperty(includedLayer)) {
                        //The current property is not a direct property of ctrl
                        continue;
                    }
                    if (ctrl.vectorLayers[includedLayer] === layer) {
                        layer.set('name', includedLayer);
                        return true;
                    }
                }
                return false;
            },
            setupEventsAndWatches: function (map) {
                //If you add a handler here, make certain that you add the
                //'un' to remove it in 'onDestroy'
                ctrl.changeSizeKey = map.on('change:size', () => {
                    var center = ctrl.map.getView().getCenter();
                    var zoom = ctrl.map.getView().getZoom();
                    ctrl.openMap();
                    ctrl.map.getView().setZoom(zoom);
                    ctrl.map.getView().setCenter(center);
                });
                ctrl.tileloadstartKey = map.on('tileloadstart', () => {
                    ctrl.addLoading();
                });

                ctrl.tileloadendKey = map.on('tileloadend', () => {
                    ctrl.addLoaded();
                    ctrl.mapLoading();
                });

                ctrl.unregisterSelectedCustomer = $rootScope.$watch('user.ui_data.selectedCustomer', (newVal, oldVal) => {
                    if (oldVal !== newVal) {
                        $state.params.appliance = undefined;
                        $state.params.target = undefined;
                        $state.params.targets = undefined;

                        // Clear out the current vectorSources
                        for (let vectorSource in ctrl.vectorSources) {
                            if (!ctrl.vectorSources.hasOwnProperty(vectorSource)) {
                                //The current property is not a direct property of ctrl
                                continue;
                            }
                            ctrl.vectorSources[vectorSource].clear(false);
                        }

                        ctrl.toggleTargets(true);
                        ctrl.toggleAppliances(true);
                        ctrl.toggleUgsDevices(true);
                        ctrl.loadTargets(ctrl.getCustomer(), true);
                        ctrl.loadAppliances(ctrl.getCustomer());
                        ctrl.loadUGSs();
                    }
                });
            },
            centerMapOnFeature: function (feature, zoom) {
                let coordinates = {
                    coordinate: [Number(feature.position.longitude), Number(feature.position.latitude)],
                };
                ctrl.centerMapOnCoordinates(coordinates, zoom);
            },
            centerMapOnCoordinates: function (coordinates, zoom) {
                let startingPosition;
                if (coordinates.coordinate[0] <= 180 && coordinates.coordinate[0] >= -180) {
                    startingPosition = fromLonLat([Number(coordinates.coordinate[0]), Number(coordinates.coordinate[1])]);
                } else {
                    startingPosition = [Number(coordinates.coordinate[0]), Number(coordinates.coordinate[1])];
                }

                let viewsZoom = ctrl.map.getView().getZoom();
                if (isNaN(zoom) && viewsZoom && viewsZoom === ctrl.defaultCenterOnZoom) {
                    zoom = ctrl.defaultCenterOnZoom * 2;
                }
                if (isNaN(zoom) && viewsZoom && viewsZoom >= ctrl.defaultCenterOnZoom * 2) {
                    zoom = viewsZoom;
                }
                if (isNaN(zoom) && viewsZoom && viewsZoom < ctrl.defaultCenterOnZoom * 2 && viewsZoom > ctrl.defaultCenterOnZoom) {
                    zoom = viewsZoom;
                } else if (isNaN(zoom)) {
                    zoom = ctrl.defaultCenterOnZoom;
                }

                //This is ugly, but it allows this function to match the needs of the ol-contextmenu callbacks

                ctrl.map.getView().animate({ center: startingPosition }, { zoom: zoom });
            },
            addInitialLayers: function (map) {
                map.addLayer(
                    (ctrl.vectorLayers.appliancesVectorLayer = new VectorLayer({
                        // source: new Cluster({
                        //     distance: 40,
                        source: ctrl.vectorSources.appliancesSourceVector,
                        // }),
                        style: ctrl.getStyleForVector(ctrl.vectorSources.appliancesSourceVector, 'appliance', ctrl.map),
                    }))
                );
                map.addLayer(
                    (ctrl.vectorLayers.appliancesHistoryVectorLayer = new VectorLayer({
                        source: ctrl.vectorSources.appliancesHistorySourceVector,
                    }))
                );
                map.addLayer(
                    (ctrl.vectorLayers.appliancesArrowVectorLayer = new VectorLayer({
                        source: ctrl.vectorSources.appliancesArrowSourceVector,
                    }))
                );
                map.addLayer(
                    (ctrl.vectorLayers.focusedAppliancesVectorLayer = new VectorLayer({
                        source: ctrl.vectorSources.focusedAppliancesSourceVector,
                    }))
                );

                map.addLayer(
                    (ctrl.vectorLayers.targetVectorLayer = new VectorLayer({
                        source: new Cluster({
                            distance: 40,
                            source: ctrl.vectorSources.targetSourceVector,
                        }),
                        style: ctrl.getStyleForVector(ctrl.vectorSources.targetSourceVector, 'target', ctrl.map),
                    }))
                );
                map.addLayer(
                    (ctrl.vectorLayers.targetHistoryVectorLayer = new VectorLayer({
                        source: ctrl.vectorSources.targetHistorySourceVector,
                    }))
                );
                map.addLayer(
                    (ctrl.vectorLayers.targeArrowVectorLayer = new VectorLayer({
                        source: ctrl.vectorSources.targetArrowSourceVector,
                    }))
                );
                map.addLayer(
                    (ctrl.vectorLayers.focusedTargetVectorLayer = new VectorLayer({
                        source: ctrl.vectorSources.focusedTargetsSourceVector,
                    }))
                );

                map.addLayer(
                    (ctrl.vectorLayers.ugsVectorLayer = new VectorLayer({
                        source: ctrl.vectorSources.ugsSourceVector,
                    }))
                );
                map.addLayer(
                    (ctrl.vectorLayers.ugsHistoryVectorLayer = new VectorLayer({
                        source: ctrl.vectorSources.ugsHistorySourceVector,
                    }))
                );
                map.addLayer(
                    (ctrl.vectorLayers.ugsArrowVectorLayer = new VectorLayer({
                        source: ctrl.vectorSources.ugsArrowSourceVector,
                    }))
                );
            },
            addLoaded: function () {
                ctrl.tilesLoaded++;
            },
            addLoading: function () {
                ctrl.tilesLoading++;
            },
            addMarker: function (feature, vector, style, standardMarker) {
                var lon = Number(feature.position.longitude);
                var lat = Number(feature.position.latitude);
                if (isNaN(lat) || isNaN(lon)) {
                    console.warn(
                        'Feature "' + feature.value + '" coordinates are invalid. ' + 'Latitude or longitude values were not numbers. feature: ',
                        feature
                    );
                    return;
                }
                let label = feature.name ? feature.name : feature.value;

                var projection = transform([lon, lat], 'EPSG:4326', 'EPSG:3857');

                var iconFeature = new Feature({
                    geometry: new Point(projection),
                    cpe: feature.cpe,
                    has_position_history: feature.has_position_history,
                    historical: feature.historical !== undefined ? feature.historical : false,
                    history: [],
                    arrows: [],
                    id: feature.id,
                    lat: feature.position.latitude,
                    long: feature.position.longitude,
                    tags: feature.tags,
                    type: feature.type,
                    value: label,
                    timestamp: new Date(feature.position.last_observed || feature.position.timestamp),
                    color: feature.color,
                    standardMarker: standardMarker,
                });

                if (style) {
                    iconFeature.setStyle(style);
                }

                vector.addFeature(iconFeature);

                return iconFeature;
            },
            removeMarker: function (feature, vector) {
                if (vector.hasFeature(feature)) {
                    vector.removeFeature(feature);
                }
            },
            addSingleFocusedAppliance: function (setView) {
                ctrl.vectorSources.appliancesSourceVector.clear(true);
                ctrl.vectorSources.focusedAppliancesSourceVector.clear(true);

                Appliances.latestHeartbeats({ applianceId: $state.params.appliance.id }).$promise.then(function (appliance) {
                    var iconStyle = mapsUtils.getStyleForType('appliance', ctrl.map, ctrl.focusedColor);

                    if (!(appliance[$state.params.appliance.id].position.longitude && appliance[$state.params.appliance.id].position.latitude)) {
                        modalService.openErrorResponse('Missing GPS Coordinates', 'This appliance does not have a GPS coordinate to show.', false);
                        return;
                    }
                    let applianceId = $state.params.appliance.id;

                    var feature = {
                        has_position_history: ctrl.gdata.applianceMap[applianceId].has_heartbeats,
                        historical: false,
                        id: applianceId,
                        value: $state.params.appliance.name,
                        position: {
                            longitude: appliance[applianceId].position.longitude,
                            latitude: appliance[applianceId].position.latitude,
                            timestamp: appliance[applianceId].timestamp,
                        },
                        type: 'appliance',
                    };

                    let marker = ctrl.addMarker(feature, ctrl.vectorSources.focusedAppliancesSourceVector, iconStyle, false);
                    marker.set('features', [marker]);

                    ctrl.centerMapOnFeature(feature);
                });
            },
            addSingleFocusedTarget: function (setView) {
                ctrl.vectorSources.targetSourceVector.clear(true);
                ctrl.vectorSources.focusedTargetsSourceVector.clear(true);

                //JAC: Come back and ask Ben for a direct way to retrieve position data given a target
                Targets.basicList({ id__in: $state.params.target.id, detail: 'position', include_tags: true }).$promise.then(function (loc_targets) {
                    let feature = loc_targets[0];
                    var iconStyle = mapsUtils.getStyleForTarget(feature, ctrl.map, ctrl.focusedColor);

                    if (!(feature.position.longitude && feature.position.latitude)) {
                        modalService.openErrorResponse('Missing GPS Coordinates', 'This target does not have a GPS coordinate to show.', false);
                        return;
                    }

                    let marker = ctrl.addMarker(feature, ctrl.vectorSources.focusedTargetsSourceVector, iconStyle, false);
                    marker.set('features', [marker]);

                    if (setView || setView === undefined) {
                        ctrl.centerMapOnFeature(feature, 10);
                    }
                });
            },
            addMultipleFocusedTargets: function (setView) {
                ctrl.vectorSources.targetSourceVector.clear(true);
                ctrl.vectorSources.focusedTargetsSourceVector.clear(true);

                var targets = $state.params.targets;
                Targets.basicListIds({ detail: 'position', include_tags: true }, { ids: targets }).$promise.then(function (loc_targets) {
                    var coordsArray = [];
                    var missingPositions = 0;

                    _.forEach(loc_targets, function (target, index, arr) {
                        if (!target.position) {
                            missingPositions++;
                            return;
                        }

                        var targetPosition = target.position;
                        coordsArray.push(transform([Number(targetPosition.longitude), Number(targetPosition.latitude)], 'EPSG:4326', 'EPSG:3857'));

                        var iconStyle = mapsUtils.getStyleForTarget(target, ctrl.map, ctrl.focusedColor);

                        let marker = ctrl.addMarker(target, ctrl.vectorSources.focusedTargetsSourceVector, iconStyle, false);
                        marker.set('features', [marker]);
                    });

                    if (missingPositions === 1) {
                        modalService.openErrorResponse(
                            'Missing GPS Coordinates',
                            'There is ' + missingPositions + ' target without GPS coordinates.',
                            false
                        );
                    } else if (missingPositions > 1) {
                        modalService.openErrorResponse(
                            'Missing GPS Coordinates',
                            'There are ' + missingPositions + ' targets without GPS coordinates.',
                            false
                        );
                    }

                    if (coordsArray.length === 0) {
                        return;
                    }

                    if (setView || setView === undefined) {
                        var targetExtent = boundingExtent(coordsArray);
                        ctrl.map.getView().fit(targetExtent);
                        ctrl.map.getView().animate({ center: getCenter(targetExtent) });
                    }
                });
            },
            calculateClusterInfo: function (resolution, vector) {
                ctrl.maxFeatureCount = 0;
                const features = vector.getFeatures();
                let feature, radius;
                for (var i = features.length - 1; i >= 0; --i) {
                    feature = features[i];
                    const originalFeatures = feature.get('features');
                    const extent = createEmpty();
                    let j = undefined,
                        jj = undefined;
                    if (originalFeatures) {
                        for (j = 0, jj = originalFeatures.length; j < jj; ++j) {
                            extend(extent, originalFeatures[j].getGeometry().getExtent());
                        }
                    }
                    ctrl.maxFeatureCount = Math.max(ctrl.maxFeatureCount, jj);
                    radius = (0.25 * (getWidth(extent) + getHeight(extent))) / resolution;
                    feature.set('radius', radius);
                }
            },

            historyArrowsStyleFunction: function (feature, color) {
                var geometry;
                if (feature.getGeometry) {
                    geometry = feature.getGeometry();
                } else {
                    geometry = feature;
                }
                var styles = [
                    // linestring
                    new Style({
                        stroke: new Stroke({
                            color: color,
                            width: 4,
                        }),
                    }),
                ];

                geometry.forEachSegment((start, end) => {
                    var dx = end[0] - start[0];
                    var dy = end[1] - start[1];
                    var rotation = Math.atan2(dy, dx);
                    // arrows
                    styles.push(
                        new Style({
                            geometry: new Point(end),
                            image: new Icon({
                                src: '/img/arrow.png',
                                anchor: [0.75, 0.5],
                                rotateWithView: true,
                                rotation: -rotation,
                                color: color,
                                scale: 1.25,
                            }),
                        })
                    );
                });

                return styles;
            },

            selectStyleFunction: function (feature) {
                const styles = [
                    new Style({
                        image: new CircleStyle({
                            radius: feature.get('radius'),
                            fill: invisibleFill,
                        }),
                    }),
                ];
                const originalFeatures = feature.get('features');
                let originalFeature;
                for (let i = originalFeatures.length - 1; i >= 0; --i) {
                    originalFeature = originalFeatures[i];
                    styles.push(mapsUtils.getStyleForType(feature.get('type'), ctrl.map));
                }
                return styles;
            },
            getStyleForVector: function (vector, map) {
                return (feature, resolution) => {
                    if (resolution != ctrl.currentResolution) {
                        ctrl.calculateClusterInfo(resolution, vector);
                        ctrl.currentResolution = resolution;
                    }
                    let style;
                    const features = feature.get('features');
                    const numFeatures = features.length;
                    if (numFeatures > 1) {
                        style = ctrl.getStyleForCluster(features);
                    } else {
                        const originalFeature = features[0];
                        style = mapsUtils.getStyleForType(originalFeature.get('type'), map);
                    }
                    return style;
                };
            },
            getStyleForCluster: function (features) {
                let style;
                const mapsUtilsStyle = mapsUtils.getStylePropertiesForType(ctrl.primaryTypeForCluster(features));
                style = new Style({
                    ...mapsUtilsStyle,
                    text: new Text({
                        text: features.length.toString(),
                        fill: textFill,
                        stroke: textStroke,
                        offsetY: 25,
                        scale: 1.5,
                    }),
                });

                return style;
            },
            primaryTypeForCluster: function (features) {
                let mostSignificantFeature = {
                    type: '',
                    count: 0,
                };
                let featureMap = {};
                for (let cnt = 0; cnt < features.length; cnt++) {
                    let feature = features[cnt];
                    if (featureMap[feature.get('type')]) {
                        featureMap[feature.get('type')]++;
                    } else {
                        featureMap[feature.get('type')] = 1;
                    }
                }

                for (let feature in featureMap) {
                    if (!featureMap.hasOwnProperty(feature)) {
                        //The current property is not a direct property of ctrl
                        continue;
                    } else if (featureMap[feature] > mostSignificantFeature.count) {
                        mostSignificantFeature = {
                            type: feature,
                            count: featureMap[feature],
                        };
                    }
                }

                return mostSignificantFeature.type;
            },
            getPrimaryFeature: function (features) {
                let primaryType = ctrl.primaryTypeForCluster(features);
                for (let subFeature = 0; subFeature < features.length; subFeature++) {
                    if (features[subFeature].get('type') === primaryType) {
                        return features[subFeature];
                    }
                }
            },
            selectedStyleFunction: function (feature, resolution) {
                let style;
                let features;

                if (feature && (features = feature.get('features')) && features.length > 1) {
                    style = ctrl.getStyleForCluster(features);
                } else if (features[0] && features[0]) {
                    style = features[0].getStyle();
                }
                return style;
            },
            generateMenu: function (initFeatures, addLabels) {
                //JAC: Attempting to reuse addedLabel code in conjuction with addLabels
                let addedLabel = !addLabels;

                let devices = {};
                let menuItems: any = [];

                let types = {};
                let features = [];

                let typesComparator = function (a, b) {
                    if (a.get('value') === b.get('value')) {
                        return 0;
                    } else if (a.get('value') > b.get('value')) {
                        return 1;
                    } else {
                        return -1;
                    }
                };

                for (let cnt = 0; cnt < initFeatures.length; cnt++) {
                    let feature = initFeatures[cnt];
                    let type = feature.get('type');
                    if (!types[type]) {
                        types[type] = [];
                    }
                    types[type].push(feature);
                }

                for (let type in types) {
                    if (!types.hasOwnProperty(type)) {
                        //The current property is not a direct property of ctrl
                        continue;
                    }
                    features = features.concat(types[type].sort(typesComparator));
                }

                //Build a map of the features contained in the cluster
                for (let cnt = 0; cnt < features.length; cnt++) {
                    let feature = features[cnt];
                    if (!devices[feature.get('id')] && feature.get('has_position_history')) {
                        if (!addedLabel) {
                            addedLabel = true;
                            menuItems.push('-');
                            menuItems.push({
                                text: 'Available Histories',
                                classname: 'center-align no-highlight', // add some CSS rules
                            });
                            if (ctrl.map.getView().getZoom() < ctrl.maxZoom) {
                                menuItems.push({
                                    text: '(Zoom In To Narrow Scope)',
                                    classname: 'center-align no-highlight smaller-menu-font', // add some CSS rules
                                });
                            }
                        }
                        devices[feature.get('id')] = feature;
                        let featureType = feature.get('type');
                        let layer;

                        let isUGS = feature.get('type') === 'ugs';
                        let icon = !isUGS ? TargetService.getTargetTypeIconUnicodeValue(feature.get('type')) + ' ' : '';
                        let historyAlreadySelected = ctrl.featuresWithShownHistory.indexOf(feature) !== -1;

                        menuItems.push({
                            text: icon + ctrl.escapeHtml(String(feature.get('value')).toUpperCase()),
                            // add some CSS rules
                            classname:
                                (isUGS ? 'maps-icons-' + feature.get('type') + ' maps-left-5 maps-text-indent-15' : ' maps-text-indent-0') +
                                ' maps-monospace ol-ctx-menu-icon' +
                                ' fa-with-defaults' +
                                (historyAlreadySelected ? ' highlight-device' : ''),
                            style: historyAlreadySelected ? 'background-color: ' + feature.get('color') + ' !important;' : '',
                            callback: () => {
                                switch (featureType) {
                                    case 'appliance':
                                        layer = ctrl.vectorLayers.appliancesHistoryVectorLayer;
                                        break;
                                    case 'ugs':
                                        layer = ctrl.vectorLayers.ugsHistoryVectorLayer;
                                        break;
                                    default:
                                        layer = ctrl.vectorLayers.targetHistoryVectorLayer;
                                }
                                ctrl.showFeatureHistory(feature, layer, false);
                            },
                        });
                    }
                }

                return menuItems;
            },
            generateTooltipHTML: function (feature, multiple) {
                let value = ctrl.escapeHtml(feature.get('value'));
                let cpe = feature.get('cpe');
                let type = feature.get('type');
                let tags = feature.get('tags');
                let lat = feature.get('lat');
                let long = feature.get('long');
                let timestamp = feature.get('timestamp');

                var content = '<div>';
                content += '<h4 style="margin-bottom: 0px">' + value + '</h4>';
                if (type !== 'ugs') {
                    content += '<div>Type: ' + mapsUtils.titleCase(type) + '</div>';
                } else {
                    content += '<div>Type: UGS</div>';
                }
                if (timestamp) {
                    content += '<div>Last Observed: ' + timestamp + '</div>';
                }
                if (cpe) {
                    content += '<div>';
                    var cpeTitle = ctrl.getCPETitle(cpe);

                    var cpeType = TargetService.getCPEType(cpeTitle);
                    var cpeIcon = TargetService.getCPEIconByType(cpeType);

                    content += '<i aria-hidden="true" class="fa ' + cpeIcon + '" ng-show="target.cpeTitle"></i> ' + cpeType;
                    content += '</div>';
                }

                if (tags && tags.length > 0) {
                    content += '<div>';
                    content += 'Tags: ';
                    for (var cnt = 0; cnt < tags.length && cnt < 5; cnt++) {
                        content += ctrl.escapeHtml(ctrl.gdata.targetTagMap[tags[cnt]].value);
                        if (cnt < tags.length - 1) {
                            content += ', ';
                        }
                    }
                    if (tags.length > 5) {
                        content += '...';
                    }
                    content += '</div>';
                }
                content += '<div>';
                content += 'Lat: ' + lat + ', Long: ' + long;
                content += '</div>';

                content += '</div>' + (multiple ? 'Click to see more...' : '');

                return content;
            },
            getCPETitle: function (cpeName) {
                if (cpeName === undefined || cpeName.length < 1) return;

                var cpeEntry: any = _.find(ctrl.cpeOptions, { name: cpeName });
                if (cpeEntry) {
                    return cpeEntry.title;
                }
                return cpeName;
            },
            loadTargets: function (customer, showOnlyActive) {
                var managedCustomer = _.get($rootScope.user, 'ui_data.selectedCustomer');
                if (managedCustomer === null) {
                    if (ctrl.showingCustomerslabel && !$state.params.managedCustomerEmpty) {
                        ctrl.showingCustomerslabel.style.display = 'none';
                    }
                    ctrl.vectorSources.targetSourceVector.clear(false);
                    ctrl.vectorSources.focusedTargetsSourceVector.clear(false);
                    $state.params.customer = undefined;
                    customer = undefined;
                } else if (ctrl.showingCustomerslabel && $state.params.managedCustomerEmpty) {
                    ctrl.showingCustomerslabel.style.display = 'none';
                }

                ctrl.vectorSources.targetSourceVector.clear(false);
                ctrl.vectorSources.targetArrowSourceVector.clear(false);
                ctrl.vectorSources.targetHistorySourceVector.clear(false);

                if (showOnlyActive) {
                    ctrl.inactiveTargetsButton.style.backgroundColor = ctrl.defaultButtonColor;
                } else {
                    ctrl.inactiveTargetsButton.style.backgroundColor = 'green';
                }

                if (!customer) {
                    return;
                }

                if (ctrl.mapsBusy === null) {
                    ctrl.mapsBusy = $q.defer();
                }
                Targets.get_ais_targets({
                    customerId: customer.id,
                    active: showOnlyActive,
                    detail: 'position',
                    include_tags: true,
                }).$promise.then(function (ais_targets) {
                    // //console.log("retrieved ais targets");
                    _.forEach(ais_targets, function (target, index, arr) {
                        if (!target.position) {
                            return;
                        }

                        ctrl.addMarker(target, ctrl.vectorSources.targetSourceVector, mapsUtils.getStyleForTarget(target, ctrl.map), true);
                    });

                    if (ctrl.mapsBusy) {
                        ctrl.mapsBusy.resolve(true);
                        ctrl.mapsBusy = null;
                    }
                });

                Targets.basicList({
                    customer: customer.id,
                    type: 'location',
                    active: showOnlyActive,
                    include_tags: true,
                }).$promise.then(function (loc_targets) {
                    _.forEach(loc_targets, function (target, index, arr) {
                        let value = target.value;
                        const [_latitude, _longitude, ..._name] = value.split(',');
                        target.position = {
                            latitude: _latitude,
                            longitude: _longitude,
                        };
                        if (_name) {
                            target.name = _name.join(',');
                        }

                        ctrl.addMarker(target, ctrl.vectorSources.targetSourceVector, mapsUtils.getStyleForTarget(target, ctrl.map), true);
                    });

                    if (ctrl.mapsBusy) {
                        ctrl.mapsBusy.resolve(true);
                        ctrl.mapsBusy = null;
                    }
                });
            },
            mapLoading: function () {
                if (ctrl.tilesLoading === ctrl.tilesLoaded) {
                    ctrl.tilesLoading = 0;
                    ctrl.tilesLoaded = 0;
                    return false;
                } else {
                    return true;
                }
            },
            openMap: function () {
                var view = ctrl.map.getView();
                var extent = ctrl.origExtent;

                var size = ctrl.map.getSize();
                //Prevent the code from moving forward if size is undefined
                if (size === undefined) {
                    return;
                }

                // fit the extent horizontally or vertically depending on screen size
                // to determine the maximum resolution possible
                var width = getWidth(extent);
                var height = getHeight(extent);
                if (size[0] / size[1] > width / height) {
                    view.fit([extent[0], (extent[1] + extent[3]) / 2, extent[2], (extent[1] + extent[3]) / 2], { constrainResolution: false });
                } else {
                    view.fit([(extent[0] + extent[2]) / 2, extent[1], (extent[0] + extent[2]) / 2, extent[3]], { constrainResolution: false });
                }

                ctrl.maxResolution = ctrl.map.getView().getResolution();
                ctrl.resChange(true);
            },
            resChange: function (initialize) {
                var oldView = ctrl.map.getView();
                var extent = ctrl.origExtent;

                if (initialize === true || oldView.getZoom() % 1 == 0) {
                    var resolution = oldView.getResolution();
                    var width = ctrl.map.getSize()[0] * resolution;
                    var height = ctrl.map.getSize()[1] * resolution;
                    var newView = new View({
                        projection: oldView.getProjection(),
                        extent: [extent[0] + width / 2, extent[1] + height / 2, extent[2] - width / 2, extent[3] - height / 2],
                        resolution: oldView.getResolution(),
                        maxResolution: initialize === true ? ctrl.maxResolution : oldView.getMaxResolution(),
                        minResolution: initialize === true ? undefined : oldView.getMinResolution(),
                        maxZoom: oldView.getMaxZoom(),
                        rotation: oldView.getRotation(),
                    });

                    newView.setCenter(newView.constrainCenter(oldView.getCenter()));

                    newView.on('change:resolution', ctrl.resChange);
                    ctrl.map.setView(newView);
                }
            },
            showFeatureHistory: function (feature, layer) {
                let features;
                let type;
                let arrowSourceVector;

                if ((features = feature.get('features'))) {
                    feature = ctrl.getPrimaryFeature(features);
                }

                let history = feature.get('history');
                let arrows = feature.get('arrows');
                if (history.length === 0) {
                    ctrl.getHistoryPositions(feature, layer, history, arrows);
                } else {
                    type = feature.get('type');
                    switch (type) {
                        case 'appliance':
                            arrowSourceVector = ctrl.vectorSources.appliancesArrowSourceVector;
                            break;
                        case 'ugs':
                            arrowSourceVector = ctrl.vectorSources.ugsArrowSourceVector;
                            break;
                        default:
                            arrowSourceVector = ctrl.vectorSources.targetArrowSourceVector;
                    }

                    history.forEach(function (feature) {
                        ctrl.removeMarker(feature, layer.getSource());
                    });
                    arrows.forEach(function (arrow) {
                        ctrl.removeMarker(arrow, arrowSourceVector);
                    });

                    history.length = 0;
                    arrows.length = 0;
                    let featureIndex = ctrl.featuresWithShownHistory.indexOf(feature);
                    if (featureIndex !== -1) {
                        ctrl.featuresWithShownHistory.splice(featureIndex, 1);
                    }
                }
            },
            getCustomer: function () {
                var customer;
                var managedCustomer = _.get($rootScope.user, 'ui_data.selectedCustomer');
                if ($state.params.customer && (!managedCustomer || $state.params.customer.id !== managedCustomer.id)) {
                    customer = $state.params.customer;
                } else if (globalDataService.isOperator) {
                    customer = managedCustomer;
                } else {
                    customer = ctrl.gdata.customer;
                }
                return customer;
            },
            getHistoryPositions: function (feature, layer, history, arrows) {
                let type = feature.get('type');

                if (!(feature || type)) {
                    console.error("Entered getHistoryPositions without either feature or feature.get('type')");
                    return;
                }

                switch (type) {
                    case 'appliance':
                        ctrl.getHistoryAppliancePositions(feature, layer, history, arrows);
                        break;
                    case 'ugs':
                        ctrl.getHistoryUGSPositions(feature, layer, history, arrows);
                        break;
                    default:
                        ctrl.getHistoryTargetPositions(feature, layer, history, arrows);
                }
            },
            getHistoryAppliancePositions: function (feature, layer, history, arrows) {
                let lineStringArray = [];
                if (ctrl.mapsBusy === null) {
                    ctrl.mapsBusy = $q.defer();
                }
                Appliances.lastCountHeartbeats({ applianceId: feature.get('id') }).$promise.then((heartbeats) => {
                    // //console.log("retrieved ais targets");

                    let color = mapsUtils.getRandomColor();
                    heartbeats.forEach((heartbeat) => {
                        //LOOK AT THE NEXT COMMENT
                        // let coordinates = undefined;
                        // let matchingFeatures = undefined;
                        // let matchingFeature = undefined;
                        // let matchingPixel = undefined;
                        // let projection = undefined;

                        if (heartbeat.position.longitude === 0 && heartbeat.position.latitude === 0) {
                            return;
                        }

                        feature.set('color', color);

                        //FAILED ATTEMPT TO ASSIGN COLOR TO MATCHING STANDARDMARKER
                        //matchingFeatures never contains the matching standard marker
                        // coordinates = [
                        //     Number(heartbeat.position.longitude),
                        //     Number(heartbeat.position.latitude)
                        // ]
                        // projection = ol.proj.transform(coordinates, 'EPSG:4326','EPSG:3857');
                        // matchingPixel = ctrl.map.getPixelFromCoordinate(projection);
                        // matchingFeatures = ctrl.map.getFeaturesAtPixel(matchingPixel, {
                        //     hitTolerance: ctrl.hitTolerance,
                        //     layerFilter: ctrl.layerFilter
                        // });
                        // for (let cnt = 0; matchingFeatures && cnt < matchingFeatures.length; cnt++) {
                        //     matchingFeature = matchingFeatures[cnt];
                        //     if (matchingFeature.get('standardMarker')) {
                        //         matchingFeature.set("color", color);
                        //         break;
                        //     }
                        // }

                        let opacity = 0.5;
                        if (
                            Number(heartbeat.position.latitude) === Number(feature.get('lat')) &&
                            Number(heartbeat.position.longitude) === Number(feature.get('long'))
                        ) {
                            opacity = 1;
                        }

                        heartbeat.position.timestamp = heartbeat.timestamp;
                        lineStringArray.push(
                            transform([Number(heartbeat.position.longitude), Number(heartbeat.position.latitude)], 'EPSG:4326', 'EPSG:3857')
                        );

                        let rebuiltFeature = { ...feature.values_, position: heartbeat.position, historical: true };
                        let historyFeature = ctrl.addMarker(
                            rebuiltFeature,
                            layer.getSource(),
                            mapsUtils.getStyleForTarget(rebuiltFeature, ctrl.map, color, undefined, opacity),
                            false
                        );
                        history.push(historyFeature);

                        //THIS GOES WITH THE PREVIOUS COMMENT
                        // if (matchingFeature !== undefined) {
                        //     matchingFeature.set('history', history);
                        // }
                    });
                    let linestring = new LineString(lineStringArray);
                    let historyArrowFeature = new Feature({ geometry: linestring });
                    historyArrowFeature.setStyle(ctrl.historyArrowsStyleFunction(linestring, color));
                    ctrl.vectorSources.appliancesArrowSourceVector.addFeature(historyArrowFeature);
                    arrows.push(historyArrowFeature);

                    ctrl.toggleAppliancesHistoryLayers(true);

                    if (ctrl.mapsBusy) {
                        ctrl.mapsBusy.resolve(true);
                        ctrl.mapsBusy = null;
                    }
                    ctrl.featuresWithShownHistory.push(feature);
                });
            },
            getHistoryTargetPositions: function (feature, layer, history, arrows) {
                let lineStringArray = [];
                if (ctrl.mapsBusy === null) {
                    ctrl.mapsBusy = $q.defer();
                }
                Targets.position_history({ targetId: feature.get('id') }).$promise.then((targets) => {
                    // //console.log("retrieved ais targets");

                    let color = mapsUtils.getRandomColor();
                    targets.forEach((position) => {
                        let opacity = 0.5;
                        if (Number(position.latitude) === Number(feature.get('lat')) && Number(position.longitude) === Number(feature.get('long'))) {
                            opacity = 1;
                        }

                        lineStringArray.push(transform([Number(position.longitude), Number(position.latitude)], 'EPSG:4326', 'EPSG:3857'));

                        feature.set('color', color);
                        let rebuiltFeature = { ...feature.values_, position: position, historical: true };
                        let historyFeature = ctrl.addMarker(
                            rebuiltFeature,
                            layer.getSource(),
                            mapsUtils.getStyleForTarget(rebuiltFeature, ctrl.map, color, undefined, opacity),
                            false
                        );
                        history.push(historyFeature);
                    });
                    let linestring = new LineString(lineStringArray.reverse());
                    let historyArrowFeature = new Feature({ geometry: linestring });
                    historyArrowFeature.setStyle(ctrl.historyArrowsStyleFunction(linestring, color));
                    ctrl.vectorSources.targetArrowSourceVector.addFeature(historyArrowFeature);
                    arrows.push(historyArrowFeature);

                    ctrl.toggleTargetsHistoryLayers(true);

                    if (ctrl.mapsBusy) {
                        ctrl.mapsBusy.resolve(true);
                        ctrl.mapsBusy = null;
                    }
                    ctrl.featuresWithShownHistory.push(feature);
                });
            },
            getHistoryUGSPositions: function (feature, layer, history, arrows) {
                let lineStringArray = [];
                if (ctrl.mapsBusy === null) {
                    ctrl.mapsBusy = $q.defer();
                }
                UGSs.positionHistory({ id: feature.get('id') }).$promise.then((ugss) => {
                    let color = mapsUtils.getRandomColor();
                    ugss.forEach((position) => {
                        let opacity = 0.5;
                        if (
                            Number(position.position.latitude) === Number(feature.get('lat')) &&
                            Number(position.position.longitude) === Number(feature.get('long'))
                        ) {
                            opacity = 1;
                        }

                        if (Number(position.position.latitude) === 0 && Number(position.position.longitude) === 0) {
                            return;
                        }

                        position.position.timestamp = position.added;

                        lineStringArray.push(
                            transform([Number(position.position.longitude), Number(position.position.latitude)], 'EPSG:4326', 'EPSG:3857')
                        );

                        feature.set('color', color);
                        let rebuiltFeature = { ...feature.values_, position: position.position, historical: true };
                        let historyFeature = ctrl.addMarker(
                            rebuiltFeature,
                            layer.getSource(),
                            mapsUtils.getStyleForTarget(rebuiltFeature, ctrl.map, color, undefined, opacity),
                            false
                        );
                        history.push(historyFeature);
                    });
                    let linestring = new LineString(lineStringArray.reverse());
                    let historyArrowFeature = new Feature({ geometry: linestring });
                    historyArrowFeature.setStyle(ctrl.historyArrowsStyleFunction(linestring, color));
                    ctrl.vectorSources.ugsArrowSourceVector.addFeature(historyArrowFeature);
                    arrows.push(historyArrowFeature);

                    if (ctrl.mapsBusy) {
                        ctrl.mapsBusy.resolve(true);
                        ctrl.mapsBusy = null;
                    }
                    ctrl.featuresWithShownHistory.push(feature);
                });
            },
            setupTooltip: function () {
                var tooltip = document.getElementById('tooltip');
                var overlay = new Overlay({
                    element: tooltip,
                    offset: [10, 0],
                    positioning: 'bottom-left',
                });
                ctrl.map.addOverlay(overlay);

                function displayTooltip(evt) {
                    if (ctrl.mapLoading()) {
                        return;
                    }
                    let pixel = evt.pixel;
                    let feature = ctrl.map.forEachFeatureAtPixel(
                        pixel,
                        function (feature) {
                            return feature;
                        },
                        {
                            hitTolerance: 4,
                            layerFilter: (layer) => {
                                let layerName = layer.get('name');
                                return ctrl.vectorLayers[layerName] && layerName.indexOf('Arrow') === -1;
                            },
                        }
                    );
                    tooltip.style.display = feature ? '' : 'none';
                    if (feature) {
                        let subFeature = 0;
                        let features;
                        overlay.setPosition(evt.coordinate);
                        let value = feature.get('value');
                        if (!value && (features = feature.get('features'))) {
                            let primaryType = ctrl.primaryTypeForCluster(features);
                            for (; subFeature < features.length; subFeature++) {
                                if (features[subFeature].get('type') === primaryType) {
                                    value = features[subFeature].get('value');
                                    break;
                                }
                            }
                        }
                        if (!value) {
                            tooltip.style.display = 'none';
                            return;
                        } else if (features && features.length > 1) {
                            tooltip.innerHTML = ctrl.generateTooltipHTML(features[subFeature], true);
                        } else if (features) {
                            tooltip.innerHTML = ctrl.generateTooltipHTML(features[0], false);
                        } else {
                            tooltip.innerHTML = ctrl.generateTooltipHTML(feature, false);
                        }
                    }
                }

                ctrl.map.on('pointermove', displayTooltip);
            },
            hasApplianceMoved: function (heartbeat, history) {
                // There was no prior heartbeat stored, so it's changed.
                if (history.length === 0) {
                    return true;
                }
                let priorApplianceHeartBeat = history[history.length - 1];
                if (priorApplianceHeartBeat) {
                    if (
                        priorApplianceHeartBeat.position.latitude !== heartbeat.position.latitude ||
                        priorApplianceHeartBeat.position.longitude !== heartbeat.position.longitude
                    ) {
                        return true;
                    }
                }
                return false;
            },

            loadAppliances: function (customer) {
                ctrl.vectorSources.appliancesSourceVector.clear(true);
                ctrl.vectorSources.focusedAppliancesSourceVector.clear(true);

                if (!customer) {
                    return;
                }

                ctrl.gdata.appliances.forEach((appliance) => {
                    Appliances.latestHeartbeats(
                        {
                            applianceId: appliance.id,
                            customer: customer.id,
                        },
                        {},
                        function (heartbeats) {
                            for (let heartbeatProp in heartbeats) {
                                if (!heartbeats.hasOwnProperty(heartbeatProp)) {
                                    //The current property is not a direct property of ctrl
                                    continue;
                                }
                                let heartbeat = heartbeats[heartbeatProp];
                                if (
                                    heartbeat.position &&
                                    Object.keys(heartbeat.position).length > 0 &&
                                    heartbeat.position.latitude !== 0 &&
                                    heartbeat.position.longitude !== 0
                                ) {
                                    let position = {
                                        longitude: heartbeat.position.longitude,
                                        latitude: heartbeat.position.latitude,
                                        timestamp: heartbeat.timestamp,
                                    };

                                    let feature = {
                                        has_position_history: ctrl.gdata.applianceMap[heartbeat.appliance].has_heartbeats,
                                        historical: false,
                                        id: heartbeat.appliance,
                                        position: position,
                                        type: 'appliance',
                                        value: ctrl.gdata.applianceMap[heartbeat.appliance].name,
                                    };

                                    ctrl.addMarker(
                                        feature,
                                        ctrl.vectorSources.appliancesSourceVector,
                                        mapsUtils.getStyleForTarget(feature, ctrl.map),
                                        true
                                    );
                                }
                            }
                        }
                    );
                });
            },
            loadUGSs: function () {
                UGSs.list(function (ugss) {
                    ugss.forEach((ugs) => {
                        if (ugs.position_detail && ugs.position_detail.converted_position) {
                            const feature = {
                                value: ugs.hardware_address,
                                position: ugs.position_detail.converted_position,
                                type: 'ugs',
                                has_position_history: ugs.position_detail.has_position_history,
                                id: ugs.id,
                            };

                            feature.position.timestamp = ugs.last_callback;

                            ctrl.addMarker(feature, ctrl.vectorSources.ugsSourceVector, mapsUtils.getStyleForType('ugs', ctrl.map), true);
                        }
                    });
                });
            },

            toggleInactiveTargets: function (override) {
                //Need to send undefined instead of false due to logic
                //needed to show all targets
                if (override === true) {
                    ctrl.showOnlyActiveTargets = true;
                } else if (ctrl.showOnlyActiveTargets === true) {
                    ctrl.showOnlyActiveTargets = undefined;
                } else {
                    ctrl.showOnlyActiveTargets = true;
                }

                ctrl.loadTargets(ctrl.getCustomer(), ctrl.showOnlyActiveTargets);

                if ($state.params.appliance) {
                    ctrl.addSingleFocusedAppliance(false);
                } else if ($state.params.target) {
                    ctrl.addSingleFocusedTarget(false);
                } else if ($state.params.targets) {
                    ctrl.addMultipleFocusedTargets(false);
                }
            },

            toggleTargets: function (override) {
                ctrl.showingTargets = typeof override === 'boolean' ? override : !ctrl.showingTargets;
                ctrl.vectorLayers.targetVectorLayer.setVisible(ctrl.showingTargets);
                ctrl.vectorLayers.targetHistoryVectorLayer.setVisible(ctrl.showingTargets);
                ctrl.vectorLayers.targeArrowVectorLayer.setVisible(ctrl.showingTargets);

                if (ctrl.showingTargets) {
                    ctrl.inactiveTargetsButton.style.display = 'block';
                } else {
                    ctrl.inactiveTargetsButton.style.display = 'none';
                }

                if (ctrl.showingTargets) {
                    ctrl.targetsButton.style.backgroundColor = 'green';
                } else {
                    ctrl.targetsButton.style.backgroundColor = ctrl.defaultButtonColor;
                }
            },
            toggleTargetsHistoryLayers: function (override) {
                ctrl.showingTargetsHistory = typeof override === 'boolean' ? override : !ctrl.showingTargetsHistory;
                ctrl.vectorLayers.targetHistoryVectorLayer.setVisible(ctrl.showingTargetsHistory);
                ctrl.vectorLayers.targeArrowVectorLayer.setVisible(ctrl.showingTargetsHistory);
            },
            toggleAppliances: function (override) {
                ctrl.showingAppliances = typeof override === 'boolean' ? override : !ctrl.showingAppliances;
                ctrl.vectorLayers.appliancesVectorLayer.setVisible(ctrl.showingAppliances);
                ctrl.vectorLayers.appliancesHistoryVectorLayer.setVisible(ctrl.showingAppliances);
                ctrl.vectorLayers.appliancesArrowVectorLayer.setVisible(ctrl.showingAppliances);

                if (ctrl.showingAppliances) {
                    ctrl.appliancesButton.style.backgroundColor = 'green';
                } else {
                    ctrl.appliancesButton.style.backgroundColor = ctrl.defaultButtonColor;
                }
            },
            toggleAppliancesHistoryLayers: function (override) {
                ctrl.showingAppliancesHistory = typeof override === 'boolean' ? override : !ctrl.showingAppliancesHistory;
                ctrl.vectorLayers.appliancesHistoryVectorLayer.setVisible(ctrl.showingAppliancesHistory);
                ctrl.vectorLayers.appliancesArrowVectorLayer.setVisible(ctrl.showingAppliancesHistory);
            },
            toggleUgsDevices: function (override) {
                ctrl.showingUgsDevices = typeof override === 'boolean' ? override : !ctrl.showingUgsDevices;
                ctrl.vectorLayers.ugsVectorLayer.setVisible(ctrl.showingUgsDevices);
                ctrl.vectorLayers.ugsHistoryVectorLayer.setVisible(ctrl.showingUgsDevices);
                ctrl.vectorLayers.ugsArrowVectorLayer.setVisible(ctrl.showingUgsDevices);

                if (ctrl.showingUgsDevices) {
                    ctrl.ugsButton.style.backgroundColor = 'green';
                } else {
                    ctrl.ugsButton.style.backgroundColor = ctrl.defaultButtonColor;
                }
            },
            toggleUGSHistoryLayers: function (override) {
                ctrl.showingUGSHistory = typeof override === 'boolean' ? override : !ctrl.showingUGSHistory;
                ctrl.vectorLayers.ugsHistoryVectorLayer.setVisible(ctrl.showingUGSHistory);
                ctrl.vectorLayers.ugsArrowVectorLayer.setVisible(ctrl.showingUGSHistory);
            },
            escapeHtml: function (str) {
                // From http://shebang.brandonmintern.com/foolproof-html-escaping-in-javascript/
                const div = document.createElement('div');
                div.appendChild(document.createTextNode(str));
                return div.innerHTML;
            },
            ShowingCustomerLabel: (function (control) {
                function createLabel() {
                    ctrl.showingCustomerslabel = document.createElement('div');
                    ctrl.showingCustomerslabel.className = 'showing-customer-label';
                    ctrl.showingCustomerslabel.innerHTML =
                        'Viewing ' + $rootScope.catotheme.customerCapitalize + ': ' + mapsUtils.titleCase(ctrl.getCustomer().name);

                    control.call(this, {
                        element: ctrl.showingCustomerslabel,
                    });
                }

                createLabel.__proto__ = control;
                createLabel.prototype = Object.create(control && control.prototype);
                createLabel.prototype.constructor = createLabel;

                return createLabel;
            })(Control),
            Toggle: (function (control) {
                function createToggle() {
                    ctrl.appliancesButton = document.createElement('button');
                    ctrl.appliancesButton.innerHTML = '<i class="fa fa-microchip"></i>';
                    ctrl.appliancesButton.title = 'Toggle Appliances';
                    ctrl.appliancesButton.style.backgroundColor = 'green';

                    ctrl.inactiveTargetsButton = document.createElement('button');
                    ctrl.inactiveTargetsButton.innerHTML = '<i class="fa fa-eye-slash"></i>';
                    ctrl.inactiveTargetsButton.title = 'Toggle Inactive Targets';

                    ctrl.targetsButton = document.createElement('button');
                    ctrl.targetsButton.innerHTML = '<i class="fa fa-location-arrow"></i>';
                    ctrl.targetsButton.title = 'Toggle Targets';
                    ctrl.targetsButton.style.backgroundColor = 'green';

                    ctrl.ugsButton = document.createElement('button');
                    ctrl.ugsButton.className = 'maps-icons-ugs';
                    ctrl.ugsButton.title = 'Toggle UGS Devices';
                    ctrl.ugsButton.style.backgroundColor = 'green';
                    ctrl.ugsButton.style.backgroundPosition = 'center';
                    ctrl.ugsButton.style.backgroundRepeat = 'no-repeat';
                    ctrl.ugsButton.style.backgroundSize = '15px';

                    var element = document.createElement('div');
                    element.className = 'toggle-markers ol-unselectable ol-control';
                    element.appendChild(ctrl.ugsButton);
                    element.appendChild(ctrl.appliancesButton);
                    element.appendChild(ctrl.targetsButton);
                    element.appendChild(ctrl.inactiveTargetsButton);

                    control.call(this, {
                        element: element,
                    });
                    ctrl.appliancesButton.addEventListener('click', ctrl.toggleAppliances, false);
                    ctrl.targetsButton.addEventListener('click', ctrl.toggleTargets, false);
                    ctrl.inactiveTargetsButton.addEventListener('click', ctrl.toggleInactiveTargets, false);
                    ctrl.ugsButton.addEventListener('click', ctrl.toggleUgsDevices, false);
                }

                createToggle.__proto__ = control;
                createToggle.prototype = Object.create(control && control.prototype);
                createToggle.prototype.constructor = createToggle;

                return createToggle;
            })(Control),

            //JAC: Not needed right now, but might be useful in the future
            // wheelZoomHandler: function(evt) {
            //     if (ol.events.condition.shiftKeyOnly(evt) !== true) {
            //         // evt.preventDefault();
            //     }
            // }
        };

        for (let prop in ctrl) {
            if (!ctrl.hasOwnProperty(prop)) {
                //The current property is not a direct property of ctrl
                continue;
            }
            this[prop] = ctrl[prop];
        }

        //Replace the initial ctrl object with the updated this object in order to prevent the two from getting out of sync
        ctrl = this;
    },
});
