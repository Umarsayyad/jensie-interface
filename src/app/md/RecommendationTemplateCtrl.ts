import app from '../app';
import * as _ from 'lodash';
import * as angular from 'angular';

import { renameDuplicate } from '../common/utilities/rename-duplicate';

import '../common/operator/taskParameters';
import '../common/actionsDropdownComp';
import './planning/modalRecommendationTemplate';

app.controller('RecommendationTemplateCtrl', function (
    $scope,
    $rootScope,
    $uibModal,
    FileSaver,
    Blob,
    modalService,
    importExportService,
    Recommendationtemplates
) {
    $scope.data.selectedRecTemplates = [];
    var config;

    $scope.actions = {
        editRecTemplate: {
            action: function (template) {
                $scope.editRecTemplate(template, false);
            },
            class: 'btn-white',
            icon: 'fa-pencil',
            label: 'Edit',
            tooltip: 'Edit Recommendation Template',
        },
        copyRecTemplate: {
            action: function (template) {
                $scope.editRecTemplate(template, true);
            },
            class: 'btn-white',
            icon: 'fa-copy',
            label: 'Duplicate',
            tooltip: 'Duplicate Recommendation Template',
        },
    };

    $scope.validateMinMax = function (data, min, max) {
        if (data.length < min) {
            return 'Value is too short, minimum length: ' + min;
        }
        if (data.length > max) {
            return 'Value is too long, maximum length: ' + max;
        }
    };

    function loadRecommendationTemplates() {
        $scope.data.rectemplates = Recommendationtemplates.list();
    }

    loadRecommendationTemplates();

    $scope.importTemplates = importExportService.importer(Recommendationtemplates.importTemplates, loadRecommendationTemplates);

    $scope.exportTemplates = importExportService.exporter(Recommendationtemplates.exportTemplates);

    $scope.$watch('data.selectedRecTemplates.length', function (newValue, oldValue) {
        $scope.allSelected = newValue !== 0 && newValue === $scope.data.rectemplates.length;
    });

    $scope.deleteRecTemplate = function (template) {
        modalService.openDelete('Confirmation', 'recommendation template <em>' + template.body_text + '</em>', {}, function () {
            template.$delete(function () {
                _.remove($scope.data.rectemplates, { id: template.id });
            }, modalService.openErrorResponseCB('Deleting recommendation template failed'));
        });
    };
    $scope.editRecTemplate = function (recommendationTemplate, copy) {
        if (!config) config = Recommendationtemplates.options();

        if (copy && recommendationTemplate) {
            recommendationTemplate = angular.copy(recommendationTemplate);
            recommendationTemplate.body_text = renameDuplicate(recommendationTemplate.body_text);
            delete recommendationTemplate.id;
            delete recommendationTemplate.uuid;
        }

        var scope = $rootScope.$new(true);
        scope['recommendationTemplate'] = recommendationTemplate;
        scope['recommendationTemplateSaved'] = recommendationTemplateSaved;

        $uibModal.open({
            // Proxy access to the component via an inline template and controller.  Once we update angular-ui-bootstrap, we can load the component directly in the modal
            template:
                '<modal-recommendation-template recommendation-template="recommendationTemplate" on-saved="recommendationTemplateSaved(recommendationTemplate)" config="config" uib-modal-instance="$uibModalInstance"></modal-recommendation-template>',
            controller: function ($scope, $uibModalInstance, config) {
                $scope.$uibModalInstance = $uibModalInstance;
                $scope.config = config;
            },
            size: 'lg',
            scope: scope,
            resolve: {
                config: config.$promise,
            },
        });
    };

    function recommendationTemplateSaved(recommendationTemplate) {
        var curRecommendationTemplate = _.find($scope.data.rectemplates, { id: recommendationTemplate.id });
        if (_.isUndefined(curRecommendationTemplate)) {
            // the recommendation template is new
            $scope.data.rectemplates.push(recommendationTemplate);
        } else {
            $rootScope.updateObjOld(curRecommendationTemplate, recommendationTemplate);
        }
    }
});
