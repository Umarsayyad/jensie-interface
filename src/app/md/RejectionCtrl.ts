import app from '../app';
import * as _ from 'lodash';
import * as angular from 'angular';

app.controller('RejectionCtrl', function ($scope, $uibModalInstance, modalService, Findings, Operations, params) {
    $scope.config = {};

    function loadFindingOptions() {
        Findings.options().$promise.then(function (result) {
            $scope.config = result.actions.POST;
        });
    }

    function loadOperationOptions() {
        $scope.config = Operations.options();
    }

    if (params.type === 'finding') {
        $scope.modalTitle = 'REJECT FINDING';
        loadFindingOptions();
    } else if (params.type === 'operation' && params.action === 'disapprove') {
        $scope.modalTitle = 'DISAPPROVE OPERATION';
        loadOperationOptions();
    } else if (params.type === 'operation' && params.action === 'reject') {
        $scope.modalTitle = 'REJECT OPERATION';
        loadOperationOptions();
    }

    var action = function (objs, obj) {
        if (params.type === 'operation') {
            if (params.action === 'disapprove') {
                Operations.disapprove({ operationId: obj.id }, { reasoning: $scope.reason }).$promise.then(function (op) {
                    if (params.remove) {
                        _.remove($scope.data[objs], { id: obj.id });
                        $uibModalInstance.close();
                    } else {
                        if (_.isArray($scope.data[objs])) {
                            var foundObj = _.find($scope.data[objs], { id: obj.id });
                            $scope.updateObjOld(foundObj, op);
                        } else {
                            $scope.updateObjOld($scope.data[objs], op);
                        }
                        if (typeof $scope.updateProgressBar === 'function') {
                            $scope.updateProgressBar();
                        }
                        $uibModalInstance.close(op);
                    }
                });
            } else if (params.action === 'reject') {
                Operations.reject({ operationId: obj.id }, { reasoning: $scope.reason }).$promise.then(function (op) {
                    if (params.remove) {
                        _.remove($scope.data[objs], { id: obj.id });
                        $uibModalInstance.close();
                    } else {
                        if (_.isArray($scope.data[objs])) {
                            var foundObj = _.find($scope.data[objs], { id: obj.id });
                            $scope.updateObjOld(foundObj, op);
                        } else {
                            $scope.updateObjOld($scope.data[objs], op);
                        }
                        if (typeof $scope.updateProgressBar === 'function') {
                            $scope.updateProgressBar();
                        }
                        $uibModalInstance.close(op);
                    }
                });
            }
        }
    };

    $scope.disapproveOrReject = function () {
        var selObjs = params.selObjs;
        var objs = params.objs;

        if (params.type === 'finding' && params.action === 'reject') {
            var finding_ids = [];
            var findings = $scope.data[params.selObjs] || params.selObjsData;
            for (var i = 0; i < findings.length; i++) {
                finding_ids.push(findings[i].id);
            }

            $scope.submission = {
                message: 'Rejecting Findings',
                promise: Findings.bulk_reject(
                    { findingId: 0 },
                    {
                        ids: finding_ids,
                        rejection: $scope.reason,
                    },
                    function () {
                        $uibModalInstance.close();
                    },
                    modalService.openErrorResponseCB('Bulk rejection of some findings failed')
                ).$promise,
            };
            return;
        } else {
            angular.forEach($scope.data[selObjs], function (obj) {
                console.log('rejecting ' + obj.id);
                action(objs, obj);
            });
        }
        if (!params.remove && params.type === 'finding') {
            $uibModalInstance.close();
        }
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});
