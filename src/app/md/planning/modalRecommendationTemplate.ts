import app from '../../app';
import * as _ from 'lodash';
import * as angular from 'angular';

app.component('modalRecommendationTemplate', {
    templateUrl: '/app/md/planning/modalRecommendationTemplate.html',
    bindings: {
        recommendationTemplate: '<',
        onSaved: '&',
        uibModalInstance: '<',
        config: '<',
    },
    controller: function (modalService, globalDataService, Recommendationtemplates) {
        var ctrl = this;
        ctrl.gdata = globalDataService;
        var $uibModalInstance;

        ctrl.$onInit = function () {
            // Bindings are now available
            $uibModalInstance = ctrl.uibModalInstance;
            // Initialize or copy rule object
            if (_.isUndefined(ctrl.recommendationTemplate)) {
                ctrl.modalTitle = 'ADD RECOMMENDATION TEMPLATE';
                ctrl.recommendationTemplate = {
                    body_text: '',
                };
                ctrl.new = true;
            } else {
                ctrl.modalTitle = 'EDIT RECOMMENDATION TEMPLATE';
                ctrl.recommendationTemplate = angular.copy(ctrl.recommendationTemplate);
            }
        };

        // Save/submit handlers
        ctrl.saveRecommendationTemplate = function () {
            var sendRecTemplate = _.pick(ctrl.recommendationTemplate, _.keys(ctrl.config));
            // cleanup objects to IDs
            _.each(sendRecTemplate, function (v, k) {
                var id = _.get(v, 'id');
                if (_.isNumber(id)) {
                    sendRecTemplate[k] = id;
                }
            });

            if (_.isUndefined(ctrl.recommendationTemplate.id)) {
                ctrl.submission = Recommendationtemplates.create(sendRecTemplate, recTemplateSaveSuccess, recTemplateSaveError);
            } else {
                ctrl.submission = Recommendationtemplates.update(sendRecTemplate, recTemplateSaveSuccess, recTemplateSaveError);
            }
        };

        function recTemplateSaveSuccess(recommendationTemplate) {
            ctrl.onSaved({ recommendationTemplate: recommendationTemplate });
            if (!ctrl.addAnother) {
                $uibModalInstance.close(recommendationTemplate);
            }
        }

        function recTemplateSaveError(result) {
            modalService.openErrorResponse('Recommendation Template could not be saved.', result, false);
        }

        ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    },
});
