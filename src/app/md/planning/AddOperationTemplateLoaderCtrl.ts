import app from '../../app';
import * as _ from 'lodash';
import * as angular from 'angular';
import '../../customer/manage/CondensedCustomerFilterComp';

app.controller('AddOperationTemplateLoaderCtrl', function ($scope, $uibModal) {
    $uibModal.open({
        templateUrl: '/app/md/planning/modalAddOperationTemplate.html',
        controller: 'addOperationTemplateCtrl',
        size: 'lg',
        scope: $scope,
    });
});

app.controller('addOperationTemplateCtrl', function ($scope, $state, $uibModalInstance, modalService, OperationTemplates, TaskTemplates) {
    $scope.template = { state: 'draft' };
    $scope.thisTaskTemplate = {};

    $scope.addOrUpdateOperationTemplate = function (finish) {
        //Get $nextStep function from scope used to call this function
        var $nextStep = this.$nextStep;

        if ($scope.template.description === undefined) {
            $scope.template.description = $scope.template.name;
        }

        if ($scope.template.id === undefined) {
            OperationTemplates.addTemplate(
                $scope.template,
                function (template) {
                    $scope.template = template;
                    if (finish) {
                        $scope.finish();
                    } else {
                        loadTaskOptions(template);
                        $nextStep();
                    }
                },
                function (result) {
                    modalService.openErrorResponse('Operation Template could not be saved.', result, false);
                }
            );
        } else {
            // TODO: This code should never be reached because we don't support going back to edit the operation
            console.warn("This code should never be reached because we don't support going back to edit the operation");

            OperationTemplates.updateTemplate(
                { templateId: $scope.template.id },
                $scope.template,
                function (template) {
                    $scope.updateObjOld($scope.template, template);
                    if (finish) {
                        $scope.finish();
                    } else {
                        loadTaskOptions(template);
                        $nextStep();
                    }
                },
                function (result) {
                    modalService.openErrorResponse('Operation Template could not be saved.', result, false);
                }
            );
        }
    };

    OperationTemplates.options(function (result) {
        $scope.operationTemplateConfig = result.actions.POST;
    });

    function loadTaskOptions(template) {
        console.log('loading task options for template: ', template);
        TaskTemplates.options(function (result) {
            $scope.taskConfig = result.actions.POST;
            $scope.taskConfig.workflow.options = {};

            var supported_applications = [];

            _.each($scope.taskConfig.application.choices, function (application) {
                // Check if all of the template's targettypes are supported by the application
                if (_.intersection(template.targettypes, application.supported).length === template.targettypes.length) {
                    supported_applications.push(application);
                }

                $scope.taskConfig.workflow.options[application.value] = application.workflows;
            });

            /* fix it so it only lists the ones you can use. */
            $scope.taskConfig.application.choices = supported_applications;
        });
    }

    $scope.saveTaskTemplate = function () {
        // `this` is the scope in effect when the function was called, so it has access to the form
        var operationTemplateTasksForm = this.operationTemplateTasksForm;

        if (_.isNull($scope.thisTaskTemplate.workflow)) {
            $scope.thisTaskTemplate.workflow = '';
        }
        if ($scope.thisTaskTemplate.description === undefined) {
            $scope.thisTaskTemplate.description = $scope.thisTaskTemplate.name;
        }

        if ($scope.shouldShowParameterSelection()) {
            // Copy parameter values into thisTaskTemplate.parameters
            var task_params =
                $scope.taskConfig.workflow.options[$scope.thisTaskTemplate.application][$scope.thisTaskTemplate.process][
                    $scope.thisTaskTemplate.workflow
                ].template_parameters;
            $scope.thisTaskTemplate.parameters = {};
            _.each(task_params, function (param) {
                if (!_.isUndefined(param.value)) {
                    $scope.thisTaskTemplate.parameters[param.name] = param.value;
                }
            });
        }

        if (_.isNil($scope.thisTaskTemplate.id)) {
            /* Create a new one, add it to the template on success. */
            $scope.thisTaskTemplate.order = $scope.template.tasktemplates.length + 1;

            OperationTemplates.addTask(
                { templateId: $scope.template.id },
                $scope.thisTaskTemplate,
                function (result) {
                    $scope.template.tasktemplates.push(result);
                    // TODO: Should notify user that task template was successfully added.
                    $scope.thisTaskTemplate = {};
                    operationTemplateTasksForm.$setPristine();
                    operationTemplateTasksForm.$setUntouched();
                },
                function (result) {
                    modalService.openErrorResponse('TaskTemplate could not be saved.', result, false);
                }
            );
        } else {
            /* Update a current one, then update it in the parent modal only on success. */
            // TODO: This code should never be reached because we don't support going back to edit a particular task after it is created

            console.warn("This code should never be reached because we don't support going back to edit a particular task after it is created");
            OperationTemplates.updateTask(
                { templateId: $scope.template.id, taskId: $scope.thisTaskTemplate.id },
                $scope.thisTaskTemplate,
                function (result) {
                    // TODO: Should notify user that task template was successfully updated.
                    $scope.thisTaskTemplate = {};
                    operationTemplateTasksForm.$setPristine();
                    operationTemplateTasksForm.$setUntouched();
                },
                function (result) {
                    modalService.openErrorResponse('TaskTemplate could not be saved.', result, false);
                }
            );
        }
    };

    $scope.shouldShowParameterSelection = function () {
        if (_.has($scope.taskConfig, ['workflow', 'options', $scope.thisTaskTemplate.application, $scope.thisTaskTemplate.process])) {
            var workflows =
                $scope.taskConfig.workflow.options[$scope.thisTaskTemplate.application][$scope.thisTaskTemplate.process][
                    $scope.thisTaskTemplate.workflow
                ];
            return _.size(workflows) > 0 && _.size(workflows.template_parameters) > 0;
        }
        return false;
    };

    $scope.dragControlListeners = {
        orderChanged: function (event) {
            angular.forEach(event.dest.sortableScope.modelValue, function (obj, key) {
                obj.order = key;
                TaskTemplates.partial_update({ templateId: obj.template, taskTemplateId: obj.id }, { order: key });
            });
        },
    };

    $scope.deleteTask = function (template, taskTemplate) {
        //THIS WAS:OperationTemplates.deleteTask({templateId: templateId, taskId: taskTemplateId},
        //GUESSING CORRECT CODE
        OperationTemplates.deleteTask({ templateId: template.id, taskId: taskTemplate.id }, function (result) {
            _.remove(template.tasktemplates, { id: taskTemplate.id });
        });
    };

    $scope.steps = [
        {
            templateUrl: '/app/md/planning/steps/creation.html',
            hasForm: true,
        },
        {
            templateUrl: '/app/md/planning/steps/tasks.html',
            hasForm: true,
        },
        {
            templateUrl: '/app/md/planning/steps/ordering.html',
        },
    ];

    $scope.finish = function () {
        //$scope.operationTemplate.tasktemplates = OperationTemplates.tasks({templateId: $scope.operationTemplate.id})
        $scope.template.service = _.find($scope.gdata.services, { id: $scope.template.service });
        $scope.templatesTable.data.push($scope.template);
        $uibModalInstance.dismiss();
    };

    $scope.cancel = function () {
        // Roll-back (delete the operaionTemplate)
        if ($scope.template !== null) {
            OperationTemplates.delete({ templateId: $scope.template.id });
        }
        $uibModalInstance.close();
    };

    $uibModalInstance.result.then(
        function () {
            $state.go('^');
        },
        function () {
            $state.go('^');
        }
    );
});
