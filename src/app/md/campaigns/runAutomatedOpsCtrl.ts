import app from '../../app';
import * as _ from 'lodash';
import * as angular from 'angular';

import '../../common/operator/taskParameters';

app.controller('runAutomatedOpsLoaderCtrl', function ($scope, $uibModal) {
    $uibModal.open({
        templateUrl: '/app/md/campaigns/modal_run_automated_ops.html',
        controller: 'runAutomatedOpsCtrl',
        size: 'lg',
        scope: $scope,
    });
});

app.controller('runAutomatedOpsCtrl', function ($scope, $state, $uibModalInstance, modalService, Campaigns, WizardHandler) {
    var statesCanStartAutomation = ['draft', 'fix up', 'created', 'rejected', 'requested'];

    var campaignId = parseInt($state.params.campaignId);
    $scope.campaignTasks = [];

    $scope.data.campaigns.$promise.then(function () {
        $scope.campaign = _.find($scope.data.campaigns, { id: campaignId });
    });

    $scope.alreadyStartedOperations = []; // TODO: tell the user these ops are already started and won't be automated?
    $scope.operationsWithParams = [];
    $scope.tasksWithParams = [];
    $scope.taskApplyAll = {};
    $scope.automatedOperationsWithNoParams = [];

    $scope.operationsLoading = Campaigns.operationDetails({ campaignId: campaignId }).$promise.then(function (operations) {
        $scope.operations = operations;
        $scope.gatherTasksWithParams();
    });

    $scope.filterTasks = function (task) {
        if (!$scope.taskApplyAll[task.tasktemplate]) {
            return true;
        }

        return task.checked;
    };

    $scope.checked = function (task) {
        $scope.taskApplyAll[task.tasktemplate] = task.checked;

        if (task.checked) {
            // If being checked, we need to set the copy source for others with the same task template.
            angular.forEach($scope.tasksWithParams, function (taskgroup) {
                angular.forEach(taskgroup, function (comparetask) {
                    comparetask.copySource = null;
                    if (task.tasktemplate === comparetask.tasktemplate && task.id !== comparetask.id) {
                        comparetask.copySource = task;
                    }
                });
            });
        }
    };

    $scope.gatherTasksWithParams = function () {
        $scope.alreadyStartedOperations = [];
        $scope.automatedOperationsWithNoParams = [];
        // Parameter inputs are grouped by task template, keep each its own list on a per-operation basis.
        var tasksByTemplate: any = {};

        angular.forEach($scope.operations, function (operation) {
            if (!_.includes(statesCanStartAutomation, operation.state)) {
                $scope.alreadyStartedOperations.push(operation);
            } else if ($scope.operationHasParams(operation)) {
                // Has parameters, pull out the tasks requiring parameters.
                angular.forEach(operation.tasks, function (task) {
                    if ($scope.taskHasParams(task)) {
                        if (!tasksByTemplate['template-' + task.tasktemplate]) {
                            tasksByTemplate['template-' + task.tasktemplate] = [];
                        }

                        tasksByTemplate['template-' + task.tasktemplate].push(task);

                        // Include the parent operation in the task, so details can reach the UI.
                        task.operation = operation;
                        task.show = true;
                    }

                    $scope.tasksWithParams = [];
                    for (var key in tasksByTemplate) {
                        $scope.tasksWithParams.push(tasksByTemplate[key]);
                    }
                });
            } else if (_.filter(operation.tasks, { process: 'automated' }).length > 0) {
                $scope.automatedOperationsWithNoParams.push(operation);
            }
        });
    };

    $scope.taskHasParams = function (task) {
        return task.accepted_parameters.length > 0;
    };

    $scope.operationHasParams = function (operation) {
        var hasParams = false;
        angular.forEach(operation.tasks, function (task) {
            if (hasParams === false) hasParams = $scope.taskHasParams(task);
        });
        return hasParams;
    };

    $scope.firstStep = function () {
        if (!WizardHandler.wizard()) return false;
        return WizardHandler.wizard().currentStepNumber() === 1;
    };

    $scope.lastStep = function () {
        var currentStepNumber = WizardHandler.wizard().currentStepNumber();
        var totalStepCount = WizardHandler.wizard().totalStepCount();
        return currentStepNumber === totalStepCount;
    };

    $scope.runCampaign = function () {
        var params = {};

        angular.forEach($scope.tasksWithParams, function (taskgroup) {
            angular.forEach(taskgroup, function (task) {
                // Default to the task's own accepted parameters, but if a copy source is given, use that.
                var sourceParams = task.accepted_parameters;
                if (!task.checked && task.copySource) {
                    sourceParams = task.copySource.accepted_parameters;
                }

                if (sourceParams.length > 0) {
                    if (_.isNil(params[task.id])) {
                        params[task.id] = {};
                    }

                    angular.forEach(sourceParams, function (parameter) {
                        params[task.id][parameter.name] = parameter.value;
                    });
                }
            });
        });

        Campaigns.runAutomatedOps(
            { campaignId: campaignId },
            params,
            function (updatedCampaign) {
                ($scope.automatedCampaignStarted || _.noop)(updatedCampaign);
                $uibModalInstance.close(updatedCampaign);
            },
            function (result) {
                // Should be getting a failed response for running an automated campaign?
                // TODO: on fail, update the status of the campaign run button..
                modalService.openErrorResponse('Could not run automated campaign', result, false);
            }
        );
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancelled');
    };

    $uibModalInstance.result.then(
        function () {
            $state.go('^');
        },
        function () {
            $state.go('^');
        }
    );
});
