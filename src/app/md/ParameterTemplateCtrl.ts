import app from '../app';
import * as _ from 'lodash';
import * as angular from 'angular';

import '../common/actionsDropdownComp';
import '../common/operator/taskParameters';
import '../operator/TaskParametersCtrl';

app.controller('ParameterTemplateCtrl', function ($scope, $uibModal, FileSaver, socketService, Blob, modalService, TaskParameters) {
    $scope.data.selectedPTemplates = [];

    $scope.validateMinMax = function (data, min, max) {
        if (data.length < min) {
            return 'Value is too short, minimum length: ' + min;
        }
        if (data.length > max) {
            return 'Value is too long, maximum length: ' + max;
        }
    };

    TaskParameters.options(function (result) {
        $scope.parameterTemplateConfig = result.actions.POST;
    });

    function loadParameterTemplates() {
        $scope.data.ptemplates = TaskParameters.list();
    }

    loadParameterTemplates();

    $scope.actions = {
        editParamTemplate: {
            action: function (template) {
                $scope.editParamTemplate(template);
            },
            class: 'btn-white',
            icon: 'fa-pencil',
            label: 'Edit',
            tooltip: 'Edit Parameter Template',
        },
        deleteParamTemplate: {
            action: function (template) {
                $scope.deleteParamTemplate(template);
            },
            class: 'btn-white',
            icon: 'fa-trash',
            label: 'Delete',
            tooltip: 'Delete this parameter template',
        },
    };

    $scope.importTemplates = function ($file) {
        if ($file !== null) {
            var reader = new FileReader();
            reader.addEventListener('loadend', function () {
                function displayImportResults(res) {
                    $uibModal.open({
                        templateUrl: '/app/md/planning/modalImportResult.html',
                        controller: function ($scope, $uibModalInstance) {
                            $scope.title = 'Import Result';
                            $scope.logmsgs = res.log;

                            $scope.errors = _.map(res.errors, function (err) {
                                if (!_.isString(err)) {
                                    return angular.toJson(err);
                                }
                                return err;
                            });

                            $scope.ok = function () {
                                $uibModalInstance.dismiss('ok');
                            };
                        },
                        size: 'lg',
                        scope: $scope,
                    });
                }

                TaskParameters.importTemplates(
                    reader.result,
                    function (res) {
                        displayImportResults(res);
                        loadParameterTemplates();
                    },
                    function (err) {
                        displayImportResults(err);
                    }
                );
            });
            reader.readAsText($file);
        }
    };

    var parameterTemplateUnsubscribe = socketService.subscribeByGroup('op-taskparameterstemplate', function (updatedParamTemplate) {
        var paramTemplate = _.find($scope.data.ptemplates, { id: updatedParamTemplate.id });

        if (!paramTemplate) {
            $scope.data.ptemplates.push(updatedParamTemplate);
        } else {
            $scope.updateObj(paramTemplate, updatedParamTemplate);
        }
    });

    $scope.$on('$destroy', function () {
        parameterTemplateUnsubscribe();
    });

    $scope.exportTemplates = function () {
        if ($scope.data.selectedPTemplates.length > 0) {
            var ids = _.map($scope.data.selectedPTemplates, 'id');
            TaskParameters.exportTemplates(
                {},
                ids,
                function (res) {
                    var data = new Blob([angular.toJson(res, true)], { type: 'application/json;charset=utf-8' });
                    FileSaver.saveAs(data, 'export.json');
                },
                function (err) {
                    console.log('export error: ', err);
                    modalService.openErrorResponse('Export Error', err, false);
                }
            );
        }
    };

    $scope.$watch('data.selectedPTemplates.length', function (newValue, oldValue) {
        $scope.allSelected = newValue !== 0 && newValue === $scope.data.ptemplates.length;
    });

    $scope.deleteParamTemplate = function (template) {
        modalService.openDelete('Confirmation', 'parameter template <em>' + template.name + '</em>', {}, function () {
            TaskParameters.delete({ paramsId: template.id }, {}, function () {
                _.remove($scope.data.ptemplates, { id: template.id });
            });
        });
    };

    $scope.editParamTemplate = function (template) {
        TaskParameters.availableTaskParameters(
            { paramsId: template.id },
            function (taskParameters) {
                template.accepted_parameters = taskParameters;
                $uibModal.open({
                    templateUrl: '/app/operator/modalTaskParameters.html',
                    controller: 'TaskParametersCtrl',
                    size: 'lg',
                    resolve: {
                        task: { template: template },
                        readonly: false,
                    },
                });
            },
            function (err) {
                modalService.openErrorResponse('Error fetching parameters', err, false);
            }
        );
    };

    $scope.alert = function (msg) {
        alert(msg);
    };
});
