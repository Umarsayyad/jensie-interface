import app from '../../app';
import { config } from '@cato/config';
import * as _ from 'lodash';

app.component('modalUploadExternalRoe', {
    bindings: {
        customer: '<',
        close: '<',
        dismiss: '<',
    },
    templateUrl: '/app/customer/manage/modalUploadExternalRoE.html',
    controller: function ($rootScope, Upload, modalService, globalDataService, RulesOfEngagements) {
        var ctrl = this;
        ctrl.catotheme = $rootScope.catotheme;
        ctrl.data = {};

        ctrl.$onChanges = function (changes) {
            if (changes.customer && changes.customer.currentValue) {
                ctrl.customerId = ctrl.customer.id || ctrl.customer;
                ctrl.customerObj = globalDataService.customerMap[ctrl.customerId];
                ctrl.roes = RulesOfEngagements.list({ customerId: ctrl.customerId }, function (roes) {
                    ctrl.roeId = roes[0].id; // TODO: change this when customers may have more than one RoE
                });
            }
        };

        // Save/submit handlers
        ctrl.uploadRoe = function () {
            ctrl.submission = Upload.upload({
                url: config.apiPath + 'customer/' + ctrl.customerId + '/roe/' + ctrl.roeId + '/upload_external',
                method: 'PUT',
                data: ctrl.data,
            }).then(
                function (resp) {
                    console.log('Success ' + resp.config.data.signed_document.name + ' uploaded. Response: ', resp);
                    ctrl.close(resp.data);
                },
                modalService.openErrorResponseCB('The external RoE was not uploaded successfully'),
                function (evt) {
                    if (evt !== undefined) {
                        var progressPercentage = Math.floor((100.0 * evt.loaded) / evt.total);
                        console.log('progress: ' + progressPercentage + '% ' + evt.config.data.signed_document.name);
                    }
                }
            );
        };
    },
});
