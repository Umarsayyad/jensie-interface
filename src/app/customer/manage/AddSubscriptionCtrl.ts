import app from '../../app';

app.controller('AddSubscriptionCtrl', function (
    $scope,
    $state,
    $stateParams,
    CustomerSubscriptions,
    SvcUtilsService,
    globalDataService,
    uibDateParser,
    modalService,
    $uibModal,
    $uibModalInstance,
    params
) {
    $scope.config = {};
    var customer_mode = params.customer_mode;
    $scope.customer_id = $state.params.customer_id;

    $scope.datePickers = {};
    $scope.dateOptions = {
        //minDate: new Date()
    };

    $scope.mode = 'new';
    $scope.subscription = {
        expiration_date: params.customer.period_of_performance_end_date,
    };

    if (params.subscription) {
        $scope.mode = 'edit';

        $scope.subscription = params.subscription;
        $scope.subscription.expiration_date = new Date($scope.subscription.expiration_date);
    }

    $scope.available_services = [];
    $scope.selectedService = {};
    for (var i = 0; i < $scope.gdata.services.length; i++) {
        var service = $scope.gdata.services[i];
        if (params.existing_services.indexOf(service.id) === -1) {
            $scope.available_services.push(service);
        } else if ($scope.mode === 'edit' && service.id === $scope.subscription.service) {
            $scope.selectedService = service;
            $scope.available_services.push(service);
        }
    }

    $scope.pickDate = function (subscription) {
        $scope.subscription.opened = true;
    };

    $scope.addSubscription = function () {
        if (customer_mode === 'new') {
            // If it's a new customer, just return the description of the service we want.
            $uibModalInstance.close({
                service: $scope.selectedService.id,
                expiration_date: $scope.subscription.expiration_date,
            });
            return;
        }

        var subscription_create_body: any = {};
        subscription_create_body.customer = $scope.customer_id;
        subscription_create_body.service = $scope.selectedService.id;
        subscription_create_body.expiration_date = $scope.subscription.expiration_date;

        if ($scope.mode === 'new') {
            var res = CustomerSubscriptions.add({ customerId: $scope.customer_id }, subscription_create_body).$promise.then(function (
                subscription_created
            ) {
                $uibModalInstance.close(subscription_created);
                params.customer.services.push(subscription_created.service);
                params.customer.subscriptions.push(subscription_created.id);
            },
            modalService.openErrorResponseCB('Failed to create subscription.'));
        } else {
            CustomerSubscriptions.update(
                {
                    customerId: $scope.customer_id,
                    subscriptionId: $scope.subscription.id,
                },
                subscription_create_body
            )
                .$promise.then(function (result) {
                    // Success
                    $uibModalInstance.close(result);
                })
                .catch(function (result) {
                    // It shouldn't be possible for the user to provide invalid input.
                    modalService.openErrorResponse('Failed to create subscription.', result);
                });
        }
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});
