import app from '../../app';
import * as _ from 'lodash';

app.component('condensedCustomerFilter', {
    templateUrl: '/app/customer/manage/condensedCustomerFilterForm.html',
    bindings: {
        setCustomer: '&',
        clearCustomer: '&?',
        placeholder: '<',
        initialCustomer: '<?',
        catoNgDisabled: '<?',
    },
    controller: function CondensedCustomerFilterCtrl($attrs, $rootScope, globalDataService, RulesOfEngagements, FileSaver) {
        var unregisterWatch;
        var customerCleared = true;
        var ctrl: any = {
            gdata: globalDataService,
            customerMap: globalDataService.customerMap,
            customers: globalDataService.customers,
            globalUser: _.get($rootScope.user, 'ui_data.selectedCustomer'),
            catotheme: $rootScope.catotheme,
            aAn: $rootScope.aAn,
            showSelectCustomer: false,
            required: 'required' in $attrs,
            noMonitor: 'noMonitorGlobal' in $attrs,
            form: {},
            $onInit: function () {
                // Bindings are now available
                // Set the default for the form (whoever we're managing)
                ctrl.form.customer = ctrl.initialCustomer || ctrl.globalUser;
                globalDataService.customers.$promise.then(function () {
                    if (ctrl.form.customer) {
                        ctrl.form.customer = globalDataService.customerMap[ctrl.form.customer.id || ctrl.form.customer];
                    }
                    customerCleared = !ctrl.form.customer;
                });
                if (!ctrl.noMonitor) {
                    unregisterWatch = $rootScope.$watch('user.ui_data.selectedCustomer', function (newVal, oldVal) {
                        if (newVal !== oldVal && !ctrl.catoNgDisabled) {
                            ctrl.form.customer = newVal;
                            ctrl.globalUser = newVal;
                            customerCleared = !ctrl.form.customer;
                        }
                    });
                }
            },
            $doCheck: function () {
                if ('required' in $attrs) {
                    ctrl.required = $attrs.required || _.isString($attrs.required);
                }
            },
            $onChanges: function (changesObj) {
                if (changesObj.initialCustomer && changesObj.initialCustomer.currentValue) {
                    // If initialCustomer changed and is currently truthy
                    var customerId = changesObj.initialCustomer.currentValue.id || changesObj.initialCustomer.currentValue;
                    if (!ctrl.form.customer || customerId !== ctrl.form.customer.id) {
                        // If customer is not set on form or customer id on form does not match the id of the newly changed customer in the attribute
                        globalDataService.customers.$promise.then(function () {
                            ctrl.form.customer = globalDataService.customerMap[customerId];
                            customerCleared = !ctrl.form.customer;
                        });
                    }
                }
            },
            $onDestroy: function () {
                if (unregisterWatch) {
                    unregisterWatch();
                }
            },
            set: function (item) {
                ctrl.showSelectCustomer = false;
                customerCleared = false;
                ctrl.setCustomer({ customer: item });
                ctrl.form.customer = item;
            },
            clear: function (fromButton) {
                if (ctrl.catoNgDisabled) {
                    return;
                }
                delete ctrl.form.customer;
                ctrl.showSelectCustomer = true;
                customerCleared = true;

                if (ctrl.clearCustomer) {
                    ctrl.clearCustomer({ fromButton: fromButton });
                } else {
                    ctrl.setCustomer({ customer: undefined });
                }
            },
            checkBlur: function () {
                if (!ctrl.form.customer && !customerCleared) {
                    // if customer isn't set and clear hasn't been called (or some other external change)
                    ctrl.clear(false);
                }
            },
            downloadRoE: function () {
                var response = RulesOfEngagements.downloadSigned({
                    customerId: ctrl.form.customer.id,
                    roeId: ctrl.customerMap[ctrl.form.customer.id].current_rules_of_engagement,
                }).$promise;
                response.then(function (data) {
                    FileSaver.saveAs(data.blob, 'Rules of Engagement');
                });
            },
        };

        for (var prop in ctrl) {
            if (!ctrl.hasOwnProperty(prop)) {
                //The current property is not a direct property of ctrl
                continue;
            }
            this[prop] = ctrl[prop];
        }

        //Replace the initial ctrl object with the updated this object in order to prevent the two from getting out of sync
        ctrl = this;
    },
});
