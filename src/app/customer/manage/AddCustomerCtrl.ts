import app from '../../app';
import * as _ from 'lodash';

app.controller('addCustomerLoaderCtrl', function ($scope, $state, $uibModal) {
    $uibModal.open({
        templateUrl: '/app/customer/manage/modalAddCustomer.html',
        controller: 'AddCustomerCtrl',
        size: 'lg',
        scope: $scope,
    });
});

app.controller('AddCustomerCtrl', function (
    $scope,
    $state,
    $q,
    moment,
    Customers,
    CustomerSubscriptions,
    globalDataService,
    modalService,
    $uibModal,
    $uibModalInstance
) {
    $scope.config = {};

    $scope.mode = 'new';
    $scope.subscriptions = [];
    var customer_id = 0;

    $state.params.existing_services = [];
    if ($state.params.customer_id) {
        /* edit user. */
        //console.log('edit customer: ' + parseInt($state.params.customer_id));

        $scope.mode = 'edit';

        customer_id = parseInt($state.params.customer_id);

        $scope.customer = _.clone($scope.gdata.customerMap[customer_id]);
        $scope.customer.period_of_performance_start_date = moment($scope.customer.period_of_performance_start_date).toDate();
        $scope.customer.period_of_performance_end_date = moment($scope.customer.period_of_performance_end_date).toDate();

        $scope.subscriptions = CustomerSubscriptions.query({ customerId: customer_id });
    } else {
        /* new customer. */
        console.log('new customer');
        //console.log('mode: ' + $scope.mode);
        $scope.customer = {
            period_of_performance_start_date: new Date(),
        };
    }

    $scope.removeSubscription = function (subscription) {
        if ($scope.mode === 'new') {
            for (var i = 0; i < $scope.subscriptions.length; i++) {
                if ($scope.subscriptions[i].service === subscription.service) {
                    $scope.subscriptions.splice(i, 1);
                    break;
                }
            }
        } else {
            CustomerSubscriptions.delete({ customerId: customer_id, subscriptionId: subscription.id }).$promise.then(function () {
                var customer = $scope.gdata.customerMap[customer_id];
                for (var i = 0; i < $scope.subscriptions.length; i++) {
                    if ($scope.subscriptions[i].id === subscription.id) {
                        $scope.subscriptions.splice(i, 1);
                        break;
                    }
                }
                for (var i = 0; i < $scope.customer.subscriptions.length; i++) {
                    if ($scope.customer.subscriptions[i] === subscription.id) {
                        $scope.customer.subscriptions.splice(i, 1);
                        break;
                    }
                }
                // Lacking a subscription, remove the service associated.
                for (var i = 0; i < $scope.customer.services.length; i++) {
                    if ($scope.customer.services[i] === subscription.service) {
                        $scope.customer.services.splice(i, 1);
                        break;
                    }
                }
                customer = $scope.gdata.customerMap[customer_id];
            });
        }
    };

    $scope.expirationColor = function (expirationDate) {
        if (new Date(expirationDate) < new Date()) {
            return 'red-txt';
        }
        return '';
    };

    $scope.addEditSubscription = function (subscription) {
        if (
            !subscription &&
            $scope.customer &&
            $scope.customer.subscriptions &&
            $scope.gdata.services.length == $scope.customer.subscriptions.length
        ) {
            return;
        }

        var existing_services = [];
        for (var i = 0; i < $scope.subscriptions.length; i++) {
            existing_services.push($scope.subscriptions[i].service);
        }

        var modalInstance = $uibModal.open({
            templateUrl: '/app/customer/manage/modalAddSubscription.html',
            controller: 'AddSubscriptionCtrl',
            resolve: {
                params: {
                    subscription: subscription,
                    existing_services: existing_services,
                    customer: $scope.customer,
                    customer_mode: $scope.mode,
                },
            },
        });

        modalInstance.result.then(function (result) {
            var service_id = result.service;
            for (var i = 0; i < $scope.subscriptions.length; i++) {
                subscription = $scope.subscriptions[i];
                if (subscription.service == service_id) {
                    $scope.subscriptions[i] = result;
                    return;
                }
            }
            $scope.subscriptions.push(result);
        });
    };

    $scope.getDisplayDate = function (date) {
        date = new Date(date);
        var month = date.getMonth() + 1;
        var day = date.getDate();
        var year = date.getFullYear();

        return month + '/' + day + '/' + year;
    };

    $scope.addCustomer = function () {
        var customer = _.clone($scope.customer);
        var customer_create_body: any = {};

        customer_create_body.name = customer.name;
        customer_create_body.nickname = customer.nickname;

        if (customer.duns) {
            // Format DUNS (if there aren't dashes)
            customer_create_body.duns = customer.duns.replace(/(\d{2})-?(\d{3})-?(\d{4})/, '$1-$2-$3');
        } else {
            console.log('no DUNS supplied');
        }
        customer_create_body.cagecode = customer.cagecode;
        customer_create_body.dnb = customer.dnb;
        customer_create_body.period_of_performance_start_date = moment(customer.period_of_performance_start_date).format('YYYY-MM-DD');
        customer_create_body.period_of_performance_end_date = moment(customer.period_of_performance_end_date).format('YYYY-MM-DD');

        console.log('customer:' + JSON.stringify(customer_create_body, null, 2));

        if ($scope.mode === 'new') {
            /* The backend prevents a user from hacking the UI to try and do things they shouldn't. */
            $scope.submission = Customers.add(
                customer_create_body,
                function (customer_created) {
                    $scope.check_sam(customer_created);

                    if (!$scope.gdata.customerMap[customer_created.id]) {
                        globalDataService.addCustomer(customer_created);
                    }

                    var subscriptionPromises = _.map($scope.subscriptions, function (subscription) {
                        return CustomerSubscriptions.add(
                            { customerId: customer_created.id },
                            subscription,
                            function (subscription_created) {
                                customer_created.services.push(subscription_created.service);
                                customer_created.subscriptions.push(subscription_created.id);
                            },
                            modalService.openErrorResponseCB('Error creating subscription')
                        ).$promise;
                    });
                    if (subscriptionPromises.length) {
                        $scope.submission = $q.all(subscriptionPromises).then(function () {
                            $uibModalInstance.close();
                        });
                    } else {
                        $uibModalInstance.close();
                    }
                },
                modalService.openErrorResponseCB('Error creating customer')
            );
        } else {
            /* it's an edit. */
            $scope.submission = Customers.update(
                { customerId: customer_id },
                customer_create_body,
                function (result) {
                    globalDataService.updateCustomer(result);
                    $scope.check_sam(result);
                    $uibModalInstance.close();
                },
                modalService.openErrorResponseCB('Error updating customer')
            );
        }
    };

    $scope.check_sam = function(customer) {
        console.log("Checking customer:", customer);
        if (!customer.sam_verified && (customer.duns || customer.cagecode)) {
            Customers.validateSam({ customerId: customer.id }, { set_auto: false });
        }
    };

    $scope.update2FA = function () {
        Customers.set2FA(
            { customerId: $scope.customer.id },
            { enforce_2fa: $scope.customer.enforce_2fa },
            _.noop,
            modalService.openErrorResponseCB('Unable to set 2FA enforcement')
        );
    };

    $scope.updateAccessGroups = function () {
        Customers.setRequireAccessGroups(
            { customerId: $scope.customer.id },
            { require_access_groups: $scope.customer.require_access_groups },
            _.noop,
            modalService.openErrorResponseCB('Unable to set access group requirement')
        );
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };

    $uibModalInstance.result.then(
        function () {
            $state.go('^');
        },
        function () {
            $state.go('^');
        }
    );
});
