import app from '../../app';
import { config } from '@cato/config';
import * as angular from 'angular';
import { INonDisclosureResult } from '../../../ng-app/customer/models/non-disclosure.interface';

app.component('modalManageNda', {
    bindings: {
        customerId: '<',
        ndaFiles: '<',
        close: '<',
        dismiss: '<',
    },
    templateUrl: '/app/customer/manage/modalManageNda.html',
    controller: function ($rootScope, Upload, modalService, globalDataService, NonDisclosureResource, FileSaver) {
        const ctrl = this;
        ctrl.deleted = [];
        ctrl.uploaded = [];

        ctrl.$onInit = function () {
            ctrl.customerObj = globalDataService.customerMap[ctrl.customerId];
            ctrl.isMissionDirectorOrAdmin = $rootScope.gdata.isMissionDirectorOrAdmin;
            ctrl.files = angular.copy(ctrl.ndaFiles);
        };

        ctrl.downloadNda = function (ndaFile) {
            NonDisclosureResource.downloadNda(ndaFile.customer, ndaFile.id)
                .then((res) => {
                    let blob = new Blob([res]);
                    FileSaver.saveAs(blob, ndaFile.name);
                })
                .catch((error) => {
                    modalService.openErrorResponse('Unable to download the NDA document', error, false);
                });
        };

        ctrl.removeNda = function (ndaFile) {
            modalService.openConfirm('Delete NDA?', 'delete', ` ${ndaFile.name}`, null, function () {
                NonDisclosureResource.delete(ndaFile.customer, ndaFile.id).subscribe((res) => {
                    ctrl.deleted.push(ndaFile);

                    // remove deleted file from ndaFiles
                    ctrl.files = ctrl.files.filter((item) => {
                        return item.id != ndaFile.id;
                    });

                    // remove deleted file from uploaded
                    ctrl.uploaded = ctrl.uploaded.filter((item) => {
                        return item.id != ndaFile.id;
                    });
                });
            });
        };

        ctrl.uploadNda = function () {
            ctrl.submission = Upload.upload({
                url: config.apiPath + 'customer/' + ctrl.customerId + '/nda',
                method: 'POST',
                data: ctrl.data,
            }).then(function (resp) {
                ctrl.files = ctrl.files ? ctrl.files : [];
                ctrl.files.push(resp.data);

                ctrl.data.attachment = undefined;
                ctrl.uploaded.push(resp.data);
            }, modalService.openErrorResponseCB('The NDA document failed to upload'));
        };

        ctrl.hasExisting = function () {
            return ctrl.files && ctrl.files.length > 0;
        };

        ctrl.done = function () {
            const ret: INonDisclosureResult = { deleted: ctrl.deleted, uploaded: ctrl.uploaded };
            ctrl.close(ret);
        };
    },
});
