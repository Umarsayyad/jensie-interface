import '../app/app';
import '../app/cato.main';
import '../app/common/general';
import './downgrades';
import './cato-files';
import './cato-templates';

import { bootstrapCato } from '../ng-app/main';
import { Session } from './session';
import { LicenseManager } from 'ag-grid-enterprise';
import { config } from '@cato/config';

const session = new Session();

session.loadData().then(() => {
    console.log('session loaded -- starting app...');
    LicenseManager.setLicenseKey(config.agGridLicense);
    bootstrapCato();
});
