//This file is a loader for all the CATO services, controllers, and components.
//It contains the top-level required files, not a complete list.

//cato.routes.cato
import '../app/login/login';
import '../app/common/navbar/NavbarCtrl';
import '../app/dashboard/SidebarCtrl';
import '../app/common/passwordManagement/InitialSetPasswordCtrl';
import '../app/common/passwordManagement/ForgotPasswordCtrl';
import '../app/common/passwordManagement/ResetPasswordCtrl';
import '../app/common/passwordManagement/ResetPasswordCtrl';

//cato.routes.general
import '../app/attackSurface/AttackSurfaceCtrl';
import '../app/common/navbar/NavbarCtrl';
import '../app/common/RandomColorService';
import '../app/dashboard/DashboardCtrl';
import '../app/dashboard/recentFindings/RecentFindingsCtrl';
import '../app/dashboard/SidebarCtrl';
import '../app/finding/FindingService';
import '../app/message/MessageService';
import '../app/notification/NotificationService';
import '../app/service/SvcUtilsService';
import '../app/settings/NotificationsCtrl';
import '../app/target/TargetService';

import '../app/dashboard/widgets/completedOps/CompletedOpsCtrl';
import '../app/dashboard/widgets/cyvar/CyvarCtrl';
import '../app/dashboard/widgets/findingsBy/FindingsByCtrl';
import '../app/dashboard/widgets/mvt/MvtCtrl';
import '../app/dashboard/widgets/opsActivity/OpsActivityCtrl';
import '../app/dashboard/widgets/phishing/PhishingCtrl';
import '../app/dashboard/widgets/phishing/PhishingCannedDataService';
import '../app/dashboard/widgets/targetTypes/targetTypesCtrl';
import '../app/dashboard/widgets/todo/todoTmplCtrl';
import '../app/dashboard/widgets/todo/AddToDoCtrl';
import '../app/dashboard/widgets/services/ServicesCtrl';
import '../app/dashboard/widgets/targetCount/TargetCountCtrl';
import '../app/dashboard/widgets/scoreHistory/score-history.component';

//cato.routes.customer
import '../app/accessGroups/AddPermissionCtrl';
import '../app/appliances/AppliancesCtrl';
import '../app/common/passwordManagement/ChangePasswordCtrl';
import '../app/common/RandomColorService';
import '../app/finding/AddFindingCtrl';
import '../app/finding/EditCtrl';
import '../app/finding/FindingService';
import '../app/finding/FindingsTabCtrl';
import '../app/incidentReporting/IncidentReportingCtrl';
import '../app/incidentReporting/IncidentReportsCtrl';
import '../app/maps/MapsCmp';
import '../app/message/MailboxCtrl';
import '../app/message/MessageService';
import '../app/message/MessageFormCtrl';
import '../app/note/NoteCtrl';
import '../app/note/NoteDisplayCtrl';
import '../app/roe/roeComp';
import '../app/service/customerServices/ServiceCatCtrl';
import '../app/service/SvcUtilsService';
import '../app/settings/CompanyCtrl';
import '../app/settings/poc/AddPoCAddressCtrl';
import '../app/settings/poc/AddPoCCtrl';
import '../app/settings/poc/DelPoCCtrl';
import '../app/settings/ProfileCtrl';
import '../app/settings/TwoFactorDevicesCtrl';
import '../app/training/TrainingCtrl';
import '../app/target/AddTargetCtrl';
import '../app/target/MetaTargetDetailsCtrl';
import '../app/target/TargetsCtrl';
import '../app/target/TargetDetailsCtrl';
import '../app/target/TargetService';
import '../app/user/AddUserCtrl';
import '../app/user/ManageUsersCtrl';

//cato.routes.missionDirector
import '../app/appliances/AppliancesCtrl';
import '../app/appliances/ApplianceEditCtrl';
import '../app/campaigns/AddCampaignCtrl';
import '../app/campaigns/CampaignsCtrl';
import '../app/campaign/CampaignTemplateCtrl';
import '../app/customer/manage/AddCustomerCtrl';
import '../app/customer/manage/AddSubscriptionCtrl';
import '../app/customer/manage/modalUploadExternalRoE';
import '../app/customer/manage/modalManageNda';
import '../app/feedback/FeedbackCtrl';
import '../app/md/campaigns/runAutomatedOpsCtrl';
import '../app/md/ParameterTemplateCtrl';
import '../app/md/planning/AddOperationTemplateLoaderCtrl';
import '../app/md/RecommendationTemplateCtrl';
// import '../md/OpAnalysisModuleCtrl';     //doesn't exist...
import '../app/note/NoteCtrl';
import '../app/note/NoteDisplayCtrl';
import '../app/operation/OpTemplatesCtrl';

//cato.routes.operator
import '../app/common/RandomColorService';
import '../app/developer/DeveloperCmp';
import '../app/finding/AddFindingCtrl';
import '../app/finding/EditCtrl';
import '../app/finding/FindingService';
import '../app/finding/FindingsTabCtrl';
import '../app/md/RejectionCtrl';
import '../app/message/MessageFormCtrl';
import '../app/message/MessageService';
import '../app/note/NoteCtrl';
import '../app/operation/OperationService';
import '../app/operation/operationsTab/autoScheduleRulesTab';
import '../app/operation/operationsTab/OperationsTableCtrl';
import '../app/operation/operationsTab/OperationsCalendarCtrl';
import '../app/operator/AnalyzeResultsCtrl';
import '../app/operator/LockingTargetsCtrl';
import '../app/operator/OperatorCtrl';
import '../app/operator/TtpCtrl';
import '../app/target/AddAccessGroupCtrl';
import '../app/target/AddTargetCtrl';
import '../app/target/MetaTargetDetailsCtrl';
import '../app/target/TargetDetailsCtrl';
import '../app/target/TargetService';
import '../app/todo/TodoCtrl';
