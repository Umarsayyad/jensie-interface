//Eric Warrington

export class Session {
    readonly UNLOAD_TIMESTAMP_KEY = 'unloadTimestamp';

    // Use localStorage as a way to pass data stored in sessionStorage between tabs
    // Derived from https://blog.guya.net/2015/06/12/sharing-sessionstorage-between-tabs-for-secure-multi-tab-authentication/
    readonly GET_STORAGE_KEY = 'getSessionStorage';
    readonly STORAGE_DATA_KEY = 'sessionStorage';

    private log: Function;

    private indicator = Date.now().toString();
    private sessionStoreLoaded = false;
    private sessionStoreLoadResolved: Function = null;

    constructor() {
        this.log = (...args: any[]) => {};
        // Uncomment the line below for debug logging
        // this.log = console.log;

        this.setEventListeners();
    }

    private setEventListeners() {
        addEventListener('beforeunload', (e) => this.beforeunloadListener(e));
        addEventListener('storage', (e) => this.storageListener(e));
    }

    public loadData(): Promise<void> {
        return new Promise((resolve, reject) => {
            if (this.sessionStoreLoadResolved != null) {
                reject('Trying to load session store twice!');
            }

            this.sessionStoreLoadResolved = resolve;
            let unloadTimestamp = parseInt(sessionStorage.getItem(this.UNLOAD_TIMESTAMP_KEY));
            if (unloadTimestamp && Date.now() > unloadTimestamp + 20000) {
                // If more than 20 seconds have elapsed since the page was unloaded
                console.log('Page unloaded for too long; clearing session');
                sessionStorage.removeItem('token');
                sessionStorage.removeItem('user');
                sessionStorage.removeItem(this.UNLOAD_TIMESTAMP_KEY);
            }

            if (!sessionStorage.getItem('token')) {
                // Ask other tabs for session storage
                this.log('we do not have any sessionStorage data to start; try requesting it');
                localStorage.setItem(this.GET_STORAGE_KEY, this.indicator);

                // if other tabs haven't responded in .5 seconds, assume we are the only/first tab
                setTimeout(() => this.sessionLoadComplete(), 500);
            } else {
                this.log('we already have sessionStorage data, record as loaded');
                this.sessionLoadComplete();
            }
        });
    }

    private sessionLoadComplete() {
        this.log('sessionLoadComplete() called ' + (sessionStorage.getItem('token') ? 'with storage populated' : 'with no data loaded'));
        if (!this.sessionStoreLoaded) {
            this.log('record loading as complete');
            this.sessionStoreLoaded = true;
            this.sessionStoreLoadResolved();
        }
    }

    private beforeunloadListener(e: Event) {
        sessionStorage.setItem(this.UNLOAD_TIMESTAMP_KEY, Date.now().toString());
    }

    private storageListener(event: StorageEvent) {
        let data: { token: string; user: string };
        this.log('storage event', event);

        if (event.key === this.GET_STORAGE_KEY && event.newValue !== this.indicator) {
            // Some tab asked for the sessionStorage -> send it
            this.log('sending sessionStorage data to another tab');

            data = {
                token: sessionStorage.getItem('token'),
                user: sessionStorage.getItem('user'),
            };

            localStorage.setItem(this.STORAGE_DATA_KEY, JSON.stringify(data));
            localStorage.removeItem(this.STORAGE_DATA_KEY);
            localStorage.removeItem(this.GET_STORAGE_KEY);
        } else if (event.key === this.STORAGE_DATA_KEY && event.newValue && !sessionStorage.getItem('token')) {
            // sessionStorage is empty -> fill it
            this.log('got sessionStorage data from another tab');

            data = JSON.parse(event.newValue) || {};

            sessionStorage.setItem('token', data.token);
            sessionStorage.setItem('user', data.user);
            this.sessionLoadComplete();
        }
    }
}
