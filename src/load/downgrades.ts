import { MatCard } from '@angular/material/card';
import { MatDialog } from '@angular/material/dialog';
import { downgradeComponent, downgradeInjectable } from '@angular/upgrade/static';

import { appModule } from '../app/cato.module';
import { BackupService, CatoTableComponent, TargetDisplayComponent } from '../ng-app/common';
import { AttachmentComponent } from '../ng-app/common/components/attachment-upload/attachment.component';
import { AttachmentService } from '../ng-app/common/services/attachment.service';
import { IndustrySelectorComponent } from '../ng-app/company-profile';
import { NaicsIndustryService } from '../ng-app/company-profile';
import { NonDisclosureResource } from '../ng-app/customer/services/non-disclosure.resource';
import { DashboardWidgets } from '../ng-app/dashboard/services/dashboard-widgets.service';
import { SidebarService } from '../ng-app/dashboard/services/sidebar.service';
import { PreferencesService } from '../ng-app/preferences/services/preferences.service';
import { ToolsWindow } from '../ng-app/tools';

appModule().directive('catoTable', downgradeComponent({ component: CatoTableComponent }));
appModule().directive('industrySelector', downgradeComponent({ component: IndustrySelectorComponent }));
appModule().directive('toolsWindow', downgradeComponent({ component: ToolsWindow }));
appModule().directive('attachmentUpload', downgradeComponent({ component: AttachmentComponent }));
appModule().directive('targetDisplay', downgradeComponent({ component: TargetDisplayComponent }));
appModule().directive('matCard', downgradeComponent({ component: MatCard }));

// Allow angularjs to use these services
appModule().factory('BackupService', downgradeInjectable(BackupService) as any);
appModule().factory('AttachmentService', downgradeInjectable(AttachmentService) as any);
appModule().factory('DashboardWidgets', downgradeInjectable(DashboardWidgets) as any);
appModule().factory('PreferencesService', downgradeInjectable(PreferencesService) as any);
appModule().factory('NaicsIndustryService', downgradeInjectable(NaicsIndustryService) as any);
appModule().factory('SidebarService', downgradeInjectable(SidebarService) as any);
appModule().factory('NonDisclosureResource', downgradeInjectable(NonDisclosureResource) as any);
appModule().factory('MatDialog', downgradeInjectable(MatDialog) as any);
