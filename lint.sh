#!/bin/bash

modified_files=$(git ls-files --modified | grep -E '\.ts$')

if [ "x$modified_files" != "x" ]; then
    npx eslint $(for file in $modified_files; do echo -n "$file "; done)
fi
