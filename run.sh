#!/bin/bash

NGINX_CONF=/etc/nginx/conf.d/cato-ui.conf
CN_NAME=testing.catosec.io

if [ -z "$UI_DOMAIN_NAME" ]; then
    echo "no UI_DOMAIN_NAME supplied, access this host by IP only"
else
    echo "UI_DOMAIN_NAME supplied, add it to the nginx conf file"
    sed -i -e "s/#UI_DOMAIN/server_name ${UI_DOMAIN_NAME};/g" $NGINX_CONF
    CN_NAME=${UI_DOMAIN_NAME}
fi

if [ -z "$CATO_PROD_MODE" ]; then
    echo "CATO_PROD_MODE not supplied, so assume it's production and set this to true"
    CATO_PROD_MODE=true
fi

sed -i -e "s/SKIP_APPROVALS_MIN_LEVEL/${SKIP_APPROVALS_MIN_LEVEL}/g" /opt/cato/app/config.js
sed -i -e "s/CATO_PROD_MODE/${CATO_PROD_MODE}/g" /opt/cato/app/config.js
sed -i -e "s/CATO_THEME/${CATO_THEME}/g" /opt/cato/app/config.js
sed -i -e "s/SHOW_MAPS/${SHOW_MAPS}/g" /opt/cato/app/config.js
sed -i -e "s/MEP_INSTANCE/${MEP_INSTANCE:-false}/g" /opt/cato/app/config.js

if [ "${SHOW_MAPS}" = "true" ]; then
    ln -f /etc/nginx/conf.d/available/cato-maps.conf /etc/nginx/conf.d/enabled/cato-maps.conf
fi

# If we don't have certs from a volume map, then let's gen them
if [ ! -f /etc/ssl/certs/nginx.crt ]; then
    echo "NGINX certs not found, creating self signed..."
    mkdir -p /etc/ssl/certs
    mkdir -p /etc/ssl/private
    cd /tmp
    openssl req -new -newkey rsa:2048 -days 1825 -nodes -x509 -keyout nginx.key -subj /CN=${CN_NAME} -out nginx.crt
    mv /tmp/nginx.key /etc/ssl/private/nginx.key
    mv /tmp/nginx.crt /etc/ssl/certs/nginx.crt
fi

# NOTE: In debian 10 image (nginx 1.16), our sed logic causes permission denied issue with this file.
# As a workaround right now, re-chmod it with the same permissions it already has to fix it.
chmod 664 /opt/cato/app/config.js

chmod og-rwx /etc/ssl/private/nginx.key
echo "Starting nginx..."
exec nginx -g "daemon off;"
