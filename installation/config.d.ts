import { ValidTheme } from './themes/valid-themes';

interface Config {
    uiDomain: string;
    apiPath: '/api/v1/' | string;
    catotheme: ValidTheme;
    api: string;
    version: string;
    support: string;
    helpNowNumber: '410-779-6700' | string;
    initialState: 'cato.loggedIn.dashboard' | string;
    linkToWiki: boolean;
    loginRefreshTimer: 300 | number;
    prod: 'true' | 'false';
    showMaps: 'true' | 'false';
    skipApprovalsMinLevel: 10 | number;
    mepInstance: 'true' | 'false';
    agGridLicense: string;
}

export declare const config: Readonly<Config>;
