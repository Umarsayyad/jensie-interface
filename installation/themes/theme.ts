import { ValidTheme } from "./valid-themes";

export interface Theme
{
    appName: string;
    appCompany: string,
    appCompanyDomain: string,
    name: ValidTheme;
    customer: string;
    customerPlural: string;
    // Prefer NOT to use _uppercase and _capitalize, but the .uppercase and .capitalize classes don't work properly in data tables
    customerUppercase: string;
    customerUppercasePlural: string;
    customerCapitalize: string;
    customerCapitalizePlural: string;
    extendedCustomer: string;
    login: { text: string};
    msgCompose: { sendToAllTooltip: string };
}
