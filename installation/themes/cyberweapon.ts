import { Theme } from "./theme";
import { ValidTheme } from "./valid-themes";

export const cyberweapon: Theme =
{
    appName: 'CATO',
    appCompany: 'CyberPoint International',
    appCompanyDomain: 'cyberpointllc.com',
    name: ValidTheme.cyberweapon,
    customer: "entity",
    customerPlural: "entities",
    // Prefer NOT to use _uppercase and _capitalize, but the .uppercase and .capitalize classes don't work properly in data tables
    customerUppercase: "ENTITY",
    customerUppercasePlural: "ENTITIES",
    customerCapitalize: "Entity",
    customerCapitalizePlural: "Entities",
    extendedCustomer: "target entity/group",
    login:
    {
        text: "CATO is not a penetration testing product. This is a distributed, scalable unified cyber operations platform for scheduling and conducting defensive cyber operations (DCO) and offensive cyber operations (OCO) for both wired and wireless targets at the push of a button. CATO supports a wide variety of target types from IP Addresses and Address Ranges to hostnames and URLs. Users assume ALL responsibility for the effect their actions while logged into this system."
    },
    msgCompose:
    {
        sendToAllTooltip: "Send to all users responsible for entity plus all CATO operators (if username field blank); or provide to filter single users in address book",
    }
};
