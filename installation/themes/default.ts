import { Theme } from "./theme";
import { ValidTheme } from "./valid-themes";

export const defaultTheme: Theme =
{
    appName: 'CATO',
    appCompany: 'CyberPoint International',
    appCompanyDomain: 'cyberpointllc.com',
    name: ValidTheme.default,
    customer: "customer",
    customerPlural: "customers",
    // Prefer NOT to use _uppercase and _capitalize, but the .uppercase and .capitalize classes don't work properly in data tables
    customerUppercase: "CUSTOMER",
    customerUppercasePlural: "CUSTOMERS",
    customerCapitalize: "Customer",
    customerCapitalizePlural: "Customers",
    extendedCustomer: "customer",
    login:
    {
        text: "CATO is not a normal penetration testing service. Instead of providing a commodity assessment, CATO provides novel red team services that closely match the operational cadence of real-world attacks. CATO enables customers to have much better insight into their security posture by witnessing well-trained and tool-rich operators attempt to gain access to their network. CATO is the next evolution in offensive services.",
    },
    msgCompose:
    {
        sendToAllTooltip: "Send to all users of a company plus all CATO operators (if username field blank); or provide to filter single users in address book",
    }
};
