import { config } from '@cato/config';
import { defaultTheme } from './default';
import { cyberweapon } from './cyberweapon';
import { jensie } from './jensie';
import { Theme } from './theme';
import { ValidTheme } from './valid-themes';


let theme: Theme;

switch(config.catotheme)
{
    case ValidTheme.default:
        theme=defaultTheme;
        break;
    case ValidTheme.cyberweapon:
        theme=cyberweapon;
        break;
    case ValidTheme.jensie:
        theme=jensie;
        break;
    default:
        throw new Error("Invalid Cato theme: " + config.catotheme);
}

export default theme;
