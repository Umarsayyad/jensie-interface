import { Theme } from "./theme";
import { ValidTheme } from "./valid-themes";

export const jensie: Theme =
{
    appName: 'Jensie',
    appCompany: 'MISI',
    appCompanyDomain: 'misi.tech',
    name: ValidTheme.jensie,
    customer: "customer",
    customerPlural: "customers",
    // Prefer NOT to use _uppercase and _capitalize, but the .uppercase and .capitalize classes don't work properly in data tables
    customerUppercase: "CUSTOMER",
    customerUppercasePlural: "CUSTOMERS",
    customerCapitalize: "Customer",
    customerCapitalizePlural: "Customers",
    extendedCustomer: "customer",
    login:
    {
        text: "Jensie is here for your protection. We are not like anything you have seen before. Jensie is designed to not only continually monitor your environment, it will assist in the security compliance and auditing process and is staffed by a team of operators who provide novel red team services that closely match the operational cadence of real-world attacks. Our operators will attack and Jensie tells you what was found, even providing courses of action for vulnerability mitigation or elimination. Jensie is the next evolution in continuous monitoring services, watching your back 24x7.",
    },
    msgCompose:
    {
        sendToAllTooltip: "Send to all users of a company plus all Jensie operators (if username field blank); or provide to filter single users in address book",
    }
};
