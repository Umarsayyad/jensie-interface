const config =
{
    "uiDomain": "CATO_DOMAIN",
    "apiPath": "/api/v1/",
    "catotheme": "CATO_THEME",
    "api": "CATO_DOMAIN" + "API_PATH",
    "version": "VERSION",
    "support": "SUPPORT",
    "helpNowNumber": "410-779-6700",
    "initialState": "cato.loggedIn.dashboard",
    "linkToWiki": true,
    "loginRefreshTimer": 300,
    "prod": "CATO_PROD_MODE",
    "showMaps": "SHOW_MAPS",
    "mepInstance": "MEP_INSTANCE",
    "agGridLicense": "CompanyName=Maryland Innovation & Security Institute,LicensedGroup=DreamPort,LicenseType=MultipleApplications,LicensedConcurrentDeveloperCount=5,LicensedProductionInstancesCount=5,AssetReference=AG-008852,ExpiryDate=9_July_2021_[v2]_MTYyNTc4NTIwMDAwMA==584d2a5ba1fe935f96b44f792434de2c",
    "skipApprovalsMinLevel": Number("SKIP_APPROVALS_MIN_LEVEL") || 10,  // || 10 ensures there is always a valid default if a number couldn't be parsed but it also prevents setting the level to 0; if you really want any level operator to not need approvals, use -1
};


window.config=config;
Object.freeze(window.config);
if(typeof module !== typeof undefined) module.exports=config;

// hack to set the favicon (derived from https://stackoverflow.com/a/260876)
var link = document.createElement('link');
link.type = 'image/x-icon';
link.rel = 'icon';
link.href = '/app/img/' + config.catotheme + '/favicon.ico';
document.getElementsByTagName('head')[0].appendChild(link);
